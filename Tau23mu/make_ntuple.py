"""Configure DaVinci for creating ntuples from Turbo data."""
import os
from Configurables import DaVinci
from Tau23mu.utils import CreateDTT, TrueMCTuple, ReRunStrippings, MomentumCorrection, RootInTes
from Tau23mu.dictionaries import StrippingVersions, decay_descriptors_trueMC, decayInfo

########################################
# User configuration begins
########################################

# # Data-taking year
# data_type = '2017'
data_type = DaVinci().DataType
# # data or MC?
IsMC = DaVinci().Simulation

# Create a tuple for each line
ntuples = [CreateDTT(decay,IsMC) for decay in decayInfo.keys()]
evtId = DaVinci().TupleFile if IsMC else None
if evtId!=None: ntuples+=[TrueMCTuple(evtId,decay_descriptors_trueMC[evtId])]

userAlgos = []
if IsMC: ReRunStrippings(StrippingVersions[data_type],
                        ['Stripping'+ decayInfo[decay]['strip_line_name'] for decay in decayInfo.keys()],
                         userAlgos)

# Setup DaVinci
dv = DaVinci()
#dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = userAlgos+[MomentumCorrection(IsMC)]+ntuples
#dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.DataType = data_type
dv.Turbo = False
#dv.InputType = 'DST' if IsMC else 'MDST' # done later
dv.RootInTES = RootInTes(IsMC)
dv.Lumi = True

from Configurables import MessageSvc
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
