# Tau23mu ntuple options

Options files for creating ntuples for $`\tau^{+} \to \mu^+\mu^{-}\mu^{+}`$ analysis.

## Data
Run1 + Run2

<!-- ## Monte Carlo samples

* 27165070: $`D^{*} \to D^{0} (\to K \pi \pi \pi) \pi`$ (Phase space model)
* 27165071: $`D^{*} \to D^{0} (\to K^- \pi^+ \pi^- \pi^+) \pi`$ (AmpGen CF model)
* 27165072: $`D^{*} \to D^{0} (\to K^+ \pi^- \pi^+ \pi^-) \pi`$ (AmpGen DCS model)

* 27165000: $`D^{*} \to D^{0} (\to K K \pi \pi) \pi`$
* 27165003: $`D^{*} \to D^{0} (\to K K \pi \pi) \pi`$ -->

## Log
* ... (2020/04/30) - first release and production of Run1 data
