# Stripping Line Names
StrippingVersions = {
    '2011': '21r1p2',
    '2012': '21r0p2',
    '2015': '24r2',
    '2016': '28r2',
    '2017': '29r2p1',
    '2018': '34r0p1',
}

decayInfo = {
    'tau23mu': {
        'strip_line_name' : 'Tau23MuTau23MuLine',
        'decay_descriptor': '${tau}[tau+ -> ${mup1}mu+ ${mum}mu- ${mup2}mu+]CC',
        'mother'          : 'tau',
        'daughters'       : ['mup1','mum','mup2']
        },
    'd2mumupi': {
        'strip_line_name' : 'Tau23MuDs2PhiPiLine',
        'decay_descriptor': '${Ds}[D_s+ -> ${pi}pi+ ${mup}mu+ ${mum}mu-]CC',
        'mother'          : 'Ds',
        'daughters'       : ['pi','mup','mum']
        }
}

decay_descriptors_trueMC = {
    '21513012'   : '${D}[ D+ => ( ${tau}(tau+ => ${mup1}mu+ ${mup2}mu+ ${mum}mu-) ) ${Nu}nu_tau ]CC',
    '21513013'   : '${D}[ D+ => ( ${tau}(tau+ => ${mup1}mu+ ${mup2}mu+ ${mum}mu-) ) ${Nu}nu_tau ]CC',
    '23513015'   : '${D}[ D_s+ => ( ${tau}(tau+ => ${mup1}mu+ ${mup2}mu+ ${mum}mu-) ) ${Nu}nu_tau ]CC',
    '23513016'   : '${D}[ D_s+ => ( ${tau}(tau+ => ${mup1}mu+ ${mup2}mu+ ${mum}mu-) ) ${Nu}nu_tau ]CC',
    }
