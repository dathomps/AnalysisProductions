"""Utilities for creating DaVinci tuples"""
import os

from Configurables import DaVinci, DecayTreeTuple, TupleToolDecay, LoKi__Hybrid__TupleTool
from Configurables import TupleToolPid, TupleToolANNPID, TupleToolTISTOS, TupleToolTrigger, TriggerTisTos
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
from LoKiPhys.decorators import *
from Tau23mu.dictionaries import decayInfo

# ROOT_IN_TES
def RootInTes(IsMC):
    return os.path.join('/Event', 'MyStream' if IsMC else 'Leptonic')

# Define some trigger lines to check for TISTOS
def TriggerList(decay, data_type):
    commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
    hlt1Lines = {'Run1': ['TrackAllL0','L0Any','TrackMuon','SingleMuonHighPT','TrackPhoton'],
                 'Run2': ['TrackMVA','TrackMuon','TrackMuonMVA','SingleMuonHighPT'] }
    hlt2Lines = {'Run1': ['SingleMuon'+x for x in ['','HighPT','LowPT','VHighPT']],
                 'Run2': ['SingleMuon'+x for x in ['','HighPT','LowPT','Rare','VHighPT']] }
    runid = 'Run1' if data_type in ['2011','2012'] else 'Run2'
    commonTriggerList += ['Hlt1'+x+'Decision' for x in hlt1Lines[runid]]
    commonTriggerList += ['Hlt2'+x+'Decision' for x in hlt2Lines[runid]]

    hlt1Comb  = {'Run1': ['DiMuonHighMass','DiMuonLowMass'],
                 'Run2': ['DiMuonLowMass','TwoTrackMVA']}
    Hlt1Comb     = ['Hlt1'+x+'Decision' for x in hlt1Comb[runid]]

    topo      = {'Run1': [y+x+'Body'+z for x in ['2','3'] for y in [''] for z in ['BBDT','Simple']]+['Mu'+x+'BodyBBDT' for x in ['2','3']],
                 'Run2': [y+x+'Body' for x in ['2','3'] for y in ['','Mu','MuMu']] }
    TopoTriggers = ['Hlt2Topo'+x+'Decision' for x in topo[runid]]

    diMuon    = {'Run1': ['Detached', 'B', 'AndMuon'],
                 'Run2': ['Soft','Detached'] }
    DiMuon       = ['Hlt2DiMuon'+x+'Decision' for x in diMuon[runid]]

    triMuon   = {'Run1': ['Detached','Tau'],
                 'Run2': ['Detached','Tau23Mu'] }
    TriMuon      = ['Hlt2TriMuon'+x+'Decision' for x in triMuon[runid]]

    charm     = {'Run1': ['SemilepD2HMuMu'],
                 'Run2': []}
    Charm        = ['Hlt2Charm'+x+'Decision' for x in charm[runid]]
    TriggerLists = {'X'    : commonTriggerList+Hlt1Comb+TopoTriggers+DiMuon+TriMuon+Charm,
                    'P0'   : commonTriggerList,
                    'P1'   : commonTriggerList,
                    'P2'   : commonTriggerList}
    TLists = {
        'tau23mu' : {
            'tau' : TriggerLists['X'],
            'mup1': commonTriggerList,
            'mum' : commonTriggerList,
            'mup2': commonTriggerList
                },
        'd2mumupi': {
            'Ds'  : TriggerLists['X'],
            'mup' : commonTriggerList,
            'mum' : commonTriggerList,
            'pi'  : commonTriggerList
                },
            }
    return TLists[decay]

# ---  Start ReRunStripping  ---
def ReRunStrippings(StrippingVersion, Lines , mainSeqAlgs, TCK=None ):

    stripping='stripping'+StrippingVersion
    from CommonParticlesArchive import CommonParticlesArchiveConf
    CommonParticlesArchiveConf().redirect(stripping)

    # Setup RawEventJuggler if TCK is selected
    # Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
    # Compare __init__.py at
    # https://svnweb.cern.ch/trac/lhcb/browser/DBASE/trunk/RawEventFormat/python/RawEventFormat/
    # for finding
    # - Input = 2.0 <-> multiple-TCK DSTs
    # - Output = 4.0 <-> Stripping21
    if TCK!=None:
        from Configurables import RawEventJuggler
        # #tck = '0x40990042'
        # if '2012a' == '2012a':
        #     tck = '0x4097003d' # 12a
        # else:
        #     tck = '0x409F0045' # 12b
        RawEventJuggler().Input  = 3.0
        RawEventJuggler().Output = 4.0
        RawEventJuggler().DataOnDemand = True
        RawEventJuggler().TCK = TCK

    # Load configurations
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive

    # Build the stripping version
    config  = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive)

    # Now build the stream
    #from StrippingConf.StrippingStream import StrippingStream
    MyStream = StrippingStream("MyStream")

    # Append the requested line to the stream
    for stream in streams:
        for line in stream.lines:
            if line.name() in Lines:
                MyStream.appendLines( [ line ] )
                print('Appended line %s to MyStream' % line.name())

    # Configure Stripping
    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()
    sc = StrippingConf( Streams = [ MyStream ],
                        MaxCandidates = 2000)#,
                        #TESPrefix = 'Strip')#,
                        #AcceptBadEvents = False,
                        #BadEventSelection = filterBadEvents )

    # EventNodeKiller
    from Configurables import EventNodeKiller
    eventNodeKiller = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

    mainSeqAlgs += [ eventNodeKiller, sc.sequence() ]
    return
# ---  End ReRunStripping  ---

def SetupMCTools(dtt, IsTurbo):
    dtt.ToolList += [ "TupleToolMCTruth", "TupleToolMCBackgroundInfo" ]
    mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic'] #, "MCTupleToolHierarchy"]
    if IsTurbo:
        ## Set MC truth for Turbo
        from TeslaTools import TeslaTruthUtils
        relations = TeslaTruthUtils.getRelLocs() + [
        TeslaTruthUtils.getRelLoc(''),
        # Location of the truth tables for PersistReco objects
        'Relations/Hlt2/Protos/Charged'
        ]
        #relations = [TeslaTruthUtils.getRelLoc('')]
        #relations.append('/Event/Turbo/Relations/Hlt2/Protos/Charged')
        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)
    else:
        from Configurables import TupleToolMCTruth
        MCTruth = TupleToolMCTruth()
        MCTruth.ToolList =  mc_tools
        dtt.addTool(MCTruth)
    return

def CreateDTT(lname, IsMC=False):
    # Variables
    strip_line_name = decayInfo[lname]['strip_line_name']
    mother = decayInfo[lname]['mother']
    daus = decayInfo[lname]['daughters']
    decay_desc = decayInfo[lname]['decay_descriptor']
    IsTurbo = 'Turbo' in strip_line_name
    name = strip_line_name.replace('Line', '')
    Probnn = ["MC15TuneV1"] if DaVinci().DataType == '2015' else []
    root_in_tes = RootInTes(IsMC)
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(name))
    dtt.Inputs = ['Phys/{0}/Particles'.format(strip_line_name)]
    dtt.setDescriptorTemplate(decay_desc)
    # fix to avoid bug in setDescriptorTemplate in DV v44r7
    #print(dtt.Branches)
    dtt.Branches[mother] = dtt.Decay.replace('^','')
    if dtt.Decay[0] == '^': dtt.Decay = dtt.Decay[1:]
    # list of general tools
    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats","Angles"]
    dtt.ToolList = ['TupleTool'+tool for tool in GeneralTools]
    # Setup MC tools if neeeded
    if IsMC: SetupMCTools(dtt, IsTurbo)
    # DTF Refit info
    X_Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), mother) ]
    fit = X_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose= True
    fit.constrainToOriginVertex = False
    fit.UpdateDaughters = True
    TrackTools = ['Geometry','Kinematic','Pid','TrackInfo']
    for trk in daus:
        branch = dtt.allConfigurables['%s.%s' % ( dtt.name(), trk) ]
        # P info
        branch.InheritTools = False
        branch.ToolList = [ "TupleTool"+name for name in TrackTools ]
        branch.addTool(TupleToolPid)
        branch.TupleToolPid.Verbose = True
        if len(Probnn):
            from Configurables import TupleToolANNPID
            branch.ToolList += [ "TupleToolANNPID" ]
            branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
            branch.TupleToolANNPID.ANNPIDTunes = Probnn
        if IsMC:
            branch.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            branch.ToolList += [ "TupleToolMCTruth",'TupleToolL0Calo' ]
    # TISTOS
    for nodeName, tList in TriggerList(lname,DaVinci().DataType).iteritems():
        Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList
        if IsTurbo: Node.TupleToolTISTOS.FillHlt2 = False
    # LoKi Variables
    LoKiVarsX = LoKi__Hybrid__TupleTool('LoKi_Variables')
    lokivarsX = {
        "DOCA" :        "DOCAMAX",
        "vchi2":        "(VFASPF(VCHI2/VDOF))",
        "BPVIPCHI2" :   "BPVIPCHI2()",
        "BPVVD" :       "BPVVD",
        "BPVCORRM" :    "BPVCORRM",
        "VFASPFVCHI2" : "VFASPF(VCHI2)",
        "DOCA01" :      "DOCA(1,2)",
        "DOCA12" :      "DOCA(2,3)",
        "DOCA02" :      "DOCA(1,3)",
        "M01" :         "M12",
        "M12" :         "M23",
        "M02" :         "M13",
        "LOKI_ETA" :    "ETA",
#        "LOKI_ETA1":    "CHILD(ETA,1)", # Cannot define a 1-1 association to decaytreetuple order
#        "LOKI_ETA2":    "CHILD(ETA,2)",
#        "LOKI_ETA3":    "CHILD(ETA,3)",
        "LOKI_PHI" :    "PHI",
        "AMAXDOCA" :    "LoKi.Particles.PFunA( AMAXDOCA ('LoKi::TrgDistanceCalculator') ) " ,
        "AMINDOCA" :    "LoKi.Particles.PFunA( AMINDOCA ('LoKi::TrgDistanceCalculator') ) ",
        }#{"BPVCORRM" : "BPVCORRM"}
    LoKiVarsX.Variables = lokivarsX
    X_Node.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Variables"]
    X_Node.addTool(LoKiVarsX)

    # Related Info
    LoKiVars = LoKi__Hybrid__TupleTool('LoKi_Variables')
    lokivars = {}
    # --- cone variables
    conepath = 'Phys/'+strip_line_name+'/{0}ConeVarInfo{1}'
    if not IsMC: conepath = root_in_tes+'/'+conepath
    conemult = 'ConeMult{0}_{1}'
    coneptas = 'ConePtAsym{0}_{1}'
    pars = {'tau23mu'   : [ ['Muon','1'],['Muon','2'],['Muon','3']],
            'd2mumupi'  : [ ['Pion',''] ,['Muon','1'],['Muon','2']] }
    for par in pars[lname]:
        cpath = conepath.format(par[0],par[1])
        lokivars.update({
            conemult.format(par[0],par[1]): "RELINFO('%s','CONEMULT',-999)"%(cpath),
            coneptas.format(par[0],par[1]): "RELINFO('%s','CONEPTASYM',-999)"%(cpath)
        })
    # --- vtx isolation variables
    vtxisopath = 'Phys/'+strip_line_name+'/VtxIsoInfo'
    if not IsMC: vtxisopath = root_in_tes+'/'+vtxisopath
    for vtxisovar in ['VTXISONUMVTX','VTXISODCHI2ONETRACK','VTXISODCHI2MASSONETRACK','VTXISODCHI2TWOTRACK','VTXISODCHI2MASSTWOTRACK']:
        lokivars.update({vtxisovar: "RELINFO('%s','%s',-999)"%(vtxisopath,vtxisovar)})
    # --- track isolation variables
    trkisopath = 'Phys/'+strip_line_name+'/{0}TrackIsoBDT{1}'
    if not IsMC: trkisopath = root_in_tes+'/'+trkisopath
    trkisoname = 'TrkIsoBDT_{0}{1}_{2}'
    trkisovars = {'1':'TRKISOBDTFIRSTVALUE','2':'TRKISOBDTSECONDVALUE','3':'TRKISOBDTTHIRDVALUE'}
    pars = {'tau23mu'   : [ ['Muon','Info1'],['Muon','Info2'],['Muon','Info3']],
            'd2mumupi'  : [ ['Pion','Info'] ,['Muon','Info1'],['Muon','_mu_2']] }
    for par in pars[lname]:
        for idx in trkisovars.keys():
            lokivars.update({trkisoname.format(par[0],par[1],idx): "RELINFO('%s','%s',-999)"%(trkisopath.format(par[0],par[1]),trkisovars[idx])})
    # --- bs2mumu isolation variables
    dimuonisopath = 'Phys/'+strip_line_name+'/TrackIsolationBDT2'
    if not IsMC: dimuonisopath = root_in_tes+'/'+dimuonisopath
    dimuonisoname = 'TrkIsoBDT2_'
    dimuonisovars = ['TRKISOBDTLONG',
     'TRKISOBDTLONG_D1MAX1','TRKISOBDTLONG_D1MAX2','TRKISOBDTLONG_D1MAX3',
     'TRKISOBDTLONG_D2MAX1','TRKISOBDTLONG_D2MAX2','TRKISOBDTLONG_D2MAX3',
     'TRKISOBDTVELO',
     'TRKISOBDTVELO_D1MAX1','TRKISOBDTVELO_D1MAX2','TRKISOBDTVELO_D1MAX3',
     'TRKISOBDTVELO_D2MAX1','TRKISOBDTVELO_D2MAX2','TRKISOBDTVELO_D2MAX3',
     'BS2MUMUBDT']#,'BS2MUMUBDT_INPUT_MINIPS','BS2MUMUBDT_INPUT_IPS'] 'INPUT' vars are empty
    pars = {'tau23mu'   : ['_01','_02','_12'],
            'd2mumupi'  : [''] }
    for pars in pars[lname]:
        for var in dimuonisovars:
            lokivars.update({var+'_'+pars: "RELINFO('%s','%s',-999)"%(dimuonisopath+pars,var)})
    LoKiVars.Variables = lokivars
    dtt.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Variables"]
    dtt.addTool(LoKiVars)

    # Finishing
    return dtt

# Tuple for true MC (MCDecayTreeTuple)
def TrueMCTuple(name, decay):
    """Create an MCDecayTree with full MC informations"""
    mc_tpl = MCDecayTreeTuple("MCDecayTreeTuple")
    mc_tpl.ToolList = [
        "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
        "LoKi::Hybrid::MCTupleTool",    #to add more variables in the tuple
        "TupleToolEventInfo",
        "MCTupleToolAngles"         #generated decay angles
        ]

    #adding more MC variables
    kin_tool = mc_tpl.addTupleTool("LoKi::Hybrid::MCTupleTool", "Kinematic")

    #adding more MC variables
    kin_tool.Variables = {
        # be careful: for some weird reason, many entries of the tree might be filled
        # with unsense values of TRUE_P and TRUE_ETA:
        # don't rely (too much) on them!
        "TRUE_P"   : "MCP",
        "TRUE_ETA" : "MCETA"
        }

    mc_tpl.addTupleTool("MCTupleToolKinematic").Verbose = True

    # Output distributions for pions do not match unless labX == True
    mc_tpl.UseLabXSyntax = True
    mc_tpl.RevertToPositiveID = False
    mc_tpl.Decay = decay

    return mc_tpl

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

# ---  Start PrinTES  ---
def PrintTes():
    from Configurables import StoreExplorerAlg
    Ex = StoreExplorerAlg("PrintTES")
    Ex.Load = 10
    Ex.PrintFreq = 1.0
    return Ex
# ---  End PrinTES  ---
