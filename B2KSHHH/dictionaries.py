from Configurables import DaVinci
if int(DaVinci().DataType) < 2015:
    from B2KSHHH.triggers import CommonlyUsedTriggers as Triggers
else:
    from B2KSHHH.triggers import CommonlyUsedTriggersRun2 as Triggers

decay_descriptors = {
    'KSPimPipPip' : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}pi- ${P1}pi+ ${P2}pi+]CC',
    'KSKmPipPip'  : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K-  ${P1}pi+ ${P2}pi+]CC',
    'KSKpPimPip'  : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}pi- ${P2}pi+]CC',
    'KSKmKpPip'   : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K-  ${P1}K+  ${P2}pi+]CC',
    'KSKpKpPim'   : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}K+  ${P2}pi-]CC',
    'KSKmKpKp'    : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}K+  ${P2}K- ]CC',
}

lokitools = {
    'all': {},
    'B': {
    # Cone Multiplicity
    'ConeMult08_1' : {'path' : 'P2ConeVar08_1', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult08_2' : {'path' : 'P2ConeVar08_2', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult08_3' : {'path' : 'P2ConeVar08_3', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult10_1' : {'path' : 'P2ConeVar10_1', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult10_2' : {'path' : 'P2ConeVar10_2', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult10_3' : {'path' : 'P2ConeVar10_3', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult13_1' : {'path' : 'P2ConeVar13_1', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult13_2' : {'path' : 'P2ConeVar13_2', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult13_3' : {'path' : 'P2ConeVar13_3', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult17_1' : {'path' : 'P2ConeVar17_1', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult17_2' : {'path' : 'P2ConeVar17_2', 'func' : 'CONEMULT', 'default': -999},
    'ConeMult17_3' : {'path' : 'P2ConeVar17_3', 'func' : 'CONEMULT', 'default': -999},
    # Cone Pt Asymmetry
    'ConePtAsym08_1' : {'path' : 'P2ConeVar08_1', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym08_2' : {'path' : 'P2ConeVar08_2', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym08_3' : {'path' : 'P2ConeVar08_3', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym10_1' : {'path' : 'P2ConeVar10_1', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym10_2' : {'path' : 'P2ConeVar10_2', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym10_3' : {'path' : 'P2ConeVar10_3', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym13_1' : {'path' : 'P2ConeVar13_1', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym13_2' : {'path' : 'P2ConeVar13_2', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym13_3' : {'path' : 'P2ConeVar13_3', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym17_1' : {'path' : 'P2ConeVar17_1', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym17_2' : {'path' : 'P2ConeVar17_2', 'func' : 'CONEPTASYM', 'default': -999},
    'ConePtAsym17_3' : {'path' : 'P2ConeVar17_3', 'func' : 'CONEPTASYM', 'default': -999},
    # Vtx Isolation
    'VTXISONUMVTX' : {'path' : 'VertexIsoInfo', 'func' : 'VTXISONUMVTX', 'default': -999},
    'VTXISODCHI2ONETRACK' : {'path' : 'VertexIsoInfo', 'func' : 'VTXISODCHI2ONETRACK', 'default': -999},
    'VTXISODCHI2MASSONETRACK' : {'path' : 'VertexIsoInfo', 'func' : 'VTXISODCHI2MASSONETRACK', 'default': -999},
    'VTXISODCHI2TWOTRACK' : {'path' : 'VertexIsoInfo', 'func' : 'VTXISODCHI2TWOTRACK', 'default': -999},
    'VTXISODCHI2MASSTWOTRACK' : {'path' : 'VertexIsoInfo', 'func' : 'VTXISODCHI2MASSTWOTRACK', 'default': -999},
    }
}

CommonTupMaker = {
    'line': 'B2KShhh_PiPiPi_LLLine',
    'stream': 'Bhadron',
    'decay_desc': decay_descriptors['KSPimPipPip'],
    'head': 'B',
    'intermediates': ['KS'],
    'tracks': ['KS_P0','KS_P1','P0','P1','P2'],
    'neutrals': {},
    'massConstraints': {'KSMC' : ['KS0']},
    'tistos': {
        'B'     : Triggers['B'],
        'KS'    : Triggers['KS'],
        'KS_P0' : Triggers['Track'],
        'KS_P1' : Triggers['Track'],
        'P0'    : Triggers['Track'],
        'P1'    : Triggers['Track'],
        'P2'    : Triggers['Track']
        },
    'lokitools': lokitools,# getLoKiToolsDictionary(), # should be fixed
    'probnn': [],
    }

TupConf = {
    'B2KSPiPiPi_LL': {},# B2KSPiPiPi_LL
    'B2KSPiPiPi_DD': {
        'line': 'B2KShhh_PiPiPi_DDLine'
        },# B2KSPiPiPi_DD
    'B2KSKpPiPi_LL': {
        'line': 'B2KShhh_KPiPi_LLLine',
        'decay_desc': decay_descriptors['KSKpPimPip'],
        }, # B2KSKpPiPi_LL
    'B2KSKpPiPi_DD': {
        'line': 'B2KShhh_KPiPi_DDLine',
        'decay_desc': decay_descriptors['KSKpPimPip'],
        }, # B2KSKpPiPi_DD
    'B2KSKmPiPi_LL': {
        'line': 'B2KShhh_KPiPi_LLLine',
        'decay_desc': decay_descriptors['KSKmPipPip'],
        }, # B2KSKmPiPi_LL
    'B2KSKmPiPi_DD': {
        'line': 'B2KShhh_KPiPi_DDLine',
        'decay_desc': decay_descriptors['KSKmPipPip'],
        }, # B2KSKmPiPi_DD
    'B2KSKKPip_LL': {
        'line': 'B2KShhh_KKPi_LLLine',
        'decay_desc': decay_descriptors['KSKmKpPip'],
        }, # B2KSKKPip_LL
    'B2KSKKPip_DD': {
        'line': 'B2KShhh_KKPi_DDLine',
        'decay_desc': decay_descriptors['KSKmKpPip'],
        }, # B2KSKKPip_DD
    'B2KSKKPim_LL': {
        'line': 'B2KShhh_KKPi_LLLine',
        'decay_desc': decay_descriptors['KSKpKpPim'],
        }, # B2KSKKPim_LL
    'B2KSKKPim_DD': {
        'line': 'B2KShhh_KKPi_DDLine',
        'decay_desc': decay_descriptors['KSKpKpPim'],
        }, # B2KSKKPim_DD
    'B2KSKKK_LL': {
        'line': 'B2KShhh_KKK_LLLine',
        'decay_desc': decay_descriptors['KSKmKpKp'],
        }, # B2KSKKK_LL
    'B2KSKKK_DD': {
        'line': 'B2KShhh_KKK_DDLine',
        'decay_desc': decay_descriptors['KSKmKpKp'],
        }, # B2KSKKK_DD
    } # TupConf
