### $`B^+ \to K_s^0 h^+h^-h^+`$ Tuples

# Analysis Production Version: 0 - MC Samples
This is the first time this AP has been run, formerly it was defined in CharmWGProd. Since that, AP has been implemented and the code has been reshaped to gain in modularity. The latest production is requested to create tuples for the study of specific MC samples created through a filtered production.

## Analysis Summary
These tuples were primarily made to study $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^\pm \pi^\mp K^+`$ decays. Nevertheless all the $`B^+ \to (K_s^0 \to \pi^+ \pi^-) h^+ h^- h^+`$ final states have been reconstructed.

## Constructing the analysis production
This production includes a script to create the relevant `info.yaml` to driver the production when needed.
By default, calling `make_info_file.py` in the `B2KSHHH` directory
```
AnalysisProduction/B2KSHHH $ python make_info_file.py
```
will prepare the production of data tuples for all years and polarities (see details below).
For preparing the production of other tuples, such as filtered 2011 and 2012 MC samples with event ids `12105160` and `12105161`, run for example
```
AnalysisProduction/B2KSHHH $ python make_info_file.py --evtIds 12105160 12105161 --years 2011 2012
```

## Options Files
The heart of the tuple production is the file `B2KSHHH_TupleMaker.py`, which setups the `DaVinci` application and adds to the `UserAlgorithms` option the `MomentumCorrection` (scaling for data, smearing for the MC) and all the tuples as defined by `DecayTreeTupleCreator`.
These algorithms are defined in the `utils.py` module and read the configuration defined in `dictionaries.py` to setup the `DecayTreeTuple` object.
Another important file is the `triggers.py` module, in which a list of commonly used trigger lines for TISTOS in Run1 and Run2 are listed.
For completing the preparation of the `DaVinci` job, some generic options are defined in the directory `options` to set the correct `DataType` and `Simulation` flags and create the environment variable `FILTEREDPRODUCTION` when dealing with them (it requires some specific configuration).

## Data - CharmWGProd v1r25 and v1r101
All data comes from the BHadron stream full dataset. The tuples were produced with `CharmWGProd`. An additional production with `CharmWGProd v1r101` was needed to fix a bug in 2017 and 2018 data production. The 2011 and 2012 tuple were produced locally when the original analysis started.

|  Year  |  Stripping  |
|---|---|
| 2015 | 24r1 |
| 2016 | 28r1 |
| 2017 | 29r2 |
| 2018 | 34 |

## MC
MC Samples are being added to this production as and when they become available in groups.
In general the MC should use the same stripping as the corresponding data however there are some cases in which the existing MC did not share the stripping but we have used anyway.

|  Year  |  Stripping  |
|---|---|
| 2011 | 21r1p1 |
| 2012 | 21r0p1 |
| 2015 | 24r2 |
| 2016 | 28r2 |
| 2017 | 29r2 |
| 2018 | 34 |

# Latest Production - v0r?

This production creates tuples for the stripping filtered production of the following event ids:

|  EventIds  |  Description  |
|---|---|
| 12105160 | $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+ K^+`$ Phase space |
| 12105161 | $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+ K^+`$ Phase space with filters at $`m(K_s^0 K^- \pi^+) < 2.5 GeV`$ |
| 12135102 | $`B^+ \to (\eta_c \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |

and for 2015, 2016 samples.
The production is prepared with
```
AnalysisProduction/B2KSHHH $ python make_info_file.py --evtIds 12105160 12105161 12135102 --years 2015 2016 --filtered
```

# Latest Production - v0r0p3040982

This production creates tuples for the stripping filtered production of the following event ids:

|  EventIds  |  Description  |
|---|---|
| 12105160 | $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+ K^+`$ Phase space |
| 12105161 | $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+ K^+`$ Phase space with filters at $`m(K_s^0 K^- \pi^+) < 2.5 GeV`$ |
| 12135102 | $`B^+ \to (\eta_c \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |

and for 2011, 2012, 2017, 2018 samples.
The production is prepared with
```
AnalysisProduction/B2KSHHH $ python make_info_file.py --evtIds 12105160 12105161 12135102 --years 2011 2012 2017 2018 --filtered
```

# CharmWGProd v1r81

This production created tuples for the unfiltered production of the following event ids:

|  EventIds  |  Description  |
|---|---|
| 12105160 | $`B^+ \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+ K^+`$ Phase space |
| 12135100 | $`B^+ \to (J/\psi \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |
| 12135102 | $`B^+ \to (\eta_c \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |
| 12135104 | $`B^+ \to (\eta_c(2S) \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |
| 12135106 | $`B^+ \to (\chi_{c1} \to (K_s^0 \to \pi^+ \pi^-) K^- \pi^+) K^+`$ Phase space |

and for 2015, 2016, 2017, 2018 samples.
The production could be prepared with
```
AnalysisProduction/B2KSHHH $ python make_info_file.py --evtIds 12105160 12135100 12135102 12135104 12135106 --years 2015 2016 2017 2018
```
