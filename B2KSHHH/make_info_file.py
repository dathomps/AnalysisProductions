#!/usr/bin/env python
import argparse
import json
from os.path import dirname, join

tcks = {
    '2011': '0x40760037',
    '2012': '0x409f0045',
    '2015': '0x411400a2',
    '2016': '0x6139160F',
    '2017': '0x62661709',
    '2018': '0x617d18a4',
}
strip_data = {
    #'2011': 'Stripping20r1',
    #'2012': ''
    '2015': 'Stripping24r1',
    '2016': 'Stripping28r1',
    '2017': 'Stripping29r2',
    '2018': 'Stripping34',
}
strip_mc = {
    '2011': 'Stripping20r1NoPrescalingFlagged',
    '2012': 'MultipleTCK-2012-01',
    '2015': 'Turbo02/Stripping24r1NoPrescalingFlagged',
    '2016': 'Turbo03a/Stripping28r1NoPrescalingFlagged',
    '2017': 'Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged',
    '2018': 'Turbo05-WithTurcal/Stripping34NoPrescalingFlagged',
}
strip_mc_filtered = {
    '2011': 'Stripping21r1p1Filtered',
    '2012': 'Stripping21r0p1Filtered',
    '2015': 'Turbo02/Stripping24r2Filtered',
    '2016': 'Turbo03a/Stripping28r2Filtered',
    '2017': 'Turbo04a-WithTurcal/Stripping29r2Filtered',
    '2018': 'Turbo05-WithTurcal/Stripping34Filtered',
}
Beam = {'2011': '3500', '2012': '4000', '2015': '6500', '2016': '6500', '2017': '6500', '2018': '6500'}
Nu =   {'2011': '2', '2012': '2.5', '2015': '1.6-25ns', '2016': '1.6-25ns', '2017': '1.6-25ns', '2018': '1.6-25ns'}
Reco = {'2011': '14c', '2012': '14c', '2015': '15a', '2016': '16', '2017': '17', '2018': '18'}
Sim = {'unfiltered': '09h', 'filtered': '09k'}
Stream = {'unfiltered': 'ALLSTREAMS.MDST', 'filtered': 'B2KSHHH.STRIP.DST'}
StripMC= {'unfiltered': strip_mc, 'filtered': strip_mc_filtered}
mcsamples = [12105160,12105161,12135100,12135102,12135104,12135106]
bk_mc_path = '/MC/20{0}/Beam{1}GeV-20{0}-{2}-Nu{3}-Pythia8/Sim{4}/Trig{5}/Reco{6}/{7}/{8}/{9}'#.format(date,beam[date],mag,nu,sim,tck,reco,trigstrip,evtid,stream)
bk_data_path = '/LHCb/Collision{0}/Beam{1}GeV-VeloClosed-{2}/Real Data/Reco{3}/{4}/90000000/BHADRON.MDST'#.format(year%100,beam,mag,reco,strip)

all_years = [2011,2012]+list(range(2015,2019))
all_polarities = ['MagDown', 'MagUp']
dv_version = 'v45r8'
wg = 'BnoC'
author= 'maurizio.martinelli@cern.ch'

parser = argparse.ArgumentParser()
parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
parser.add_argument('--evtIds', type=int, nargs='+', choices=mcsamples, default=[])
parser.add_argument('--filtered', action='store_true', help='run on the filtered mc production')
parser.add_argument('--force-full-lfn', action='store_false', help='Force the merge request testing to use a full LFN')
args = parser.parse_args()

def main():

    # write yaml file - defaults
    yaml_str =  'defaults:\n'
    yaml_str+= f'  application: DaVinci/{dv_version}\n'
    yaml_str+=  '  output: BHADRON_B2KSHHH_DVNTUPLE.ROOT\n'
    yaml_str+= f'  wg: {wg}\n'
    yaml_str+=  '  inform:\n'
    yaml_str+= f'    - {author}\n'
    yaml_str+=  '  turbo: no\n'
    yaml_str+=  '  automatically_configure: yes\n\n'
    # specific options
    if len(args.evtIds): # mc
        mctype = 'filtered' if args.filtered else 'unfiltered'
        simv = Sim[mctype]
        stream = Stream[mctype]
        stripmc = StripMC[mctype]
        datasets = [ (y%100,Beam[str(y)],Nu[str(y)],tcks[str(y)],Reco[str(y)],stripmc[str(y)]) for y in args.years ]
        yaml_str+= f'{{%- set datasets = {datasets} %}}\n\n'
        yaml_str+= f'{{%- for year, beam, nu, tck, reco, strip in datasets %}}\n'
        yaml_str+= f'  {{%- for polarity in {args.polarities} %}}\n'
        for evtId in args.evtIds:
            yaml_str+= f'20{{{{year}}}}_{{{{polarity}}}}_{evtId}:\n'
            yaml_str+=  '  input:\n'
            bkpath = bk_mc_path.format('{{year}}','{{beam}}','{{polarity}}','{{nu}}',simv, '{{tck}}','{{reco}}','{{strip}}',evtId,stream)
            yaml_str+= f'    bk_query: {bkpath}\n'
            yaml_str+=  '  options:\n'
            if args.filtered: yaml_str+=  '    - FilteredProd.py\n'
            yaml_str+=  '    - B2KSHHH_TupleMaker.py\n'
    else: # data
        datasets = [ (y%100,Beam[str(y)],Reco[str(y)],strip_data[str(y)]) for y in args.years ]
        yaml_str+= f'{{%- set datasets = {datasets} %}}\n'
        yaml_str+= f'{{%- for year, beam, reco, strip in datasets %}}\n'
        yaml_str+= f'  {{%- for polarity in {args.polarities} %}}\n'
        yaml_str+=  '20{{year}}_{{polarity}}:\n'
        yaml_str+=  '  input:\n'
        bkpath = bk_data_path.format('{{year}}','{{beam}}','{{polarity}}','{{reco}}','{{strip}}')
        yaml_str+= f'    bk_query: {bkpath}\n'
        yaml_str+=  '  options:\n'
        yaml_str+=  '    - B2KSHHH_TupleMaker.py\n\n'
    # close loops
    yaml_str+= '  {%- endfor %}\n'
    yaml_str+= '{%- endfor %}'
    # save to file
    f = open('info.yaml','w')
    f.write(yaml_str)
    f.close()


    return

if __name__ == '__main__':
    main()
