####################################################
# DVUTILS
#
#   DVUtils - a collection of useful classes and
#    functions to easily produce tuples in DaVinci
#
#    - DecayTreeTupleCreator
#    - DVFilter
#    - MomentumCorrection
#    - CheckStreams
#    - CheckInputType
#    - PrintDecay
#    - PrintTes
#
####################################################
from copy import deepcopy
from Configurables import (DecayTreeTuple, TupleToolMCTruth, TupleToolTISTOS,
    TupleToolANNPID, LoKi__Hybrid__TupleTool, TupleToolPrimaries,
    PrintDecayTree, PrintDecayTreeTool, StoreExplorerAlg,
    GaudiSequencer, ChargedProtoANNPIDConf, RawEventJuggler,
    ProcStatusCheck, EventNodeKiller)
from Configurables import TrackScaleState as SCALE
from Configurables import TrackSmearState as SMEAR
from DecayTreeTuple import Configuration # to load setDescriptorTemplate
from TeslaTools import TeslaTruthUtils
from PhysConf.Filters import LoKi_Filters
# for restripping
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from CommonParticlesArchive import CommonParticlesArchiveConf

# --- Begin DecayTreeTupleCreator ---
class DecayTreeTupleCreator:
    """ This class generates the structure of a common TTDTF tuple
        starting from a dictionary with the following structure:
          {'line': <str>,
           'decay': <str>,
           'branches': { <name> : <decayfinder>,
                         <name> : <decayfinder> },
           'head': <str>,
           'intermediates': [ <list of string with intermediate branch names> ],
           'tracks': [ <list of string with track branch names> ],
           'neutrals': [ <list of string with intermediate branch names> ],
           'massConstraints': { <name>: [  <list of string with pdg names> ] },
           'tistos': { <name>: [ <list of string with trigger lines> ]}}
           'lokitools' : { <dictionary with LoKiTools> }
           'probnn' : [ <list of probnn tunes> ]
           'pvconstraint' : True of False
           }
        Alternatively, the `decay` and `branches` keys can be substituted by a decay descriptor:
           {
           'decay_desc' : <str>
           }
    """
    def __init__(self, name='TupleDir', opts={},
                 inputType='DST', commonTools = [],
                 MCStream = 'AllStreams'):
        """ Initialise class """
        self.Name = name
        self.Opts = opts
        self.InputType = inputType
        self.PVconstraint = True if not 'pvconstraint' in opts.keys() else opts['pvconstraint']
        self.MCStream = MCStream
        self.HeadTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
        self.TrackTools = ['Geometry','Kinematic','Pid','TrackInfo']
        self.PizTools = ['Kinematic','Pi0Info']
        self.PhotonTools = ['Kinematic','PhotonInfo']
        self.IntermediateTools = [ 'Geometry', 'Kinematic', 'Propertime']

    def setName(self, name):
        """ set name """
        self.Name = name

    def setOptions(self, opts):
        """ set options """
        self.Opts = opts

    def getInput(self):
        """ get the particles location """
        # Particles Location
        if 'userfunc' in self.Opts.keys():
            return 'Phys/'+self.Opts['line']+'/Particles'
        elif self.MCStream != None:
            if self.Opts['stream'] == 'Turbo':
                return self.Opts['line']+'/Particles'
            return '/Event/'+self.MCStream+'/Phys/'+self.Opts['line']+'/Particles'
        elif self.InputType == 'DST': return '/Event/'+self.Opts['stream']+'/Phys/'+self.Opts['line']+'/Particles'
        elif self.InputType == 'MDST':
            if self.Opts['stream'] == 'Turbo':
                return '/Event/'+self.Opts['stream']+'/'+self.Opts['line']+'/Particles'
            else: return 'Phys/'+self.Opts['line']+'/Particles'
        # elif self.InputType == 'DST': return '/Event/'+self.Opts['stream']+'/Phys/'+self.Opts['line']+'/Particles'
        # elif self.InputType == 'MDST':
        #     if self.Opts['stream'] == 'Turbo':
        #         return '/Event/'+self.Opts['stream']+'/'+self.Opts['line']+'/Particles'
        #     else: return 'Phys/'+self.Opts['line']+'/Particles'
        # elif self.InputType == 'MC':
        #     if self.Opts['stream'] == 'Turbo':
        #         return self.Opts['line']+'/Particles'
        #     return '/Event/'+self.MCStream+'/Phys/'+self.Opts['line']+'/Particles' if self.MCStream != '' else 'Phys/'+self.Opts['line']+'/Particles'
        #     #return 'Phys/'+self.Opts['line']+'/Particles'
        else: 'The specified input type=',self.InputType,'is not valid!'
        return None

    def getPathLoKiTool(self, Location):
        """ get the path for isolation variables """
        if self.MCStream != None: #MC
            # path = '/'+self.MCStream+'/' if self.MCStream != '' else ''
            # return path+'Phys/'+self.Opts['line']+'/'+Location ## NEED TO CHECK WHY THESE LINES WHERE THERE
            return '/Event/'+self.MCStream+'/Phys/'+self.Opts['line']+'/'+Location
        else: #data
            return '/Event/'+self.Opts['stream']+'/Phys/'+self.Opts['line']+'/'+Location
        return None

    def initialiseTool(self):
        """ initialise tool """
        self.tuptool = DecayTreeTuple(self.Name)
        print(self.getInput())
        self.tuptool.Inputs = [self.getInput()]
        # Setup decay structure
        if 'decay_desc' in self.Opts.keys():
            self.tuptool.setDescriptorTemplate(self.Opts['decay_desc'])
        else:
            self.tuptool.Decay = self.Opts['decay']
            # Setup branches
            self.tuptool.Branches = self.Opts['branches']
            #for br in self.Opts['branches'].keys(): self.tuptool.addTupleTool('TupleToolDecay/'+br)
        branches = list(self.tuptool.Branches.keys())
        branches.pop(branches.index(self.Opts['head']))
        for br in branches: self.tuptool.addTupleTool('TupleToolDecay/'+br)
        # setup tool list
        self.tuptool.ToolList = []
        # Set PV location for Turbo
        if self.Opts['stream'] == 'Turbo':
            self.tuptool.WriteP2PVRelations = False
            self.tuptool.InputPrimaryVertices = "/Event/Turbo/Primary"
        if self.MCStream != None: self.defineCommonMCTools()
        return

    def defineCommonMCTools(self):
        """ setup common MC truth tools """
        # Configure MCtruth
        mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic'] #, "MCTupleToolHierarchy"]
        if self.Opts['stream'] == 'Turbo':
            ## Set MC truth for Turbo
            relations = [TeslaTruthUtils.getRelLoc('')]
            relations.append('/Event/Turbo/Relations/Hlt2/Protos/Charged')
            TeslaTruthUtils.makeTruth(self.tuptool, relations, mc_tools)
        else:
            MCTruth = TupleToolMCTruth()
            MCTruth.ToolList =  mc_tools
            self.tuptool.addTool(MCTruth)
        self.tuptool.ToolList += [ "TupleToolMCTruth", "TupleToolMCBackgroundInfo" ]
        return

    def getNode(self, name):
        return self.tuptool.allConfigurables['%s.%s' % ( self.tuptool.name(), name) ]

    def getTriggerList(self, triggerDict):
        """
        create trigger list starting from a dictionary like:
           dd = {'L0':[], 'Hlt1': [], 'Hlt2': []}
        """
        triggerList = []
        for level, lines in triggerDict.items():
            for line in lines:
                triggerList  += [ level+line+('Decision' if not (level=='L0' and line=='Global') else '') ]
        return triggerList

    def TISTOS(self,triggerList, nodeName='Head', kTurbo=False):
        """ setup TISTOS tool """
        # Get trigger list
        tList = self.getTriggerList(triggerList)
        # TISTOS
        Node = self.getNode(nodeName)
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList
        if kTurbo: Node.TupleToolTISTOS.FillHlt2 = False
        return

    def MassConstrainedFits(self, updateDaughters=True):
        """ setup mass constrained fits """
        Node = self.getNode(self.Opts['head'])
        for mcName, mcs in self.Opts['massConstraints'].items():
            fit = Node.addTupleTool('TupleToolDecayTreeFitter/'+mcName+'Fit')
            fit.Verbose= True
            fit.constrainToOriginVertex = self.PVconstraint
            fit.daughtersToConstrain += mcs
            fit.UpdateDaughters = updateDaughters
        return

    def ReFit(self, updateDaughters=True):
        """ setup a refit of the decay tree """
        Node = self.getNode(self.Opts['head'])
        fit = Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
        fit.Verbose= True
        fit.constrainToOriginVertex = self.PVconstraint
        fit.UpdateDaughters = updateDaughters
        return

    def Intermediates(self):
        """ store information for intermediate resonances """
        for vtx in self.Opts['intermediates']:
            Vtx = self.tuptool.allConfigurables['%s.%s' % ( self.tuptool.name(), vtx) ]
            Vtx.InheritTools = False
            Vtx.ToolList = [  "TupleTool"+name for name in self.IntermediateTools ]
            if self.MCStream != None:
                Vtx.addTool( self.tuptool.allConfigurables['ToolSvc.TupleToolMCTruth'] )
                Vtx.ToolList += [ "TupleToolMCTruth" ]
        return

    def Tracks(self):
        """ define which informations to store for tracks """
        for trk in self.Opts['tracks']:
            branch = self.tuptool.allConfigurables['%s.%s' % ( self.tuptool.name(), trk) ]
            # P info
            branch.InheritTools = False
            branch.ToolList = [ "TupleTool"+name for name in self.TrackTools ]
            if len(self.Opts['probnn']):
                branch.ToolList += [ "TupleToolANNPID" ]
                branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
                branch.TupleToolANNPID.ANNPIDTunes = self.Opts['probnn']
            if self.MCStream != None:
                branch.addTool(self.tuptool.allConfigurables['ToolSvc.TupleToolMCTruth'])
                branch.ToolList += [ "TupleToolMCTruth",'TupleToolL0Calo' ]
        return

    def Neutrals(self):
        """ define which informations to store for neutrals """
        for ptype, pars in self.Opts['neutrals'].items():
            for par in pars:
                branch = self.tuptool.allConfigurables['%s.%s' % ( self.tuptool.name(), par) ]
                # P info
                branch.InheritTools = False
                branch.ToolList = [ "TupleTool"+name for name in (self.PizTools if ptype=='pi0' else self.PhotonTools) ]
                if self.MCStream != None:
                    branch.addTool(self.tuptool.allConfigurables['ToolSvc.TupleToolMCTruth'])
                    branch.ToolList += [ "TupleToolMCTruth" ]
        return

    def LoKi(self):
        if self.Opts['lokitools'] == None: return
        # Define LoKi Tools
        loki_path = self.getPathLoKiTool('')
        for node, lokitools in self.Opts['lokitools'].items():
            if not lokitools: continue
            LoKi_Tool = LoKi__Hybrid__TupleTool('LoKi_Variables_'+node)
            for name, args in lokitools.items():
                LoKi_Tool.Variables[name] = "RELINFO('{}','{}',{})".format(loki_path+args['path'],args['func'],args['default'])
            if node == 'all':
                self.tuptool.addTool(LoKi_Tool)
                self.tuptool.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Variables_"+node]
            else:
                Node = self.getNode(node)
                Node.addTool(LoKi_Tool)
                Node.ToolList  += ["LoKi::Hybrid::TupleTool/LoKi_Variables_"+node]
        return

    def defineTool(self):
        """ define the tool """
        # Initialise tool
        self.initialiseTool()
        # Add common tools
        self.tuptool.ToolList += ['TupleTool'+tool for tool in self.HeadTools]
        if self.Opts['stream']=='Turbo':
            self.tuptool.addTool(TupleToolPrimaries, name="TupleToolPrimaries" )
            self.tuptool.TupleToolPrimaries.InputLocation = "/Event/Turbo/Primary"
        # Fits
        self.ReFit()
        self.MassConstrainedFits()
        # Store info
        self.Intermediates()
        self.Tracks()
        self.Neutrals()
        # TISTOS
        for branch, triggers in self.Opts['tistos'].items(): self.TISTOS(triggers, branch, self.Opts['stream'] == 'Turbo')
        # LoKi
        self.LoKi()
        return

    def getTool(self):
        """ return the dtf tool """
        # Define tool in case something has been changed
        self.defineTool()
        return self.tuptool
# ---  End DecayTreeTupleCreator  ---

# --- Begin DVFilter ---
class DVFilter:
    """
        Run a filter on HLT code, Sripping stream and Stripping line
            - HLT1 = [<list of HLT1 trigger lines>]
            - HLT2 = [<list of HLT1 trigger lines>]
            - Lines = [<list of stripping lines>]
            - Streams = [<list of stripping streams>]
    """
    def __init__(self, hlt1 = [''], hlt2 = [''], lines = [''], streams = ['']):
        """ Initialise """
        self.HLT1 = hlt1
        self.HLT2 = hlt2
        self.Lines = lines
        self.Streams = streams
        self.Filter = None

    def getFilters(self):
        """ Return the list of filters """
        if self.Filter == None: self.defineFilter()
        return self.Filter.filters('Filters')

    def getFilterString(self, line):
        """ Return the string used for filtering """
        return " ( HLT_PASS_RE ('.*"+line+".*Decision') ) "

    def getVoidFilterString(self, name):
        """ Return the string used for filtering """
        return " ( EXISTS ('"+name+"') ) "

    def defineFilter(self):
        """ Define filter """
        hlt1Code, hlt2Code, stripCode, voidCode = self.getFilterString('None'), self.getFilterString('None'), self.getFilterString('None'), self.getVoidFilterString('None')
        for hlt in self.HLT1:
            hlt1Code += ' | ' + self.getFilterString(hlt)
        for hlt in self.HLT2:
            hlt2Code += ' | ' + self.getFilterString(hlt)
        for line in self.Lines:
            stripCode += ' | ' + self.getFilterString(line)
        for stream in self.Streams:
            voidCode += ' | ' + self.getVoidFilterString(stream)
        if self.HLT1 == ['']: hlt1Code = ""
        if self.HLT2 == ['']: hlt2Code = ""
        self.Filter = LoKi_Filters( HLT1_Code = hlt1Code, HLT2_Code = hlt2Code,
                                    STRIP_code = stripCode, VOID_Code = voidCode)
        return
# ---  End DVFilters  ---

# --- Begin ProbNNFromTurbo ---
def ProbNNFromTurbo(hlt2_line):
    # Use ProbNN as computed in HLT2
    # https://twiki.cern.ch/twiki/bin/view/LHCb/EnsureProbNNsCalculated
    probnn_seq = GaudiSequencer(hlt2_line + 'ProbNNSeq')
    annpid = ChargedProtoANNPIDConf(
        hlt2_line + 'ProbNNalg',
        RecoSequencer=probnn_seq,
        ProtoParticlesLocation='/Event/Turbo/{0}/Protos'.format(hlt2_line)
        )
    return probnn_seq
# ---  End ProbNNFromTurbo  ---

# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

# --- Begin CheckStreams ---
def CheckStreams(tMaker, IsMC):
    """
        Checks that the all the tuples request data from the same stream
    """
    streams= []
    for name, args in tMaker.items():
        streams += [args['stream']]
    if len(set(streams)) != 1:
        sys.exit('Check your TupMaker objects! Multiple streams requested')
    if IsMC: return 'AllStreams'
    return streams[0]
# ---  End CheckStreams  ---

# --- Begin CheckInputType ---
def CheckInputType(tMaker, InputType, IsMC):
    """
        Checks that the inputtype corresponding to the requested stream is properly set
    """
    stream= tMaker.itervalues().next()['stream']
    if not IsMC and (stream in ['Bhadron','Charm','Leptonic','PID'] and InputType == 'DST'):
        print('Wrong input type chosen for '+stream+', reverting to MDST')
        InputType = 'MDST'
    if not IsMC and (stream in [  'BhadronCompleteEvent','Calibration','CharmCompleteEvent','CharmControl','CharmToBeSwum',
                            'Dimuon','EW','MiniBias','Radiative','Semileptonic'] and InputType == 'MDST'):
        print('Wrong input type chosen for '+stream+', reverting to DST')
        InputType = 'DST'
    # if IsMC and InputType == 'MDST':
    #     print('Wrong input type chosen for MC, reverting to DST')
    #     InputType = 'DST'
    return InputType
# ---  End CheckInputType  ---

# ---  Begin PrintDecay  ---
def PrintDecay(inputs):
    tree = PrintDecayTree("PrintFoundParticle")
    tree.Inputs = inputs
    tree.addTool( PrintDecayTreeTool, name = "PrintDecay" )
    tree.PrintDecay.Information = "Name M P Px Py Pz Pt chi2"
    return tree
# ---  End PrintDecay  ---

# ---  Start PrinTES  ---
def PrintTes():
    Ex = StoreExplorerAlg("PrintTES")
    Ex.Load = 10
    Ex.PrintFreq = 1.0
    return Ex
# ---  End PrinTES  ---

# ---  Start ReRunStripping  ---
def ReRunStrippings(StrippingVersion, Lines , mainSeqAlgs, TCK=None ):

    stripping='stripping'+StrippingVersion
    CommonParticlesArchiveConf().redirect(stripping)

    # Setup RawEventJuggler if TCK is selected
    # Raw event juggler to split Other/RawEvent into Velo/RawEvent and Tracker/RawEvent
    # Compare __init__.py at
    # https://svnweb.cern.ch/trac/lhcb/browser/DBASE/trunk/RawEventFormat/python/RawEventFormat/
    # for finding
    # - Input = 2.0 <-> multiple-TCK DSTs
    # - Output = 4.0 <-> Stripping21
    if TCK!=None:
        # #tck = '0x40990042'
        # if '2012a' == '2012a':
        #     tck = '0x4097003d' # 12a
        # else:
        #     tck = '0x409F0045' # 12b
        RawEventJuggler().Input  = 3.0
        RawEventJuggler().Output = 4.0
        RawEventJuggler().DataOnDemand = True
        RawEventJuggler().TCK = TCK

    # Build the stripping version
    config  = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive)

    # Now build the stream
    #from StrippingConf.StrippingStream import StrippingStream
    MyStream = StrippingStream("MyStream")

    # Append the requested line to the stream
    for stream in streams:
        for line in stream.lines:
            if line.name() in Lines:
                MyStream.appendLines( [ line ] )
    '''
    # Append the Xb2phhh line
    from StrippingSelections import StrippingXb2p2hOrXb2p3h as Xb2p3h
    # add lines with default cuts
    Xb2p3hConfigurationDefault = Xb2p3h.default_config['CONFIG']
    Xb2p3hDefault = Xb2p3h.Xb2p2hOrXb2p3hConf("StrippingXb2p3hDefault",Xb2p3hConfigurationDefault)
    stream.appendLines( l for l in Xb2p3hDefault.lines() )
    '''

    # Configure Stripping
    filterBadEvents = ProcStatusCheck()
    sc = StrippingConf( Streams = [ MyStream ],
                        MaxCandidates = 1000,
                        AcceptBadEvents = False,
                        BadEventSelection = filterBadEvents )

    # EventNodeKiller
    eventNodeKiller = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

    mainSeqAlgs += [ eventNodeKiller, sc.sequence() ]
    return
# ---  End ReRunStripping  ---

# ---  Begin DecodeStrippingVersion  ---
def DecodeStrippingVersion(sv):
    '''
    returns a dictionary with stripping version, revision and patch
    '''
    vv = sv[:sv.find('p')][:(sv.find('r') if 'r' in sv else len(sv))]
    rr = sv[:(sv.find('p') if 'p' in sv else len(sv))][(sv.find('r')+1):] if 'r' in sv else None
    pp = sv[(sv.find('p')+1):] if 'p' in sv else None
    return {'version':int(vv), 'revision':int(rr), 'patch':int(pp)}
# ---  End DecodeStrippingVersion  ---

# ---  Begin CreateTupleMaker  ---
def CreateTupleMaker(tuple_conf,common_tup_maker):
    '''
    This function copies the dictionaries as defined in the dictionaries module and returns an object to feed DecayTreeTupleCreator
    '''
    TupMaker = {}
    for tmname, tmargs in tuple_conf.items():
        TupMaker[tmname] = deepcopy(common_tup_maker)
        for aname, aval in tmargs.items():
            TupMaker[tmname][aname] = aval
    return TupMaker
# ---  End CreateTupleMaker  ---

# ---  Begin RootInTes  ---
def RootInTes(stream,mc=False,filtered=False):
    '''
    A function to return the correct RootInTES for DaVinci
    '''
    if filtered: return ''
    stream_tes = stream if not mc else 'AllStreams'
    root_in_tes = '/Event/{}/'.format(stream_tes)
    if mc: root_in_tes = root_in_tes[:-1]
    return root_in_tes
# ---  End RootInTes  ---
