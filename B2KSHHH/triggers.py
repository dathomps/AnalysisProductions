'''
In this module we define some lists and dictionaries to fill the TISTOS tool with commonly used triggers.
'''

L0Base = ['Hadron','Muon','DiMuon','Electron','Photon']
Hlt1Base = ['TrackAllL0', 'L0Any']
Hlt1BaseRun2 = ['TrackMVA','TwoTrackMVA']
Hlt2Topo = ['Topo2BodyBBDT', 'Topo3BodyBBDT', 'Topo4BodyBBDT']
Hlt2TopoMu = ['TopoMu2BodyBBDT','TopoMu3BodyBBDT','TopoMu4BodyBBDT']
Hlt2TopoRun2 = ['Topo2Body', 'Topo3Body', 'Topo4Body']
Hlt2TopoMuRun2 = ['TopoMu2Body', 'TopoMu3Body', 'TopoMu4Body']

CommonlyUsedTriggersTurbo = {
    'L0'  : L0Base,
    'Hlt1': Hlt1BaseRun2,
    'Hlt2': []
}

CommonlyUsedTriggers = {
    'B': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base+['TrackAllMuon'],
        'Hlt2': Hlt2Topo+Hlt2TopoMu+['SingleMuon','SingleMuonHighPT']
        },
    'D0': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': Hlt2Topo
        },
    'D+': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': Hlt2Topo
        },
    'Ds+': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': Hlt2Topo
        },
    'D*': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': Hlt2Topo
        },
    'Lc': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': Hlt2Topo
        },
    'L': {
        'L0':   L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': ['Topo2BodyBBDT']
        },
    'Mu': {
        'L0':   L0Base,
        'Hlt1': ['TrackAllL0','TrackAllMuon','SingleMuonHighPTMass','L0Any'],
        'Hlt2': ['SingleMuon','SingleMuonHighPT']
        },
    'Phi': {
        'L0':   [],
        'Hlt1': [],
        'Hlt2': ['IncPhi']
        },
    'Jpsi': {
        'L0':   ['Hadron','Muon','DiMuon','Global'],
        'Hlt1': ['TrackAllL0','TrackAllMuon','SingleMuonHighPTMass','L0Any','DiMuonHighMass'],
        'Hlt2': ['SingleMuon','SingleMuonHighPT','DiMuonJPsi','DiMuonJPsiHighPT','DiMuonDetachedJPsi']
        },
    'KS': {
        'L0':   ['Hadron'],
        'Hlt1': Hlt1Base,
        'Hlt2': ['Topo2BodyBBDT']
        },
    'Track': {
        'L0': L0Base,
        'Hlt1': Hlt1Base,
        'Hlt2': []
        }
    }

CommonlyUsedTriggersRun2 = {
    'B': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2+['TrackMuon','TrackMuonMVA'],
        'Hlt2': Hlt2TopoRun2+Hlt2TopoMuRun2+['SingleMuon','SingleMuonHighPT']
        },
    'D0': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'D+': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'Ds+': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'D*': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'Lc': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'L': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': Hlt2TopoRun2
        },
    'Mu': {
        'L0':   L0Base,
        'Hlt1': Hlt1BaseRun2+['TrackAllMuon','SingleMuonHighPTMass'],
        'Hlt2': ['SingleMuon','SingleMuonHighPT']
        },
    'Phi': {
        'L0':   [],
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': ['IncPhi']
        },
    'Jpsi': {
        'L0':   ['Hadron','Muon','DiMuon','Global'],
        'Hlt1': Hlt1BaseRun2+['TrackAllMuon','SingleMuonHighPTMass','DiMuonHighMass'],
        'Hlt2': ['SingleMuon','SingleMuonHighPT','DiMuonJPsi','DiMuonJPsiHighPT','DiMuonDetachedJPsi']
        },
    'KS': {
        'L0':   ['Hadron'],
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': ['Topo2Body']
        },
    'Track': {
        'L0': L0Base,
        'Hlt1': Hlt1BaseRun2,
        'Hlt2': []
        }
    }
