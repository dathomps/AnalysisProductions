#
# DVTupleMaker
# A script to make tuples out of the stripping using DaVinci
#
from Configurables import DaVinci
import B2KSHHH.global_variables as global_variables
from B2KSHHH.utils import DecayTreeTupleCreator, MomentumCorrection, CreateTupleMaker, RootInTes
from B2KSHHH.dictionaries import CommonTupMaker, TupConf

FilteredProd = 'filtered_production_stream' in global_variables.gvars.keys()

dv = DaVinci()
InputType = 'DST' if FilteredProd else 'MDST'
MCStream = (global_variables.gvars['filtered_production_stream'] if FilteredProd else 'AllStreams') if dv.Simulation else None
if FilteredProd: CommonTupMaker['stream'] = global_variables.gvars['filtered_production_stream']

# Create dictionary for DecayTreeTupleCreator
tm = CreateTupleMaker(TupConf,CommonTupMaker)
ntuples = [DecayTreeTupleCreator(k,tm[k],InputType,MCStream=MCStream).getTool() for k in tm.keys()]

########################################
# User configuration ends
########################################

dv = DaVinci()
#dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = [MomentumCorrection(dv.Simulation)]+ntuples
dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.Turbo = False
dv.InputType = InputType
dv.RootInTES = RootInTes(CommonTupMaker['stream'],dv.Simulation,FilteredProd)
dv.Lumi = True
