from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

from copy import copy

from Configurables import (
        DaVinci,
        GaudiSequencer,
        ChargedProtoANNPIDConf,
        LoKi__Hybrid__Dict2Tuple,
        LoKi__Hybrid__DictOfFunctors)
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

from PhysConf.Selections import (AutomaticData, MomentumScaling,
                                 RebuildSelection, SelectionSequence,
                                 CombineSelection)
from PhysConf.Filters import LoKi_Filters
from StandardParticles import StdAllLooseMuons
from TeslaTools import TeslaTruthUtils


DaVinci().Turbo = True
DaVinci().RootInTES = '/Event/Turbo'


hh_keys = {'RS': 'KmPip'}

hlt2_lines = {
        '{k}{ltunb}'.format(k=k, ltunb=ltunb):
        'Hlt2CharmHadDstp2D0Pip_D02{hh}{ltunb}Turbo'.format(hh=hh, ltunb=ltunb)
        for k, hh in hh_keys.iteritems()
        for ltunb in ['', '_LTUNB']}

# https://twiki.cern.ch/twiki/bin/view/LHCb/RefitTracksFromDST#Momentum_Scale_correction
particles = {k: AutomaticData('{hlt2_line}/Particles'.format(hlt2_line=hlt2_line))
             for k, hlt2_line in hlt2_lines.iteritems()}
if not DaVinci().Simulation:
    for k in particles.keys():
        particles[k] = MomentumScaling(
                particles[k], Turbo=True, Year=DaVinci().DataType)

# https://twiki.cern.ch/twiki/bin/view/LHCb/EnsureProbNNsCalculated
probnn_sequence = {}
for k, v in hlt2_lines.iteritems():
    probnn_sequence[k] = GaudiSequencer('{0}_ProbNNSeq'.format(k))
    annpid = ChargedProtoANNPIDConf(
        k + 'ProbNNalg',
        RecoSequencer=probnn_sequence[k],
        ProtoParticlesLocation='{0}/Protos'.format(v)
    )


# K- pi+, K+ K-, pi+ pi- are reconstructed only as D0
# https://gitlab.cern.ch/lhcb/Hlt/blob/2018-patches/Hlt/Hlt2Lines/python/Hlt2Lines
#   - CharmHad/D02HHLines.py
#   - SLB/Lines.py
decay_descriptors = {
      "RS_LTUNB": "     ${Dst}[D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+]CC",
      "RS": "           ${Dst}[D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+]CC"
}

decay_descriptors_mc = {
      "RS": "                  ${Dst}[D*(2010)+ => ${D0}(D0     => ${P1}K-  ${P2}pi+) ${sPi}pi+]CC"
}


def AddDTF(dtt, constrain_pv=True, constrain_d0_m=False):
    # DecayTreeFitter configuration
    # https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinciTutorial9b
    name = 'DTF{0}{1}'.format('_PV' if constrain_pv else '',
                              '_D0M' if constrain_d0_m else '')
    DictTuple = dtt.Dst.addTupleTool(LoKi__Hybrid__Dict2Tuple,
                                     '{}_DictTuple'.format(name))
    DictTuple.addTool(DTFDict, name)
    DictTuple.Source = 'LoKi::Hybrid::DTFDict/{}'.format(name)
    DictTuple.NumVar = 22
    dtf = getattr(DictTuple, name)
    dtf.constrainToOriginVertex = constrain_pv
    if constrain_d0_m:
        dtf.daughtersToConstrain = ['D0', 'D~0']
    dtf.addTool(LoKi__Hybrid__DictOfFunctors, 'dict')
    dtf.Source = 'LoKi::Hybrid::DictOfFunctors/dict'
    dtf.dict.Variables = {
            'Dst_M': 'M',
            'Dst_PX': 'PX',
            'Dst_PY': 'PY',
            'Dst_PZ': 'PZ',
            'D0_M': 'CHILD(1, M)',
            'D0_PX': 'CHILD(1, PX)',
            'D0_PY': 'CHILD(1, PY)',
            'D0_PZ': 'CHILD(1, PZ)',
            'P1_PX': 'CHILD(1, CHILD(2, PX))',
            'P1_PY': 'CHILD(1, CHILD(2, PY))',
            'P1_PZ': 'CHILD(1, CHILD(2, PZ))',
            'P2_PX': 'CHILD(1, CHILD(1, PX))',
            'P2_PY': 'CHILD(1, CHILD(1, PY))',
            'P2_PZ': 'CHILD(1, CHILD(1, PZ))',
            'sPi_PX': 'CHILD(2, PX)',
            'sPi_PY': 'CHILD(2, PY)',
            'sPi_PZ': 'CHILD(2, PZ)'}
    # the DTF fit is run once for each of the following variable
    Loki_DTF = dtt.Dst.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_{0}'.format(
                name))
    Loki_DTF.Variables = {
            "{0}_D0_CTAU".format(name): "DTF_CTAU(1, {0}{1})".format(
                    True if constrain_pv else False,
                    ", strings(['D0', 'D~0'])" if constrain_d0_m else ""),
            "{0}_D0_CTAUERR".format(name): "DTF_CTAUERR( 1, {0}{1})".format(
                    True if constrain_pv else False,
                    ", strings(['D0', 'D~0'])" if constrain_d0_m else "")}
    return


def SetupMCTools(dtt, is_turbo):
    dtt.ToolList += ['TupleToolMCTruth', 'TupleToolMCBackgroundInfo']
    mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic', 'MCTupleToolHierarchy']
    # https://twiki.cern.ch/twiki/bin/view/LHCb/MakeNTupleFromTurbo#Monte_Carlo_truth
    relations = TeslaTruthUtils.getRelLocs() + [
            TeslaTruthUtils.getRelLoc(''),
            'Relations/Hlt2/Protos/Charged']  # Location of the truth tables for PersistReco objects
    TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)
    return


# see https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/*/src/*
tuple_tools = [
        "TupleToolEventInfo",   # DecayTreeTupleTrigger
        "TupleToolBeamSpot",    # DecayTreeTuple
        "TupleToolPid",         # DecayTreeTuple
        "TupleToolRecoStats",   # DecayTreeTupleReco
        "TupleToolMuonPid"]

l0_triggers = [
        "L0DiMuonDecision",
        "L0ElectronDecision",
        "L0HadronDecision",
        "L0MuonDecision",
        "L0PhotonDecision"]

hlt1_triggers = [
        "Hlt1TrackMVADecision",
        "Hlt1TwoTrackMVADecision",
        "Hlt1CalibTrackingKPiDecision",
        "Hlt1CalibTrackingKKDecision",
        "Hlt1CalibTrackingPiPiDecision"]

loki_vars_dst = {
    "DOCACHI2": "DOCACHI2(1,2)",
    "DOCA": "DOCA(1,2)"}

loki_vars_d0 = {
    "DOCACHI2": "DOCACHI2(1,2)",
    "DOCA": "DOCA(1,2)",
    "BPVLTIME": "BPVLTIME()"}


def MakeTuple(key):
    """ Returns a DecayTreeTuple algorithm configured to be run on real data"""

    dtt = DecayTreeTuple("{0}_Tuple".format(key))
    dtt.TupleName = key
    dtt.setDescriptorTemplate(decay_descriptors[key])
    dtt.Inputs = [particles[key].outputLocation()]

    dtt.ToolList = copy(tuple_tools)

    # path of header files starts with https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/*/src
    dtt.addTupleTool("TupleToolTrackInfo").Verbose = True               # DecayTreeTupleReco
    dtt.addTupleTool("TupleToolGeometry").Verbose = False               # DecayTreeTuple
    dtt.addTupleTool("TupleToolKinematic").Verbose = True               # DecayTreeTuple
    dtt.addTupleTool("TupleToolANNPID").ANNPIDTunes = ["MC15TuneV1"]    # DecayTreeTupleANNPID
    primTool = dtt.addTupleTool("TupleToolPrimaries")                   # DecayTreeTuple
    primTool.Verbose = True

    # trigger global info for the event - DecayTreeTupleTrigger/src/TupleToolTrigger.h
    global_trigger = dtt.addTupleTool("TupleToolTrigger")
    global_trigger.VerboseL0 = True
    global_trigger.TriggerList = copy(l0_triggers)

    # TISTOS - DecayTreeTupleTrigger/src/TupleToolTISTOS.h
    tis_tos_tools = [
            dtt.D0.addTupleTool("TupleToolTISTOS"),
            dtt.P1.addTupleTool("TupleToolTISTOS"),
            dtt.P2.addTupleTool("TupleToolTISTOS")]
    for tis_tos_tool in tis_tos_tools:
        tis_tos_tool.VerboseL0 = True
        tis_tos_tool.VerboseHlt1 = True
        tis_tos_tool.TriggerList = l0_triggers + hlt1_triggers

    hlt2_key = key[:2]
    hlt2_triggers = [
        hlt2_lines[hlt2_key] + 'Decision',
        hlt2_lines[hlt2_key + '_LTUNB'] + 'Decision']
    hlt2_tool = dtt.Dst.addTupleTool('TupleToolTISTOS')
    hlt2_tool.VerboseHlt2 = True
    hlt2_tool.FillHlt2 = True
    hlt2_tool.TriggerList = copy(hlt2_triggers)

    LoKi_D0 = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
    LoKi_D0.Variables = loki_vars_d0

    AddDTF(dtt, constrain_pv=True, constrain_d0_m=False)
    AddDTF(dtt, constrain_pv=True, constrain_d0_m=True)

    if not ('SL' in key):
        LoKi_DStar = dtt.Dst.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_DStar")
        LoKi_DStar.Variables = loki_vars_dst

    if DaVinci().Simulation:
        SetupMCTools(dtt, DaVinci().Turbo)

    return dtt


def MakeTupleMC(key):
    """ Returns a MCDecayTreeTuple algorithm configured to be run on simulated
        data
    """

    dtt = MCDecayTreeTuple('{0}_MCTuple'.format(key))
    dtt.TupleName = key
    dtt.setDescriptorTemplate(decay_descriptors_mc[key])
    dtt.ToolList = [
            "MCTupleToolKinematic",
            "TupleToolEventInfo"]
    return dtt


tuple_keys = hlt2_lines.keys()
tuples = [MakeTuple(key) for key in tuple_keys]
if DaVinci().Simulation:
    tuples += [MakeTupleMC(key) for key in decay_descriptors_mc.iterkeys()]


def MomentumCorrection(is_mc=False):
    """
    Returns the momentum scale correction algorithm for data tracks or the
    momentum smearing algorithm for MC tracks
    """
    if not is_mc:
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else:
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return


trigger_filter = LoKi_Filters(
    HLT1_Code="  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')"
              "| HLT_PASS_RE('Hlt1CalibTracking.*Decision')",
    HLT2_Code="  HLT_PASS_RE('"+hlt2_lines['RS']+"Decision') "
              "| HLT_PASS_RE('"+hlt2_lines['RS_LTUNB']+"Decision') ")


DaVinci().EventPreFilters = trigger_filter.filters('TriggerFilter')
DaVinci().UserAlgorithms += [v for v in probnn_sequence.itervalues()]
DaVinci().UserAlgorithms += [MomentumCorrection(DaVinci().Simulation)]
DaVinci().UserAlgorithms += [p for p in particles.itervalues()]
DaVinci().UserAlgorithms += tuples
