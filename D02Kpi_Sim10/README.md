# AnalysisProductions options for $` D^{0}\rightarrow K^- \pi^+ `$

The options contained in this directory generate the NTuples for the Sim10 
simulation of prompt 
$` D^{\ast+} \rightarrow (D^{0}\rightarrow K^- \pi^+) \pi^{+}_{tag} `$
decays (for both standard and LTUNB triggers).
