#!/usr/bin/env python
"""
	Retrieve Generator Level efficiencies from statistic webpage for SIM09/SIM08 prod stat for samples in gTools.py
	to run : python scripts/retrieveGenEff.py --outdir="nameofoutdirtostoreresults" [ default = geneffs_test]
"""

__author__ = "Renato Quagliani, reworked Alessio Piucci script"

import sys
import subprocess
try:
    from bs4 import BeautifulSoup
except ImportError:
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", "--user", "bs4"]
    )
    from bs4 import BeautifulSoup
import socket
import os.path
import itertools
import tempfile
import argparse
#from prodIDExtractor import *
import re
import os
if sys.version_info[0] < 3:
    py2 = True
    from urllib2 import Request, urlopen, URLError, HTTPError
else:
    py2 = False
    from urllib.request import Request, urlopen, URLError, HTTPError
#sys.path.append('{0}/ganga'.format(os.getcwd()))
#sys.path.append('{0}/scripts'.format(os.getcwd()))
from helpers.tags import tags
#import gTools
# urllib2 is not compatible with Python 3...
# for more informations, read here:
# https://docs.python.org/2/howto/urllib2
# https://docs.python.org/2/library/htmlparser
# try:
#     import urllib.request as urllib2
# except ImportError:
#     import urllib.request
#     import urllib.error
#     import urllib.parse

#import BeautifulSoup
#from BeautifulSoup import BeautifulSoup
socket.setdefaulttimeout(100000)  # specify connection timeout in seconds 10 seconds
# Import from gTools the DecayDictionary used to submit jobs to the GRID, each element in this dictionary has a file associated to in BKKP
#from  gTools import DecayDictionary
###pass an html table and a set of columns to get out
def get_table( table, filter_column = [] ) :     
    rows = table.findAll('tr')
    # arrays with titles, efficiency values and errors
    titles = []
    eff = []
    errors = []
    # print rows
    # parsing of tables
    for tr in rows:
        # finding columns
        cols = tr.findAll('td')
        for td in cols:
            text = ''.join(td.find(text=True))
            # encoding the efficiency value
            if text[0].isdigit() == True:
                eff.append(text)
            else:
                # encoding the title
                if text[0].isalpha() == True:
                    titles.append(text)
                else:
                    # encoding the error value
                    if py2:
                        if text[0] == u"\u00B1":
                            # I'm not interested on the initial "+-"
                            errors.append(text[1:len(text)])
                    else:
                        if text[0] == "\u00B1":
                            # I'm not interested on the initial "+-"
                            errors.append(text[1:len(text)])
    if len(filter_column) != 0 :
        filter_indices = []        
        for idx, t in enumerate(titles) : 
            for s_col in filter_column : 
                if t == s_col and idx not in filter_indices: 
                    filter_indices.append( idx)
        titles = [  titles[i] for i in filter_indices ]
        eff    = [  eff[i] for i in filter_indices ]
        errors = [  errors[i] for i in filter_indices ]
    return titles, eff,errors

def pretty(d, indent=0):
    """pretty printing of dictionaries, indent is the indentation level"""
    for key, value in list(d.items()):
        indentation = '\t' * (indent + 1)
        print('{0} {1}'.format(indentation, str(key)))
        if isinstance(value, dict):
            pretty(value, indent + 1)
        else:
            indentation = '\t' * (indent + 1)
            print('{0} {1}'.format(indentation, str(value)))


def openURL(URL):
    """
            Given the webpage URL , return the full htmlSource code in it
    """
    req = Request(URL)
    # try to retrieve the source
    try:
        urlopen(req)
    except HTTPError as e:
        print("The server could not fulfill the request.")
        print("Error code: ", e.code)
    except URLError as e:
        print("Failed to reach a server.")
        print("Reason: ", e.reason)
    else:
        #everything is fine
        sock = urlopen(req)
        # reading the html source
        htmlSource = sock.read()
        # closing the Web link
        sock.close()
        return htmlSource


def getInfoYamlToStore(inputs, condition=""):
    """
            Given the inputs defined as a dictionary for :
            inputs = {"EventTypeID" : number ,
                              "RD-WG-URL"   : url with GENerator Statistics tables , shtml
                              "condition"   : Sim0X-BeamYYYYGeV-201Z-MagPPP-NuUU-FFns-PythiaN}
                              if condition = "" fill a list of all the found URLS available
            return output in the form
            results = [] list
            resutls filled with dictionaries being  the Generator level cut efficiency
             { "condition" : condition,
               "URL" : str(url),
               "GenLevVar" : var[1] ,
               "Val" : eff[i] ,
               "Err" : errors[i]
             }
    """
    # pretty( inputs )
    variables = [['Generator level cut efficiency', 'generator level cut'],
                 ['particle cut efficiency', 'generator particle level cut'],
                 ['anti-particle cut efficiency', 'generator anti-particle level cut']]
    # MC condition to save in the output file
    condition = inputs['condition']
    SIMVER = inputs['sim']
    ########

    # parser for the input options from command line
    import getopt
    import sys
    decay_type = str(inputs["EventTypeID"])
    year = str(inputs["Year"])
    polarity = str(inputs["Polarity"])
    RD_WG_URL = inputs['RD-WG-URL']
    main_URL_toappend = RD_WG_URL
    main_URL = main_URL_toappend + "index.shtml"
    # +inputs['condition']
    # I first open the main URl of the generator efficiencies
    # print "Open url = "+ main_URL
    main_hmtlSource = openURL(main_URL)

    # now I parse all the links contained in the main URL
    main_soup = BeautifulSoup(main_hmtlSource, "html.parser")
    # print(main_soup)

    # array of URLs contained in the main URL
    URLs = []

    linkCon = condition
    print("---- main_URL        : ", main_URL)
    print("---- Condition search: ", condition)
    print("---- EventType ID    : ", decay_type)

    if 'Sim08' in condition:
        parts = linkCon.split('-')
        linkCon = '{}-{}-{}-{}-{}-{}'.format(parts[0], parts[1], parts[3], parts[2], parts[4], parts[5])
        linkCon = linkCon.replace('Nu', 'nu').replace('MagDown', 'md100').replace('MagUp', 'mu100')
    for link in main_soup.find_all('a', href=True):
        if str(decay_type) in link:
            if condition in link['href']:
                link_name = main_URL_toappend + link['href']
                URLs.append(link_name)
                print("URL found matching condition: {0}".format(link_name))

    #Force other URLs to be checked as well.
    if 'Sim08' not in condition : 
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/RD-WG/Generation_{}.html#{}".format(condition, decay_type ))
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/B2Charm-WG/Generation_{}.html#{}".format(condition, decay_type ))
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/Charmless-WG/Generation_{}.html#{}".format(condition, decay_type ))
    else : 
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM08STAT/RD-WG/Generation_{}.html#{}".format(condition, decay_type )) 
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM08STAT/B2Charm-WG/Generation_{}.html#{}".format(condition, decay_type )) 
        URLs.append( "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM08STAT/Charmless-WG/Generation_{}.html#{}".format(condition, decay_type )) 

    if len(URLs) == 0:
        print("No URLs Found...")
        return []
    else:
        print("found {0} URLS".format((len(URLs))))

    def srtLog(url):
        return "RD-WG" in url
    # GIVE PRIORITY TO URLS ASSOCIATED TO RD-WG !
    URLs.sort(reverse=True, key=srtLog)

    results = []
    for url in URLs:
        print("==== Now searching in: ", url)
        htmlSource = openURL(url)

        # search for the correct decay type, and selection of the interested portion of text
        search_decay = ('<a name="' + decay_type + '"> </a>').encode('utf-8')

        # first I check if the desired decay descriptor is in the current URL
        if search_decay in htmlSource:
            # print("-----------------------------\n")
            """
            start_text , end_text : indices of the HLTML file from a decay to another 
            """    
            start_text = htmlSource.find(search_decay)
            end_text = htmlSource.find(('<a name="').encode('utf-8'), start_text + 1)
            final_text = htmlSource[start_text:end_text]
            """
            Make a soup of the HTML file
            """   
            soup = BeautifulSoup(final_text, "html.parser")
            """
            Get all tables which are present in the HTML file
            For a given eventType there might be more than 1 entry with tags stored in paragraphs
            """
            tables = soup.find_all("table")

            TAGS = [] 
            if "SIM09STAT" in url : 
                for tag in soup.find_all('p'):
                    for simVer in tag.find_all('strong'):
                        simVer = "{}".format( str(simVer).replace("<strong>","").replace("</strong>","").replace("\n","").replace("\t","") )
                        TAGS.append(str(simVer))

            if len(TAGS) == 0 : 
                """ if you have not found any tag associated , dummy filler here, plug in whatever makes sense """
                print("Append dummy SIMVER TAG [Sim08 case]")
                TAGS.append( "{}".format(SIMVER))                     
            else : 
                print("TAGS from paragraph found {}".format(TAGS))
            tables_tagged = {}
            IDX = 0 
            for SIM_VER in TAGS : 
                tables_tagged.update( { SIM_VER : { "tables"  : [ tables[idx + IDX*4] for idx in [ 0,1,2,3] ] , 
                                                    "titles" : [] , 
                                                    "efficiency" : [],
                                                    "errors" : []  } }  )
                IDX+=1
            if SIMVER not in TAGS : 
                print("Failure, Tag not found {}  in {}".format( SIMVER, TAGS))
                continue
                #return []                

            for SIM_VER in TAGS : 
                if SIM_VER != SIMVER:
                    continue 
                print("------Filling results from {} ------".format(SIM_VER))                        
                for table in tables_tagged[SIM_VER]["tables"] : 
                    stitles, sefficiency, serrors = get_table( table , ['Generator level cut efficiency'])
                    if(len(stitles) == 0 or len(stitles) not in [1,2,3]  ):                         
                        continue
                    for tt in range(0,len(stitles)) : 
                        tables_tagged[SIM_VER]["titles"].append( stitles[tt])    
                        tables_tagged[SIM_VER]["efficiency"].append( sefficiency[tt])
                        tables_tagged[SIM_VER]["errors"].append( serrors[tt])

            titles = tables_tagged[SIMVER]['titles']
            eff    = tables_tagged[SIMVER]['efficiency']
            errors = tables_tagged[SIMVER]['errors']
            for i in range(0, len(titles)):
                if 'Generator level cut efficiency' in titles[i] and len( results) ==0:
                    print("ELEMENT : " + titles[i], eff[i], " +- ", errors[i])
                    for var in variables:
                        # if titles[i] == "generator level cut" :
                        # print var[0]                        
                        if titles[i] == var[0] and var[0] == 'Generator level cut efficiency':
                            print("---->GenLevelCut appending {0}".format(titles[i]))
                            results.append({"condition": condition,
                                            "URL": str(url),
                                            "GenLevVar": var[1],
                                            "Val": eff[i],
                                            "Err": errors[i]})
                    #break
                """
                if 'particle cut efficiency' in titles[i] and len(results_particle)==0: 
                    print("ELEMENT : " + titles[i], eff[i], " +- ", errors[i])
                    for var in variables : 
                        if titles[i] == var[0] and var[0] == 'particle cut efficiency' : 
                        print('---->Particle cut appending {0}'.format(titles[i]))
                        results_particle.append({"condition": condition,
                                                "URL": str(url),
                                                "GenLevVar": var[1],
                                                "Val": eff[i],
                                                "Err": errors[i]})                        
                if 'anti-particle cut efficiency' in titles[i] and len(results_antiparticle)==0: 
                    print("ELEMENT : " + titles[i], eff[i], " +- ", errors[i])
                    for var in variables :
                    if titles[i] == var[0] and var[0] == 'anti-particle cut efficiency' :
                    print('---->AntiParticle cut appending {0}'.format(titles[i]))
                    results_antiparticle.append({"condition": condition,
                    "URL": str(url),
                    "GenLevVar": var[1],
                    "Val": eff[i],
                    "Err": errors[i]})                        
                """

    print(results)
    #print(results_antiparticle)
    #print(results_particle)
    #break
    return results #, results_antiparticle, results_particle


def main():
    '''
            Main method : parser --outDir="directory_to_store_results"
    '''
    ########

    samples = list(set([sample for sample in tags]))
    print(samples)
    import argparse
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument("--outdir", type=str, default="NONE", help="the output dir to store results")
    parser.add_argument("--sample", type=str, default="ALL", help="Make it for only one sample instead of running over all available ones")
    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--dirac', dest='useDirac', action='store_true')
    feature_parser.add_argument('--no-dirac', dest='useDirac', action='store_false')
    parser.set_defaults(useDirac=True)

    parser.add_argument("--useDirac", type=bool, default=True, help="Run it faster not running dirac commands to retrieve DDDB tags etc")
    args = parser.parse_args()
    out_dir = args.outdir
    one_sample = args.sample
    useDirac = args.useDirac
    print("OUTDIR {0} SAMPLE {1} DIRAC {2}".format(out_dir, one_sample, useDirac))
    if out_dir == "NONE":
        parser.print_help()
        print("available samples = ")
        for dec in samples:
            print(dec)
        exit()
    if not os.path.isdir(out_dir):
        os.system('mkdir {0}'.format(out_dir))
    else:
        print("Rewriting content for samples in {0}".format(out_dir))
    webpage_09 = "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/"
    webpage_08 = "http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM08STAT/"
    # Condition used... What about Pythia6 ?
    WebURLProdStat = {'Sim09': {'2011': {'MD': 'Sim09-Beam3500GeV-2011-MagDown-Nu2-Pythia8',
                                         'MU': 'Sim09-Beam3500GeV-2011-MagUp-Nu2-Pythia8',
                                       },

                                '2012': {'MD': 'Sim09-Beam4000GeV-2012-MagDown-Nu2.5-Pythia8',
                                         'MU': 'Sim09-Beam4000GeV-2012-MagUp-Nu2.5-Pythia8'},

                                '2015': {'MD': 'Sim09-Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8',
                                         'MU': 'Sim09-Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8'},

                                '2016': {'MD': 'Sim09-Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8',
                                         'MU': 'Sim09-Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8'},
                                '2017': {'MD': 'Sim09-Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8',
                                         'MU': 'Sim09-Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8'},
                                '2018': {'MD': 'Sim09-Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8',
                                         'MU': 'Sim09-Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8'}
                            },

                        'Sim08': {'2011': {'MD': 'Sim08-Beam3500GeV-md100-2011-nu2-Pythia8',
                        'MU': 'Sim08-Beam3500GeV-mu100-2011-nu2-Pythia8'},

                        '2012': {'MD': 'Sim08-Beam4000GeV-md100-2012-nu2.5-Pythia8',
                        'MU': 'Sim08-Beam4000GeV-mu100-2012-nu2.5-Pythia8'},

                        '2015': {'MD': 'Sim08-Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8   ',
                        'MU': 'Sim08-Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8   '},

                        '2016': {'MD': 'Sim08-Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8',
                        'MU': 'Sim08-Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8'},
                        },
                        }
    # mode_years = {}
    polaritySamples=["Bd2Ksee", "Bd2KsJpsee", "Bd2KsPsiee", "Bd2Ksmm", "Bd2KsJpsmm", "Bd2KsPsimm", "Bu2JpsiK_ee", "Bu2JpsiK_mm", "Lb2Lem", "Lb2Lee", "Lb2LJpsee", "Lb2LPsiee", "Lb2Lmm", "Lb2LJpsmm", "Lb2LPsimm", "Lb2Lcenu_L0e", "Lb2Lcenu_L0mu", "Lb2Lcenu_L0pi", "Lb2Lcmunu_L0mu", "Lb2Lcmunu_L0e", "Lb2Lcmunu_L0pi", "Lb2Lpipi", "Lb2LKpi", "Lb2LKK"]
    MISSINGTABLESMISSING = []
    pass_decay = {}
    polarities = ['MD', 'MU']
    for sample in tags:
        if 'LEPTONIC' in sample:
            continue
        if one_sample != 'ALL':
            if str(sample) != str(one_sample.replace(' ', '')):
                print("SKIPPING FROM DICTIONARY {0}!={1}".format(sample, one_sample))
                continue
        print("----> processing sample {0}".format(sample))
        # if year not in ["MC17", "MC18"]:
        #	print "SKIPPING"
        #	continue
        # if sample not in mode_years[sample] :
        # mode_years.update({ str(sample) : [] })
        simVer = tags[sample]['sim']
        evtID  = tags[sample]['id']
        year   = sample[:4]
        sim = 'Sim08'
        webpage = webpage_08
        if '09' in simVer:
            sim = 'Sim09'
            webpage = webpage_09
        sample_dir = sample.split('_')[-1]
        directory = '{0}/{1}_{2}'.format(out_dir, sample_dir, evtID)
        fname_miss = '{0}/{1}_{2}/{3}_MISSING.yaml'.format(out_dir, sample_dir, evtID, sample)
        fname_found = '{0}/{1}_{2}/{3}.yaml'.format(out_dir, sample_dir, evtID, sample)
        if directory not in pass_decay:
            pass_decay[directory] = False

        inputs = []
        # Where to store MISSING and FOUND gen level efficiency

        if pass_decay[directory] == False:
            if os.path.isdir(directory):
                print("REMOVING DIR {0} CONTENT".format(directory))
                os.system('rm -rf {0}/*'.format(directory))
            else:
                os.system('mkdir {0}'.format(directory))

            pass_decay[directory] = True  # next iters will not do remove file
        print("{0} : {1} : {2} ".format(sample, simVer, evtID))
        polConvert = {'MU': 'MagUp', 'MD': 'MagDown'}
         
        pol = "MU" if ("MagUp" in sample) or ("MU" in sample) else "MD" 
        condition = WebURLProdStat[sim][year][pol]

        inputs.append({'DecayMode': sample,
                       'Year': year,
                       'Polarity': pol,
                       'EventTypeID': evtID,
                       'RD-WG-URL': webpage,  # 'http://lhcbdoc.web.cern.ch/lhcbdoc/STATISTICS/SIM09STAT/RD-WG/',
                       'condition': condition,
                       'sim': simVer,
                       })  # < condition we search for!
        for inp in inputs:
            print("--------")
            print(inp['Year'])
            # print inp
            results = getInfoYamlToStore(inp)
            print(f"found {len(results)} results")
            if len(results) == 0:
                if polConvert[inp['Polarity']] in inp['DecayMode']:
                    MISSINGTABLESMISSING.append("{0}-{1}-{2}-{3}  ({4})".format(inp['EventTypeID'], inp['Year'], inp['Polarity'], inp['sim'], inp['DecayMode']))
                    pretty(inp)
                    out_file = open(fname_miss, "a+")
                    out_file.write("    {0} :\n".format(inp['Polarity']))
                    out_file.write("        eType: {0}\n".format(inp['EventTypeID']))
                    out_file.write("        sim : {0}\n".format(inp['sim']))
                    out_file.write("        web : {0}\n".format("-------"))
                    out_file.write("        eff : {0}\n".format("-------"))
                    out_file.write("        err : {0}\n".format("-------"))
                    # TMP uncomment , store stuff for failed always:
                    #prodID, dddb, conddb =  getProdID_DDDB_CondDDB( "20{0}".format( year.replace('MC','')) , polConvert[pol], simVer, str(evtID), inp['BKKPath'])
                    #out_file.write("        DDBProduction : {0}\n".format( dddb) )
                    #out_file.write("        CONDBProduction : {0}\n".format( conddb))
                    #out_file.write("        ProdID          : {0}\n".format( prodID))
                    #out_file.write("        BKKPath         : {0}\n".format( inp['BKKPath']))

                    out_file.close()
            elif len(results) == 1:
                print("SUCCESS")
                #results.append( { "condition" : condition, "URL" : str(URL), "GenLevVar" : var[1] , "Val" : eff[i] , "Err" : errors[i] } )
                # print results
                result = results[0]
                # out_file.write("#---- {0}-{1}-{2}-{3}-{4}"+str(inp['DecayMode']))
                pretty(inp)
                out_file = open(fname_found, "a+")
                out_file.write("    {0} :\n".format(inp['Polarity']))
                out_file.write("        eType: {0}\n".format(inp['EventTypeID']))
                out_file.write("        sim : {0}\n".format(inp['sim']))
                out_file.write("        web : {0}\n".format(result["URL"]))
                out_file.write("        eff : {0}\n".format(result["Val"]))
                out_file.write("        err : {0}\n".format(result["Err"]))
                out_file.close()
            else:
                print("MORE THAN 1 RESULT FOUND , something went wrong")
                pretty(results)
                exit()
            # break

    for missing in MISSINGTABLESMISSING:
        print("MISS TABLE STATISTICS FOR : " + str(missing))


if __name__ == '__main__':
    sys.exit(main())
