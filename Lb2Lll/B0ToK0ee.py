from Lb2Lll.helpers import tuple_maker

# ONLY CONFIG NEEDED
tuple_name = "Bd2JpsiKs_eeTuple"  # BECAUSE THIS RECONSTRUCTS Lb2Lee with Jpsi resonance

tuple_seq = tuple_maker.tuple_maker(tuple_name, upstream_electrons=True)
