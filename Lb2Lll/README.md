# Lb2Lll ntuple options

Option files for creating ntuples for the measurement of $`R(\Lambda^{0})`$ using $`\Lambda_{b}^{0} \to \Lambda^{0} \mathcal{l}\mathcal{l}`$ decays
(and the accompanying LFV and JpsiL modes for a LFV search and normalisation mode BF measurement respectively). 
Based on the BsToJpsiPhi option files. 

All of the datasets that are produced are defined in `info.yaml`.

The different decay mode files use the `tuple_maker` helper file to add all the required TupleTools, DecayTreeFits, and LoKi functions. 
The electron modes include upstream tracks using the `Bu2LLK_eeLine4` stripping line.
The `MC_tuple_maker` helper is used to include MC tuples, with and without the V0 decay.

## Datasets

* $`\Lambda_{b}^{0} \to \Lambda^{0} \mathcal{e}^{+}\mathcal{e}^{-}`$
* $`\Lambda_{b}^{0} \to \Lambda^{0} \mathcal{e}^{+}\mathcal{e}^{+}`$
* $`\Lambda_{b}^{0} \to \Lambda^{0} \mu^{+}\mu^{-}`$
* $`\Lambda_{b}^{0} \to \Lambda^{0} \mu^{+}\mu^{+}`$
* $`\Lambda_{b}^{0} \to \Lambda^{0} \mathcal{e}^{+}\mu^{-}`$
* $`\Lambda_{b}^{0} \to \Lambda^{0} \mathcal{e}^{+}\mu^{+}`$
* $`B^{0} \to K^{0}_{S} \mathcal{e}^{+}\mathcal{e}^{-}`$
* $`B^{0} \to K^{0}_{S} \mu^{+}\mu^{-}`$
* $`B^{\pm} \to K^{\pm} e^{+}e^{-}`$
* $`B^{\pm} \to K^{\pm} \mu^{+}\mu^{-}`$

## Monte Carlo samples
The MC samples currently included are for Lb->Lll including mumu, emu, ee and Jpsi, Psi2s resonant modes;
Bd->Ksll including mumu, ee and Jpsi, Psi2s resonant modes; 
Bu->JpsiK (mumu and ee) and all the currently available Lb SL modes.

## Local testing
1. Clone the repository using `git clone -b mmulder_Lb2Lll ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/WG/CharmWGProd.git`.
2. Go into directory `cd AnalysisProductions`
3. List available productions with `lb-ap list Lb2Lll`
4. Test interactively with lb-ap debug Lb2Lll SomeJobName` (note: this does not yet include the tuple after stripping job)
