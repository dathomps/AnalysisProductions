###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

"""
  B --> ll K selections for (SS and OS leptons):
  B --s> ee K  versus  B --> mumu K
  Lb --> ee Lambda  versus  Lb --> mumu Lambda
  Lb--> Lambda emu with OS and SS leptons
"""

daughter_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty -> ^X+  (X0 ->  l+  l-)]CC": "{0}H",
    "[Beauty ->  X+  (X0 -> ^l+  l-)]CC": "{0}L1",
    "[Beauty ->  X+  (X0 ->  l+ ^l-)]CC": "{0}L2",
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X0 ->  l+  l-)]CC": "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X0 ->  l+  l-)]CC": "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X0 ->  l+  l-)]CC": "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X0 ->  l+  l-)]CC": "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X0 ->  l+  l-)]CC": "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 -> ^l+  l-)]CC": "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X0 ->  l+ ^l-)]CC": "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X0 ->  l+  l-)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X0 ->  l+  l-)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 -> ^l+  l-)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X0 ->  l+ ^l-)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X0 ->  l+  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X0 ->  l+  l-)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X0 ->  l+  l-)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 -> ^l+  l-)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X0 ->  l+ ^l-)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X0 ->  l+  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X0 ->  l+  l-)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X0 ->  l+  l-)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 -> ^l+  l-)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X0 ->  l+ ^l-)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X0 ->  l+  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l-)]CC": "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l-)]CC": "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l-)]CC": "{0}pi0L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- -> ^X-  X-  X+))]CC": "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X- ^X-  X+))]CC": "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X0 ->  l+  (l- ->  X-  X- ^X+))]CC": "{0}L23",
    # SAME SIGN
    # 3-body
    "[Beauty -> ^X+  (X+ ->  l+  l+)]CC": "{0}H",
    "[Beauty ->  X+  (X+ -> ^l+  l+)]CC": "{0}L1",
    "[Beauty ->  X+  (X+ ->  l+ ^l+)]CC": "{0}L2",
    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> (X+ -> ^X+  X+  X-) (X+ ->  l+  l+)]CC": "{0}H1",
    "[Beauty -> (X+ ->  X+ ^X+  X-) (X+ ->  l+  l+)]CC": "{0}H2",
    "[Beauty -> (X+ ->  X+  X+ ^X-) (X+ ->  l+  l+)]CC": "{0}H3",
    # 5-body
    "[Beauty -> (X+ -> ^X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "{0}H1",
    "[Beauty -> (X+ ->  X+  (X0 -> ^X+  X-)) (X+ ->  l+  l+)]CC": "{0}H2",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+ ^X-)) (X+ ->  l+  l+)]CC": "{0}H3",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ -> ^l+  l+)]CC": "{0}L1",
    "[Beauty -> (X+ ->  X+  (X0 ->  X+  X-)) (X+ ->  l+ ^l+)]CC": "{0}L2",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "{0}HH",
    # 4-body with a strange particle in the final state
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l+  l+)]CC": "{0}H1",
    "[Beauty ->  (X0 -> ^Xs  X-)  (X ->  l-  l-)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l+  l+)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  Xs ^X-)  (X ->  l-  l-)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l+  l+)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X -> ^l-  l-)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l+ ^l+)]CC": "{0}L2",
    "[Beauty ->  (X0 ->  Xs  X-)  (X ->  l- ^l-)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l+  l+)]CC": "{0}HH",
    "[Beauty -> ^(X0 ->  Xs  X-)  (X ->  l-  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l+  l+)]CC": "{0}LL",
    "[Beauty ->  (X0 ->  Xs  X-) ^(X ->  l-  l-)]CC": "{0}LL",
    # 4-body with two pions in the final state
    "[Beauty ->  (X0 -> ^pi+  pi-)  (X+ ->  l+  l+)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  pi+ ^pi-)  (X+ ->  l+  l+)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ -> ^l+  l+)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  pi+  pi-)  (X+ ->  l+ ^l+)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  pi+  pi-)  (X+ ->  l+  l+)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  pi+  pi-) ^(X+ ->  l+  l+)]CC": "{0}LL",
    # 4-body with p and pi in the final state. Here the names are kept generic (H1,H2,L1,L2), for uniformity with strippingof other years. This is needed for Lb->Lll analyses.
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l+  l+)]CC": "{0}H1",
    "[Beauty ->  (X0 -> ^p+  pi-)  (X ->  l-  l-)]CC": "{0}H1",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l+  l+)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  p+ ^pi-)  (X ->  l-  l-)]CC": "{0}H2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l+  l+)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X -> ^l-  l-)]CC": "{0}L1",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l+ ^l+)]CC": "{0}L2",
    "[Beauty ->  (X0 ->  p+  pi-)  (X ->  l- ^l-)]CC": "{0}L2",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l+  l+)]CC": "{0}HH",
    "[Beauty -> ^(X0 ->  p+  pi-)  (X ->  l-  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l+  l+)]CC": "{0}LL",
    "[Beauty ->  (X0 ->  p+  pi-) ^(X ->  l-  l-)]CC": "{0}LL",
    # 4-body decays involving K*(892)+, with pi0 in final state. (Note: had some trouble with generic X+ in the 3-body descriptors matching the K*(892)+, and getting junk output).
    "[Beauty -> (K*(892)+ -> ^K+  pi0) (X0 ->  l+  l+)]CC": "{0}pi0H1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  ^l+  l+)]CC": "{0}pi0L1",
    "[Beauty -> (K*(892)+ -> K+  pi0) (X0 ->  l+  ^l+)]CC": "{0}pi0L2",
    # 7-body (quasi 4-body)
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ -> ^X+  X-  X+))]CC": "{0}L21",
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+ ^X-  X+))]CC": "{0}L22",
    "[Beauty ->  (X0 ->  X+  X-)  (X+ ->  l+  (l+ ->  X+  X- ^X+))]CC": "{0}L23",
}

daughter_vtx_locations = {
    # OPPOSITE SIGN
    # 3-body
    "[Beauty ->  X+ ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X0 ->  l+  l-)]CC": "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X0 ->  l+  l-)]CC": "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X0 ->  l+  l-)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l-)]CC": "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l-)]CC": "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X0 ->  l+  ^(l- -> X-  X-  X+))]CC": "{0}L",
    # SAME SIGN
    # 3-body
    "[Beauty ->  X+ ^(X+ ->  l+  l+)]CC": "{0}LL",
    # 5-body (quasi 3-body)
    "[Beauty -> ^(X+ -> X+  X+  X-) (X+ ->  l+  l+)]CC": "{0}H",
    # 5-body
    "[Beauty -> ^(X+ -> X+  (X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "{0}H",
    "[Beauty -> (X+ ->  X+ ^(X0 ->  X+  X-)) (X+ ->  l+  l+)]CC": "{0}HH",
    # 4-body
    "[Beauty -> ^(X0 ->  X+  X-)  (X ->  l+  l+)]CC": "{0}HH",
    "[Beauty ->  (X0 ->  X+  X-) ^(X ->  l+  l+)]CC": "{0}LL",
    # 4-body with neutral in final state
    "[Beauty -> ^(X+ -> X+  X0) (X0 ->  l+  l+)]CC": "{0}HH",
    "[Beauty -> (X+ -> X+  X0) ^(X0 ->  l+  l+)]CC": "{0}LL",
    # 7-body (quasi 4-body)
    "[Beauty ->  X  (X+ ->  l+  ^(l+ -> X-  X-  X+))]CC": "{0}L",
}

config = {
    "BFlightCHI2": 100,
    "BDIRA": 0.9995,
    "BIPCHI2": 25,
    "BVertexCHI2": 9,
    "DiLeptonPT": 0,
    "DiLeptonFDCHI2": 16,
    "DiLeptonIPCHI2": 0,
    "LeptonIPCHI2": 9,
    "LeptonPT": 350,
    "LeptonPTTight": 500,
    "TauPT": 0,
    "TauVCHI2DOF": 150,
    "KaonIPCHI2": 9,
    "KaonPT": 400,
    "KaonPTLoose": 250,
    "PionPTRho": 350,
    "ProtonP": 2000,
    "KstarPVertexCHI2": 25,
    "KstarPMassWindow": 300,
    "KstarPADOCACHI2": 30,
    "Pi0PT": 600,
    "ProbNNCut": 0.05,
    "ProbNNCutTight": 0.1,
    "DiHadronMass": 2600,
    "DiHadronVtxCHI2": 25,
    "DiHadronADOCACHI2": 30,
    "UpperMass": 5500,
    "BMassWindow": 1500,
    "UpperBMass": 5280,
    "UpperBsMass": 5367,
    "UpperLbMass": 5620,
    "BMassWindowTau": 5000,
    "PIDe": 0,
    "RICHPIDe_Up": -5,
    "Trk_Chi2": 3,
    "Trk_GhostProb": 0.35,  # 0.4
    "K1_MassWindow_Lo": 0,
    "K1_MassWindow_Hi": 4200,
    "K1_VtxChi2": 12,
    "K1_SumPTHad": 1200,  # 800
    "K1_SumIPChi2Had": 48.0,
    "Bu2eeLinePrescale": 1,
    "Bu2eeLine2Prescale": 1,
    "Bu2eeSSLine2Prescale": 1,
    "Bu2eeLine3Prescale": 1,
    "Bu2eeLine4Prescale": 1,
    "Bu2mmLinePrescale": 1,
    "Bu2mmSSLinePrescale": 1,
    "Bu2meLinePrescale": 1,
    "Bu2meSSLinePrescale": 1,
    "Bu2mtLinePrescale": 1,
    "Bu2mtSSLinePrescale": 1,
    "RelatedInfoTools": [
        {
            "Type": "RelInfoVertexIsolation",
            "Location": "VertexIsoInfo",
            "IgnoreUnmatchedDescriptors": True,
            "DaughterLocations": {
                key: val.format("VertexIsoInfo")
                for key, val in daughter_vtx_locations.items()
            },
        },
        {
            "Type": "RelInfoVertexIsolationBDT",
            "Location": "VertexIsoBDTInfo",
            "IgnoreUnmatchedDescriptors": True,
            "DaughterLocations": {
                key: val.format("VertexIsoBDTInfo")
                for key, val in daughter_vtx_locations.items()
            },
        },
        {
            "Type": "RelInfoConeVariables",
            "ConeAngle": 0.5,
            "IgnoreUnmatchedDescriptors": True,
            "Location": "TrackIsoInfo05",
            "DaughterLocations": {
                key: val.format("TrackIsoInfo")
                for key, val in daughter_locations.items()
            },
        },
        {
            "Type": "RelInfoConeIsolation",
            "ConeSize": 0.5,
            "IgnoreUnmatchedDescriptors": True,
            "Location": "ConeIsoInfo05",
            "DaughterLocations": {
                key: val.format("ConeIsoInfo")
                for key, val in daughter_locations.items()
            },
        },
        {
            "Type": "RelInfoTrackIsolationBDT",
            "IgnoreUnmatchedDescriptors": True,
            # Use the BDT with 9 input variables
            # This requires that the "Variables" value is set to 2
            "Variables": 2,
            "WeightsFile": "BsMuMu_TrackIsolationBDT9vars_v1r4.xml",
            "Location": "TrackIsoBDTInfo",
            "DaughterLocations": {
                key: val.format("TrackIsoBDTInfo")
                for key, val in daughter_locations.items()
            },
        },
        {
            "Type": "RelInfoBs2MuMuTrackIsolations",
            "IgnoreUnmatchedDescriptors": True,
            "Location": "TrackIsoBs2MMInfo",
            "DaughterLocations": {
                key: val.format("TrackIsoBs2MMInfo")
                for key, val in daughter_locations.items()
            },
        },
        {
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.0,
            "Location": "TrackIsoInfo10",
        },
        {
            "Type": "RelInfoConeVariables",
            "ConeAngle": 1.5,
            "Location": "TrackIsoInfo15",
        },
        {
            "Type": "RelInfoConeVariables",
            "ConeAngle": 2.0,
            "Location": "TrackIsoInfo20",
        },
        {"Type": "RelInfoConeIsolation", "ConeSize": 1.0, "Location": "ConeIsoInfo10"},
        {"Type": "RelInfoConeIsolation", "ConeSize": 1.5, "Location": "ConeIsoInfo15"},
        {"Type": "RelInfoConeIsolation", "ConeSize": 2.0, "Location": "ConeIsoInfo20"},
    ],
}

# This tool applies the BDT track isolation from the 2017 B->mumu analysis to the Jpsi daughters
# It is separated from the other tools, since it cannot be applied to lines containing taus
RelInfoTrackIsolationBDT2 = [
    {
        "Type": "RelInfoTrackIsolationBDT2",
        "Location": "TrackIsolationBDT2",
        "Particles": [1, 2],
    }
]

from Gaudi.Configuration import *
from GaudiConfUtils.ConfigurableGenerators import (
    FilterDesktop,
    CombineParticles,
    DaVinci__N3BodyDecays,
)
from PhysSelPython.Wrappers import (
    Selection,
    DataOnDemand,
    MergedSelection,
    AutomaticData,
)
from StrippingConf.StrippingLine import StrippingLine
from StrippingUtils.Utils import LineBuilder
from GaudiKernel.SystemOfUnits import *
from CommonParticles.Utils import *


class Bu2LLK_NoPID_LineBuilder(LineBuilder):
    """
    Builder for R_X measurements
    """

    # now just define keys. Default values are fixed later
    __configuration_keys__ = (
        "BFlightCHI2",
        "BDIRA",
        "BIPCHI2",
        "BVertexCHI2",
        "DiLeptonPT",
        "DiLeptonFDCHI2",
        "DiLeptonIPCHI2",
        "LeptonIPCHI2",
        "LeptonPT",
        "LeptonPTTight",
        "TauPT",
        "TauVCHI2DOF",
        "KaonIPCHI2",
        "KaonPT",
        "KaonPTLoose",
        "PionPTRho",
        "ProtonP",
        "KstarPVertexCHI2",
        "KstarPMassWindow",
        "KstarPADOCACHI2",
        "Pi0PT",
        "ProbNNCut",
        "ProbNNCutTight",
        "DiHadronMass",
        "DiHadronVtxCHI2",
        "DiHadronADOCACHI2",
        "UpperMass",
        "BMassWindow",
        "UpperBMass",
        "UpperBsMass",
        "UpperLbMass",
        "BMassWindowTau",
        "PIDe",
        "RICHPIDe_Up",
        "Trk_Chi2",
        "Trk_GhostProb",
        "K1_MassWindow_Lo",
        "K1_MassWindow_Hi",
        "K1_VtxChi2",
        "K1_SumPTHad",
        "K1_SumIPChi2Had",
        "Bu2eeLinePrescale",
        "Bu2eeLine2Prescale",
        "Bu2eeSSLine2Prescale",
        "Bu2eeLine3Prescale",
        "Bu2eeLine4Prescale",
        "Bu2mmLinePrescale",
        "Bu2mmSSLinePrescale",
        "Bu2meLinePrescale",
        "Bu2meSSLinePrescale",
        "Bu2mtLinePrescale",
        "Bu2mtSSLinePrescale",
        "RelatedInfoTools",
    )

    def __init__(self, name, config):
        LineBuilder.__init__(self, name, config)

        self._name = name

        mmXLine_name = name + "_mm"
        mmXSSLine_name = name + "_mmSS"
        eeXLine_name = name + "_ee"
        eeXSSLine_name = name + "_eeSS"
        meXLine_name = name + "_me"
        meXSSLine_name = name + "_meSS"

        from StandardParticles import StdAllNoPIDsKaons as Kaons

        # V0s
        from StandardParticles import StdVeryLooseKsLL as KshortsLL
        from StandardParticles import StdLooseKsDD as KshortsDD
        from StandardParticles import StdVeryLooseLambdaLL as LambdasLL
        from StandardParticles import StdLooseLambdaDD as LambdasDD

        from StandardParticles import StdKs2PiPiLL as BrunelKshortsLL
        from StandardParticles import StdKs2PiPiDD as BrunelKshortsDD
        from StandardParticles import StdLambda2PPiLL as BrunelLambdasLL
        from StandardParticles import StdLambda2PPiDD as BrunelLambdasDD

        # 1 : Make K, Ks, and Lambdas

        ### opposite sign hadrons
        SelKaons = self._filterHadron(
            name="KaonsFor" + self._name, sel=Kaons, params=config
        )

        SelKshortsLL = self._filterHadron(
            name="KshortsLLFor" + self._name,
            sel=[KshortsLL, BrunelKshortsLL],
            params=config,
        )

        SelKshortsDD = self._filterHadron(
            name="KshortsDDFor" + self._name,
            sel=[KshortsDD, BrunelKshortsDD],
            params=config,
        )

        SelLambdasLL = self._filterHadron(
            name="LambdasLLFor" + self._name,
            sel=[LambdasLL, BrunelLambdasLL],
            params=config,
        )

        SelLambdasDD = self._filterHadron(
            name="LambdasDDFor" + self._name,
            sel=[LambdasDD, BrunelLambdasDD],
            params=config,
        )

        # 2 : Make Dileptons

        from StandardParticles import StdLooseDetachedDiElectronLU as DiElectronsLU

        from StandardParticles import StdAllNoPIDsMuons as Muons
        from StandardParticles import StdAllNoPIDsElectrons as Electrons

        CombDiMuons = CombineParticles()
        CombDiMuons.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
        CombDiMuons.CombinationCut = "(ADOCACHI2CUT(30, ''))"
        CombDiMuons.MotherCut = "(VFASPF(VCHI2) < 25)"
        DiMuons = Selection(
            "SelNoPIDMuons", Algorithm=CombDiMuons, RequiredSelections=[Muons]
        )

        CombDiElectrons = CombineParticles()
        CombDiElectrons.DecayDescriptor = "J/psi(1S) -> e+ e-"
        CombDiElectrons.DaughtersCuts = {"e+": "(PT>500*MeV)"}
        CombDiElectrons.CombinationCut = "(AM>30*MeV) & (ADOCACHI2CUT(30,''))"
        CombDiElectrons.MotherCut = "(VFASPF(VCHI2)<25)"
        DiElectrons = Selection(
            "SelNoPIDDiElectrons",
            Algorithm=CombDiElectrons,
            RequiredSelections=[Electrons],
        )

        # Now same container as above, but with DiElectronMaker
        from Configurables import DiElectronMaker, ProtoParticleCALOFilter

        MakeDiElectronsFromTracks = DiElectronMaker("MakeNoPIDDiElectronsFromTracks")
        MakeDiElectronsFromTracks.Particle = "J/psi(1S)"
        selector = trackSelector(MakeDiElectronsFromTracks, trackTypes=["Long"])
        MakeDiElectronsFromTracks.addTool(ProtoParticleCALOFilter, name="Electron")
        MakeDiElectronsFromTracks.Electron.Selection = ["RequiresDet='CALO'"]
        MakeDiElectronsFromTracks.DiElectronMassMax = (
            5000.0 * MeV
        )  # 1000000.*GeV #just to give a high limit.  Not setting anything defaults it to 200 which is wrong.
        MakeDiElectronsFromTracks.DiElectronMassMin = 0.0 * MeV  # 30.*MeV
        MakeDiElectronsFromTracks.DiElectronPtMin = 200.0 * MeV  # 500.*MeV
        DiElectronsFromTracks = Selection(
            "SelNoPIDDiElectronsFromTracks", Algorithm=MakeDiElectronsFromTracks
        )

        MuMu_SS = self._makeMuMuSS("MuMuSSFor" + self._name, params=config, muonid=None)
        EE_SS = self._makeEESS("EESSFor" + self._name, params=config, electronid=None)

        MuE = self._makeMuE(
            "MuEFor" + self._name, params=config, electronid=None, muonid=None
        )
        MuE_SS = self._makeMuE(
            "MuESSFor" + self._name,
            params=config,
            electronid=None,
            muonid=None,
            samesign=True,
        )

        # DIFFERENT CUTS FOR THE LU LINE:
        # THEY ALWAYS HAVE PID ON THE LONG TRACK (DLLe > PIDe )
        # ONLY LU COMBINATIONS AND RICHPIDe>(RICHPIDe from config) OR HASBREMADDED FOR THE UPSTREAM TRACKS
        DiElectronIDLU = (
            "((NINTREE((TRTYPE==4)) == 1))"
            % config
        )

        # Define dilepton selections
        SelDiElectron = self._filterDiLepton(
            "SelDiElectronFor" + self._name,
            dilepton=DiElectrons,
            params=config,
            idcut=None,
        )

        SelDiElectronFromTracks = self._filterDiLepton(
            "SelDiElectronFromTracksFor" + self._name,
            dilepton=DiElectronsFromTracks,
            params=config,
            idcut=None,
        )

        SelDiElectronFromTracks_SS = self._filterDiLeptonTight(
            "SelDiElectronFromTracksSSFor" + self._name,
            dilepton=EE_SS,
            params=config,
            idcut=None,
        )

        SelDiElectronLU = self._filterDiLepton(
            "SelDiElectronLU" + self._name,
            dilepton=DiElectronsLU,
            params=config,
            idcut=DiElectronIDLU,
        )

        SelDiMuon = self._filterDiLepton(
            "SelDiMuonsFor" + self._name, dilepton=DiMuons, params=config, idcut=None
        )

        SelDiMuon_SS = self._filterDiLeptonTight(
            "SelMuMuSSFor" + self._name, dilepton=MuMu_SS, params=config, idcut=None
        )

        SelMuE = self._filterDiLepton(
            "SelMuEFor" + self._name, dilepton=MuE, params=config, idcut=None
        )

        SelMuE_SS = self._filterDiLepton(
            "SelMuESSFor" + self._name, dilepton=MuE_SS, params=config, idcut=None
        )

        # 4 : Combine Particles

        SelB2eeX = self._makeB2LLX(
            eeXLine_name,
            dilepton=SelDiElectron,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2eeXFromTracks = self._makeB2LLX(
            eeXLine_name + "2",
            dilepton=SelDiElectronFromTracks,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2eeXFromTracks_SS = self._makeB2LLX(
            eeXSSLine_name + "2",
            dilepton=SelDiElectronFromTracks_SS,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2mmX = self._makeB2LLX(
            mmXLine_name,
            dilepton=SelDiMuon,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2mmX_SS = self._makeB2LLX(
            mmXSSLine_name,
            dilepton=SelDiMuon_SS,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2meX = self._makeB2LLX(
            meXLine_name,
            dilepton=SelMuE,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2meX_SS = self._makeB2LLX(
            meXSSLine_name,
            dilepton=SelMuE_SS,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        SelB2eeXLU = self._makeB2LLX(
            eeXLine_name + "4",
            dilepton=SelDiElectronLU,
            hadrons=[SelKaons, SelKshortsLL, SelKshortsDD, SelLambdasLL, SelLambdasDD],
            params=config,
            masscut="ADAMASS('B+') <  %(BMassWindow)s *MeV" % config,
        )

        # 5 : Declare Lines

        SPDFilter = {
            "Code": " ( recSummary(LHCb.RecSummary.nSPDhits,'Raw/Spd/Digits') < 600 )",
            "Preambulo": [
                "from LoKiNumbers.decorators import *",
                "from LoKiCore.basic import LHCb",
            ],
        }

        self.B2eeXLine = StrippingLine(
            eeXLine_name + "Line",
            prescale=config["Bu2eeLinePrescale"],
            postscale=1,
            selection=SelB2eeX,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
        )

        self.B2eeXFromTracksLine = StrippingLine(
            eeXLine_name + "Line2",
            prescale=config["Bu2eeLine2Prescale"],
            postscale=1,
            selection=SelB2eeXFromTracks,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
        )

        self.B2eeX_SSFromTracksLine = StrippingLine(
            eeXSSLine_name + "Line2",
            prescale=config["Bu2eeSSLine2Prescale"],
            postscale=1,
            selection=SelB2eeXFromTracks_SS,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
            MaxCandidates=300,
        )

        self.B2mmXLine = StrippingLine(
            mmXLine_name + "Line",
            prescale=config["Bu2mmLinePrescale"],
            postscale=1,
            selection=SelB2mmX,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
        )

        self.B2mmX_SSLine = StrippingLine(
            mmXSSLine_name + "Line",
            prescale=config["Bu2mmSSLinePrescale"],
            postscale=1,
            selection=SelB2mmX_SS,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
            MaxCandidates=300,
        )

        self.B2meXLine = StrippingLine(
            meXLine_name + "Line",
            prescale=config["Bu2meLinePrescale"],
            postscale=1,
            selection=SelB2meX,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
            MaxCandidates=400,
        )

        self.B2meX_SSLine = StrippingLine(
            meXSSLine_name + "Line",
            prescale=config["Bu2meSSLinePrescale"],
            postscale=1,
            selection=SelB2meX_SS,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
            MaxCandidates=300,
        )

        self.B2eeXLULine = StrippingLine(
            eeXLine_name + "Line4",
            prescale=config["Bu2eeLine4Prescale"],
            postscale=1,
            selection=SelB2eeXLU,
            RelatedInfoTools=config["RelatedInfoTools"] + RelInfoTrackIsolationBDT2,
            FILTER=SPDFilter,
            RequiredRawEvents=[],
            MDSTFlag=False,
            MaxCandidates=300,
        )

        # 6 : Register Lines

        self.registerLine(self.B2eeXLine)
        self.registerLine(self.B2eeXFromTracksLine)
        self.registerLine(self.B2eeX_SSFromTracksLine)
        self.registerLine(self.B2mmXLine)
        self.registerLine(self.B2mmX_SSLine)
        self.registerLine(self.B2meXLine)
        self.registerLine(self.B2meX_SSLine)
        self.registerLine(self.B2eeXLULine)

    #####################################################

    def _filterHadron(self, name, sel, params):
        """
        Filter for some of hadronic final states
        Check for list as input for V0s which use (Very)Loose and Brunel candidates
        """
        if isinstance(sel, list):
            sel_list = [
                MergedSelection("Merged" + name, RequiredSelections=sel, Unique=True)
            ]
        else:
            sel_list = [sel]

        # requires all basic particles to have IPCHI2 > KaonIPCHI2
        # and hadron PT > KaonPT
        # need to add the ID here
        _Code = (
            "(PT > %(KaonPT)s *MeV) & "
            "(M < %(DiHadronMass)s*MeV) & "
            "((ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s)) | "
            "(NDAUGHTERS == NINTREE(ISBASIC & (MIPCHI2DV(PRIMARY) > %(KaonIPCHI2)s))))"
            % params
        )

        _Filter = FilterDesktop(Code=_Code)

        return Selection(name, Algorithm=_Filter, RequiredSelections=sel_list)

    #####################################################

    def _filterDiLepton(self, name, dilepton, params, idcut=None):
        """
        Handy interface for dilepton filter
        """

        _Code = (
            "(ID=='J/psi(1S)') & "
            "(PT > %(DiLeptonPT)s *MeV) & "
            "(MM < %(UpperMass)s *MeV) & "
            "(MINTREE(ABSID<14,PT) > %(LeptonPT)s *MeV) & "
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "
            "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "
            "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params
        )

        # add additional cut on PID if requested
        if idcut:
            _Code += " & " + idcut

        _Filter = FilterDesktop(Code=_Code)

        return Selection(name, Algorithm=_Filter, RequiredSelections=[dilepton])

    #####################################################

    def _filterDiLeptonTight(self, name, dilepton, params, idcut=None):
        """
        Handy interface for dilepton filter
        """

        _Code = (
            "(ID=='J/psi(1S)') & "
            "(PT > %(DiLeptonPT)s *MeV) & "
            "(MM < %(UpperMass)s *MeV) & "
            "(MINTREE(ABSID<14,PT) > %(LeptonPTTight)s *MeV) & "
            "(MINTREE(ABSID<14,MIPCHI2DV(PRIMARY)) > %(LeptonIPCHI2)s) & "
            "(VFASPF(VCHI2/VDOF) < 9) & (BPVVDCHI2 > %(DiLeptonFDCHI2)s) & "
            "(MIPCHI2DV(PRIMARY) > %(DiLeptonIPCHI2)s)" % params
        )

        # add additional cut on PID if requested
        if idcut:
            _Code += " & " + idcut

        _Filter = FilterDesktop(Code=_Code)

        return Selection(name, Algorithm=_Filter, RequiredSelections=[dilepton])

    #####################################################

    def _makeMuE(self, name, params, electronid=None, muonid=None, samesign=False):
        """
        Makes MuE combinations
        """

        from StandardParticles import StdAllNoPIDsMuons as Muons
        from StandardParticles import StdAllNoPIDsElectrons as Electrons

        _DecayDescriptor = "[J/psi(1S) -> mu+ e-]cc"
        if samesign:
            _DecayDescriptor = "[J/psi(1S) -> mu+ e+]cc"

        _MassCut = "(AM > 100*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = (
            "(PT > %(LeptonPT)s) & " "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params
        )

        _Combine = CombineParticles(
            DecayDescriptor=_DecayDescriptor,
            CombinationCut=_MassCut,
            MotherCut=_MotherCut,
        )

        _MuonCut = _DaughtersCut
        _ElectronCut = _DaughtersCut

        if muonid:
            _MuonCut += "&" + muonid
        if electronid:
            _ElectronCut += "&" + electronid

        _Combine.DaughtersCuts = {"mu+": _MuonCut, "e+": _ElectronCut}

        return Selection(
            name, Algorithm=_Combine, RequiredSelections=[Muons, Electrons]
        )

    ####################################################

    def _makeMuMuSS(self, name, params, muonid=None):
        """
        Makes MuMu same sign combinations
        """
        from StandardParticles import StdAllNoPIDsMuons as Muons

        _DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"

        _MassCut = "(AM > 100*MeV)"

        _MotherCut = "(VFASPF(VCHI2/VDOF) < 9)"

        _DaughtersCut = (
            "(PT > %(LeptonPT)s) & " "(MIPCHI2DV(PRIMARY) > %(LeptonIPCHI2)s)" % params
        )

        _Combine = CombineParticles(
            DecayDescriptor=_DecayDescriptor,
            CombinationCut=_MassCut,
            MotherCut=_MotherCut,
        )

        _MuonCut = _DaughtersCut

        if muonid:
            _MuonCut += "&" + muonid

        _Combine.DaughtersCuts = {
            "mu+": _MuonCut,
        }

        return Selection(name, Algorithm=_Combine, RequiredSelections=[Muons])

    ####################################################

    def _makeEESS(self, name, params, electronid=None):
        """
        Makes EE same-sign combinations
        """
        from Configurables import DiElectronMaker, ProtoParticleCALOFilter
        from CommonParticles.Utils import trackSelector
        from GaudiKernel.SystemOfUnits import MeV

        ee = DiElectronMaker("DiElectronsSS" + name)
        ee.Particle = "J/psi(1S)"
        # ee.DecayDescriptor = "[J/psi(1S) -> e+ e+]cc"
        selector = trackSelector(ee, trackTypes=["Long"])

        ee.addTool(ProtoParticleCALOFilter("Electron"))
        ee.Electron.Selection = ["RequiresDet='CALO'"]
        ee.DiElectronMassMin = 0.0 * MeV
        ee.DiElectronMassMax = 5000.0 * MeV
        ee.DiElectronPtMin = 200.0 * MeV
        ee.OppositeSign = 0

        if electronid:
            ee.Electron.Selection = [
                "RequiresDet='CALO' CombDLL(e-pi)>'%(PIDe)s'" % params
            ]

        return Selection(name + "eeSelection", Algorithm=ee)

    ####################################################

    def _makeB2LLX(
        self, name, dilepton, hadrons, params, masscut="(ADAMASS('B+')< 1500 *MeV"
    ):
        """
        CombineParticles / Selection for the B
        """

        _Decays = [
            "[ B+ -> J/psi(1S) K+ ]cc",
            " B0 -> J/psi(1S) KS0 ",
            "[ Lambda_b0 -> J/psi(1S) Lambda0 ]cc",
        ]

        _Cut = (
            "((VFASPF(VCHI2/VDOF) < %(BVertexCHI2)s) "
            "& (BPVIPCHI2() < %(BIPCHI2)s) "
            "& (BPVDIRA > %(BDIRA)s) "
            "& (BPVVDCHI2 > %(BFlightCHI2)s))" % params
        )

        _Combine = CombineParticles(
            DecayDescriptors=_Decays, CombinationCut=masscut, MotherCut=_Cut
        )

        _Merge = MergedSelection("Merge" + name, RequiredSelections=hadrons)

        return Selection(
            name, Algorithm=_Combine, RequiredSelections=[dilepton, _Merge]
        )


####################################################
