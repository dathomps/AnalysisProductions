# FILE CONTAINING ALL DECAY DESCRIPTORS

dict_rec = {
    "Lb2JpsiL_mm": "[Lambda_b0 ->   ^(J/psi(1S) -> ^mu+  ^mu-)   ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Lb2JpsiL_ee": "[Lambda_b0 ->   ^(J/psi(1S) -> ^e+  ^e- )    ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Lb2Lmm_SS": "[Lambda_b0 ->   ^(J/psi(1S) -> ^mu+  ^mu+)   ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Lb2Lee_SS": "[Lambda_b0 ->   ^(J/psi(1S) -> ^e+  ^e+ )    ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Lb2Lemu": "[Lambda_b0 -> [ ^(J/psi(1S) -> ^e+  ^mu-)]CC ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Lb2Lemu_SS": "[Lambda_b0 ->  [ ^(J/psi(1S) -> ^e+  ^mu+)]CC ^(Lambda0 -> ^p+ ^pi-)]CC",
    "Bd2JpsiKs_mm": "        B0 ->   ^(J/psi(1S) -> ^mu+  ^mu-)   ^(KS0 -> ^pi+ ^pi-)   ",
    "Bd2JpsiKs_ee": "        B0 ->   ^(J/psi(1S) -> ^e+  ^e- )    ^(KS0 -> ^pi+ ^pi-)   ",
    "Bd2Ksmm_SS": "        B0 ->   ^(J/psi(1S) -> ^mu+  ^mu+)   ^(KS0 -> ^pi+ ^pi-)   ",
    "Bd2Ksee_SS": "        B0 ->   ^(J/psi(1S) -> ^e+  ^e+ )    ^(KS0 -> ^pi+ ^pi-)   ",
    "Bd2Ksemu   ": "        B0 -> [ ^(J/psi(1S) -> ^e+  ^mu-)]CC ^(KS0 -> ^pi+ ^pi-)   ",
    "Bd2Ksemu_SS": "        B0 -> [ ^(J/psi(1S) -> ^e+  ^mu+)]CC ^(KS0 -> ^pi+ ^pi-)   ",
    "Bu2JpsiK_mm": "[       B+ ->   ^(J/psi(1S) -> ^mu+  ^mu-)   ^K+                   ]CC",
    "Bu2JpsiK_ee": "[       B+ ->   ^(J/psi(1S) -> ^e+  ^e- )    ^K+                   ]CC",
    "Bu2Kmm_SS": "[       B+ ->   ^(J/psi(1S) -> ^mu+  ^mu+)   ^K+                   ]CC",
    "Bu2Kee_SS": "[       B+ ->   ^(J/psi(1S) -> ^e+  ^e+ )    ^K+                   ]CC",
    "Bu2Kemu   ": "[       B+ -> [ ^(J/psi(1S) -> ^e+  ^mu-)]CC ^K+                   ]CC",
    "Bu2Kemu_SS": "[       B+ -> [ ^(J/psi(1S) -> ^e+  ^mu+)]CC ^K+                   ]CC",

    "Lb2JpsiL_ee_upstream": "[Lambda_b0 ->   ^(J/psi(1S) -> ^e+  ^e- )    ^(Lambda0 -> ^p+ ^pi-)]CC",

}

dict_mc = {
    "Lb2LJpsmm": "[Lambda_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2LJpsee": "[Lambda_b0 =>  ^(J/psi(1S) => ^e+  ^e- )    ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2LPsimm": "[Lambda_b0 =>  ^(  psi(2S) => ^mu+  ^mu-)    ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2LPsiee": "[Lambda_b0 =>  ^(  psi(2S) => ^e+  ^e- )    ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2Lee": "[Lambda_b0 =>                 ^e+  ^e-      ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2Lmm": "[Lambda_b0 =>                 ^mu+  ^mu-     ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2Lem": "([Lambda_b0 =>                 ^e+  ^mu-      ^(Lambda0 => ^p+ ^pi-)]CC) || ([Lambda_b0 =>  ^e-  ^mu+ ^(Lambda0 => ^p+ ^pi-)]CC)",
    "Bd2Ksmm": "    [B0]cc =>                 ^mu+  ^mu-     ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2Ksee": "    [B0]cc =>                 ^e+  ^e-      ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2KsJpsmm": "    [B0]cc =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2KsJpsee": "    [B0]cc =>  ^(J/psi(1S) => ^e+  ^e- )    ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2KsPsimm": "    [B0]cc =>  ^(  psi(2S) => ^mu+  ^mu-)    ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2KsPsiee": "    [B0]cc =>  ^(  psi(2S) => ^e+  ^e- )    ^(KS0 => ^pi+ ^pi-)   ",
    "Bd2Ksemu": "    [B0]cc =>                  ^e+  ^mu-      ^(KS0 => ^pi+ ^pi-)  ||      [B0]cc =>  ^e-  ^mu+ ^(KS0 => ^pi+ ^pi-)    ",
    "Bu2JpsiK_mm": "[       B+ =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^K+                   ]CC",
    "Bu2JpsiK_ee": "[       B+ =>  ^(J/psi(1S) => ^e+  ^e- )    ^K+                   ]CC",
    "Bu2PsiK_mm": "[       B+ =>  ^(  psi(2S) => ^mu+  ^mu-)    ^K+                   ]CC",
    "Bu2PsiK_ee": "[       B+ =>  ^(  psi(2S) => ^e+  ^e- )    ^K+                   ]CC",
    "Bu2Kemu": "([      B+ =>                 ^e+  ^mu-     ^K+                   ]CC) || ([       B+ =>  ^e-  ^mu+ ^K+                   ]CC)",
    "Lb2Lcmunu_L0e": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^nu_e ^e+   ) ^mu- ^nu_mu~]CC",
    "Lb2Lcmunu_L0mu": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^nu_mu ^mu+ ) ^mu- ^nu_mu~]CC",
    "Lb2Lcmunu_L0pi": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^pi+        ) ^mu- ^nu_mu~]CC",
    "Lb2Lcenu_L0mu": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^nu_mu ^mu+ ) ^e- ^nu_e~]CC",
    "Lb2Lcenu_L0e": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^nu_e  ^e+  ) ^e- ^nu_e~]CC",
    "Lb2Lcenu_L0pi": "[Lambda_b0 => ^(Lambda_c+ => ^(Lambda0 => ^p+ ^pi-) ^pi+        ) ^e- ^nu_e~]CC",
    "Lb2Lpipi": "[Lambda_b0 => ^(Lambda0 => ^p+ ^pi-) ^pi+ ^pi- ]CC",
    "Lb2LKpi":  "([Lambda_b0 => ^(Lambda0 => ^p+ ^pi-) ^K+ ^pi- ]CC) || ([Lambda_b0 => ^(Lambda0 => ^p+ ^pi-) ^K- ^pi+ ]CC) ",
    "Lb2LKK":   "[Lambda_b0 => ^(Lambda0 => ^p+ ^pi-) ^K+ ^K- ]CC",    
    "Lb2Sigmapipi": "[Lambda_b0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma ) ^pi+ ^pi- ]CC",
    "Lb2SigmaKpi":  "[Lambda_b0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma ) ^K+ ^pi- ]CC",
    "Lb2SigmaKK":   "[Lambda_b0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma ) ^K+ ^K- ]CC",
    "Xib2XiJpsmm": "[Xi_b- =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib2XiJpsee": "[Xi_b- =>  ^(J/psi(1S) => ^e+  ^e- )  ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib2XiPsimm": "[Xi_b- =>  ^(  psi(2S) => ^mu+  ^mu-) ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib2XiPsiee": "[Xi_b- =>  ^(  psi(2S) => ^e+  ^e- )  ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib2Xiee":    "[Xi_b- =>                 ^e+  ^e-    ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib2Ximm":    "[Xi_b- =>                 ^mu+  ^mu-  ^(Xi- =>  ^(Lambda0 => ^p+ ^pi-) ^pi-) ]CC",
    "Xib02Xi0Jpsmm": "[Xi_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Xib02Xi0Jpsee": "[Xi_b0 =>  ^(J/psi(1S) => ^e+  ^e- )  ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Xib02Xi0Psimm": "[Xi_b0 =>  ^(  psi(2S) => ^mu+  ^mu-) ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Xib02Xi0Psiee": "[Xi_b0 =>  ^(  psi(2S) => ^e+  ^e- )  ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Xib02Xi0ee":    "[Xi_b0 =>                 ^e+  ^e-    ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Xib02Xi0mm":    "[Xi_b0 =>                 ^mu+  ^mu-  ^(Xi0 =>  ^(Lambda0 => ^p+ ^pi-) ^pi0) ]CC",
    "Omegab2OmegaJpsmm": "[Xi_b- =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Omegab2OmegaJpsee": "[Xi_b- =>  ^(J/psi(1S) => ^e+  ^e- )  ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Omegab2OmegaPsimm": "[Xi_b- =>  ^(  psi(2S) => ^mu+  ^mu-) ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Omegab2OmegaPsiee": "[Xi_b- =>  ^(  psi(2S) => ^e+  ^e- )  ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Omegab2Omegaee":    "[Xi_b- =>                 ^e+  ^e-    ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Omegab2Omegamm":    "[Xi_b- =>                 ^mu+  ^mu-  ^(Omega- =>  ^(Lambda0 => ^p+ ^pi-) ^K-) ]CC",
    "Lb2JPsiX_mm" :   "([Lambda_b0 => ^(J/psi(1S) => ^mu+ ^mu-  ) X]CC) || ([Lambda_b0 => ^(psi(2S) => ^mu+ ^mu-) X]CC)",
    "Lb2JPsiX_ee" :   "([Lambda_b0 => ^(J/psi(1S) => ^e+ ^e-  ) X]CC) || ([Lambda_b0 => ^(psi(2S) => ^e+ ^e-)  X]CC)",
    "Bd2JPsiX_mm" :   "([B0 => ^(J/psi(1S) => ^mu+ ^mu-  ) X]CC) || ([B0 => ^(psi(2S) => ^mu+ ^mu-) X]CC)",
    "Bd2JPsiX_ee" :   "([B0 => ^(J/psi(1S) => ^e+ ^e-  ) X]CC) || (  [B0 => ^(psi(2S) => ^e+ ^e-)  X]CC)",
    "Bu2JPsiX_mm" :   "([B+ => ^(J/psi(1S) => ^mu+ ^mu-  ) X]CC) || ([B+ => ^(psi(2S) => ^mu+ ^mu-) X]CC)",
    "Bu2JPsiX_ee" :   "([B+ => ^(J/psi(1S) => ^e+ ^e-  ) X]CC) || (  [B+ => ^(psi(2S) => ^e+ ^e-)  X]CC)",
    "Bs2JPsiX_mm" :   "([B_s0 => ^(J/psi(1S) => ^mu+ ^mu-  ) X]CC) || ([B_s0 => ^(psi(2S) => ^mu+ ^mu-) X]CC)",
    "Bs2JPsiX_ee" :   "([B_s0 => ^(J/psi(1S) => ^e+ ^e-    ) X]CC) || (  [B_s0 => ^(psi(2S) => ^e+ ^e-)  X]CC)",
    "Lb2JPsiL1405_mm": "[Lambda_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Lambda(1405)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2JPsiL1520_mm": "[Lambda_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2JPsiL1605_mm": "[Lambda_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-) ^(Lambda(1600)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",

    "Lb2JPsiL1520_ee": "[Lambda_b0 =>  ^(J/psi(1S) => ^e+ ^e-   ) ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2PsiL1520_mm":  "[Lambda_b0 =>  ^(  psi(2S) => ^mu+  ^mu-) ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2PsiL1520_ee":  "[Lambda_b0 =>  ^(  psi(2S) => ^e+ ^e-   ) ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2L1520mm":      "[Lambda_b0 =>                 ^mu+  ^mu- ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",
    "Lb2L1520ee":      "[Lambda_b0 =>                 ^e+   ^e-  ^(Lambda(1520)0 => ^(Sigma0 -> ^(Lambda0 => ^p+ ^pi-) ^gamma )  ^pi0) ]CC",

    "Lb2LPsimm_JpsiPiPi" : "[Lambda_b0 =>  ^(psi(2S) => ^(J/psi(1S) =>  ^mu+  ^mu-) ^pi+ ^pi-)   ^(Lambda0 => ^p+ ^pi-)]CC",
    "Lb2LPsiee_JpsiPiPi" : "[Lambda_b0 =>  ^(psi(2S) => ^(J/psi(1S) =>  ^e+   ^e- ) ^pi+ ^pi-)   ^(Lambda0 => ^p+ ^pi-)]CC",


}

# Define samples without V0 decay:
# No change for B+
# Replace pi+ pi- term for Bd
# Replace p pi- term for Lb
dict_mc_noV0dec = {
    k: (
        v
        if "Bu" in k
        else v.replace("=> ^pi+ ^pi-)", "").replace("(KS0", "KS0")
        if "Bd" in k
        else v.replace("=> ^p+ ^pi-)", "").replace("(Lambda0", "Lambda0")
    )
    for (k, v) in dict_mc.items()
}

#print(dict_mc_noV0dec)
#for keys, vals in dict_mc_noV0dec.iteritems():
    #print(keys, vals)

dict_mc_branches = {
    "Lb2Lpipi": {
        "Lb" : "[Lambda_b0 => (Lambda0 => p+ pi-) pi+ pi- ]CC",
        "L0" : "[Lambda_b0 => ^(Lambda0 => p+ pi-) pi+ pi- ]CC",
        "P"  : "[Lambda_b0 => (Lambda0 => ^p+ pi-) pi+ pi- ]CC",
        "Pi" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) pi+ pi- ]CC",
        "L1" : "[Lambda_b0 => (Lambda0 => p+ pi-) ^pi+ pi- ]CC",
        "L2" : "[Lambda_b0 => (Lambda0 => p+ pi-) pi+^ pi- ]CC",
        },
    "Lb2LKpi": {
        "Lb" : "([Lambda_b0 => (Lambda0 => p+ pi-) K+ pi- ]CC) || ([Lambda_b0 => (Lambda0 => p+ pi-) K- pi+ ]CC) ",
        "L0" : "([Lambda_b0 => ^(Lambda0 => p+ pi-) K+ pi- ]CC) || ([Lambda_b0 => ^(Lambda0 => p+ pi-) K- pi+ ]CC) ",
        "P"  : "([Lambda_b0 => (Lambda0 => ^p+ pi-) K+ pi- ]CC) || ([Lambda_b0 => (Lambda0 => ^p+ pi-) K- pi+ ]CC) ",
        "Pi" : "([Lambda_b0 => (Lambda0 => p+ ^pi-) K+ pi- ]CC) || ([Lambda_b0 => (Lambda0 => p+ ^pi-) K- pi+ ]CC) ",
        "L1" : "([Lambda_b0 => (Lambda0 => p+ pi-) ^K+ pi- ]CC) || ([Lambda_b0 => (Lambda0 => p+ pi-) K- ^pi+ ]CC) ",
        "L2" : "([Lambda_b0 => (Lambda0 => p+ pi-) K+ ^pi- ]CC) || ([Lambda_b0 => (Lambda0 => p+ pi-) ^K- pi+ ]CC) ",
        },
    "Lb2LKK" : {
        "Lb" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+ K- ]CC",
        "L0" : "[Lambda_b0 => ^(Lambda0 => p+ pi-) K+ K- ]CC",
        "P"  : "[Lambda_b0 => (Lambda0 => ^p+ pi-) K+ K- ]CC",
        "Pi" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) K+ K- ]CC",
        "L1" : "[Lambda_b0 => (Lambda0 => p+ pi-) ^K+ K- ]CC",
        "L2" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+^ K- ]CC",
    },
    "Lb2Sigmapipi": {
        "Lb" : "[Lambda_b0 => (Lambda0 => p+ pi-) pi+ pi- ]CC",
        "L0" : "[Lambda_b0 => ^(Lambda0 => p+ pi-) pi+ pi- ]CC",
        "P"  : "[Lambda_b0 => (Lambda0 => ^p+ pi-) pi+ pi- ]CC",
        "Pi" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) pi+ pi- ]CC",
        "L1" : "[Lambda_b0 => (Lambda0 => p+ pi-) ^pi+ pi- ]CC",
        "L2" : "[Lambda_b0 => (Lambda0 => p+ pi-) pi+^ pi- ]CC",
        },
    "Lb2SigmaKpi": {
        "Lb" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+ pi- ]CC",
        "L0" : "[Lambda_b0 => ^(Lambda0 => p+ pi-) K+ pi- ]CC",
        "P"  : "[Lambda_b0 => (Lambda0 => ^p+ pi-) K+ pi- ]CC",
        "Pi" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) K+ pi- ]CC",
        "L1" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) ^K+ pi- ]CC",
        "L2" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+ ^pi- ]CC",
        },
    "Lb2SigmaKK" : {
        "Lb" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+ K- ]CC",
        "L0" : "[Lambda_b0 => ^(Lambda0 => p+ pi-) K+ K- ]CC",
        "P"  : "[Lambda_b0 => (Lambda0 => ^p+ pi-) K+ K- ]CC",
        "Pi" : "[Lambda_b0 => (Lambda0 => p+ ^pi-) K+ K- ]CC",
        "L1" : "[Lambda_b0 => (Lambda0 => p+ pi-) ^K+ K- ]CC",
        "L2" : "[Lambda_b0 => (Lambda0 => p+ pi-) K+^ K- ]CC",
    },

    "Lb2Lcmunu_L0e": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) nu_e e+   ) mu- nu_mu~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) nu_e e+   ) mu- nu_mu~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) nu_e e+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e ^e+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcmunu_L0mu": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) nu_mu mu+   ) mu- nu_mu~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) nu_mu mu+   ) mu- nu_mu~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) nu_mu mu+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu ^mu+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcmunu_L0pi": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) pi+   ) mu- nu_mu~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) pi+   ) mu- nu_mu~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) pi+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) ^pi+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcenu_L0e": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) nu_e e+   ) e- nu_e~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) nu_e e+   ) e- nu_e~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) nu_e e+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e ^e+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_e e+   ) ^e- nu_e~]CC",
    },
    "Lb2Lcenu_L0mu": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) nu_mu mu+   ) e- nu_e~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) nu_mu mu+   ) e- nu_e~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) nu_mu mu+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu ^mu+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) nu_mu mu+   ) ^e- nu_e~]CC",
    },
    "Lb2Lcenu_L0pi": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^(Lambda0 => p+ pi-) pi+   ) e- nu_e~]CC",
        "P" : "[Lambda_b0 => (Lambda_c+ => (Lambda0 => ^p+ pi-) pi+   ) e- nu_e~]CC",
        "Pi": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ ^pi-) pi+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) ^pi+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => (Lambda0 => p+ pi-) pi+   ) ^e- nu_e~]CC",
    },
    "Xib2XiJpsmm": {
       "Xib": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => mu+  ^mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Xib2XiJpsee": {
       "Xib": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>  (J/psi(1S) => e+  e-) ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => e+  ^e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Xib2XiPsimm": {
       "Xib": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^mu+  mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => mu+  ^mu-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Xib2XiPsiee": {
       "Xib": "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>  (  psi(2S) => e+  e-) ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^e+  e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => e+  ^e-) (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Xib2Ximm": {
       "Xib": "[Xi_b- =>  mu+ mu- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>  mu+ mu- ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>  mu+ mu- (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>  mu+ mu- (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>  mu+ mu- (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  mu+ mu- (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "L1" : "[Xi_b- =>  ^mu+ mu- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>  mu+ ^mu- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Xib2Xiee": {
       "Xib": "[Xi_b- =>   e+  e- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "Xi" : "[Xi_b- =>   e+  e- ^(Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L0" : "[Xi_b- =>   e+  e- (Xi- =>  ^(Lambda0 => p+ pi-) pi-) ]CC",
       "P"  : "[Xi_b- =>   e+  e- (Xi- =>  (Lambda0 => ^p+ pi-) pi-) ]CC",
       "Pi" : "[Xi_b- =>   e+  e- (Xi- =>  (Lambda0 => p+ ^pi-) pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>   e+  e- (Xi- =>  (Lambda0 => p+ pi-) ^pi-) ]CC",
       "L1" : "[Xi_b- =>   ^e+  e- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       "L2" : "[Xi_b- =>   e+  ^e- (Xi- =>  (Lambda0 => p+ pi-) pi-) ]CC",
       },
    "Omegab2OmegaJpsmm": {
       "Ob" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
    "Omega" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => mu+  ^mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },
    "Omegab2OmegaJpsee": {
       "Ob": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
    "Omega" : "[Xi_b- =>  (J/psi(1S) => e+  e-) ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K"  : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => e+  ^e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },
    "Omegab2OmegaPsimm": {
       "Ob": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
    "Omega" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K"  : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^mu+  mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => mu+  ^mu-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },
    "Omegab2OmegaPsiee": {
       "Ob": "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
   "Omega" : "[Xi_b- =>  (  psi(2S) => e+  e-) ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K"  : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^e+  e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => e+  ^e-) (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },
    "Omegab2Omegamm": {
       "Ob": "[Xi_b- =>  mu+ mu- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
   "Omega" : "[Xi_b- =>  mu+ mu- ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>  mu+ mu- (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>  mu+ mu- (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>  mu+ mu- (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K"  : "[Xi_b- =>  mu+ mu- (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "L1" : "[Xi_b- =>  ^mu+ mu- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>  mu+ ^mu- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },
    "Omegab2Omegaee": {
       "Ob": "[Xi_b- =>   e+  e- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
   "Omega" : "[Xi_b- =>   e+  e- ^(Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L0" : "[Xi_b- =>   e+  e- (Omega- =>  ^(Lambda0 => p+ pi-) K-) ]CC",
       "P"  : "[Xi_b- =>   e+  e- (Omega- =>  (Lambda0 => ^p+ pi-) K-) ]CC",
       "Pi" : "[Xi_b- =>   e+  e- (Omega- =>  (Lambda0 => p+ ^pi-) K-) ]CC",
       "K"  : "[Xi_b- =>   e+  e- (Omega- =>  (Lambda0 => p+ pi-) ^K-) ]CC",
       "L1" : "[Xi_b- =>   ^e+  e- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       "L2" : "[Xi_b- =>   e+  ^e- (Omega- =>  (Lambda0 => p+ pi-) K-) ]CC",
       },

    "Lb2JPsiX_mm" : { 
        "Lb": "([Lambda_b0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => mu+ mu-)  X]CC)",
       "JPs": "([Lambda_b0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || (^[Lambda_b0 => (psi(2S) => mu+ mu-)  X]CC)",
        "L1": "([Lambda_b0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => ^mu+ mu-)  X]CC)",
        "L2": "([Lambda_b0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => mu+ ^mu-) X]CC)",
        },
    "Lb2JPsiX_ee" : {
        "Lb": "([Lambda_b0 => (J/psi(1S) => e+ e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => e+ e-)  X]CC)",
       "JPs": "([Lambda_b0 => ^(J/psi(1S) => e+ e-  ) X]CC) || (^[Lambda_b0 => (psi(2S) => e+ e-)  X]CC)",
        "L1": "([Lambda_b0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => ^e+ e-)  X]CC)",
        "L2": "([Lambda_b0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => e+ ^e-)  X]CC)",
        },
    "Bd2JPsiX_mm" : {
        "B": "([B0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B0 => (psi(2S) => mu+ mu-)  X]CC)",
      "JPs": "([B0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B0 => ^(psi(2S) => mu+ mu-)  X]CC)",
       "L1": "([B0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B0 => (psi(2S) => ^mu+ mu-)  X]CC)",
       "L2": "([B0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B0 => (psi(2S) => mu+ ^mu-) X]CC)",
        },
    "Bd2JPsiX_ee" : {  
        "B" : "([B0 => (J/psi(1S) => e+ e-  ) X]CC) || (  [B0 => (psi(2S) => e+ e-)  X]CC)",
      "JPs": "([B0 => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B0 => ^(psi(2S) => e+ e-)  X]CC)",
       "L1": "([B0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B0 => (psi(2S) => ^e+ e-)  X]CC)",
       "L2": "([B0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B0 => (psi(2S) => e+ ^e-)  X]CC)",
        },
    "Bu2JPsiX_mm" : { 
        "B" : "([B+ => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B+ => (psi(2S) => mu+ mu-)  X]CC)",
      "JPs": "([B+ => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B+ => ^(psi(2S) => mu+ mu-)  X]CC)",
       "L1": "([B+ => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B+ => (psi(2S) => ^mu+ mu-)  X]CC)",
       "L2": "([B+ => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B+ => (psi(2S) => mu+ ^mu-) X]CC)",
        },
    "Bu2JPsiX_ee" : {  
        "B" : "([B+ => (J/psi(1S) => e+ e-  ) X]CC) || (  [B+ => (psi(2S) => e+ e-)  X]CC)",
      "JPs": "([B+ => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B+ => ^(psi(2S) => e+ e-)  X]CC)",
       "L1": "([B+ => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B+ => (psi(2S) => ^e+ e-)  X]CC)",
       "L2": "([B+ => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B+ => (psi(2S) => e+ ^e-)  X]CC)",
        },
    "Bs2JPsiX_mm" : {  
        "B" : "([B_s0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B_s0 => (psi(2S) => mu+ mu-)  X]CC)",
      "JPs": "([B_s0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B_s0 => ^(psi(2S) => mu+ mu-)  X]CC)",
       "L1": "([B_s0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B_s0 => (psi(2S) => ^mu+ mu-)  X]CC)",
       "L2": "([B_s0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B_s0 => (psi(2S) => mu+ ^mu-) X]CC)",
        },
    "Bs2JPsiX_ee" : {  
        "B" : "([B_s0 => (J/psi(1S) => e+ e-    ) X]CC) || (  [B_s0 => (psi(2S) => e+ e-)  X]CC)",
      "JPs": "([B_s0 => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B_s0 => ^(psi(2S) => e+ e-)  X]CC)",
       "L1": "([B_s0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B_s0 => (psi(2S) => ^e+ e-)  X]CC)",
       "L2": "([B_s0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B_s0 => (psi(2S) => e+ ^e-)  X]CC)",
        },
    "Lb2JPsiL1405_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1405)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },
    "Lb2JPsiL1520_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },
    "Lb2JPsiL1605_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1600)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },


    "Lb2JPsiL1520_ee": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => e+ ^e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },

    "Lb2PsiL1520_mm": {
       "Lb" : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => ^mu+  mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => mu+ ^mu-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },

    "Lb2PsiL1520_ee": {
       "Lb" : "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (psi(2S) => e+  e-) ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => ^e+  e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => e+ ^e-) (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },


    "Lb2L1520mm": {
       "Lb" : "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>   mu+  mu- ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>   mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>   ^mu+  mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>   mu+ ^mu- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },

    "Lb2L1520ee": {
       "Lb" : "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>   e+  e- ^(Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => ^(Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> ^(Lambda0 => p+ pi-) gamma )  pi0) ]CC", 
       "P"  : "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => ^p+ pi-) gamma )  pi0) ]CC", 
       "Pi" : "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ ^pi-) gamma )  pi0) ]CC",
       "Pi0": "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>   e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) ^gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>   ^e+  e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>   e+ ^e- (Lambda(1520)0 => (Sigma0 -> (Lambda0 => p+ pi-) gamma )  pi0) ]CC",
      },


    "Lb2LPsimm_JpsiPiPi" : {
        "Lb": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L0" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)  ^(Lambda0 => p+ pi-)]CC",
       "P"  : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   (Lambda0 => ^p+ pi-)]CC",
       "Pi" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   (Lambda0 => p+ ^pi-)]CC",
       "Psi": "[Lambda_b0 =>  ^(psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "Pi1": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) ^pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "Pi2": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ ^pi-)   (Lambda0 => p+ pi-)]CC",
       "JPs": "[Lambda_b0 =>  (psi(2S) => ^(J/psi(1S) =>  mu+  mu-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  ^mu+  mu-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  ^mu-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
      },
    "Lb2LPsiee_JpsiPiPi" : {
        "Lb": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L0" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)  ^(Lambda0 => p+ pi-)]CC",
       "P"  : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   (Lambda0 => ^p+ pi-)]CC",
       "Pi" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   (Lambda0 => p+ ^pi-)]CC",
       "Psi": "[Lambda_b0 =>  ^(psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "Pi1": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) ^pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "Pi2": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ ^pi-)   (Lambda0 => p+ pi-)]CC",
       "JPs": "[Lambda_b0 =>  (psi(2S) => ^(J/psi(1S) =>  e+  e-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  ^e+  e-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  ^e-) pi+ pi-)   (Lambda0 => p+ pi-)]CC",

  },
}

dict_mc_branches_noV0dec = {
    "Lb2Lpipi": {
        "Lb" : "[Lambda_b0 => Lambda0 pi+ pi- ]CC",
        "L0" : "[Lambda_b0 => ^Lambda0 pi+ pi- ]CC",
        "L1" : "[Lambda_b0 => Lambda0 ^pi+ pi- ]CC",
        "L2" : "[Lambda_b0 => Lambda0 pi+ ^pi- ]CC",
        },
    "Lb2LKpi": {
        "Lb" : "[Lambda_b0 => Lambda0 K+ pi- ]CC",
        "L0" : "[Lambda_b0 => ^Lambda0 K+ pi- ]CC",
        "L1" : "[Lambda_b0 => Lambda0 ^K+ pi- ]CC",
        "L2" : "[Lambda_b0 => Lambda0 K+ ^pi- ]CC",
        },
    "Lb2LKpi": {
        "Lb" : "([Lambda_b0 => Lambda0  K+ pi- ]CC) || ([Lambda_b0 => Lambda0   K- pi+ ]CC) ",
        "L0" : "([Lambda_b0 => ^Lambda0 K+ pi- ]CC) || ([Lambda_b0 => ^Lambda0  K- pi+ ]CC) ",
        "L1" : "([Lambda_b0 => Lambda0 ^K+ pi- ]CC) || ([Lambda_b0 => Lambda0   K- ^pi+ ]CC) ",
        "L2" : "([Lambda_b0 => Lambda0 K+ ^pi- ]CC) || ([Lambda_b0 => Lambda0  ^K- pi+ ]CC) ",
        },
    "Lb2LKK" : {
        "Lb" : "[Lambda_b0 => Lambda0 K+ K- ]CC",
        "L0" : "[Lambda_b0 => ^Lambda0 K+ K- ]CC",
        "L1" : "[Lambda_b0 => Lambda0 ^K+ K- ]CC",
        "L2" : "[Lambda_b0 => Lambda0 K+^ K- ]CC",
    },
    "Lb2Lcmunu_L0e": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e e+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  nu_e e+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  nu_e e+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e ^e+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e e+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcmunu_L0mu": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu mu+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  nu_mu mu+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  nu_mu mu+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu ^mu+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu mu+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcmunu_L0pi": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  pi+   ) mu- nu_mu~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  pi+   ) mu- nu_mu~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  pi+   ) mu- nu_mu~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  ^pi+   ) mu- nu_mu~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  pi+   ) ^mu- nu_mu~]CC",
    },
    "Lb2Lcenu_L0e": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e e+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  nu_e e+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  nu_e e+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e ^e+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_e e+   ) ^e- nu_e~]CC",
    },
    "Lb2Lcenu_L0mu": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu mu+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  nu_mu mu+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  nu_mu mu+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu ^mu+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  nu_mu mu+   ) ^e- nu_e~]CC",
    },
    "Lb2Lcenu_L0pi": {
        "Lb": "[Lambda_b0 => (Lambda_c+ => Lambda0  pi+   ) e- nu_e~]CC",
        "Lc": "[Lambda_b0 => ^(Lambda_c+ => Lambda0  pi+   ) e- nu_e~]CC",
        "L0": "[Lambda_b0 => (Lambda_c+ => ^Lambda0  pi+   ) e- nu_e~]CC",
        "L1": "[Lambda_b0 => (Lambda_c+ => Lambda0  ^pi+   ) e- nu_e~]CC",
        "L2": "[Lambda_b0 => (Lambda_c+ => Lambda0  pi+   ) ^e- nu_e~]CC",
    },
    "Xib2XiJpsmm": {
       "Xib": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Xi- =>  Lambda0 ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => mu+  ^mu-) (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Xib2XiJpsee": {
       "Xib": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>  (J/psi(1S) => e+  e-) ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Xi- =>  Lambda0 ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => e+  ^e-) (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Xib2XiPsimm": {
       "Xib": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Xi- =>  Lambda0 ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^mu+  mu-) (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => mu+  ^mu-) (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Xib2XiPsiee": {
       "Xib": "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>  (  psi(2S) => e+  e-) ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  (  psi(2S) => e+  e-) (Xi- =>  Lambda0 ^pi-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^e+  e-) (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => e+  ^e-) (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Xib2Ximm": {
       "Xib": "[Xi_b- =>  mu+ mu- (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>  mu+ mu- ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>  mu+ mu- (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>  mu+ mu- (Xi- =>  Lambda0 ^pi-) ]CC",
       "L1" : "[Xi_b- =>  ^mu+ mu- (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>  mu+ ^mu- (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Xib2Xiee": {
       "Xib": "[Xi_b- =>   e+  e- (Xi- =>  Lambda0 pi-) ]CC",
       "Xi" : "[Xi_b- =>   e+  e- ^(Xi- =>  Lambda0 pi-) ]CC",
       "L0" : "[Xi_b- =>   e+  e- (Xi- =>  ^Lambda0 pi-) ]CC",
     "Pi_Xi": "[Xi_b- =>   e+  e- (Xi- =>  Lambda0 ^pi-) ]CC",
       "L1" : "[Xi_b- =>   ^e+  e- (Xi- =>  Lambda0 pi-) ]CC",
       "L2" : "[Xi_b- =>   e+  ^e- (Xi- =>  Lambda0 pi-) ]CC",
       },
    "Omegab2OmegaJpsmm": {
       "Ob" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  Lambda0 K-) ]CC",
    "Omega" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) ^(Omega- =>  Lambda0  K-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  ^Lambda0  K-) ]CC",
       "K": "[Xi_b- =>  (J/psi(1S) => mu+  mu-) (Omega- =>  Lambda0  ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => mu+  mu-) (Omega- =>  Lambda0  K-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^mu+  mu-) (Omega- =>  Lambda0  K-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => mu+  ^mu-) (Omega- =>  Lambda0  K-) ]CC",
       },
    "Omegab2OmegaJpsee": {
       "Ob": "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  Lambda0 K-) ]CC",
    "Omega" : "[Xi_b- =>  (J/psi(1S) => e+  e-) ^(Omega- =>  Lambda0 K-) ]CC",
       "L0" : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  ^Lambda0 K-) ]CC",
       "K"  : "[Xi_b- =>  (J/psi(1S) => e+  e-) (Omega- =>  Lambda0 ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(J/psi(1S) => e+  e-) (Omega- =>  Lambda0 K-) ]CC",
       "L1" : "[Xi_b- =>  (J/psi(1S) => ^e+  e-) (Omega- =>  Lambda0 K-) ]CC",
       "L2" : "[Xi_b- =>  (J/psi(1S) => e+  ^e-) (Omega- =>  Lambda0 K-) ]CC",
       },
    "Omegab2OmegaPsimm": {
       "Ob": "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  Lambda0  K-) ]CC",
    "Omega" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) ^(Omega- =>  Lambda0 K-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  ^Lambda0 K-) ]CC",
       "K"  : "[Xi_b- =>  (  psi(2S) => mu+  mu-) (Omega- =>  Lambda0 ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => mu+  mu-) (Omega- =>  Lambda0 K-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^mu+  mu-) (Omega- =>  Lambda0 K-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => mu+  ^mu-) (Omega- =>  Lambda0  K-) ]CC",
       },
    "Omegab2OmegaPsiee": {
       "Ob": "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  Lambda0  K-) ]CC",
   "Omega" : "[Xi_b- =>  (  psi(2S) => e+  e-) ^(Omega- =>  Lambda0  K-) ]CC",
       "L0" : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  ^Lambda0  K-) ]CC",
       "K"  : "[Xi_b- =>  (  psi(2S) => e+  e-) (Omega- =>  Lambda0  ^K-) ]CC",
       "JPs": "[Xi_b- =>  ^(  psi(2S) => e+  e-) (Omega- =>  Lambda0  K-) ]CC",
       "L1" : "[Xi_b- =>  (  psi(2S) => ^e+  e-) (Omega- =>  Lambda0  K-) ]CC",
       "L2" : "[Xi_b- =>  (  psi(2S) => e+  ^e-) (Omega- =>  Lambda0  K-) ]CC",
       },
    "Omegab2Omegamm": {
       "Ob": "[Xi_b- =>  mu+ mu- (Omega- =>  Lambda0  K-) ]CC",
   "Omega" : "[Xi_b- =>  mu+ mu- ^(Omega- =>  Lambda0  K-) ]CC",
       "L0" : "[Xi_b- =>  mu+ mu- (Omega- =>  ^Lambda0 K-) ]CC",
       "K"  : "[Xi_b- =>  mu+ mu- (Omega- =>  Lambda0 ^K-) ]CC",
       "L1" : "[Xi_b- =>  ^mu+ mu- (Omega- =>  Lambda0 K-) ]CC",
       "L2" : "[Xi_b- =>  mu+ ^mu- (Omega- =>  Lambda0 K-) ]CC",
       },
    "Omegab2Omegaee": {
       "Ob": "[Xi_b- =>   e+  e- (Omega- =>  Lambda0  K-) ]CC",
   "Omega" : "[Xi_b- =>   e+  e- ^(Omega- =>  Lambda0  K-) ]CC",
       "L0" : "[Xi_b- =>   e+  e- (Omega- =>  ^Lambda0  K-) ]CC",
       "K"  : "[Xi_b- =>   e+  e- (Omega- =>  Lambda0  ^K-) ]CC",
       "L1" : "[Xi_b- =>   ^e+  e- (Omega- =>  Lambda0  K-) ]CC",
       "L2" : "[Xi_b- =>   e+  ^e- (Omega- =>  Lambda0  K-) ]CC",
       },
    "Lb2JPsiX_mm" : { 
        "Lb": "([Lambda_b0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => mu+ mu- )  X]CC)",
       "JPs": "([Lambda_b0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || (^[Lambda_b0 => (psi(2S) => mu+ mu- )  X]CC)",
        "L1": "([Lambda_b0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => ^mu+ mu- )  X]CC)",
        "L2": "([Lambda_b0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([Lambda_b0 => (psi(2S) => mu+ ^mu- )   X]CC)",
        },
    "Lb2JPsiX_ee" : {
        "Lb": "([Lambda_b0 => (J/psi(1S) => e+ e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => e+ e- )  X]CC)",
       "JPs": "([Lambda_b0 => ^(J/psi(1S) => e+ e-  ) X]CC) || (^[Lambda_b0 => (psi(2S) => e+ e- )  X]CC)",
        "L1": "([Lambda_b0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => ^e+ e- )  X]CC)",
        "L2": "([Lambda_b0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([Lambda_b0 => (psi(2S) => e+ ^e- )  X]CC)",
        },
    "Bd2JPsiX_mm" : {
        "B": "([B0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B0 => (psi(2S) => mu+ mu- )  X]CC)",
      "JPs": "([B0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B0 => ^(psi(2S) => mu+ mu- )  X]CC)",
       "L1": "([B0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B0 => (psi(2S) => ^mu+ mu- )  X]CC)",
       "L2": "([B0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B0 => (psi(2S) => mu+ ^mu- )   X]CC)",
        },
    "Bd2JPsiX_ee" : {  
        "B" : "([B0 => (J/psi(1S) => e+ e-  ) X]CC) || (  [B0 => (psi(2S) => e+ e- )   X]CC)",
      "JPs": "([B0 => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B0 => ^(psi(2S) => e+ e- )  X]CC)",
       "L1": "([B0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B0 => (psi(2S) => ^e+ e- )  X]CC)",
       "L2": "([B0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B0 => (psi(2S) => e+ ^e- )  X]CC)",
        },
    "Bu2JPsiX_mm" : { 
        "B" : "([B+ => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B+ => (psi(2S) => mu+ mu- )  X]CC)",
      "JPs": "([B+ => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B+ => ^(psi(2S) => mu+ mu- )  X]CC)",
       "L1": "([B+ => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B+ => (psi(2S) => ^mu+ mu- )  X]CC)",
       "L2": "([B+ => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B+ => (psi(2S) => mu+ ^mu- )  X]CC)",
        },
    "Bu2JPsiX_ee" : {  
        "B" : "([B+ => (J/psi(1S) => e+ e-  ) X]CC) || (  [B+ => (psi(2S) => e+ e- )  X]CC)",
      "JPs": "([B+ => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B+ => ^(psi(2S) => e+ e- )  X]CC)",
       "L1": "([B+ => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B+ => (psi(2S) => ^e+ e- )  X]CC)",
       "L2": "([B+ => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B+ => (psi(2S) => e+ ^e- )  X]CC)",
        },
    "Bs2JPsiX_mm" : {  
        "B" : "([B_s0 => (J/psi(1S) => mu+ mu-  ) X]CC) || ([B_s0 => (psi(2S) => mu+ mu- )  X]CC)",
      "JPs": "([B_s0 => ^(J/psi(1S) => mu+ mu-  ) X]CC) || ([B_s0 => ^(psi(2S) => mu+ mu- )   X]CC)",
       "L1": "([B_s0 => (J/psi(1S) => ^mu+ mu-  ) X]CC) || ([B_s0 => (psi(2S) => ^mu+ mu- )   X]CC)",
       "L2": "([B_s0 => (J/psi(1S) => mu+ ^mu-  ) X]CC) || ([B_s0 => (psi(2S) => mu+ ^mu- )   X]CC)",
        },
    "Bs2JPsiX_ee" : {  
        "B" : "([B_s0 => (J/psi(1S) => e+ e-    ) X]CC) || (  [B_s0 => (psi(2S) => e+ e- )  X]CC)",
      "JPs": "([B_s0 => ^(J/psi(1S) => e+ e-  ) X]CC) || ([B_s0 => ^(psi(2S) => e+ e- )  X]CC)",
       "L1": "([B_s0 => (J/psi(1S) => ^e+ e-  ) X]CC) || ([B_s0 => (psi(2S) => ^e+ e- )  X]CC)",
       "L2": "([B_s0 => (J/psi(1S) => e+ ^e-  ) X]CC) || ([B_s0 => (psi(2S) => e+ ^e- )  X]CC)",
        },
    "Lb2JPsiL1405_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1405)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },
    "Lb2JPsiL1520_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },
    "Lb2JPsiL1605_mm": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) ^(Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => mu+  mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^mu+  mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => mu+ ^mu-) (Lambda(1600)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },


    "Lb2JPsiL1520_ee": {
       "Lb" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(J/psi(1S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (J/psi(1S) => ^e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (J/psi(1S) => e+ ^e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },


    "Lb2PsiL1520_mm": {
       "Lb" : "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (  psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(  psi(2S) => mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (  psi(2S) => ^mu+  mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (  psi(2S) => mu+ ^mu-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },


    "Lb2PsiL1520_ee": {
       "Lb" : "[Lambda_b0 =>  (  psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  (  psi(2S) => e+  e-) ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  (  psi(2S) => e+  e-) (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  (  psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  (  psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  (  psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "JPs": "[Lambda_b0 => ^(  psi(2S) => e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  (  psi(2S) => ^e+  e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  (  psi(2S) => e+ ^e-) (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },



    "Lb2L1520mm": {
       "Lb" : "[Lambda_b0 =>  mu+  mu- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  mu+  mu- ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  mu+  mu- (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  mu+  mu- (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  mu+  mu- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  mu+  mu- (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  ^mu+  mu- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  mu+ ^mu- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },

    "Lb2L1520ee": {
       "Lb" : "[Lambda_b0 =>  e+  e- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      "Lst" : "[Lambda_b0 =>  e+  e- ^(Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "Sigma" : "[Lambda_b0 =>  e+  e- (Lambda(1520)0 => ^(Sigma0 -> Lambda0 gamma )  pi0) ]CC",
    "L0"    : "[Lambda_b0 =>  e+  e- (Lambda(1520)0 => (Sigma0 -> ^Lambda0 gamma )  pi0) ]CC", 
       "Pi0": "[Lambda_b0 =>  e+  e- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  ^pi0) ]CC",
     "Gamma": "[Lambda_b0 =>  e+  e- (Lambda(1520)0 => (Sigma0 -> Lambda0 ^gamma )  pi0) ]CC",
       "L1" : "[Lambda_b0 =>  ^e+  e- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
       "L2" : "[Lambda_b0 =>  e+ ^e- (Lambda(1520)0 => (Sigma0 -> Lambda0 gamma )  pi0) ]CC",
      },


    "Lb2LPsimm_JpsiPiPi" : {
        "Lb": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   Lambda0]CC",
       "L0" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)  ^Lambda0]CC",
       "Psi": "[Lambda_b0 =>  ^(psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ pi-)   Lambda0]CC",
       "Pi1": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) ^pi+ pi-)   Lambda0]CC",
       "Pi2": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  mu-) pi+ ^pi-)   Lambda0]CC",
       "JPs": "[Lambda_b0 =>  (psi(2S) => ^(J/psi(1S) =>  mu+  mu-) pi+ pi-)   Lambda0]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  ^mu+  mu-) pi+ pi-)   Lambda0]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  mu+  ^mu-) pi+ pi-)   Lambda0]CC",
  },

    "Lb2LPsiee_JpsiPiPi" : {
        "Lb": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   Lambda0]CC",
       "L0" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)  ^Lambda0]CC",
       "Psi": "[Lambda_b0 =>  ^(psi(2S) => (J/psi(1S) =>  e+  e-) pi+ pi-)   Lambda0]CC",
       "Pi1": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) ^pi+ pi-)   Lambda0]CC",
       "Pi2": "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  e-) pi+ ^pi-)   Lambda0]CC",
       "JPs": "[Lambda_b0 =>  (psi(2S) => ^(J/psi(1S) =>  e+  e-) pi+ pi-)   Lambda0]CC",
       "L1" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  ^e+  e-) pi+ pi-)   Lambda0]CC",
       "L2" : "[Lambda_b0 =>  (psi(2S) => (J/psi(1S) =>  e+  ^e-) pi+ pi-)   Lambda0]CC",
  },


}

def tuple_branches(decay_desc_full,sample_decay=None):

    # Now add resonances for data/MC and different b hadrons
    tuple_branches = []
    particles = []

    # Some configuration
    charm_resonances = ["J/psi(1S)", "psi(2S)"]
    V0_resonances = ["Lambda0", "KS0"]

    V0dec = "pi-" in decay_desc_full  # Needed to add branches
    V0dec = (V0dec and ("Lambda0 =>" in decay_desc_full or "KS0 =>" in decay_desc_full or "Lambda0 ->" in decay_desc_full or "KS0 ->" in decay_desc_full))

    # Exception for Lambda_c
    exceptions = ('Lambda_c+' in decay_desc_full or "Xi_b" in decay_desc_full or "Omega_b" in decay_desc_full or sample_decay in ["Lb2Lpipi", "Lb2LKpi", "Lb2LKK"])
    exceptions = (exceptions or 'X]' in decay_desc_full or 'pi+ pi-' in decay_desc_full or "Sigma" in decay_desc_full or "L1520" in decay_desc_full)
    exceptions = (exceptions or sample_decay in ["Lb2LPsiee_JpsiPiPi","Lb2LPsimm_JpsiPiPi"])
    if exceptions:
        dec_dict_mc = dict_mc_branches
        if not V0dec: dec_dict_mc = dict_mc_branches_noV0dec
        branches = dec_dict_mc[sample_decay]
        tuple_branches = [x for x in branches]
        print(branches)
        return branches, tuple_branches

    Lb_decay = "Lambda" in decay_desc_full
    Bd_decay = "B0" in decay_desc_full
    Bu_decay = "B+" in decay_desc_full

    # First add the b and s hadrons
    if Lb_decay:
        tuple_branches = ["Lb", "L0"]
        particles += ["Lambda0"]
    elif Bd_decay:
        tuple_branches = ["B", "K0"]
        particles += ["KS0"]
    elif Bu_decay:
        tuple_branches = ["B", "K"]
        particles += ["K+"]

    # take leptons from decay descriptor
    # decay descriptor has first lepton after first or second ^
    # ['e+ ','mu+)    '] is an example string
    first_lepton = 1

    # Add Jpsi if needed and appropriate leptons
    for charm_resonance in ["J/psi(1S)", "psi(2S)"]:
        if charm_resonance in decay_desc_full:  # FOR DecayTree or MCTree with Jpsi
            tuple_branches += ["JPs"]
            particles += [charm_resonance]
            first_lepton += 1

    # Format leptons for SS parsing
    leptons = decay_desc_full.split("^")[first_lepton : first_lepton + 2]
    print ("leptons", leptons)
    leptons[0] = leptons[0].replace(" ", "")
    leptons[1] = leptons[1].split(")")[0].replace(" ", "")
    print ("leptons", leptons)

    # Add lepton branches
    tuple_branches += ["L1", "L2"]
    particles += leptons

    # Check if SS tuple, used to add correct L2 matching
    SS_tuple = leptons in [["e+", "e+"], ["e+", "mu+"], ["mu+", "mu+"]]

    # Add hadrons if the Lambda decays
    if V0dec:
        if Lb_decay:
            tuple_branches += ["P", "Pi"]
            particles += ["p+", "pi-"]
        elif Bd_decay:
            tuple_branches += ["Pi1", "Pi2"]
            particles += ["pi+", "pi-"]

    # Replace hats to get individual particles
    decay_desc = decay_desc_full.replace(
        "^", ""
    )  # remove hats because only one will be needed
    decay_desc_list = decay_desc.split(
        "||"
    )  # TO DEAL WITH emu case: split e+mu- and e-mu+

    print(particles)
    print(tuple_branches)

    # add single hats
    branches = {tuple_branches[0]: decay_desc}
    for i in range(len(tuple_branches) - 1):
        print(i)
        part = particles[i]

        if part in charm_resonances or (part in V0_resonances and V0dec):
            part = "(" + part  # To replace (Lambda for ex.

        # Run over separate decay_desc to deal with e+mu- and e-mu+ case
        decay_desc_replaced = []
        for j in range(len(decay_desc_list)):  # For each separate list, replace
            if j == 0:
                decay_desc_replaced.append(
                    decay_desc_list[j].replace(" " + part, "^" + part, 1)
                )
                decay_desc_tot = decay_desc_replaced[j]
                print(decay_desc_tot)

            else:
                if "e" in part or "mu" in part:
                    if "-" in part:
                        part = part.replace("-", "+")
                    else:
                        part = part.replace(
                            "+", "-"
                        )  # Swap charge for opposite charge lepton pair
                print(part)
                print(decay_desc_list[j])
                decay_desc_replaced.append(
                    decay_desc_list[j].replace(" " + part, "^" + part, 1)
                )
                decay_desc_tot = "{0} || {1}".format(
                    decay_desc_tot, decay_desc_replaced[j]
                )

        branches[tuple_branches[i + 1]] = decay_desc_tot

    if SS_tuple:  # IF SAME SIGN IN TUPLE BRANCHES
        # decay desc has ^e+   e+ twice
        print(branches)
        input_string = "^{0}  {1}".format(leptons[0], leptons[1])
        output_string = " {0} ^{1}".format(leptons[0], leptons[1])
        print(input_string, output_string)
        branches["L2"] = branches["L2"].replace(input_string, output_string)

    return branches, tuple_branches
