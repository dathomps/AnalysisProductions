from __future__ import print_function

from Configurables import DaVinci, GaudiSequencer, EventNodeKiller, ProcStatusCheck
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

from CommonParticlesArchive import CommonParticlesArchiveConf

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

STRIPPING_VERSIONS = {
    "2011": "Stripping21r1p2",
    "2012": "Stripping21r0p2",
    "2015": "Stripping24r2",
    "2016": "Stripping28r2",
    "2017": "Stripping29r2p1",
    "2018": "Stripping34r0p1",
}


def stripping(data_type, stripping_lines):
    # kill the banks
    event_node_killer = EventNodeKiller("StripKiller")
    event_node_killer.Nodes = ["/Event/AllStreams", "/Event/Strip"]

    # figure out stripping version from the year
    stripping_version = STRIPPING_VERSIONS[data_type]

    # import common particles as used at the time
    CommonParticlesArchiveConf().redirect(stripping_version)

    # load linebuilder
    if data_type in ["2011", "2012"]: # Add exception for Xib case?
        import StrippingBu2LLK_R1 as StrippingBu2LLK
    elif data_type in ["2015", "2016", "2017", "2018"]:
        import StrippingBu2LLK_R2 as StrippingBu2LLK

    # declare your custom stream
    custom_stream = StrippingStream("AllStreams")
    custom_linebuilder = StrippingBu2LLK.Bu2LLK_NoPID_LineBuilder(
        "Bu2LLK", StrippingBu2LLK.config
    )

    for line in custom_linebuilder.lines():
        if line.name() in stripping_lines:
            line._prescale = 1.0
            custom_stream.appendLines([line])

    filterBadEvents = ProcStatusCheck()

    # configure your custom stream
    sc = StrippingConf(
        Streams=[custom_stream],
        MaxCandidates=2000,
        AcceptBadEvents=False,
        BadEventSelection=filterBadEvents,
    )

    enablePacking = False


    SelDSTWriterElements = {"default": stripDSTElements(pack=enablePacking)}

    SelDSTWriterConf = {
        "default": stripDSTStreamConf(
            pack=enablePacking,
            selectiveRawEvent=False,
            fileExtension=".RD_LBTOL0LL.DST",
        )
    }
    #Items that might get lost when running the CALO+PROTO ReProcessing in DV
    caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
     
    # Make sure they are present on full DST streams
    SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


    dstWriter = SelDSTWriter(
        "MyDSTWriter",
        StreamConf=SelDSTWriterConf,
        MicroDSTElements=SelDSTWriterElements,
        OutputFileSuffix="",
        SelectionSequences=sc.activeStreams(),
    )

    DaVinci().ProductionType = "Stripping"
    seq = GaudiSequencer("TupleSeq")
    seq.IgnoreFilterPassed = True
    seq.Members += [event_node_killer, sc.sequence()] + [dstWriter.sequence()]

    """
    if "21" in stripping_version:
        print("######################################################################")
        print("###########################   REDO-CALO   ############################")
        print("######################################################################")
        from Gaudi.Configuration import importOptions

        # To be executed always when Stripping production is not done ( i.e. the LDST samples )
        # Not clear if this has to be redone also when Stripping Production is already Stripping 21
        importOptions("$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")
    """
 
    # This is a hack to work around https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/merge_requests/736/
    if getattr(stripping, "NEED_FILENAME_PATCH", True):
        import atexit

        atexit.register(fix_filenames)
        stripping.NEED_FILENAME_PATCH = False

    return seq


def fix_filenames():
    """This is a hack to work around https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/merge_requests/736/"""
    import glob
    import os

    for fn in glob.glob("*.anaprod_bstojpsiphi.ldst"):
        print("Renaming", fn, "to", fn.lower())
        os.rename(fn, fn.lower())

        for xml_fn in glob.glob("summaryDaVinci_*.xml"):
            print("Fixing XML in", xml_fn)
            with open(xml_fn, "rt") as fp:
                xml = fp.read()
            xml = xml.replace(fn, fn.lower())
            with open(xml_fn, "wt") as fp:
                fp.write(xml)

        print("Fixing pool_xml_catalog.xml")
        with open("pool_xml_catalog.xml", "rt") as fp:
            xml = fp.read()
        xml = xml.replace(fn, fn.lower())
        with open("pool_xml_catalog.xml", "wt") as fp:
            fp.write(xml)
