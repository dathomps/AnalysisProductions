# coding=utf-8

# Ryunosuke O'Neil
# r.oneil@cern.ch

# DaVinci version: DaVinci/v45r6
# NOTICE: when using this options file, please make note of the below:

# - These should be set in another options file BEFORE this one:
#    DaVinci().Simulation=RUNNING_SIMULATION
#    DaVinci().DataType=DATA_YEAR
# - If Simulation == True, the right DDDB and CondDB tags need to be set


from StandardParticles import StdAllNoPIDsPions
from Configurables import (
    CheckPV,
    DaVinci,
    DstConf,
    TurboConf,
    TupleToolRecoStats,
    TupleToolTISTOS,
    TupleToolPropertime,
    SubstitutePID,
    CondDB,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
    MCDecayTreeTuple,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    LoKi__Hybrid__DTFDict as DTFDict,
)
from PhysConf.Selections import (
    FilterSelection,
    MomentumScaling,
    MomentumSmear,
    TupleSelection,
    CombineSelection,
    SelectionSequence,
    RebuildSelection,
    AutomaticData,
    Selection,
)
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils
from DecayTreeTuple.Configuration import *  # noqa: F403, F401


HLT2_LINE = "Hlt2CharmHadXicpToPpKmPipTurbo"
DEBUGGING = False

enableTISTOSbranches = False
triggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0PhysDecision",
    "L0GlobalDecision",
    # Hlt1 track
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackPhotonDecision",
    "Hlt1GlobalDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1TrackMVALooseDecision",
    "Hlt1TwoTrackMVALooseDecision",
    "Hlt1L0AnyDecision",
]
toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]

mcToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy", "MCTupleToolPID"]

XicpStStTree_LoKiVariables = {

}  # "M12": "M12", "M13": "M13", "M23": "M23"}

XicpTree_LoKiVariables = {
    "BPVVDCHI2": "BPVVDCHI2",
    # IPChi2 on the related PV.
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    # "M12": "M12",
    # "M13": "M13",
    # "M23": "M23",
    # Rapidity and pseudorapidity on the Xic+.
    "Y": "Y",
    "ETA": "ETA",
    # pT sum over proton, kaon, pion
    "SUM_PT": "(CHILD(PT, 1) + CHILD(PT, 2) + CHILD(PT, 3))",
}

BachelorPionCut = (
    "(BPVIPCHI2() > 0) & (BPVIPCHI2() < 9) & "
    "(TRGHOSTPROB < 0.4) & (PROBNNpi > 0.1) & "
    "(PT > 200*MeV)"
)

XicpStSt_CombineSelectionCuts = dict(
    CombinationCut="(AM < 3350*MeV) & (AM > 2700*MeV)",
    # Avoid problems with missing PV and proper time fit
    # cut out mass range above 3.3 GeV
    MotherCut="""BPVVALID() & (M < 3300*MeV) & (CHI2VXNDF > 0)""",
    CheckOverlapTool="LoKi::CheckOverlap",
    ReFitPVs=True,
    DaughtersCuts={
        "pi+": BachelorPionCut,
        "pi-": BachelorPionCut,
    },
)

Xic0St_CombineSelectionCuts = dict(
    CombinationCut="((AM - AM1 - AM2) < 200)",
    MotherCut=(
        "BPVVALID() & (CHI2VXNDF > 0) & (CHI2VXNDF < 15) & "
        "(BPVIPCHI2() > 0) & ((M - CHILD(M, 1) - CHILD(M, 2)) < 150) & "
        "((M - CHILD(M, 1) - CHILD(M, 2)) > 0)"
    ),
    CheckOverlapTool="LoKi::CheckOverlap",
    ReFitPVs=True,
    DaughtersCuts={
        "pi+": BachelorPionCut,
        "pi-": BachelorPionCut,
    },
)


# No Momentum Scaling for 2018
transientEventStore_root = {
    "2015": "/Event/Turbo",
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmmultibody/Turbo",
    "2018": "/Event/Charmmultibody/Turbo",
}


def ConfigureDTT(dtt, TISTOS_Branches=[]):
    dtt.ReFitPVs = True
    dtt.ToolList = toolList[:]

    tt_proptime = dtt.addTupleTool(TupleToolPropertime)
    tt_proptime.Verbose = True

    tt_recostats = dtt.addTupleTool(TupleToolRecoStats)
    tt_recostats.Verbose = True

    if enableTISTOSbranches:
        for br in TISTOS_Branches:
            tt_tistos = br.addTupleTool(TupleToolTISTOS)
            tt_tistos.Verbose = True
            tt_tistos.VerboseHlt1 = True
            tt_tistos.VerboseL0 = True
            tt_tistos.FillHlt2 = False
            tt_tistos.TriggerList = triggerList

    if DaVinci().Simulation:
        relations = TeslaTruthUtils.getRelLocs() + [
            TeslaTruthUtils.getRelLoc(""),
            # Location of the truth tables for PersistReco objects
            "Relations/Hlt2/Protos/Charged",
        ]
        mc_tools = mcToolList[:]
        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)

        tt_mctruth = dtt.addTupleTool(TupleToolMCTruth)
        tt_mctruth.ToolList = mcToolList[:]

        dtt.addTupleTool(TupleToolMCBackgroundInfo)

    return dtt


def ConfigureDTF(dtt, branch_name, DTFDictVariables=None, allow_cospol=False):
    MakeDTFD(
        dtt,
        branch_name,
        DTFDictVariables,
        tool_name="DTF_PV_XicpPDG",
        constrainPV=True,
        constrainDaughters=["Xi_c+"],
    )

    # MakeDTFD(
    #     dtt,
    #     branch_name,
    #     DTFDictVariables,
    #     tool_name="DTF_PV",
    #     constrainPV=True,
    # )

    # MakeDTFD(
    #     dtt,
    #     branch_name,
    #     DTFDictVariables,
    #     tool_name="DTF_XicpPDG",
    #     constrainPV=False,
    #     constrainDaughters=["Xi_c+"],
    # )

    xicpstst_vars = dict(**XicpStStTree_LoKiVariables)

    if allow_cospol:
        # xicpstst_vars["CosThetaC_pipi"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) ^pi+ ]CC', False)"
        # xicpstst_vars["CosThetaC_Xicpip"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) ^pi+ ]CC', False)"
        # xicpstst_vars["CosThetaC_Xicpim"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', False)"

        # xicpstst_vars["CosThetaC_PipXic0st"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) ^pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', False)"
        # xicpstst_vars["CosThetaC_PimXic0st"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', False)"


        # xicpstst_vars["CosThetaC_PipXic0st2"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) ^pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC')"
        xicpstst_vars["CosThetaC_PimXic0st2"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC')"


    if len(xicpstst_vars.items()) != 0:
        # LoKi variables on the Xic+ in the Xic**+ tree.
        getattr(dtt, branch_name).addTupleTool(
            "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
        ).Variables = xicpstst_vars


def MakeDTFD(
    dtt,
    branch_name,
    variables,
    tool_name="DTFTuple",
    constrainPV=True,
    constrainDaughters=None,
):
    branch = getattr(dtt, branch_name)

    DictTuple = branch.addTupleTool(LoKi__Hybrid__Dict2Tuple, tool_name)
    DictTuple.addTool(DTFDict, tool_name)
    DictTuple.Source = "LoKi::Hybrid::DTFDict/" + tool_name
    DictTuple.NumVar = len(
        variables.items()
    )  # reserve a suitable size for the dictionary

    DTF = getattr(DictTuple, tool_name)

    # configure the DecayTreeFitter in the usual way
    DTF.constrainToOriginVertex = constrainPV
    if constrainDaughters is not None:
        DTF.daughtersToConstrain = constrainDaughters

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict_" + tool_name)
    DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict_" + tool_name

    DTF_dict = getattr(DTF, "dict_" + tool_name)
    DTF_dict.Variables = dict(**variables)


def ConfigureXicp(dtt, branch_name):
    branch = getattr(dtt, branch_name)

    xicp_vars = dict(**XicpTree_LoKiVariables)

    # LoKi variables on the Xic+ in the Xic**+ tree.
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = xicp_vars


def make_decay_str_and_branches(decay):
    from string import Formatter

    branches = [fn for _, fn, _, _ in Formatter().parse(decay) if fn is not None]
    branch_dict = {}
    for branch in branches:
        branch_dict[branch] = decay.format(
            **dict((b, "^" if b == branch else "") for b in branches)
        )
    decay_allcaret = decay.format(**dict((b, "^") for b in branches))
    # no caret at the beginning (for the decay key)
    return {"Decay": decay_allcaret[1:], "Branches": branch_dict}


def make_tree(
    tree_name,
    Xicp,
    pions,
    decay_str_with_branches,
    # If this is None, we only reconstruct the Xicp and ignore the pions.
    XicStSt_branch=None,
    # Xicp is always reconstructed.
    Xic_branch="Xicp",
    # If True, we attempt to include the intermediate Xic(2645)+ state
    include_intermediate_decay=False,
    # The bachelor pions
    bachelor_pion_1="pi-",
    bachelor_pion_2="pi+",
    **combine_selection_kwargs
):
    decay_dict = make_decay_str_and_branches(decay_str_with_branches)

    input_particles = [Xicp] if XicStSt_branch is None else [Xicp, pions]

    if XicStSt_branch is not None:
        if XicStSt_branch == "XicpStSt":
            if include_intermediate_decay:
                input_particles = [
                    CombineSelection(
                        tree_name + "_CombineSelectionXic2645",  # the name
                        [Xicp, pions],  # inputs to combine
                        DecayDescriptor="[Xi_c*0 -> Xi_c+ " + bachelor_pion_1 + "]cc",
                        **Xic0St_CombineSelectionCuts
                    ),
                    pions,
                ]
                decay_descriptor = "[Sigma_c*+ -> Xi_c*0 " + bachelor_pion_2 + "]cc"
            else:
                # Reconstruct Xic** directly.
                decay_descriptor = (
                    "[Sigma_c*+ -> Xi_c+ "
                    + bachelor_pion_2
                    + " "
                    + bachelor_pion_2
                    + "]cc"
                )

        elif XicStSt_branch == "Xic0St":
            decay_descriptor = "[Xi_c*0 -> Xi_c+ " + bachelor_pion_1 + "]cc"
        else:
            raise RuntimeError("Unrecognised XicpStSt_branch " + XicStSt_branch)

        combineSel = [
            CombineSelection(
                tree_name + "_CombineSelection",  # the name
                input_particles,  # inputs to combine
                DecayDescriptor=decay_descriptor,
                **combine_selection_kwargs
            )
        ]
    else:
        if include_intermediate_decay:
            raise RuntimeError(
                "Can't set include_intermediate_decay True if XicStSt_branch is None"
            )
        combineSel = input_particles

    tupleSel = TupleSelection(tree_name, combineSel, **decay_dict)

    dtt = tupleSel.algorithm()

    dtf_dict = {"M12": "M12", "M": "M", "PT": "PT"}

    if XicStSt_branch == "XicpStSt" and include_intermediate_decay:
        Xic0St_dtf_dict = dict(**dtf_dict)
        Xic0St_dtf_dict["Xicp_M"] = "CHILD(M, 1)"
        ConfigureDTF(dtt, "Xic0St", DTFDictVariables=Xic0St_dtf_dict)

    tt_br = [getattr(dtt, b) for b in [Xic_branch, XicStSt_branch] if b is not None]
    ConfigureDTT(dtt, TISTOS_Branches=tt_br)

    if XicStSt_branch is not None:
        XicpStSt_dtf_dict = dict(**dtf_dict)
        cospol = False
        if include_intermediate_decay:
            XicpStSt_dtf_dict["Xicp_M"] = "CHILD(M, 1, 1)"
            XicpStSt_dtf_dict["Xicp_PT"] = "CHILD(PT, 1, 1)"
            if not "WS" in tree_name and not XicStSt_branch == "Xic0St":
                XicpStSt_dtf_dict["CosThetaC_Xicpip"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 ->  (Xi_c+ -> p+ K- pi+) pi-) ^pi+ ]CC', False)"
                XicpStSt_dtf_dict["CosThetaC_Xicpim"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 ->  (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', False)"
                XicpStSt_dtf_dict["CosThetaC_PimXic0st2"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi-) pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi-) pi+ ]CC')"
                cospol = True
            elif "WSPPP" in tree_name and not XicStSt_branch == "Xic0St":
                XicpStSt_dtf_dict["CosThetaC_Xicpip"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi+) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 ->  (Xi_c+ -> p+ K- pi+) pi+) ^pi+ ]CC', False)"
                XicpStSt_dtf_dict["CosThetaC_Xicpim"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> ^(Xi_c+ -> p+ K- pi+) pi+) pi+ ]CC', '[Sigma_c*+ -> (Xi_c*0 ->  (Xi_c+ -> p+ K- pi+) ^pi+) pi+ ]CC', False)"
                XicpStSt_dtf_dict["CosThetaC_PimXic0st2"] = "COSPOL('[Sigma_c*+ -> (Xi_c*0 -> (Xi_c+ -> p+ K- pi+) ^pi+) pi+ ]CC', '[Sigma_c*+ -> ^(Xi_c*0 -> (Xi_c+ -> p+ K- pi+) pi+) pi+ ]CC')"
                cospol = True

        else:
            XicpStSt_dtf_dict["Xicp_M"] = "CHILD(M, 1)"
            XicpStSt_dtf_dict["Xicp_PT"] = "CHILD(PT, 1)"

        ConfigureDTF(dtt, XicStSt_branch, DTFDictVariables=XicpStSt_dtf_dict, allow_cospol=cospol)

    ConfigureXicp(dtt, Xic_branch)

    return [
        SelectionSequence(
            tree_name + "_SelectionSequence",
            tupleSel,
            EventPreSelector=[CheckPV("TwoPV", MinPVs=1)],
        ).sequence()
    ]


rootInTES = transientEventStore_root[DaVinci().DataType]
the_line = "{}/Particles".format(HLT2_LINE)
all_sequences = []

# CondDB tag corresp. to the year (if data, and not MC)
if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)
# Otherwise, DDDB and CondDB tags set in another options file!

mcTuple_seq = None
if DaVinci().Simulation:
    mcTuple = MCDecayTreeTuple("MCTuple")
    dk = make_decay_str_and_branches(
        "{XicpStSt}([Sigma_c*+ ==> ^(Xi_c*0 ==> "
        "{Xicp}(Xi_c+ ==> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi-) {Pi2}pi+ ]CC)"
    )
    mcTuple.Decay = dk["Decay"]
    mcTuple.Branches = dk["Branches"]
    mcTuple.ToolList = mcToolList[:]

    mcTuple_sel = Selection("MCTree", Algorithm=mcTuple, RequiredSelections=[])
    mcTuple_seq = SelectionSequence("MCTREE_SEQ", mcTuple_sel).sequence()
    all_sequences += [mcTuple_seq]

trigger_filter = LoKi_Filters(
    # Adjust this regular expression to match whatever set of lines you're
    # interested in studying
    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(HLT2_LINE)
)

# Read data from the Turbo trigger line
XicpTurbo = AutomaticData(the_line)

# (2) get pions from PersistReco
pions = RebuildSelection(StdAllNoPIDsPions)

if DEBUGGING:
    from PhysConf.Selections import PrintSelection

    pions = PrintSelection(pions)

subPID = SubstitutePID(
    "MakeXicpToPpKmPip",
    Code="DECTREE('[Lambda_c+ -> K- p+ pi+]CC')",
    MaxChi2PerDoF=-1,  # necessary to correctly fill loki doca variables
    Substitutions={
        "Lambda_c+  -> K- p+  pi+": "Xi_c+",
        "Lambda_c~- -> K+ p~- pi-": "Xi_c~-",
    },
)

Xicp = Selection("Xicp", Algorithm=subPID, RequiredSelections=[XicpTurbo])

if DaVinci().Simulation:
    Xicp = MomentumSmear(Xicp)
else:
    # insert momentum scaling
    Xicp = MomentumScaling(Xicp, Turbo="PERSISTRECO", Year=DaVinci().DataType)


Xicp = FilterSelection(
    "XicpFilter",
    [Xicp],
    Code=(
        "( (CHI2VXNDF < 16) & (CHI2VXNDF > 0) & (ADMASS('Xi_c+') < 80) & "
        "(BPVIPCHI2('') > 0) & (BPVIPCHI2('') < 30.0) & (BPVDIRA > 0.99995) ) & "
        "(INTREE( (ABSID=='p+') & (PROBNNp > 0.4) & (P > 10000) & (TRGHOSTPROB < 0.35) )) & "
        "(INTREE( (ABSID=='K+') & (PROBNNk > 0.4) & (TRGHOSTPROB < 0.35) )) & "
        "(INTREE( (ABSID=='pi+') & (PROBNNpi > 0.4) & (TRGHOSTPROB < 0.35) )) "
    ),
)

# create Xic**+ candidates

all_sequences += make_tree(
    tree_name="XicpStSt_ToXicpPimPip",
    Xicp=Xicp,
    pions=pions,
    bachelor_pion_1="pi-",
    bachelor_pion_2="pi+",
    decay_str_with_branches=(
        "{XicpStSt}([Sigma_c*+ -> {Xic0St}(Xi_c*0 -> "
        "{Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi-) {Pi2}pi+ ]CC)"
    ),
    XicStSt_branch="XicpStSt",
    Xic_branch="Xicp",
    include_intermediate_decay=True,
    **XicpStSt_CombineSelectionCuts
)


############################################################################
# Wrong Sign Xicp Pip Pip (WS_PPP)

all_sequences += make_tree(
    tree_name="XicpStStWSPPP_ToXicpPipPip",
    Xicp=Xicp,
    pions=pions,
    bachelor_pion_1="pi+",
    bachelor_pion_2="pi+",
    decay_str_with_branches=(
        "{XicpStSt}([Sigma_c*+ -> {Xic0St}(Xi_c*0 -> "
        "{Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi+) {Pi2}pi+ ]CC)"
    ),
    XicStSt_branch="XicpStSt",
    Xic_branch="Xicp",
    include_intermediate_decay=True,
    **XicpStSt_CombineSelectionCuts
)

############################################################################
# Wrong Sign Xicp Pim Pim (WS_PMM)

# all_sequences += make_tree(
#     tree_name="XicpStStWSPMM_ToXicpPimPim",
#     Xicp=Xicp,
#     pions=pions,
#     bachelor_pion_1="pi-",
#     bachelor_pion_2="pi-",
#     decay_str_with_branches=(
#         "{XicpStSt}([Sigma_c*+ -> {Xic0St}(Xi_c*0 -> "
#         "{Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi-) {Pi2}pi- ]CC)"
#     ),
#     XicStSt_branch="XicpStSt",
#     Xic_branch="Xicp",
#     include_intermediate_decay=True,
#     **XicpStSt_CombineSelectionCuts
# )


#
# Tuple containing Xic0* -> Xic+ pi- info
#

# Combine pions with Xic+ in event to make Xic*0 candidates (intermediate
# resonance)

# all_sequences += make_tree(
#     tree_name="Xic0St_ToXicpPim",
#     Xicp=Xicp,
#     pions=pions,
#     bachelor_pion_1="pi-",
#     bachelor_pion_2=None,
#     decay_str_with_branches=(
#         "{Xic0St}([Xi_c*0 -> {Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi-]CC)"
#     ),
#     XicStSt_branch="Xic0St",
#     Xic_branch="Xicp",
#     **Xic0St_CombineSelectionCuts
# )


#################################
# Wrong Sign

# all_sequences += make_tree(
#     tree_name="Xic0StWS_ToXicpPip",
#     Xicp=Xicp,
#     pions=pions,
#     bachelor_pion_1="pi+",
#     bachelor_pion_2=None,
#     decay_str_with_branches=(
#         "{Xic0St}([Xi_c*0 -> {Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) {Pi1}pi+]CC)"
#     ),
#     XicStSt_branch="Xic0St",
#     Xic_branch="Xicp",
#     **Xic0St_CombineSelectionCuts
# )


#
# Tuple containing Xi_c+ info
#

# all_sequences += make_tree(
#     tree_name="Xicp_ToPpKmPip",
#     Xicp=Xicp,
#     pions=None,
#     bachelor_pion_1=None,
#     bachelor_pion_2=None,
#     decay_str_with_branches=(
#         "{Xicp}([Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+]CC)"
#     ),
#     XicStSt_branch=None,
#     Xic_branch="Xicp",
# )


# (7) configure DaVinci
DaVinci().PrintFreq = 10000
DaVinci().RootInTES = rootInTES
DaVinci().InputType = "MDST"
DaVinci().Turbo = True

# Ignore events that don't pass the HLT2 trigger.
DaVinci().EventPreFilters = trigger_filter.filters("TriggerFilter")

# (8) insert our sequence into DaVinci
DaVinci().UserAlgorithms = all_sequences

DaVinci().Lumi = not DaVinci().Simulation

# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
    from Configurables import DataOnDemandSvc
    from Configurables import Gaudi__DataLink as Link

    dod = DataOnDemandSvc()
    for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
        dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")
