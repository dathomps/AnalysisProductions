from PhysConf.Selections import StrippingData, SelectionSequence,\
    TupleSelection
from Configurables import DaVinci
from OmbToOmcPi_XibToXic0Pi.utils import presel_selection, mcmatch_selection,\
    configure_tools

# Set configuration switches
mcMatch   = False
preSel    = True
decayMode = "Bu2D0Pi"

# Select stream for MC or data
if DaVinci().getProp('Simulation'):
    stream = 'B2D0Pi_D2KKPiPi.Strip'
    isMC = True
else:
    stream = 'Bhadron'
    isMC = False
DaVinci().StrippingStream = stream

# Set stripping line name and data location
line = 'B2D0PiD2HHHHBeauty2CharmLine'
mdst = (DaVinci().InputType.lower() == 'mdst')
strippingOutput = StrippingData(line, stream if not mdst else '')
dttInput = strippingOutput

# Set up MC truth matching selection
# Only mcMatch for Run 2 since the particle relations are not correct for Run 1
if mcMatch and isMC and int(DaVinci().DataType) > 2012:
    dttInput = mcmatch_selection(dttInput, decayMode)
    
# Set up preselection
if preSel: 
    dttInput = presel_selection(dttInput, isMC, "B-", 5000, 6000)

# Create an ntuple to capture output from the selection
dtt = TupleSelection('TupleBu2D0Pi', dttInput,
                     Decay='[ B- -> ( [D0]cc  -> K+  K- pi+ pi-)  pi-  ]CC')
dtt.setDescriptorTemplate('${Hb}[B- -> ${Hc0}( [D0]cc  -> ${Kp}K+  ${Km}K-  ${pip}pi+  ${pim}pi- )  ${pi_batch}pi- ]CC')

# Configure DecayTreeFitter
configure_tools(dtt, isMC, decayMode)

# Configure DaVinci
selseq = SelectionSequence('TupleSeqD0', TopSelection=dtt)
DaVinci().UserAlgorithms += [selseq.sequence()]
DaVinci().TupleFile = 'DVNTuple.root'

# DaVinci().EvtMax = 1000
