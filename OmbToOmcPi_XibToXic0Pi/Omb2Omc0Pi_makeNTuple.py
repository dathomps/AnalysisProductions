from PhysConf.Selections import StrippingData, SelectionSequence,\
    TupleSelection
from Configurables import DaVinci
from OmbToOmcPi_XibToXic0Pi.utils import presel_selection, mcmatch_selection,\
    configure_tools

# Set configuration switches
mcMatch   = True
preSel    = True
toMatch = "Omb2Omc0Pi"

# Select stream for MC or data
if DaVinci().getProp('Simulation'):
    stream = 'AllStreams'
    isMC = True
else:
    stream = 'Bhadron'
    isMC = False
DaVinci().StrippingStream = stream

# Set stripping line name and data location
line = 'Omegab2Omegac0PiOmegac02PKKPiBeauty2CharmLine'
mdst = (DaVinci().InputType.lower() == 'mdst')
strippingOutput = StrippingData(line, stream if not mdst else '')
dttInput = strippingOutput

# Set up MC truth matching selection
if mcMatch and isMC:
    dttInput = mcmatch_selection(dttInput, toMatch)
    
# Set up preselection
if preSel: 
    dttInput = presel_selection(dttInput, isMC, "Omega_b-", 5600, 6600)

# Create an ntuple to capture output from the selection
dtt = TupleSelection('TupleOmb2Omc0Pi', dttInput,
                     Decay='[ Omega_b- -> ( Omega_c0  -> p+  K-  K-  pi+ )  pi-  ]CC')
dtt.setDescriptorTemplate('${Hb}[Omega_b- -> ${Hc0}( Omega_c0  -> ${proton}p+  ${K1}K-  ${K2}K-  ${pi}pi+ )  ${pi_batch}pi- ]CC')

# Configure DecayTreeFitter
configure_tools(dtt, isMC, toMatch)

# Configure DaVinci
selseq = SelectionSequence('TupleSeq', TopSelection=dtt)
DaVinci().UserAlgorithms += [selseq.sequence()]
DaVinci().TupleFile = 'DVNTuple.root'
# DaVinci().EvtMax = 300000
