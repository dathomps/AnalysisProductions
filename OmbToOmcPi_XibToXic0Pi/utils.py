from PhysConf.Selections import Selection
from Configurables import FilterDesktop, LoKi__Hybrid__TupleTool
from DecayTreeTuple import Configuration

# MC decay descriptors
mcmatchDescs = {
    # This decay is generated wtih TRUEID for Xi_b- -> (Omega_c0 -> p+ K- K- pi+) pi- for some reason
    "Omb2Omc0Pi" : "[ Xi_b- ==> ( Omega_c0 ==> p+ K- K- pi+ )  pi- ]CC",
    "Xib2Xic0Pi" : "[ Xi_b- ==> ( Xi_c0 ==> p+ K- K- pi+ )  pi- ]CC",
    "Bu2D0Pi" : '[ B- ==> ( D0  ==> K+  K- pi+ pi-)  pi-  ]CC',
}


def mcmatch_selection(inputSel, toMatch):
    '''Make the MC truth matching selection.'''
    # Set Code for MC truth matching
    if toMatch in mcmatchDescs:
        descriptor = mcmatchDescs[toMatch]
    else:
        print("*** MC Truth match: decay mode {} not known".format(toMatch))
        return inputSel
    
    filterMCMatch = FilterDesktop('filterMCMatch',
                                  Code = 'mcMatch("{}")'.format(descriptor))
    filterMCMatch.Preambulo = ["from LoKiPhysMC.decorators import *" ,
                               "from LoKiPhysMC.functions import mcMatch" ]
        
    selMCMatch = Selection(name = "selMCMatch",
                           Algorithm = filterMCMatch,
                           RequiredSelections = [inputSel])
    return selMCMatch


def presel_selection(inputSel, isMC, mother, mmin=5600, mmax=6600, RSorWS = "RS"):
    '''Make the preselection Selection.'''
    cutM      = f"(M>{mmin}*MeV) & (M<{mmax}*MeV)"
    cutBIP    = "(BPVIPCHI2()<15)"
    cutDIRA   = "(INTREE( (ABSID=='{}') & (BPVDIRA>0.99993) ) )".format(mother)
    print("*** cutDIRA = {}".format(cutDIRA))
    cutFD     = "(INTREE( (ABSID=='{}') & (BPVVDCHI2>100) ) )".format(mother)
    print("*** cutFD = {}".format(cutFD))
    cutDauPT  = "(MINTREE(ABSID=='K+',PT)>250*MeV) & (MINTREE(ABSID=='p+',PT)>450*MeV)"
    #cutDauPID = "(MINTREE(ABSID=='K+',PROBNNk)>0.3) & (MINTREE(ABSID=='p+',PROBNNp)>0.3)"
    cuts = [
        cutM,
        cutBIP,
        cutDIRA,
        cutFD,
        cutDauPT,
    ]
    # Don't want to cut on uncorrected PID in MC (no PID at all in preselection now)
    #if not isMC:
    #    cuts.append(cutDauPID)

    fltPreSel = FilterDesktop(name = "fltPreSel{0}_{1}".format(mother, RSorWS),
                              Code =  ' & '.join(cuts))

    selPreSel = Selection(name = "selPreSel{0}_{1}".format(mother, RSorWS),
                          Algorithm = fltPreSel,
                          RequiredSelections = [inputSel])
    return selPreSel


def configure_tools(dtt, isMC, toMatch=None):
    '''Configure tuple tools.'''
    dtt.Hb.addTupleTool('TupleToolDecayTreeFitter/DTF')
    dtt.Hb.DTF.constrainToOriginVertex = True
    dtt.Hb.DTF.Verbose = True
    dtt.Hb.DTF.UseFullTreeInName = True
    
    # Configure the TupleTools
    allToolList = ["TupleToolKinematic", "TupleToolGeometry",
                   "TupleToolPrimaries", "TupleToolAngles",
                   "TupleToolEventInfo", "TupleToolRecoStats",
                   "TupleToolPid",       "TupleToolTrackInfo"]
    llbToolList = ["TupleToolPropertime"]
    resToolList = ["TupleToolDalitz"]
    #dauToolList = ["TupleToolPid", "TupleToolTrackInfo"]
    triggerList = ["L0MuonDecision",
                   "L0DiMuonDecision",
                   "L0ElectronDecision",
                   "L0PhotonDecision",
                   "L0HadronDecision",
                   "Hlt1TrackMVADecision",
                   "Hlt1TwoTrackMVADecision",
                   "Hlt1TrackMVATightDecision",
                   "Hlt1TwoTrackMVATightDecision",
                   "Hlt2Topo2BodyDecision",
                   "Hlt2Topo3BodyDecision",
                   "Hlt2Topo4BodyDecision",
                   "Hlt2CharmHadXic0ToPpKmKmPipTurboDecision"]

    # Add LOKI tool
    LoKi_ETA = LoKi__Hybrid__TupleTool("LoKi_ETA")
    LoKi_ETA.Variables = {
        "ETA"   : "ETA"
        }
    
    # dtt.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_ETA"]
    dtt.addTool(LoKi_ETA)

    # Add the TupleTools
    ttt = dtt.addTupleTool("TupleToolTrigger")
    ttt.Verbose = True
    ttt.TriggerList = triggerList
    tttt = dtt.addTupleTool("TupleToolTISTOS")
    tttt.Verbose = True 
    tttt.TriggerList = triggerList

    dtt.ToolList          += allToolList
    dtt.Hb.ToolList       += llbToolList
    dtt.Hc0.ToolList      += llbToolList
    dtt.Hc0.ToolList      += resToolList
    #dtt.proton.ToolList   += dauToolList
    #dtt.K1.ToolList       += dauToolList
    #dtt.K2.ToolList       += dauToolList
    #dtt.pi.ToolList       += dauToolList
    #dtt.pi_batch.ToolList += dauToolList
    if not isMC:
        return
    
    dtt.ToolList  += ["TupleToolMCTruth", "TupleToolMCBackgroundInfo",
                      "TupleToolL0Calo"]
    if toMatch and toMatch in mcmatchDescs:
        mcmatch = dtt.Hb.addTupleTool('LoKi__Hybrid__TupleTool/mcmatch')
        mcmatch.Preambulo += ['from LoKiPhysMC.functions import mcMatch']
        mcmatch.Variables = {'MCMatch' : 'switch(mcMatch({0!r}), 1, 0)'
                             .format(mcmatchDescs[toMatch])}
