from Configurables import DaVinci, TrackScaleState

if not DaVinci().getProp('Simulation'):
    DaVinci().UserAlgorithms.insert(0, TrackScaleState())
