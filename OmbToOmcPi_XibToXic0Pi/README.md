# OmbToOmcPi_XibToXic0Pi

Productions for Omb- -> (Omc0 -> p+ K- K- pi+) pi- and Xib- -> (Xic0 -> p+ K- K- pi+) pi- with B- -> (D0 -> K+K-pi+pi-) pi- as the control mode.

Run 1 & 2 data are included, along with wrong-sign modes for background studies, and signal & background MC.

The ntuples include branches with DecayTreeFitter with a vertex constraint but no mass constraints. Momentum scaling is used for real data.

The current usage is for Omc0 & Xic0 lifetime measurements ([TWiki](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/OmcXic0LifetimesFromHadronicB)). The Run 1 samples have previously been used for Omb & Xib mass & lifetime measurements ([here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/OmegabtoOmegacPi) & [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/XibMinus2XicZeroHDecays)) so could be used for an update on Run 2.
