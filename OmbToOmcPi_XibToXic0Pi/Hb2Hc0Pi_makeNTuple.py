from PhysConf.Selections import StrippingData, SelectionSequence,\
    TupleSelection
from Configurables import DaVinci
from OmbToOmcPi_XibToXic0Pi.utils import presel_selection, mcmatch_selection,\
    configure_tools

# Set configuration switches
mcMatch   = True
preSel    = True

# Select stream for MC or data
if DaVinci().getProp('Simulation'):
    stream = 'AllStreams'
    isMC = True
else:
    stream = 'Bhadron'
    isMC = False
DaVinci().StrippingStream = stream

# Set stripping line name and data location
lineOm   = 'Omegab2Omegac0PiOmegac02PKKPiBeauty2CharmLine'
lineXi   = 'Xib2Xic0PiXic02PKKPiBeauty2CharmLine'
lineD0   = 'B2D0PiD2HHHHBeauty2CharmLine'
lineOmWS = 'Omegab2Omegac0PiWSOmegac02PKKPiBeauty2CharmLine' # Wrong sign pion
lineXiWS = 'Xib2Xic0PiWSXic02PKKPiBeauty2CharmLine'          # Wrong sign pion
mdst     = (DaVinci().InputType.lower() == 'mdst')
strippingOutputOm   = StrippingData(lineOm,   stream if not mdst else '')
strippingOutputXi   = StrippingData(lineXi,   stream if not mdst else '')
strippingOutputD0   = StrippingData(lineD0,   stream if not mdst else '')
strippingOutputOmWS = StrippingData(lineOmWS, stream if not mdst else '')
strippingOutputXiWS = StrippingData(lineXiWS, stream if not mdst else '')
dttInputOm   = strippingOutputOm
dttInputXi   = strippingOutputXi
dttInputD0   = strippingOutputD0
dttInputOmWS = strippingOutputOmWS
dttInputXiWS = strippingOutputXiWS

# Set up MC truth matching selection
if mcMatch and isMC:
    dttInputOm   = mcmatch_selection(dttInputOm,   "Omb2Omc0Pi")
    dttInputXi   = mcmatch_selection(dttInputXi,   "Xib2Xic0Pi")
    dttInputD0   = mcmatch_selection(dttInputD0,   "Bu2D0Pi")
    dttInputOmWS = mcmatch_selection(dttInputOmWS, "Omb2Omc0Pi")
    dttInputXiWS = mcmatch_selection(dttInputXiWS, "Xib2Xic0Pi")
    
# Set up preselection
if preSel: 
    dttInputOm   = presel_selection(dttInputOm,   isMC, "Omega_b-", 5600, 6600, "RS")
    dttInputXi   = presel_selection(dttInputXi,   isMC, "Xi_b-",    5400, 6400, "RS")
    dttInputD0   = presel_selection(dttInputD0,   isMC, "B-",       5000, 6000, "RS")
    dttInputOmWS = presel_selection(dttInputOmWS, isMC, "Omega_b-", 5600, 6600, "WS")
    dttInputXiWS = presel_selection(dttInputXiWS, isMC, "Xi_b-",    5400, 6400, "WS")
    
# Create an ntuple to capture output from the selection
dttOm = TupleSelection('TupleOmb2Omc0Pi', dttInputOm,
                       Decay='[ Omega_b- -> ( Omega_c0  -> p+  K-  K-  pi+ )  pi-  ]CC')
dttOm.setDescriptorTemplate('${Hb}[Omega_b- -> ${Hc0}( Omega_c0  -> ${proton}p+  ${K1}K-  ${K2}K-  ${pi}pi+ )  ${pi_batch}pi- ]CC')

dttXi = TupleSelection('TupleXib2Xic0Pi', dttInputXi,
                       Decay = '[ Xi_b- -> ( Xi_c0 -> p+ K- K- pi+ )  pi- ]CC')
dttXi.setDescriptorTemplate('${Hb}[Xi_b- -> ${Hc0}(Xi_c0  -> ${proton}p+  ${K1}K-  ${K2}K-  ${pi}pi+ )  ${pi_batch}pi- ]CC')

dttD0 = TupleSelection('TupleBu2D0Pi', dttInputD0,
                       Decay='[ B- -> ( [D0]cc  -> K+ K- pi+ pi-)  pi-  ]CC')
dttD0.setDescriptorTemplate('${Hb}[B- -> ${Hc0}( [D0]cc  -> ${Kp}K+  ${Km}K-  ${pip}pi+ ${pim}pi- )  ${pi_batch}pi- ]CC')

dttOmWS = TupleSelection('TupleOmb2Omc0PiWS', dttInputOmWS,
                         Decay='[ Omega_b- -> ( Omega_c0  -> p+  K-  K-  pi+ )  pi+  ]CC')
dttOmWS.setDescriptorTemplate('${Hb}[Omega_b- -> ${Hc0}( Omega_c0  -> ${proton}p+  ${K1}K-  ${K2}K-  ${pi}pi+ )  ${pi_batch}pi+ ]CC')

dttXiWS = TupleSelection('TupleXib2Xic0PiWS', dttInputXiWS,
                       Decay = '[ Xi_b- -> ( Xi_c0 -> p+ K- K- pi+ )  pi+ ]CC')
dttXiWS.setDescriptorTemplate('${Hb}[Xi_b- -> ${Hc0}(Xi_c0  -> ${proton}p+  ${K1}K-  ${K2}K-  ${pi}pi+ )  ${pi_batch}pi+ ]CC')

# Configure DecayTreeFitter
configure_tools(dttOm,   isMC)
configure_tools(dttXi,   isMC)
configure_tools(dttD0,   isMC)
configure_tools(dttOmWS, isMC)
configure_tools(dttXiWS, isMC)


# Configure DaVinci
selseqOm   = SelectionSequence('TupleSeqOm',   TopSelection=dttOm)
selseqXi   = SelectionSequence('TupleSeqXi',   TopSelection=dttXi)
selseqD0   = SelectionSequence('TupleSeqD0',   TopSelection=dttD0)
selseqOmWS = SelectionSequence('TupleSeqOmWS', TopSelection=dttOmWS)
selseqXiWS = SelectionSequence('TupleSeqXiWS', TopSelection=dttXiWS)
DaVinci().UserAlgorithms += [selseqOm.sequence(),
                             selseqXi.sequence(),
                             selseqD0.sequence(),
                             selseqOmWS.sequence(),
                             selseqXiWS.sequence()]
DaVinci().TupleFile = 'DVNTuple.root'
#DaVinci().EvtMax = 500000
