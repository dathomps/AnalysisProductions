# Lc2pKpi ntuple options

This is an options file for the creation of nTuples for $\Lambda_C \to p K \pi$

## Status

Currently we run over data only, in one magnet polarity (MagDown) for one year (2017) to establish if this is a viable replacement for our Ganga scripts.

We ask for both a Turbo and a Stripping version of data - so far have struggled to find all necessary Trigger info via Turbo which we are hopeful Analysis Profuctions can resolve for us. We expect for Run 2 to be able to use one or the other in our analysis eventually.

## Log
* v0r0p? - Initial configuration to contain as a minimum the same Tuple functionality as previous code.
