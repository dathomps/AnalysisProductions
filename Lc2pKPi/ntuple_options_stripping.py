from Configurables import (
    DaVinci,
    DecayTreeTuple,
    LHCbApp,
    TupleToolDecay
)
from DecayTreeTuple.Configuration import *
from PhysConf.Filters import LoKi_Filters

# Stream and stripping line we want to use
stream = 'Charm'
line = 'LambdaCForPromptCharm'
inputtype = 'MDST'

# Create an ntuple to capture our decays from the StrippingLine line
dtt = DecayTreeTuple('Tuple_Lc2pKpi')
dtt.Inputs = ['Phys/{0}/Particles'.format(line)] 
dtt.Decay = '[Lambda_c+ -> ^p+ ^K- ^pi+]CC'
dtt.addBranches({ 'lcplus' : '[Lambda_c+ -> p+ K- pi+]CC',
                            'pplus'  : '[Lambda_c+ -> ^p+ K- pi+]CC',
                            'kminus' : '[Lambda_c+ -> p+ ^K- pi+]CC',
                            'piplus' : '[Lambda_c+ -> p+ K- ^pi+]CC' })

dtt.addTupleTool('TupleToolDecayTreeFitter/PVConstrainedDTF')
dtt.PVConstrainedDTF.constrainToOriginVertex = True


# add some useful tools
dtt.ToolList = []
dtt.ToolList += ["TupleToolKinematic",  # Mass and momenta
                 "TupleToolPid",        # PID info
                 "TupleToolANNPID",     # ProbNN for specific MC tunes
                 "TupleToolGeometry",   # ENDVERTEX, OWNPV, IP, FD, DIRA
                 "TupleToolAngles",     # CosTheta, angle between daughter tracks
                 "TupleToolEventInfo",  # Runnr, eventnr, gpstime, magpol, BX
                 "TupleToolPropertime", # Proper lifetime TAU in ns
                 "TupleToolTrackInfo",  # TRACK info
                 "TupleToolPrimaries",  # nPV, PV pos, PVnTracks
                 "TupleToolRecoStats"]  # nPVs, nTracks, etc.

tistostool = dtt.addTupleTool("TupleToolTISTOS")
tistostool.VerboseL0 = True
tistostool.VerboseHlt1 = True
tistostool.VerboseHlt2 = True
tistostool.TriggerList = ["Hlt1TrackAllL0Decision", "Hlt1TrackMVADecision",
 "Hlt2CharmHadD2HHHDecision", "Hlt2CharmHadLambdaC2KPPiDecision",
 "Hlt2CharmHadLcpToPpKmPipTurboDecision", "Hlt2CharmHadXicpToPpKmPipTurboDecision",
 "L0HadronDecision","L0MuonDecision","L0ElectronDecision"]

#add custom variables with functor
hybridtool = dtt.addTupleTool('LoKi::Hybrid::TupleTool')
hybridtool.Variables = {'ETA' : 'ETA' ,
                            'PHI' : 'PHI',
                            'RAPIDITY' : 'Y',
                            'TIP' : '1e3 * (PX * (VFASPF(VY)-BPV(VY)) - PY * (VFASPF(VX)-BPV(VX))) / sqrt(PX*PX + PY*PY)'
                        }

filters = LoKi_Filters(
        HLT2_Code = " HLT_PASS_RE('Hlt2Charm.*Decision') ",
        STRIP_Code = "HLT_PASS_RE('Stripping{}Decision')".format(line)
        )
DaVinci.EventPreFilters = filters.filters("Filters")





# refit PVs with exclusion of our tracks of interest
dtt.ReFitPVs = True


# Configure DaVinci
if inputtype == 'MDST':
    DaVinci().RootInTES = "/Event/{0}".format(stream)


DaVinci().UserAlgorithms += [dtt]

DaVinci().InputType = inputtype
DaVinci().Simulation = False
DaVinci().Lumi = True
DaVinci().PrintFreq = 1000
DaVinci().EvtMax = -1
