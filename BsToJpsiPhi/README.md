# BsToJpsiPhi ntuple options

Options files for creating ntuples for the measurement of $`\phi_{s}`$ using
$`B_{s}^{0} \to J/\psi\phi`$ decays.

## Datasets

Currently, options are provided for running over all Run 2 data, 2015–2018. The
data streams used depend on the decay mode.

* DIMUON
  * $`B_{s}^{0} \to J/\psi\phi`$ (signal mode)
  * $`B^{0} \to J/\psi K^{*0}`$ (control mode)
  * $`B^{+} \to J/\psi K^{+}`$
* LEPTONIC
  * $`B_{s}^{0} \to J/\psi\phi`$ with prompt $`J/\psi`$
* BHADRONCOMPLETEEVENT
  * $`B_{s}^{0} \to D_{s}^{\mp}\pi^{\pm}`$
  * $`B_{s}^{0} \to D_{s}^{\mp}\pi^{\pm}`$ with Prompt $'D_s'$

## Monte Carlo samples

* 13144011: $`B_{s}^{0} \to J/\psi\phi`$ (nominal signal sample)
* 24142001: Inclusive $`J/\psi`$ for $`B_{s}^{0} \to J/\psi\phi`$ with prompt
  $`J/\psi`$
* 13144004: $`B_{s}^{0} \to J/\psi\phi`$ (with $`\Delta\Gamma = 0`$)
#* 13144016: $`B_{s}^{0} \to J/\psi\phi`$ (with $`\Delta\Gamma = 0`$ and a 20x
#  larger lifetime) - this is depricated
* 11144001: $`B^{0} \to J/\psi K^{*0}`$
* 12143001: $`B^{+} \to J/\psi K^{+}`$
* 13264021: $`B_{s}^{0} \to D_{s}^{\mp}\pi^{\pm}`$
* 15144001: $`\Lambda_{b}^{0} \to J/\psi p K^{-}`$

