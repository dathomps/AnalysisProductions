import os
import sys
sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'BsToJpsiPhi'))

from Configurables import DaVinci

from helpers.stripping import stripping

#jsut a precaution for now. The script is only used for 2016/2015 restripping. 
data_type = DaVinci().DataType

oldline = False if data_type=='2018' else True  
stripping_line = 'BetaSBs2DsPiDetachedLine' if oldline==False else 'B02DPiD2HHHBeauty2CharmLine' 

seq = stripping(data_type, stripping_line)
DaVinci().UserAlgorithms = [seq]
DaVinci().EvtMax = -1
