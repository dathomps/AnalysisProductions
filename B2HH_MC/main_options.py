from Gaudi.Configuration import *
from Configurables import DaVinci, CondDB

from Configurables import TupleToolDecay, DecayTreeTuple, LoKi__Hybrid__TupleTool, TupleToolDecayTreeFitter

LoKiTool = LoKi__Hybrid__TupleTool('LoKiTool')
preseltuple = DecayTreeTuple("PreSelB2HH")
preseltuple.Decay = "B0 -> ^pi+ ^pi-"
preseltuple.Inputs = [ "/Event/B2HH.Strip/Phys/B2HHBDTLine/Particles" ]
preseltuple.ToolList +=  [ "LoKi::Hybrid::TupleTool/LoKiTool"
                           ,"TupleToolKinematic"
                           ,"TupleToolGeometry"
                           ,"TupleToolPid"
                           ,"TupleToolPrimaries"
                           ,"TupleToolPropertime"
                           ,"TupleToolTrackInfo"
                           ,"TupleToolEventInfo"
                           ,"TupleToolTISTOS"
                           ,"TupleToolRecoStats"
                           ,"TupleToolTagging"
                           ,"TupleToolMCTruth"
                           ,"MCTupleToolPrimaries"
                           ,"TupleToolMCBackgroundInfo"
                         ]
preseltuple.addTool(LoKiTool)
preseltuple.LoKiTool.Variables = {
    "RichPIDk" : "PPINFO(LHCb.ProtoParticle.RichDLLk,-1000)"
    ,"RichPIDp" : "PPINFO(LHCb.ProtoParticle.RichDLLp,-1000)"
    ,"DOCA" : "DOCAMAX"}
from Configurables import TupleToolGeometry, TupleToolRecoStats, TupleToolKinematic, TupleToolTISTOS, TupleToolTagging, TupleToolStripping
preseltuple.addTool(TupleToolGeometry())
preseltuple.TupleToolGeometry.Verbose = True
preseltuple.addTool(TupleToolRecoStats())
preseltuple.TupleToolRecoStats.Verbose = True
preseltuple.addTool(TupleToolKinematic())
preseltuple.TupleToolKinematic.Verbose = True

#tt_tagging = preseltuple.addTupleTool(TupleToolTagging())
tt_tagging = preseltuple.addTool(TupleToolTagging())
tt_tagging.Verbose = True
tt_tagging.UseFTfromDST = False
tt_tagging.AddMVAFeatureInfo = False
tt_tagging.AddTagPartsInfo = False
from Configurables import BTaggingTool
btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool")
from FlavourTagging.Tunings import applyTuning as applyFTTuning
applyFTTuning(btagtool, tuning_version="Summer2017Optimisation_v4_Run2")
tt_tagging.TaggingToolName = btagtool.getFullName()

preseltuple.addTool(TupleToolTISTOS())
preseltuple.TupleToolTISTOS.Verbose = True
preseltuple.TupleToolTISTOS.VerboseL0 = True
preseltuple.TupleToolTISTOS.VerboseHlt1 = True
preseltuple.TupleToolTISTOS.VerboseHlt2 = True
preseltuple.TupleToolTISTOS.TriggerList = [ 'L0DiMuonDecision',
                                            'L0ElectronDecision',
                                            'L0HadronDecision',
                                            'L0MuonDecision',
                                            'L0JetElDecision',
                                            'L0JetPhDecision',
                                            'L0MuonEWDecision',
                                            'L0PhotonDecision',
                                            ###########################
                                            'Hlt1TrackMVADecision',
                                            'Hlt1TrackMuonDecision',
                                            'Hlt1TwoTrackMVADecision',
                                            ###########################
                                            'Hlt2B2HH_B2HHDecision',
                                            'Hlt2B2HHDecision',
                                            'Hlt2Topo2BodyDecision']
preseltuple.Branches = {"B0" : "B0 -> pi+ pi-"}
preseltuple.addTool(TupleToolDecay("B0"))
preseltuple.B0.ToolList += ["TupleToolDecayTreeFitter/MPiPi"
                           ,"TupleToolDecayTreeFitter/MKK"
                           ,"TupleToolDecayTreeFitter/MKPi"
                           ,"TupleToolDecayTreeFitter/MPiK"
                           ,"TupleToolDecayTreeFitter/MPK"
                           ,"TupleToolDecayTreeFitter/MKP"
                           ,"TupleToolDecayTreeFitter/MPPi"
                           ,"TupleToolDecayTreeFitter/MPiP"]
dtfPiPi = TupleToolDecayTreeFitter("MPiPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True)
dtfKK = TupleToolDecayTreeFitter("MKK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "K+"
                                                 ,"Beauty ->  X+  ^pi-" : "K-"})
dtfKPi = TupleToolDecayTreeFitter("MKPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  pi-" : "K+"})
dtfPiK = TupleToolDecayTreeFitter("MPiK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty ->  pi+ ^pi-" : "K-"})
dtfPK = TupleToolDecayTreeFitter("MPK"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "p+"
                                                 ,"Beauty ->  X+  ^pi-" : "K-"})
dtfKP = TupleToolDecayTreeFitter("MKP"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "K+"
                                                 ,"Beauty ->  X+  ^pi-" : "p~-"})
dtfPPi = TupleToolDecayTreeFitter("MPPi"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "p+"
                                                 ,"Beauty ->  X+  ^pi-" : "pi-"})
dtfPiP = TupleToolDecayTreeFitter("MPiP"
                                ,Verbose = True
                                ,constrainToOriginVertex = True
                                ,Substitutions = {"Beauty -> ^pi+  X-"  : "pi+"
                                                 ,"Beauty ->  X+  ^pi-" : "p~-"})
preseltuple.B0.addTool(dtfPiPi)
preseltuple.B0.addTool(dtfKK)
preseltuple.B0.addTool(dtfKPi)
preseltuple.B0.addTool(dtfPiK)
preseltuple.B0.addTool(dtfPK)
preseltuple.B0.addTool(dtfKP)
preseltuple.B0.addTool(dtfPPi)
preseltuple.B0.addTool(dtfPiP)
#preseltuple.RootInTES = "/Event/Bhadron"
from Configurables import TupleToolMCTruth
preseltuple.addTool(TupleToolMCTruth())
preseltuple.TupleToolMCTruth.ToolList += ["MCTupleToolHierarchy"
                                         ,"MCTupleToolKinematic"
                                         ]

preseltuple.TupleName = "PreSelB2HH"

from Configurables import GaudiSequencer
myseq = GaudiSequencer("MySeq")
myseq.Members += [ preseltuple ]

from Configurables import LoKi__HDRFilter as StripFilter

_StripFilter = StripFilter( 'StripFilter',
                            Code="HLT_PASS('StrippingB2HHBDTLineDecision')",
                            Location="/Event/Strip/Phys/DecReports" )

dv = DaVinci()
dv.EventPreFilters = [ _StripFilter ]
dv.Lumi = False
dv.EvtMax = -1
dv.PrintFreq = 1000
dv.InputType = "DST"
dv.Simulation = True
dv.TupleFile = "b2hh.root"

dv.UserAlgorithms = [ myseq ]


def _configure_output_ () :

    outputfile = 'StripB2HHBDTLine.dst'
    ##
    from Gaudi.Configuration import allConfigurables
    fakew  = allConfigurables.get('MyDSTWriter',None)
    if fakew and 'Sel' != fakew.OutputFileSuffix :
        outputfile = fakew.OutputFileSuffix + '.' + outputfile

    ##for i in range(1) :
    ##    print 'I AM POST   ACTION!!', outputfile

    ##
    from GaudiConf import IOHelper
    ioh    = IOHelper        ( 'ROOT'     , 'ROOT' )
    oalgs  = ioh.outputAlgs  ( outputfile , 'InputCopyStream/DSTFilter' )

    writer = oalgs[0]
    writer.AcceptAlgs = [ _StripFilter ]

    from Configurables import GaudiSequencer
    oseq  = GaudiSequencer ( 'WRITEOUTPUT', Members = oalgs )

    from Configurables import ApplicationMgr
    AM    = ApplicationMgr ()

    if not AM.OutStream :
        AM.OutStream =[]

        AM.OutStream.append ( oseq )


#from Gaudi.Configuration import appendPostConfigAction
#appendPostConfigAction ( _configure_output_ )

