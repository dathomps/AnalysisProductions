# Helper file to define data type for davinci

year      = '2015'
fileType  = 'MDST'
rootintes = "/Event/AllStreams"

from Configurables import DaVinci, CondDB
dv = DaVinci (  DataType                  = year           ,
                InputType                 = fileType       ,
                RootInTES                 = rootintes      ,
                Simulation                = True,
                Lumi                      = False,
                DDDBtag                   = "dddb-20150724",
                CondDBtag                 = "sim-20161124-vc-md100"
             )

