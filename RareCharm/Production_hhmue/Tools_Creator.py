from Gaudi.Configuration import *
MessageSvc().Format = "% F%80W%S%7W%R%T %0W%M"
from Configurables import DaVinci, PrintDecayTree
from Configurables import LoKi__HDRFilter as StripFilter
# get classes to build the SelectionSequence
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence
from PhysConf.Selections import MomentumScaling, TupleSelection
from PhysConf.Filters import LoKi_Filters


# Filter the Candidate
from Configurables import FilterDesktop

#DecayTreeTuple
from Configurables import DecayTreeTuple
from Configurables import TupleToolTrigger
from Configurables import TupleToolGeometry
from Configurables import TupleToolKinematic
from Configurables import TupleToolPropertime
from Configurables import TupleToolPrimaries
from Configurables import TupleToolEventInfo
from Configurables import TupleToolPid
from Configurables import TupleToolMuonPid
#from Configurables import TupleToolRICHPid
from Configurables import TupleToolTrackInfo
from Configurables import TupleToolDecay
from Configurables import TupleToolP2VV
from Configurables import TupleToolTISTOS
from Configurables import TupleToolL0Calo
from Configurables import TupleToolTrackIsolation
from Configurables import TupleToolDecayTreeFitter
from Configurables import TupleToolANNPID
from Configurables import TupleToolSubMass
from Configurables import EventNodeKiller
from Configurables import TupleToolIsolationTwoBody

from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__EvtTupleTool
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict

from Configurables import TupleToolBremInfo
from Configurables import TupleToolProtoPData
# from Configurables import TupleToolHOP

from DecayTreeTuple.Configuration import *
# from LoKiPhys.decorators import BPVHOPM, HOPM



##---------------------------------------------------------------#
# For MC : 
#    1) TruthTruple and various tools
#       the right Decay Descriptor is needed (end of this file)!
#    2) use AddToolMCTruth(ntuple) function for the right tuple
##---------------------------------------------------------------#


from Configurables import MCDecayTreeTuple
from Configurables import MCTupleToolKinematic
from Configurables import MCTupleToolHierarchy
from Configurables import MCTupleToolReconstructed
from Configurables import TupleToolMCTruth
from Configurables import TupleToolMCBackgroundInfo
from Configurables import LoKi__Hybrid__MCTupleTool 
from Configurables import MCMatchObjP2MCRelator
#import DecayTreeTuple.Configuration

#workaround for a bug (from lhcb-davinci mailing list)
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['DAQ','pRec']


ntuple          = DecayTreeTuple()



##### Few things are common to all ntuples  #####

# Tools in the ntuples
ntuple.ToolList = [
    "TupleToolEventInfo"
    ,"TupleToolKinematic"
    ,"TupleToolMuonPid"
    ,"TupleToolPrimaries"
    ,"TupleToolPropertime"
    ,"TupleToolL0Calo"
    ,"TupleToolBremInfo"
    ,"TupleToolProtoPData"
    # ,"TupleToolHOP"
    #,"TupleToolPid"
    #,"TupleToolGeometry"
    #,"TupleToolANNPID"
    #,"TupleToolTrigger" ###
    #,"TupleToolRICHPid"
    #,"TupleToolTrackInfo"
    #,"TupleToolRecoStats"
    #,"TupleToolTISTOS"
    ]
    





##---------------------------------------------------#
## Useful and not default variables (Loki + RELINFO) 
##---------------------------------------------------#

def AddLoKi_All_hhmumu(ntuple):
    my_LoKi_All_hhmumu_tool = ntuple.D.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_All_hhmumu')
    my_LoKi_All_hhmumu_tool.Variables={
        "Loki_DiLepton_Mass"  : "M34"
        ,"Loki_DiHadron_Mass" : "M12"
        ,"Loki_h0_l0_Mass"    : "MASS(1,3)"                                                                                                                                                                                                  
        ,"Loki_h0_l1_Mass"    : "MASS(1,4)"                                                                                                                                                                                                  
        ,"Loki_h1_l0_Mass"    : "MASS(2,3)"                                                                                                                                                                                                  
        ,"Loki_h1_l1_Mass"    : "MASS(2,4)"                                                                                                                                                                                                  
        ,"Loki_h0_h1_l0_Mass" : "MASS(1,2,3)"                                                                                                                                                                                                
        ,"Loki_h0_h1_l1_Mass" : "MASS(1,2,4)"                                                                                                                                                                                                
        ,"Loki_h0_l0_l1_Mass" : "MASS(1,3,4)"                                                                                                                                                                                                
        ,"Loki_h1_l0_l1_Mass" : "MASS(2,3,4)"                                                                                                                                                                                                
        ,"Loki_h0_ID"         : "CHILD(ID,1)"
        ,"Loki_h0_KEY"        : "CHILD(KEY,1)"
        ,"Loki_h0_Q"          : "CHILD(Q,1)"
        ,"Loki_h1_ID"         : "CHILD(ID,2)"
        ,"Loki_h1_KEY"        : "CHILD(KEY,2)"
        ,"Loki_h1_Q"          : "CHILD(Q,2)"
        ,"Loki_l0_ID"         : "CHILD(ID,3)"
        ,"Loki_l0_KEY"        : "CHILD(KEY,3)"
        ,"Loki_l0_Q"          : "CHILD(Q,3)"
        ,"Loki_l1_ID"         : "CHILD(ID,4)"
        ,"Loki_l1_KEY"        : "CHILD(KEY,4)"
        ,"Loki_l1_Q"          : "CHILD(Q,4)"
        ,"Loki_LV01"          : "LV01"
        ,"Loki_LV02"          : "LV02"
        ,"Loki_LV03"          : "LV03"
        ,"Loki_LV04"          : "LV04"                                                                                                                                                                                                       
        ,"Loki_MAXDOCA"       : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
        ,"Loki_h0_h1_DOCA"    : "DOCA(1,2)"
        ,"Loki_l0_l1_DOCA"    : "DOCA(3,4)"
        ,"Loki_h0_l0_DOCA"    : "DOCA(1,3)"                                                                                                                                                                                                  
        ,"Loki_h0_l1_DOCA"    : "DOCA(1,4)"                                                                                                                                                                                                  
        ,"Loki_h1_l0_DOCA"    : "DOCA(2,3)"                                                                                                                                                                                                  
        ,"Loki_h1_l1_DOCA"    : "DOCA(2,4)"                                                                                                                                                                                                  
        ,"LoKi_FDCHI2"        : "BPVVDCHI2"
        ,"LoKi_FDS"           : "BPVDLS"

        }
        
def AddLoKi_All_hhhh(ntuple):
    my_LoKi_All_hhhh_tool = ntuple.D.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_All_hhhh')
    my_LoKi_All_hhhh_tool.Variables={
         "Loki_h0_h2_Mass"    : "MASS(1,3)"
        ,"Loki_h0_h3_Mass"    : "MASS(1,4)"
        ,"Loki_h1_h2_Mass"    : "MASS(2,3)"
        ,"Loki_h1_h3_Mass"    : "MASS(2,4)"
        ,"Loki_h0_h1_h2_Mass" : "MASS(1,2,3)"
        ,"Loki_h0_h1_h3_Mass" : "MASS(1,2,4)"
        ,"Loki_h0_h2_h3_Mass" : "MASS(1,3,4)"
        ,"Loki_h1_h2_h3_Mass" : "MASS(2,3,4)"
        ,"Loki_h0_ID"         : "CHILD(ID,1)"
        ,"Loki_h0_KEY"        : "CHILD(KEY,1)"
        ,"Loki_h0_Q"          : "CHILD(Q,1)"
        ,"Loki_h1_ID"         : "CHILD(ID,2)"
        ,"Loki_h1_KEY"        : "CHILD(KEY,2)"
        ,"Loki_h1_Q"          : "CHILD(Q,2)"
        ,"Loki_h2_ID"         : "CHILD(ID,3)"
        ,"Loki_h2_KEY"        : "CHILD(KEY,3)"
        ,"Loki_h2_Q"          : "CHILD(Q,3)"
        ,"Loki_h3_ID"         : "CHILD(ID,4)"
        ,"Loki_h3_KEY"        : "CHILD(KEY,4)"
        ,"Loki_h3_Q"          : "CHILD(Q,4)"
        ,"Loki_MAXDOCA"       : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
        }


def AddLokiCone_MC(ntuple):
    my_LoKiCone_tool = ntuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone')
    my_LoKiCone_tool.Variables={
        "CONEANGLE_D_11"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEANGLE',-1.)",
        "CONEMULT_D_11"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEMULT', -1.)",
        "CONEPTASYM_D_11"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_11"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEANGLE',-1.)",
        "CONEMULT_Dstar_11"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_11" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEPTASYM',-1.)",
        "CONEANGLE_D_13"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEANGLE',-1.)",
        "CONEMULT_D_13"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEMULT', -1.)",
        "CONEPTASYM_D_13"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_13"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEANGLE',-1.)",
        "CONEMULT_Dstar_13"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_13" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEPTASYM',-1.)",
        "CONEANGLE_D"         : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEANGLE',-1.)",
        "CONEMULT_D"          : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEMULT', -1.)",
        "CONEPTASYM_D"        : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEANGLE',-1.)",
        "CONEMULT_Dstar"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEMULT',-1.)",
        "CONEPTASYM_Dstar"    : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEPTASYM',-1.)",
        "CONEANGLE_D_17"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEANGLE',-1.)",
        "CONEMULT_D_17"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEMULT', -1.)",
        "CONEPTASYM_D_17"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_17"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEANGLE',-1.)",
        "CONEMULT_Dstar_17"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_17" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEPTASYM',-1.)"
        }

def AddLokiCone_DATA(ntuple):
    my_LoKiCone_tool = ntuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone')
    my_LoKiCone_tool.Variables={
        "CONEANGLE_D_11"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEANGLE',-1.)",
        "CONEMULT_D_11"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEMULT', -1.)",
        "CONEPTASYM_D_11"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD011','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_11"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEANGLE',-1.)",
        "CONEMULT_Dstar_11"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_11" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst11','CONEPTASYM',-1.)",
        "CONEANGLE_D_13"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEANGLE',-1.)",
        "CONEMULT_D_13"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEMULT', -1.)",
        "CONEPTASYM_D_13"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD013','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_13"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEANGLE',-1.)",
        "CONEMULT_Dstar_13"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_13" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst13','CONEPTASYM',-1.)",
        "CONEANGLE_D"         : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEANGLE',-1.)",
        "CONEMULT_D"          : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEMULT', -1.)",
        "CONEPTASYM_D"        : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD015','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEANGLE',-1.)",
        "CONEMULT_Dstar"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEMULT',-1.)",
        "CONEPTASYM_Dstar"    : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst15','CONEPTASYM',-1.)",
        "CONEANGLE_D_17"      : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEANGLE',-1.)",
        "CONEMULT_D_17"       : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEMULT', -1.)",
        "CONEPTASYM_D_17"     : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVD017','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_17"  : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEANGLE',-1.)",
        "CONEMULT_Dstar_17"   : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_17" : "RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHLLLine/P2CVDst17','CONEPTASYM',-1.)"
        }

def AddLokiCone_had(ntuple):
    my_LoKiCone_had_tool = ntuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone_had')
    my_LoKiCone_had_tool.Variables={
        "CONEANGLE_D_11"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEANGLE',-1.)",
        "CONEMULT_D_11"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEMULT', -1.)",
        "CONEPTASYM_D_11"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_11"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEANGLE',-1.)",
        "CONEMULT_Dstar_11"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_11" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEPTASYM',-1.)",
        "CONEANGLE_D_13"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEANGLE',-1.)",
        "CONEMULT_D_13"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEMULT', -1.)",
        "CONEPTASYM_D_13"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_13"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEANGLE',-1.)",
        "CONEMULT_Dstar_13"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_13" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEPTASYM',-1.)",
        "CONEANGLE_D"         : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEANGLE',-1.)",
        "CONEMULT_D"          : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEMULT', -1.)",
        "CONEPTASYM_D"        : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEANGLE',-1.)",
        "CONEMULT_Dstar"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEMULT',-1.)",
        "CONEPTASYM_Dstar"    : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEPTASYM',-1.)",
        "CONEANGLE_D_17"      : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEANGLE',-1.)",
        "CONEMULT_D_17"       : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEMULT', -1.)",
        "CONEPTASYM_D_17"     : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEPTASYM',-1.)",
        "CONEANGLE_Dstar_17"  : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEANGLE',-1.)",
        "CONEMULT_Dstar_17"   : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEMULT',-1.)",
        "CONEPTASYM_Dstar_17" : "RELINFO('/Event/AllStreams/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEPTASYM',-1.)"
        }

def AddLokiCone_had_SINFO(ntuple):
    my_LoKiCone_had_sinfo_tool = ntuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone_had_sinfo')
    my_LoKiCone_had_sinfo_tool.Variables={
        "CONEANGLE_D_11"      : "INFO(9014, -1.)",
        "CONEMULT_D_11"       : "INFO(9025, -1.)",
        "CONEPTASYM_D_11"     : "INFO(9036, -1.)",
        "CONEANGLE_Dstar_11"  : "INFO(9047, -1.)",
        "CONEMULT_Dstar_11"   : "INFO(9058, -1.)",
        "CONEPTASYM_Dstar_11" : "INFO(9069, -1.)",
        "CONEANGLE_D_13"      : "INFO(9080, -1.)",
        "CONEMULT_D_13"       : "INFO(9091, -1.)",
        "CONEPTASYM_D_13"     : "INFO(9102, -1.)",
        "CONEANGLE_Dstar_13"  : "INFO(9113, -1.)",
        "CONEMULT_Dstar_13"   : "INFO(9124, -1.)",
        "CONEPTASYM_Dstar_13" : "INFO(9135, -1.)",
        "CONEANGLE_D"         : "INFO(9146, -1.)",
        "CONEMULT_D"          : "INFO(9157, -1.)",
        "CONEPTASYM_D"        : "INFO(9168, -1.)",
        "CONEANGLE_Dstar"     : "INFO(9179, -1.)",
        "CONEMULT_Dstar"      : "INFO(9190, -1.)",
        "CONEPTASYM_Dstar"    : "INFO(9201, -1.)",
        "CONEANGLE_D_17"      : "INFO(9212, -1.)",
        "CONEMULT_D_17"       : "INFO(9223, -1.)",
        "CONEPTASYM_D_17"     : "INFO(9234, -1.)",
        "CONEANGLE_Dstar_17"  : "INFO(9245, -1.)",
        "CONEMULT_Dstar_17"   : "INFO(9256, -1.)",
        "CONEPTASYM_Dstar_17" : "INFO(9267, -1.)",
        "D_Loki_MAXDOCA"      : "INFO(9278, -1.)",
        "Dst_Loki_MAXDOCA"    : "INFO(9289, -1.)"
        }


##------------------------------------------#
## DTF dictionaries Configuration
##------------------------------------------#

def Add_DTF_variables_new_tool(ntuple):
    DictTuple = ntuple.Dst.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
    DictTuple.addTool(DTFDict,"DTF")
    DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
    DictTuple.NumVar = 150 
    DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors,"dict")
    DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"

    DictTuple.DTF.dict.Variables = {

          "DTF_CHI2"            : "DTF_CHI2(True)"
        , "DTF_NDOF"          : "DTF_NDOF(True)"
        , "DTF_Dstarplus_ID"  : "ID"
        , "DTF_Dstarplus_KEY" : "KEY"
        , "DTF_Dstarplus_Q"   : "Q"
        , "DTF_Dstarplus_M"   : "M"
        , "DTF_Dstarplus_P"   : "P"
        , "DTF_Dstarplus_PT"  : "PT"
        , "DTF_Dstarplus_E"   : "E"
        , "DTF_Dstarplus_PX"  : "PX"
        , "DTF_Dstarplus_PY"  : "PY"
        , "DTF_Dstarplus_PZ"  : "PZ"
        , "DTF_D0_ID"         : "CHILD(ID,1)"
        , "DTF_D0_KEY"        : "CHILD(KEY,1)"
        , "DTF_D0_M"          : "CHILD(M,1)"
        , "DTF_D0_P"          : "CHILD(P, 1)"
        , "DTF_D0_PT"         : "CHILD(PT, 1)"
        , "DTF_D0_E"          : "CHILD(E, 1)"
        , "DTF_D0_PX"         : "CHILD(PX, 1)"
        , "DTF_D0_PY"         : "CHILD(PY, 1)"
        , "DTF_D0_PZ"         : "CHILD(PZ, 1)"
        , "DTF_D0_BPVIPCHI2"  : "CHILDFUN(BPVIPCHI2(), 1)"
        #, "DTF_D0_CTAU"       : "CTAU(1,TRUE)"
        , "DTF_Pis_ID"        : "CHILD(ID,2)"
        , "DTF_Pis_KEY"       : "CHILD(KEY,2)"
        , "DTF_Pis_Q"         : "CHILD(Q,2)"
        , "DTF_Pis_M"         : "CHILD(M,2)"
        , "DTF_Pis_P"         : "CHILD(P, 2)"
        , "DTF_Pis_PT"        : "CHILD(PT, 2)"
        , "DTF_Pis_E"         : "CHILD(E, 2)"
        , "DTF_Pis_PX"        : "CHILD(PX, 2)"
        , "DTF_Pis_PY"        : "CHILD(PY, 2)"
        , "DTF_Pis_PZ"        : "CHILD(PZ, 2)"
        , "DTF_Pis_BPVIPCHI2" : "CHILDFUN(BPVIPCHI2(), 2)"
        , "DTF_h0_ID"         : "CHILD(CHILD(ID,1),1)"
        , "DTF_h0_KEY"        : "CHILD(CHILD(KEY,1),1)"
        , "DTF_h0_Q"          : "CHILD(CHILD(Q,1),1)"
        , "DTF_h0_P"          : "CHILD(CHILD(P,1),1)"
        , "DTF_h0_PT"         : "CHILD(CHILD(PT,1),1)"
        , "DTF_h0_E"          : "CHILD(CHILD(E,1),1)"
        , "DTF_h0_PX"         : "CHILD(CHILD(PX,1),1)"
        , "DTF_h0_PY"         : "CHILD(CHILD(PY,1),1)"
        , "DTF_h0_PZ"         : "CHILD(CHILD(PZ,1),1)"
        , "DTF_h1_ID"         : "CHILD(CHILD(ID,2),1)"
        , "DTF_h1s_KEY"       : "CHILD(CHILD(KEY,2),1)"
        , "DTF_h1_Q"          : "CHILD(CHILD(Q,2),1)"
        , "DTF_h1_P"          : "CHILD(CHILD(P,2),1)"
        , "DTF_h1_PT"         : "CHILD(CHILD(PT,2),1)"
        , "DTF_h1_E"          : "CHILD(CHILD(E,2),1)"
        , "DTF_h1_PX"         : "CHILD(CHILD(PX,2),1)"
        , "DTF_h1_PY"         : "CHILD(CHILD(PY,2),1)"
        , "DTF_h1_PZ"         : "CHILD(CHILD(PZ,2),1)"
        , "DTF_l0_ID"         : "CHILD(CHILD(ID,3),1)"
        , "DTF_l0_KEY"        : "CHILD(CHILD(KEY,3),1)"
        , "DTF_l0_Q"          : "CHILD(CHILD(Q,3),1)"
        , "DTF_l0_P"          : "CHILD(CHILD(P,3),1)"
        , "DTF_l0_PT"         : "CHILD(CHILD(PT,3),1)"
        , "DTF_l0_E"          : "CHILD(CHILD(E,3),1)"
        , "DTF_l0_PX"         : "CHILD(CHILD(PX,3),1)"
        , "DTF_l0_PY"         : "CHILD(CHILD(PY,3),1)"
        , "DTF_l0_PZ"         : "CHILD(CHILD(PZ,3),1)"
        , "DTF_l1_ID"         : "CHILD(CHILD(P,4),1)"
        , "DTF_l1_KEY"        : "CHILD(CHILD(P,4),1)"
        , "DTF_l1_Q"          : "CHILD(CHILD(Q,4),1)"
        , "DTF_l1_P"          : "CHILD(CHILD(P,4),1)"
        , "DTF_l1_PT"         : "CHILD(CHILD(PT,4),1)"
        , "DTF_l1_E"          : "CHILD(CHILD(E,4),1)"
        , "DTF_l1_PX"         : "CHILD(CHILD(PX,4),1)"
        , "DTF_l1_PY"         : "CHILD(CHILD(PY,4),1)"
        , "DTF_l1_PZ"         : "CHILD(CHILD(PZ,4),1)"
        , "Loki_DOCAMAX"      : "DOCAMAX"
        , "Loki_MAXDOCA"      : "LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator'))"
        , "DTF_deltaM"        : "M - CHILDFUN(M,'D0'==ABSID)"
        , "DTF_DiLepton_Mass" : "CHILD(M34,1)"
        , "DTF_DiHadron_Mass" : "CHILD(M12,1)"
        , "DTF_h0_l0_Mass"    : "CHILD(MASS(1,3),1)" 
        , "DTF_h0_l1_Mass"    : "CHILD(MASS(1,4),1)"
        , "DTF_h1_l0_Mass"    : "CHILD(MASS(2,3),1)"
        , "DTF_h1_l1_Mass"    : "CHILD(MASS(2,4),1)"
        , "DTF_h0_h1_l0_Mass" : "CHILD(MASS(1,2,3),1)"
        , "DTF_h0_h1_l1_Mass" : "CHILD(MASS(1,2,4),1)"
        , "DTF_h0_l0_l1_Mass" : "CHILD(MASS(1,3,4),1)"
        , "DTF_h1_l0_l1_Mass" : "CHILD(MASS(2,3,4),1)"

    
        
        }


def Add_DTF_variables_new_tool_D0_costr(ntuple):
    DictTuple_D0_costr = ntuple.Dst.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple_D0constr")
    DictTuple_D0_costr.addTool(DTFDict,"DTF_D0constr")
    DictTuple_D0_costr.Source = "LoKi::Hybrid::DTFDict/DTF_D0constr"
    DictTuple_D0_costr.NumVar = 150 
    DictTuple_D0_costr.DTF_D0constr.addTool(LoKi__Hybrid__DictOfFunctors,"dict_D0constr")
    DictTuple_D0_costr.DTF_D0constr.Source = "LoKi::Hybrid::DictOfFunctors/dict_D0constr"

    DictTuple_D0_costr.DTF_D0constr.daughtersToConstrain = ["D0"]

    DictTuple_D0_costr.DTF_D0constr.dict_D0constr.Variables = {

          

         "DTF_CHI2_D0constr"            : "DTF_CHI2(True,'D0')"
        , "DTF_NDOF_D0constr"          : "DTF_NDOF(True,'D0')"
        , "DTF_Dstarplus_M_D0constr"   : "M"
        , "DTF_Dstarplus_P_D0constr"   : "P"
        , "DTF_Dstarplus_PT_D0constr"  : "PT"
        , "DTF_Dstarplus_E_D0constr"   : "E"
        , "DTF_Dstarplus_PX_D0constr"  : "PX"
        , "DTF_Dstarplus_PY_D0constr"  : "PY"
        , "DTF_Dstarplus_PZ_D0constr"  : "PZ"
        , "DTF_D0_M_D0constr"          : "CHILD(M,1)"
        , "DTF_D0_P_D0constr"          : "CHILD(P, 1)"
        , "DTF_D0_PT_D0constr"         : "CHILD(PT, 1)"
        , "DTF_D0_E_D0constr"          : "CHILD(E, 1)"
        , "DTF_D0_PX_D0constr"         : "CHILD(PX, 1)"
        , "DTF_D0_PY_D0constr"         : "CHILD(PY, 1)"
        , "DTF_D0_PZ_D0constr"         : "CHILD(PZ, 1)"
        , "DTF_D0_BPVIPCHI2_D0constr"  : "CHILDFUN(BPVIPCHI2(), 1)"
        , "DTF_Pis_M_D0constr"         : "CHILD(M,2)"
        , "DTF_Pis_P_D0constr"         : "CHILD(P, 2)"
        , "DTF_Pis_PT_D0constr"        : "CHILD(PT, 2)"
        , "DTF_Pis_E_D0constr"         : "CHILD(E, 2)"
        , "DTF_Pis_PX_D0constr"        : "CHILD(PX, 2)"
        , "DTF_Pis_PY_D0constr"        : "CHILD(PY, 2)"
        , "DTF_Pis_PZ_D0constr"        : "CHILD(PZ, 2)"
        , "DTF_Pis_BPVIPCHI2_D0constr" : "CHILDFUN(BPVIPCHI2(), 2)"
        , "DTF_h0_P_D0constr"          : "CHILD(CHILD(P,1),1)"
        , "DTF_h0_PT_D0constr"         : "CHILD(CHILD(PT,1),1)"
        , "DTF_h0_E_D0constr"          : "CHILD(CHILD(E,1),1)"
        , "DTF_h0_PX_D0constr"         : "CHILD(CHILD(PX,1),1)"
        , "DTF_h0_PY_D0constr"         : "CHILD(CHILD(PY,1),1)"
        , "DTF_h0_PZ_D0constr"         : "CHILD(CHILD(PZ,1),1)"
        , "DTF_h1_P_D0constr"          : "CHILD(CHILD(P,2),1)"
        , "DTF_h1_PT_D0constr"         : "CHILD(CHILD(PT,2),1)"
        , "DTF_h1_E_D0constr"          : "CHILD(CHILD(E,2),1)"
        , "DTF_h1_PX_D0constr"         : "CHILD(CHILD(PX,2),1)"
        , "DTF_h1_PY_D0constr"         : "CHILD(CHILD(PY,2),1)"
        , "DTF_h1_PZ_D0constr"         : "CHILD(CHILD(PZ,2),1)"
        , "DTF_l0_P_D0constr"          : "CHILD(CHILD(P,3),1)"
        , "DTF_l0_PT_D0constr"         : "CHILD(CHILD(PT,3),1)"
        , "DTF_l0_E_D0constr"          : "CHILD(CHILD(E,3),1)"
        , "DTF_l0_PX_D0constr"         : "CHILD(CHILD(PX,3),1)"
        , "DTF_l0_PY_D0constr"         : "CHILD(CHILD(PY,3),1)"
        , "DTF_l0_PZ_D0constr"         : "CHILD(CHILD(PZ,3),1)"
        , "DTF_l1_P_D0constr"          : "CHILD(CHILD(P,4),1)"
        , "DTF_l1_PT_D0constr"         : "CHILD(CHILD(PT,4),1)"
        , "DTF_l1_E_D0constr"          : "CHILD(CHILD(E,4),1)"
        , "DTF_l1_PX_D0constr"         : "CHILD(CHILD(PX,4),1)"
        , "DTF_l1_PY_D0constr"         : "CHILD(CHILD(PY,4),1)"
        , "DTF_l1_PZ_D0constr"         : "CHILD(CHILD(PZ,4),1)"
        , "DTF_deltaM_D0constr"        : "M - CHILDFUN(M,'D0'==ABSID)"
        ,"DTF_DiLepton_Mass_D0constr"  : "CHILD(M34,1)"
        ,"DTF_DiHadron_Mass_D0constr"  : "CHILD(M12,1)"
        ,"DTF_h0_l0_Mass_D0constr"     : "CHILD(MASS(1,3),1)" 
        ,"DTF_h0_l1_Mass_D0constr"     : "CHILD(MASS(1,4),1)"
        ,"DTF_h1_l0_Mass_D0constr"     : "CHILD(MASS(2,3),1)"
        ,"DTF_h1_l1_Mass_D0constr"     : "CHILD(MASS(2,4),1)"
        ,"DTF_h0_h1_l0_Mass_D0constr"  : "CHILD(MASS(1,2,3),1)"
        ,"DTF_h0_h1_l1_Mass_D0constr"  : "CHILD(MASS(1,2,4),1)"
        ,"DTF_h0_l0_l1_Mass_D0constr"  : "CHILD(MASS(1,3,4),1)"
        ,"DTF_h1_l0_l1_Mass_D0constr"  : "CHILD(MASS(2,3,4),1)"
        }

        
##------------------------------------------#
## GEC variables
##------------------------------------------#

def AddEvtTuple(ntuple):
    my_EvtTuple_tool = ntuple.addTupleTool('LoKi__Hybrid__EvtTupleTool/LoKi_EvtTuple')
    my_EvtTuple_tool.VOID_Variables={
        # track information
        "nLong"        : "RECSUMMARY(LHCb.RecSummary.nLongTracks      , -999, '', False )"
        ,"nUpstream"   : "RECSUMMARY(LHCb.RecSummary.nUpstreamTracks  , -999, '', False )"
        ,"nDownstream" : "RECSUMMARY(LHCb.RecSummary.nDownstreamTracks, -999, '', False )"
        ,"nBackward"   : "RECSUMMARY(LHCb.RecSummary.nBackTracks      , -999, '', False )" 
        ,"nMuon"       : "RECSUMMARY(LHCb.RecSummary.nMuonTracks      , -999, '', False )"
        ,"nVELO"       : "RECSUMMARY(LHCb.RecSummary.nVeloTracks      , -999, '', False )"
        ,"nTracks"     : "RECSUMMARY( LHCb.RecSummary.nTracks,-1,'/Event/Rec/Summary',False )"
        # pileup
        ,"nPVs"        : "RECSUMMARY(LHCb.RecSummary.nPVs, -999, '', False )"
        # tracking multiplicities
        ,"nSpdDigits"  : "RECSUMMARY(LHCb.RecSummary.nSPDhits,    -999, '', False )"
        ,"nITClusters" : "RECSUMMARY(LHCb.RecSummary.nITClusters, -999, '', False )"
        ,"nTTClusters" : "RECSUMMARY(LHCb.RecSummary.nTTClusters, -999, '', False )"
    }
    my_EvtTuple_tool.Preambulo +=['from LoKiTracks.decorators import *',
                                  'from LoKiNumbers.decorators import *',
                                  'from LoKiCore.functions  import *' ]



##------------------------------------------#
## Trigger Lines
##------------------------------------------#

l0TriggerLines=["L0HadronDecision"
           ,"L0MuonDecision"
           ,"L0DiMuonDecision"
           ,"L0ElectronDecision"
           ,"L0PhotonDecision"]

hlt1TriggerLines=['Hlt1SingleMuonHighPTDecision',
    'Hlt1MultiDiMuonNoIPDecision',
    'Hlt1DiMuonNoL0Decision',
    'Hlt1DiMuonNoIPDecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMuonNoSPDDecision',
    'Hlt1TrackMVADecision',
    'Hlt1DiMuonHighMassDecision', 
    'Hlt1DiMuonLowMassDecision', 
    'Hlt1SingleMuonNoIPDecision',  
    'Hlt1TrackAllL0Decision', 
    'Hlt1TrackMuonDecision', 
    'Hlt1TrackPhotonDecision', 
    'Hlt1L0AnyDecision', 
    'Hlt1GlobalDecision' , 
    'Hlt1TrackMVALooseDecision', 
    'Hlt1TwoTrackMVALooseDecision' , 
    'Hlt1TrackMuonMVADecision']



hlt2TriggerLines=[
        'Hlt2SingleMuonDecision',
        # 'Hlt2DiMuonDetachedDecision', 
        # 'Hlt2CharmSemilepD2HMuMuDecision', 
        # 'Hlt2CharmSemilep3bodyD2PiMuMuDecision', 
        # 'Hlt2CharmSemilep3bodyD2KMuMuDecision', 
        # 'Hlt2CharmSemilep3bodyD2PiMuMuSSDecision', 
        # 'Hlt2CharmSemilep3bodyD2KMuMuSSDecision', 
        # 'Hlt2CharmSemilepD02PiPiMuMuDecision', 
        # 'Hlt2CharmSemilepD02KKMuMuDecision',
        # 'Hlt2CharmSemilepD02KPiMuMuDecision',
        # 'Hlt2CharmHadD02HHHHDst_4piDecision',
        # 'Hlt2CharmHadD02HHHHDst_K3piDecision',
        # 'Hlt2CharmHadD02HHHHDst_KKpipiDecision',
        # 'Hlt2CharmHadD02HHXDst_hhXDecision', 
        # 'Hlt2CharmHadD02HHXDst_hhXWideMassDecision', 
        # 'Hlt2CharmHadD02HHXDst_BaryonhhXDecision', 
        # 'Hlt2CharmHadD02HHXDst_BaryonhhXWideMassDecision',
        # 'Hlt2CharmHadD02HHXDst_LeptonhhXDecision', 
        # 'Hlt2CharmHadD02HHXDst_LeptonhhXWideMassDecision',
        'Hlt2CharmHadD02HHHH_K3piDecision', 
        'Hlt2CharmHadD02HHHH_K3piWideMassDecision',
        'Hlt2CharmHadD02HHHH_KKpipiDecision', 
        'Hlt2CharmHadD02HHHH_KKpipiWideMassDecision',
        'Hlt2CharmHadD02HHHH_4piDecision', 
        'Hlt2CharmHadD02HHHH_4piWideMassDecision',
        'Hlt2RareCharmD02KKMuMuFilterDecision',
        # 'Hlt2RareCharmD02KPiDecision',
        # 'Hlt2RareCharmD02MuMuDecision',
        'Hlt2RareCharmD02PiPieeFilterDecision',
        'Hlt2RareCharmD02KKMueFilterDecision',
        # 'Hlt2RareCharmLc2PMueFilterDecision',
        # 'Hlt2RareCharmD02PiPiDecision',
        # 'Hlt2RareCharmD2KMuMuSSFilterDecision',
        # 'Hlt2RareCharmD2PiMueFilterDecision',
        # 'Hlt2RareCharmD2KMueFilterDecision',
        'Hlt2RareCharmD02KPiMuMuFilterDecision',
        # 'Hlt2RareCharmD2KMuMuFilterDecision',
        # 'Hlt2RareCharmD2KeeFilterDecision',
        'Hlt2RareCharmD02KPiMueFilterDecision',
        'Hlt2RareCharmD02KPieeFilterDecision',
        'Hlt2RareCharmD02KKeeFilterDecision',
        # 'Hlt2RareCharmD2PiMuMuFilterDecision',
        # 'Hlt2RareCharmLc2PeeFilterDecision',
        # 'Hlt2RareCharmD2PieeFilterDecision',
        'Hlt2RareCharmD02KPiMuMuSSFilterDecision',
        # 'Hlt2RareCharmD2PiMuMuSSFilterDecision',
        # 'Hlt2RareCharmLc2PMuMuSSFilterDecision',
        'Hlt2RareCharmD02PiPiMuMuFilterDecision',
        # 'Hlt2RareCharmD02EMuDecision',
        # 'Hlt2RareCharmD02KMuDecision',
        # 'Hlt2RareCharmLc2PMuMuFilterDecision',
        'Hlt2RareCharmD02PiPiMueFilterDecision',
        # 'Hlt2RareCharmD2PieeSSFilterDecision',
        # 'Hlt2RareCharmD2KeeSSFilterDecision',
        # 'Hlt2RareCharmD2PiMueSSFilterDecision',
        # 'Hlt2RareCharmD2KMueSSFilterDecision',
        # 'Hlt2RareCharmD2PiMuMuOSDecision',
        # 'Hlt2RareCharmD2PiMuMuSSDecision',
        # 'Hlt2RareCharmD2PiMuMuWSDecision',
        # 'Hlt2RareCharmD2KMuMuOSDecision',
        # 'Hlt2RareCharmD2KMuMuSSDecision',
        # 'Hlt2RareCharmD2KMuMuWSDecision',
        # 'Hlt2RareCharmD2PiEEOSDecision',
        # 'Hlt2RareCharmD2PiEESSDecision',
        # 'Hlt2RareCharmD2PiEEWSDecision',
        # 'Hlt2RareCharmD2KEEOSDecision',
        # 'Hlt2RareCharmD2KEESSDecision',
        # 'Hlt2RareCharmD2KEEWSDecision',
        # 'Hlt2RareCharmD2PiMuEOSDecision',
        # 'Hlt2RareCharmD2PiMuESSDecision',
        # 'Hlt2RareCharmD2PiMuEWSDecision',
        # 'Hlt2RareCharmD2PiEMuOSDecision',
        # 'Hlt2RareCharmD2KMuEOSDecision',
        # 'Hlt2RareCharmD2KMuESSDecision',
        # 'Hlt2RareCharmD2KMuEWSDecision',
        # 'Hlt2RareCharmD2KEMuOSDecision',
        # 'Hlt2RareCharmLc2PMuMuDecision',
        # 'Hlt2RareCharmLc2PMuMuSSDecision',
        # 'Hlt2RareCharmLc2PeeDecision',
        # 'Hlt2RareCharmLc2PMueDecision',
        'Hlt2RareCharmD02PiPiMuMuDecision',
        'Hlt2RareCharmD02KKMuMuDecision',
        'Hlt2RareCharmD02KPiMuMuDecision',
        'Hlt2RareCharmD02PiPieeDecision',
        'Hlt2RareCharmD02KKeeDecision',
        'Hlt2RareCharmD02KPieeDecision',
        'Hlt2RareCharmD02PiPiMueDecision',
        'Hlt2RareCharmD02KKMueDecision',
        'Hlt2RareCharmD02KPiMueDecision',
        'Hlt2RareCharmD02KPiMuMuSSDecision',
        'Hlt2RareCharmD2KMueSSDecision'
        # 'Hlt2CharmHadInclDst2PiD02HHXBDTDecision'
         ]


triggerLines = l0TriggerLines + hlt1TriggerLines + hlt2TriggerLines


##--------------------------------------------#
## Useful functions to add all variables
##--------------------------------------------#
def AddLoKi_Keys(ntuple, branch_type):
    assert (branch_type=='muons' or branch_type=='electrons' or branch_type=='LFV'), "Invalid branches conditions"
    
    my_LoKi_Keys_tool_h0 = ntuple.h0.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_h0')
    my_LoKi_Keys_tool_h0.Variables={"KEY_OnBranch" : "KEY"}
    my_LoKi_Keys_tool_h1 = ntuple.h1.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_h1')
    my_LoKi_Keys_tool_h1.Variables={"KEY_OnBranch" : "KEY"}
    if branch_type=='muons':
       my_LoKi_Keys_tool_mu0 = ntuple.mu0.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_mu0')
       my_LoKi_Keys_tool_mu0.Variables={"KEY_OnBranch" : "KEY"}
       my_LoKi_Keys_tool_mu1 = ntuple.mu1.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_mu1')
       my_LoKi_Keys_tool_mu1.Variables={"KEY_OnBranch" : "KEY"}
    if branch_type=='electrons':
       my_LoKi_Keys_tool_l0 = ntuple.l0.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_l0')
       my_LoKi_Keys_tool_l0.Variables={"KEY_OnBranch" : "KEY"}
       my_LoKi_Keys_tool_l1 = ntuple.l1.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_l1')
       my_LoKi_Keys_tool_l1.Variables={"KEY_OnBranch" : "KEY"}
    if branch_type=='LFV':
       my_LoKi_Keys_tool_e = ntuple.e.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_e')
       my_LoKi_Keys_tool_e.Variables={"KEY_OnBranch" : "KEY"}
       my_LoKi_Keys_tool_mu = ntuple.mu.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Keys_mu')
       my_LoKi_Keys_tool_mu.Variables={"KEY_OnBranch" : "KEY"}


    
def AddAlltheTools(ntuple, branch_type):
    assert (branch_type=='muons' or branch_type=='electrons' or branch_type=='LFV'), "Invalid branches conditions"
    
    ntuple.addTool(TupleToolDecay, name="D")
    ntuple.addTool(TupleToolDecay, name="Dst") 
    ntuple.addTool(TupleToolDecay, name="h0")
    ntuple.addTool(TupleToolDecay, name="h1")
    ntuple.addTool(TupleToolDecay, name="Slowpi")
    
    ##Explicit Trigger for each branch
    myTISTOStool_D = ntuple.D.addTupleTool('TupleToolTISTOS')
    myTISTOStool_D.VerboseL0=True
    myTISTOStool_D.VerboseHlt1=True
    myTISTOStool_D.VerboseHlt2=True
    myTISTOStool_D.TriggerList = triggerLines
    
    myTISTOStool_Dst = ntuple.Dst.addTupleTool('TupleToolTISTOS')
    myTISTOStool_Dst.VerboseL0=True
    myTISTOStool_Dst.VerboseHlt1=True
    myTISTOStool_Dst.VerboseHlt2=True
    myTISTOStool_Dst.TriggerList = triggerLines

    myTISTOStool_h0 = ntuple.h0.addTupleTool('TupleToolTISTOS')
    myTISTOStool_h0.VerboseL0=True
    myTISTOStool_h0.VerboseHlt1=True
    #myTISTOStool_h0.VerboseHlt2=True
    myTISTOStool_h0.TriggerList = triggerLines
    
    myTISTOStool_h1 = ntuple.h1.addTupleTool('TupleToolTISTOS')
    myTISTOStool_h1.VerboseL0=True
    myTISTOStool_h1.VerboseHlt1=True
    #myTISTOStool_h1.VerboseHlt2=True
    myTISTOStool_h1.TriggerList = triggerLines
    
    if branch_type=='muons':
        ntuple.addTool(TupleToolDecay, name="mu0")
        ntuple.addTool(TupleToolDecay, name="mu1")
        
        myTISTOStool_mu0 = ntuple.mu0.addTupleTool('TupleToolTISTOS')
        myTISTOStool_mu0.VerboseL0=True
        myTISTOStool_mu0.VerboseHlt1=True
        myTISTOStool_mu0.VerboseHlt2=True
        myTISTOStool_mu0.TriggerList = triggerLines
        
        myTISTOStool_mu1 = ntuple.mu1.addTupleTool('TupleToolTISTOS')
        myTISTOStool_mu1.VerboseL0=True
        myTISTOStool_mu1.VerboseHlt1=True
        myTISTOStool_mu1.VerboseHlt2=True
        myTISTOStool_mu1.TriggerList = triggerLines

    if branch_type=='electrons': 
        ntuple.addTool(TupleToolDecay, name="l0")
        ntuple.addTool(TupleToolDecay, name="l1")
        
        myTISTOStool_l0 = ntuple.l0.addTupleTool('TupleToolTISTOS')
        myTISTOStool_l0.VerboseL0=True
        myTISTOStool_l0.VerboseHlt1=True
        myTISTOStool_l0.VerboseHlt2=True
        myTISTOStool_l0.TriggerList = triggerLines
        
        myTISTOStool_l1 = ntuple.l1.addTupleTool('TupleToolTISTOS')
        myTISTOStool_l1.VerboseL0=True
        myTISTOStool_l1.VerboseHlt1=True
        myTISTOStool_l1.VerboseHlt2=True
        myTISTOStool_l1.TriggerList = triggerLines
        
    if branch_type=='LFV': 
        ntuple.addTool(TupleToolDecay, name="DiLepton")
        ntuple.addTool(TupleToolDecay, name="e")
        ntuple.addTool(TupleToolDecay, name="mu")

        myTISTOStool_e = ntuple.e.addTupleTool('TupleToolTISTOS')
        myTISTOStool_e.VerboseL0=True
        myTISTOStool_e.VerboseHlt1=True
        #myTISTOStool_e.VerboseHlt2=True
        myTISTOStool_e.TriggerList = triggerLines

        myTISTOStool_mu = ntuple.mu.addTupleTool('TupleToolTISTOS')
        myTISTOStool_mu.VerboseL0=True
        myTISTOStool_mu.VerboseHlt1=True
        #myTISTOStool_mu.VerboseHlt2=True
        myTISTOStool_mu.TriggerList = triggerLines

        
    myTISTOStool = ntuple.addTupleTool("TupleToolTISTOS")
    #myTISTOStool.Verbose = True
    # Standard and yandex PID variables
    mypidtune = ntuple.addTupleTool("TupleToolANNPID")
    mypidtune.ANNPIDTunes= ["MC15TuneDNNV1", "MC15TuneCatBoostV1", "MC15TuneFLAT4dV1", "MC15TuneV1"]
#    myPidtool = ntuple.addTupleTool("TupleToolPid")
#    myPidtool.Verbose = True
    
    # Add and configure other tupletools
    myGeometrytool = ntuple.addTupleTool("TupleToolGeometry")
    myGeometrytool.Verbose = True
    myTrackInfotool = ntuple.addTupleTool("TupleToolTrackInfo")
    myTrackInfotool.Verbose = True
    myL0Calotool = ntuple.addTupleTool("TupleToolL0Calo",name='L0Calo_ECAL')
    myL0Calotool.WhichCalo = "ECAL"

    AddEvtTuple(ntuple)
    
    myL0Datatool = ntuple.addTupleTool("TupleToolL0Data")
    myL0Datatool.Verbose = True

def AddLokiVars_DATA(ntuple):
    AddLoKi_All_hhmumu(ntuple)
    AddLokiCone_DATA(ntuple)
    
    Add_DTF_variables_new_tool(ntuple)
    Add_DTF_variables_new_tool_D0_costr(ntuple)
    # AddLoKi_All_hhmumu_Dst(ntuple)
    # AddLoKi_All_hhmumu_Dst_D0constr(ntuple)
    # AddLoKi_All_hhmumu_Dst_Dstconstr(ntuple)

def AddLokiVars_MC(ntuple):
    AddLoKi_All_hhmumu(ntuple)
    AddLokiCone_MC(ntuple)
    
    Add_DTF_variables_new_tool(ntuple)
    Add_DTF_variables_new_tool_D0_costr(ntuple)
    # AddLoKi_All_hhmumu_Dst(ntuple)
    # AddLoKi_All_hhmumu_Dst_D0constr(ntuple)
    # AddLoKi_All_hhmumu_Dst_Dstconstr(ntuple)
    
def AddLokiVars_had(ntuple):
    AddLoKi_All_hhhh(ntuple)   
    AddLokiCone_had(ntuple)
    #AddLokiCone_had_SINFO(ntuple)
    AddLoKi_All_hhmumu_Dst_had(ntuple)
    AddLoKi_All_hhmumu_Dst_had_D0constr(ntuple)

def AddBremInfo(ntuple):
    myBremTool = ntuple.addTupleTool("TupleToolBremInfo")
    myBremTool.Particle = ["e+","e-"]
    myBremTool.Verbose = True
    myProtoDataTool = ntuple.addTupleTool("TupleToolProtoPData")
    myProtoDataTool.DataList = ["VeloCharge","CaloEoverP", "CaloEcalChi2", "CaloPrsE", "CaloHcalE", "EcalPIDe", "PrsPIDe", "HcalPIDe", "CaloEcalE","CaloNeutralEcal","CaloBremMatch"]
    
def AddToolMCTruth(ntuple):
    ntuple.ToolList += ['TupleToolMCBackgroundInfo']
    mctruthtool = ntuple.addTool(TupleToolMCTruth, name='TupleToolMCTruth')
    mctruthtool.addTool(MCMatchObjP2MCRelator)
    mctruthtool.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
    ntuple.TupleToolMCTruth.ToolList = ["MCTupleToolKinematic","MCTupleToolHierarchy"]

def AddToolSubstMass(ntuple):
    mySubMassTool = ntuple.D.addTupleTool( 'TupleToolSubMass' ) 
    # TupTmp.D0.ToolList += [ "TupleToolSubMass" ] 
    # mySubMassTool.Substitution += [ "mu+ => pi+" ] 
    mySubMassTool.DoubleSubstitution += [ "pi+/pi- => e+/mu-" ] 
    mySubMassTool.DoubleSubstitution += [ "pi+/pi- => mu+/e-"] 
    mySubMassTool.DoubleSubstitution += [ "pi+/pi- => e+/e-" ] 
    mySubMassTool.DoubleSubstitution += [ "pi+/pi- => mu+/mu-"] 

# def AddToolHOP(ntuple):
#     # myHOPTool = ntuple.addTupleTool('TupleToolHOP')
#     myHOPTool = ntuple.Dst.addTupleTool('TupleToolHOP')
#     # d0_hybrid = ntuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_D')
#     # d0_hybrid.Variables={
#     #     "hop_mass_bestPV" : "BPVHOPM",
#     #     "hop_mass"  :   "HOPM"
#     #     }
