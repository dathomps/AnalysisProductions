from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay
from PhysConf.Selections import MomentumScaling, TupleSelection
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence, SimpleSelection
from Configurables import MCDecayTreeTuple
from Configurables import DaVinci, PrintDecayTree
from PhysConf.Filters import LoKi_Filters

import sys, os

if 'ANALYSIS_PRODUCTIONS_BASE' in os.environ:
    sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'RareCharm/Production_hhmue'))
else:
    sys.path.append(os.getcwd())

from Tools_Creator import ntuple, AddAlltheTools , AddToolMCTruth , AddLokiVars_DATA , AddBremInfo , AddLoKi_Keys , AddLokiVars_had

Enable_muons_channles     = False
Enable_electrons_channels = False
Enable_hadronic_channels  = False
Enable_LFV_channels       = True


###-------------------------------------------------------------###
###        Define Input particles from stripping lines
###        Apply MOMENTUM SCALING
###        Being efficient: define prefilters on Stripping
###-------------------------------------------------------------###

Particles_fromStrippingLine        = AutomaticData('Phys/DstarPromptWithD02HHLLLine/Particles')
Particles_fromStrippingControlLine = AutomaticData('Phys/DstarPromptWithD02HHMuMuControlLine/Particles')

ScaledParticles_fromStrippingLine        = MomentumScaling(Particles_fromStrippingLine)
ScaledParticles_fromStrippingControlLine = MomentumScaling(Particles_fromStrippingControlLine)

#subs_pipi = SubstitutePID(
#            'MakeD02pipimumu',
#            Code="DECTREE('[ D*(2010)+ -> (D0 -> pi+ pi- pi+ pi- ) pi+ ]CC')", 
#            # note that SubstitutePID can't handle automatic CC
#            Substitutions={
#                'Charm -> (D0 -> ^pi+ X- X+ X-) Meson': 'mu+',
#                'Charm -> (D0 -> ^pi- X+ X+ X-) Meson': 'mu-',
#                'Charm -> (D~0 -> ^pi+ X- X+ X-) Meson': 'mu+',
#                'Charm -> (D~0 -> ^pi- X+ X+ X-) Meson': 'mu-'}
#)

#subs_kk   = SubstitutePID(
#            'MakeD02KKmumu',
#            Code="DECTREE('[ D*(2010)+ -> (D0 -> K+ K- pi+ pi- ) pi+ ]CC')", 
#            # note that SubstitutePID can't handle automatic CC
#            Substitutions={
#                'Charm -> (D0 -> ^pi+ X- X+ X-) Meson': 'mu+',
#                'Charm -> (D0 -> ^pi- X+ X+ X-) Meson': 'mu-',
#                'Charm -> (D~0 -> ^pi+ X- X+ X-) Meson': 'mu+',
#                'Charm -> (D~0 -> ^pi- X+ X+ X-) Meson': 'mu-'}
#)


#Workaround to use RELINFO with SubstitutePID
#flter = FilterDesktop( 'InfoWriter')
#flter.Preambulo = [    "x1 = SINFO(9014, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEANGLE',-1.), True)",
#                "x2 = SINFO(9025, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEMULT', -1.), True)",
#                "x3 = SINFO(9036, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD011','CONEPTASYM',-1.), True)",
#                "x4 = SINFO(9047, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEANGLE',-1.), True)",
#                "x5 = SINFO(9058, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEMULT',-1.), True)",
#                "x6 = SINFO(9069, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst11','CONEPTASYM',-1.), True)",
#                "x7 = SINFO(9080, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEANGLE',-1.), True)",
#                "x8 = SINFO(9091, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEMULT', -1.), True)",
#                "x9 = SINFO(9102, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD013','CONEPTASYM',-1.), True)",
#                "x10 = SINFO(9113, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEANGLE',-1.), True)",
#                "x11 = SINFO(9124, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEMULT',-1.), True)",
#                "x12 = SINFO(9135, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst13','CONEPTASYM',-1.), True)",
#                "x13 = SINFO(9146, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEANGLE',-1.), True)",
#                "x14 = SINFO(9157, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEMULT', -1.), True)",
#                "x15 = SINFO(9168, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD015','CONEPTASYM',-1.), True)",
#                "x16 = SINFO(9179, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEANGLE',-1.), True)",
#                "x17 = SINFO(9190, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEMULT',-1.), True)",
#                "x18 = SINFO(9201, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst15','CONEPTASYM',-1.), True)",
#                "x19 = SINFO(9212, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEANGLE',-1.), True)",
#                "x20 = SINFO(9223, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEMULT', -1.), True)",
#                "x21 = SINFO(9234, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVD017','CONEPTASYM',-1.), True)",
#                "x22 = SINFO(9245, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEANGLE',-1.), True)",
#                "x23 = SINFO(9256, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEMULT',-1.), True)",
#                "x24 = SINFO(9267, RELINFO('/Event/Charm/Phys/DstarPromptWithD02HHMuMuControlLine/P2CVDst17','CONEPTASYM',-1.), True)",
#                "x25 = SINFO(9278, CHILD(LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator')),1) , True)",
#                "x26 = SINFO(9289, LoKi.Particles.PFunA(AMAXDOCA('LoKi::TrgDistanceCalculator')) , True)" ]
#flter.Code = '(x1>-1000) & (x2>-1000) & (x3>-1000) & (x4>-1000) & (x5>-1000) & (x6>-1000) & (x7>-1000) & (x8>-1000) & (x9>-1000) & (x10>-1000) &(x11>-1000) & (x12>-1000) & (x13>-1000) & (x14>-1000) & (x15>-1000) & (x16>-1000) & (x17>-1000) & (x18>-1000) &(x19>-1000) & (x20>-1000) & (x21>-1000) & (x22>-1000) & (x23>-1000) & (x24>-1000) & (x25>-1000) & (x26>-1000)'


#SaveRelInfo =  Selection("InfoWriter_Sel", 
#                          Algorithm = flter, 
#                          RequiredSelections = [ScaledParticles_fromStrippingControlLine] )


#ScaledParticles_fromStrippingControlLine_SubtsPID_pipi = Selection('MakeD02pipimumu_sel',
#                                                                    Algorithm=subs_pipi,
#                                                                    RequiredSelections=[SaveRelInfo])
#ScaledParticles_fromStrippingControlLine_SubtsPID_kk   = Selection('MakeD02kkmumu_sel',
#                                                                    Algorithm=subs_kk,
#                                                                    RequiredSelections=[SaveRelInfo])

fltrs = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('StrippingDstarPromptWithD02HHLL.*Decision') | HLT_PASS_RE('StrippingDstarPromptWithD02HHMuMu.*Decision')"
)




##----------------------------------------------------#
##   4-body decays with TAG
##   ntuples + add some tools 
##   Muonic Channels
##----------------------------------------------------#

############ Creation of the D*-> D->KKmumu ntuple #######################
# Get KKmumu candidates and start the ntuple

Tuple16 = TupleSelection (
            'DstD2KKMuMu', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay          = "[ D*(2010)+ -> ^(D0 -> ^K+ ^K- ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches       = {
                 "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ K- mu+ mu- ) pi+ ]CC"
                ,"h1"       : "[ D*(2010)+ -> (D0 -> K+ ^K- mu+ mu- ) pi+ ]CC"
                ,"mu0"      : "[ D*(2010)+ -> (D0 -> K+ K- ^mu+ mu- ) pi+ ]CC"
                ,"mu1"      : "[ D*(2010)+ -> (D0 -> K+ K- mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"   : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) ^pi+ ]CC"
                ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                ,"Dst"      : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )

AddAlltheTools(Tuple16, 'muons')
AddLokiVars_DATA(Tuple16)
AddLoKi_Keys(Tuple16, 'muons')




############ Creation of the D*-> D->PiPimumu ntuple #######################
# Get pipimumu candidates and start the ntuple
Tuple17 = TupleSelection (
            'DstD2PiPiMuMu', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- mu+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> pi+ ^pi- mu+ mu- ) pi+ ]CC"
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^mu+ mu- ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                ,"Dst"     : "[D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )

AddAlltheTools(Tuple17, 'muons')
AddLokiVars_DATA(Tuple17)
AddLoKi_Keys(Tuple17, 'muons')


############ Creation of the D*-> D->K-Pi+mumu ntuple #######################
# Get Kpimumu candidates and start the ntuple
Tuple18 = TupleSelection (
            'DstD2KPiMuMu', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ mu- ) pi+ ]CC"
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ mu- ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple18, 'muons')
AddLokiVars_DATA(Tuple18)
AddLoKi_Keys(Tuple18, 'muons')

############ Creation of the D*-> D->K+Pi-mumu WS ntuple #######################
# Get Kpimumu WS candidates and start the ntuple
Tuple19 = TupleSelection (
            'DstD2KPiMuMuWS', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- mu+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K+ ^pi- mu+ mu- ) pi+ ]CC"
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^mu+ mu- ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ mu- ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple19, 'muons')
AddLokiVars_DATA(Tuple19)
AddLoKi_Keys(Tuple19, 'muons')




##----------------------------------------------------#
##
##   Pure hadronic channels
##
##----------------------------------------------------#

# Get Kpipipi candidates and start the ntuple
Tuple20 = TupleSelection (
            'DstD2KPiPiPi', ## unique name 
           [ ScaledParticles_fromStrippingControlLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi- ^pi+ ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ pi- pi+ ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ pi- pi+ ) pi+ ]CC"    
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^pi- pi+ ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> K- pi+ pi- ^pi+ ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ pi- pi+ ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ pi- pi+ ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ pi- pi+ ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple20, 'muons')
AddLokiVars_had(Tuple20)
AddLoKi_Keys(Tuple20, 'muons')
#AddToolSubstMass(Tuple20)


# Get pipipipi candidates and start the ntuple
Tuple21 = TupleSelection (
            'DstD2PiPiPiPi', ## unique name 
           [ ScaledParticles_fromStrippingControlLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- mu+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> pi+ ^pi- mu+ mu- ) pi+ ]CC"
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^mu+ mu- ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                ,"Dst"     : "[D*(2010)+ -> (D0 -> pi+ pi- mu+ mu- ) pi+ ]CC"
                    } ,
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple21, 'muons')
#AddLokiVars_had(Tuple21)
AddLokiVars_DATA(Tuple21)
AddLoKi_Keys(Tuple21, 'muons')
#AddToolSubstMass(Tuple21)


# Get KKpipi candidates and start the ntuple
Tuple22 = TupleSelection (
            'DstD2KKPiPi', ## unique name 
           [ ScaledParticles_fromStrippingControlLine ], ## required selections
             Decay          = "[ D*(2010)+ -> ^(D0 -> ^K+ ^K- ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches       = {
                 "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ K- mu+ mu- ) pi+ ]CC"
                ,"h1"       : "[ D*(2010)+ -> (D0 -> K+ ^K- mu+ mu- ) pi+ ]CC"
                ,"mu0"      : "[ D*(2010)+ -> (D0 -> K+ K- ^mu+ mu- ) pi+ ]CC"
                ,"mu1"      : "[ D*(2010)+ -> (D0 -> K+ K- mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"   : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) ^pi+ ]CC"
                ,"D"        : "[ D*(2010)+ -> ^(D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                ,"Dst"      : "[ D*(2010)+ -> (D0 -> K+ K- mu+ mu- ) pi+ ]CC"
                } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple22, 'muons')
#AddLokiVars_had(Tuple22)
AddLokiVars_DATA(Tuple22)
AddLoKi_Keys(Tuple22, 'muons')
#AddToolSubstMass(Tuple22)


# Get Kpipipi WS candidates and start the ntuple
Tuple23 = TupleSelection (
            'DstD2KPiPiPi_WS', ## unique name 
           [ ScaledParticles_fromStrippingControlLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^pi- ^pi+ ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- pi- pi+ ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K+ ^pi- pi- pi+ ) pi+ ]CC"    
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> K+ pi- ^pi- pi+ ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> K+ pi- pi- ^pi+ ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K+ pi- pi- pi+ ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple23, 'muons')
AddLokiVars_had(Tuple23)
AddLoKi_Keys(Tuple23, 'muons')
#AddToolSubstMass(Tuple23)


##----------------------------------------------------#
##
##   Electron modes
##
##----------------------------------------------------#

############ Creation of the D*-> D->K-K+ee ntuple #######################
# Get KKee candidates and start the ntuple
Tuple24 = TupleSelection (
            'DstD2KKEE', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^K+ ^e+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- K+ e+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^K+ e+ e- ) pi+ ]CC"
                ,"l0"      : "[ D*(2010)+ -> (D0 -> K- K+ ^e+ e- ) pi+ ]CC"
                ,"l1"      : "[ D*(2010)+ -> (D0 -> K- K+ e+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- K+ e+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- K+ e+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- K+ e+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple24, 'electrons')
AddLokiVars_DATA(Tuple24)
AddBremInfo(Tuple24)
AddLoKi_Keys(Tuple24, 'electrons')





############ Creation of the D*-> D->pi-pi+ee ntuple #######################
# Get pipiee candidates and start the ntuple
Tuple25 = TupleSelection (
            'DstD2PiPiEE', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^e+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^pi- pi+ e+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> pi- ^pi+ e+ e- ) pi+ ]CC"
                ,"l0"      : "[ D*(2010)+ -> (D0 -> pi- pi+ ^e+ e- ) pi+ ]CC"
                ,"l1"      : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi- pi+ e+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple25, 'electrons')
AddLokiVars_DATA(Tuple25)
AddBremInfo(Tuple25)
AddLoKi_Keys(Tuple25, 'electrons')


############ Creation of the D*-> D->K-pi+ee ntuple #######################

Tuple26 = TupleSelection (
            'DstD2KPiEE', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ e+ e- ) pi+ ]CC"
                ,"l0"      : "[ D*(2010)+ -> (D0 -> K- pi+ ^e+ e- ) pi+ ]CC"
                ,"l1"      : "[ D*(2010)+ -> (D0 -> K- pi+ e+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ e+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ e+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ e+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple26, 'electrons')
AddLokiVars_DATA(Tuple26)
AddBremInfo(Tuple26)
AddLoKi_Keys(Tuple26, 'electrons')


############ Creation of the D*-> D->K-pi+ee ntuple #######################
# Get Kpiee WS candidates and start the ntuple
Tuple27 = TupleSelection (
            'DstD2KPiEEWS', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^e+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- e+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K+ ^pi- e+ e- ) pi+ ]CC"
                ,"l0"      : "[ D*(2010)+ -> (D0 -> K+ pi- ^e+ e- ) pi+ ]CC"
                ,"l1"      : "[ D*(2010)+ -> (D0 -> K+ pi- e+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K+ pi- e+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K+ pi- e+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K+ pi- e+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple27, 'electrons')
AddLokiVars_DATA(Tuple27)
AddBremInfo(Tuple27)
AddLoKi_Keys(Tuple27, 'electrons')





##----------------------------------------------------#
##
##   LFV modes
##
##----------------------------------------------------#

############ Creation of the D*-> D->K-K+e+mu- ntuple #######################
# Get KKemu candidates and start the ntuple
Tuple28 = TupleSelection (
                'DstD2KKeMu', ## unique name
               [ ScaledParticles_fromStrippingLine ], ## required selections
                 Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^K+ ^e+ ^mu- ) ^pi+ ]CC" ,
                 Branches      = {
                    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- K+ e+ mu- ) pi+ ]CC"
                    ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^K+ e+ mu- ) pi+ ]CC"
                    ,"e"       : "[ D*(2010)+ -> (D0 -> K- K+ ^e+ mu- ) pi+ ]CC"
                    ,"mu"      : "[ D*(2010)+ -> (D0 -> K- K+ e+ ^mu- ) pi+ ]CC"
                    ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- K+ e+ mu- ) ^pi+ ]CC"
                    ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- K+ e+ mu- ) pi+ ]CC"
                    ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- K+ e+ mu- ) pi+ ]CC"
                        }  ,
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
                 )
AddAlltheTools(Tuple28, 'LFV')
AddLokiVars_DATA(Tuple28)
AddBremInfo(Tuple28)
AddLoKi_Keys(Tuple28, 'LFV')


############ Creation of the D*-> D->K-K+mu+e- ntuple #######################
# Get KKmue candidates and start the ntuple
Tuple29 = TupleSelection (
            'DstD2KKMuE', ## unique name
            [ ScaledParticles_fromStrippingLine ], ## required selections
              Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^K+ ^mu+ ^e- ) ^pi+ ]CC" ,
              Branches      = {
                 "h0"       : "[ D*(2010)+ -> (D0 -> ^K- K+ mu+ e- ) pi+ ]CC"
                 ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^K+ mu+ e- ) pi+ ]CC"
                 ,"mu"      : "[ D*(2010)+ -> (D0 -> K- K+ ^mu+ e- ) pi+ ]CC"
                 ,"e"       : "[ D*(2010)+ -> (D0 -> K- K+ mu+ ^e- ) pi+ ]CC"
                 ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- K+ mu+ e- ) ^pi+ ]CC"
                 ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- K+ mu+ e- ) pi+ ]CC"
                 ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- K+ mu+ e- ) pi+ ]CC"
                 }  ,
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
            )
AddAlltheTools(Tuple29, 'LFV')
AddLokiVars_DATA(Tuple29)
AddBremInfo(Tuple29)
AddLoKi_Keys(Tuple29, 'LFV')


############ Creation of the D*-> D->pi-pi+e+mu- ntuple #######################
# Get pipiemu candidates and start the ntuple
Tuple30 = TupleSelection (
                'DstD2PiPiEMu', ## unique name
               [ ScaledParticles_fromStrippingLine ], ## required selections
                 Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^e+ ^mu- ) ^pi+ ]CC" ,
                 Branches      = {
                    "h0"       : "[ D*(2010)+ -> (D0 -> ^pi- pi+ e+ mu- ) pi+ ]CC"
                    ,"h1"      : "[ D*(2010)+ -> (D0 -> pi- ^pi+ e+ mu- ) pi+ ]CC"
                    ,"e"       : "[ D*(2010)+ -> (D0 -> pi- pi+ ^e+ mu- ) pi+ ]CC"
                    ,"mu"      : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ ^mu- ) pi+ ]CC"
                    ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ mu- ) ^pi+ ]CC"
                    ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi- pi+ e+ mu- ) pi+ ]CC"
                    ,"Dst"     : "[ D*(2010)+ -> (D0 -> pi- pi+ e+ mu- ) pi+ ]CC"
                        }  ,
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
                 )
AddAlltheTools(Tuple30, 'LFV')
AddLokiVars_DATA(Tuple30)
AddBremInfo(Tuple30)
AddLoKi_Keys(Tuple30, 'LFV')


############ Creation of the D*-> D->pi-pi+mu+e- ntuple #######################
# Get KKmue candidates and start the ntuple
Tuple31 = TupleSelection (
            'DstD2PiPiMue', ## unique name
            [ ScaledParticles_fromStrippingLine ], ## required selections
              Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^mu+ ^e- ) ^pi+ ]CC" ,
              Branches      = {
                 "h0"       : "[ D*(2010)+ -> (D0 -> ^pi- pi+ mu+ e- ) pi+ ]CC"
                 ,"h1"      : "[ D*(2010)+ -> (D0 -> pi- ^pi+ mu+ e- ) pi+ ]CC"
                 ,"mu"      : "[ D*(2010)+ -> (D0 -> pi- pi+ ^mu+ e- ) pi+ ]CC"
                 ,"e"       : "[ D*(2010)+ -> (D0 -> pi- pi+ mu+ ^e- ) pi+ ]CC"
                 ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi- pi+ mu+ e- ) ^pi+ ]CC"
                 ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi- pi+ mu+ e- ) pi+ ]CC"
                 ,"Dst"     : "[ D*(2010)+ -> (D0 -> pi- pi+ mu+ e- ) pi+ ]CC"
                 }  ,
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
            )
AddAlltheTools(Tuple31, 'LFV')
AddLokiVars_DATA(Tuple31)
AddBremInfo(Tuple31)
AddLoKi_Keys(Tuple31, 'LFV')

############ Creation of the D*-> D->K-pi+e+mu- ntuple #######################
Tuple32 = TupleSelection (
            'DstD2KPieMu', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ e+ mu- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ ^e+ mu- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ e+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ e+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ e+ mu- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ e+ mu- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple32, 'LFV')
AddLokiVars_DATA(Tuple32)
AddBremInfo(Tuple32)
AddLoKi_Keys(Tuple32, 'LFV')


############ Creation of the D*-> D->K-pi+mu+e- ntuple #######################
Tuple33 = TupleSelection (
            'DstD2KPiMue', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ e- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ e- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple33, 'LFV')
AddLokiVars_DATA(Tuple33)
AddBremInfo(Tuple33)
AddLoKi_Keys(Tuple33, 'LFV')


############ Creation of the D*-> D->K+pi-e+mu- ntuple #######################
# Get KpieMu WS candidates and start the ntuple
Tuple34 = TupleSelection (
            'DstD2KPieMuWS', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^e+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- e+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K+ ^pi- e+ mu- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K+ pi- ^e+ mu- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K+ pi- e+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K+ pi- e+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K+ pi- e+ mu- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K+ pi- e+ mu- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple34, 'LFV')
AddLokiVars_DATA(Tuple34)
AddBremInfo(Tuple34)
AddLoKi_Keys(Tuple34, 'LFV')


############ Creation of the D*-> D->K+pi-mu+e- ntuple #######################
# Get KpiMue WS candidates and start the ntuple
Tuple35 = TupleSelection (
            'DstD2KPiMueWS', ## unique name 
           [ ScaledParticles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K+ ^pi- ^mu+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K+ pi- mu+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K+ ^pi- mu+ e- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K+ pi- ^mu+ e- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K+ pi- mu+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K+ pi- mu+ e- ) pi+ ]CC"
                    }  , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]     
             )
AddAlltheTools(Tuple35, 'LFV')
AddLokiVars_DATA(Tuple35)
AddBremInfo(Tuple35)
AddLoKi_Keys(Tuple35, 'LFV')






##----------------------------------------------------#
##
##   Execution part (ie Sequences and DV settings)
##
##----------------------------------------------------#


DaVinci().TupleFile = "RARECHARM_4BODIES_4H.root"
DaVinci().EventPreFilters = fltrs.filters('Filters')

if Enable_muons_channles:
    Seq_KK_tag        = SelectionSequence('Seq_KK_tag',       TopSelection = Tuple16)
    Seq_PiPi_tag      = SelectionSequence('Seq_PiPi_tag',     TopSelection = Tuple17)
    Seq_KPi_tag       = SelectionSequence('Seq_KPi_tag',      TopSelection = Tuple18)
    Seq_KPi_WS_tag    = SelectionSequence('Seq_KPi_WS_tag',   TopSelection = Tuple19)
    DaVinci().UserAlgorithms += [Seq_KK_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_PiPi_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPi_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPi_WS_tag.sequence()]
    
if Enable_hadronic_channels:
    Seq_K3pi_tag      = SelectionSequence('Seq_K3pi_tag',     TopSelection = Tuple20)
    #Seq_4Pi_tag       = SelectionSequence('Seq_4Pi_tag',      TopSelection = Tuple21)
    #Seq_2K2Pi_tag     = SelectionSequence('Seq_2K2Pi_tag',    TopSelection = Tuple22)
    Seq_K3pi_WS_tag   = SelectionSequence('Seq_K3pi_WS_tag',  TopSelection = Tuple23)
    DaVinci().UserAlgorithms += [Seq_K3pi_tag.sequence()]
    #DaVinci().UserAlgorithms += [Seq_4Pi_tag.sequence()]
    #DaVinci().UserAlgorithms += [Seq_2K2Pi_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_K3pi_WS_tag.sequence()]
    
if Enable_electrons_channels:
    Seq_KKee_tag      = SelectionSequence('Seq_KKee_tag',     TopSelection = Tuple24)
    Seq_PiPiee_tag    = SelectionSequence('Seq_PiPiee_tag',   TopSelection = Tuple25)
    Seq_KPiee_tag     = SelectionSequence('Seq_KPiee_tag',    TopSelection = Tuple26)
    Seq_KPiee_WS_tag  = SelectionSequence('Seq_KPiee_WS_tag', TopSelection = Tuple27)
    DaVinci().UserAlgorithms += [Seq_KKee_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_PiPiee_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPiee_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPiee_WS_tag.sequence()]
    
if Enable_LFV_channels:
    Seq_KKemu_tag     = SelectionSequence('Seq_KKemu_tag',    TopSelection = Tuple28)
    Seq_KKmue_tag     = SelectionSequence('Seq_KKmue_tag',    TopSelection = Tuple29)
    Seq_PiPiemu_tag   = SelectionSequence('Seq_PiPiemu_tag',  TopSelection = Tuple30)
    Seq_PiPimue_tag   = SelectionSequence('Seq_PiPimue_tag',  TopSelection = Tuple31)
    Seq_KPiemu_tag    = SelectionSequence('Seq_KPiemu_tag',   TopSelection = Tuple32)
    Seq_KPimue_tag    = SelectionSequence('Seq_KPimue_tag',   TopSelection = Tuple33)
    Seq_KPiemu_WS_tag = SelectionSequence('Seq_KPiemu_WS_tag',TopSelection = Tuple34)
    Seq_KPimue_WS_tag = SelectionSequence('Seq_KPimue_WS_tag',TopSelection = Tuple35)
    DaVinci().UserAlgorithms += [Seq_KKemu_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KKmue_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_PiPiemu_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_PiPimue_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPiemu_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPimue_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPiemu_WS_tag.sequence()]
    DaVinci().UserAlgorithms += [Seq_KPimue_WS_tag.sequence()]
