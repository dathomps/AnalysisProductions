from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay
from PhysConf.Selections import MomentumScaling, TupleSelection
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence, SimpleSelection
from Configurables import MCDecayTreeTuple
from Configurables import DaVinci, PrintDecayTree

import sys, os

if 'ANALYSIS_PRODUCTIONS_BASE' in os.environ:
    sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'RareCharm/Production_hhmue'))
else:
    sys.path.append(os.getcwd())

from Tools_Creator import ntuple, AddAlltheTools , AddToolMCTruth , AddLokiVars_MC , AddBremInfo , AddLoKi_Keys




Particles_fromStrippingLine        = AutomaticData('Phys/DstarPromptWithD02HHLLLine/Particles')


Tuple1 = TupleSelection (
                'DstD2KPieMu', ## unique name
                [Particles_fromStrippingLine ], ## required selections
                Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e+ ^mu- ) ^pi+ ]CC" ,
                Branches      = {
                    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e+ mu- ) pi+ ]CC"
                    ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ e+ mu- ) pi+ ]CC"
                    ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ ^e+ mu- ) pi+ ]CC"
                    ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ e+ ^mu- ) pi+ ]CC"
                    ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ e+ mu- ) ^pi+ ]CC"
                    ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ e+ mu- ) pi+ ]CC"
                    ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+  e+ mu- ) pi+ ]CC"
                   }  ,
                ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]
                )
AddAlltheTools(Tuple1, 'LFV')
AddLokiVars_MC(Tuple1)
AddLoKi_Keys(Tuple1, 'LFV')
AddToolMCTruth(Tuple1)
AddBremInfo(Tuple1)

Tuple2 = TupleSelection (
            'DstD2KPiMue', ## unique name
            [Particles_fromStrippingLine ], ## required selections
            Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^e- ) ^pi+ ]CC" ,
            Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ e- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ e- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+  mu+ e- ) pi+ ]CC"
                }  ,
            ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolMuonPid","TupleToolPrimaries" ]
            )
AddAlltheTools(Tuple2, 'LFV')
AddLokiVars_MC(Tuple2)
AddLoKi_Keys(Tuple2, 'LFV')
AddToolMCTruth(Tuple2)
AddBremInfo(Tuple2)


##----------------------------------------------------#
##   MCTRUTHTUPLE for Simulation
##   --> NB: put the right Decay descriptor
##----------------------------------------------------#

MyMCDecayTreeTuple1 = MCDecayTreeTuple("MCTruthTuple_DstD2KPieMu")
MyMCDecayTreeTuple1.Decay = "[ D*(2010)+ -> ^(D0 ==> ^K- ^pi+ ^e+ ^mu- ) ^pi+ ]CC"
MyMCDecayTreeTuple1.TupleName = "MCTruthTuple"
MyMCDecayTreeTuple1.ToolList+=["MCTupleToolKinematic", 
                                  "MCTupleToolPrimaries", 
                                  "MCTupleToolHierarchy",
                                  "TupleToolEventInfo",
                                  #"MCTupleToolReconstructed",
                                  "MCTupleToolPID"]

MyMCDecayTreeTuple2 = MCDecayTreeTuple("MCTruthTuple_DstD2KPiMue")
MyMCDecayTreeTuple2.Decay = "[ D*(2010)+ -> ^(D0 ==> ^K- ^pi+ ^mu+ ^e- ) ^pi+ ]CC"
MyMCDecayTreeTuple2.TupleName = "MCTruthTuple"
MyMCDecayTreeTuple2.ToolList+=["MCTupleToolKinematic", 
                                  "MCTupleToolPrimaries", 
                                  "MCTupleToolHierarchy",
                                  "TupleToolEventInfo",
                                  #"MCTupleToolReconstructed",
                                  "MCTupleToolPID"]

#MyMCDecayTreeTuple.addTool(MCTupleToolKinematic())
#MyMCDecayTreeTuple.MCTupleToolKinematic.Verbose=True
#MyMCDecayTreeTuple.MCTupleToolKinematic.StoreStablePropertime = True
#MyMCDecayTreeTuple.addTool(MCTupleToolReconstructed())
#MyMCDecayTreeTuple.MCTupleToolReconstructed.Verbose=True


##----------------------------------------------------#
##
##   Execution part (ie Sequences and DV settings)
##
##----------------------------------------------------#
from Configurables import GaudiSequencer

DaVinci().TupleFile = 'RARECHARM_MC_4BODIES_KPIMUE.ROOT'

#Seq_KPiMupEm_tag        = SelectionSequence('Seq_KPiMupEm_tag',       TopSelection = Tuple1)
#Seq_KPiMumEp_tag        = SelectionSequence('Seq_KPiMumEp_tag',       TopSelection = Tuple2)
#Seq_KPiMupEm_MCTruth_tag = GaudiSequencer("Seq_KPiMupEm_MCTruth_tag")
#Seq_KPiMupEm_MCTruth_tag.Members  += [ MyMCDecayTreeTuple1 ]
#Seq_KPiMumEp_MCTruth_tag = GaudiSequencer("Seq_KPiMumEp_MCTruth_tag")
#Seq_KPiMumEp_MCTruth_tag.Members  += [ MyMCDecayTreeTuple2 ]

Seq_KPiMumEp_tag        = SelectionSequence('Seq_KPiMumEp_tag',       TopSelection = Tuple1)
Seq_KPiMupEm_tag        = SelectionSequence('Seq_KPiMupEm_tag',       TopSelection = Tuple2)
Seq_KPiMumEp_MCTruth_tag = GaudiSequencer("Seq_KPiMumEp_MCTruth_tag")
Seq_KPiMumEp_MCTruth_tag.Members  += [ MyMCDecayTreeTuple1 ]
Seq_KPiMupEm_MCTruth_tag = GaudiSequencer("Seq_KPiMupEm_MCTruth_tag")
Seq_KPiMupEm_MCTruth_tag.Members  += [ MyMCDecayTreeTuple2 ]

DaVinci().UserAlgorithms += [Seq_KPiMupEm_tag.sequence()]
DaVinci().UserAlgorithms += [Seq_KPiMumEp_tag.sequence()]
DaVinci().UserAlgorithms += [Seq_KPiMupEm_MCTruth_tag]
DaVinci().UserAlgorithms += [Seq_KPiMumEp_MCTruth_tag]
