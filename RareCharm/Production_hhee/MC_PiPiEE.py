from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay
from PhysConf.Selections import MomentumScaling, TupleSelection
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence, SimpleSelection
from Configurables import MCDecayTreeTuple
from Configurables import DaVinci, PrintDecayTree

import sys, os

if 'ANALYSIS_PRODUCTIONS_BASE' in os.environ:
    sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'RareCharm/Production_hhee'))
else:
    sys.path.append(os.getcwd())

from Tools_Creator import ntuple, AddAlltheTools , AddToolMCTruth , AddLokiVars_MC , AddBremInfo , AddLoKi_Keys





   


   





Particles_fromStrippingLine        = AutomaticData('Phys/DstarPromptWithD02HHLLLine/Particles') 

#Tuple17 = ntuple.clone("DstD2PiPiEE")

Tuple17 = TupleSelection (
            'DstD2PiPiEE', ## unique name 
           [ Particles_fromStrippingLine ], ## required selection
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^pi+ ^pi- ^e+ ^e- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^pi+ pi- e+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> pi+ ^pi- e+ e- ) pi+ ]CC"
                ,"l0"     : "[ D*(2010)+ -> (D0 -> pi+ pi- ^e+ e- ) pi+ ]CC"
                ,"l1"     : "[ D*(2010)+ -> (D0 -> pi+ pi- e+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> pi+ pi- e+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> pi+ pi- e+ e- ) pi+ ]CC"
                ,"Dst"     : "[D*(2010)+ -> (D0 -> pi+ pi- e+ e- ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]     
             )



AddAlltheTools(Tuple17, 'electrons')
AddLokiVars_MC(Tuple17)
AddBremInfo(Tuple17)
AddLoKi_Keys(Tuple17, 'electrons')


##### added for MC
AddToolMCTruth(Tuple17)


##----------------------------------------------------#
##   MCTRUTHTUPLE for Simulation
##   --> NB: put the right Decay descriptor
##----------------------------------------------------#


MyMCDecayTreeTuple = MCDecayTreeTuple("MCTruthTuple")
MyMCDecayTreeTuple.Decay = "[ D*(2010)+ -> ^(D0 ==> ^pi+ ^pi- ^e+ ^e- ) ^pi+ ]CC"
MyMCDecayTreeTuple.TupleName = "MCTruthTree"
MyMCDecayTreeTuple.ToolList+=["MCTupleToolKinematic", 
                                  "MCTupleToolPrimaries", 
                                  "MCTupleToolHierarchy",
                                  "TupleToolEventInfo",
                                  #"MCTupleToolReconstructed",
                                  "MCTupleToolPID"]
#MyMCDecayTreeTuple.addTool(MCTupleToolKinematic())
#MyMCDecayTreeTuple.MCTupleToolKinematic.Verbose=True
#MyMCDecayTreeTuple.MCTupleToolKinematic.StoreStablePropertime = True
#MyMCDecayTreeTuple.addTool(MCTupleToolReconstructed())
#MyMCDecayTreeTuple.MCTupleToolReconstructed.Verbose=True




##----------------------------------------------------#
##
##   Execution part (ie Sequences and DV settings)
##
##----------------------------------------------------#


DaVinci().TupleFile = "RARECHARM_4BODIES.root"

Seq_PiPi_tag      = SelectionSequence('Seq_PiPi_tag',     TopSelection = Tuple17)
Seq_PiPiEE_MCTruth_tag = GaudiSequencer("Seq_PiPiEE_MCTruth_tag")
Seq_PiPiEE_MCTruth_tag.Members  += [ MyMCDecayTreeTuple ]

DaVinci().UserAlgorithms += [Seq_PiPi_tag.sequence()]
DaVinci().UserAlgorithms += [Seq_PiPiEE_MCTruth_tag]