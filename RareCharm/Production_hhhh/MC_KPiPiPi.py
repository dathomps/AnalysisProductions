from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay
from PhysConf.Selections import MomentumScaling, TupleSelection
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, MultiSelectionSequence, SimpleSelection
from Configurables import MCDecayTreeTuple
from Configurables import DaVinci, PrintDecayTree

import sys, os

if 'ANALYSIS_PRODUCTIONS_BASE' in os.environ:
    sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'RareCharm/Production_hhhh'))
else:
    sys.path.append(os.getcwd())

from Tools_Creator import ntuple, AddAlltheTools , AddToolMCTruth , AddLokiVars_MC, AddLokiVars_MC_no_DTF_D0costr , AddBremInfo , AddLoKi_Keys






Particles_fromStrippingLine        = AutomaticData('Phys/DstarPromptWithD02HHLLLine/Particles')
Particles_fromControlStrippingLine        = AutomaticData('Phys/DstarPromptWithD02HHMuMuControlLine/Particles')



Tuple17 = TupleSelection (
            'DstD2KPiEE', ## unique name 
           [ Particles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e- ^e+ ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e- e+ ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ e- e+ ) pi+ ]CC"    
                ,"l0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^e- e+ ) pi+ ]CC"
                ,"l1"     : "[ D*(2010)+ -> (D0 -> K- pi+ e- ^e+ ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ e- e+ ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ e- e+ ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ e- e+ ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]  
             )


AddAlltheTools(Tuple17, 'electrons')
AddLokiVars_MC(Tuple17)
AddBremInfo(Tuple17)
AddLoKi_Keys(Tuple17, 'electrons')

##### added for MC
AddToolMCTruth(Tuple17)


Tuple1 = TupleSelection (
                'DstD2KPieMu', ## unique name
                [Particles_fromStrippingLine ], ## required selections
                Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^e+ ^mu- ) ^pi+ ]CC" ,
                Branches      = {
                    "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ e+ mu- ) pi+ ]CC"
                    ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ e+ mu- ) pi+ ]CC"
                    ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ ^e+ mu- ) pi+ ]CC"
                    ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ e+ ^mu- ) pi+ ]CC"
                    ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ e+ mu- ) ^pi+ ]CC"
                    ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ e+ mu- ) pi+ ]CC"
                    ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+  e+ mu- ) pi+ ]CC"
                   }  ,
                ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]
                )
AddAlltheTools(Tuple1, 'LFV')
AddLokiVars_MC(Tuple1)
AddLoKi_Keys(Tuple1, 'LFV')
AddToolMCTruth(Tuple1)
AddBremInfo(Tuple1)

Tuple2 = TupleSelection (
            'DstD2KPiMue', ## unique name
            [Particles_fromStrippingLine ], ## required selections
            Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^e- ) ^pi+ ]CC" ,
            Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ e- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ e- ) pi+ ]CC"
                ,"mu"      : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ e- ) pi+ ]CC"
                ,"e"       : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^e- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ e- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ e- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+  mu+ e- ) pi+ ]CC"
                }  ,
            ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]
            )
AddAlltheTools(Tuple2, 'LFV')
AddLokiVars_MC(Tuple2)
AddLoKi_Keys(Tuple2, 'LFV')
AddToolMCTruth(Tuple2)
AddBremInfo(Tuple2)

Tuple18 = TupleSelection (
            'DstD2KPiMuMu', ## unique name 
           [ Particles_fromStrippingLine ], ## required selections
             Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^mu+ ^mu- ) ^pi+ ]CC" ,
             Branches      = {
                "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ mu+ mu- ) pi+ ]CC"
                ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ mu+ mu- ) pi+ ]CC"
                ,"mu0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^mu+ mu- ) pi+ ]CC"
                ,"mu1"     : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ ^mu- ) pi+ ]CC"
                ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) ^pi+ ]CC"
                ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ mu+ mu- ) pi+ ]CC"
                    } , 
             ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]   
             )
AddAlltheTools(Tuple18, 'muons')
AddLokiVars_MC(Tuple18)
AddLoKi_Keys(Tuple18, 'muons')
AddToolMCTruth(Tuple18)

##### commenting control line #####
# Tuple19 = TupleSelection (
#             'DstD2KPiPiPi', ## unique name 
#            [ Particles_fromControlStrippingLine ], ## required selections CONTROL LINE
#              Decay         = "[ D*(2010)+ -> ^(D0 -> ^K- ^pi+ ^pi+ ^pi- ) ^pi+ ]CC" ,
#              Branches      = {
#                 "h0"       : "[ D*(2010)+ -> (D0 -> ^K- pi+ pi+ pi- ) pi+ ]CC"
#                 ,"h1"      : "[ D*(2010)+ -> (D0 -> K- ^pi+ pi+ pi- ) pi+ ]CC"
#                 ,"l0"     : "[ D*(2010)+ -> (D0 -> K- pi+ ^pi+ pi- ) pi+ ]CC"
#                 ,"l1"     : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ ^pi- ) pi+ ]CC"
#                 ,"Slowpi"  : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ pi- ) ^pi+ ]CC"
#                 ,"D"       : "[ D*(2010)+ -> ^(D0 -> K- pi+ pi+ pi- ) pi+ ]CC"
#                 ,"Dst"     : "[ D*(2010)+ -> (D0 -> K- pi+ pi+ pi- ) pi+ ]CC"
#                     } , 
#              ToolList  = ["TupleToolEventInfo","TupleToolKinematic","TupleToolPrimaries" ]   
#              )
# AddAlltheTools(Tuple19, 'electrons')
# # AddLokiVars_MC(Tuple19)
# AddLokiVars_MC_no_DTF_D0costr(Tuple19)
# AddLoKi_Keys(Tuple19, 'electrons')
# AddToolMCTruth(Tuple19)





##----------------------------------------------------#
##   MCTRUTHTUPLE for Simulation
##   --> NB: put the right Decay descriptor
##----------------------------------------------------#

MyMCDecayTreeTuple = MCDecayTreeTuple("MCTruthTuple")
MyMCDecayTreeTuple.Decay = "[ D*(2010)+ -> ^(D0 ==> ^K- ^pi+ ^pi+ ^pi- ) ^pi+ ]CC"
MyMCDecayTreeTuple.TupleName = "MCTruthTree"
MyMCDecayTreeTuple.ToolList+=["MCTupleToolKinematic", 
                                  "MCTupleToolPrimaries", 
                                  "MCTupleToolHierarchy",
                                  "TupleToolEventInfo",
                                  #"MCTupleToolReconstructed",
                                  "MCTupleToolPID"]
#MyMCDecayTreeTuple.addTool(MCTupleToolKinematic())
#MyMCDecayTreeTuple.MCTupleToolKinematic.Verbose=True
#MyMCDecayTreeTuple.MCTupleToolKinematic.StoreStablePropertime = True
#MyMCDecayTreeTuple.addTool(MCTupleToolReconstructed())
#MyMCDecayTreeTuple.MCTupleToolReconstructed.Verbose=True




##----------------------------------------------------#
##
##   Execution part (ie Sequences and DV settings)
##
##----------------------------------------------------#
from Configurables import GaudiSequencer

DaVinci().TupleFile = "RARECHARM_4BODIES.root"

Seq_KPi_tag      = SelectionSequence('Seq_KPi_tag',     TopSelection = Tuple17)
Seq_KPiEE_MCTruth_tag = GaudiSequencer("Seq_KPiEE_MCTruth_tag")
Seq_KPiEE_MCTruth_tag.Members  += [ MyMCDecayTreeTuple ]
DaVinci().UserAlgorithms += [Seq_KPi_tag.sequence()]
DaVinci().UserAlgorithms += [Seq_KPiEE_MCTruth_tag]


Seq_KPiMumEp_tag        = SelectionSequence('Seq_KPiMumEp_tag',       TopSelection = Tuple1)
Seq_KPiMupEm_tag        = SelectionSequence('Seq_KPiMupEm_tag',       TopSelection = Tuple2)
DaVinci().UserAlgorithms += [Seq_KPiMupEm_tag.sequence()]
DaVinci().UserAlgorithms += [Seq_KPiMumEp_tag.sequence()]



Seq_KPiMuMu_tag        = SelectionSequence('Seq_KPiMuMu_tag',       TopSelection = Tuple18)
DaVinci().UserAlgorithms += [Seq_KPiMuMu_tag.sequence()]

##### commenting control line #####
# Seq_KPiPiPi_tag        = SelectionSequence('Seq_KPiPiPi_tag',       TopSelection = Tuple19)
# DaVinci().UserAlgorithms += [Seq_KPiPiPi_tag.sequence()]
