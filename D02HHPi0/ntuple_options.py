
from Configurables import DaVinci
from D02HHPi0.utils import make_tuple, make_mctuple, get_strippingfilter
from D02HHPi0.dicts import descriptors

# use 'AllStreams' for unfiltered samples
stream = 'Dst2D0Pi_D02PiPiPi0.StripTrig'

mc = DaVinci().getProp('Simulation')
if not mc:
    DaVinci().EventPreFilters = get_strippingfilter()

DaVinci().RootInTES = '/Event/{0}'.format(stream)

for tuplename in descriptors:
    dtt = make_tuple(tuplename, simulation = mc, stream = stream)
    DaVinci().UserAlgorithms.append(dtt)
                                               
    if mc and 'Pi0R' in tuplename:
        dttmc = make_mctuple(tuplename + '_MC')
        DaVinci().UserAlgorithms.append(dttmc)
