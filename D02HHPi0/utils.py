from D02HHPi0.dicts import triggers, get_descriptor, get_inputlocation, \
    get_mcdescriptor, get_template, mctools, strippingLines, \
    get_mctemplate, mcDescriptors
from DecayTreeTuple.Configuration import DecayTreeTuple
from Configurables import TupleToolRecoStats, TupleToolProtoPData, \
    TupleToolMCTruth, LoKi__Hybrid__TupleTool, MCDecayTreeTuple
from PhysConf.Filters import LoKi_Filters


def make_tuple(name, stream = None, simulation = False,
               widemass = False):
    '''Make a DecayTreeTuple for a specific final state. The decay mode
    and input location are inferred from the name.'''
    
    dtt = DecayTreeTuple(name, Decay = get_descriptor(name))
    dtt.setDescriptorTemplate(get_template(name))
    
    inputloc = get_inputlocation(name)
    if stream:
        inputloc = '/Event/{0}/{1}'.format(stream, inputloc)
    if widemass:
        inputloc = inputloc.replace('Line', 'WIDEMASS_Line')
    dtt.Inputs = [inputloc]
    configure_tools(dtt, simulation)
    return dtt


def configure_tools(dtt, simulation = False):
    '''Configure tuple tools'''

    resolved = 'gamma' in dtt.Decay
    # Check if the decay descriptor uses the intermediate K*
    Kstar = 'K*' in dtt.Decay
    
    dtt.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        "TupleToolProtoPData",
        "TupleToolTrigger",
        ]

    # RecoStats to filling SpdMult, etc
    dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    dtt.TupleToolRecoStats.Verbose=False
    
    dtt.addTupleTool("TupleToolTISTOS")
    dtt.TupleToolTISTOS.Verbose=True
    dtt.TupleToolTISTOS.VerboseL0=True
    dtt.TupleToolTISTOS.VerboseHlt1=True
    dtt.TupleToolTISTOS.VerboseHlt2=True
    dtt.TupleToolTISTOS.TriggerList = [trig + 'Decision' for trig in triggers]
    
    dtt.addTool(TupleToolProtoPData,name="TupleToolProtoPData") 
    dtt.TupleToolProtoPData.DataList = ["CaloEcalE", "CaloHcalE",
                                        "ProtoPNeutral", "IsPhoton"] 

    LoKi_DTFMASS = LoKi__Hybrid__TupleTool("LoKi_DTFMASS")
    LoKi_DTFMASS.Variables = {
        # chi^2 of fit
        "FIT_CHI2"      : "DTF_CHI2(True, strings(['D0','pi0']) )"
        # ndof of fit
        , "FIT_NDOF"      : "DTF_NDOF(True, strings(['D0','pi0']) )"
        ###############FIT0 without D0 mass constraint new added wrt to v21(=new1)
        # chi^2 of fit
        , "FIT0_CHI2"      : "DTF_CHI2(True, 'pi0' )"
        # ndof of fit
        , "FIT0_NDOF"      : "DTF_NDOF(True, 'pi0' )"
    }
    # Variables for D* & D
    # Function for the mass error
    merr = '0.5/M * M2ERR2**.5'
    for pref in '', 'D':
        childfunc = child_funcs[pref]
        LoKi_DTFMASS.Variables.update(
            # Masses and their errors with various constraints
            {"FIT_" + pref + "M"       : DTF_FUN(childfunc('M', Kstar), True, 'pi0')
             , "FIT_" + pref + "MERR"  : DTF_FUN(childfunc(merr, Kstar), True, 'pi0')
             , "FIT_" + pref + "M1"    : DTF_FUN(childfunc('M', Kstar), True, 'D0')
             , "FIT_" + pref + "M1ERR" : DTF_FUN(childfunc(merr, Kstar), True, 'D0')
             , "FIT_" + pref + "M2"    : DTF_FUN(childfunc('M', Kstar), True, ['D0', 'pi0'])
             , "FIT_" + pref + "M2ERR" : DTF_FUN(childfunc(merr, Kstar), True, ['D0', 'pi0'])
             , "FIT_" + pref + "M3"    : DTF_FUN(childfunc('M', Kstar), True)
             , "FIT_" + pref + "M3ERR" : DTF_FUN(childfunc(merr, Kstar), True)
             , "FIT_" + pref + "M4"    : DTF_FUN(childfunc('M', Kstar), False, ['D0', 'pi0'])
             , "FIT_" + pref + "M4ERR" : DTF_FUN(childfunc(merr, Kstar), False, ['D0', 'pi0'])
             # Lifetime
             , "FIT_" + pref + "LT"    : DTF_default(childfunc("BPVLTIME('PropertimeFitter/ProperTime::PUBLIC')", Kstar))
             # VertexChi2
             , "FIT_" + pref + "VTXCHI2DOF": DTF_default(childfunc("VFASPF(VCHI2/VDOF)", Kstar))
             # Max DOCA of children
             , "FIT_" + pref + "MAXDOCA"   : DTF_default(childfunc("DOCAMAX", Kstar))
             # DIRA
             , "FIT_" + pref + "DIRABPV"   : DTF_default(childfunc("BPVDIRA", Kstar))
             #  
             ####new added wrt to v21
             # IPCHI2
             , "FIT_" + pref + "IPCHI2BPV"   : DTF_FUN(childfunc("BPVIPCHI2()", Kstar), False, ['D0', 'pi0'])
             # DVCHI2
             , "FIT_" + pref + "DVCHI2BPV"   : DTF_default(childfunc("BPVVDCHI2", Kstar))
             # LifeTime CHI2
             , "FIT_" + pref + "LTCHI2BPV"   : DTF_default(childfunc("BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC')", Kstar))
             # Fitted LifeTime CHI2
             , "FIT_" + pref + "LTFITCHI2BPV": DTF_default(childfunc("BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC')", Kstar))}
        )

        # Functors for all particles
        for pref, childfunc in child_funcs.items():
            if pref.startswith('gamma') and not resolved:
                continue
            if not Kstar and pref == 'Kstr':
                continue
            # Momentum components
            # Add LoKi_ID as safety check that we're getting the right particle
            LoKi_DTFMASS.Variables.update(
                {pref + 'LoKi_ID' : childfunc('ID', Kstar)
                 , "FIT_" + pref + "P"    : DTF_default(childfunc("P", Kstar))
                 , "FIT_" + pref + "PT"   : DTF_default(childfunc("PT", Kstar))
                 , "FIT_" + pref + "PX"   : DTF_default(childfunc("PX", Kstar))
                 , "FIT_" + pref + "PY"   : DTF_default(childfunc("PY", Kstar))
                 , "FIT_" + pref + "PZ"   : DTF_default(childfunc("PZ", Kstar))
                 , "FIT_" + pref + "E"    : DTF_default(childfunc("E", Kstar))
                 , "FIT_" + pref + "ETA"  : DTF_default(childfunc("ETA", Kstar))
                 }
            )
            if pref != 'Pi0':
                # IPCHI2 for all but pi0
                LoKi_DTFMASS.Variables["FIT_" + pref + "MIPCHI2"] = \
                    DTF_default(childfunc("MIPCHI2DV(PRIMARY)", Kstar))
            else:
                # pi0 mass and its error without pi0 constraint (just D0 & vertex)
                LoKi_DTFMASS.Variables["FIT_Pi0M"] = \
                    DTF_FUN(childfunc('M', Kstar), True, 'D0')
                LoKi_DTFMASS.Variables["FIT_Pi0MERR"] = \
                    DTF_FUN(childfunc(merr, Kstar), True, 'D0')
                
    dtt.Dstr.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_DTFMASS"]
    dtt.Dstr.addTool(LoKi_DTFMASS)
    dtt.pi0.ToolList +=  ["TupleToolPi0Info"]
    if resolved:
        dtt.gamma1.ToolList +=  ["TupleToolPhotonInfo"]
        dtt.gamma2.ToolList +=  ["TupleToolPhotonInfo"]

    if simulation:
        configure_mc_tools(dtt)
    return dtt


def configure_mc_tools(dtt):
    '''Configure MC tools'''
    
    dtt.addTool(TupleToolMCTruth, name="truth")
    dtt.truth.ToolList += mctools
    dtt.ToolList += ["TupleToolMCTruth/truth"]
    dtt.addTupleTool("TupleToolMCBackgroundInfo")
    # For L0 efficiency correction
    dtt.addTupleTool("TupleToolL0Calo")

    # Use mcMatch for truth matching
    name = dtt.name().replace('Pi0M', 'Pi0').replace('Pi0R', 'Pi0')
    desc = get_mcdescriptor(name)
    ttloki = dtt.Dstr.addTupleTool('LoKi__Hybrid__TupleTool/lokitruth')
    ttloki.Preambulo += ['from LoKiPhysMC.functions import mcMatch']
    ttloki.Variables = \
        {'mcMatch' : 'switch(mcMatch({0!r}), 1, 0)'.format(desc)}
    for name, desc in mcDescriptors.items():
        ttloki.Variables['mcMatch_' + name] = \
            'switch(mcMatch({0!r}), 1, 0)'.format(desc)


def make_mctuple(name):
    '''Make an MCDecayTreeTuple for the given channel.'''
    name = name.replace('Pi0M', 'Pi0').replace('Pi0R', 'Pi0')

    dtt = MCDecayTreeTuple(name)
    dtt.Decay = get_mcdescriptor(name)
    dtt.setDescriptorTemplate(get_mctemplate(name))
    
    dtt.ToolList += mctools

    return dtt


def get_strippingfilter():
    '''Get a filter on the stripping line decisions.'''
    filters = LoKi_Filters(
        STRIP_code = ' | '.join('HLT_PASS({0}Decision)'.format(line)
                                for line in strippingLines))
    return filters.filters('StrippingFilters')


def DTF_FUN(func, constrain = False, massconstraints = []):
    form = 'DTF_FUN(' + func
    if constrain or massconstraints:
        form += ', ' + str(constrain)
    if massconstraints:
        if isinstance(massconstraints, str):
            form += ', {0!r}'.format(massconstraints)
        else:
            form += ', strings({0!r})'.format(massconstraints)
    form += ')'
    return form


def DTF_default(func):
    return DTF_FUN(func, True, ['D0', 'pi0'])


def CHILD(func, n):
    return 'CHILD({0}, {1})'.format(func, n)


def D_func(func, Kstar = True):
    return CHILD(func, 1)


def Pis_func(func, Kstar = True):
    return CHILD(func, 2)


def Pi0_func(func, Kstar = True):
    return CHILD(CHILD(func, (2 if Kstar else 3)), 1)


def H1_func(func, Kstar = True):
    if Kstar:
        return CHILD(CHILD(CHILD(func, 1), 1), 1)
    return CHILD(CHILD(func, 1), 1)


def H2_func(func, Kstar = True):
    if Kstar:
        return CHILD(CHILD(CHILD(func, 2), 1), 1)
    return CHILD(CHILD(func, 2), 1)
    

def gamma1_func(func, Kstar = True):
    return Pi0_func(CHILD(func, 1), Kstar)


def gamma2_func(func, Kstar = True):
    return Pi0_func(CHILD(func, 2), Kstar)


child_funcs = {
    '' : (lambda func, Kstar : func),
    'D' : D_func,
    'Pis' : Pis_func,
    'H1' : H1_func,
    'H2' : H2_func,
    'Pi0' : Pi0_func,
    'gamma1' : gamma1_func,
    'gamma2' : gamma2_func
}
