from Configurables import TrackSelector
from Configurables import CombineParticles
from Configurables import PhotonMaker
from Configurables import ResolvedPi0Maker
from DecayTreeTuple.Configuration import DecayTreeTuple
from Configurables import DaVinci
from Configurables import TupleToolDecay
from Configurables import CheckPV
from Configurables import NoPIDsParticleMaker
from Configurables import FilterDesktop
from Configurables import SubstitutePID
from Configurables import GaudiSequencer

PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker',
            PtCut = 200.0)

ResolvedPi0Maker('REBUILD:StdLoosePi02gg',
                 Output = 'Phys/REBUILD:StdLoosePi02gg/Particles',
                 MassWindow = 60.0,
                 DecayDescriptor = 'Pi0')
ResolvedPi0Maker('REBUILD:StdLoosePi02gg').addTool(PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker'))

FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles',
              Code = "mcMatch('pi0 -> gamma gamma') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdLooseResolvedPi0/Particles'])

FilterDesktop('[pi+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[pi+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[pi+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsPions/Particles'])

TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions',
                    Output = 'Phys/REBUILD:StdAllNoPIDsPions/Particles',
                    Particle = 'pion')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions').addTool(TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector'))

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi',
               Inputs = ['Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Sub/Particles'],
               Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_pipi/Particles',
               Decay = '[D*(2010)+ -> ^( D0 -> ^pi- ^pi+ ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC')

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi').addBranches({'H1': '[D*(2010)+ -> ( D0 -> ^pi- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'D': '[D*(2010)+ -> ^( D0 -> pi- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'gamma1': '[D*(2010)+ -> ( D0 -> pi- pi+ ( pi0 -> ^gamma gamma ) ) pi+]CC', 'H2': '[D*(2010)+ -> ( D0 -> pi- ^pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'piSoft': '[D*(2010)+ -> ( D0 -> pi- pi+ ( pi0 -> gamma gamma ) ) ^pi+]CC', 'Dstr': '[D*(2010)+ -> ( D0 -> pi- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'pi0': '[D*(2010)+ -> ( D0 -> pi- pi+ ^( pi0 -> gamma gamma ) ) pi+]CC', 'gamma2': '[D*(2010)+ -> ( D0 -> pi- pi+ ( pi0 -> gamma ^gamma ) ) pi+]CC'})

















CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[K+]CC_RefitPVs_MCSel/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles', 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC') & BPVVALID()",
                 DecayDescriptors = ['[D0 -> K+ pi- pi0]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles')

CheckPV('CheckPV')

FilterDesktop('REBUILD:StdLooseResolvedPi0',
              Inputs = ['Phys/REBUILD:StdLoosePi02gg/Particles'],
              Code = "ADMASS('pi0') < 30 * MeV ",
              Output = 'Phys/REBUILD:StdLooseResolvedPi0/Particles')

TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons',
                    Output = 'Phys/REBUILD:StdAllNoPIDsKaons/Particles',
                    Particle = 'kaon')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons').addTool(TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector'))

CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles')

SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Sub',
              Code = 'ALL',
              Inputs = ['Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles'],
              Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Sub/Particles',
              MaxChi2PerDoF = 1e+100,
              Substitutions = {'D*(2010)- -> ( D~0 -> X+ ^K- ( pi0 -> gamma gamma ) ) pi-': 'pi-', 'D*(2010)+ -> ( D0 -> X- ^K+ ( pi0 -> gamma gamma ) ) pi+': 'pi+'})

FilterDesktop('[K+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[K+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[K+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsKaons/Particles'])

GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Seq',
               Members = [CheckPV('CheckPV'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons'), FilterDesktop('[K+]CC_RefitPVs_MCSel'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions'), FilterDesktop('[pi+]CC_RefitPVs_MCSel'), ResolvedPi0Maker('REBUILD:StdLoosePi02gg'), FilterDesktop('REBUILD:StdLooseResolvedPi0'), FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel'), CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb'), CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb'), SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Sub'), DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi')])

FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles',
              Code = "mcMatch('pi0 -> gamma gamma') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdLooseResolvedPi0/Particles'])

FilterDesktop('[pi+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[pi+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[pi+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsPions/Particles'])

PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker',
            PtCut = 200.0)

ResolvedPi0Maker('REBUILD:StdLoosePi02gg',
                 Output = 'Phys/REBUILD:StdLoosePi02gg/Particles',
                 MassWindow = 60.0,
                 DecayDescriptor = 'Pi0')
ResolvedPi0Maker('REBUILD:StdLoosePi02gg').addTool(PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker'))

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_KK',
               Inputs = ['Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Sub/Particles'],
               Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_KK/Particles',
               Decay = '[D*(2010)+ -> ^( D0 -> ^K- ^K+ ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC')

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_KK').addBranches({'H1': '[D*(2010)+ -> ( D0 -> ^K- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'D': '[D*(2010)+ -> ^( D0 -> K- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'gamma1': '[D*(2010)+ -> ( D0 -> K- K+ ( pi0 -> ^gamma gamma ) ) pi+]CC', 'H2': '[D*(2010)+ -> ( D0 -> K- ^K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'piSoft': '[D*(2010)+ -> ( D0 -> K- K+ ( pi0 -> gamma gamma ) ) ^pi+]CC', 'Dstr': '[D*(2010)+ -> ( D0 -> K- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'pi0': '[D*(2010)+ -> ( D0 -> K- K+ ^( pi0 -> gamma gamma ) ) pi+]CC', 'gamma2': '[D*(2010)+ -> ( D0 -> K- K+ ( pi0 -> gamma ^gamma ) ) pi+]CC'})

















CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[K+]CC_RefitPVs_MCSel/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles', 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC') & BPVVALID()",
                 DecayDescriptors = ['[D0 -> K+ pi- pi0]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles')

CheckPV('CheckPV')

FilterDesktop('REBUILD:StdLooseResolvedPi0',
              Inputs = ['Phys/REBUILD:StdLoosePi02gg/Particles'],
              Code = "ADMASS('pi0') < 30 * MeV ",
              Output = 'Phys/REBUILD:StdLooseResolvedPi0/Particles')

TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons',
                    Output = 'Phys/REBUILD:StdAllNoPIDsKaons/Particles',
                    Particle = 'kaon')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons').addTool(TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector'))

CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles')

TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions',
                    Output = 'Phys/REBUILD:StdAllNoPIDsPions/Particles',
                    Particle = 'pion')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions').addTool(TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector'))

FilterDesktop('[K+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[K+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[K+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsKaons/Particles'])

SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Sub',
              Code = 'ALL',
              Inputs = ['Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles'],
              Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Sub/Particles',
              MaxChi2PerDoF = 1e+100,
              Substitutions = {'D*(2010)+ -> ( D0 -> ^pi- X+ ( pi0 -> gamma gamma ) ) pi+': 'K-', 'D*(2010)- -> ( D~0 -> ^pi+ X- ( pi0 -> gamma gamma ) ) pi-': 'K+'})

GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Seq',
               Members = [CheckPV('CheckPV'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons'), FilterDesktop('[K+]CC_RefitPVs_MCSel'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions'), FilterDesktop('[pi+]CC_RefitPVs_MCSel'), ResolvedPi0Maker('REBUILD:StdLoosePi02gg'), FilterDesktop('REBUILD:StdLooseResolvedPi0'), FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel'), CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb'), CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb'), SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Sub'), DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_KK')])

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi',
               Inputs = ['Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Sub/Particles'],
               Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi/Particles',
               Decay = '[D*(2010)+ -> ^( D0 -> ^K- ^pi+ ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC')

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi').addBranches({'H1': '[D*(2010)+ -> ( D0 -> ^K- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'D': '[D*(2010)+ -> ^( D0 -> K- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'gamma1': '[D*(2010)+ -> ( D0 -> K- pi+ ( pi0 -> ^gamma gamma ) ) pi+]CC', 'H2': '[D*(2010)+ -> ( D0 -> K- ^pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'piSoft': '[D*(2010)+ -> ( D0 -> K- pi+ ( pi0 -> gamma gamma ) ) ^pi+]CC', 'Dstr': '[D*(2010)+ -> ( D0 -> K- pi+ ( pi0 -> gamma gamma ) ) pi+]CC', 'pi0': '[D*(2010)+ -> ( D0 -> K- pi+ ^( pi0 -> gamma gamma ) ) pi+]CC', 'gamma2': '[D*(2010)+ -> ( D0 -> K- pi+ ( pi0 -> gamma ^gamma ) ) pi+]CC'})

















FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles',
              Code = "mcMatch('pi0 -> gamma gamma') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdLooseResolvedPi0/Particles'])

FilterDesktop('[pi+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[pi+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[pi+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsPions/Particles'])

PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker',
            PtCut = 200.0)

ResolvedPi0Maker('REBUILD:StdLoosePi02gg',
                 Output = 'Phys/REBUILD:StdLoosePi02gg/Particles',
                 MassWindow = 60.0,
                 DecayDescriptor = 'Pi0')
ResolvedPi0Maker('REBUILD:StdLoosePi02gg').addTool(PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker'))

SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Sub',
              Code = 'ALL',
              Inputs = ['Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles'],
              Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Sub/Particles',
              MaxChi2PerDoF = 1e+100,
              Substitutions = {'D*(2010)- -> ( D~0 -> ^pi+ X- ( pi0 -> gamma gamma ) ) pi-': 'K+', 'D*(2010)+ -> ( D0 -> ^pi- X+ ( pi0 -> gamma gamma ) ) pi+': 'K-', 'D*(2010)- -> ( D~0 -> X+ ^K- ( pi0 -> gamma gamma ) ) pi-': 'pi-', 'D*(2010)+ -> ( D0 -> X- ^K+ ( pi0 -> gamma gamma ) ) pi+': 'pi+'})

CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[K+]CC_RefitPVs_MCSel/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles', 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC') & BPVVALID()",
                 DecayDescriptors = ['[D0 -> K+ pi- pi0]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles')

CheckPV('CheckPV')

FilterDesktop('REBUILD:StdLooseResolvedPi0',
              Inputs = ['Phys/REBUILD:StdLoosePi02gg/Particles'],
              Code = "ADMASS('pi0') < 30 * MeV ",
              Output = 'Phys/REBUILD:StdLooseResolvedPi0/Particles')

TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons',
                    Output = 'Phys/REBUILD:StdAllNoPIDsKaons/Particles',
                    Particle = 'kaon')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons').addTool(TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector'))

CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles')

TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions',
                    Output = 'Phys/REBUILD:StdAllNoPIDsPions/Particles',
                    Particle = 'pion')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions').addTool(TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector'))

FilterDesktop('[K+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[K+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[K+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsKaons/Particles'])

GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Seq',
               Members = [CheckPV('CheckPV'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons'), FilterDesktop('[K+]CC_RefitPVs_MCSel'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions'), FilterDesktop('[pi+]CC_RefitPVs_MCSel'), ResolvedPi0Maker('REBUILD:StdLoosePi02gg'), FilterDesktop('REBUILD:StdLooseResolvedPi0'), FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel'), CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb'), CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb'), SubstitutePID('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Sub'), DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi')])

PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker',
            PtCut = 200.0)

ResolvedPi0Maker('REBUILD:StdLoosePi02gg',
                 Output = 'Phys/REBUILD:StdLoosePi02gg/Particles',
                 MassWindow = 60.0,
                 DecayDescriptor = 'Pi0')
ResolvedPi0Maker('REBUILD:StdLoosePi02gg').addTool(PhotonMaker('REBUILD:StdLoosePi02gg.PhotonMaker'))

CheckPV('CheckPV')

FilterDesktop('[pi+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[pi+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[pi+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsPions/Particles'])

FilterDesktop('REBUILD:StdLooseResolvedPi0',
              Inputs = ['Phys/REBUILD:StdLoosePi02gg/Particles'],
              Code = "ADMASS('pi0') < 30 * MeV ",
              Output = 'Phys/REBUILD:StdLooseResolvedPi0/Particles')

CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[K+]CC_RefitPVs_MCSel/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles', 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC') & BPVVALID()",
                 DecayDescriptors = ['[D0 -> K+ pi- pi0]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles')

CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb',
                 Inputs = ['Phys/[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb/Particles', 'Phys/[pi+]CC_RefitPVs_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
                 Output = 'Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles')

TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons',
                    Output = 'Phys/REBUILD:StdAllNoPIDsKaons/Particles',
                    Particle = 'kaon')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons').addTool(TrackSelector('REBUILD:StdAllNoPIDsKaons.TrackSelector'))

FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/pi0 -> gamma gamma_RefitPVs_MCSel/Particles',
              Code = "mcMatch('pi0 -> gamma gamma') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdLooseResolvedPi0/Particles'])

TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions',
                    Output = 'Phys/REBUILD:StdAllNoPIDsPions/Particles',
                    Particle = 'pion')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions').addTool(TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector'))

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel',
               Inputs = ['Phys/[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb/Particles'],
               Output = 'Phys/DstrD2KPiPi0R_WS_MCTruthSel/Particles',
               Decay = '[D*(2010)+ -> ^( D0 -> ^pi- ^K+ ^( pi0 -> ^gamma ^gamma ) ) ^pi+]CC')

DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel').addBranches({'H1': '[D*(2010)+ -> ( D0 -> ^pi- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'D': '[D*(2010)+ -> ^( D0 -> pi- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'gamma1': '[D*(2010)+ -> ( D0 -> pi- K+ ( pi0 -> ^gamma gamma ) ) pi+]CC', 'H2': '[D*(2010)+ -> ( D0 -> pi- ^K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'piSoft': '[D*(2010)+ -> ( D0 -> pi- K+ ( pi0 -> gamma gamma ) ) ^pi+]CC', 'Dstr': '[D*(2010)+ -> ( D0 -> pi- K+ ( pi0 -> gamma gamma ) ) pi+]CC', 'pi0': '[D*(2010)+ -> ( D0 -> pi- K+ ^( pi0 -> gamma gamma ) ) pi+]CC', 'gamma2': '[D*(2010)+ -> ( D0 -> pi- K+ ( pi0 -> gamma ^gamma ) ) pi+]CC'})

















FilterDesktop('[K+]CC_RefitPVs_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch', 'from LoKiMC.decorators import MCABSID'],
              ReFitPVs = True,
              Output = 'Phys/[K+]CC_RefitPVs_MCSel/Particles',
              Code = "mcMatch('[K+]CC') & BPVVALID()",
              Inputs = ['Phys/REBUILD:StdAllNoPIDsKaons/Particles'])

GaudiSequencer('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb_MCUnbiasedSeq',
               Members = [CheckPV('CheckPV'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsKaons'), FilterDesktop('[K+]CC_RefitPVs_MCSel'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions'), FilterDesktop('[pi+]CC_RefitPVs_MCSel'), ResolvedPi0Maker('REBUILD:StdLoosePi02gg'), FilterDesktop('REBUILD:StdLooseResolvedPi0'), FilterDesktop('pi0 -> gamma gamma_RefitPVs_MCSel'), CombineParticles('[D0 ==> K+ pi- ( pi0 -> gamma gamma )]CC_RefitPVs_Comb'), CombineParticles('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb'), DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel')])

DaVinci().UserAlgorithms += [GaudiSequencer('[D*(2010)+ => ( D0 ==> K+ pi- ( pi0 -> gamma gamma ) ) pi+]CC_RefitPVs_Comb_MCUnbiasedSeq'), GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi_Seq'), GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi_Seq'), GaudiSequencer('DstrD2KPiPi0R_WS_MCTruthSel_as_KK_Seq')]

dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_KK = DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_KK')

dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi = DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi')

dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_pipi = DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel_as_pipi')

dtt = DecayTreeTuple('DstrD2KPiPi0R_WS_MCTruthSel')


from D02HHPi0.utils import configure_tools
for _dtt in dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_KK, dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_Kpi, dtt_DstrD2KPiPi0R_WS_MCTruthSel_as_pipi, dtt:
    configure_tools(_dtt, True)
