'''Fixed inputs for ntuple config'''

L0Triggers = [
    'L0Hadron',
    'L0Photon',
    ]

HLT1Triggers = [
    'Hlt1TrackAllL0',
    'Hlt1TrackMVA',
    'Hlt1TwoTrackMVA',
    'Hlt1TrackMVATight',
    'Hlt1TwoTrackMVATight',
    ]

HLT2Triggers = [
    'Hlt2CharmHadInclDst2PiD02HHXBDT',
    'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0M',
    'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0R',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0M',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0R',
    'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0M',
    'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0R',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0M',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0R']

triggers = L0Triggers + HLT1Triggers + HLT2Triggers

# Note: the Kpi lines include RS and WS
# The regular lines have a 15 MeV mass window on the pi0
# while the Kpipi0_R_WIDEMASS line uses everything from StdLooseResolvedPi0
# All lines have a mass window of 150 MeV on the D0
strippingLines = ['DstarD0ToHHPi0_{0}pi0_R_Line'.format(fs)
                  for fs in ('pipi', 'Kpi', 'KK')]\
                      + ['DstarD0ToHHPi0_Kpipi0_R_WIDEMASS_Line']

# Reconstructed decay descriptors
_descriptors = {'Dstr2DPiPiPi0' : '[D*(2010)+ -> ( D0 -> ( K*(892)0  -> pi-  pi+ )  pi0 )  pi+]CC',
                'Dstr2DKKPi0' : '[D*(2010)+ -> ( D0 -> ( K*(892)0  -> K-  K+ )  pi0 )  pi+]CC',
                'Dstr2DKPiPi0_RS' : '[D*(2010)+ -> ( D0 -> ( K*(892)0  -> K-  pi+ )  pi0 )  pi+]CC',
                'Dstr2DKPiPi0_WS' : '[D*(2010)+ -> ( D0 -> ( K*(892)0  -> K+  pi- )  pi0 )  pi+]CC'}
descriptors = {}
for pi0, suff in ('pi0', 'M'), ('( pi0 -> gamma gamma )', 'R'):
    for name, desc in _descriptors.items():
        descriptors[name.replace('Pi0', 'Pi0' + suff)] \
            = desc.replace('pi0', pi0)
del _descriptors

# Descriptor templates
_branches = [('[D*(2010)+', '${Dstr}[D*(2010)+'),
             ('( D0', '${D}( D0'),
             ('( K*(892)0', '${Kstr}( K*(892)0'),
             ('pi-  pi+', '${H1}pi- ${H2}pi+'),
             ('K-  K+', '${H1}K- ${H2}K+'),
             ('K-  pi+', '${H1}K- ${H2}pi+'),
             ('K+  pi-', '${H1}K+ ${H2}pi-'),
             ('pi0', '${pi0}pi0'),
             ('( ${pi0}pi0', '${pi0}( pi0'),
             ('gamma gamma', '${gamma1}gamma ${gamma2}gamma'),
             ('pi+]CC', '${piSoft}pi+]CC')]


def make_templates(descriptors):
    '''Make descriptor templates from the dict of descriptors.'''
    templates = dict(descriptors)
    for name, desc in templates.items():
        for pat, sub in _branches:
            desc = desc.replace(pat, sub)
        templates[name] = desc
    return templates


templates = make_templates(descriptors)

# Input locations
inputLocations = {}
for name in descriptors:
    _name = name.replace('_WS', '').replace('_RS', '')
    _name = _name[len('Dstr2D'):]
    _name = _name.replace('Pi', 'pi').replace('pi0', 'pi0_')
    line = 'DstarD0ToHHPi0_{0}_Line'.format(_name)
    inputLocations[name] = '/'.join(['Phys', line, 'Particles'])

# MC decay descriptors
mcDescriptors = {'Dstr2DPiPiPi0' : '[D*(2010)+ => ( D0 ==> pi-  pi+ ( pi0 -> gamma gamma ) )  pi+]CC',
                 'Dstr2DKKPi0' : '[D*(2010)+ => ( D0 ==> K-  K+ ( pi0 -> gamma gamma ) )  pi+]CC',
                 'Dstr2DKPiPi0_RS' : '[D*(2010)+ => ( D0 ==> K-  pi+ ( pi0 -> gamma gamma ) )  pi+]CC',
                 'Dstr2DKPiPi0_WS' : '[D*(2010)+ => ( D0 ==> K+  pi- ( pi0 -> gamma gamma ) )  pi+]CC'}
mcTemplates = make_templates(mcDescriptors)
    

def get_value(tuplename, _dict):
    '''Find the value in the dict whose key starts with tuplename.'''
    for name, val in _dict.items():
        if tuplename.startswith(name):
            return val


def get_descriptor(tuplename):
    '''Get the decay descriptor from the tuple name.'''
    return get_value(tuplename, descriptors)


def get_template(tuplename):
    '''Get the decay descriptor template from the tuple name.'''
    return get_value(tuplename, templates)


def get_inputlocation(tuplename):
    '''Get the input location given the tuple name.'''
    return get_value(tuplename, inputLocations)


def get_mcdescriptor(tuplename):
    '''Get the MC decay descriptor given the tuple name.'''
    return get_value(tuplename.replace('Pi0M', 'Pi0').replace('Pi0R', 'Pi0'),
                     mcDescriptors)


def get_mctemplate(tuplename):
    '''Get the MC descriptor template given the tuple name.'''
    return get_value(tuplename.replace('Pi0M', 'Pi0').replace('Pi0R', 'Pi0'),
                     mcTemplates)
    

# MC tools
mctools = ["MCTupleToolHierarchy", "MCTupleToolPrompt"]
