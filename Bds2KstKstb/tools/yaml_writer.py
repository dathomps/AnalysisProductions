import json

bk_file = open('./bk_locations.json', 'r')
bk_dicts = json.load(bk_file)
bk_file.close()

base_yaml_file = open('./base_info.yaml', 'r')
base_yaml = base_yaml_file.read()
base_yaml_file.close()

to_write = [base_yaml]
to_write += '\n\n'

options_path = 'mc_options'

#Lookup for the DaVinci version used for the stripping for each year
dv_versions = {
    '2011':'39r1p6',
    '2012':'39r1p6',
    '2015':'44r10p5',
    '2016':'44r10p5',
    '2017':'42r11p2',
    '2018':'44r4'
}

for event_type in bk_dicts:
    #ignore the signal MC and PhSp as not finished generating yet
    #if event_type != '13104006' and event_type != '13104094':
        decay_dict = bk_dicts[event_type]
        name = decay_dict['name']
        locations = decay_dict['locations']
        #Need file extension because we want to re-strip the DSTs (should be the same for each year)
        #So just pick any entry from dictionary, then any polarity then get the file extension from the end
        file_ext = locations[list(locations.keys())[0]][0].split('/')[-1].split('.')[-1]
        options_name = f'{options_path}/{name}_MC_options.py'
        for year in locations:
            locs = locations[year]
            #signal MC is a bit different
            if event_type == '13104006':
                options_name = 'sig_MC_options.py'
            else:
                dv_version = dv_versions[year]
            #figure out which is MagDown and which is MagUp
            mag_up, mag_down = '',''
            if '-MagDown-' in locs[0]:
                mag_down = locs[0]
                mag_up = locs[1]
            else:
                mag_down = locs[1]
                mag_up = locs[0]
            if file_ext == 'MDST' or event_type=='13104006':
                to_write += f'{event_type}_{name}_{year}_MagDown:\n    options:\n        - {options_name}\n    input:\n        bk_query: {mag_down}\n\n'
                to_write += f'{event_type}_{name}_{year}_MagUp:\n    options:\n        - {options_name}\n    input:\n        bk_query: {mag_up}\n\n\n'
            else:
                to_write += f'{event_type}_{name}_{year}_MagDown_STRIP:\n    application: DaVinci/v{dv_version}\n    options:\n        - strip_options.py\n    input:\n        bk_query: {mag_down}\n    output: BDS2KSTKSTB.STRIP.DST\n\n'
                to_write += f'{event_type}_{name}_{year}_MagDown:\n    options:\n        - {options_name}\n    input:\n        job_name: {event_type}_{name}_{year}_MagDown_STRIP\n\n'
                

                to_write += f'{event_type}_{name}_{year}_MagUp_STRIP:\n    application: DaVinci/v{dv_version}\n    options:\n        - strip_options.py\n    input:\n        bk_query: {mag_up}\n    output: BDS2KSTKSTB.STRIP.DST\n\n'
                to_write += f'{event_type}_{name}_{year}_MagUp:\n    options:\n        - {options_name}\n    input:\n        job_name: {event_type}_{name}_{year}_MagUp_STRIP\n\n'

yaml_out_file = open('auto_info.yaml', 'w')
for line in to_write:
    yaml_out_file.write(line)
yaml_out_file.close()
