#Many thanks to Aidan Wiederhold who I adapted this script from

import subprocess
import argparse

parser = argparse.ArgumentParser(description='Writes a file of the bookkeeping locations of each MC sample, ordered per year per sample.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--test', default=False, action='store_const', const=True, help='Test the find_locations mode by only running on one MC sample.')
parser.add_argument('--output', type=str, default='./bk_locations.json', help='Output json filename')
args = parser.parse_args()

import json
f_in = open('../mc_decay_descriptors.json', 'r')
mc_dds = json.load(f_in)
f_in.close()

#modes = {}
#
#for event_type,decay_dict in mc_dds.items():
#    modes[decay_dict['name']]=event_type

'''
# Bs signal
modes['Bs2Kst(892)Kstb(892)']=13104006
# Bs bkgs
    #Bs -> VV
modes['Bs2Kst(892)Kstb(1430)']=13104041
modes['Bs2Kst(1430)Kstb(1430)']=13104042
modes['Bs2Kstphi']=13104021
modes['Bs2Kstrho']=13104048
    #Bs PHSP
modes['Bs2KpiKpi']=13104094
#Bd signal
modes['Bd2Kst(892)Kstb(892)']=11104002
#Bd bkgs
    #Bd -> VV
modes['Bd2Kstphi']=11104020
modes['Bd2Kstrho']=11104041
    #Bd PHSP
modes['Bd2KpiKpi']=11104094
#Lb bkgs
modes['Lb2pKpipi']=15204011
modes['Lb2ppipipi']=15204010
'''

#Signal MC uses Sim09l for 11-16 and Sim09k for 17-18
diff_sim_sig_years = ['2011', '2012', '2015', '2016']

def bk_locations():
    counter=0

    bk_dict = {}

    for event_type in mc_dds:
        if (not args.test) or counter==0:
            decay_dict = mc_dds[event_type]
            name = decay_dict['name']
            decay_locations_dict = {'name': name, 'locations':{}}
            decay_locations = decay_locations_dict['locations']
            output = subprocess.check_output(['lb-dirac', 'dirac-bookkeeping-decays-path', event_type]).decode()
            output=output.split('\n')
            for line in output:
                if line!='':
                    bk_location = line.split(' ')[0]
                    bk_location_split = bk_location.split('/')
                    year = bk_location_split[2]
                    sim = bk_location_split[4]
                    trig = bk_location_split[5]
                    #Don't want to include `upgrade` MC and in 2012 samples there are sometimes
                    #two trigger keys, for now just take the same one the new MC uses which is
                    #Trig0x409f0045
                    #Also only want Sim09k samples and don't inlcude ephemera from on-going
                    #simulations
                    #But, 11-16 SigMC is now Sim09l so slightly different there
                    #There is a much better way to do this
                    if (year != 'Upgrade' and (sim == 'Sim09k' or sim=='Sim09l')):
                        if (bk_location_split[-1] != 'GAUSSHIST' and bk_location_split[-1] != 'SIM'):
                            if (event_type=='13104006'):
                                if (year in diff_sim_sig_years and sim=='Sim09l'):
                                    if year not in decay_locations: decay_locations[year] = []
                                    decay_locations[year].append(bk_location)
                                elif (year not in diff_sim_sig_years and sim=='Sim09k'):
                                    if year not in decay_locations: decay_locations[year] = []
                                    decay_locations[year].append(bk_location)
                            else:
                                if year != '2012':
                                    if year not in decay_locations: decay_locations[year] = []
                                    decay_locations[year].append(bk_location)
                                elif trig == 'Trig0x409f0045':
                                    if year not in decay_locations: decay_locations[year] = []
                                    decay_locations[year].append(bk_location)
            bk_dict[event_type] = decay_locations_dict
            counter+=1

    f_out = open(args.output, 'w')
    json.dump(bk_dict, f_out, indent=4)
    f_out.close()
    print(f'bk_locations() was successful, output can be found in {args.output}')

bk_locations()

