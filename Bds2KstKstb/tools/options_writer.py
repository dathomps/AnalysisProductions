import json

decays_file = open('../mc_decay_descriptors.json','r')
decays_dict = json.load(decays_file)
decays_file.close()

def write_options(event_type, desc, name, f_path):
    f_base = open('./base_options.py', 'r')
    lines = f_base.read().split('###IN')
    f_base.close()

    #Inbetween the ###IN clauses is just empty lines so can overwrite it
    lines[1] = f'#event type: {event_type}\nmc_decay_descriptor = \'{desc}\'\nname=\'{name}\'\n'

    f_out = open(f'{f_path}/{name}_MC_options.py', 'w')
    for line in lines:
        f_out.write(line)
    f_out.close()

f_path = '../mc_options'
#test
#decays_dict = {'13104041':decays_dict['13104041']}

for decay in decays_dict:
    #sigMC is done separately
    if decay != '13104006':
        write_options(decay, decays_dict[decay]['decay'], decays_dict[decay]['name'], f_path)



