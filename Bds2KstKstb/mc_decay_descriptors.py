#Need to double check some of these as sometimes the Kst is described as a Kst(1430) instead of Kst(892)

mc_decay_descriptors = {
    #Bs signal
    '13104006': {'decay': '${B}[[B_s0]cc => ${Kst}(K*(892)0 -> ${Kp}K+ ${pim}pi-) ${Kstb}(K*(892)~0 -> ${Km}K- ${pip}pi+)]CC', 'particles':['B', 'Kst', 'Kp', 'pim', 'Kstb', 'Km', 'pip'], 'name':'Bs2Kst_892_Kstb_892'},
    #Bs bkgs
        #Bs -> VV
        '13104041': {'decay': '${B}[[B_s0]cc => ${Kst}(K*(892)0 => ${Kp}K+ ${pim}pi-) ${Kstb}(K*_0(1430)~0 => ${Km}K- ${pip}pi+)]CC', 'particles':['B', 'Kst', 'Kp', 'pim', 'Kstb', 'Km', 'pip'], 'name':'Bs2Kst_892_Kstb_1430'},
        '13104042': {'decay': '${B}[[B_s0]cc => ${Kst}(K*_0(1430)0 => ${Kp}K+ ${pim}pi-) ${Kstb}(K*_0(1430)~0 => ${Km}K- ${pip}pi+)]CC', 'particles':['B', 'Kst', 'Kp', 'pim', 'Kstb', 'Km', 'pip'], 'name':'Bs2Kst_1430_Kstb_1430'},
        '13104021': {'decay': '${B}[[B_s0]cc => ${Kstb}(K*(892)~0 => ${Km1}K- ${pip}pi+) ${phi}(phi(1020) => ${Km2}K- ${Kp}K+)]CC', 'particles':['B', 'Kstb', 'Km1', 'pip', 'phi', 'Km2', 'Kp'], 'name':'Bs2Kstphi'},
        '13104048': {'decay': '${B}[[B_s0]cc => ${Kstb}(K*(892)~0 => ${Km}K- ${pip1}pi+) ${rho}(rho(770)0 => ${pim}pi- ${pip2}pi+)]CC', 'particles':['B', 'Kstb', 'Km', 'pip1', 'rho', 'pim', 'pip2'], 'name':'Bs2Kstrho'},
        #Bs PHSP
        '13104094': {'decay': '${B}[[B_s0]cc => ${Kp}K+ ${pim}pi- ${Km}K- ${pip}pi+]CC', 'particles':['B', 'Kp', 'pim', 'Km', 'pip'], 'name':'Bs2KpiKpi'},
    #Bd signal
    '11104002': {'decay': '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp}K+ ${pim}pi-) ${Kstb}(K*(892)~0 => ${Km}K- ${pip}pi+)]CC', 'particles':['B', 'Kst', 'Kp', 'pim', 'Kstb', 'Km', 'pip'], 'name':'Bd2Kst_892_Kstb_892'},
    #Bd bkgs
        #Bd => VV
        '11104020': {'decay': '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp1}K+ ${pim}pi-) ${phi}(phi(1020) => ${Km}K- ${Kp2}K+)]CC', 'particles':['B', 'Kst', 'Kp1', 'pim', 'phi', 'Km', 'Kp2'], 'name':'Bd2Kstphi'},
        '11104041': {'decay': '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp}K+ ${pim1}pi-) ${rho}(rho(770)0 => ${pim2}pi- ${pip}pi+)]CC', 'particles':['B', 'Kst', 'Kp', 'pim1', 'rho', 'pim2', 'pip'], 'name': 'Bd2Kstrho'},
        #Bd PHSP
        '11104094': {'decay': '${B}[[B0]cc => ${Kp}K+ ${pim}pi- ${Km}K- ${pip}pi+]CC', 'particles':['B', 'Kp', 'pim', 'Km', 'pip'], 'name':'Bd2KpiKpi'},
    #Lb bkgs
    '15204011': {'decay': '${Lb}[Lambda_b0 => ${p}p+ ${Km}K- ${pip}pi+ ${pim}pi-]CC', 'particles':['Lb', 'p', 'Km', 'pip', 'pim'], 'name':'Lb2pKpipi'},
    '15204010': {'decay': '${Lb}[Lambda_b0 => ${p}p+ ${pim1}pi- ${pip}pi+ ${pim2}pi-]CC', 'particles':['Lb', 'p', 'pim1', 'pip', 'pim2'], 'name':'Lb2ppipipi'}
}

#Just a dictionary so convert it to a JSON
import json
with open('mc_decay_descriptors.json','w') as f_out:
    json.dump(mc_decay_descriptors,f_out, indent=4)

