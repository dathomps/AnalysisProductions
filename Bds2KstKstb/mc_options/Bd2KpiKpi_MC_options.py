from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 11104094
mc_decay_descriptor = '${B}[[B0]cc => ${Kp}K+ ${pim}pi- ${Km}K- ${pip}pi+]CC'
name='Bd2KpiKpi'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
