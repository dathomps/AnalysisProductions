from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 15204011
mc_decay_descriptor = '${Lb}[Lambda_b0 => ${p}p+ ${Km}K- ${pip}pi+ ${pim}pi-]CC'
name='Lb2pKpipi'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
