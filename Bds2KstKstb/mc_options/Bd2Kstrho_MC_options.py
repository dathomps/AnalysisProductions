from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 11104041
mc_decay_descriptor = '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp}K+ ${pim1}pi-) ${rho}(rho(770)0 => ${pim2}pi- ${pip}pi+)]CC'
name='Bd2Kstrho'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
