from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 11104020
mc_decay_descriptor = '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp1}K+ ${pim}pi-) ${phi}(phi(1020) => ${Km}K- ${Kp2}K+)]CC'
name='Bd2Kstphi'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
