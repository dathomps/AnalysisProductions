from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 13104048
mc_decay_descriptor = '${B}[[B_s0]cc => ${Kstb}(K*(892)~0 => ${Km}K- ${pip1}pi+) ${rho}(rho(770)0 => ${pim}pi- ${pip2}pi+)]CC'
name='Bs2Kstrho'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
