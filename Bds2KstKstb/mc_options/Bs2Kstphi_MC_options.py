from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 13104021
mc_decay_descriptor = '${B}[[B_s0]cc => ${Kstb}(K*(892)~0 => ${Km1}K- ${pip}pi+) ${phi}(phi(1020) => ${Km2}K- ${Kp}K+)]CC'
name='Bs2Kstphi'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
