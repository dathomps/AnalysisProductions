from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 11104002
mc_decay_descriptor = '${B}[[B0]cc => ${Kst}(K*(892)0 => ${Kp}K+ ${pim}pi-) ${Kstb}(K*(892)~0 => ${Km}K- ${pip}pi+)]CC'
name='Bd2Kst_892_Kstb_892'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
