from Bds2KstKstb import bkg_MC_options

# Need to write into this bit:
#mc_decay_descriptor
#name (this is the short name like Bs2KstRho)

#event type: 13104042
mc_decay_descriptor = '${B}[[B_s0]cc => ${Kst}(K*_0(1430)0 => ${Kp}K+ ${pim}pi-) ${Kstb}(K*_0(1430)~0 => ${Km}K- ${pip}pi+)]CC'
name='Bs2Kst_1430_Kstb_1430'


bkg_MC_options.main_seq(mc_decay_descriptor, name)
