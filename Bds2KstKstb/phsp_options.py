from Gaudi.Configuration import importOptions, FileCatalog
from Configurables import DaVinci, GaudiSequencer, TupleToolGeometry, TrackSmearState, TrackScaleState, DecayTreeTuple, TupleToolTrigger, TupleToolTISTOS, CondDB, CheckPV, BTaggingTool, TupleToolDecay, TupleToolVtxIsoln, TupleToolPid, TupleToolRecoStats, LoKi__Hybrid__TupleTool, LoKi__Hybrid__EvtTupleTool, TupleToolTrackInfo, TupleToolTagging, TupleToolL0Data, TupleToolL0Calo, TupleToolDecayTreeFitter, TupleToolConeIsolation, TupleToolTrackIsolation, TupleToolMCBackgroundInfo, TupleToolMCTruth#, TupleToolApplyIsolation
from Configurables import MCDecayTreeTuple, MCTupleToolKinematic, MCTupleToolHierarchy, MCTupleToolPrimaries, MCTupleToolReconstructed, MCTupleToolPID, MCTupleToolP2VV 
from Configurables import EventTuple

from DecayTreeTuple.Configuration import *

from FlavourTagging.Tunings import applyTuning as applyFTTuning

#Much of this script adapted from Asier's (apereiro) script

with_ft = True

# Stream and stripping line we want to use
stream = 'Bs2KpPimKmPip.Strip'
line = 'Bs2K0stK0stNominalLine'
DaVinci().RootInTES = f'/Event/{stream}'
tes_loc = f'/Phys/{line}/Particles'


dtt = DecayTreeTuple('TupleKpiKpi')
dtt.Inputs = [tes_loc]
decay_template = '${B}[B_s0 -> ${Kst}(K*(892)0 -> ${Kp}K+ ${pim}pi-) ${Kstb}(K*(892)~0 -> ${Km}K- ${pip}pi+)]CC'
dtt.setDescriptorTemplate(decay_template)


l0_lines = [
    'L0HadronDecision', 'L0GlobalDecision'
]

hlt1_lines = [
    'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1TrackMVALooseDecision',
    'Hlt1TwoTrackMVALooseDecision', 'Hlt1TrackAllL0Decision', 'Hlt1L0AnyDecision',
    'Hlt1GlobalDecision'
]

hlt2_lines = [
    'Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision', 'Hlt2Topo4BodyDecision',
    'Hlt2Topo2BodyBBDTDecision', 'Hlt2Topo3BodyBBDTDecision', 'Hlt2Topo4BodyBBDTDecision'
]

trigger_lines = l0_lines + hlt1_lines + hlt2_lines

from Configurables import CheckPV

for particle in ['B', 'Kst', 'Kstb', 'Kp', 'pim', 'Km', 'pip']:
    dtt.addTool(TupleToolDecay, name = particle)

tool_list = [
    'TupleToolKinematic', 'TupleToolPropertime', 'TupleToolPrimaries',
    'TupleToolEventInfo', 'TupleToolANNPID', 'TupleToolTISTOS', 'TupleToolMCBackgroundInfo'
]
dtt.ToolList = tool_list

track_tool = dtt.addTupleTool('TupleToolTrackInfo')
track_tool.Verbose = True

pid_tool = dtt.addTupleTool('TupleToolPid')
pid_tool.Verbose = True

geom = dtt.addTupleTool('TupleToolGeometry')

reco_stats = dtt.addTupleTool('TupleToolRecoStats')
reco_stats.Verbose = True


trig_tool = dtt.addTupleTool('TupleToolTrigger')
trig_tool.Verbose = True
trig_tool.TriggerList = trigger_lines
trig_tool.OutputLevel = 6

#May need to remove, see if it works with MDST
vtx_isoln_tool = dtt.addTupleTool('TupleToolVtxIsoln')

### TISTOS
dtt.B.ToolList += ['TupleToolTISTOS']
dtt.B.addTool(TupleToolTISTOS, name = 'TupleToolTISTOS')
dtt.B.TupleToolTISTOS.Verbose = True
dtt.B.TupleToolTISTOS.TriggerList = trigger_lines

from Configurables import MCMatchObjP2MCRelator
mc_truth = dtt.addTupleTool('TupleToolMCTruth')
mc_truth.addTool(MCMatchObjP2MCRelator)
mc_truth.addTool(MCTupleToolP2VV)
mc_truth.ToolList += ['MCTupleToolHierarchy','MCTupleToolKinematic','MCTupleToolPID']

LoKi_B = LoKi__Hybrid__TupleTool('LoKi_B')
LoKi_B.Variables = {
    'ETA'                  : 'ETA',
    'DOCA'                 : 'DOCA(1,2)',
    'Y'                    : 'Y',
    'LV01'                 : 'LV01',
    'LV02'                 : 'LV02',
    'LOKI_FDCHI2'          : 'BPVVDCHI2',
    'LOKI_FDS'             : 'BPVDLS',
    'LOKI_DIRA'            : 'BPVDIRA',
    'LOKI_DTF_CTAU'        : 'DTF_CTAU(0, True)',
    'LOKI_DTF_CTAUS'       : 'DTF_CTAUSIGNIFICANCE(0, True)',
    'LOKI_DTF_CHI2NDOF'    : 'DTF_CHI2NDOF(True)',
    'LOKI_DTF_CTAUERR'     : 'DTF_CTAUERR(0, True)',
    'LOKI_DTF_VCHI2NDOF'   : 'DTF_FUN(VFASPF(VCHI2/VDOF), True)'
}
dtt.B.ToolList+=['LoKi::Hybrid::TupleTool/LoKi_B']
dtt.B.addTool(LoKi_B)

# refit with B0 mass constraint and PV
dtt.B.ToolList += ['TupleToolDecayTreeFitter/ConstB0andPV']
dtt.B.addTool(TupleToolDecayTreeFitter('ConstB0andPV'))
dtt.B.ConstB0andPV.Verbose = True
dtt.B.ConstB0andPV.UpdateDaughters = True
dtt.B.ConstB0andPV.constrainToOriginVertex = True
dtt.B.ConstB0andPV.Substitutions = {dtt.Branches['B']: 'B0'}
#dtt.B.ConstB0andPV.Substitutions = {'B_s0 -> (K*_0(1430)0 -> K+ pi-) (K*_0(1430)~0 -> K- pi+)': 'B0',
#    'B_s~0 -> (K*_0(1430)~0 -> K- pi+) (K*_0(1430)0 -> K+ pi-)': 'B~0'
#}
dtt.B.ConstB0andPV.daughtersToConstrain = ['B0']

# refit with Bs mass constraint and PV
dtt.B.ToolList += ['TupleToolDecayTreeFitter/ConstBsandPV']
dtt.B.addTool(TupleToolDecayTreeFitter('ConstBsandPV'))
dtt.B.ConstBsandPV.Verbose = True
dtt.B.ConstBsandPV.UpdateDaughters = True
dtt.B.ConstBsandPV.constrainToOriginVertex = True
dtt.B.ConstBsandPV.daughtersToConstrain = ['B_s0']

# refit with only PV
dtt.B.ToolList += ['TupleToolDecayTreeFitter/ConstOnlyPV']
dtt.B.addTool(TupleToolDecayTreeFitter('ConstOnlyPV'))
dtt.B.ConstOnlyPV.Verbose = True
dtt.B.ConstOnlyPV.UpdateDaughters = True
dtt.B.ConstOnlyPV.constrainToOriginVertex = True

LoKi_EvtTuple = LoKi__Hybrid__EvtTupleTool('LoKi_EvtTuple')
LoKi_EvtTuple.VOID_Variables = {
    'LoKi_nPVs'              : 'CONTAINS(\'Rec/Vertex/Primary\')',
    'LoKi_nSpdMult'          : 'CONTAINS(\'Raw/Spd/Digits\')',
    'LoKi_nVeloClusters'     : 'CONTAINS(\'Raw/Velo/Clusters\')',
    'LoKi_nVeloLiteClusters' : 'CONTAINS(\'Raw/Velo/LiteClusters\')',
    'LoKi_nITClusters'       : 'CONTAINS(\'Raw/IT/Clusters\')',
    'LoKi_nTTClusters'       : 'CONTAINS(\'Raw/TT/Clusters\')',
    'LoKi_nOThits'           : 'CONTAINS(\'Raw/OT/Times\')'
}

dtt.ToolList += ['LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple']
dtt.addTool(LoKi_EvtTuple)

year = int(DaVinci().DataType)

#No FT with mdst
'''
if with_ft:
    btag = dtt.addTupleTool('TupleToolTagging')
    btag.Verbose = True
    btag.AddMVAFeatureInfo = True

    btagtool = btag.addTool(BTaggingTool, name = 'MyBTaggingTool')


    if year<=2012: applyFTTuning(btagtool, tuning_version = 'Summer2017Optimisation_Run1')
    else: applyFTTuning(btagtool, tuning_version = 'Summer2017Optimisation_v4_Run2')
    btag.TaggingToolName = btagtool.getFullName()
'''
#DO NOT USE THIS ON MC! Sets the CondDB tags to Sim10 Smog (data is fine)!
#CondDB(LatestGlobalTagByDataType = str(year))


smearer = TrackSmearState('TrackSmearState')
#scaler = TrackScaleState('StateScale')

#No isolation with mdst
'''
# From Julian's (@jugarcia) script
# Neutral isolation variables
cone_tool = dtt.B.addTupleTool('TupleToolConeIsolation')
#cone_tool.MinConeSize = 0.1
#cone_tool.MaxConeSize = 0.6
cone_tool.MinConeSize = 0.2
cone_tool.MaxConeSize = 0.2
cone_tool.SizeStep = 0.1
cone_tool.FillAsymmetry = True
cone_tool.FillDeltas = True
cone_tool.FillComponents = True
#Pi0 info does not seem to work so turn it off
cone_tool.FillPi0Info = False
cone_tool.MaxhPi0Mass = 3000.
cone_tool.FillMergedPi0Info = False
cone_tool.FillCharged = True
cone_tool.FillNeutral = True
cone_tool.MaxPtParticlesLocation = '/Event/Phys/StdAllNoPIDsPions/Particles'
'''
# Charged isolation variables (from Greg's tool)
#from Configurables import TupleToolApplyIsolation
#dtt.B.addTool(TupleToolApplyIsolation, name="TupleToolApplyIsolation")
#dtt.B.TupleToolApplyIsolation.WeightsFile="weights.xml"
#dtt.B.ToolList+=["TupleToolApplyIsolation/TupleToolApplyIsolation"]

#No isolation for mdst
'''
# Charged isolation variables (cone variables)
from Configurables import TupleToolTrackIsolation
Cone = dtt.B.addTupleTool("TupleToolTrackIsolation/Cone")
Cone.FillAsymmetry = True
Cone.MinConeAngle = 1.5
Cone.MaxConeAngle = 1.9
Cone.StepSize = 0.2
Cone.Verbose = True
'''

mc_dtt = MCDecayTreeTuple('TruthTupleKpiKpi')
mc_decay_descriptor = '${B}[B_s0 => ${Kp}K+ ${pim}pi- ${Km}K- ${pip}pi+]CC'
mc_dtt.setDescriptorTemplate(mc_decay_descriptor)
mc_dtt.ToolList += [
    'MCTupleToolKinematic',
    'TupleToolEventInfo',
    'MCTupleToolHierarchy',
    'MCTupleToolPrimaries',
    #'MCTupleToolReconstructed',
    'MCTupleToolPID'
]

event_tuple = EventTuple('EventTuple')
event_tuple.ToolList = ['TupleToolEventInfo']

# Configure DaVinci

DaVinci().UserAlgorithms += [CheckPV(), smearer, dtt, mc_dtt, event_tuple]
#DaVinci().EvtMax = 500
'''
DaVinci().InputType = 'mDST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2018'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = 10000
DaVinci().CondDBtag = 'sim-20190430-vc-md100'
DaVinci().DDDBtag = 'dddb-20170721-3'
DaVinci().Turbo = True

from GaudiConf import IOHelper
IOHelper().inputFiles([
    #'root://x509up_u15960@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00146430/0000/00146430_00006541_1.bhadroncompleteevent.dst'
    'root://x509up_u15960@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2018/ALLSTREAMS.MDST/00145741/0000/00145741_00000041_7.AllStreams.mdst'
], clear=True)
'''

# Use input data from the bookkeeping query with XML catalog
#importOptions(
#    "MC_2016_27163002_"
#    "Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09c_"
#    "Trig0x6138160F_Reco16_Turbo03_"
#    "Stripping28r1NoPrescalingFlagged_ALLSTREAMS.DST.py"
#    )
#FileCatalog().Catalogs = [
#    "xmlcatalog_file:MC_2016_27163002_"
#    "Beam6500GeV2016MagDownNu1.625nsPythia8_Sim09c_"
#    "Trig0x6138160F_Reco16_Turbo03_"
#    "Stripping28r1NoPrescalingFlagged_ALLSTREAMS.DST.xml"
#    ]

