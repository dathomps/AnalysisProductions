#Would like to res-strip our background samples to remove the PID cuts used in the default stripping line for
#Bs2K0stK0stNominalLine

stripping_lookup = {
    '2011': 'stripping21r1p2',
    '2012': 'stripping21r0p2',
    '2015': 'stripping24r2',
    '2016': 'stripping28r2',
    '2017': 'stripping29r2p2',
    '2018': 'stripping34'
}

#Some years need an extra bit for calo reprocessing
calo_years = ['2011', '2012', '2015', '2016']


# copy the algorithm form the stripping line, without PID cuts (avoid bias)
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from Gaudi.Configuration import *

from Configurables import EventNodeKiller, DaVinci, DecayTreeTuple
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *
from DSTWriters.microdstelements import *

def makeKst_02Kpi(
        name, KaonPT, KaonIPCHI2,PionPT,
        PionIPCHI2, KstarPT, KstarAPT,
        KstarVCHI2, KstarMassWin_ul, MaxGHOSTPROB):


    _stdKaons = DataOnDemand(Location="Phys/StdNoPIDsKaons/Particles")
    _stdPions = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")


    _Kstar_02Kpi = CombineParticles()

    _Kstar_02Kpi.DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
    _Kstar_02Kpi.DaughtersCuts = {"K+" : "(PT > %(KaonPT)s *MeV) & (MIPCHI2DV(PRIMARY)> %(KaonIPCHI2)s) & (TRGHOSTPROB < %(MaxGHOSTPROB)s)"% locals()
                                ,"pi-" : "(PT > %(PionPT)s *MeV) & (MIPCHI2DV(PRIMARY)> %(PionIPCHI2)s) & (TRGHOSTPROB < %(MaxGHOSTPROB)s)"% locals()}
                                
    _Kstar_02Kpi.CombinationCut = "(AM < %(KstarMassWin_ul)s *MeV) & (APT > %(KstarAPT)s *MeV)"% locals()
    _Kstar_02Kpi.MotherCut = "(VFASPF(VCHI2/VDOF)< %(KstarVCHI2)s) & (PT > %(KstarPT)s *MeV)"% locals()



    return Selection(name, Algorithm = _Kstar_02Kpi, RequiredSelections = [_stdKaons,_stdPions])

def makeBs2Kst_0Kst_0(
        name, Kst_0sel, BMassWin, BVCHI2,
        BDOCA, BIPCHI2, BFDistanceCHI2,
        SumPT,BDIRA):

    _motherCuts = " (VFASPF(VCHI2/VDOF) < %(BVCHI2)s) & (MIPCHI2DV(PRIMARY)< %(BIPCHI2)s) & (BPVVDCHI2 > %(BFDistanceCHI2)s) & (BPVDIRA > %(BDIRA)s)"% locals()
    _combinationCut = "(ADAMASS('B_s0') < %(BMassWin)s *MeV) & (AMAXDOCA('',False)< %(BDOCA)s *mm) "\
                        "& ( (AMINCHILD(PT,ID=='K+') + AMINCHILD(PT,ID=='K-') + AMINCHILD(PT,ID=='pi-') + AMINCHILD(PT,ID=='pi+'))> %(SumPT)s *MeV)" % locals() 

    _Bs = CombineParticles()
    _Bs.DecayDescriptor = "B_s0 -> K*(892)0 K*(892)~0"
    _Bs.CombinationCut = _combinationCut
    _Bs.MotherCut = _motherCuts

    _Bs.ReFitPVs = True

    return Selection(name, Algorithm = _Bs, RequiredSelections = [Kst_0sel])

# select K*0 -> K+ pi-
selKst_02Kpi = makeKst_02Kpi(
                name = 'Kst_02Kpi_without_PID_ForBs2K0stK0st',
                KaonPT = 500.0,
                KaonIPCHI2 = 9.0,
                PionPT = 500.0,
                PionIPCHI2 = 9.0,
                KstarPT = 900.0,
                KstarAPT = 800.0,
                KstarVCHI2 = 9.0,
                KstarMassWin_ul = 1600.0,
                MaxGHOSTPROB = 0.8)

# select Bs -> K*0 K*0~
selBs2Kst_0Kst_0 = makeBs2Kst_0Kst_0(
                    name = 'Bs2K0stK0st',
                    Kst_0sel = selKst_02Kpi,
                    BMassWin = 500.0,
                    BVCHI2 = 15.0,
                    BDOCA = 0.3,
                    BIPCHI2 = 25.0,
                    BFDistanceCHI2 = 81.0,
                    SumPT = 5000.0,
                    BDIRA = 0.99)

def configure_restripping():
    year = DaVinci().DataType
    stripping = stripping_lookup[year]

    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

    #
    # Disable the cache in Tr/TrackExtrapolators
    #
    from Configurables import TrackStateProvider
    TrackStateProvider().CacheStatesOnDemand = False

    # Build the streams and stripping object

    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive
    from CommonParticlesArchive import CommonParticlesArchiveConf

    #streams = buildStreams(stripping=strippingConfiguration(stripping), archive=strippingArchive(stripping))

    CommonParticlesArchiveConf().redirect(stripping)

    MyStreams = StrippingStream("BDS2KSTKSTB.STRIP")

    line = StrippingLine(
        'Bs2K0stK0stNominalLine',
        prescale = 1,
        postscale = 1,
        algos = [ selBs2Kst_0Kst_0 ],
        EnableFlavourTagging = True,
        MDSTFlag = False)

    MyStreams.appendLines( [line] )

    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(
            Streams = [MyStreams],
            MaxCandidates = 2000,
            MaxCombinations = 10000000,
              AcceptBadEvents = False,
              BadEventSelection = filterBadEvents,
            #TESPrefix = 'STRIP'
        )

    MyStreams.sequence().IgnoreFilterPassed = False

    #Setting to True causes a lot of warnings on Run 1 data related to
    #'Unknown Velo Cluster'
    enablePacking = year not in ['2011', '2012']

    from DSTWriters.Configuration import (SelDSTWriter,
                                          stripDSTStreamConf,
                                          stripDSTElements)

    SelDSTWriterElements = {
        'default': stripDSTElements(
                                pack=enablePacking
                                )
        }

    SelDSTWriterConf = {
        'default': stripDSTStreamConf(
                                pack=enablePacking,
                                selectiveRawEvent=True,
                                fileExtension='.{}'.format(DaVinci().InputType)
                                )
        }
    if year in calo_years:
        #Items that get lost when running the CALO+PROTO ReProcessing in DV
        caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

        # Make sure they are present on full DST streams
        SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

    dstWriter = SelDSTWriter( "MyDSTWriter",
                              StreamConf = SelDSTWriterConf,
                              MicroDSTElements = SelDSTWriterElements,
                              OutputFileSuffix ='',
                              SelectionSequences = sc.activeStreams()
                              )   
    DaVinci().UserAlgorithms = [event_node_killer, sc.sequence(), dstWriter.sequence()]
