### B(s)0 to K*(892)0 K*(892)~0 Analysis

# Analysis Production Version 6 - Add phasespace 17/18 tuples
This is the 6th run of this analysis production. The options are identical to version 5 as this is just adding the 17/18 phasespace (13104094) MC which had not finished generating when version 5 ran but now has.


## Analysis Summary
This analysis will first look to perform an amplitude analysis of the decay $`B_{(s)}^0 \to K^*(892)^0 \overline{K}^*(892)^0`$ with $`K^*(892)^0 \to K^+ \pi^-`$ and $`\overline{K}^*(892)^0 \to K^- \pi^+`$ followed later by a time-dependent angular analysis. Both these analyses have been completed for Run 1 data so this analysis will extend that to include Run 1 and Run 2.

## Options files
The data options files are archived in data/. There is a single options file for the signal MC `signal_MC_options.py` which is used for 17-18 with both polarities.

The background MC uses a single options file (with a function) and if statements for MDST vs DST. There is then an options file for each of the individual background MCs which then call this main function.

## Structure
The decays are contained in `mc_decay_descriptors.py` (event type as the key, decay descriptor, name and particles as the fields) which are written out to `mc_decay_descriptors.json` file using `python mc_decay_descriptors.py`.

`tools/bk_locations.py` then uses `mc_decay_descriptors.json` to write out the bookkeeping locations of the MC to `tools/bk_locations.json`, run with `python bk_locations.py` (expects to be run from within `tools/`).

Still within the `tools/` directory, the options files are then written using `python options_writer.py` which writes them to `mc_options/` using the stub in `base_options/py`. Similarly, the `yaml` file can be generated by using `python yaml_writer.py`. Note, the `info.yaml` produced by this in `tools/` must be moved, not copied, into the main directory. Having more than one file called `info.yaml` (even in subdirectories) breaks the AProd pipeline.

### Re-stripping
The background samples are re-stripped using `strip.py` to remove the PID cuts that are present in the nominal stripping line, Bs2K0stK0stNominalLine. This can only be done for the `DST` samples and so the `MDST` samples are not re-stripped.

## Data
The events are taken from the BHadronCompleteEvent stream, with the stripping line Bs2K0stK0stNominalLine. Note that this stripping line calls the K\* as K\*(1430) rather than K\*(892) but no cuts are made to enforce this, it is simply the name. For each year, the stripping versions are as follows:
* 2018 - Stripping 34
* 2017 - Stripping 29r2p2
* 2016 - Stripping 28r2
* 2015 - Stripping 24r2
* 2012 - Stripping 21r0p2
* 2011 - Stripping 21r12

## MC files
### Signal MC:
* 13104006 $`B_{s}^0 \to K^*(892)^0 \overline{K}^*(892)^0`$ custom-filtered full DST 11-18.
### Background MC:
* 13104094 $`B_{s}^0 \to K^+ \pi^- K^- \pi^+`$  phasespace MDST 11-18
* 13104041 $`B_{s}^0 \to K^*(892)^0 \overline{K}^*(1430)^0`$ full DST 11-18
* 13104042 $`B_{s}^0 \to K^*(1430)^0 \overline{K}^*(1430)^0`$ full DST 11-18
* 13104021 $`B_{s}^0 \to K^*(892)^0 \phi`$ full DST 11-18
* 13104048 $`B_{s}^0 \to K^*(892)^0 \rho^0`$ full DST 11-18

* 11104002 $`B^0 \to K^*(892)^0 \overline{K}^*(892)^0`$ full DST 11-18
* 11104020 $`B^0 \to K^*(892)^0 \phi`$ MDST 11-18
* 11104041 $`B^0 \to K^*(892)^0 \rho^0`$ MDST 11-18
* 11104094 $`B^0 \to K^+ \pi^- K^- \pi^+`$ full DST 11-18

* 15204010 $`\Lambda_b^0 \to p \pi^- \pi^+ \pi^-`$ MDST 11-18
* 15204011 $`\Lambda_b^0 \to p K^- \pi^+ \pi^-`$ MDST 11-18


