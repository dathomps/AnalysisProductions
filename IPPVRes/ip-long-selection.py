from IPResolutions.ntupling import ip_conf

ipconf = ip_conf()
ipconf.TrackSelectorKwargs = dict(TrackTypes = ["Long"], MinPtCut = 300)
