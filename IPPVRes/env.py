'''Add IPPVRes directory to PYTHONPATH.'''

import os, sys
sys.path.append(os.path.join(os.environ['ANALYSIS_PRODUCTIONS_BASE'], 'IPPVRes'))
