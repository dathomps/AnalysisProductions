'''Functions to configure IP ntupling.'''

from Configurables import GaudiSequencer, PVOfflineTool, VertexCompare, \
    SplitTracks, PatPV3D, LSAdaptPV3DFitter, TrackSelector, \
    TrackContainerCopy, PrimaryVertexChecker, PVResolution, TESCheck
from math import pi
from PatPV import PVConf


def pv_tracks(DataType, InputTracks = ''):
    '''Get the default input tracks location for PV fitting.'''
    if InputTracks:
        return InputTracks
    if int(DataType) < 2015:
        return 'Rec/Track/Best'
    return 'Rec/Track/FittedHLT1VeloTracks'


def configure_pv_tool(ipMoniAlgo, DataType, InputTracks = '') :
    '''Configure the PV fitter for Run2, which is non-default. This is copied from RecSysConf. 
    Ideally, they'd have put the PV fitter config into a function there, but this works anyway.'''

    InputTracks = pv_tracks(DataType, InputTracks)

    ipMoniAlgo.addTool(PVOfflineTool, 'PVOfflineTool')
    pvTool = ipMoniAlgo.PVOfflineTool
    pvTool.InputTracks = [ InputTracks ]

    # Run 1 config from 
    # https://gitlab.cern.ch/lhcb/Rec/-/blob/v14r1p1/Rec/RecConf/python/RecConf/Configuration.py#L82
    if int(DataType) < 2015:
        PVConf.StandardPV().configureTool(pvTool)
        # Run 1 version of PVOfflineTool didn't have a different R cut for high mult PVs.
        # https://gitlab.cern.ch/lhcb/Rec/-/blob/v14r1p1/Tr/PatPV/src/PVOfflineTool.cpp#L274
        if 'BeamSpotRMultiplicityTreshold' in pvTool.properties():
            pvTool.BeamSpotRMultiplicityTreshold = 100000
    # 2015 config from
    # https://gitlab.cern.ch/lhcb/Rec/-/blob/v18r1/Rec/RecConf/python/RecConf/Configuration.py#L123
    elif int(DataType) == 2015:
        pvTool.addTool(LSAdaptPV3DFitter, "LSAdaptPV3DFitter")
        pvTool.PVFitterName = "LSAdaptPV3DFitter"
        pvTool.LSAdaptPV3DFitter.UseFittedTracks = True
        pvTool.LSAdaptPV3DFitter.AddMultipleScattering = False
        pvTool.LSAdaptPV3DFitter.TrackErrorScaleFactor = 1.0
        pvTool.LSAdaptPV3DFitter.MinTracks = 4
        pvTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0
        pvTool.UseBeamSpotRCut = True
        pvTool.BeamSpotRCut = 0.2
        # 2015 version of PVOfflineTool didn't have a different R cut for high mult PVs.
        # https://gitlab.cern.ch/lhcb/Rec/-/blob/v18r1/Tr/PatPV/src/PVOfflineTool.cpp#L274
        if 'BeamSpotRMultiplicityTreshold' in pvTool.properties():
            pvTool.BeamSpotRMultiplicityTreshold = 100000
    # 2016-2018 Config copied from 
    # https://gitlab.cern.ch/lhcb/Rec/-/blob/run2-patches/Rec/RecConf/python/RecConf/Configuration.py#L171
    else:
        pvTool.addTool(LSAdaptPV3DFitter, "LSAdaptPV3DFitter")
        pvTool.PVFitterName = "LSAdaptPV3DFitter"
        pvTool.LSAdaptPV3DFitter.UseFittedTracks = True
        pvTool.LSAdaptPV3DFitter.AddMultipleScattering = False
        pvTool.LSAdaptPV3DFitter.TrackErrorScaleFactor = 1.0
        pvTool.LSAdaptPV3DFitter.MinTracks = 4
        pvTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0
        pvTool.UseBeamSpotRCut = True
        pvTool.BeamSpotRCut = 0.2
        pvTool.BeamSpotRHighMultiplicityCut = 0.4
        pvTool.BeamSpotRMultiplicityTreshold = 10

def vertex_compare_seq(name, tracks1, tracks2, DataType):
    '''Make the sequence for VertexCompare with the given split track locations.'''
    
    pvAlg1 = PatPV3D("PatPV3D_" + name + "1")
    pvAlg2 = PatPV3D("PatPV3D_" + name + "2")
    configure_pv_tool(pvAlg1, DataType, tracks1)
    configure_pv_tool(pvAlg2, DataType, tracks2)

    pvAlg1.OutputVerticesName = "Rec/Vertex/Primary" + name + "1"
    pvAlg2.OutputVerticesName = "Rec/Vertex/Primary" + name + "2"

    vertexCompare = VertexCompare("VertexCompare" + name)
    vertexCompare.inputVerticesName1 = pvAlg1.OutputVerticesName
    vertexCompare.inputVerticesName2 = pvAlg2.OutputVerticesName
    vertexCompare.produceNtuple = True
    vertexCompare.produceHistogram = True

    seq = GaudiSequencer('VertexCompare' + name + 'Seq', IgnoreFilterPassed = True,
                         ModeOR = True, ShortCircuit = False,
                         Members = [pvAlg1, pvAlg2, vertexCompare])
    return seq
    

def vertex_compare_random_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with random track splitting.'''
    
    split  = SplitTracks("SplitRandom")
    split.InputTracks = InputTracks
    split.OutputTracks1 = "Rec/Track/FittedSampleA"
    split.OutputTracks2 = "Rec/Track/FittedSampleB"
    # Force the split tracks containers to be the same size.
    split.EqualizeSplitContainerSizes = True

    seq = vertex_compare_seq('', split.OutputTracks1, split.OutputTracks2, DataType)
    seq.Members.insert(0, split)

    return seq

def track_selector_phi_range(name, InputTracks, phimin, phimax):
    '''Make a TrackContainerCopy instance selecting tracks in the given phi range.'''
    
    copy = TrackContainerCopy(name, inputLocations = [InputTracks], outputLocation = InputTracks + name)
    selector = TrackSelector(name + 'Selector', MinPhiCut = phimin, MaxPhiCut = phimax)
    copy.Selector = selector
    return copy
    

def vertex_compare_LR_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with left-right track splitting.'''
    
    copyR = track_selector_phi_range('CopyR', InputTracks, -pi/2, pi/2)
    copyL1 = track_selector_phi_range('CopyL1', InputTracks, -pi, -pi/2)
    copyL2 = track_selector_phi_range('CopyL2', InputTracks, pi/2, pi)
    copyL = TrackContainerCopy('CopyL', inputLocations = [copyL1.outputLocation, copyL2.outputLocation],
                               outputLocation = InputTracks + 'CopyL')
    seq = vertex_compare_seq('LR', copyR.outputLocation, copyL.outputLocation, DataType)
    seq.Members = [copyR, copyL1, copyL2, copyL] + seq.Members
    return seq

def vertex_compare_UD_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with up-down track splitting.'''
    
    copyU = track_selector_phi_range('CopyU', InputTracks, 0., pi)
    copyD = track_selector_phi_range('CopyD', InputTracks, -pi, 0.)
    seq = vertex_compare_seq('UD', copyU.outputLocation, copyD.outputLocation, DataType)
    seq.Members = [copyU, copyD] + seq.Members
    return seq

def vertex_compare_FB_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with forward-backward track splitting.'''
    
    copyF = TrackContainerCopy('CopyF', inputLocations = [InputTracks], outputLocation = InputTracks + 'CopyF',
                               Selector = TrackSelector('CopyFSelector', TrackTypes = ['Long', 'Velo', 'Upstream']))
    copyB = TrackContainerCopy('CopyB', inputLocations = [InputTracks], outputLocation = InputTracks + 'CopyB',
                               Selector = TrackSelector('CopyBSelector', TrackTypes = ['Backward']))
    seq = vertex_compare_seq('FB', copyF.outputLocation, copyB.outputLocation, DataType)
    seq.Members = [copyF, copyB] + seq.Members
    return seq

def vertex_compare_sanity_check_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with the default PVs compared to rebuilt PVs.
    This provides a sanity check in DaVinci that the original PVs are recovered correctly.'''
    #pvAlg = PatPV3D('PatPV3D', OutputVerticesName = 'Rec/Vertex/PrimarySanity')
    pvAlg = PatPV3D('PatPV3D_sanity', OutputVerticesName = 'Rec/Vertex/PrimarySanity')
    configure_pv_tool(pvAlg, DataType, InputTracks)
    
    vertexCompare = VertexCompare('VertexCompareSanity',
                                  inputVerticesName1 = 'Rec/Vertex/Primary',
                                  inputVerticesName2 = pvAlg.OutputVerticesName,
                                  produceNtuple = True, produceHistogram = True)
    return GaudiSequencer('VertexCompareSanitySeq', IgnoreFilterPassed = True, ModeOR = True, ShortCircuit = False,
                          Members = [pvAlg, vertexCompare])

def pv_conf():
    '''Get the PVResolutionsConf instance.'''
    from IPResolutions.Configuration import PVResolutionsConf
    return PVResolutionsConf('PVResolutionConf')

def ip_conf():
    '''Get the IPResolutionsConf instance.'''
    from IPResolutions.Configuration import IPResolutionsConf
    return IPResolutionsConf('VeloIPResolutionMonitorConf')
    
def make_confs(ipres = True, pvres = True, pvtracks = ''):
    '''Make the configurables for IP and PV resolution'''
    confs = []
    # PV res goes first as IP res might filter Rec/Track/Best (only relevant for Run 1).
    if pvres:
        pvconf = pv_conf()
        pvconf.InputTracks = pvtracks
        confs.append(pvconf)
    if ipres:
        ipconf = ip_conf()
        ipconf.PVTracksLocation = pvtracks
        confs.append(ipconf)
    return confs

def configure_dv(ipres = True, pvres = True, pvtracks = ''):
    '''Configure DaVinci for ntupling.'''
    from Configurables import DaVinci
    from Gaudi.Configuration import appendPostConfigAction
    
    dv = DaVinci()
    dv.TupleFile = 'IPTuple.root'

    confs = make_confs(ipres, pvres, pvtracks)
    for conf in confs:
        dv.UserAlgorithms.append(conf.sequence())
    if pvres:
        appendPostConfigAction(lambda : dv_post_config(confs[0]))
    return confs

def dv_post_config(pvconf):
    '''Post config actions for DaVinci.'''
    from Configurables import DaVinci, EventNodeKiller, TrackAssociator
    from Gaudi.Configuration import allConfigurables
    
    if int(DaVinci().DataType) < 2015:
        return

    # Try using RecSysConf to configure PV alg (make sure PV alg for sanity is
    # called PatPV3D). Still doesn't fix things ...
    # from Configurables import RecSysConf
    # RecSysConf().DataType = '2015'
    # RecSysConf().__apply_configuration__()
    # pvAlg = PatPV3D('PatPV3D', OutputVerticesName = 'Rec/Vertex/PrimarySanity')

    # Need to re-reconstruct all VELO tracks in Run 2, since only tracks
    # previously used in PVs are kept
    seq = GaudiSequencer('RerunVELOSeq', IgnoreFilterPassed = True)
    # Make sure VELO raw banks can be found
    seq.Members.append(TESCheck('CheckVeloRaw', Inputs = ['Velo/RawEvent'],
                                Stop = True))
    # Get the VELO reco sequence, initialise TrackSys if necessary
    reco = GaudiSequencer('RecoVELOSeq')
    if 'VeloOnlyInitAlg' not in allConfigurables:
        from Configurables import TrackSys
        TrackSys().DataType = DaVinci().DataType
        TrackSys().__apply_configuration__()
    init = allConfigurables['VeloOnlyInitAlg']
    # Redirect output for fitted VELO tracks
    init.TrackLocation = 'Rec/Track/Velo'
    reco.Members.append(init)
    seq.Members += [reco]
    # Make MC links for recreated VELO tracks (requires LDST)
    if DaVinci().getProp('Simulation') and DaVinci().getProp('InputType') == 'LDST':
        linkseq = GaudiSequencer('AssocVeloSeq')
        # Check that the cluster -> MCParticle links exist
        check = TESCheck('CheckVeloLinks', Inputs = ['Link/Raw/Velo/Clusters'],
                         Stop = False)
        assoc = TrackAssociator('AssocVELO',
                                TracksInContainer = init.TrackLocation)
        linkseq.Members = [check, assoc]
        seq.Members.append(linkseq)
    # Redirect PV resolutions algo to the rebuilt tracks.
    pvconf.InputTracks = init.TrackLocation
    pvconf.__apply_configuration__()
    GaudiSequencer('DaVinciUserSequence').Members.insert(0, seq)

def kill_sequence(seqName) :
    '''Remove all members from a sequence.'''
    if isinstance(seqName, list) :
        for name in seqName :
            kill_sequence(name)
    else :
        GaudiSequencer(seqName).Members = []

def configure_brunel(ipres = True, pvres = True, pvtracks = '', veloonly = False):
    '''Configure Brunel for ntupling.'''
    from Configurables import Brunel, RecSysConf, RecMoniConf, TrackSys
    from Gaudi.Configuration import HistogramPersistencySvc, NTupleSvc, appendPostConfigAction

    # Instantiate the application.
    app = Brunel()
    # Don't save a DST
    app.OutputType = 'NONE'

    # Save a histo and ntuple file.
    HistogramPersistencySvc().OutputFile = "BrunelHisto.root"
    NTupleSvc().Output=["FILE1 DATAFILE='IPTuple.root' TYP='ROOT' OPT='NEW'"]

    try:
        RecMoniConf().Detectors = ['Velo']
    except AttributeError:
        pass
    # Set these to run only the VELO tracking (for Run 2 PV resolution)
    if veloonly:
        RecSysConf().Detectors = ['Velo', 'Tr']
        TrackSys().TrackPatRecAlgorithms = ["Velo","FastVelo"]

    confs = make_confs(ipres, pvres, pvtracks)
    appendPostConfigAction(lambda : brunel_post_config(confs))
    return confs

def brunel_post_config(confs):
    '''Post config actions for Brunel: kill unneeded algos and add the IP/PV res. sequences.'''

    from Configurables import Brunel
    mc = Brunel().getProp('Simulation')
    datatype = Brunel().DataType

    if int(datatype) >= 2015:
        # Don't clean tracks not included in PVs.
        pvseq = GaudiSequencer('RecoVertexSeq')
        if pvseq.Members[-1].name() == 'PVVeloTracksCleaner':
            pvseq.Members.pop()

    # Kill everything in Brunel that we don't need.
    kill_sequence(["MoniVELOSeq",
                   "MoniCALOSeq",
                   "RecoRICHSeq",
                   "RecoRICHFUTURESeq",
                   "MoniRICHSeq",
                   "MoniRICHFUTURESeq",
                   "DecodeTriggerSeq",
                   "RecoPROTOSeq",
                   "MoniPROTOSeq",
                   "LumiSeq",
                   "MoniHltSeq",
                   "HltfilterSeq",
                   "CaloBanksHandler",
                   "MoniMUONSeq",
                   "RecoCALOSeq",
                   "CaloPIDsSeq",
                   "RecoMUONSeq",
                   "MoniGENERALSeq",
                   "MoniTrSeq",
                   "MoniOTSeq",
                   "MoniSTSeq"])
    if mc:
        kill_sequence(["CheckRICHSeq",
                       "CheckRICHFUTURESeq",
                       "CheckMUONSeq",
                       "MCLinksCaloSeq"])
        # If we're running on simulated data add the IP monitoring algorthim
        # later in the sequence, after the MC links have been made.
        seq = GaudiSequencer("CheckPatSeq")
    else:
        seq = GaudiSequencer("MoniVELOSeq")

    seq.IgnoreFilterPassed = True
    for conf in confs:
        seq.Members.append(conf.sequence())
