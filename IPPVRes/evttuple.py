from Configurables import DaVinci, EventTuple

evt = EventTuple()
evt.ToolList = ['TupleToolEventInfo', 'TupleToolPrimaries', 'TupleToolRecoStats']
DaVinci().UserAlgorithms.append(evt)
