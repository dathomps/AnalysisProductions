from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from copy import deepcopy
from collections import defaultdict
from itertools import product

from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import MCMatchObjP2MCRelator
from Configurables import SubstitutePID
from Configurables import FilterInTrees
from Configurables import CombineParticles

# This import is required even when not using GaudiSequencer
from DecayTreeTuple.Configuration import GaudiSequencer  # NOQA
from PhysSelPython.Wrappers import AutomaticData
from PhysSelPython.Wrappers import Selection
from PhysSelPython.Wrappers import SelectionSequence

from D2hll.decay_parser import parse_decay
from D2hll.decay_parser import utils as decay_utils

import D2hll.config as config


def make_decay_tree_tuple(name, line, decay_descriptor, input_type, year, mc=False):
    assert str(year)[:2] == '20', year
    decay = parse_decay(decay_descriptor.replace('D_s', 'D'))

    if input_type == 'MDST':
        input_location = 'Phys/{line}/Particles'
    else:
        assert input_type == 'DST' and mc, input_type
        input_location = '/Event/AllStreams/Phys/{line}/Particles'

    # Create an ntuple
    dtt = DecayTreeTuple(name)
    if 'phi' not in name:
        dtt.Inputs = [input_location.format(line=line)]
        dtt.Decay = str(decay_utils.mark_all(decay))
        # Add the branches
        dtt.addBranches(decay_utils.get_branches(decay))
    else:
        leps_phi_sel = Selection(
            'leps_phi_sel' + name,
            Algorithm=FilterInTrees('particles_from_phi_filter' + name, Code="ALL"),
            RequiredSelections=[AutomaticData(Location=input_location.format(line=line))]
        )

        phi = CombineParticles(
            'phi_particles' + name,
            DecayDescriptor=' '.join(['phi(1020)', '->', str(decay.children[1]), str(decay.children[2])]),
            MotherCut='ALL',
        )
        phi_sel = Selection('phi_sel' + name, Algorithm=phi, RequiredSelections=[leps_phi_sel])

        dp = CombineParticles(
            'dp_particles' + name,
            DecayDescriptor='[D+ -> ' + str(decay.children[0]) + ' phi(1020)]cc',
            MotherCut='ALL',
        )
        dp_sel = Selection('dp_sel' + name, Algorithm=dp, RequiredSelections=[leps_phi_sel, phi_sel])

        dp_seq = SelectionSequence('phi_Seq' + name, TopSelection=dp_sel)
        DaVinci().UserAlgorithms += [dp_seq.sequence()]

        dtt.setDescriptorTemplate('${Dp}[D+ -> ${Dp_h}' + str(decay.children[0]) + ' ${Dp_phi}(phi(1020) -> ${Dp_l1}' + str(decay.children[1]) + ' ${Dp_l2}' + str(decay.children[2]) + ')]CC')
        # Workaround until next DaVinci release for https://gitlab.cern.ch/lhcb/Analysis/merge_requests/560
        dtt.Branches['Dp'] = dtt.Branches['Dp'].replace('^[', '[')
        dtt.Inputs = [dp_seq.outputLocation()]

        # Add a decay tree fit to constrain the phi mass
        dtt.Dp.addTupleTool('TupleToolDecayTreeFitter/DTFphi')
        dtt.Dp.DTFphi.constrainToOriginVertex = False
        dtt.Dp.DTFphi.Verbose = True
        dtt.Dp.DTFphi.UpdateDaughters = True
        dtt.Dp.DTFphi.daughtersToConstrain = ['phi(1020)']

        # Add a decay tree fit to constrain the phi mass and origin vertex
        dtt.Dp.addTupleTool('TupleToolDecayTreeFitter/DTFphivtx')
        dtt.Dp.DTFphivtx.constrainToOriginVertex = True
        dtt.Dp.DTFphivtx.Verbose = True
        dtt.Dp.DTFphivtx.UpdateDaughters = True
        dtt.Dp.DTFphivtx.daughtersToConstrain = ['phi(1020)']

        # Add a decay tree fit to constrain the origin vertex
        dtt.Dp.addTupleTool('TupleToolDecayTreeFitter/DTFvtx')
        dtt.Dp.DTFvtx.constrainToOriginVertex = True
        dtt.Dp.DTFvtx.Verbose = True
        dtt.Dp.DTFvtx.UpdateDaughters = True
        dtt.Dp.DTFvtx.daughtersToConstrain = []

    # Add the basic tools
    dtt.ToolList = config.tuple_tools[:]
    if mc:
        dtt.ToolList += ['TupleToolMCBackgroundInfo']  # 'TupleToolMCTruth']

        # Add TupleToolMCTruth with fix for "Fatal error No valid data at '/Event/Hlt2/Long/Protos'"
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

        mctruth = dtt.addTupleTool('TupleToolMCTruth')
        mctruth.addTool(MCMatchObjP2MCRelator)
        mctruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
    else:
        dtt.ToolList += ['TupleToolEventInfo']
        # Time DaVinciInitAlg to avoid pointless warnings from https://gitlab.cern.ch/lhcb/Analysis/merge_requests/512/
        # dtt.TupleToolEventInfo.Algorithms = ["DaVinciInitAlg"]

    # Add verbose reconstruction information
    dtt.addTupleTool('TupleToolRecoStats').Verbose = True

    # Setup for the related info variables
    parent_vars = deepcopy(config.mother_loki_vars)
    child_vars = defaultdict(dict)
    add_related_info(name, parent_vars, child_vars, line, mc)

    # Extra information from LoKi
    loki = dtt.addTupleTool('LoKi::Hybrid::TupleTool/basicLoKiTT')
    loki.Variables = config.basic_loki_vars
    loki.Preambulo = ['from LoKiTracks.decorators import TrIDC']

    # Add mother-specific varaibles
    loki = dtt.Dp.addTupleTool('LoKi::Hybrid::TupleTool/Dp_LoKiTT')
    loki.Variables = parent_vars
    if mc:
        # Add secondary info if a particle has a parent with lifetime > 1e-7 ns
        mctruth = dtt.Dp.addTupleTool('TupleToolMCTruth')
        mctruth.addTool(MCMatchObjP2MCRelator)
        mctruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        mctruth.ToolList = ['MCTupleToolPrompt']

    # Related info variables
    for child in ['Dp_h', 'Dp_l1', 'Dp_l2']:
        dtt.Dp.addTupleTool(
            'LoKi::Hybrid::TupleTool/{0}LoKiTT'.format(child)
        ).Variables = child_vars[child]

    # Add trigger information
    trigger = dtt.addTupleTool('TupleToolTISTOS')
    trigger.VerboseL0 = True
    trigger.VerboseHlt1 = True
    trigger.VerboseHlt2 = True
    trigger.TriggerList = get_triggers(mc, year)

    # Add calorimeter information
    l0_calo_tool = dtt.addTupleTool('TupleToolL0Calo')
    l0_calo_tool.WhichCalo = 'ECAL'

    return dtt


def make_substitute_pid(name, decay_descriptor, decay, input_location):
    raise NotImplementedError('Broken for uDST, see "Tupling MicroDSTs issue"')

    subs = SubstitutePID(
        'SubPID_' + name,
        Code="DECTREE('{0}')".format(decay_descriptor.replace('D_s', 'D')),
        Substitutions={
            'D+ -> X+ l+ l-': 'D_s+',
            'D+ -> X- l+ l+': 'D_s+',
            'D- -> X- l+ l-': 'D_s-',
            'D- -> X+ l- l-': 'D_s-'
        },
        MaxChi2PerDoF=-1
    )

    selSub = Selection(
        'Sel_' + name,
        Algorithm=subs,
        RequiredSelections=[AutomaticData(Location=input_location)]
    )

    return SelectionSequence('SelSeq_' + name, TopSelection=selSub)


def add_related_info(name, parent_vars, child_vars, line, mc):
    def add_relinfo(location, functor, prefix=None, parent=True, child=True):
        var_name = '_'.join(filter(None, ['Loki', str(prefix or ''), functor]))

        loki_var = "RELINFO('{location}', '{functor}', -42)".format(
            location='/'.join(filter(None, [
                '/Event', 'AllStreams' if mc else 'Charm', 'Phys', line, 'RelInfo'+location
            ])),
            functor=functor
        )

        if parent:
            parent_vars[var_name] = loki_var
        if child:
            for child in ['Dp_h', 'Dp_l1', 'Dp_l2']:
                child_vars[child][var_name] = loki_var.replace(location, location+'_'+child)

    for func, angle in product(config.cone_vars, config.cone_angles):
        add_relinfo('ConeVariables_{}'.format(angle), func, prefix=angle)

    # for func, angle in product(config.ew_cone_vars, config.ew_cone_angles):
    #     add_relinfo('ConeVariablesForEW_{}'.format(angle), func, prefix=angle)

    # add_relinfo('BstautauCDFIso', 'BSTAUTAUCDFISO', child=False)

    # for var in config.vertex_iso_variables:
    #     add_relinfo('VertexIsolation', var, child=False)

    # for var in config.vertex_iso_bdt_variables:
    #     add_relinfo('VertexIsolationBDT', var, child=False)

    # for var in config.track_iso_bdt_variables:
    #     add_relinfo('TrackIsolationBDT', var)

    # for var in config.Bs2MuMuTrackIsolation_variables:
    #     add_relinfo('Bs2MuMuTrackIsolations', var, parent=False)


def get_triggers(is_mc, year):
    # getL0Channels(0x6138160F)
    L0_LINES = [
        'L0CALODecision',
        'L0DiEM,lowMultDecision',
        'L0DiHadron,lowMultDecision',
        'L0DiMuonDecision',
        'L0DiMuon,lowMultDecision',
        'L0ElectronDecision',
        'L0Electron,lowMultDecision',
        'L0HadronDecision',
        'L0MuonDecision',
        'L0Muon,lowMultDecision',
        'L0MuonEWDecision',
        'L0PhotonDecision',
        'L0Photon,lowMultDecision',
    ]
    # getHlt1Lines(0x11321609)
    HLT1_LINES = [
        'Hlt1TrackMVADecision',
        'Hlt1TwoTrackMVADecision',
        'Hlt1TrackMVALooseDecision',
        'Hlt1TwoTrackMVALooseDecision',
        'Hlt1TrackMuonDecision',
        'Hlt1DiMuonHighMassDecision',
        'Hlt1DiMuonLowMassDecision',
        'Hlt1SingleMuonHighPTDecision',
        'Hlt1DiMuonNoL0Decision',
        'Hlt1B2GammaGammaDecision',
        'Hlt1B2PhiGamma_LTUNBDecision',
        'Hlt1B2PhiPhi_LTUNBDecision',
        'Hlt1DiMuonNoIPDecision',
        'Hlt1DiProtonDecision',
        'Hlt1DiProtonLowMultDecision',
        'Hlt1IncPhiDecision',
        'Hlt1L0AnyDecision',
        'Hlt1L0AnyNoSPDDecision',
        'Hlt1MBNoBiasDecision',
        'Hlt1MultiDiMuonNoIPDecision',
        'Hlt1MultiMuonNoL0Decision',
        'Hlt1NoBiasNonBeamBeamDecision',
        'Hlt1ODINTechnicalDecision',
        'Hlt1SingleElectronNoIPDecision',
        'Hlt1SingleMuonNoIPDecision',
    ]
    # getHlt2Lines(0x6138160F)
    if str(year) == '2015':
        HLT2_LINES = [
            'Hlt2RareCharmD2KeeFilterDecision',
            'Hlt2RareCharmD2KeeSSFilterDecision',
            'Hlt2RareCharmD2KMueFilterDecision',
            'Hlt2RareCharmD2KMueSSFilterDecision',
            'Hlt2RareCharmD2KMuMuFilterDecision',
            'Hlt2RareCharmD2KMuMuSSFilterDecision',
            'Hlt2RareCharmD2PieeFilterDecision',
            'Hlt2RareCharmD2PieeSSFilterDecision',
            'Hlt2RareCharmD2PiMueFilterDecision',
            'Hlt2RareCharmD2PiMueSSFilterDecision',
            'Hlt2RareCharmD2PiMuMuFilterDecision',
            'Hlt2RareCharmD2PiMuMuSSFilterDecision',
            'Hlt2SingleMuonDecision',
        ]
    else:
        HLT2_LINES = [
            'Hlt2RareCharmD2KEEOSDecision',
            'Hlt2RareCharmD2KEESSDecision',
            'Hlt2RareCharmD2KEEWSDecision',
            'Hlt2RareCharmD2KEMuOSDecision',
            'Hlt2RareCharmD2KMuEOSDecision',
            'Hlt2RareCharmD2KMuESSDecision',
            'Hlt2RareCharmD2KMuEWSDecision',
            'Hlt2RareCharmD2KMuMuOSDecision',
            'Hlt2RareCharmD2KMuMuSSDecision',
            'Hlt2RareCharmD2KMuMuWSDecision',
            'Hlt2RareCharmD2PiEEOSDecision',
            'Hlt2RareCharmD2PiEESSDecision',
            'Hlt2RareCharmD2PiEEWSDecision',
            'Hlt2RareCharmD2PiEMuOSDecision',
            'Hlt2RareCharmD2PiMuEOSDecision',
            'Hlt2RareCharmD2PiMuESSDecision',
            'Hlt2RareCharmD2PiMuEWSDecision',
            'Hlt2RareCharmD2PiMuMuOSDecision',
            'Hlt2RareCharmD2PiMuMuSSDecision',
            'Hlt2RareCharmD2PiMuMuWSDecision',
            'Hlt2SingleMuonDecision',
        ]

    if is_mc:
        HLT2_LINES += [
            'Hlt2B2Kpi0_B2K0pi0Decision',
            'Hlt2B2Kpi0_B2Kpi0Decision',
            'Hlt2BHadB02PpPpPmPmDecision',
            'Hlt2Bc2JpsiXTFBc2JpsiMuXDecision',
            'Hlt2CcDiHadronDiPhiDecision',
            'Hlt2CcDiHadronDiProtonDecision',
            'Hlt2CcDiHadronDiProtonLowMultDecision',
            'Hlt2CharmHadD02KmPipTurboDecision',
            'Hlt2CharmHadDp2EtaKp_Eta2EmEpGDecision',
            'Hlt2CharmHadDp2EtaKp_Eta2PimPipGDecision',
            'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0MDecision',
            'Hlt2CharmHadDp2EtaKp_Eta2PimPipPi0_Pi0RDecision',
            'Hlt2CharmHadDp2EtaPip_Eta2EmEpGDecision',
            'Hlt2CharmHadDp2EtaPip_Eta2PimPipGDecision',
            'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0MDecision',
            'Hlt2CharmHadDp2EtaPip_Eta2PimPipPi0_Pi0RDecision',
            'Hlt2CharmHadDp2EtapKp_Etap2EtaPimPip_EtaRDecision',
            'Hlt2CharmHadDp2EtapKp_Etap2PimPipGDecision',
            'Hlt2CharmHadDp2EtapPip_Etap2EtaPimPip_EtaRDecision',
            'Hlt2CharmHadDp2EtapPip_Etap2PimPipGDecision',
            'Hlt2CharmHadDp2KS0KS0KpTurboDecision',
            'Hlt2CharmHadDp2KS0KS0PipTurboDecision',
            'Hlt2CharmHadDp2KS0KmKpPip_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0KmKpPip_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0KmPipPip_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0KmPipPip_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0KpKpPim_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0KpKpPim_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0KpPimPip_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0KpPimPip_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0Kp_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0Kp_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0PimPipPip_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0PimPipPip_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KS0Pip_KS0DDTurboDecision',
            'Hlt2CharmHadDp2KS0Pip_KS0LLTurboDecision',
            'Hlt2CharmHadDp2KmKmKpPipPipTurboDecision',
            'Hlt2CharmHadDp2KmKpPimPipPipTurboDecision',
            'Hlt2CharmHadDp2KmPimPipPipPipTurboDecision',
            'Hlt2CharmHadDp2KpPi0_Pi02EmEpGDecision',
            'Hlt2CharmHadDp2PipPi0_Pi02EmEpGDecision',
            'Hlt2CharmHadDpDsp2KmKpKpPi0Decision',
            'Hlt2CharmHadDpDsp2KmKpPipPi0Decision',
            'Hlt2CharmHadDpDsp2KmPipPipPi0Decision',
            'Hlt2CharmHadDpDsp2KpKpPimPi0Decision',
            'Hlt2CharmHadDpDsp2KpPimPipPi0Decision',
            'Hlt2CharmHadDpDsp2PimPipPipPi0Decision',
            'Hlt2CharmHadDpToKmKpKpTurboDecision',
            'Hlt2CharmHadDpToKmKpPipTurboDecision',
            'Hlt2CharmHadDpToKmPipPipTurboDecision',
            'Hlt2CharmHadDpToKmPipPip_ForKPiAsymTurboDecision',
            'Hlt2CharmHadDpToKmPipPip_LTUNBTurboDecision',
            'Hlt2CharmHadDpToKpKpPimTurboDecision',
            'Hlt2CharmHadDpToKpPimPipTurboDecision',
            'Hlt2CharmHadDpToPimPipPipTurboDecision',
            'Hlt2CharmHadDsp2KS0KS0KpTurboDecision',
            'Hlt2CharmHadDsp2KS0KS0PipTurboDecision',
            'Hlt2CharmHadDsp2KS0KmKpPip_KS0DDTurboDecision',
            'Hlt2CharmHadDsp2KS0KmKpPip_KS0LLTurboDecision',
            'Hlt2CharmHadDsp2KS0KmPipPip_KS0DDTurboDecision',
            'Hlt2CharmHadDsp2KS0KmPipPip_KS0LLTurboDecision',
            'Hlt2CharmHadDsp2KS0KpKpPim_KS0DDTurboDecision',
            'Hlt2CharmHadDsp2KS0KpKpPim_KS0LLTurboDecision',
            'Hlt2CharmHadDsp2KS0KpPimPip_KS0DDTurboDecision',
            'Hlt2CharmHadDsp2KS0KpPimPip_KS0LLTurboDecision',
            'Hlt2CharmHadDsp2KS0PimPipPip_KS0DDTurboDecision',
            'Hlt2CharmHadDsp2KS0PimPipPip_KS0LLTurboDecision',
            'Hlt2CharmHadDsp2KmKmKpPipPipTurboDecision',
            'Hlt2CharmHadDsp2KmKpPimPipPipTurboDecision',
            'Hlt2CharmHadDsp2KmPimPipPipPipTurboDecision',
            'Hlt2CharmHadDspToKmKpKpTurboDecision',
            'Hlt2CharmHadDspToKmKpPipDecision',
            'Hlt2CharmHadDspToKmKpPipTurboDecision',
            'Hlt2CharmHadDspToKmKpPip_LTUNBTurboDecision',
            'Hlt2CharmHadDspToKmPipPipTurboDecision',
            'Hlt2CharmHadDspToKpKpPimTurboDecision',
            'Hlt2CharmHadDspToKpPimPipTurboDecision',
            'Hlt2CharmHadDspToPimPipPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02EmEpDecision',
            'Hlt2CharmHadDstp2D0Pip_D02GG_G2EmEpDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LLTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KS0_KS0LL_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0DD_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LLTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0LL_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0DD_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LLTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0LL_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0DD_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LLTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0LL_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DDTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0DD_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LLTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0LL_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKmKpPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKpKpPimTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0MDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKpPi0_Pi0RDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKpPimPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKpTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmKp_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmPimPipPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0MDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0RDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0MDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KpPimPi0_Pi0RDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KpPimPimPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KpPimTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02KpPim_LTUNBTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02PimPimPipPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0MDecision',
            'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0RDecision',
            'Hlt2CharmHadDstp2D0Pip_D02PimPipTurboDecision',
            'Hlt2CharmHadDstp2D0Pip_D02PimPip_LTUNBTurboDecision',
            'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
            'Hlt2CharmHadInclSigc2PiLc2HHXBDTDecision',
            'Hlt2CharmHadLcp2KS0KS0PpTurboDecision',
            'Hlt2CharmHadLcp2LamKmKpPip_Lam2PpPimTurboDecision',
            'Hlt2CharmHadLcp2LamKmPipPip_Lam2PpPimTurboDecision',
            'Hlt2CharmHadLcp2LamKp_LamDDTurboDecision',
            'Hlt2CharmHadLcp2LamKp_LamLLTurboDecision',
            'Hlt2CharmHadLcp2LamPip_LamDDTurboDecision',
            'Hlt2CharmHadLcp2LamPip_LamLLTurboDecision',
            'Hlt2CharmHadLcpToPpKmKmPipPipTurboDecision',
            'Hlt2CharmHadLcpToPpKmKpPimPipTurboDecision',
            'Hlt2CharmHadLcpToPpKmKpTurboDecision',
            'Hlt2CharmHadLcpToPpKmPimPipPipTurboDecision',
            'Hlt2CharmHadLcpToPpKmPipTurboDecision',
            'Hlt2CharmHadLcpToPpKmPip_LTUNBTurboDecision',
            'Hlt2CharmHadLcpToPpKpKpPimPimTurboDecision',
            'Hlt2CharmHadLcpToPpKpPimPimPipTurboDecision',
            'Hlt2CharmHadLcpToPpKpPimTurboDecision',
            'Hlt2CharmHadLcpToPpPimPimPipPipTurboDecision',
            'Hlt2CharmHadLcpToPpPimPipTurboDecision',
            'Hlt2CharmHadOmm2LamKm_DDDTurboDecision',
            'Hlt2CharmHadOmm2LamKm_DDLTurboDecision',
            'Hlt2CharmHadOmm2LamKm_LLLTurboDecision',
            'Hlt2CharmHadPentaToPhiPpPimTurboDecision',
            'Hlt2CharmHadPromptH2LamLamBar_LamDDTurboDecision',
            'Hlt2CharmHadPromptH2LamLamBar_LamLLTurboDecision',
            'Hlt2CharmHadPromptH2LamLamBar_LamLL_LamDDTurboDecision',
            'Hlt2CharmHadPromptH2LamLam_LamDDTurboDecision',
            'Hlt2CharmHadPromptH2LamLam_LamLLTurboDecision',
            'Hlt2CharmHadPromptH2LamLam_LamLL_LamDDTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPipPmDDDDTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPipPmDDLLTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPipPmLLLLTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPpPimDDDDTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPpPimDDLLTurboDecision',
            'Hlt2CharmHadSecondaryH2LamPpPimLLLLTurboDecision',
            'Hlt2CharmHadXic0ToPpKmKmPipTurboDecision',
            'Hlt2CharmHadXic0ToPpKmKmPip_LTUNBTurboDecision',
            'Hlt2CharmHadXiccp2D0PpKmPim_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccp2D0PpKmPip_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccp2D0PpKpPim_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccp2DpPpKm_Dp2KmPipPipTurboDecision',
            'Hlt2CharmHadXiccp2DpPpKp_Dp2KmPipPipTurboDecision',
            'Hlt2CharmHadXiccp2LcpKmPim_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccp2LcpKmPip_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccp2LcpKpPim_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccp2Xic0Pim_Xic0ToPpKmKmPipTurboDecision',
            'Hlt2CharmHadXiccp2Xic0Pip_Xic0ToPpKmKmPipTurboDecision',
            'Hlt2CharmHadXiccp2XicpPimPim_Xicp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccp2XicpPimPip_Xicp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2D0PpKmPimPip_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccpp2D0PpKmPipPip_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccpp2D0PpKpPimPip_D02KmPipTurboDecision',
            'Hlt2CharmHadXiccpp2DpPpKmPim_Dp2KmPipPipTurboDecision',
            'Hlt2CharmHadXiccpp2DpPpKmPip_Dp2KmPipPipTurboDecision',
            'Hlt2CharmHadXiccpp2DpPpKpPip_Dp2KmPipPipTurboDecision',
            'Hlt2CharmHadXiccpp2LcpKmPimPip_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2LcpKmPipPip_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2LcpKpPimPip_Lcp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2Xic0PimPip_Xic0ToPpKmKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2Xic0PipPip_Xic0ToPpKmKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2XicpPim_Xicp2PpKmPipTurboDecision',
            'Hlt2CharmHadXiccpp2XicpPip_Xicp2PpKmPipTurboDecision',
            'Hlt2CharmHadXicpToPpKmPipTurboDecision',
            'Hlt2CharmHadXim2LamPim_DDDTurboDecision',
            'Hlt2CharmHadXim2LamPim_DDLTurboDecision',
            'Hlt2CharmHadXim2LamPim_LLLTurboDecision',
            'Hlt2DPS2muHcDecision',
            'Hlt2DPS2x2muDecision',
            'Hlt2DPS2xHcDecision',
            'Hlt2DebugEventDecision',
            'Hlt2DiElectronElSoftDecision',
            'Hlt2DiMuonBDecision',
            'Hlt2DiMuonBTurboDecision',
            'Hlt2DiMuonDetachedDecision',
            'Hlt2DiMuonDetachedHeavyDecision',
            'Hlt2DiMuonDetachedJPsiDecision',
            'Hlt2DiMuonDetachedPsi2SDecision',
            'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonJPsiHighPTDecision',
            'Hlt2DiMuonJPsiTurboDecision',
            'Hlt2DiMuonPsi2SDecision',
            'Hlt2DiMuonPsi2SHighPTDecision',
            'Hlt2DiMuonPsi2STurboDecision',
            'Hlt2DiMuonSoftDecision',
            'Hlt2DiMuonZDecision',
            'Hlt2ForwardDecision',
            'Hlt2JetsJetLowPtDecision',
            'Hlt2JetsJetMuLowPtDecision',
            'Hlt2JetsJetSVLowPtDecision',
            'Hlt2LFVJpsiMuETurboDecision',
            'Hlt2LFVPhiMuETurboDecision',
            'Hlt2LFVPromptPhiMuETurboDecision',
            'Hlt2LFVUpsilonMuETurboDecision',
            'Hlt2MajoranaBLambdaMuDDDecision',
            'Hlt2MajoranaBLambdaMuLLDecision',
            'Hlt2NoBiasNonBeamBeamDecision',
            'Hlt2PassThroughDecision',
            'Hlt2PhiBs2PhiPhiDecision',
            'Hlt2PhiIncPhiDecision',
            'Hlt2PhiPhi2EETurboDecision',
            'Hlt2PhiPhi2KsKsDecision',
            'Hlt2PhiPhi2KsKsD0CtrlDecision',
            'Hlt2PhiPromptPhi2EETurboDecision',
            'Hlt2RareCharmD02EMuDecision',
            'Hlt2RareCharmD02KKMuMuDecision',
            'Hlt2RareCharmD02KKMueDecision',
            'Hlt2RareCharmD02KKeeDecision',
            'Hlt2RareCharmD02KMuDecision',
            'Hlt2RareCharmD02KPiDecision',
            'Hlt2RareCharmD02KPiMuMuDecision',
            'Hlt2RareCharmD02KPiMuMuSSDecision',
            'Hlt2RareCharmD02KPiMueDecision',
            'Hlt2RareCharmD02KPieeDecision',
            'Hlt2RareCharmD02MuMuDecision',
            'Hlt2RareCharmD02PiPiDecision',
            'Hlt2RareCharmD02PiPiMuMuDecision',
            'Hlt2RareCharmD02PiPiMueDecision',
            'Hlt2RareCharmD02PiPieeDecision',
            'Hlt2RareCharmLc2PMuMuDecision',
            'Hlt2RareCharmLc2PMuMuSSDecision',
            'Hlt2RareCharmLc2PMueDecision',
            'Hlt2RareCharmLc2PeeDecision',
            'Hlt2SingleMuonHighPTDecision',
            'Hlt2SingleMuonLowPTDecision',
            'Hlt2SingleMuonRareDecision',
            'Hlt2SingleMuonVHighPTDecision',
            'Hlt2StrangeKPiPiPiTurboDecision',
            'Hlt2StrangeLFVMuonElectronSoftDecision',
            'Hlt2TriMuonDetachedDecision',
            'Hlt2TriMuonTau23MuDecision',
            'Hlt2XcMuXForTauB2XcFakeMuDecision',
            'Hlt2XcMuXForTauB2XcMuDecision',
            'Hlt2ErrorEventDecision',
        ]

    return L0_LINES + HLT1_LINES + HLT2_LINES
