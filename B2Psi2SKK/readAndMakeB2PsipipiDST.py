# script to create ntuples for B->Psi2Spiplupiminus and B->Psi2SKplusKminus
# 18-11-15: remove pidk cut on mc samples

########################################################################
from os import environ
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
# from Hlt.Configuration import *
from Configurables import (GaudiSequencer,
                           DecayTreeTuple,
                           MCDecayTreeTuple,
                           CombineParticles,
                           LoKi__Hybrid__TupleTool,
                           DaVinci,
                           FilterDesktop)

from Configurables import LoKi__Hybrid__PlotTool as PlotTool
from Configurables import LoKi__Hybrid__FilterCriterion as LoKiFilterCriterion
from DecayTreeTuple.Configuration import *

from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import (
    SelectionSequence, Selection, DataOnDemand, AutomaticData, MergedSelection)
from Configurables import LoKi__VoidFilter as Filter
from PhysConf.Filters import LoKi_Filters

redoCalo = False
mcdecaytreetuple = False
simulation = True
restrip = False
daughters = "KK"
decay = "13144044"
LoKiTool = LoKi__Hybrid__TupleTool('LoKiTool')

# this is needed to emulate Stripping 21 on Stripping 20
if redoCalo is True:
    from Configurables import PhysConf
    PhysConf().CaloReProcessing = True
# to allow non-ascii strings in the options file
# !/usr/bin/env python
#  -*- coding: utf-8 -*-
###################################################
# central settings
#
###################################################
if restrip is False:
    if simulation is True:
        location = 'AllStreams/Phys/FullDSTDiMuonPsi2MuMuDetachedLine/'\
            'Particles'
        location_jpsi4pi = 'AllStreams/Phys/'\
            'FullDSTDiMuonJpsi2MuMuDetachedLine/Particles'
        if daughters is "KK":
            location_jpsi4pi = '/Event/AllStreams/Phys/'\
                'BetaSPsi2SMuMu_InclJPsi2SToMuMuDetachedLine/Particles'
    if simulation is False:
        location = 'Dimuon/Phys/FullDSTDiMuonPsi2MuMuDetachedLine/Particles'
        location_jpsi4pi = 'Dimuon/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/'\
            'Particles'

else:
    # ReStripping sequence
    # +++++++++++++++++++

    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive

    from Gaudi.Configuration import *
    from Configurables import DaVinci

    from Configurables import LoKi__PVReFitter
    LoKi__PVReFitter("LoKi::PVReFitter:PUBLIC").CheckTracksByLHCbIDs = True

    from CommonParticles.Utils import DefaultTrackingCuts
    DefaultTrackingCuts().Cuts = {"Chi2Cut": [0, 3],
                                  "CloneDistCut": [5000, 9e+99]}
    # Standard stripping21
    stripping = 'stripping21r1'
    config = strippingConfiguration(stripping)
    archive = strippingArchive(stripping)
    streams = buildStreams(stripping=config, archive=archive)

    # Select my line
    MyStream = StrippingStream("MyStream")

    MyLines = ['StrippingFullDSTDiMuonPsi2MuMuDetachedLine']

    MySLines = ['FullDSTDiMuonPsi2MuMuDetachedLine']
   # if(daughters is "KK" and simulation):
   #     MyLines = ['StrippingBetaSPsi2SMuMu_InclPsi2SToMuMuDetachedLine']
   #     MySLines = ['BetaSPsi2SMuMu_InclPsi2SToMuMuDetachedLine']
   # if("jpsipipi" in daughters):
   #     MyLines += ['StrippingFullDSTDiMuonJPsi2MuMuDetachedLine']
   #
   #     MySLines += ['FullDSTDiMuonJPsi2MuMuDetachedLine']

    for stream in streams:
        for line in stream.lines:
            if line.name() in MyLines:
                MyStream.appendLines([line])

    # Standard configuration of Stripping, do NOT change them
    from Configurables import ProcStatusCheck
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[MyStream],
                       HDRLocation="FakeDecReport",
                       MaxCandidates=2000,
                       AcceptBadEvents=False,
                       BadEventSelection=filterBadEvents,
                       TESPrefix='Strip'
                       )

    from Configurables import AuditorSvc, ChronoAuditor
    AuditorSvc().Auditors.append(ChronoAuditor("Chrono"))

    from PhysSelPython.Wrappers import AutomaticData, SelectionSequence
    # if restripping needed, really think of how to implement different
    # stripping lines for psi->mumu vs psi->jpsi pipi
    reqsel = AutomaticData(Location="/Event/Phys/" + MySLines[0] +
                           "/Particles")
    selSeq = SelectionSequence("SelSeq", TopSelection=reqsel)

    from Configurables import EventNodeKiller
    eventNodeKiller = EventNodeKiller('Stripkiller')
    eventNodeKiller.Nodes = ['/Event/AllStreams', '/Event/Strip']

    location = "/Event/Phys/" + MySLines[0] + "/Particles"

################################################
################################################
# CUTS CUTS CUTS CUTS CUTS CUTS CUTS
####################################
BCombCuts = "(AM > 4500 * MeV) & "\
    "(AM < 7100 * MeV)"

BCuts = "(M                              > 4600 * MeV) & "\
    "(M                                   < 6500 * MeV) & "\
    "(BPVIPCHI2()                         < 16) & "\
    "(BPVDIRA                             > 0.9999) & "\
    "(BPVVDCHI2                           > 64) & "\
    "(MAXTREE(ISBASIC,MIPCHI2DV(PRIMARY)) > 9 ) & "\
    "(VFASPF(VCHI2/VDOF)                  < 10)"
# softer cuts for jpsi4pi, bc more can go wrong
# with more tracks
BCuts_psi4pi = "(M                              > 4600 * MeV) & "\
    "(M                                   < 6500 * MeV) & "\
    "(BPVIPCHI2()                         < 16) & "\
    "(BPVDIRA                             > 0.9999) & "\
    "(BPVVDCHI2                           > 64) & "\
    "(MAXTREE(ISBASIC,MIPCHI2DV(PRIMARY)) > 9 ) & "\
    "(VFASPF(VCHI2/VDOF)                  < 8)"  # here is the difference
MuonCuts = "(TRGHP < 0.5) & "\
           "(MIPCHI2DV(PRIMARY) > 9) & "\
           "(PIDmu> 0) & "\
           "(ISMUON)"

PionCuts = "(TRGHP < 0.5) & "\
    "(MIPCHI2DV(PRIMARY) > 6.0) & "\
    "(HASRICH) & "\
    "(PT > 180) & "\
    "(PIDK < 5)"
KaonCuts = "(TRGHP < 0.5) & "\
    "(MIPCHI2DV(PRIMARY) > 6.0) & "\
    "(HASRICH) & "\
    "(PT > 180) & "\
    "(PIDK > -5)"

if simulation is True:
    PionCuts = "(TRGHP < 0.5) & "\
        "(MIPCHI2DV(PRIMARY) > 6.0) & "\
        "(HASRICH)  "\
        # "&(PIDK < 25)"
    KaonCuts = "(TRGHP < 0.5) & "\
        "(MIPCHI2DV(PRIMARY) > 6.0) & "\
        "(HASRICH)  "\
        # "&(PIDK > -25)"\
arrow = "->"
# simulation needs special arrow to fetch decay with fsr


def __Pions__(PionCuts):
    """
    Filter pions from StdLoosePions
    """
    _pions = AutomaticData(Location='Phys/StdAllLoosePions/Particles')
    _filter = FilterDesktop("PionFilter", Code=PionCuts)
    _sel = Selection("Selection_StdAllLoosePions",
                     RequiredSelections=[_pions],
                     Algorithm=_filter)
    return _sel


def __Kaons__(KaonCuts):
    """
    # Filter Kaons from StdLooseKaons
    """

    # _kaons = AutomaticData(Location = 'Phys/StdAllNoPIDsKaons/Particles')
    _kaons = AutomaticData(Location='Phys/StdAllLooseKaons/Particles')
    _filter = FilterDesktop("KaonFilter", Code=KaonCuts)
    # _sel = Selection("Selection_StdAllNoPIDsKaons",
    _sel = Selection("Selection_StdAllLooseKaons",
                     RequiredSelections=[_kaons],
                     Algorithm=_filter)
    return _sel


def __Muons__(MuonCuts):
    """
    Filter pions from StdLoosePions
    """
    # _muons = AutomaticData(Location = 'Phys/StdLooseMuons/Particles')
    _muons = AutomaticData(Location='Phys/StdAllLooseMuons/Particles')
    _filter = FilterDesktop("MuonFilter", Code=MuonCuts)
    # _sel = Selection("Selection_StdLooseMuons",
    _sel = Selection("Selection_StdAllLooseMuons",
                     RequiredSelections=[_muons],
                     Algorithm=_filter)
    return _sel


def __Psi__(Muons):
    _psi = CombineParticles()
    # _psi.InputLocations = ["StdLooseMuons"]
    _psi.DecayDescriptor = "psi(2S) " + arrow + " mu+ mu-"
    _psi.CombinationCut = "ADAMASS('psi(2S)')<100*MeV"
    _psi.MotherCut = "(VFASPF(VCHI2) < 25.0)"
    _psiConf = _psi.configurable("Combine_psi")
    _selpsi = Selection("Selection_psi",
                        Algorithm=_psiConf,
                        RequiredSelections=[Muons])

    return _selpsi


###
###
"""
def __Jpsi__(Muons):
    _Jpsi = CombineParticles ()
    _Jpsi.DecayDescriptor = "J/psi(1S) -> mu+ mu-"
    _Jpsi.CombinationCut = "ADAMASS('J/psi(1S)')<100*MeV"
    _Jpsi.MotherCut = "(VFASPF(VCHI2/VDOF) < 20.0)"
    _JpsiConf = _Jpsi.configurable("Combine_Jpsi")
    _selJpsi = Selection("Selection_Jpsi",
                         Algorithm = _JpsiConf,
                         RequiredSelections = [Muons])

    return _selJpsi

def __Psi__(location):
    return AutomaticData(Location = location)
"""


def __Jpsi__(location):
    return AutomaticData(Location=location)


def __Jpsipipi__(Jpsi, Pions):
    _Jpsipipi = CombineParticles()
    # _psi.InputLocations = ["StdLooseMuons"]
    _Jpsipipi.DecayDescriptor = "psi(2S) " + arrow + " J/psi(1S) pi+ pi-"
    _Jpsipipi.CombinationCut = "ADAMASS('psi(2S)')<100*MeV"
    _Jpsipipi.MotherCut = "(VFASPF(VCHI2) < 45.0)"
    _JpsipipiConf = _Jpsipipi.configurable("Combine_Jpsipipi")
    _selJpsipipi = Selection("Selection_Jpsipipi",
                             Algorithm=_JpsipipiConf,
                             RequiredSelections=[Jpsi, Pions])
    """
    Get the Psi
    Note the different location for data and MC
    """
    return _selJpsipipi


"""
def __Rho02PipPim__(Pions, RhoCuts):
#    Make rho from two pions

    _rho2pipi = CombineParticles()
    _rho2pipi.DecayDescriptor = "rho(770)0 -> pi+ pi-"
    _rho2pipi.MotherCut = RhoCuts

    _rho2pipiConf = _rho2pipi.configurable("Combine_rho2pipi")

    _selrho2pipi = Selection("Selection_rho2pipi",
                                  Algorithm = _rho2pipiConf,
                                  RequiredSelections = [Pions])
    return _selrho2pipi
"""
###


def __psipipi__(Psi, Pions, BCombCuts, BCuts):
    _psipipi = CombineParticles()
    _psipipi.DecayDescriptor = "B0 " + arrow + " psi(2S) pi+ pi-"
    if(decay.startswith("131")):
        _psipipi.DecayDescriptor = "B_s0 -> psi(2S) pi+ pi-"
    _psipipi.CombinationCut = BCombCuts
    _psipipi.MotherCut = BCuts
    _psipipiConf = _psipipi.configurable("Combine_psipipi")
    _selpsipipi = Selection("Selection_psipipi",
                            Algorithm=_psipipiConf,
                            RequiredSelections=[Psi, Pions])
    return _selpsipipi


def __psiKK__(Psi, Kaons, BCombCuts, BCuts):
    _psiKK = CombineParticles()
    _psiKK.DecayDescriptor = "B0 " + arrow + " psi(2S) K+ K-"
    if(decay.startswith("131")):
        _psiKK.DecayDescriptor = "B_s0 " + arrow + " psi(2S) K+ K-"
    _psiKK.CombinationCut = BCombCuts
    _psiKK.MotherCut = BCuts
    _psiKKConf = _psiKK.configurable("Combine_psiKK")
    _selpsiKK = Selection("Selection_psiKK",
                          Algorithm=_psiKKConf,
                          RequiredSelections=[Psi, Kaons])
    return _selpsiKK


def __psikpi__(Psi, Kaons, Pions, BCombCuts, BCuts):
    _psiKpi = CombineParticles()
    _psiKpi.DecayDescriptor = "[B0 " + arrow + " psi(2S) K+ pi-]cc"
    if(decay.startswith("131")):
        _psiKpi.DecayDescriptor = "[B_s0 " + arrow + " psi(2S) K+ pi-]cc"
    _psiKpi.CombinationCut = BCombCuts
    _psiKpi.MotherCut = BCuts
    _psiKpiConf = _psiKpi.configurable("Combine_psiKpi")
    _selpsiKpi = Selection("Selection_psiKpi",
                           Algorithm=_psiKpiConf,
                           RequiredSelections=[Psi, Kaons, Pions])
    return _selpsiKpi


def __Jpsi4pi__(Jpsipipi, Pions, BCombCuts, BCuts):

    _jpsi4pi = CombineParticles()
    _jpsi4pi.DecayDescriptor = "B0 " + arrow + " psi(2S) pi+ pi-"
    if(decay.startswith("131")):
        _jpsi4pi.DecayDescriptor = "B_s0 " + arrow + " psi(2S) pi+ pi-"
    _jpsi4pi.CombinationCut = BCombCuts
    _jpsi4pi.MotherCut = BCuts
    _jpsi4piConf = _jpsi4pi.configurable("Combine_jpsi4pi")
    _seljpsi4pi = Selection("Selection_jpsi4pi",
                            Algorithm=_jpsi4piConf,
                            RequiredSelections=[Jpsipipi, Pions])
    return _seljpsi4pi


def __psi4pi__(Psi, Pions, BCombCuts, BCuts):

    _psi4pi = CombineParticles()
    _psi4pi.DecayDescriptor = "B0 " + arrow + " psi(2S) pi+ pi- pi+ pi-"
    if(decay.startswith("131")):
        _psi4pi.DecayDescriptor = "B_s0 " + arrow + " psi(2S) pi+ pi- pi+ pi-"
    _psi4pi.CombinationCut = BCombCuts
    _psi4pi.MotherCut = BCuts
    _psi4piConf = _psi4pi.configurable("Combine_psi4pi")
    _selpsi4pi = Selection("Selection_psi4pi",
                           Algorithm=_psi4piConf,
                           RequiredSelections=[Psi, Pions])
    return _selpsi4pi


def __samesign__(Psi, Pions, BCombCuts, BCuts):
    _samesign = CombineParticles()
    _samesign.DecayDescriptor = "B0 " + arrow + " psi(2S) pi+ pi+"
    if(decay.startswith("131")):
        _samesign.DecayDescriptor = "B_s0 " + arrow + " psi(2S) pi+ pi+"
    _samesign.CombinationCut = BCombCuts
    _samesign.MotherCut = BCuts
    _samesignConf = _samesign.configurable("Combine_samesign")
    _selsamesign = Selection("Selection_samesign",
                             Algorithm=_samesignConf,
                             RequiredSelections=[Psi, Pions])
    return _selsamesign


"""
def __psipiplus__(Psi, Pions):
    _psipiplus = CombineParticles()
    _psipiplus.DecayDescriptor = "B0 -> psi(2S) pi+]"
    _psipiplusConf = _psipipi.configurable("Combine_psipipi")
    _selpsipiplus = Selection("Selection_psipiplus",
                            Algorithm = _psipiplusConf,
                            RequiredSelections = [Psi, Pions])
    return _selpsipiplus

def __psipiminus__(Psi, Pions):
    _psipiminus = CombineParticles()
    _psipiminus.DecayDescriptor = "B0 -> psi(2S) pi+]"
    _psipiminusConf = _psipipi.configurable("Combine_psipipi")
    _selpsipiminus = Selection("Selection_psipiminus",
                            Algorithm = _psipiminusConf,
                            RequiredSelections = [Psi, Pions])
    return _selpsipiminus
"""
######
Pions = __Pions__(PionCuts)
Kaons = __Kaons__(KaonCuts)
Muons = __Muons__(MuonCuts)
Psi = __Psi__(Muons)
# Jpsi = __Jpsi__(Muons)  # location_jpsi4pi)
# Psi = __Psi__(location)
Jpsi = __Jpsi__(location_jpsi4pi)
Jpsipipi = __Jpsipipi__(Jpsi, Pions)
Jpsi4Pi = __Jpsi4pi__(Jpsipipi, Pions, BCombCuts, BCuts)
# was BCuts_jpsi4pi before. too loose though
PsiPiPi = __psipipi__(Psi, Pions, BCombCuts, BCuts)
PsiKPi = __psikpi__(Psi, Kaons, Pions, BCombCuts, BCuts)
Psi4Pi = __psi4pi__(Psi, Pions, BCombCuts, BCuts_psi4pi)
PsiKK = __psiKK__(Psi, Kaons, BCombCuts, BCuts)
samesign = __samesign__(Psi, Pions, BCombCuts, BCuts)
# PsiPiplus           = __psipiplus__(Psi, Pions)
###
BPsiPiPiSeq = SelectionSequence('BPsiPiPiSeq', TopSelection=PsiPiPi)
BPsiKPiSeq = SelectionSequence('BPsiKPiSeq', TopSelection=PsiKPi)
BJpsi4PiSeq = SelectionSequence('BJpsi4PiSeq', TopSelection=Jpsi4Pi)
BPsi4PiSeq = SelectionSequence('Bpsi4PiSeq', TopSelection=Psi4Pi)
BPsiKKSeq = SelectionSequence('BPsiKKSeq', TopSelection=PsiKK)
SameSignSeq = SelectionSequence('SameSignSeq', TopSelection=samesign)
####################################################################
if mcdecaytreetuple:
    BPsiPiPi = MCDecayTreeTuple("BPsiPiPiTuple")
    BPsKPi = MCDecayTreeTuple("BPsiKPiTuple")

    BJpsi4Pi = MCDecayTreeTuple("BJpsi4PiTuple")
    BPsi4Pi = MCDecayTreeTuple("Bpsi4PiTuple")
    BPsiKK = MCDecayTreeTuple("BPsiKKTuple")
else:
    BPsiPiPi = DecayTreeTuple("BPsiPiPiTuple")
    BPsiKPi = DecayTreeTuple("BPsiKPiTuple")

    BJpsi4Pi = DecayTreeTuple("BJpsi4PiTuple")
    BPsi4Pi = DecayTreeTuple("Bpsi4PiTuple")
    BPsiKK = DecayTreeTuple("BPsiKKTuple")
samesignbg = DecayTreeTuple("Bsamesignbg")

# input locations
BPsiPiPi.Inputs = [PsiPiPi.outputLocation()]
BPsiKPi.Inputs = [PsiKPi.outputLocation()]
BPsi4Pi.Inputs = [Psi4Pi.outputLocation()]
BJpsi4Pi.Inputs = [Jpsi4Pi.outputLocation()]
BPsiKK.Inputs = [PsiKK.outputLocation()]
samesignbg.Inputs = [samesign.outputLocation()]

# decay descriptors
if(decay.startswith("131")):
    BPsiPiPi.Decay = "B_s0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^pi+ ^pi-"
    BPsiKPi.Decay = "[B_s0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^K+ ^pi-]CC"
    BPsi4Pi.Decay = "B_s0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^pi+ ^pi- ^pi+ ^pi- "
    BJpsi4Pi.Decay = "B_s0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^(J/psi(1S) " + arrow + " ^mu- ^mu+) ^pi+ ^pi-)  ^pi+ ^pi-"
    BPsiKK.Decay = "B_s0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^K+ ^K-"
else:
    BPsiPiPi.Decay = "B0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^pi+ ^pi-"
    BPsiKPi.Decay = "[B0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^K+ ^pi-]CC"
    BPsi4Pi.Decay = "B0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^pi+ ^pi- ^pi+ ^pi-"
    BJpsi4Pi.Decay = "B0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^(J/psi(1S) " + arrow + " ^mu- ^mu+) ^pi+ ^pi-  )  ^pi+ ^pi-"
    BPsiKK.Decay = "B0 " + arrow + " ^(psi(2S) " + arrow +\
        " ^mu- ^mu+)  ^K+ ^K-"
samesignbg.Decay = "B0 " + arrow + " ^(psi(2S) " + arrow +\
    " ^mu- ^mu+)  ^pi+ ^pi+"


# define the tools and configure them
BPsiPiPi.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    # ,"TupleToolTrigger",
    "TupleToolPrimaries",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolAngles",
    "LoKi::Hybrid::EvtTupleTool/LoKiEvent",
    # ,"DecayTreeTupleDalitz"
]
BPsiKPi.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    # "TupleToolTrigger",
    "TupleToolPrimaries",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolAngles",
    "LoKi::Hybrid::EvtTupleTool/LoKiEvent",
    # "DecayTreeTupleDalitz",
]

BJpsi4Pi.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    # "TupleToolTrigger",
    "TupleToolPrimaries",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolAngles",
    "LoKi::Hybrid::EvtTupleTool/LoKiEvent",
    # "DecayTreeTupleDalitz",
]
BPsi4Pi.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    # "TupleToolTrigger",
    "TupleToolPrimaries",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolAngles",
    "LoKi::Hybrid::EvtTupleTool/LoKiEvent",
    # "DecayTreeTupleDalitz",
]
BPsiKK.ToolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    # "TupleToolTrigger",
    "TupleToolPrimaries",
    "TupleToolEventInfo",
    "TupleToolGeometry",
    "TupleToolAngles",
    "LoKi::Hybrid::EvtTupleTool/LoKiEvent",
    # "DecayTreeTupleDalitz"
]


# define the list of triggers that could have fired...
triggerList = ["L0MuonDecision",
               "L0DiMuonDecision",
               "Hlt1TrackMuonDecision",
               "Hlt1TrackAllL0Decision",
               "Hlt1DiMuonHighMassDecision",
               "Hlt2TopoMu2BodyBBDTDecision",
               "Hlt2TopoMu3BodyBBDTDecision",
               "Hlt2TopoMu4BodyBBDTDecision",
               "Hlt2DiMuonJPsiDecision",
               "Hlt2DiMuonDetachedJPsiDecision",
               "Hlt2DiMuonJPsiHighPTDecision",
               "Hlt2DiMuonDetachedDecision",
               "Hlt2DiMuonPsi2SHighPTDecision",
               "Hlt2DiMuonDetachedPsi2SDecision",
               "Hlt2DiMuonDetachedHeavyDecision"]


# other variables
# including isolation variables
# for microDST, they are stored under a special location
BPsiPiPi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
BPsiKPi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
BPsi4Pi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
BJpsi4Pi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
BPsiKK.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")

# some truthmatching stuff
if simulation is True:
    if(daughters is "KK"):
        MCTruth1 = BPsiKK.addTupleTool("TupleToolMCTruth/MCTruth1")
    else:
        MCTruth1 = BPsiPiPi.addTupleTool("TupleToolMCTruth/MCTruth1")
    if("jpsi" in daughters):
        MCTruth3 = BJpsi4Pi.addTupleTool("TupleToolMCTruth/MCTruth3")
        MCTruth3.addTupleTool("MCTupleToolHierarchy")
        MCTruth3.addTupleTool("MCTupleToolAngles")
    MCTruth4 = BPsiKPi.addTupleTool("TupleToolMCTruth/MCTruth4")

    # add truthmatching for 4 pion cases, when mc available!
    MCTruth1.addTupleTool("MCTupleToolHierarchy")
    MCTruth1.addTupleTool("MCTupleToolAngles")

    BPsiPiPi.addTupleTool("TupleToolMCBackgroundInfo")
    BPsiKPi.addTupleTool("TupleToolMCBackgroundInfo")
    BJpsi4Pi.addTupleTool("TupleToolMCBackgroundInfo")
    BPsiKK.addTupleTool("TupleToolMCBackgroundInfo")
    MCTruth2 = samesignbg.addTupleTool("TupleToolMCTruth/MCTruth2")
    MCTruth2.addTupleTool("MCTupleToolHierarchy")
    MCTruth2.addTupleTool("MCTupleToolAngles")

    MCTruth4.addTupleTool("MCTupleToolHierarchy")
    MCTruth4.addTupleTool("MCTupleToolAngles")

####
# end of configuration for all particles
samesignbg.ToolList = BPsiPiPi.ToolList
####

if(mcdecaytreetuple):
    arrow = "=>"
if(decay.startswith("131")):
    BPsiPiPi.addBranches({
        "B": "^(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi-)",

        "Psi": "B_s0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) pi+ pi-",

        "muplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  pi+ pi-",

        "muminus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) pi+ pi-",

        "piplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^pi+ pi-",

        "piminus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) pi+ ^pi-",

        # "pipi":"B_s0 " + arrow +" (psi(2S) " + arrow +\
        # "mu- mu+) ^(rho(770)0 " + arrow +" pi+ pi-)"
        # "Psipiplus":"B_s0 " + arrow +\
        # "^((psi(2S) " + arrow +" mu- mu+ pi+)  pi-",
        # "Psipiminus":"B_s0 " + arrow +\
        # "^((psi(2S) " + arrow +  mu- mu+ pi-) pi+"
    })
    BPsiKPi.addBranches({
        "B": "[^(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  K+ pi-)]CC",

        "Psi": "[(B_s0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) K+ pi-)]CC",

        "muplus": "[(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  K+ pi-)]CC",

        "muminus": "[(B_s0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) K+ pi-)]CC",

        "Kplus": "[(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^K+ pi-)]CC",

        "piminus": "[(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) K+ ^pi-)]CC",
        # "pipi":"B_s0 " + arrow + " (psi(2S) " + arrow +\
        # "mu- mu+) ^(rho(770)0 " + arrow + " pi+ pi-)"
        # "Psipiplus":"B_s0 " + arrow +\
        # "^((psi(2S) " + arrow + " mu- mu+ pi+)  pi-",
        # "Psipiminus":"B_s0 " + arrow +\
        # "^((psi(2S) " + arrow + " mu- mu+ pi-) pi+"
    })
    BPsi4Pi.addBranches({
        "B": "^(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi- pi+ pi-)",

        "Psi": "B_s0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) pi+ pi- pi+ pi-",

        "muplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  pi+ pi- pi+ pi-",

        "muminus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) pi+ pi- pi+ pi-",

        "piplus1": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^pi+ pi- pi+ pi-",

        "piminus1": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) pi+ ^pi- pi+ pi-",

        "piplus2": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi- ^pi+ pi-",

        "piminus2": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi- pi+ ^pi-",
        # "pipi":"B_s0 " + arrow + " (psi(2S) " + arrow +
        # "mu- mu+) ^(rho(770)0 " + arrow + " pi+ pi-)"
        # "Psipiplus":"B_s0 " + arrow +
        # "^((psi(2S) " + arrow + " mu- mu+ pi+)  pi-",
        # "Psipiminus":"B_s0 " + arrow +
        # "^((psi(2S) " + arrow + " mu- mu+ pi-) pi+"
    })
    BJpsi4Pi.addBranches({
        "B": "^(B_s0 " + arrow + " (psi(2S) " + arrow +
        " (J/psi(1S) " + arrow + "mu- mu+) pi+ pi-)  pi+ pi-)",

        "Psi": "B_s0 " + arrow + " ^(psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) pi+ pi-",

        "Jpsi": "B_s0 " + arrow +
        "(psi(2S) " + arrow + " ^(J/psi(1S) " + arrow +
        "mu- mu+) pi+ pi-) pi+ pi-",
        "muplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- ^mu+) pi+ pi-)  pi+ pi-",

        "muminus": "B_s0 " + arrow +
        "(psi(2S) " + arrow + " (J/psi(1S) " + arrow +
        "^mu- mu+) pi+ pi-) pi+ pi-",

        "piplus_psi": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) ^pi+ pi-)  pi+ pi-",

        "piminus_psi": "B_s0 " + arrow +
        "(psi(2S) " + arrow + " (J/psi(1S) " + arrow +
        " mu- mu+) pi+ ^pi-) pi+ pi-",

        "piplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) ^pi+ pi-",

        "piminus": "B_s0 " + arrow +
        "(psi(2S) " + arrow + " (J/psi(1S) " + arrow +
        " mu- mu+) pi+ pi-) pi+ ^pi-",

    })

    BPsiKK.addBranches({
        "B": "^(B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  K+ K-)",

        "Psi": "B_s0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) K+ K-",

        "muplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  K+ K-",

        "muminus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) K+ K-",

        "Kplus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^K+ K-",

        "Kminus": "B_s0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) K+ ^K-",

        # "pipi":"B_s0 " + arrow + " (psi(2S) " + arrow +
        # "mu- mu+) ^(rho(770)0 " + arrow + " pi+ pi-)"
        # "Psipiplus":"B_s0 " + arrow + " ^((psi(2S) " + arrow +
        # "mu- mu+ pi+)  pi-",
        # "Psipiminus":"B_s0 " + arrow + " ^((psi(2S) " + arrow +
        # "mu- mu+ pi-) pi+"
    })

else:
    BPsiPiPi.addBranches({
        "B": "^(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi-)",

        "Psi": "B0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) pi+ pi-",

        "muplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  pi+ pi-",

        "muminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) pi+ pi-",

        "piplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^pi+ pi-",

        "piminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) pi+ ^pi-",

    })
    BPsiKPi.addBranches({
        "B": "[^(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  K+ pi-)]CC",

        "Psi": "[(B0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) K+ pi-)]CC",

        "muplus": "[(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  K+ pi-)]CC",

        "muminus": "[(B0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) K+ pi-)]CC",

        "Kplus": "[(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^K+ pi-)]CC",

        "piminus": "[(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) K+ ^pi-)]CC",

    })
    BPsi4Pi.addBranches({
        "B": "^(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  pi+ pi- pi+ pi-)",

        "Psi": "B0 " + arrow +
        "^(psi(2S) " + arrow + " mu- mu+) pi+ pi- pi+ pi-",

        "muplus": "B0 " + arrow +
        "(psi(2S) " + arrow + " mu- ^mu+)  pi+ pi- pi+ pi-",

        "muminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) pi+ pi- pi+ pi-",

        "piplus1": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^pi+ pi- pi+ pi-",

        "piminus1": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) pi+ ^pi- pi+ pi-",

        "piplus2": "B0 " + arrow +
        "(psi(2S) " + arrow + " mu- mu+)  pi+ pi- ^pi+ pi-",

        "piminus2": "B0 " + arrow +
        "(psi(2S) " + arrow + " mu- mu+) pi+ pi- pi+ ^pi-",

    })
    BJpsi4Pi.addBranches({
        "B": "^(B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-)  pi+ pi-)",

        "Psi": "B0 " + arrow + " ^(psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) pi+ pi-",

        "Jpsi": "B0 " + arrow + " (psi(2S) " + arrow +
        "^(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) pi+ pi-",

        "muplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- ^mu+) pi+ pi-)  pi+ pi-",

        "muminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " ^mu- mu+) pi+ pi-) pi+ pi-",

        "piplus_psi": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) ^pi+ pi-)  pi+ pi-",

        "piminus_psi": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ ^pi-) pi+ pi-",

        "piplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) ^pi+ pi-",

        "piminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "(J/psi(1S) " + arrow + " mu- mu+) pi+ pi-) pi+ ^pi-",


    })

    BPsiKK.addBranches({
        "B": "^(B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+)  K+ K-)",

        "Psi": "B0 " + arrow + " ^(psi(2S) " + arrow +
        "mu- mu+) K+ K-",

        "muplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- ^mu+)  K+ K-",

        "muminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "^mu- mu+) K+ K-",

        "Kplus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) ^K+ K-",

        "Kminus": "B0 " + arrow + " (psi(2S) " + arrow +
        "mu- mu+) K+ ^K-",
    })

BPsiPiPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
BPsiPiPi.B.BDTF.constrainToOriginVertex = True
BPsiPiPi.B.BDTF.daughtersToConstrain = ['psi(2S)']


BPsiPiPi.B.addTupleTool("TupleToolDecayTreeFitter/BdconstraintDTF")
BPsiPiPi.B.BdconstraintDTF.constrainToOriginVertex = True
BPsiPiPi.B.BdconstraintDTF.daughtersToConstrain = ['B0', 'psi(2S)']
BPsiPiPi.B.BdconstraintDTF.Verbose = True
BPsiPiPi.B.BdconstraintDTF.UpdateDaughters = True

BPsiPiPi.B.addTupleTool("TupleToolDecayTreeFitter/BsconstraintDTF")
BPsiPiPi.B.BsconstraintDTF.constrainToOriginVertex = True
BPsiPiPi.B.BsconstraintDTF.daughtersToConstrain = ['B_s0', 'psi(2S)']
BPsiPiPi.B.BsconstraintDTF.Verbose = True
BPsiPiPi.B.BsconstraintDTF.UpdateDaughters = True

BPsiPiPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kplus_hypo")
BPsiPiPi.B.BDTF_Kplus_hypo.constrainToOriginVertex = True
BPsiPiPi.B.BDTF_Kplus_hypo.daughtersToConstrain = ['psi(2S)']

BPsiPiPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kminus_hypo")
BPsiPiPi.B.BDTF_Kminus_hypo.constrainToOriginVertex = True
BPsiPiPi.B.BDTF_Kminus_hypo.daughtersToConstrain = ['psi(2S)']

BPsiKPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
BPsiKPi.B.BDTF.constrainToOriginVertex = True
BPsiKPi.B.BDTF.daughtersToConstrain = ['psi(2S)']


BPsiKPi.B.addTupleTool("TupleToolDecayTreeFitter/BdconstraintDTF")
BPsiKPi.B.BdconstraintDTF.constrainToOriginVertex = True
BPsiKPi.B.BdconstraintDTF.daughtersToConstrain = ['B0', 'psi(2S)']
BPsiKPi.B.BdconstraintDTF.Verbose = True
BPsiKPi.B.BdconstraintDTF.UpdateDaughters = True

BPsiKPi.B.addTupleTool("TupleToolDecayTreeFitter/BsconstraintDTF")
BPsiKPi.B.BsconstraintDTF.constrainToOriginVertex = True
BPsiKPi.B.BsconstraintDTF.daughtersToConstrain = ['B_s0', 'psi(2S)']
BPsiKPi.B.BsconstraintDTF.Verbose = True
BPsiKPi.B.BsconstraintDTF.UpdateDaughters = True

BPsiKPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_piplus_hypo")
BPsiKPi.B.BDTF_piplus_hypo.constrainToOriginVertex = True
BPsiKPi.B.BDTF_piplus_hypo.daughtersToConstrain = ['psi(2S)']

BPsiKPi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kminus_hypo")
BPsiKPi.B.BDTF_piplus_hypo.constrainToOriginVertex = True
BPsiKPi.B.BDTF_piplus_hypo.daughtersToConstrain = ['psi(2S)']



if(decay.startswith("131")):

    BPsiPiPi.B.BdconstraintDTF.Substitutions = {
        'B_s0 ->   psi(2S) pi+ pi-': 'B0',
    }

    BPsiPiPi.B.BDTF_Kplus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   ^pi+ Meson': 'K+',
    }
    BPsiPiPi.B.BDTF_Kminus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson ^pi- ': 'K-',
    }

    BPsiKPi.B.BdconstraintDTF.Substitutions = {
        'B_s0 ->   psi(2S) K+ pi-': 'B0',
        'B_s~0 ->  psi(2S) K- pi+': 'B~0'
    }

    BPsiKPi.B.BDTF_piplus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   ^K+ Meson': 'pi+',
        'B_s~0 -> psi(2S)  ^K- Meson': 'pi-'
    }
    BPsiKPi.B.BDTF_Kminus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson ^pi-': 'K+',
        'B_s~0 -> psi(2S)  Meson ^pi+': 'K-'
    }

else:
    BPsiPiPi.B.BsconstraintDTF.Substitutions = {
        'B0 ->  psi(2S) pi+ pi-': 'B_s0',
    }
    BPsiPiPi.B.BDTF_Kplus_hypo.Substitutions = {
        'B0 -> psi(2S)   ^pi+ Meson': 'K+',
    }
    BPsiPiPi.B.BDTF_Kminus_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson ^pi- ': 'K-',
    }
    BPsiKPi.B.BsconstraintDTF.Substitutions = {
        'B0 ->  psi(2S) K+ pi-': 'B_s0',
        'B~0 -> psi(2S) K- pi+': 'B_s~0'
    }
    BPsiKPi.B.BDTF_piplus_hypo.Substitutions = {
        'B0 -> psi(2S)   ^K+ Meson': 'pi+',
        'B~0 -> psi(2S)  ^K- Meson': 'pi-'
    }
    BPsiKPi.B.BDTF_Kminus_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson ^pi-': 'K-',
        'B~0 -> psi(2S)  Meson ^pi+': 'K+'
    }

# BPsiPiPi.B.BdconstraintDTF.variables = Variables
# BPsiPiPi.B.BsconstraintDTF.variables = Variables


BPsiPiPi.B.addTupleTool("TupleToolGeometry")
BPsiPiPi.B.addTupleTool("TupleToolDira")
BPsiPiPi.B.addTupleTool("TupleToolPropertime")
BPsiPiPi.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiPiPi.B.TupleToolTISTOS.TriggerList = triggerList
BPsiPiPi.B.TupleToolTISTOS.VerboseL0 = True
BPsiPiPi.B.TupleToolTISTOS.VerboseHlt1 = True
BPsiPiPi.B.TupleToolTISTOS.VerboseHlt2 = True
BPsiPiPi.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

BPsiPiPi.B.TupleToolConeIsolation.Verbose = True
BPsiPiPi.B.TupleToolConeIsolation.MinConeSize = 0.6
BPsiPiPi.B.TupleToolConeIsolation.MaxConeSize = 2.0
BPsiPiPi.B.TupleToolConeIsolation.SizeStep = 0.3


BPsiKPi.B.addTupleTool("TupleToolGeometry")
BPsiKPi.B.addTupleTool("TupleToolDira")
BPsiKPi.B.addTupleTool("TupleToolPropertime")
BPsiKPi.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKPi.B.TupleToolTISTOS.TriggerList = triggerList
BPsiKPi.B.TupleToolTISTOS.VerboseL0 = True
BPsiKPi.B.TupleToolTISTOS.VerboseHlt1 = True
BPsiKPi.B.TupleToolTISTOS.VerboseHlt2 = True
BPsiKPi.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

BPsiKPi.B.TupleToolConeIsolation.Verbose = True
BPsiKPi.B.TupleToolConeIsolation.MinConeSize = 0.6
BPsiKPi.B.TupleToolConeIsolation.MaxConeSize = 2.0
BPsiKPi.B.TupleToolConeIsolation.SizeStep = 0.3


from Configurables import VertexIsolation

BPsiPiPi.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")

BPsiPiPi.B.TupleToolVertexI.ExtraName = "LongVtxVeto"

# Psi tools
BPsiPiPi.Psi.addTupleTool("TupleToolGeometry")
BPsiPiPi.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiPiPi.Psi.TupleToolTISTOS.TriggerList = triggerList
BPsiPiPi.Psi.TupleToolTISTOS.VerboseL0 = True
BPsiPiPi.Psi.TupleToolTISTOS.VerboseHlt1 = True
BPsiPiPi.Psi.TupleToolTISTOS.VerboseHlt2 = True

# muplus tools
BPsiPiPi.muplus.addTupleTool("TupleToolGeometry")
BPsiPiPi.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiPiPi.muplus.TupleToolTISTOS.TriggerList = triggerList
BPsiPiPi.muplus.TupleToolTISTOS.VerboseL0 = True
BPsiPiPi.muplus.TupleToolTISTOS.VerboseHlt1 = True
BPsiPiPi.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
BPsiPiPi.muminus.addTupleTool("TupleToolGeometry")
BPsiPiPi.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiPiPi.muminus.TupleToolTISTOS.TriggerList = triggerList
BPsiPiPi.muminus.TupleToolTISTOS.VerboseL0 = True
BPsiPiPi.muminus.TupleToolTISTOS.VerboseHlt1 = True
BPsiPiPi.muminus.TupleToolTISTOS.VerboseHlt2 = True

# piplus tools
BPsiPiPi.piplus.addTupleTool("TupleToolGeometry")
# piminus tools
BPsiPiPi.piminus.addTupleTool("TupleToolGeometry")


# same for B->PsiKPi

BPsiKPi.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")

BPsiKPi.B.TupleToolVertexI.ExtraName = "LongVtxVeto"


# Psi tools
BPsiKPi.Psi.addTupleTool("TupleToolGeometry")
BPsiKPi.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKPi.Psi.TupleToolTISTOS.TriggerList = triggerList
BPsiKPi.Psi.TupleToolTISTOS.VerboseL0 = True
BPsiKPi.Psi.TupleToolTISTOS.VerboseHlt1 = True
BPsiKPi.Psi.TupleToolTISTOS.VerboseHlt2 = True

# muplus tools
BPsiKPi.muplus.addTupleTool("TupleToolGeometry")
BPsiKPi.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKPi.muplus.TupleToolTISTOS.TriggerList = triggerList
BPsiKPi.muplus.TupleToolTISTOS.VerboseL0 = True
BPsiKPi.muplus.TupleToolTISTOS.VerboseHlt1 = True
BPsiKPi.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
BPsiKPi.muminus.addTupleTool("TupleToolGeometry")
BPsiKPi.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKPi.muminus.TupleToolTISTOS.TriggerList = triggerList
BPsiKPi.muminus.TupleToolTISTOS.VerboseL0 = True
BPsiKPi.muminus.TupleToolTISTOS.VerboseHlt1 = True
BPsiKPi.muminus.TupleToolTISTOS.VerboseHlt2 = True

# piplus tools
BPsiKPi.Kplus.addTupleTool("TupleToolGeometry")

# piminus tools
BPsiKPi.piminus.addTupleTool("TupleToolGeometry")

# same for B->Psi4pi


BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
BPsi4Pi.B.BDTF.constrainToOriginVertex = True
BPsi4Pi.B.BDTF.daughtersToConstrain = ['psi(2S)']


BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BdconstraintDTF")
BPsi4Pi.B.BdconstraintDTF.constrainToOriginVertex = True

BPsi4Pi.B.BdconstraintDTF.daughtersToConstrain = ['B0', 'psi(2S)']
BPsi4Pi.B.BdconstraintDTF.Verbose = True
BPsi4Pi.B.BdconstraintDTF.UpdateDaughters = True

BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BsconstraintDTF")
BPsi4Pi.B.BsconstraintDTF.constrainToOriginVertex = True
BPsi4Pi.B.BsconstraintDTF.daughtersToConstrain = ['B_s0', 'psi(2S)']
BPsi4Pi.B.BsconstraintDTF.Verbose = True
BPsi4Pi.B.BsconstraintDTF.UpdateDaughters = True

BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kplus1_hypo")
BPsi4Pi.B.BDTF_Kplus1_hypo.constrainToOriginVertex = True
BPsi4Pi.B.BDTF_Kplus1_hypo.daughtersToConstrain = ['psi(2S)']

BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kminus1_hypo")
BPsi4Pi.B.BDTF_Kminus1_hypo.constrainToOriginVertex = True
BPsi4Pi.B.BDTF_Kminus1_hypo.daughtersToConstrain = ['psi(2S)']

BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kplus2_hypo")
BPsi4Pi.B.BDTF_Kplus2_hypo.constrainToOriginVertex = True
BPsi4Pi.B.BDTF_Kplus2_hypo.daughtersToConstrain = ['psi(2S)']

BPsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kminus2_hypo")
BPsi4Pi.B.BDTF_Kminus2_hypo.constrainToOriginVertex = True
BPsi4Pi.B.BDTF_Kminus2_hypo.daughtersToConstrain = ['psi(2S)']

if(decay.startswith("131")):

    BPsi4Pi.B.BdconstraintDTF.Substitutions = {
        'B_s0 ->   psi(2S) pi+ pi- pi+ pi-': 'B0',
    }

    BPsi4Pi.B.BDTF_Kplus1_hypo.Substitutions = {
        'B_s0 -> psi(2S)   ^pi+ pi- Meson pi-': 'K+',
    }
    BPsi4Pi.B.BDTF_Kminus1_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson ^pi-  Meson pi-': 'K-',
    }
    BPsi4Pi.B.BDTF_Kplus2_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson pi- ^pi+ pi- ': 'K+',
    }
    BPsi4Pi.B.BDTF_Kminus2_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson pi- Meson ^pi-  ': 'K-',
    }
else:
    BPsi4Pi.B.BsconstraintDTF.Substitutions = {
        'B0 ->  psi(2S) pi+ pi-  Meson pi-': 'B_s0',
    }
    BPsi4Pi.B.BDTF_Kplus1_hypo.Substitutions = {
        'B0 -> psi(2S)   ^pi+ Meson  Meson pi-': 'K+',
    }
    BPsi4Pi.B.BDTF_Kminus1_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson ^pi-  Meson pi-': 'K-',
    }
    BPsi4Pi.B.BDTF_Kplus2_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson pi- ^pi+ pi- ': 'K+',
    }
    BPsi4Pi.B.BDTF_Kminus2_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson pi- Meson ^pi-  ': 'K-',
    }

BPsi4Pi.B.addTupleTool("TupleToolGeometry")
BPsi4Pi.B.addTupleTool("TupleToolDira")
BPsi4Pi.B.addTupleTool("TupleToolPropertime")
BPsi4Pi.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsi4Pi.B.TupleToolTISTOS.TriggerList = triggerList
BPsi4Pi.B.TupleToolTISTOS.VerboseL0 = True
BPsi4Pi.B.TupleToolTISTOS.VerboseHlt1 = True
BPsi4Pi.B.TupleToolTISTOS.VerboseHlt2 = True
BPsi4Pi.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

BPsi4Pi.B.TupleToolConeIsolation.Verbose = True
BPsi4Pi.B.TupleToolConeIsolation.MinConeSize = 0.6
BPsi4Pi.B.TupleToolConeIsolation.MaxConeSize = 2.0
BPsi4Pi.B.TupleToolConeIsolation.SizeStep = 0.3

from Configurables import VertexIsolation

BPsi4Pi.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")

BPsi4Pi.B.TupleToolVertexI.ExtraName = "LongVtxVeto"

# Psi tools
BPsi4Pi.Psi.addTupleTool("TupleToolGeometry")
BPsi4Pi.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsi4Pi.Psi.TupleToolTISTOS.TriggerList = triggerList
BPsi4Pi.Psi.TupleToolTISTOS.VerboseL0 = True
BPsi4Pi.Psi.TupleToolTISTOS.VerboseHlt1 = True
BPsi4Pi.Psi.TupleToolTISTOS.VerboseHlt2 = True

# muplus tools
BPsi4Pi.muplus.addTupleTool("TupleToolGeometry")
BPsi4Pi.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsi4Pi.muplus.TupleToolTISTOS.TriggerList = triggerList
BPsi4Pi.muplus.TupleToolTISTOS.VerboseL0 = True
BPsi4Pi.muplus.TupleToolTISTOS.VerboseHlt1 = True
BPsi4Pi.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
BPsi4Pi.muminus.addTupleTool("TupleToolGeometry")
BPsi4Pi.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsi4Pi.muminus.TupleToolTISTOS.TriggerList = triggerList
BPsi4Pi.muminus.TupleToolTISTOS.VerboseL0 = True
BPsi4Pi.muminus.TupleToolTISTOS.VerboseHlt1 = True
BPsi4Pi.muminus.TupleToolTISTOS.VerboseHlt2 = True


# piplus tools
BPsi4Pi.piplus1.addTupleTool("TupleToolGeometry")
# piminus tools
BPsi4Pi.piminus1.addTupleTool("TupleToolGeometry")

BPsi4Pi.piplus2.addTupleTool("TupleToolGeometry")
# piminus tools
BPsi4Pi.piminus2.addTupleTool("TupleToolGeometry")


# same for B->Jpsi4pi

BJpsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
BJpsi4Pi.B.BDTF.constrainToOriginVertex = True
BJpsi4Pi.B.BDTF.daughtersToConstrain = ['psi(2S)']


BJpsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BdconstraintDTF")
BJpsi4Pi.B.BdconstraintDTF.constrainToOriginVertex = True


BJpsi4Pi.B.BdconstraintDTF.daughtersToConstrain = ['B0', 'psi(2S)']
BJpsi4Pi.B.BdconstraintDTF.Verbose = True
BJpsi4Pi.B.BdconstraintDTF.UpdateDaughters = True

BJpsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BsconstraintDTF")
BJpsi4Pi.B.BsconstraintDTF.constrainToOriginVertex = True
BJpsi4Pi.B.BsconstraintDTF.daughtersToConstrain = ['B_s0', 'psi(2S)']
BJpsi4Pi.B.BsconstraintDTF.Verbose = True
BJpsi4Pi.B.BsconstraintDTF.UpdateDaughters = True

BJpsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kplus_hypo")
BJpsi4Pi.B.BDTF_Kplus_hypo.constrainToOriginVertex = True
BJpsi4Pi.B.BDTF_Kplus_hypo.daughtersToConstrain = ['psi(2S)']

BJpsi4Pi.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_Kminus_hypo")
BJpsi4Pi.B.BDTF_Kminus_hypo.constrainToOriginVertex = True
BJpsi4Pi.B.BDTF_Kminus_hypo.daughtersToConstrain = ['psi(2S)']


BJpsi4Pi.Psi.addTupleTool("TupleToolDecayTreeFitter/PsiDTF")
BJpsi4Pi.Psi.PsiDTF.constrainToOriginVertex = False
BJpsi4Pi.Psi.PsiDTF.daughtersToConstrain = ['J/psi(1S)']


if(decay.startswith("131")):

    BJpsi4Pi.B.BdconstraintDTF.Substitutions = {
        'B_s0 ->   psi(2S) pi+ pi-': 'B0',
    }

    BJpsi4Pi.B.BDTF_Kplus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   ^pi+ Meson': 'K+',
    }
    BJpsi4Pi.B.BDTF_Kminus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson ^pi- ': 'K-',
    }

else:
    BJpsi4Pi.B.BsconstraintDTF.Substitutions = {
        'B0 ->  psi(2S) pi+ pi-': 'B_s0',
    }
    BJpsi4Pi.B.BDTF_Kplus_hypo.Substitutions = {
        'B0 -> psi(2S)   ^pi+ Meson': 'K+',
    }
    BJpsi4Pi.B.BDTF_Kminus_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson ^pi- ': 'K-',
    }

#################################
# same for Jpsi4pi


BJpsi4Pi.B.addTupleTool("TupleToolGeometry")
BJpsi4Pi.B.addTupleTool("TupleToolDira")
BJpsi4Pi.B.addTupleTool("TupleToolPropertime")
BJpsi4Pi.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BJpsi4Pi.B.TupleToolTISTOS.TriggerList = triggerList
BJpsi4Pi.B.TupleToolTISTOS.VerboseL0 = True
BJpsi4Pi.B.TupleToolTISTOS.VerboseHlt1 = True
BJpsi4Pi.B.TupleToolTISTOS.VerboseHlt2 = True
BJpsi4Pi.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

BJpsi4Pi.B.TupleToolConeIsolation.Verbose = True
BJpsi4Pi.B.TupleToolConeIsolation.MinConeSize = 0.6
BJpsi4Pi.B.TupleToolConeIsolation.MaxConeSize = 2.0
BJpsi4Pi.B.TupleToolConeIsolation.SizeStep = 0.3

from Configurables import VertexIsolation

BJpsi4Pi.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")

BJpsi4Pi.B.TupleToolVertexI.ExtraName = "LongVtxVeto"


# Psi tools
BJpsi4Pi.Psi.addTupleTool("TupleToolGeometry")
BJpsi4Pi.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BJpsi4Pi.Psi.TupleToolTISTOS.TriggerList = triggerList
BJpsi4Pi.Psi.TupleToolTISTOS.VerboseL0 = True
BJpsi4Pi.Psi.TupleToolTISTOS.VerboseHlt1 = True
BJpsi4Pi.Psi.TupleToolTISTOS.VerboseHlt2 = True
# JPsi tools
BJpsi4Pi.Jpsi.addTupleTool("TupleToolGeometry")
BJpsi4Pi.Jpsi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BJpsi4Pi.Jpsi.TupleToolTISTOS.TriggerList = triggerList
BJpsi4Pi.Jpsi.TupleToolTISTOS.VerboseL0 = True
BJpsi4Pi.Jpsi.TupleToolTISTOS.VerboseHlt1 = True
BJpsi4Pi.Jpsi.TupleToolTISTOS.VerboseHlt2 = True


# muplus tools
BJpsi4Pi.muplus.addTupleTool("TupleToolGeometry")
BJpsi4Pi.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BJpsi4Pi.muplus.TupleToolTISTOS.TriggerList = triggerList
BJpsi4Pi.muplus.TupleToolTISTOS.VerboseL0 = True
BJpsi4Pi.muplus.TupleToolTISTOS.VerboseHlt1 = True
BJpsi4Pi.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
BJpsi4Pi.muminus.addTupleTool("TupleToolGeometry")
BJpsi4Pi.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BJpsi4Pi.muminus.TupleToolTISTOS.TriggerList = triggerList
BJpsi4Pi.muminus.TupleToolTISTOS.VerboseL0 = True
BJpsi4Pi.muminus.TupleToolTISTOS.VerboseHlt1 = True
BJpsi4Pi.muminus.TupleToolTISTOS.VerboseHlt2 = True


# piplus tools
BJpsi4Pi.piplus.addTupleTool("TupleToolGeometry")
# piminus tools
BJpsi4Pi.piminus.addTupleTool("TupleToolGeometry")
BJpsi4Pi.piplus_psi.addTupleTool("TupleToolGeometry")
# piminus tools
BJpsi4Pi.piminus_psi.addTupleTool("TupleToolGeometry")


#############################################################
# same for PsiKK:

# same for Bs->Psi KK

BPsiKK.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
BPsiKK.B.BDTF.constrainToOriginVertex = True
BPsiKK.B.BDTF.daughtersToConstrain = ['psi(2S)']


BPsiKK.B.addTupleTool("TupleToolDecayTreeFitter/BdconstraintDTF")
BPsiKK.B.BdconstraintDTF.constrainToOriginVertex = True

BPsiKK.B.BdconstraintDTF.daughtersToConstrain = ['B0', 'psi(2S)']
BPsiKK.B.BdconstraintDTF.Verbose = True
BPsiKK.B.BdconstraintDTF.UpdateDaughters = True

BPsiKK.B.addTupleTool("TupleToolDecayTreeFitter/BsconstraintDTF")
BPsiKK.B.BsconstraintDTF.constrainToOriginVertex = True
BPsiKK.B.BsconstraintDTF.daughtersToConstrain = ['B_s0', 'psi(2S)']
BPsiKK.B.BsconstraintDTF.Verbose = True
BPsiKK.B.BsconstraintDTF.UpdateDaughters = True

BPsiKK.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_piplus_hypo")
BPsiKK.B.BDTF_piplus_hypo.constrainToOriginVertex = True
BPsiKK.B.BDTF_piplus_hypo.daughtersToConstrain = ['psi(2S)']

BPsiKK.B.addTupleTool("TupleToolDecayTreeFitter/BDTF_piminus_hypo")
BPsiKK.B.BDTF_piminus_hypo.constrainToOriginVertex = True
BPsiKK.B.BDTF_piminus_hypo.daughtersToConstrain = ['psi(2S)']
if(decay.startswith("131")):

    BPsiKK.B.BdconstraintDTF.Substitutions = {
        'B_s0 ->   psi(2S) K+ K-': 'B0',
    }

    BPsiKK.B.BDTF_piplus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   ^K+ Meson': 'pi+',
    }
    BPsiKK.B.BDTF_piminus_hypo.Substitutions = {
        'B_s0 -> psi(2S)   Meson ^K- ': 'pi-',
    }
else:
    BPsiKK.B.BsconstraintDTF.Substitutions = {
        'B0 ->  psi(2S) K+ K-': 'B_s0',
    }
    BPsiKK.B.BDTF_piplus_hypo.Substitutions = {
        'B0 -> psi(2S)   ^K+ Meson': 'pi+',
    }
    BPsiKK.B.BDTF_piminus_hypo.Substitutions = {
        'B0 -> psi(2S)   Meson ^K- ': 'pi-',
    }


BPsiKK.B.addTupleTool("TupleToolGeometry")
BPsiKK.B.addTupleTool("TupleToolDira")
BPsiKK.B.addTupleTool("TupleToolPropertime")
BPsiKK.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKK.B.TupleToolTISTOS.TriggerList = triggerList
BPsiKK.B.TupleToolTISTOS.VerboseL0 = True
BPsiKK.B.TupleToolTISTOS.VerboseHlt1 = True
BPsiKK.B.TupleToolTISTOS.VerboseHlt2 = True
BPsiKK.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

BPsiKK.B.TupleToolConeIsolation.Verbose = True
BPsiKK.B.TupleToolConeIsolation.MinConeSize = 0.6
BPsiKK.B.TupleToolConeIsolation.MaxConeSize = 2.0
BPsiKK.B.TupleToolConeIsolation.SizeStep = 0.3

from Configurables import VertexIsolation

BPsiKK.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")

BPsiKK.B.TupleToolVertexI.ExtraName = "LongVtxVeto"


# Psi tools
BPsiKK.Psi.addTupleTool("TupleToolGeometry")
BPsiKK.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKK.Psi.TupleToolTISTOS.TriggerList = triggerList
BPsiKK.Psi.TupleToolTISTOS.VerboseL0 = True
BPsiKK.Psi.TupleToolTISTOS.VerboseHlt1 = True
BPsiKK.Psi.TupleToolTISTOS.VerboseHlt2 = True

# muplus tools
BPsiKK.muplus.addTupleTool("TupleToolGeometry")
BPsiKK.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKK.muplus.TupleToolTISTOS.TriggerList = triggerList
BPsiKK.muplus.TupleToolTISTOS.VerboseL0 = True
BPsiKK.muplus.TupleToolTISTOS.VerboseHlt1 = True
BPsiKK.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
BPsiKK.muminus.addTupleTool("TupleToolGeometry")
BPsiKK.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
BPsiKK.muminus.TupleToolTISTOS.TriggerList = triggerList
BPsiKK.muminus.TupleToolTISTOS.VerboseL0 = True
BPsiKK.muminus.TupleToolTISTOS.VerboseHlt1 = True
BPsiKK.muminus.TupleToolTISTOS.VerboseHlt2 = True


# piplus tools
BPsiKK.Kplus.addTupleTool("TupleToolGeometry")
# piminus tools
BPsiKK.Kminus.addTupleTool("TupleToolGeometry")

BPsiPiPi.LoKiTool.Variables = {
    "eta": "ETA",
    "phi": "PHI",
    "BPVDLS": "BPVDLS"
}
BPsiKPi.LoKiTool.Variables = BPsiPiPi.LoKiTool.Variables
BJpsi4Pi.LoKiTool.Variables = BPsiPiPi.LoKiTool.Variables
BPsi4Pi.LoKiTool.Variables = BPsiPiPi.LoKiTool.Variables
BPsiKK.LoKiTool.Variables = BPsiPiPi.LoKiTool.Variables


#Add LoKi Variables to single Branches (decay angles etc)
PiPiB_hybrid = BPsiPiPi.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
PiPiPsi_hybrid = BPsiPiPi.Psi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")

KPiB_hybrid = BPsiKPi.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
KPiPsi_hybrid = BPsiKPi.Psi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")

fourPiB_hybrid = BPsi4Pi.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
fourPiPsi_hybrid = BPsi4Pi.Psi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")

KKB_hybrid = BPsiKK.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")
KKPsi_hybrid = BPsiKK.Psi.addTupleTool("LoKi::Hybrid::TupleTool/LoKiTool")

AngleVariable ={
    "LV01": "LV01",
}

PiPiB_hybrid.Variables = AngleVariable
PiPiPsi_hybrid.Variables = AngleVariable
KPiB_hybrid.Variables = AngleVariable
KPiPsi_hybrid.Variables = AngleVariable
fourPiB_hybrid.Variables = AngleVariable
fourPiPsi_hybrid.Variables = AngleVariable
KKB_hybrid.Variables = AngleVariable
KKPsi_hybrid.Variables = AngleVariable

"""
BJpsi4Pi.B.ToolList = BPsiPiPi.B.ToolList
BJpsi4Pi.Psi.ToolList = BPsiPiPi.Psi.ToolList
BJpsi4Pi.piplus.ToolList = BPsiPiPi.piplus.ToolList
BJpsi4Pi.piminus.ToolList = BPsiPiPi.piminus.ToolList
BJpsi4Pi.piplus_psi.ToolList = BPsiPiPi.piplus.ToolList
BJpsi4Pi.piminus_psi.ToolList = BPsiPiPi.piminus.ToolList
"""

# samesign is currently not used...
"""
samesignbg.addBranches({
                    "B":"^(B0 -> (psi(2S) -> mu- mu+)  pi+ pi+)",
                    "Psi":"B0 -> ^(psi(2S) -> mu- mu+) pi+ pi+",
                    "muplus":"B0 -> (psi(2S) -> mu- ^mu+)  pi+ pi+",
                    "muminus":"B0 -> (psi(2S) -> ^mu- mu+) pi+ pi+",
                    "piplus":"B0 -> (psi(2S) -> mu- mu+) ^pi+ pi+",
                    "piminus":"B0 -> (psi(2S) -> mu- mu+) pi+ ^pi+",
                    "pipi":"B0 -> (psi(2S) -> mu- mu+) ^(rho(770)0 -> pi+ pi-)"
                    "Psipiplus":"B0 -> ^((psi(2S) -> mu- mu+ pi+)  pi-",
                    "Psipiminus":"B0 -> ^((psi(2S) -> mu- mu+ pi-) pi+"
                    })

samesignbg.B.addTupleTool("TupleToolDecayTreeFitter/BDTF")
samesignbg.B.BDTF.constrainToOriginVertex = True
samesignbg.B.BDTF.daughtersToConstrain = ['psi(2S)']
samesignbg.B.addTupleTool("TupleToolGeometry")
samesignbg.B.addTupleTool("TupleToolDira")
samesignbg.B.addTupleTool("TupleToolPropertime")
samesignbg.B.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
samesignbg.B.TupleToolTISTOS.TriggerList = triggerList
samesignbg.B.TupleToolTISTOS.VerboseL0 = True
samesignbg.B.TupleToolTISTOS.VerboseHlt1 = True
samesignbg.B.TupleToolTISTOS.VerboseHlt2 = True
samesignbg.B.addTupleTool("TupleToolConeIsolation/TupleToolConeIsolation")

samesignbg.B.TupleToolConeIsolation.Verbose = True
samesignbg.B.TupleToolConeIsolation.MinConeSize = 0.6
samesignbg.B.TupleToolConeIsolation.MaxConeSize = 2.0
samesignbg.B.TupleToolConeIsolation.SizeStep = 0.3


samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexI")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexII")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexIII")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexIV")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexV")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexVI")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexVII")
samesignbg.B.addTupleTool("TupleToolVtxIsoln/TupleToolVertexVIII")
samesignbg.B.TupleToolVertexI.ExtraName     = "LongVtxVeto"
samesignbg.B.TupleToolVertexII.ExtraName    = "LongVtxVetoOnlyIPCut4"
samesignbg.B.TupleToolVertexIII.ExtraName   = "LongVtxVetoOnlyIPCut16"
samesignbg.B.TupleToolVertexIV.ExtraName    = "LongVtxVetoOnlyPTCut250"
samesignbg.B.TupleToolVertexV.ExtraName     = "LongVtxVetoOnlyPTCut500"
samesignbg.B.TupleToolVertexVI.ExtraName    = "LongVtxVetoIPCut4PTCut500"
samesignbg.B.TupleToolVertexVII.ExtraName   = "LongVtxVetoIPCut16PTCut250"
samesignbg.B.TupleToolVertexVIII.ExtraName  = "LongVtxVetoIPCut16PTCut500"


samesignbg.B.TupleToolVertexII.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexII.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsOnlyIPCut4"]
samesignbg.B.TupleToolVertexIII.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexIII.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsOnlyIPCut16"]
samesignbg.B.TupleToolVertexIV.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexIV.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsOnlyPTCut250"]
samesignbg.B.TupleToolVertexV.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexV.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsOnlyPTCut500"]
samesignbg.B.TupleToolVertexVI.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexVI.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsIPCut4PTCut500"]
samesignbg.B.TupleToolVertexVII.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexVII.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsIPCut16PTCut250"]
samesignbg.B.TupleToolVertexVIII.addTool(VertexIsolation("VertexIsolation"))
samesignbg.B.TupleToolVertexVIII.VertexIsolation.InputParticles =
["/Event/Phys/StdAllNoPIDsPionsIPCut16PTCut500"]



### Psi tools
samesignbg.Psi.addTupleTool("TupleToolGeometry")
samesignbg.Psi.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
samesignbg.Psi.TupleToolTISTOS.TriggerList = triggerList
samesignbg.Psi.TupleToolTISTOS.VerboseL0 = True
samesignbg.Psi.TupleToolTISTOS.VerboseHlt1 = True
samesignbg.Psi.TupleToolTISTOS.VerboseHlt2 = True



# muplus tools
samesignbg.muplus.addTupleTool("TupleToolGeometry")
samesignbg.muplus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
samesignbg.muplus.TupleToolTISTOS.TriggerList = triggerList
samesignbg.muplus.TupleToolTISTOS.VerboseL0 = True
samesignbg.muplus.TupleToolTISTOS.VerboseHlt1 = True
samesignbg.muplus.TupleToolTISTOS.VerboseHlt2 = True

# muminus tools
samesignbg.muminus.addTupleTool("TupleToolGeometry")
samesignbg.muminus.addTupleTool("TupleToolTISTOS/TupleToolTISTOS")
samesignbg.muminus.TupleToolTISTOS.TriggerList = triggerList
samesignbg.muminus.TupleToolTISTOS.VerboseL0 = True
samesignbg.muminus.TupleToolTISTOS.VerboseHlt1 = True
samesignbg.muminus.TupleToolTISTOS.VerboseHlt2 = True


#rho0 tools
#samesignbg.pipi.addTupleTool("TupleToolGeometry")
# piplus tools
samesignbg.piplus.addTupleTool("TupleToolGeometry")
# piminus tools
samesignbg.piminus.addTupleTool("TupleToolGeometry")
"""

################################################################
################################################################
Seq1 = GaudiSequencer("Seq1")
# commented this out for testing with psi->jpsipipi
"""
if("pipi" in daughters):
    Seq1.Members.append(BPsiPiPiSeq.sequence())
    Seq1.Members.append(BPsiPiPi)
    Seq1.Members.append(BPsi4PiSeq.sequence())
    Seq1.Members.append(BPsi4Pi)
if("jpsi" in daughters):
    Seq1.Members.append(BJpsi4PiSeq.sequence())
    Seq1.Members.append(BJpsi4Pi)
if("KK" in daughters):
    Seq1.Members.append(BPsiKKSeq.sequence())
    Seq1.Members.append(BPsiKK)
#Seq1.Members.append(samesignbg.sequence())
#Seq1.Members.append(samesignbg)
"""
Seq1.Members.append(BPsiPiPiSeq.sequence())
Seq1.Members.append(BPsiPiPi)
Seq1.Members.append(BPsiKPiSeq.sequence())
Seq1.Members.append(BPsiKPi)
Seq1.Members.append(BPsi4PiSeq.sequence())
Seq1.Members.append(BPsi4Pi)
Seq1.Members.append(BJpsi4PiSeq.sequence())
Seq1.Members.append(BJpsi4Pi)
Seq1.Members.append(BPsiKKSeq.sequence())
Seq1.Members.append(BPsiKK)

Seq1.ModeOR = True
Seq1.ShortCircuit = False
# ######################################################################################################
DaVinci().MainOptions = ""
if restrip:
    DaVinci().UserAlgorithms = [eventNodeKiller, sc.sequence(),
                                selSeq.sequence(), Seq1]
else:
    DaVinci().UserAlgorithms = [Seq1]
    # ######################################################################################################
    if("jpsi" in daughters):
        stripFilter = LoKi_Filters(
            STRIP_Code="HLT_PASS_RE ('StrippingFullDSTDiMuonJpsi2"
            "MuMuDetachedLineDecision')  | HLT_PASS_RE ('StrippingFull"
            "DSTDiMuonPsi2MuMuDetachedLineDecision')  "
        )
        stripFilterSeq = stripFilter.sequence("StripFilter")
    else:
        stripFilter = LoKi_Filters(
            STRIP_Code=" HLT_PASS_RE ('StrippingFullDST"
                       "DiMuonPsi2MuMuDetachedLineDecision') "
        )
        stripFilterSeq = stripFilter.sequence("StripFilter")

    # #######################################################################
#DaVinci().PrintFreq = 1000
#DaVinci().InputType = "DST"


# ####################################################
# # local files for testing go here
# ####################################################
# from Gaudi.Configuration import *
# from GaudiConf import IOHelper
# DaVinci().EvtMax = 1000

# if(simulation):
#     IOHelper('ROOT').inputFiles(['00027535_00000001_1.allstreams.dst'])
#     DaVinci().TupleFile = "BPsipipiTestsim" + str(DaVinci().EvtMax) + ".root"
# else:
#     DaVinci().TupleFile = "BPsipipiTestdata" + str(DaVinci().EvtMax) + ".root"

#     IOHelper('ROOT').inputFiles([
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000234_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000007_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000021_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000041_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000042_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000063_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000077_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000091_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000105_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000119_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000133_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000147_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000161_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000175_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000197_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000198_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000219_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000249_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000263_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000277_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000292_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000307_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000322_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000336_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000357_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000358_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000380_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000395_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000409_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000423_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000438_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000459_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000460_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000482_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000498_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000512_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000530_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000544_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000558_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000572_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000591_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000606_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000627_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000645_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000660_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000683_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000702_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000718_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000734_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000750_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000766_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000780_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000795_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000810_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000827_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000842_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000856_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000880_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000894_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000908_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000925_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000942_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000956_1'
#         '.dimuon.dst',
#         '/auto/data/grabowski/B2Psipipi/tuples/data/2011/00041840_00000971_1'
#         '.dimuon.dst'

#     ])
# DaVinci().CondDBtag = "sim-20130522-vc-md100"
# DaVinci().DDDBtag = "dddb-20130929"

# DaVinci().DataType = '2011'
# DaVinci().Simulation = True
# DaVinci().Lumi = False


# if(not simulation):
#     DaVinci().CondDBtag = "cond-20141107"
#     DaVinci().DataType = '2011'
#     DaVinci().Simulation = False
#     DaVinci().Lumi = True
