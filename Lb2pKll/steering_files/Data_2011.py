import os
os.environ['ISRUN2']              = "False"
os.environ['ISMC']                = "False"
os.environ['YEAR']                = '2011'
os.environ['INPUTTYPE']           = 'MDST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "False"
os.environ['MAGPOS']              = 'MU'
os.environ['RESTRIP']             = "False"
os.environ['DOMUON']              = "True"
os.environ['DOELECTRON']          = "True"
