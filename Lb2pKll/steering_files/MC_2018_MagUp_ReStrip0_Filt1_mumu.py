import os
os.environ['ISRUN2']              = "True"
os.environ['ISMC']                = "True"
os.environ['YEAR']                = '2018'
os.environ['INPUTTYPE']           = 'DST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "True"
os.environ['MAGPOS']              = 'MU'
os.environ['RESTRIP']             = "False"
os.environ['DOMUON']              = "True"
os.environ['DOELECTRON']          = "False"
