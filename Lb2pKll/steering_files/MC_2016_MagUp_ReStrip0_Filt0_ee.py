import os
os.environ['ISRUN2']              = "True"
os.environ['ISMC']                = "True"
os.environ['YEAR']                = '2016'
os.environ['INPUTTYPE']           = 'DST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "False"
os.environ['MAGPOS']              = 'MU'
os.environ['RESTRIP']             = "False"
os.environ['DOMUON']              = "False"
os.environ['DOELECTRON']          = "True"
