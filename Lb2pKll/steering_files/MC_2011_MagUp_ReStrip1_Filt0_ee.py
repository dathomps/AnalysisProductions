import os
os.environ['ISRUN2']              = "False"
os.environ['ISMC']                = "True"
os.environ['YEAR']                = '2011'
os.environ['INPUTTYPE']           = 'DST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "False"
os.environ['MAGPOS']              = 'MU'
os.environ['RESTRIP']             = "True"
os.environ['DOMUON']              = "False"
os.environ['DOELECTRON']          = "True"
