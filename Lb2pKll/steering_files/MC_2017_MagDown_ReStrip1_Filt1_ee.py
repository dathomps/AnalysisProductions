import os
os.environ['ISRUN2']              = "True"
os.environ['ISMC']                = "True"
os.environ['YEAR']                = '2017'
os.environ['INPUTTYPE']           = 'DST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "True"
os.environ['MAGPOS']              = 'MD'
os.environ['RESTRIP']             = "True"
os.environ['DOMUON']              = "False"
os.environ['DOELECTRON']          = "True"
