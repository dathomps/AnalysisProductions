import os
os.environ['ISRUN2']              = "False"
os.environ['ISMC']                = "True"
os.environ['YEAR']                = '2012'
os.environ['INPUTTYPE']           = 'DST'
os.environ['ISREDECAY']           = "False"
os.environ['ISFILTERED']          = "True"
os.environ['MAGPOS']              = 'MD'
os.environ['RESTRIP']             = "True"
os.environ['DOMUON']              = "True"
os.environ['DOELECTRON']          = "False"
