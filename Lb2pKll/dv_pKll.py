# References:
# https://twiki.cern.ch/twiki/pub/LHCbPhysics/Lst2pkll/LHCb-ANA-2019-007_v3.1.pdf
# https://gitlab.cern.ch/LHCb-RD/ewp-rpk-fullr1r2/-/blob/master/DaVinci/LeptonU-options-NOPID.py
# https://gitlab.cern.ch/LHCb-RD/ewp-rks-rkstplus/-/blob/master/Produce_Ntuples/Data/Bu2LLK_DaVinci.py
# https://gitlab.cern.ch/LHCb-RD/ewp-bd2ksteeangular-legacy/-/blob/master/samples_and_selection/DaVinci_scripts/options_Bu2LLK_EE.py

import sys
sys.path.append("./")
#from steering_files.MC_2018_MagDown_ReStrip1_Filt0_mumu import * # BD localtest
#from steering_files.MC_2012_MagUp_ReStrip0_Filt0_mumu import * # BD localtest
#from steering_files.MC_2012_MagDown_ReStrip1_Filt0_mumu import * # BD localtest
#from steering_files.Data_2017 import * # BD localtest

import os
isRun2      = (os.getenv("ISRUN2", 'False') == 'True')     
isMC        = (os.getenv("ISMC", 'False') == 'True') 
year        = os.getenv("YEAR")   
inputType   = os.getenv("INPUTTYPE")  
isReDecay   = (os.getenv("ISREDECAY", 'False') == 'True')  
isfiltered  = (os.getenv("ISFILTERED", 'False') == 'True')  
magpos      = os.getenv("MAGPOS")    
restrip     = (os.getenv("RESTRIP", 'False') == 'True') 
doMuon      = (os.getenv("DOMUON", 'False') == 'True')  
doElectron  = (os.getenv("DOELECTRON", 'False') == 'True') 


from Lb2pKll.helpers.helper import *
#from helpers.helper import *  # BD localtest
from LHCbKernel.Configuration import *
from Configurables import DaVinci, TupleToolDecayTreeFitter, DecayTreeTuple, TupleToolDecay, TupleToolTrigger, TupleToolRecoStats, TupleToolTISTOS, TupleToolGeometry, TupleToolTrackInfo, TupleToolMCTruth, TupleToolL0Data, SubstitutePID
if isReDecay: from Configurables import MCTupleToolRedecay

from Configurables import DstConf, CaloDstUnPackConf, LoKi__Hybrid__TupleTool
from Configurables import EventNodeKiller, ProcStatusCheck
from Configurables import StrippingReport, TimingAuditor, SequencerTimerTool

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *

from Configurables import CombineParticles
from Configurables import TupleToolTrigger, SubstitutePID
from Configurables import FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MergedSelection

##################################################################
# Initialize some settings independent of LHCb tools
evtmax         =  -1 # change this for localtest
###################################################################

# RpK
linename_e_Run1   = "Bu2LLK_eeLine2"
linename_mu_Run1  = "Bu2LLK_mmLine"
# new lines
linename_e_Run2   = "Bu2LLK_ee_Lb2pkeeLooseLine"
linename_mu_Run2  = "Bu2LLK_mm_Lb2pkmmLooseLine"

#######################################################################

# applied on the B head
trig_Run1_B_mu    = ['L0GlobalDecision', 'Hlt1TrackAllL0Decision', 'Hlt2Topo2BodyBBDTDecision', 'Hlt2Topo3BodyBBDTDecision', 'Hlt2Topo4BodyBBDTDecision', 'Hlt2TopoMu2BodyBBDTDecision', 'Hlt2TopoMu3BodyBBDTDecision', 'Hlt2TopoMu4BodyBBDTDecision']
trig_Run2_B_mu    = ['L0GlobalDecision', 'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1TrackMuonMVADecision',  'Hlt1TrackMuonDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1DiMuonHighMassDecision', 'Hlt2Topo2BodyDecision','Hlt2Topo3BodyDecision','Hlt2Topo4BodyDecision','Hlt2TopoMu2BodyDecision','Hlt2TopoMu3BodyDecision','Hlt2TopoMu4BodyDecision','Hlt2TopoMuMu2BodyDecision','Hlt2TopoMuMu3BodyDecision','Hlt2TopoMuMu4BodyDecision','Hlt2DiMuonDetachedDecision','Hlt2SingleMuonDecision']
trig_Run1_B_e     = ['L0GlobalDecision', 'Hlt1TrackAllL0Decision', 'Hlt1TrackAllL0TightDecision','Hlt2Topo2BodyBBDTDecision', 'Hlt2Topo3BodyBBDTDecision', 'Hlt2Topo4BodyBBDTDecision', 'Hlt2TopoE2BodyBBDTDecision', 'Hlt2TopoE3BodyBBDTDecision', 'Hlt2TopoE4BodyBBDTDecision']
trig_Run2_B_e     = ['L0GlobalDecision', 'Hlt1TrackMVADecision', 'Hlt2Topo2BodyDecision', 'Hlt2Topo3BodyDecision', 'Hlt2Topo4BodyDecision', 'Hlt2TopoE2BodyDecision', 'Hlt2TopoE3BodyDecision', 'Hlt2TopoE4BodyDecision']

# some Hlt1/2 muon lines for the dimuon
trig_Run1_Jpsi_mu = ["L0MuonDecision","L0DiMuonDecision", "Hlt1DiMuonLowMassDecision","Hlt1DiMuonHighMassDecision","Hlt1TrackMuonDecision","Hlt2SingleMuonDecision","Hlt2DiMuonDetachedDecision"]
trig_Run2_Jpsi_mu = ["L0MuonDecision","L0DiMuonDecision", "Hlt1TrackMuonMVADecision","Hlt1TrackMuonDecision","Hlt1SingleMuonHighPTDecision","Hlt1DiMuonLowMassDecision","Hlt1DiMuonHighMassDecision","Hlt2DiMuonDetachedDecision","Hlt2SingleMuonDecision"]
trig_e            = ["L0ElectronDecision","L0ElectronHiDecision", "L0PhotonDecision", "L0PhotonHiDecision", "Hlt1SingleElectronNoIPDecision"]

# applied on the pK
trig_Lst   = [ 'L0HadronDecision']

def addNtupleToDaVinci(name, isRun2, isMC, isDST):
    trig_B = trig_Run1_B_mu
    trig_Jpsi = trig_Run1_Jpsi_mu
    line   = linename_mu_Run1
    if "ee" in name and isRun2 == 0: 
       trig_B = trig_Run1_B_e 
       line   = linename_e_Run1
    elif "mm" in name and isRun2 == 1:
       trig_B = trig_Run2_B_mu 
       trig_Jpsi = trig_Run2_Jpsi_mu
       line   = linename_mu_Run2
    elif "ee" in name and isRun2 == 1:
       trig_B = trig_Run2_B_e 
       line   = linename_e_Run2
    theTuple = DecayTreeTuple(name)
    if "ee" in name:
       trig_Jpsi = trig_e
       theTuple.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC"
       theTuple.addBranches ({
         "Lb"  : "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "L1"  : "[Lambda_b0->  (J/psi(1S) -> ^e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "L2"  : "[Lambda_b0->  (J/psi(1S) -> e+ ^e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "Jpsi": "[Lambda_b0->  ^(J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "p"   : "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> ^p+  K-) ]CC",
         "k"   : "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  ^K-) ]CC",
         "Lst" : "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ^( Lambda(1520)0 -> p+  K-)]CC"
       })
    else:
        theTuple.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC" 
        theTuple.addBranches ({
         "Lb"  : "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "L1"  : "[Lambda_b0->  (J/psi(1S) -> ^mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "L2"  : "[Lambda_b0->  (J/psi(1S) -> mu+ ^mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "Jpsi": "[Lambda_b0->  ^(J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
         "p"   : "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> ^p+  K-) ]CC",
         "k"   : "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  ^K-) ]CC",
         "Lst" : "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ^( Lambda(1520)0 -> p+  K-)]CC"
        })
    if isMC and restrip: 
        if year == "2011" : 
              strip = "stripping21r1p2"
        elif year == "2012" : 
              strip = "stripping21r0p2"
        elif year == "2016" :
              strip = "stripping28r2p1"
        elif year == "2017" :
              strip = "stripping29r2p2"
        elif year == "2018" :
              strip = "stripping34r0p2"
        # The event node killer should be run before
        event_node_killer = EventNodeKiller("StripKiller")
        event_node_killer.Nodes = ["/Event/AllStreams", "/Event/Strip"]

        DaVinci().appendToMainSequence( [ event_node_killer ] )
        #print(year, strip)
        streams = buildStreams(stripping=strippingConfiguration(strip), archive=strippingArchive(strip))

        custom_stream = StrippingStream("CustomStream")
        custom_line = "Stripping" + line
        print("line: ", line, " custom_line ", custom_line)

        for stream in streams:
           for line2 in stream.lines:
              if line2.name() == custom_line:
                custom_stream.appendLines([line2])
                print("line2 name during strip lines loop:", line2.name())

        # Create the actual Stripping configurable
        filterBadEvents = ProcStatusCheck()
        sc = StrippingConf(Streams=[custom_stream], MaxCandidates=2000, AcceptBadEvents=False, BadEventSelection=filterBadEvents)
        # The output is placed directly into Phys, so we only need to

        sr = StrippingReport(Selections = sc.selections())
        DaVinci().appendToMainSequence( [ sc.sequence() ] )

    relinfo_prefix = ""
    if isMC:
       if restrip:
          theTuple.Inputs = ["/Event/Phys/{}/Particles".format(line)]
          relinfo_prefix = "/Event/Phys/{}/".format(line)
       elif isfiltered: # filtering script
          #stream = "Bu2KLL_NoPID.Strip" 
          #line_noPID = line.replace("Bu2LLK", "Bu2LLKNoPID")
          #theTuple.Inputs = ['/Event/{}/Phys/{}/Particles'.format(stream,line_noPID)]
          stream = "Leptonic.Strip" 
          theTuple.Inputs = ['/Event/{}/Phys/{}/Particles'.format(stream,line)]
          relinfo_prefix = '/Event/{}/Phys/{}/'.format(stream,line)
       else: #flagged MC
          theTuple.Inputs = ['/Event/AllStreams/Phys/{}/Particles'.format(line)]
          relinfo_prefix = '/Event/AllStreams/Phys/{}/'.format(line)
    else:
       # local tests fail with this from RpK
       #DaVinci().RootInTES = "/Event/Leptonic/"  
       #theInput = "Phys/" + line + "/Particles"     
       theInput = "/Event/Leptonic/Phys/" + line + "/Particles"     
       theTuple.Inputs = [ theInput ]
       relinfo_prefix = '/Event/Leptonic/Phys/{}/'.format(line)
    print("relinfo prefix is %s"%relinfo_prefix)

    theTuple.ReFitPVs = True 
    theTuple.ToolList = ["TupleToolRecoStats","TupleToolEventInfo","TupleToolGeometry","TupleToolTrackInfo","TupleToolMuonPid","TupleToolTrackPosition",]
    # Tools for particle combinations
    theTuple.Lb.ToolList   = ["TupleToolKinematic"]
    theTuple.Jpsi.ToolList = ["TupleToolKinematic"]
    theTuple.Lst.ToolList  = ["TupleToolKinematic"]

    # Tools for the final tracks
    theTuple.L1.ToolList    =  ["TupleToolKinematic","TupleToolANNPID"] 
    tt_L1_pid = theTuple.L1.addTupleTool('TupleToolPid')    
    tt_L1_pid.Verbose = True

    theTuple.L2.ToolList    =  ["TupleToolKinematic","TupleToolANNPID"] 
    tt_L2_pid = theTuple.L2.addTupleTool('TupleToolPid')    
    tt_L2_pid.Verbose = True

    theTuple.p.ToolList    =  ["TupleToolKinematic","TupleToolANNPID"] 
    tt_p_pid = theTuple.p.addTupleTool('TupleToolPid')    
    tt_p_pid.Verbose = True

    theTuple.k.ToolList    =  ["TupleToolKinematic","TupleToolANNPID"] 
    tt_k_pid = theTuple.k.addTupleTool('TupleToolPid')    
    tt_k_pid.Verbose = True
   
    theTuple.addTool(TupleToolGeometry,name = 'TupleToolGeometry') 
    theTuple.TupleToolGeometry.RefitPVs = True
    if isMC: 
         theTuple.ToolList += [ "TupleToolMCBackgroundInfo"] 
         MCTruth=theTuple.addTupleTool("TupleToolMCTruth")
         MCTruth.addTupleTool("MCTupleToolHierarchy")
         if isReDecay: theTuple.ToolList += ["MCTupleToolRedecay"]

    LoKiVariables = LoKi__Hybrid__TupleTool('LoKiVariables')
    LoKiVariables.Variables = {
                                "ETA"        : "ETA",
                                "LOKI_PHI"    : "PHI",
                                "inMuon"      : "switch(INMUON, 1, 0)"
                              }

    theTuple.addTool(LoKiVariables , name = "LoKiVariables" )
    theTuple.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables"]

    # TupleToolTISTOS
    theTuple.Lb.addTool(TupleToolTISTOS)
    theTuple.Lb.TupleToolTISTOS.Verbose = True     
    theTuple.Lb.TupleToolTISTOS.VerboseL0 = True
    theTuple.Lb.TupleToolTISTOS.VerboseHlt1 = True
    theTuple.Lb.TupleToolTISTOS.VerboseHlt2 = True
    theTuple.Lb.TupleToolTISTOS.FillL0 = True
    theTuple.Lb.TupleToolTISTOS.FillHlt1 = True
    theTuple.Lb.TupleToolTISTOS.FillHlt2 = True
    theTuple.Lb.TupleToolTISTOS.OutputLevel = INFO
    theTuple.Lb.TupleToolTISTOS.TriggerList = trig_B     
    theTuple.Lb.ToolList +=["TupleToolTISTOS"]     

    theTuple.Lst.addTool(TupleToolTISTOS)
    theTuple.Lst.TupleToolTISTOS.Verbose = True     
    theTuple.Lst.TupleToolTISTOS.TriggerList = trig_Lst     
    theTuple.Lst.ToolList += ["TupleToolTISTOS"]     

    theTuple.Jpsi.addTool(TupleToolTISTOS)
    theTuple.Jpsi.TupleToolTISTOS.Verbose = True     
    theTuple.Jpsi.TupleToolTISTOS.TriggerList = trig_Jpsi     
    theTuple.Jpsi.ToolList +=["TupleToolTISTOS"]     

    theTuple.ToolList +=["TupleToolL0Data"]     

    # Brem stuff ---------------- 
    if "ee" in name:

         theTuple.addTupleTool('TupleToolHOP')
         theTuple.TupleToolHOP.Verbose=True

         theTuple.addTupleTool('TupleToolBremInfo')
         theTuple.TupleToolBremInfo.Verbose=True

         theTuple.addTupleTool('TupleToolPhotonInfo')
         theTuple.TupleToolPhotonInfo.Verbose=True

         # track variables: including track momentum before brem recovery and some calo-track matching info
         LoKiCaloVars = LoKi__Hybrid__TupleTool("LoKiCaloVars")
         LoKiCaloVars.Preambulo += ['from LoKiTracks.decorators import *']

         LoKiCaloVars.Variables = {
             "TRACK_PX" : "TRFUN(TrPX)",
             "TRACK_PY" : "TRFUN(TrPY)",
             "TRACK_PZ" : "TRFUN(TrPZ)",
             "TrP"      : "TRFUN(TrP)",
             "TrQ"      : "TRFUN(TrQ)",

             "LoKi_InAccBrem" : "PPINFO( LHCb.ProtoParticle.InAccBrem, -1 )",

             "CaloTrMatch"       : "PPINFO( LHCb.ProtoParticle.CaloTrMatch,       -999 )",
             "CaloElectronMatch" : "PPINFO( LHCb.ProtoParticle.CaloElectronMatch, -999 )",
             "CaloBremMatch"     : "PPINFO( LHCb.ProtoParticle.CaloBremMatch,     -999 )"
         }
         
         theTuple.L1.addTool(LoKiCaloVars , name="LoKiCaloVars" )
         theTuple.L1.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiCaloVars"]
         theTuple.L2.addTool(LoKiCaloVars , name="LoKiCaloVars" )
         theTuple.L2.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiCaloVars"]
       
         theTuple.L1.addTupleTool( 'TupleToolL0Calo', name = "L1L0ECalo" )
         theTuple.L1.ToolList += [ "TupleToolL0Calo/L1L0ECalo" ]
         theTuple.L1.L1L0ECalo.WhichCalo = "ECAL"
         theTuple.L2.addTupleTool( 'TupleToolL0Calo', name = "L2L0ECalo" )
         theTuple.L2.ToolList += [ "TupleToolL0Calo/L2L0ECalo" ]
         theTuple.L2.L2L0ECalo.WhichCalo = "ECAL"

         theTuple.L1.addTupleTool( 'TupleToolL0Calo', name = "L1L0HCalo" )
         theTuple.L1.ToolList += [ "TupleToolL0Calo/L1L0HCalo" ]
         theTuple.L1.L1L0HCalo.WhichCalo = "HCAL"
         theTuple.L2.addTupleTool( 'TupleToolL0Calo', name = "L2L0HCalo" )
         theTuple.L2.ToolList += [ "TupleToolL0Calo/L2L0HCalo" ]
         theTuple.L2.L2L0HCalo.WhichCalo = "HCAL"

         theTuple.k.addTupleTool( 'TupleToolL0Calo', name = "KaonL0Calo" )
         theTuple.k.ToolList += [ "TupleToolL0Calo/KaonL0Calo" ]
         theTuple.k.KaonL0Calo.WhichCalo = "HCAL"
         theTuple.p.addTupleTool( 'TupleToolL0Calo', name = "ProtonL0Calo" )
         theTuple.p.ToolList += [ "TupleToolL0Calo/ProtonL0Calo" ]
         theTuple.p.ProtonL0Calo.WhichCalo = "HCAL"

    #if not isDST: # isolations 
    LoKi_Isols =  LoKi__Hybrid__TupleTool("LoKi_Isols")
    LoKi_Isols.Variables = fillIsoVars(relinfo_prefix,isRun2)    
    theTuple.Lb.addTool(LoKi_Isols , name = "LoKi_Isols" )
    theTuple.Lb.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKi_Isols"]

    ##### mass hypothesis substitutions, to study misID backgrounds #####
    theTuple.Lb.addTupleTool( 'TupleToolSubMass' )
    theTuple.Lb.ToolList += [ "TupleToolSubMass" ]
  
    ## common for mm and ee modes:  
    theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => pi+"]+["K+ => p+"]
    theTuple.Lb.TupleToolSubMass.Substitution += ["p+ => K+"]+["p+ => pi+"]

    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/K-"]+["K-/p+ => pi+/K-"]+["K-/p+ => p+/pi-"]+["K-/p+ => pi+/pi-"]+["K-/p+ => mu+/mu-"]+["K-/p+ => e+/e-"]

    ##involving specific leptons:
    if "ee" in name: #electrons
         theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => e+"]+["p+ => e+"]
         theTuple.Lb.TupleToolSubMass.Substitution += ["e+ => K+"]+["e+ => pi+"]+["e+ => p+"]+["e+ => mu+"]
   
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["e+/e- => pi+/pi-"]+["e+/e- => K+/K-"]+["e+/e- => K+/pi-"]+["e+/e- => pi+/K-"]+["e+/e- => mu+/mu-"]
    
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/e+ => e+/K+"]+["K+/e+ => pi+/pi+"]+["K+/e+ => p+/p+"]
  
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/e+ => e+/p+"]+["p+/e+ => pi+/pi+"]+["p+/e+ => K+/K+"]+["p+/e+ => K+/pi+"]
  
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/e- => e+/K-"]+["K+/e- => pi+/pi-"]+["K+/e- => p+/p~-"]+["K+/e- => pi+/K-"]+["K+/e- => pi+/p~-"]+["K+/e- => p+/pi-"]+["K+/e- => p+/K-"]+["K+/e- => mu+/mu-"]
    
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/e- => e+/p~-"]+["p+/e- => K+/K-"]+["p+/e- => pi+/pi-"]+["p+/e- => pi+/p~-"]+["p+/e- => K+/pi-"]+["p+/e- => K+/p~-"]+["p+/e- => pi+/K-"]+["p+/e- => mu+/mu-"]
    else: #muons
         theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => mu+"]+["p+ => mu+"]
         theTuple.Lb.TupleToolSubMass.Substitution += ["mu+ => K+"]+["mu+ => pi+"]+["mu+ => p+"]+["mu+ => e+"]

         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["mu+/mu- => pi+/pi-"]+["mu+/mu- => K+/K-"]+["mu+/mu- => K+/pi-"]+["mu+/mu- => pi+/K-"]+["mu+/mu- => e+/e-"]
    
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/mu+ => mu+/K+"]+["K+/mu+ => pi+/pi+"]+["K+/mu+ => p+/p+"]
  
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/mu+ => mu+/p+"]+["p+/mu+ => pi+/pi+"]+["p+/mu+ => K+/K+"]+["p+/mu+ => K+/pi+"]
 
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/mu- => mu+/K-"]+["K+/mu- => pi+/pi-"]+["K+/mu- => p+/p~-"]+["K+/mu- => pi+/K-"]+["K+/mu- => pi+/p~-"]+["K+/mu- => p+/pi-"]+["K+/mu- => p+/K-"]+["K+/mu- => e+/e-"] 
    
         theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/mu- => mu+/p~-"]+["p+/mu- => pi+/pi-"]+["p+/mu- => pi+/p~-"]+["p+/mu- => K+/pi-"]+["p+/mu- => K+/p~-"]+["p+/mu- => pi+/K-"]+["p+/mu- => e+/e-"]


    # the signal modes
    # --- no PV constraint
    #theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_NoPV"]
    #dtfSigB_NoPV = TupleToolDecayTreeFitter("SigB_NoPV",
    #                                        Verbose=True ,
    #                                        UpdateDaughters=True
    #                                       )
    #theTuple.Lb.addTool(dtfSigB_NoPV)


    #theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_noPV_Jpsi"]
    #dtfSigB_noPV_Jpsi = TupleToolDecayTreeFitter("SigB_noPV_Jpsi",
    #                                        Verbose=True ,
    #                                        UpdateDaughters=True,
    #                                        daughtersToConstrain = [ "J/psi(1S)" ] 
    #                                       )
    #theTuple.Lb.addTool(dtfSigB_noPV_Jpsi)

    #theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_noPV_psi2S"]
    #dtfSigB_noPV_psi2S = TupleToolDecayTreeFitter("SigB_noPV_psi2S",
    #                                        Verbose=True ,
    #                                        UpdateDaughters=True,
    #                                        Substitutions = { "[ [Lambda_b0]cc -> ^J/psi(1S) Lambda(1520)0 ]CC" : "psi(2S)" },
    #                                        daughtersToConstrain = [ "psi(2S)" ] 
    #                                       )
    #theTuple.Lb.addTool(dtfSigB_noPV_psi2S)

    # --- with PV constraint (nominal)
    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_PV"]
    dtfSigB_PV = TupleToolDecayTreeFitter("SigB_PV",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            UpdateDaughters=True
                                           )
    theTuple.Lb.addTool(dtfSigB_PV)

    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_PV_Lb_mc"]
    dtfSigB_PV_Lb_mc = TupleToolDecayTreeFitter("SigB_PV_Lb_mc",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            UpdateDaughters=True,
                                            daughtersToConstrain = [ "Lambda_b0" ] 
                                           )
    theTuple.Lb.addTool(dtfSigB_PV_Lb_mc)


    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_PV_Jpsi"]
    dtfSigB_PV_Jpsi = TupleToolDecayTreeFitter("SigB_PV_Jpsi",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            UpdateDaughters=True,
                                            daughtersToConstrain = [ "J/psi(1S)" ] 
                                           )
    theTuple.Lb.addTool(dtfSigB_PV_Jpsi)


    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_PV_Jpsi_Lb_mc"]
    dtfSigB_PV_Jpsi_Lb_mc = TupleToolDecayTreeFitter("SigB_PV_Jpsi_Lb_mc",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            UpdateDaughters=True,
                                            daughtersToConstrain = [ "J/psi(1S)", "Lambda_b0" ] 
                                           )
    theTuple.Lb.addTool(dtfSigB_PV_Jpsi_Lb_mc)


    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SigB_PV_psi2S"]
    dtfSigB_PV_psi2S = TupleToolDecayTreeFitter("SigB_PV_psi2S",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            UpdateDaughters=True,
                                            Substitutions = { "Lambda_b0 -> ^J/psi(1S) Lambda(1520)0" : "psi(2S)",
                                                              "Lambda_b~0 -> ^J/psi(1S) Lambda(1520)~0" : "psi(2S)" 
                                                            },
                                            daughtersToConstrain = [ "psi(2S)" ] 
                                           )
    theTuple.Lb.addTool(dtfSigB_PV_psi2S)


    """
    # the B0 -> K pi l+ l- bkgd 
    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SubBd_PV"]
    dtfSubBd_PV = TupleToolDecayTreeFitter("SubBd_PV",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            Substitutions = { "Lambda_b0 -> J/psi(1S)  (Lambda(1520)0 -> ^p+ X-)"  : "pi+",
                                                              "Lambda_b~0 -> J/psi(1S)    (Lambda(1520)~0 -> ^p~- X+)" : "pi-" 
                                                            }
                                           )
    theTuple.Lb.addTool(dtfSubBd_PV)

    # the B0s -> K K l+ l- bkgd 
    theTuple.Lb.ToolList +=  ["TupleToolDecayTreeFitter/SubBs_PV"]
    dtfSubBs_PV = TupleToolDecayTreeFitter("SubBs_PV",
                                            Verbose=True ,
                                            constrainToOriginVertex=True ,
                                            Substitutions = { "Lambda_b0 -> J/psi(1S)   (Lambda(1520)0 -> ^p+ X-)"  : "K+",
                                                              "Lambda_b~0 -> J/psi(1S)  (Lambda(1520)~0 -> ^p~- X+)" : "K-" 
                                                            }
                                           )
    theTuple.Lb.addTool(dtfSubBs_PV)
    """


    return theTuple


###################################################
# automatically_configure
# Use the CondDB to get the right database tags for data
#from Configurables import CondDB
#if not isMC: CondDB(LatestGlobalTagByDataType = year )
###################################################

dv = DaVinci(
     #InputType     = inputType, #automatically_configure # BD localtest
     #DataType      =  year  , # automatically_configure # BD localtest
     EvtMax        =  evtmax  , # -1 for all files
     TupleFile     = 'DTT.root',
     PrintFreq     = 100000,
     Simulation    = isMC,
     Lumi          = False,
)

#MessageSvc().OutputLevel = DEBUG

"""
# Matt Needham(?)'s momentum scale calibration for data
from Configurables import TrackScaleState
if not isMC:
   scaler            = TrackScaleState('scaler')
   #scaler.Input = "/Event/Leptonic/Rec/Track/Best"
   dv.UserAlgorithms =  [ scaler ]
"""

# smear proto particles and their associated tracks for MC
# https://gitlab.cern.ch/LHCb-RD/ewp-bd2ksteeangular-legacy/-/blob/master/samples_and_selection/DaVinci_scripts/options_Bu2LLK_EE.py#L121
#from Configurables import TrackSmearState
# removed 10th march 2022
#if isMC:
#   smear = TrackSmearState("TrackSmearState")
#   dv.UserAlgorithms =  [ smear ]

# scaler/smear must be executed before the analysis algorithms
isDST = True
if inputType == "MDST": isDST = False

if doMuon:
   #print(isRun2, isMC, isDST) # BD localtest
   Lbtuple_mm = addNtupleToDaVinci("Lb2pkmm", isRun2, isMC, isDST)
   dv.UserAlgorithms += [Lbtuple_mm]

if doElectron: 
   #print("doElectron: ",doElectron) # BD localtest
   Lbtuple_ee = addNtupleToDaVinci("Lb2pkee", isRun2, isMC, isDST)
   dv.UserAlgorithms += [Lbtuple_ee]

#evt+std://LHCb/Collision12/90000000/Beam4000GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping21r0p2/LEPTONIC.MDST
#import glob
#flist    = glob.glob("/eos/lhcb/grid/prod//lhcb/LHCb/Collision12/LEPTONIC.MDST/00095380/0000/0*.leptonic.mdst")
#dv.Input = flist[:1]

# evt+std://LHCb/Collision17/90000000/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2p2/LEPTONIC.MDST 
#dv.Input = ["/eos/lhcb/grid/prod//lhcb/LHCb/Collision17/LEPTONIC.MDST/00141012/0000/00141012_00000060_1.leptonic.mdst"]
#flist    = glob.glob("/eos/lhcb/grid/prod//lhcb/LHCb/Collision17/LEPTONIC.MDST/00141012/0000/0*.leptonic.mdst")
#dv.Input = flist[:1]

#MC_15144001_Lb_JpsipKmmphspDecProdCut_ReStrip
# evt+std://MC/2018/15144001/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/ALLSTREAMS.DST
#dv.Input = ["/eos/lhcb/grid/prod//lhcb/MC/2018/ALLSTREAMS.DST/00086923/0000/00086923_00000406_7.AllStreams.dst"]

# MC_15114401_Lb_pKstmmKpi0phspDecProdCut
# evt+std://MC/2012/15114401/Beam4000GeV-2012-MagUp-Nu2.5-Pythia8/Sim09k/Trig0x409f0045/Reco14c/Stripping21r0p2NoPrescalingFlagged/ALLSTREAMS.DST
#dv.Input = ["/eos/lhcb/grid/prod//lhcb/MC/2012/ALLSTREAMS.DST/00149395/0000/00149395_00000110_5.AllStreams.dst"]

#MC_15144059_Lb_JpsipKmmXLL2_ReStrip
# evt+std://MC/2012/15144059/Beam4000GeV-2012-MagDown-Nu2.5-Pythia8/Sim09h/Trig0x409f0045/Reco14c/Stripping21NoPrescalingFlagged/ALLSTREAMS.DST
#dv.Input = ["/eos/lhcb/grid/prod//lhcb//MC/2012/ALLSTREAMS.DST/00095494/0000/00095494_00000001_5.AllStreams.dst"]




