def fillIsoVars(line, isRun2):
    lineQ = "'%s"%line
    print("RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEPT', -999.)")
    var_dict = {
    # https://gitlab.cern.ch/LHCb-RD/ewp-rpk/-/blob/master/DaVinci/LeptonU-options-NOPID.py#L207
                 #track
 "L1_cpt_0.50": "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEPT', -999.)",
 "L2_cpt_0.50": "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEPT', -999.)",
 "Proton_cpt_0.50": "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEPT', -999.)",
 "Kaon_cpt_0.50": "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEPT', -999.)",

 "L1_cp_0.50": "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEP', -999.)",
 "L2_cp_0.50": "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEP', -999.)",
 "Proton_cp_0.50": "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEP', -999.)",
 "Kaon_cp_0.50": "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEP', -999.)",

 "L1_cmult_0.50": "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEMULT', -1.)",
 "L2_cmult_0.50": "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEMULT', -1.)",
 "Proton_cmult_0.50": "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEMULT', -1.)",
 "Kaon_cmult_0.50": "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEMULT', -1.)",

 "L1_ptasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEPTASYM', -999.)",
 "L2_ptasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEPTASYM', -999.)",
 "Proton_ptasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEPTASYM', -999.)",
 "Kaon_ptasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEPTASYM', -999.)",

 "L1_pasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEPASYM', -999.)",
 "L2_pasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEPASYM', -999.)",
 "Proton_pasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEPASYM', -999.)",
 "Kaon_pasy_0.50": "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEPASYM', -999.)",

 "L1_deltaEta_0.50":  "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEDELTAETA', -999.)",
 "L2_deltaEta_0.50":  "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEDELTAETA', -999.)",
 "Proton_deltaEta_0.50":  "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEDELTAETA', -999.)",
 "Kaon_deltaEta_0.50":  "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEDELTAETA', -999.)",

 "L1_deltaPhi_0.50":  "RELINFO("+lineQ+"TrackIsoInfoL1', 'CONEDELTAPHI', -999.)",
 "L2_deltaPhi_0.50":  "RELINFO("+lineQ+"TrackIsoInfoL2', 'CONEDELTAPHI', -999.)",
 "Proton_deltaPhi_0.50":  "RELINFO("+lineQ+"TrackIsoInfoH1', 'CONEDELTAPHI', -999.)",
 "Kaon_deltaPhi_0.50":  "RELINFO("+lineQ+"TrackIsoInfoH2', 'CONEDELTAPHI', -999.)",

                 #cone
 "L1_0.50_cc_mult": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_MULT', -1.)",
 "L2_0.50_cc_mult": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_MULT', -1.)",
 "Proton_0.50_cc_mult": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_MULT', -1.)",
 "Kaon_0.50_cc_mult": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_MULT', -1.)",

 "L1_0.50_cc_sPT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_SPT', -1.)",
 "L2_0.50_cc_sPT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_SPT', -1.)",
 "Proton_0.50_cc_sPT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_SPT', -1.)",
 "Kaon_0.50_cc_sPT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_SPT', -1.)",

 "L1_0.50_cc_vPT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_VPT', -1.)",
 "L2_0.50_cc_vPT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_VPT', -1.)",
 "Proton_0.50_cc_vPT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_VPT', -1.)",
 "Kaon_0.50_cc_vPT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_VPT', -1.)",

 "L1_0.50_cc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_PASYM', -1.)",
 "L2_0.50_cc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_PASYM', -1.)",
 "Proton_0.50_cc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_PASYM', -1.)",
 "Kaon_0.50_cc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_PASYM', -1.)",

 "L1_0.50_cc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_PTASYM', -1.)",
 "L2_0.50_cc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_PTASYM', -1.)",
 "Proton_0.50_cc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_PTASYM', -1.)",
 "Kaon_0.50_cc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_PTASYM', -1.)",

 "L1_0.50_cc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_DELTAETA', -1.)",
 "L2_0.50_cc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_DELTAETA', -1.)",
 "Proton_0.50_cc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_DELTAETA', -1.)",
 "Kaon_0.50_cc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_DELTAETA', -1.)",

 "L1_0.50_cc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_DELTAPHI', -1.)",
 "L2_0.50_cc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_DELTAPHI', -1.)",
 "Proton_0.50_cc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_DELTAPHI', -1.)",
 "Kaon_0.50_cc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_DELTAPHI', -1.)",

 "L1_0.50_cc_IT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_IT', -1.)",
 "L2_0.50_cc_IT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_IT', -1.)",
 "Proton_0.50_cc_IT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_IT', -1.)",
 "Kaon_0.50_cc_IT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_IT', -1.)",

 "L1_0.50_cc_maxPt_Q": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_MAXPT_Q', -1.)",
 "L2_0.50_cc_maxPt_Q": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_MAXPT_Q', -1.)",
 "Proton_0.50_cc_maxPt_Q": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_MAXPT_Q', -1.)",
 "Kaon_0.50_cc_maxPt_Q": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_MAXPT_Q', -1.)",

 "L1_0.50_cc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_MAXPT_PT', -1.)",
 "L2_0.50_cc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_MAXPT_PT', -1.)",
 "Proton_0.50_cc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_MAXPT_PT', -1.)",
 "Kaon_0.50_cc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_MAXPT_PT', -1.)",

 "L1_0.50_cc_maxPt_PE": "RELINFO("+lineQ+"ConeIsoInfoL1', 'CC_MAXPT_PE', -1.)",
 "L2_0.50_cc_maxPt_PE": "RELINFO("+lineQ+"ConeIsoInfoL2', 'CC_MAXPT_PE', -1.)",
 "Proton_0.50_cc_maxPt_PE": "RELINFO("+lineQ+"ConeIsoInfoH1', 'CC_MAXPT_PE', -1.)",
 "Kaon_0.50_cc_maxPt_PE": "RELINFO("+lineQ+"ConeIsoInfoH2', 'CC_MAXPT_PE', -1.)",

                 #cone
 "L1_0.50_nc_mult": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_MULT', -1.)",
 "L2_0.50_nc_mult": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_MULT', -1.)",
 "Proton_0.50_nc_mult": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_MULT', -1.)",
 "Kaon_0.50_nc_mult": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_MULT', -1.)",

 "L1_0.50_nc_sPT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_SPT', -1.)",
 "L2_0.50_nc_sPT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_SPT', -1.)",
 "Proton_0.50_nc_sPT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_SPT', -1.)",
 "Kaon_0.50_nc_sPT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_SPT', -1.)",


 "L1_0.50_nc_vPT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_VPT', -1.)",
 "L2_0.50_nc_vPT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_VPT', -1.)",
 "Proton_0.50_nc_vPT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_VPT', -1.)",
 "Kaon_0.50_nc_vPT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_VPT', -1.)",

 "L1_0.50_nc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_PASYM', -1.)",
 "L2_0.50_nc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_PASYM', -1.)",
 "Proton_0.50_nc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_PASYM', -1.)",
 "Kaon_0.50_nc_asy_P": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_PASYM', -1.)",

 "L1_0.50_nc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_PTASYM', -1.)",
 "L2_0.50_nc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_PTASYM', -1.)",
 "Proton_0.50_nc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_PTASYM', -1.)",
 "Kaon_0.50_nc_asy_PT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_PTASYM', -1.)",

 "L1_0.50_nc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_DELTAETA', -1.)",
 "L2_0.50_nc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_DELTAETA', -1.)",
 "Proton_0.50_nc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_DELTAETA', -1.)",
 "Kaon_0.50_nc_deltaEta": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_DELTAETA', -1.)",

 "L1_0.50_nc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_DELTAPHI', -1.)",
 "L2_0.50_nc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_DELTAPHI', -1.)",
 "Proton_0.50_nc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_DELTAPHI', -1.)",
 "Kaon_0.50_nc_deltaPhi": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_DELTAPHI', -1.)",

 "L1_0.50_nc_IT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_IT', -1.)",
 "L2_0.50_nc_IT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_IT', -1.)",
 "Proton_0.50_nc_IT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_IT', -1.)",
 "Kaon_0.50_nc_IT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_IT', -1.)",

 "L1_0.50_nc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoL1', 'NC_MAXPT_PT', -1.)",
 "L2_0.50_nc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoL2', 'NC_MAXPT_PT', -1.)",
 "Proton_0.50_nc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoH1', 'NC_MAXPT_PT', -1.)",
 "Kaon_0.50_nc_maxPt_PT": "RELINFO("+lineQ+"ConeIsoInfoH2', 'NC_MAXPT_PT', -1.)",

###vertex variable
 "NumVtxWithinChi2WindowOneTrack":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISONUMVTX', -999.)",
 "SmallestDeltaChi2OneTrack":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISODCHI2ONETRACK', -999.)",
 "SmallestDeltaChi2MassOneTrack":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISODCHI2MASSONETRACK', -999.)",
 "SmallestDeltaChi2TwoTracks":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISODCHI2TWOTRACK', -999.)",
 "SmallestDeltaChi2MassTwoTracks":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISODCHI2MASSTWOTRACK', -999.)",
 "NumVtxWithinChi2WindowOneTrack":  "RELINFO("+lineQ+"VertexIsoInfo', 'VTXISONUMVTX', -999.)",
               }

    var_dict_R1 = {
#  new Bs2MM vars for leptons
 "TRKISOBDTLONG_12":  "RELINFO("+lineQ+"TrackIsolationBDT2', 'TRKISOBDTLONG', -999.)",
 "TRKISOBDTVELO_12":  "RELINFO("+lineQ+"TrackIsolationBDT2', 'TRKISOBDTVELO', -999.)",
    }

    var_dict_R2 = {
#  new Bs2MM vars for all combinations
 "TRKISOBDTLONG_12":  "RELINFO("+lineQ+"TrackIsolationBDT2_12', 'TRKISOBDTLONG', -999.)",
 "TRKISOBDTVELO_12":  "RELINFO("+lineQ+"TrackIsolationBDT2_12', 'TRKISOBDTVELO', -999.)",
 "TRKISOBDTLONG_14":  "RELINFO("+lineQ+"TrackIsolationBDT2_14', 'TRKISOBDTLONG', -999.)",
 "TRKISOBDTVELO_14":  "RELINFO("+lineQ+"TrackIsolationBDT2_14', 'TRKISOBDTVELO', -999.)",
 "TRKISOBDTLONG_25":  "RELINFO("+lineQ+"TrackIsolationBDT2_25', 'TRKISOBDTLONG', -999.)",
 "TRKISOBDTVELO_25":  "RELINFO("+lineQ+"TrackIsolationBDT2_25', 'TRKISOBDTVELO', -999.)",
 "TRKISOBDTLONG_45":  "RELINFO("+lineQ+"TrackIsolationBDT2_45', 'TRKISOBDTLONG', -999.)",
 "TRKISOBDTVELO_45":  "RELINFO("+lineQ+"TrackIsolationBDT2_45', 'TRKISOBDTVELO', -999.)",
               }

    if not isRun2: var_dict.update(var_dict_R1)
    if isRun2: var_dict.update(var_dict_R2)
    
    return var_dict


