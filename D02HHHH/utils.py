from Configurables import LoKi__Hybrid__TupleTool, TupleToolVtxDaughters, \
    TupleToolANNPID


# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---


def SetupToolsPrompt(dtt, TriggerLists, Probnn, IsTurbo, IsMC):
    '''Configure DecayTreeTuple tools for prompt lines.'''
    # list of general tools
    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger",
                    "Kinematic", "TrackInfo", "Propertime", "Pid",
                    "RecoStats"]
    dtt.ToolList = ['TupleTool'+tool for tool in GeneralTools]
    # Setup MC tools if neeeded
    if IsMC:
        SetupMCTools(dtt, IsTurbo)

    # Adding additional LoKi tools for variables not included in the standard
    # tuple tools for the D0 only
    loki_vars = {
        'Loki_BPVVDCHI2': 'BPVVDCHI2',
        'Loki_BPVIPCHI2': 'BPVIPCHI2()',
        'Loki_DOCAMAX': 'DOCAMAX',
        'Loki_AMAXDOCA': "LoKi.Particles.PFunA(AMAXDOCA(''))",
        'Loki_AMINDOCA': "LoKi.Particles.PFunA(AMINDOCA(''))",
        'Loki_DOCACHI2MAX': 'DOCACHI2MAX',
        'Loki_VCHI2NDOF': 'VFASPF(VCHI2/VDOF)',
        'Loki_VX': 'VFASPF(VX)',
        'Loki_VY': 'VFASPF(VY)',
        'Loki_VZ': 'VFASPF(VZ)',
        'Loki_SUMPT': 'SUMTREE(PT,  ISBASIC)',
        'Loki_BPVLTIME': "BPVLTIME()"
    }
    # only want the additional LoKi branches for the D* and D0
    # DTF Refit info
    Dst_Node = dtt.allConfigurables['%s.%s' % (dtt.name(), 'Dst')]
    fit = Dst_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose = True
    fit.constrainToOriginVertex = True
    fit.UpdateDaughters = True
    # Creating LoKi TupleTool
    loki_D0_tool = LoKi__Hybrid__TupleTool('{0}LoKiTT'.format('D0'))
    loki_D0_tool.Variables = loki_vars

    # DTF Mass-Constrained D0 fit
    fit_D0_MC = Dst_Node.addTupleTool('TupleToolDecayTreeFitter/D0Fit')
    fit_D0_MC.Verbose = True
    fit_D0_MC.constrainToOriginVertex = True
    fit_D0_MC.daughtersToConstrain += ['D0']
    fit_D0_MC.UpdateDaughters = True
    # Setup the long-lived particles branches
    IntermediateTools = ['Geometry', 'Kinematic', 'Propertime', 'VtxDaughters']
    # IntermediateTools = [ 'Geometry', 'Kinematic', 'Propertime' ]
    for vtx in ['D0']:
        Vtx = dtt.allConfigurables['%s.%s' % (dtt.name(), vtx)]
        Vtx.InheritTools = False
        Vtx.ToolList = ["TupleTool"+name for name in IntermediateTools]
        Vtx.ToolList += ['LoKi::Hybrid::TupleTool/{0}LoKiTT'.format('D0')]
        Vtx.addTool(loki_D0_tool)
        # Sub-vertex information
        Vtx.addTool(TupleToolVtxDaughters())
        Vtx.TupleToolVtxDaughters.FillSubVtxInfo = True
        Vtx.TupleToolVtxDaughters.DoThreeProng = True
        # Monte Carlo
        if IsMC:
            Vtx.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            Vtx.ToolList += ["TupleToolMCTruth"]
            SetupMCTools(Vtx, IsTurbo)

    # Setup the stable particles branches
    TrackTools = ['Geometry', 'Kinematic', 'Pid', 'TrackInfo']
    for trk in ['D0_P0', 'D0_P1', 'D0_P2', 'D0_P3', 'Dst_pi' ]:
        branch = dtt.allConfigurables['%s.%s' % (dtt.name(), trk)]
        # P info
        branch.InheritTools = False
        branch.ToolList = ["TupleTool"+name for name in TrackTools]
        if len(Probnn):
            branch.ToolList += ["TupleToolANNPID"]
            branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
            branch.TupleToolANNPID.ANNPIDTunes = Probnn
        if IsMC:
            branch.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            branch.ToolList += ["TupleToolMCTruth", 'TupleToolL0Calo']
            SetupMCTools(branch, IsTurbo)
    # TISTOS
    from Configurables import TupleToolTISTOS
    for nodeName, tList in TriggerLists.iteritems():
        Node = dtt.allConfigurables['%s.%s' % (dtt.name(), nodeName)]
        Node.ToolList += ["TupleToolTISTOS"]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS")
        Node.TupleToolTISTOS.Verbose = True
        Node.TupleToolTISTOS.TriggerList = tList
        if IsTurbo:
            Node.TupleToolTISTOS.FillHlt2 = False


# Tuple Maker utils
def SetupMCTools(dtt, IsTurbo):
    dtt.ToolList += [ "TupleToolMCTruth", "TupleToolMCBackgroundInfo" ]
    mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic'] #, "MCTupleToolHierarchy"]
    if IsTurbo:
        ## Set MC truth for Turbo
        from TeslaTools import TeslaTruthUtils
        relations = TeslaTruthUtils.getRelLocs() + [
        TeslaTruthUtils.getRelLoc(''),
        # Location of the truth tables for PersistReco objects
        'Relations/Hlt2/Protos/Charged'
        ]
        #relations = [TeslaTruthUtils.getRelLoc('')]
        #relations.append('/Event/Turbo/Relations/Hlt2/Protos/Charged')
        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)
    else:
        from Configurables import TupleToolMCTruth
        MCTruth = TupleToolMCTruth()
        MCTruth.ToolList =  mc_tools
        dtt.addTool(MCTruth)
    return

# Tuple for true MC (MCDecayTreeTuple)
def TrueMCTuple(name, decay, IsTurbo, data_type):
    """Create an MCDecayTree with full MC informations"""
    from Configurables import MCDecayTreeTuple
    mc_tpl = MCDecayTreeTuple("MCDecayTreeTuple_"+name)
    mc_tpl.ToolList = [
        "MCTupleToolKinematic",         #kinematic info
        "MCTupleToolHierarchy",         #generated informations about parent and grand-parent
        "MCTupleToolPID",               #pid info
        ]
    mc_tpl.Decay = decay
    if IsTurbo and data_type=='2015': mc_tpl.RootInTES = '/Event/AllStreams'
    return mc_tpl
