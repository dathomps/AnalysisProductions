from Configurables import CombineParticles
from Configurables import TrackSelector
from DecayTreeTuple.Configuration import DecayTreeTuple
from Configurables import FilterDesktop
from Configurables import CheckPV
from Configurables import NoPIDsParticleMaker
from Configurables import GaudiSequencer
from D02HHHH.utils import SetupToolsPrompt
from D02HHHH.dictionaries import PromptTriggerLists, decay_descriptors, \
    PromptHLT2Lines_Run2
from Configurables import DaVinci

CombineParticles('D0Topiminuspipluspiminuspiplus_MCSel',
                 Inputs = ['Phys/piminus_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D0 ==> pi- pi+ pi- pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D0 -> pi- pi+ pi- pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch'],
                 Output = 'Phys/D0Topiminuspipluspiminuspiplus_MCSel/Particles')

FilterDesktop('piminus_MCSel',
              Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch'],
              Inputs = ['Phys/REBUILD:StdAllNoPIDsPions/Particles'],
              Code = "mcMatch('[pi-]CC')",
              Output = 'Phys/piminus_MCSel/Particles')

CheckPV('CheckPV')

TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector',
              TrackTypes = ['Long'],
              MinChi2Cut = 0.0,
              MaxCloneDistCut = 9e+99,
              MaxChi2Cut = 5.0,
              AcceptClones = False,
              MinCloneDistCut = 5000.0)

NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions',
                    Output = 'Phys/REBUILD:StdAllNoPIDsPions/Particles',
                    Particle = 'pion')
NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions').addTool(TrackSelector('REBUILD:StdAllNoPIDsPions.TrackSelector'))

DecayTreeTuple('Dstp2D0Pip_D02PimPimPipPip_MCUnbiasedTuple',
               Inputs = ['Phys/Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCSel/Particles'],
               Output = 'Phys/Dstp2D0Pip_D02PimPimPipPip_MCUnbiasedTuple/Particles',
               Decay = '[D*(2010)+ -> ^(D0 -> ^pi- ^pi+ ^pi- ^pi+) ^pi+]CC')

CombineParticles('Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCSel',
                 Inputs = ['Phys/D0Topiminuspipluspiminuspiplus_MCSel/Particles', 'Phys/piminus_MCSel/Particles'],
                 ReFitPVs = True,
                 MotherCut = "mcMatch('[D*(2010)+ ==> (D0 ==> pi- pi+ pi- pi+) pi+]CC') & BPVVALID()",
                 DecayDescriptors = ['[D*(2010)+ -> D0 pi+]cc'],
                 Preambulo = ['from LoKiPhysMC.decorators import *', 'from LoKiPhysMC.functions import mcMatch'],
                 Output = 'Phys/Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCSel/Particles')

GaudiSequencer('Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCUnbiasedSeq',
               Members = [CheckPV('CheckPV'), NoPIDsParticleMaker('REBUILD:StdAllNoPIDsPions'), FilterDesktop('piminus_MCSel'), CombineParticles('D0Topiminuspipluspiminuspiplus_MCSel'), CombineParticles('Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCSel'), DecayTreeTuple('Dstp2D0Pip_D02PimPimPipPip_MCUnbiasedTuple')])

seq = GaudiSequencer('Dstar_2010_plusTo_D0Topiminuspipluspiminuspiplus_piplus_MCUnbiasedSeq')

dtt = DecayTreeTuple('Dstp2D0Pip_D02PimPimPipPip_MCUnbiasedTuple')
dtt.setDescriptorTemplate(decay_descriptors['PimPimPipPip'])

triggerlists = dict(PromptTriggerLists[DaVinci().DataType])
triggerlists['Dst'] += [line + 'Decision'
                        for line in PromptHLT2Lines_Run2.values()]

SetupToolsPrompt(dtt, triggerlists, [], False, True)
DaVinci().UserAlgorithms.append(seq)
