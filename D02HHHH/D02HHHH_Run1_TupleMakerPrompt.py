"""Configure DaVinci for creating ntuples from Turbo data."""
from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
from D02HHHH.dictionaries import decay_descriptors, decay_descriptors_trueMC,\
    PromptTriggerLists, PromptHLT2Lines_Run2
from D02HHHH.utils import MomentumCorrection, SetupToolsPrompt

########################################
# User configuration begins
########################################

data_type = DaVinci().DataType
# data or MC?
IsMC = DaVinci().Simulation

# Probnn tunings
Probnn = ["MC15TuneV1"] if data_type == '2015' else [] #uncomment if using DV < v40r0 or new tuning becomes available

########################################
# User configuration ends
########################################

dv = DaVinci()

# DecayTreeTuple input location template
stripping_input = 'Phys/DstarPromptWithD02HHHHLine/Particles'

if IsMC:
    # MDST MC
    if dv.InputType == 'MDST':
        dv.RootInTES = '/Event/AllStreams'
    # Full DST MC
    else:
        stripping_input = 'AllStreams/' + stripping_input
# Real data
else:
    dv.RootInTES = '/Event/Charm'

# Define some trigger lines to check for TISTOS
TriggerLists = PromptTriggerLists[data_type]


def CreateDTT(lname, IsMC=False):
    decay_desc = decay_descriptors[lname]
    IsTurbo = False
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(lname))
    dtt.Inputs = [stripping_input]
    dtt.setDescriptorTemplate(decay_desc)
    SetupToolsPrompt(dtt, TriggerLists, Probnn, IsTurbo, IsMC)
    # Finishing
    return dtt


# Create a tuple for each line
ntuples = [CreateDTT(lname,IsMC) for lname in PromptHLT2Lines_Run2]
dv.UserAlgorithms = [MomentumCorrection(IsMC)]+ntuples

# Speed up processing by filtering out events with no positive signal decisions
filters = LoKi_Filters(STRIP_Code = 'HLT_PASS("StrippingDstarPromptWithD02HHHHLineDecision")')
dv.EventPreFilters = filters.filters('Stripping2Filters')
