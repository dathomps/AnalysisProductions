#"""Configure DaVinci for creating ntuples from Turbo data."""
import os

from Configurables import DaVinci, DecayTreeTuple
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
# from LoKiPhys.decorators import *
from D02HHHH.dictionaries import decay_descriptors, decay_descriptors_trueMC,\
    PromptTriggerLists, PromptHLT2Lines_Run2
from D02HHHH.utils import TrueMCTuple, MomentumCorrection, SetupToolsPrompt

########################################
# User configuration begins
########################################

# # Data-taking year
# data_type = '2017'
data_type = DaVinci().DataType
# # data or MC?
IsMC=DaVinci().Simulation
# MC event type
# passing this value with os.environ (or DaVinci().TupleFile) is kind of hacky
# hopefully there should be a better way to pass values into/between options files in run 3 DaVinci
evtId = os.environ['EVENTTYPE']
if IsMC and data_type=='2015':
    genOrReco = os.environ['GENORRECO']
else:
    genOrReco = None

# Stream the line belongs to (ignored for 2016 data)
turbo_stream = 'Charmmultibody'

# Probnn tunings
Probnn = ["MC15TuneV1"] if data_type == '2015' else []# uncomment if using DV < v40r0 or new tuning becomes available

########################################
# User configuration ends
########################################

if data_type in ['2015','2016'] or IsMC:
    turbo_stream = ''
if IsMC and data_type=='2015' and genOrReco=='GEN':
    root_in_tes = os.path.join('/Event', turbo_stream, 'AllStreams')
else:
    root_in_tes = os.path.join('/Event', turbo_stream, 'Turbo')
#if IsMC: root_in_tes = '/Event/AllStreams' # dtt is created without it

# DecayTreeTuple input location template
turbo_input = '{0}/Particles'
turbo_input_mc = '/Event/Turbo/{0}/Particles'

TriggerLists = PromptTriggerLists[data_type]


def CreateDTT(lname, IsMC=False):
    hlt2_line_name = PromptHLT2Lines_Run2[lname]
    decay_desc = decay_descriptors[lname]
    IsTurbo = 'Turbo' in hlt2_line_name
    name = hlt2_line_name.replace('CharmHad', '').replace('Turbo', '')
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(name))
    dtt.Inputs = [turbo_input.format(hlt2_line_name)]
    if data_type in ['2015','2016']: dtt.InputPrimaryVertices = '/Event/Turbo/Primary'
    dtt.setDescriptorTemplate(decay_desc)
    SetupToolsPrompt(dtt, TriggerLists, Probnn, IsTurbo, IsMC)
    # Finishing
    return dtt


# Create a tuple for each line
# temporary fix: in the case of 2015 MC, choose whether to run on generated or reconstructed
if IsMC and data_type=='2015' and genOrReco:
    if genOrReco=='GEN':
        ntuples = [TrueMCTuple(evtId,decay_descriptors_trueMC[evtId],True,data_type)]
    elif genOrReco=='RECO':
        ntuples = [CreateDTT(lname,IsMC) for lname in PromptHLT2Lines_Run2]
else:
    ntuples = [CreateDTT(lname,IsMC) for lname in PromptHLT2Lines_Run2]
    if IsMC: ntuples+= [TrueMCTuple(evtId,decay_descriptors_trueMC[evtId],True,data_type)]

# Speed up processing by filtering out events with no positive signal decisions
trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('Hlt2CharmHadDstp2D0Pip_D02({0})TurboDecision')"
    .format('|'.join(PromptHLT2Lines_Run2))
)

dv = DaVinci()
dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = [MomentumCorrection(IsMC)]+ntuples
dv.RootInTES = root_in_tes
