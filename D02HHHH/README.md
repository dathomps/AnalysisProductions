# D02HHHH ntuple options

Options files for creating ntuples for e.g. $`D^{0} \to K \pi \pi \pi`$ decays.

## Notes on 2015 MC
Due to Turbo stream setup in 2015 the Truth and reconstructed MC are stored in separate nTuples.

## Monte Carlo samples

* 27165070: $`D^{*} \to D^{0} (\to K \pi \pi \pi) \pi`$ (Phase space model)
* 27165071: $`D^{*} \to D^{0} (\to K^- \pi^+ \pi^- \pi^+) \pi`$ (AmpGen CF model)
* 27165072: $`D^{*} \to D^{0} (\to K^+ \pi^- \pi^+ \pi^-) \pi`$ (AmpGen DCS model)

* 27165000: $`D^{*} \to D^{0} (\to K K \pi \pi) \pi`$
* 27165003: $`D^{*} \to D^{0} (\to K K \pi \pi) \pi`$

* 27265009: $`D^{*} \to D^{0} (\to \pi \pi \pi \pi) \pi`$ (Phase space model)

## Log
* v0r0? (Jul 2021) - produced 2016-2017-2018 MC nTuples ($`D^{0} \to \pi \pi \pi \pi`$: 27265009)
* v0r02742122 (2021-06-07) - Add productions for Run 1 (all final states), and "cheated selection" ntuples for $`D^{0} \to \pip\pip\pim\pim`$ (using only truth matching) for 27165002 (unfiltered) and 27265009 (filtered). Note that for the filtered 27265009 sample, truth matching doesn't work on the saved Turbo candidates due to [LHCBGAUSS-1776](https://its.cern.ch/jira/browse/LHCBGAUSS-1776), but does work on candidates rebuilt from Brunel tracks as is done for the cheated selections.
* v0r02632463 (May 2021) - produced 2017-2018 MC nTuples to fix bug in MC truth ($`D^{0} \to K K \pi \pi`$: 27165000/27165003)
* v0r0p2201063 (Dec 2020) - produced 2015-18 MC nTuples (but bug in 2015 MU) ($`D^{0} \to K \pi \pi \pi`$ : 27165070/27165071/27165072)
* v0r0p1960798 (Sept 2020) - produced nTuples  ($`D^{0} \to K K \pi \pi`$ : 27165000/27165003)
* ... (April 2020) - submit to fix 2015 MU bug ($`D^{0} \to K \pi \pi \pi`$ : 27165070/27165071/27165072)
