# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the branches should be
# called
decay_descriptors = {
    'KmPimPipPip' : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KpPimPimPip' : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}pi- ${D0_P2}pi+ ${D0_P3}pi-) ${Dst_pi}pi+]CC',
    'PimPimPipPip': '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}pi- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKpPimPip'  : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKmKpPip'   : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}K- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKpKpPim'   : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}K- ${D0_P2}K+ ${D0_P3}pi-) ${Dst_pi}pi+]CC'
}

decay_descriptors_trueMC = {
    '27165000' : '[D*(2010)+ ==> ^([D0]CC ==> ^K- ^K+ ^pi- ^pi+) ^pi+]CC',
    '27165002' : '[D*(2010)+ ==> ^([D0]CC ==> ^pi- ^pi+ ^pi- ^pi+) ^pi+]CC',
    '27165003' : '[D*(2010)+ ==> ^([D0]CC ==> ^K- ^K+ ^pi- ^pi+) ^pi+]CC',
    '27165070' : '[D*(2010)+ ==> ^([D0]CC ==> ^K- ^pi+ ^pi- ^pi+) ^pi+]CC',
    '27165071' : '[D*(2010)+ ==> ^([D0]CC ==> ^K- ^pi+ ^pi- ^pi+) ^pi+]CC',
    '27165072' : '[D*(2010)+ ==> ^([D0]CC ==> ^K+ ^pi- ^pi+ ^pi-) ^pi+]CC',
    '27265009' : '[D*(2010)+ ==> ^([D0]CC ==> ^pi- ^pi+ ^pi- ^pi+) ^pi+]CC'
}

# Trigger lists for TISTOS
L0Triggers = ['L0'+x+'Decision' for x in
              ['Hadron', 'Muon', 'DiMuon', 'Electron', 'Photon']]

HLT1Triggers_Run1 = ['Hlt1'+x+'Decision' for x in
                     ['SingleHadron', 'DiHadron', 'TrackAllL0', 'TrackMuon']]
HLT1Triggers_Run2 = ['Hlt1'+x+'Decision' for x in ['TrackMVA', 'TwoTrackMVA']]

PromptHLT2Lines_Run1 = ['Hlt2CharmHadD02HHHHDecision',
                        'Hlt2CharmHadD02HHHHWideMassDecision'] \
                        + ['Hlt2CharmHadD02HHHHDst_'+x+'Decision' for x in
                           ['4pi', '4piWideMass', 'K3pi', 'K3piWideMass', 'KKpipi',
                            'KKpipiWideMass', '2K2pi', '2K2piWideMass', '3Kpi',
                            '3KpiWideMass']]

# The full name of the HLT2 line
PromptHLT2Lines_Run2 = {name : 'Hlt2CharmHadDstp2D0Pip_D02' + name + 'Turbo'
                        for name in ['KmPimPipPip','KpPimPimPip',
                                     'PimPimPipPip','KmKpPimPip',
                                     'KmKmKpPip','KmKpKpPim']}

topo_body_lines = ["Hlt2Topo2BodyDecision",
                   "Hlt2Topo3BodyDecision",
                   "Hlt2Topo4BodyDecision",
                   "Hlt2TopoE2BodyDecision",
                   "Hlt2TopoE3BodyDecision",
                   "Hlt2TopoE4BodyDecision",
                   "Hlt2TopoEE2BodyDecision",
                   "Hlt2TopoEE3BodyDecision",
                   "Hlt2TopoEE4BodyDecision",
                   "Hlt2TopoMu2BodyDecision",
                   "Hlt2TopoMu3BodyDecision",
                   "Hlt2TopoMu4BodyDecision",
                   "Hlt2TopoMuE2BodyDecision",
                   "Hlt2TopoMuE3BodyDecision",
                   "Hlt2TopoMuE4BodyDecision",
                   "Hlt2TopoMuMu2BodyDecision",
                   "Hlt2TopoMuMu3BodyDecision",
                   "Hlt2TopoMuMu4BodyDecision",
                   "Hlt2TopoMuMuDDDecision"]


def prompt_trigger_list(common, heads = []):
    '''Make the trigger lists for each particle in the tree.'''
    return {'Dst'   : common + heads,
            'D0'    : common + heads,
            'D0_P0' : common,
            'D0_P1' : common,
            'D0_P2' : common,
            'D0_P3' : common,
            'Dst_pi': common}


PromptTriggerLists = {
    '2011' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run1
                                 + PromptHLT2Lines_Run1,
                                 topo_body_lines),
    '2012' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run1
                                 + PromptHLT2Lines_Run1,
                                 topo_body_lines),
    '2015' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run2,
                                 topo_body_lines),
    # Don't include topo lines in 2016
    '2016' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run2),
    '2017' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run2,
                                 topo_body_lines),
    '2018' : prompt_trigger_list(L0Triggers + HLT1Triggers_Run2,
                                 topo_body_lines),
}
