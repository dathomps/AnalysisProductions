from Configurables import DaVinci
from B2Kmumu_isospin import main_options 

main_options.addNtupleToDaVinci("tuple_Kmm", DaVinci().Simulation, True)
main_options.addNtupleToDaVinci("tuple_Ksmm", DaVinci().Simulation, True)
