from Configurables import DaVinci, GaudiSequencer, EventNodeKiller, ProcStatusCheck
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

Year = DaVinci().DataType

def addRestrippingToDaVinci():
    event_node_killer = EventNodeKiller("StripKiller")
    event_node_killer.Nodes = ["/Event/AllStreams", "/Event/Strip"]

    strip = 'stripping21'
    if(Year=='2011'): strip = 'Stripping21r1p2'
    if(Year=='2012'): strip = 'Stripping21r0p2'
    if(Year=='2015'): strip = 'Stripping24r2'
    if(Year=='2016'): strip = 'Stripping28r2'
    if(Year=='2017'): strip = 'Stripping29r2p1'
    if(Year=='2018'): strip = 'Stripping34r0p1'
    streams = buildStreams(stripping=strippingConfiguration(strip), archive=strippingArchive(strip))
    
    custom_stream = StrippingStream("CustomStream")
    custom_line = "StrippingB2XMuMu_Line"
    
    for stream in streams :
        for sline in stream.lines :
            if sline.name() == custom_line :
                custom_stream.appendLines([sline])
                
    filterBadEvents = ProcStatusCheck()
                
    sc = StrippingConf(Streams = [custom_stream],
                       MaxCandidates = 2000,
                       AcceptBadEvents = False,
                       BadEventSelection = filterBadEvents)
    
    enablePacking = False
    
    SelDSTWriterElements = {"default": stripDSTElements(pack=enablePacking)}
    
    SelDSTWriterConf = {"default": stripDSTStreamConf(pack=enablePacking, selectiveRawEvent=False, fileExtension=".dst",)}
    
    #Items that might get lost when running the CALO+PROTO ReProcessing in DV
    #caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
    
    # Make sure they are present on full DST streams
    #SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
    
    
    dstWriter = SelDSTWriter("MyDSTWriter", StreamConf=SelDSTWriterConf, MicroDSTElements=SelDSTWriterElements, OutputFileSuffix="", SelectionSequences=sc.activeStreams(),)
    
    DaVinci().ProductionType = "Stripping"
    seq = GaudiSequencer("TupleSeq")
    seq.IgnoreFilterPassed = True
    seq.Members += [event_node_killer, sc.sequence()] + [dstWriter.sequence()]

    DaVinci().UserAlgorithms = [seq]


addRestrippingToDaVinci()
