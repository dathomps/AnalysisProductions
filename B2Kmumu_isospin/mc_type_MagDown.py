from Configurables import DaVinci

def getDBInfo(magnet, lyr):
    if lyr == "2011":
        TagDDDB = "dddb-20170721-1"
        TagCondDB = "sim-20160614-1-vc"
    if lyr == "2012":
        TagDDDB = "dddb-20170721-2"
        TagCondDB = "sim-20160321-2-vc"
    if lyr == "2015":
        TagDDDB   = "dddb-20170721-3"
        TagCondDB = "sim-20161124-vc"
    if lyr == "2016":
        TagDDDB   = "dddb-20170721-3"
        TagCondDB = "sim-20170721-2-vc"
    if lyr == "2017":
        TagDDDB = "dddb-20170721-3"
        TagCondDB = "sim-20190430-1-vc"
    if lyr == "2018":
        TagDDDB = "dddb-20170721-3"
        TagCondDB = "sim-20190430-vc"

    if magnet == "MD":
        TagCondDB += "-md100"
    else:
        TagCondDB += "-mu100"

    return TagDDDB, TagCondDB

MagnetPolarity = "MD"
DaVinci().Simulation = True

tDDDB, tConDB = getDBInfo(MagnetPolarity, DaVinci().DataType)
DaVinci().DDDBtag   = tDDDB
DaVinci().CondDBtag = tConDB

print("Year = ", DaVinci().DataType)
print("MagnetPolarity = ", MagnetPolarity)

print("DDDBtag = ", tDDDB)
print("CondDBtag = ", tConDB)

#DaVinci().EvtMax = 1000

