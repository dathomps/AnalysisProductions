from B2Kmumu_isospin import main_options_mc_gen 

decay_heads = ["B0", "B~0"]

decay = "[[B0]cc ==> ^mu+ ^mu- ^(KS0 ==> ^pi+ ^pi- )]CC"
main_options_mc_gen.addNtupleToDaVinci(decay,decay_heads,"B2MuMuKs2PiPi")

decay = "[[B0]cc ==> ^mu+ ^mu- ^KS0 ]CC"
main_options_mc_gen.addNtupleToDaVinci(decay,decay_heads,"B2MuMuKs")

