from B2Kmumu_isospin import main_options_mc_gen 

decay = "[B+ ==> ^(psi(2S) ==> ^mu+ ^mu-) ^K+]CC"
decay_heads = ["B+", "B-"]

main_options_mc_gen.addNtupleToDaVinci(decay,decay_heads,"B2psi2sK")

