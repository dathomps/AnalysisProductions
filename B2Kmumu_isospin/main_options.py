# author : F. De Vellis, V. Lisovskyi, P. d'Argent
# description : Produce B->Kmumu tuple
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, SubstitutePID
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

from Configurables import GaudiSequencer, DataOnDemandSvc,CondDB
from Configurables import DstConf, CaloDstUnPackConf, LoKi__Hybrid__TupleTool
from Configurables import EventNodeKiller, ProcStatusCheck
from Configurables import StrippingReport, TimingAuditor, SequencerTimerTool

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

Year = DaVinci().DataType

def addNtupleToDaVinci(name, isSim, isRestripped):

  theTuple = DecayTreeTuple(name)
  if name == "tuple_Kmm":
    theTuple.Decay = "[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC"
    line = "B2XMuMu_Line"
    lineQ = "'"+line+"'"
    theInput = "Phys/" + line + "/Particles"
    theTuple.addBranches ({
      "B":"[B+ -> (J/psi(1S) -> mu+ mu-) K+]CC",
      "Jpsi":"[B+ -> ^(J/psi(1S) -> mu+ mu-) K+]CC",
      "M1":"[B+ -> (J/psi(1S) -> ^mu+ mu-) K+]CC",
      "M2":"[B+ -> (J/psi(1S) -> mu+ ^mu-) K+]CC",
      "K":"[B+ -> (J/psi(1S) -> mu+ mu-) ^K+]CC",
    })
  elif name == "tuple_Ksmm":
    theTuple.Decay = "B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(KS0 -> ^pi+ ^pi-)"
    line = "B2XMuMu_Line"
    lineQ = "'"+line+"'"
    theInput = "Phys/" + line + "/Particles"
    theTuple.addBranches ({
      "B":"B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> pi+ pi-)",
      "Jpsi":"B0 -> ^(J/psi(1S) -> mu+ mu-) (KS0 -> pi+ pi-)",
      "M1":"B0 -> (J/psi(1S) -> ^mu+ mu-) (KS0 -> pi+ pi-)",
      "M2":"B0 -> (J/psi(1S) -> mu+ ^mu-) (KS0 -> pi+ pi-)",
      "KS":"B0 -> (J/psi(1S) -> mu+ mu-) ^(KS0 -> pi+ pi-)",
      "Pi1":"B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> ^pi+ pi-)",
      "Pi2":"B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> pi+ ^pi-)",
    })
  else:
    print("Unknown tuple name :", name)
    return

  # define the TES location for inputs
  if isSim:
    if isRestripped == 1:
        theTuple.Inputs = ["/Event/CustomStream/Phys/"+line+"/Particles"]
    if isRestripped == 0:
        if DaVinci().InputType == "MDST":
            DaVinci().RootInTES = "/Event/AllStreams/"
            theTuple.Inputs = [ theInput ]
        else: 
            theTuple.Inputs = ['/Event/AllStreams/Phys/'+line+'/Particles']
            
  else:
    DaVinci().RootInTES = "/Event/Leptonic/"
    theTuple.Inputs = [ theInput ]

  TriggerListL0 = [
    "L0ElectronDecision",
    "L0HadronDecision",
    "L0PhotonDecision",
    "L0MuonDecision",
    "L0DiMuonDecision"
  ]
  if Year == "2011" or Year == "2012":
    TriggerListHlt = [
      "Hlt1TrackAllL0Decision",
      "Hlt1DiMuonLowMassDecision",
      "Hlt1DiMuonHighMassDecision",
      "Hlt1MuTrackDecision",
      "Hlt1TrackMuonDecision",
      "Hlt2Topo2BodyBBDTDecision",
      "Hlt2Topo3BodyBBDTDecision",
      "Hlt2Topo4BodyBBDTDecision",
      "Hlt2TopoE2BodyBBDTDecision",
      "Hlt2TopoE3BodyBBDTDecision",
      "Hlt2TopoE4BodyBBDTDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision",
      "Hlt2RadiativeTopoTrackTOSDecision",
      "Hlt2RadiativeTopoPhotonL0Decision",
      "Hlt2SingleMuonDecision",
      "Hlt2DiMuonDetachedDecision"
    ]
  elif Year == "2015" or Year == "2016" or Year == "2017" or Year == "2018":
    TriggerListHlt = [
    # hlt1
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMVATightDecision',
    'Hlt1TwoTrackMVATightDecision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackMuonMVADecision',
    'Hlt1DiMuonHighMassDecision',
    'Hlt1DiMuonLowMassDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt1DiMuonNoL0Decision',
    'Hlt1DiMuonNoIPDecision',
    'Hlt1SingleMuonHighPTNoMUIDDecision',
    'Hlt1SingleMuonNoIPDecision',
    #some more for 2015
    'Hlt1MultiMuonNoL0Decision',
    'Hlt1TrackMuonNoSPDDecision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonDetachedPsi2SDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2DiMuonJPsiHighPTDecision',
    'Hlt2DiMuonPsi2SDecision',
    'Hlt2DiMuonPsi2SHighPTDecision',
    'Hlt2DiMuonSoftDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2SingleMuonRareDecision',
    'Hlt2SingleMuonVHighPTDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision',
    'Hlt2SingleMuonNoSPDDecision',
    ]

## We are redefining the tupletool list from scratch : the default tupletool list does not load some of important geometry variables.
  theTuple.ToolList = []

  LoKiVariables_all = LoKi__Hybrid__TupleTool("LoKiVariables_all")
  LoKiVariables_all.Preambulo +=['from LoKiTracks.decorators import *']

  LoKiVariables_all.Variables = {
     "PHI"    : "PHI",
     "ETA" : "ETA",
  }
  theTuple.addTool(LoKiVariables_all, name = "LoKiVariables_all" )
  theTuple.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables_all"]

  LoKiVariables = LoKi__Hybrid__TupleTool("LoKiVariables")
  LoKiVariables.Preambulo +=['from LoKiTracks.decorators import *']
  LoKiVariables.Preambulo +=['masses = LoKi.AuxDTFBase.MASSES()',
                             'masses ["J/psi(1S)"] = 3686.097']

  LoKiVariables.Variables = {
      "LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )",
      "LOKI_DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
      "LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )",

      "LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",

      "LOKI_DTF_DIRA"    : "DTF_FUN(BPVDIRA , True )",
      "LOKI_DTF_VCHI2"    : "DTF_FUN(VFASPF(VCHI2) , True )",

      "LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
      "LOKI_MASS_psi2SConstr" : "DTF_FUN ( M , True , 'J/psi(1S)', masses )" ,
      "LOKI_MASS_BConstr" : "DTF_FUN ( M , True , 'B+' )" ,

      "LOKI_DTF_MASS" : "DTF_FUN ( M , True )" ,
      "LOKI_DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",

      "DOCA" : "DOCA(1,2)",
      "DOCACHI2" : "DOCACHI2(1,2)",

      "MTDOCA_1" : "MTDOCA(1)",
      "MTDOCACHI2_1" : "MTDOCACHI2(1)",
      "MTDOCA_2" : "MTDOCA(2)",
      "MTDOCACHI2_2" : "MTDOCACHI2(2)",
  }
  
  if name == "tuple_Ksmm": 
  
    LoKiVariables.Variables.update( {
      "LOKI_DTF_CHI2NDOF_KSConstr"    : "DTF_CHI2NDOF( True , 'KS0')",
      "LOKI_DTF_DIRA_KSConstr"    : "DTF_FUN(BPVDIRA , True , 'KS0')",
      "LOKI_DTF_VCHI2_KSConstr"    : "DTF_FUN(VFASPF(VCHI2) , True , 'KS0')",
      "LOKI_DTF_LTIME_KSConstr"    : "DTF_FUN(BPVLTIME() , True , 'KS0')",
      "LOKI_MASS_KSConstr" : "DTF_FUN ( M , True , 'KS0' )" ,
      "LOKI_MASS_AllConstr" : "DTF_FUN ( M , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_MASS_AllConstr2S" : "DTF_FUN ( M , True , strings(['KS0', 'J/psi(1S)']), masses)" ,
    } )
    
    theTuple.KS.addTool(LoKiVariables , name = "LoKiVariables" )
    theTuple.KS.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables"]

  theTuple.B.addTool(LoKiVariables , name = "LoKiVariables" )
  theTuple.B.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables"]

  theTuple.Jpsi.addTool(LoKiVariables , name = "LoKiVariables" )
  theTuple.Jpsi.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables"]

  LoKiVariables2 = LoKi__Hybrid__TupleTool("LoKiVariables2")
  LoKiVariables2.Variables = {
      "LOKI_DTF_CHI2NDOFnoPV"    : "DTF_CHI2NDOF( False )",
      "LOKI_MASS_JpsiConstrnoPV" : "DTF_FUN ( M , False , 'J/psi(1S)' )" ,
      "LOKI_MASS_psi2SConstrnoPV" : "DTF_FUN ( M , False , 'J/psi(1S)', masses )" ,
      "LOKI_MASS_BConstrnoPV" : "DTF_FUN ( M , False , 'B+' )" ,

      "LOKI_DTF_MASS" : "DTF_FUN ( M , True )" ,
      "LOKI_DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",

      "LOKI_DTF_MASSnoPV" : "DTF_FUN ( M , False )" ,
      "LOKI_DTF_VCHI2NDOFnoPV"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , False )",

      "LOKI_Jpsi_M_BConstr" : "DTF_FUN ( CHILD(1,M) , True , 'B+' )" ,
      "LOKI_q2_BConstr" : "DTF_FUN ( CHILD(1,M*M) , True , 'B+' )" , 
      
      "LOKI_Jpsi_PX_BConstr" : "DTF_FUN ( CHILD(1,PX) , True , 'B+' )" ,
      "LOKI_Jpsi_PY_BConstr" : "DTF_FUN ( CHILD(1,PY) , True , 'B+' )" ,
      "LOKI_Jpsi_PZ_BConstr" : "DTF_FUN ( CHILD(1,PZ) , True , 'B+' )" ,
      "LOKI_Jpsi_PT_BConstr" : "DTF_FUN ( CHILD(1,PT) , True , 'B+' )" ,
      "LOKI_Jpsi_PE_BConstr" : "DTF_FUN ( CHILD(1,E) , True , 'B+' )" ,

      "LOKI_JpsiPX_JpsiConstr" : "DTF_FUN ( CHILD(1,PX) , True , 'J/psi(1S)' )" ,
      "LOKI_JpsiPY_JpsiConstr" : "DTF_FUN ( CHILD(1,PY) , True , 'J/psi(1S)' )" ,
      "LOKI_JpsiPZ_JpsiConstr" : "DTF_FUN ( CHILD(1,PZ) , True , 'J/psi(1S)' )" ,
      "LOKI_JpsiPT_JpsiConstr" : "DTF_FUN ( CHILD(1,PT) , True , 'J/psi(1S)' )" ,
      "LOKI_JpsiPE_JpsiConstr" : "DTF_FUN ( CHILD(1,E) , True , 'J/psi(1S)' )" ,
      "LOKI_KPX_JpsiConstr" : "DTF_FUN ( CHILD(2,PX) , True , 'J/psi(1S)' )" ,
      "LOKI_KPY_JpsiConstr" : "DTF_FUN ( CHILD(2,PY) , True , 'J/psi(1S)' )" ,
      "LOKI_KPZ_JpsiConstr" : "DTF_FUN ( CHILD(2,PZ) , True , 'J/psi(1S)' )" ,
      "LOKI_KPT_JpsiConstr" : "DTF_FUN ( CHILD(2,PT) , True , 'J/psi(1S)' )" ,
      "LOKI_KPE_JpsiConstr" : "DTF_FUN ( CHILD(2,E) , True , 'J/psi(1S)' )" ,
  }
  
  if name == "tuple_Ksmm": 
  
    LoKiVariables2.Variables.update( {
      "LOKI_MASS_KSConstrnoPV" : "DTF_FUN ( M , False , 'KS0' )" ,
      "LOKI_MASS_KSB0ConstrnoPV" : "DTF_FUN ( M , False , strings(['KS0', 'B0']) )" ,
      "LOKI_MASS_AllConstrnoPV" : "DTF_FUN ( M , False , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_MASS_AllConstr2SnoPV" : "DTF_FUN ( M , False , strings(['KS0', 'J/psi(1S)']), masses)" ,

      # the dimuon mass with KS and B mass constraints (+vertex constraint)
      "LOKI_Jpsi_M_KSBConstr" : "DTF_FUN ( CHILD(1,M) , True , strings(['KS0', 'B0']) )" ,
      "LOKI_q2_KSBConstr" : "DTF_FUN ( CHILD(1,M*M) , True , strings(['KS0', 'B0']) )" , #same but squared
      # some momenta
      "LOKI_Jpsi_PX_KSBConstr" : "DTF_FUN ( CHILD(1,PX) , True , strings(['KS0', 'B0']) )" ,
      "LOKI_Jpsi_PY_KSBConstr" : "DTF_FUN ( CHILD(1,PY) , True , strings(['KS0', 'B0']) )" ,
      "LOKI_Jpsi_PZ_KSBConstr" : "DTF_FUN ( CHILD(1,PZ) , True , strings(['KS0', 'B0']) )" ,
      "LOKI_Jpsi_PT_KSBConstr" : "DTF_FUN ( CHILD(1,PT) , True , strings(['KS0', 'B0']) )" ,
      "LOKI_Jpsi_PE_KSBConstr" : "DTF_FUN ( CHILD(1,E) , True , strings(['KS0', 'B0']) )" ,

      "LOKI_JpsiPX_AllConstr" : "DTF_FUN ( CHILD(1,PX) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_JpsiPY_AllConstr" : "DTF_FUN ( CHILD(1,PY) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_JpsiPZ_AllConstr" : "DTF_FUN ( CHILD(1,PZ) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_JpsiPT_AllConstr" : "DTF_FUN ( CHILD(1,PT) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_JpsiPE_AllConstr" : "DTF_FUN ( CHILD(1,E) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_KsPX_AllConstr" : "DTF_FUN ( CHILD(2,PX) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_KsPY_AllConstr" : "DTF_FUN ( CHILD(2,PY) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_KsPZ_AllConstr" : "DTF_FUN ( CHILD(2,PZ) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_KsPT_AllConstr" : "DTF_FUN ( CHILD(2,PT) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_KsPE_AllConstr" : "DTF_FUN ( CHILD(2,E) , True , strings(['KS0', 'J/psi(1S)']) )" ,

      "LOKI_Pi2PX_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PX)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi2PY_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PY)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi2PZ_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PZ)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi2PE_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(2,E)) , True , strings(['KS0', 'J/psi(1S)']) )" ,

      "LOKI_Pi1PX_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PX)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi1PY_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PY)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi1PZ_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PZ)) , True , strings(['KS0', 'J/psi(1S)']) )" ,
      "LOKI_Pi1PE_AllConstr" : "DTF_FUN ( CHILD(2,CHILD(1,E)) , True , strings(['KS0', 'J/psi(1S)']) )" ,

      "LOKI_JpsiPX_KSConstr" : "DTF_FUN ( CHILD(1,PX) , True , strings(['KS0']) )" ,
      "LOKI_JpsiPY_KSConstr" : "DTF_FUN ( CHILD(1,PY) , True , strings(['KS0']) )" ,
      "LOKI_JpsiPZ_KSConstr" : "DTF_FUN ( CHILD(1,PZ) , True , strings(['KS0']) )" ,
      "LOKI_JpsiPT_KSConstr" : "DTF_FUN ( CHILD(1,PT) , True , strings(['KS0']) )" ,
      "LOKI_JpsiPE_KSConstr" : "DTF_FUN ( CHILD(1,E) , True , strings(['KS0']) )" ,
      "LOKI_JpsiIPCHI2_KSConstr" : "DTF_FUN ( CHILD(1,MIPCHI2DV(PRIMARY)) , True , strings(['KS0']) )" ,
      "LOKI_JpsiDIRA_KSConstr" : "DTF_FUN ( CHILD(1,BPVDIRA) , True , strings(['KS0']) )" ,
      "LOKI_JpsiBPVVDCHI2_KSConstr" : "DTF_FUN ( CHILD(1,BPVVDCHI2) , True , strings(['KS0']) )" ,
      "LOKI_JpsiVCHI2_KSConstr" : "DTF_FUN ( CHILD(1,VFASPF(VCHI2)) , True , strings(['KS0']) )" ,

      "LOKI_KsPX_KSConstr" : "DTF_FUN ( CHILD(2,PX) , True , strings(['KS0']) )" ,
      "LOKI_KsPY_KSConstr" : "DTF_FUN ( CHILD(2,PY) , True , strings(['KS0']) )" ,
      "LOKI_KsPZ_KSConstr" : "DTF_FUN ( CHILD(2,PZ) , True , strings(['KS0']) )" ,
      "LOKI_KsPT_KSConstr" : "DTF_FUN ( CHILD(2,PT) , True , strings(['KS0']) )" ,
      "LOKI_KsPE_KSConstr" : "DTF_FUN ( CHILD(2,E) , True , strings(['KS0']) )" ,
      "LOKI_KsETA_KSConstr" : "DTF_FUN ( CHILD(2,ETA) , True , strings(['KS0']) )" ,
      "LOKI_KsIPCHI2_KSConstr" : "DTF_FUN ( CHILD(2,MIPCHI2DV(PRIMARY)) , True , strings(['KS0']) )" ,
      "LOKI_KsDIRA_KSConstr" : "DTF_FUN ( CHILD(2,BPVDIRA) , True , strings(['KS0']) )" ,
      "LOKI_KsBPVVDCHI2_KSConstr" : "DTF_FUN ( CHILD(2,BPVVDCHI2) , True , strings(['KS0']) )" ,
      "LOKI_KsVCHI2_KSConstr" : "DTF_FUN ( CHILD(2,VFASPF(VCHI2)) , True , strings(['KS0']) )" ,
      "LOKI_KsVZ_KSConstr" : "DTF_FUN ( CHILD(2,VFASPF(VZ)) , True , strings(['KS0']) )" ,
      "LOKI_KsLTIME_KSConstr" : "DTF_FUN ( CHILD(2,BPVLTIME()) , True , strings(['KS0']) )" ,

      "LOKI_Pi2PX_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PX)) , True , strings(['KS0']) )" ,
      "LOKI_Pi2PY_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PY)) , True , strings(['KS0']) )" ,
      "LOKI_Pi2PZ_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,PZ)) , True , strings(['KS0']) )" ,
      "LOKI_Pi2PE_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,E)) , True , strings(['KS0']) )" ,
      "LOKI_Pi2ETA_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,ETA)) , True , strings(['KS0']) )" ,
      "LOKI_Pi2IPCHI2_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(2,MIPCHI2DV(PRIMARY))) , True , strings(['KS0']) )" ,

      "LOKI_Pi1PX_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PX)) , True , strings(['KS0']) )" ,
      "LOKI_Pi1PY_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PY)) , True , strings(['KS0']) )" ,
      "LOKI_Pi1PZ_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,PZ)) , True , strings(['KS0']) )" ,
      "LOKI_Pi1PE_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,E)) , True , strings(['KS0']) )" ,
      "LOKI_Pi1ETA_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,ETA)) , True , strings(['KS0']) )" ,
      "LOKI_Pi1IPCHI2_KSConstr" : "DTF_FUN ( CHILD(2,CHILD(1,MIPCHI2DV(PRIMARY))) , True , strings(['KS0']) )" ,

      "LOKI_Mu2PX_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(2,PX)) , True , strings(['KS0']) )" ,
      "LOKI_Mu2PY_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(2,PY)) , True , strings(['KS0']) )" ,
      "LOKI_Mu2PZ_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(2,PZ)) , True , strings(['KS0']) )" ,
      "LOKI_Mu2PE_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(2,E)) , True , strings(['KS0']) )" ,
      "LOKI_Mu2IPCHI2_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(2,MIPCHI2DV(PRIMARY))) , True , strings(['KS0']) )" ,

      "LOKI_Mu1PX_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(1,PX)) , True , strings(['KS0']) )" ,
      "LOKI_Mu1PY_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(1,PY)) , True , strings(['KS0']) )" ,
      "LOKI_Mu1PZ_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(1,PZ)) , True , strings(['KS0']) )" ,
      "LOKI_Mu1PE_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(1,E)) , True , strings(['KS0']) )" ,
      "LOKI_Mu1IPCHI2_KSConstr" : "DTF_FUN ( CHILD(1,CHILD(1,MIPCHI2DV(PRIMARY))) , True , strings(['KS0']) )" ,
    } )
    
    theTuple.KS.addTool(LoKiVariables2 , name = "LoKiVariables2L" )
    theTuple.KS.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables2L"]
  
  theTuple.B.addTool(LoKiVariables2 , name = "LoKiVariables2" )
  theTuple.B.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables2"]

  theTuple.Jpsi.addTool(LoKiVariables2 , name = "LoKiVariables2J" )
  theTuple.Jpsi.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables2J"]

##### mass hypothesis substitutions, to study misID backgrounds #####
  theTuple.B.addTupleTool( 'TupleToolSubMass' )
  theTuple.B.ToolList += [ "TupleToolSubMass" ]

## common for mm and ee modes:
  theTuple.B.TupleToolSubMass.Substitution += ["K+ => pi+"]+["K+ => p+"]
  theTuple.B.TupleToolSubMass.Substitution += ["p+ => K+"]+["p+ => pi+"]
  theTuple.B.TupleToolSubMass.Substitution += ["pi+ => K+"]+["pi+ => p+"]

  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/K-"]+["K-/p+ => pi+/K-"]+["K-/p+ => p+/pi-"]+["K-/p+ => pi+/pi-"]+["K-/p+ => mu+/mu-"]+["K-/p+ => e+/e-"]
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["pi-/p+ => pi+/K-"]+["pi-/p+ => pi+/K-"]+["pi-/p+ => p+/pi-"]+["pi-/p+ => pi+/pi-"]+["pi-/p+ => mu+/mu-"]+["pi-/p+ => e+/e-"]

##involving specific leptons:
  #muons
  theTuple.B.TupleToolSubMass.Substitution += ["pi+ => mu+"]+["p+ => mu+"]
  theTuple.B.TupleToolSubMass.Substitution += ["mu+ => K+"]+["mu+ => pi+"]+["mu+ => p+"]+["mu+ => e+"]
  
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["mu+/mu- => pi+/pi-"]+["mu+/mu- => K+/K-"]+["mu+/mu- => K+/pi-"]+["mu+/mu- => pi+/K-"]+["mu+/mu- => e+/e-"]
  
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["K+/mu+ => mu+/K+"]+["K+/mu+ => pi+/pi+"]+["K+/mu+ => p+/p+"]
  
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["p+/mu+ => mu+/p+"]+["p+/mu+ => pi+/pi+"]+["p+/mu+ => K+/K+"]+["p+/mu+ => K+/pi+"]
  
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["K+/mu- => mu+/K-"]+["K+/mu- => pi+/pi-"]+["K+/mu- => p+/p~-"]+["K+/mu- => pi+/K-"]+["K+/mu- => pi+/p~-"]+["K+/mu- => p+/pi-"]+["K+/mu- => p+/K-"]+["K+/mu- => e+/e-"]
  
  theTuple.B.TupleToolSubMass.DoubleSubstitution += ["p+/mu- => mu+/p~-"]+["p+/mu- => pi+/pi-"]+["p+/mu- => pi+/p~-"]+["p+/mu- => K+/pi-"]+["p+/mu- => K+/p~-"]+["p+/mu- => pi+/K-"]+["p+/mu- => e+/e-"]

#other useful variables
  theTuple.addTupleTool('TupleToolTrackInfo')
  theTuple.TupleToolTrackInfo.Verbose=True

  theTuple.addTupleTool('TupleToolEventInfo')
  theTuple.TupleToolEventInfo.Verbose=True

  theTuple.addTupleTool('TupleToolPropertime')
  theTuple.TupleToolPropertime.Verbose=True

  theTuple.addTupleTool('TupleToolRecoStats')
  theTuple.TupleToolRecoStats.Verbose=True

  theTuple.addTupleTool('TupleToolGeometry')
  theTuple.TupleToolGeometry.Verbose=True

  theTuple.addTupleTool('TupleToolKinematic')
  theTuple.TupleToolKinematic.Verbose=True

  theTuple.addTupleTool('TupleToolPid')
  theTuple.TupleToolPid.Verbose=True

  theTuple.addTupleTool('TupleToolTrigger')
  theTuple.TupleToolTrigger.Verbose=True

  theTuple.addTupleTool('TupleToolAngles')
  theTuple.TupleToolAngles.Verbose=True

  theTuple.addTupleTool('TupleToolPrimaries')
  theTuple.TupleToolPrimaries.Verbose=True

  theTuple.ToolList += ["TupleToolL0Data"]

  if isSim:
    if isRestripped:
      path_pref = '/Event/CustomStream/Phys/'
    else:
      path_pref = '/Event/AllStreams/Phys/'
  else:
    path_pref = '/Event/Leptonic/Phys/'

  if lineQ == "'B2XMuMu_Line'":
    LoKi_Cone =  LoKi__Hybrid__TupleTool("LoKi_Cone")
    LoKi_Cone.Variables = {}

    # cone iso 
    cone_iso_vars = {
        "CONEMULT":"CONEMULT",
        "CONEPTASYM":"CONEPTASYM",
        "CONEPT":"CONEPT",
        "CONEP":"CONEP",
        "CONEPASYM":"CONEPASYM",
        "CONEDELTAETA":"CONEDELTAETA",
        "CONEDELTAPHI":"CONEDELTAPHI",    
    }
    parent_cone_vars  = dict( (var_ntp, 
                               "RELINFO('{}'".format(path_pref) + lineQ +"'/ConeIsoInfo', '{}', -999.)".format( var_loki))
                              for var_ntp, var_loki in cone_iso_vars.items()
                              )
    LoKi_Cone.Variables.update( parent_cone_vars )

    # vertex iso info
    vtxiso_vars = {
      "NumVtxWithinChi2WindowOneTrack": 'VTXISONUMVTX',
      "SmallestDeltaChi2OneTrack"     : 'VTXISODCHI2ONETRACK',
      "SmallestDeltaChi2MassOneTrack" : 'VTXISODCHI2MASSONETRACK',
      "SmallestDeltaChi2TwoTracks"    : 'VTXISODCHI2TWOTRACK',
      "SmallestDeltaChi2MassTwoTracks": 'VTXISODCHI2MASSTWOTRACK',
      }
    vtxiso_vars_dict = dict( (var_ntp, "RELINFO('{}'".format(path_pref) + lineQ +"'/VtxIsoInfo', '{}', -999.)".format(var_loki))
                             for var_ntp, var_loki in vtxiso_vars.items() )
    LoKi_Cone.Variables.update(vtxiso_vars_dict)

    # vtx iso BDT
    vtx_iso_bdt_names = ["VTXISOBDTHARDFIRSTVALUE", "VTXISOBDTHARDSECONDVALUE", "VTXISOBDTHARDTHIRDVALUE"]
    vtx_iso_bdt_vars = dict( ("VtxIsoBDT_{}".format(var_name),
                              "RELINFO('{}'".format(path_pref) + lineQ +"'/VtxIsoBDTInfo', '{}', -999.)".format(var_name))
                               for var_name in vtx_iso_bdt_names
                               )
    LoKi_Cone.Variables.update(vtx_iso_bdt_vars)

    # KstMuMuVariables
    kstmm_isoL_bdt_names = ["MU_SLL_ISO_1", "MU_SLL_ISO_2"]
    kstmm_iso_bdt_vars = dict( ("VtxIsoBDT_{}".format(var_name),
                              "RELINFO('{}'".format(path_pref) + lineQ +"'/KSTARMUMUVARIABLES', '{}', -999.)".format(var_name))
                               for var_name in kstmm_isoL_bdt_names
                               )
    if isSim: 
        if isRestripped:
            if (int(DaVinci().DataType) < 2015):
                kstmm_iso_bdt_vars = dict( ("VtxIsoBDT_{}".format(var_name),
                                  "RELINFO('{}'".format(path_pref) + lineQ +"'/KSTARMUMUVARIABLES2', '{}', -999.)".format(var_name))
                                   for var_name in kstmm_isoL_bdt_names
                                   )
            LoKi_Cone.Variables.update(kstmm_iso_bdt_vars)
                                   
    else: LoKi_Cone.Variables.update(kstmm_iso_bdt_vars)

    theTuple.B.addTool(LoKi_Cone , name = "LoKi_Cone" )
    theTuple.B.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKi_Cone"]


  theTuple.addTupleTool('TupleToolTISTOS')
  theTuple.TupleToolTISTOS.TriggerList = TriggerListL0
  theTuple.TupleToolTISTOS.TriggerList += TriggerListHlt

  theTuple.TupleToolTISTOS.Verbose = True
  theTuple.TupleToolTISTOS.VerboseL0 = True
  theTuple.TupleToolTISTOS.VerboseHlt1 = True
  theTuple.TupleToolTISTOS.VerboseHlt2 = True

  theTuple.addTupleTool('TupleToolANNPID')

##DecayTreeFitter
  theTuple.B.addTupleTool( 'TupleToolDecayTreeFitter', name = "DTF" )
  theTuple.B.addTupleTool( theTuple.B.DTF.clone( "DTF_PV",
                                                 constrainToOriginVertex = True ) )
  theTuple.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]
  theTuple.B.addTupleTool( theTuple.B.DTF.clone( "DTF_JPsi",
                                                 constrainToOriginVertex = False,
                                                 daughtersToConstrain = [ "J/psi(1S)" ] ) )
  theTuple.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi" ]
  
  if name == "tuple_Kmm":
    theTuple.B.addTupleTool( theTuple.B.DTF.clone( "DTF_Psi",
                                                 constrainToOriginVertex = False,
                                                 Substitutions = { 
                                                   "B+ -> ^J/psi(1S) K+" : "psi(2S)",
                                                   "B- -> ^J/psi(1S) K-" : "psi(2S)" },
                                                 daughtersToConstrain = [ "psi(2S)" ] ) )
    theTuple.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_Psi" ]
  
  else:
    theTuple.B.addTupleTool( theTuple.B.DTF.clone( "DTF_Psi",
                                            constrainToOriginVertex = False,
                                            Substitutions = { "B0 -> ^J/psi(1S) KS0" : "psi(2S)" },
                                            daughtersToConstrain = [ "psi(2S)" ] ) )
    theTuple.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_Psi" ]


    theTuple.KS.addTupleTool( 'TupleToolDecayTreeFitter', name = "DTFL" )
    theTuple.KS.addTupleTool( theTuple.KS.DTFL.clone( "DTFL_KS1",
                                                constrainToOriginVertex = False,
                                                Substitutions = { "KS0 -> ^pi+ pi-" : "p+" },
                                                       ) )
    theTuple.KS.ToolList += [ "TupleToolDecayTreeFitter/DTFL_KS1" ]

    theTuple.KS.addTupleTool( theTuple.KS.DTFL.clone( "DTFL_KS2",
                                                constrainToOriginVertex = False,
                                                Substitutions = { "KS0 -> pi+ ^pi-" : "p~-" },
                                                       ) )
    theTuple.KS.ToolList += [ "TupleToolDecayTreeFitter/DTFL_KS2" ]

    theTuple.B.addTupleTool( theTuple.B.DTF.clone( "DTFL_KSall", #probably does not work correctly
                                            constrainToOriginVertex = False,
                                            Substitutions = { "B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> ^pi+ pi-)" : "p+","B0 -> (J/psi(1S) -> mu+ mu-) (KS0 -> pi+ ^pi-)" : "p~-"},
                                                   ) )
    theTuple.B.ToolList += [ "TupleToolDecayTreeFitter/DTFL_KSall" ]


##_TRUE* variables for MC
  if isSim:
    theTuple.addTupleTool('TupleToolMCTruth')
    theTuple.addTupleTool('TupleToolMCBackgroundInfo')
    theTuple.TupleToolMCTruth.ToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
    theTuple.ToolList += [ "TupleToolMCTruth" ]
    theTuple.ToolList += [ "TupleToolMCBackgroundInfo" ]


  DaVinci().UserAlgorithms += [theTuple]


###############   Pre Filter, does not really do much except choose only candidates passing the Stripping line, maybe beneficial to performance
from Configurables import LoKi__HDRFilter as StripFilter
stripFilter = StripFilter( 'stripPassFilter', Code = "HLT_PASS('StrippingB2XMuMu_LineDecision')", Location= "/Event/Strip/Phys/DecReports")

if not DaVinci().Simulation: 
#if not DaVinci().InputType == "MDST": 
    DaVinci().EventPreFilters = [stripFilter]
#addNtupleToDaVinci("tuple_Kmm", DaVinci().Simulation)
#addNtupleToDaVinci("tuple_Ksmm", DaVinci().Simulation)

