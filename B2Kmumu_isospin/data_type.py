from Configurables import DaVinci
from Configurables import CondDB

DaVinci().Simulation = False

print("Year = ", DaVinci().DataType)

if DaVinci().DataType == "2011":
    DaVinci().DDDBtag   = "dddb-20190206-1"
    DaVinci().CondDBtag = "cond-20150409-1"
    print("DDDBtag = ", DaVinci().DDDBtag)
    print("CondDBtag = ", DaVinci().CondDBtag)
else:
    CondDB().LatestGlobalTagByDataType = DaVinci().DataType
