from Configurables import (
    DaVinci,
    MCDecayTreeTuple,
    PrintMCTree
)
from DecayTreeTuple.Configuration import *

mc_basic_loki_vars = {
    'TRUEETA': 'MCETA',
    'TRUEPHI': 'MCPHI',
    'TRUEPT': 'MCPT',
    'TRUEPX': 'MCPX',
    'TRUEPY': 'MCPY',
    'TRUEPZ': 'MCPZ',
    'TRUEE': 'MCE',
    'TRUEP': 'MCP',
}

debug = False

def addNtupleToDaVinci(decay, decay_heads, name):

    mctuple = MCDecayTreeTuple("MCDecayTreeTuple_"+name)
    mctuple.Decay = decay

    mctuple.ToolList = [
        "MCTupleToolHierarchy",
        "LoKi::Hybrid::MCTupleTool/LoKi_Photos" 
    ]
    mctuple.addTupleTool(
        'LoKi::Hybrid::MCTupleTool/basicLoKiTT'
    ).Variables = mc_basic_loki_vars

    mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
    mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
        "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
    }

    mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/Loki_All")
    mctuple.Loki_All.Variables = {'TRUEID' : 'MCID'}

    mctuple.ToolList += [ "MCTupleToolPID" ]

    from Configurables import TupleToolDecay
    mctuple.addTool(TupleToolDecay, name = 'B')
    mctuple.B.ToolList += ["MCTupleToolPID"]

    mctuple.ToolList += [ "MCTupleToolReconstructed","TupleToolEventInfo","MCTupleToolPrimaries" ]

    from Configurables import LoKi__Hybrid__EvtTupleTool
    mctuple.addTool( LoKi__Hybrid__EvtTupleTool, name = "ELoKiHybrid" )
    mctuple.ToolList += [ "LoKi::Hybrid::EvtTupleTool/ELoKiHybrid" ]
    mctuple.ELoKiHybrid.Preambulo = [ "from LoKiCore.basic import LHCb" ]
    mctuple.ELoKiHybrid.VOID_Variables = {
        "nSPDHits" : "RECSUMMARY( LHCb.RecSummary.nSPDhits, -1, '/Event/Rec/Summary', False )",
        "nTracks"  : "RECSUMMARY( LHCb.RecSummary.nTracks, -1, '/Event/Rec/Summary', False )"
    }

    # Print the decay tree for any particle in decay_heads                                                     
    if debug:
        from Configurables import PrintMCTree, PrintMCDecayTreeTool
        mctree = PrintMCTree("PrintTrue")
        mctree.addTool( PrintMCDecayTreeTool )
        mctree.PrintMCDecayTreeTool.Information = "Name M P Px Py Pz Pt Vx Vy Vz"
        mctree.ParticleNames = decay_heads
        mctree.Depth = 3 


    # Configure DaVinci
    DaVinci().TupleFile = "DVntuple.root"
    DaVinci().UserAlgorithms += [mctuple]
    
    if debug:
        DaVinci().MoniSequence += [ mctree ]
