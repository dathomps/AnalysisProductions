from B2Kmumu_isospin import main_options_mc_gen 

decay_heads = ["B0", "B~0"]

decay = "[[B0]cc ==> ^(psi(2S) ==> ^mu+ ^mu-) ^(KS0 ==> ^pi+ ^pi- )]CC"
main_options_mc_gen.addNtupleToDaVinci(decay,decay_heads,"B2JpsiKs2PiPi")

decay = "[[B0]cc ==> ^(psi(2S) ==> ^mu+ ^mu-) ^KS0 ]CC"
main_options_mc_gen.addNtupleToDaVinci(decay,decay_heads,"B2psi2sKs")

