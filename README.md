# LHCb Analysis Production submission

Welcome to the *DPA Analysis Productions* data packages repository.
This is for analysts and WGs to upload options files, configure, validate and submit Analysis Productions.
If you are looking for the magic behind the scenes, please see the [`LbAnalysisProductions` repository](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAnalysisProductions) and related repositories under the [WP2 Analysis Productions "hat"](https://gitlab.cern.ch/lhcb-dpa/analysis-productions).

## Table of contents

* [Overview](#overview)
* [Creating a new Analysis Production](#creating-a-new-analysis-production)
  * [YAML configuration](#yaml-configuration)
    * [Automatic Configuration](#automatic-configuration)
    * [Input](#input)
    * [Checks](#checks)
  * [Options files](#options-files)
  * [Python environment](#python-environment)
* [Testing](#testing)
  * [Local testing](#local-testing)
  * [Continuous integration](#continuous-integration)
* [Submitting](#submitting)
  * [Merge requests](#merge-requests)
* [Monitoring](#monitoring)
* [Accessing output](#accessing-output)
* [Instructions for liaisons](#instructions-for-liaisons)
* [Technical support](#technical-support)

## Overview

Analysis Productions are a method by which data processing can be performed using DIRAC, most commonly to produce ntuples for analysis.
This is similar to "WG productions", with a focus on individual analyses and automated tooling.
Productions are normally run as follows:

1. Analysts create a merge request to this repository, adding their options and the associated metadata.
2. After the merge request is accepted, the production is submitted to LHCbDIRAC.
3. Productions are run using the DIRAC transformation system and can be monitored on the [`Analysis Productions` webpage](https://lhcb-analysis-productions.web.cern.ch/productions/).
4. After the transformations complete, the output data is replicated to CERN EOS.

## Creating a new Analysis Production

To create the configuration for a new analysis production, you will need to clone the repository and create a new branch:

```bash
git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/AnalysisProductions.git
cd AnalysisProductions
git checkout -b ${USER}/my-analysis
```

Then you need to create a directory containing an [`info.yaml`](#yaml-configuration) file and any [options files](#options-files) your jobs may need.

Once you have added these, you can commit and push your changes, which will be reviewed and subsequently approved.

```bash
git add <new directory>
git commit -m "<meaningful commit message>"
git push -u origin ${USER}/my-analysis
```

### YAML configuration

Write a file called `info.yaml` which will configure your jobs.

Each top-level key is the name of a job, and the value must be a dict whose allowed keys are:

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `application`               | string         | The application and version that you want to run, separated by a `/`. |
| `options`                   | string or list | The options files to pass to the application. |
| `input`                     | dict           | The input to the job. You can use `bk_query` for data in BookKeeping, or `job_name` to use the output of another job in the same production. |
| `output`                    | string         | The output file to be registered in BookKeeping. **NB:** must be upper-case. |
| `wg`                        | string         | The Working Group that the analysis belongs to. The allowed values are listed [here](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbapcommon/-/blob/master/src/LbAPCommon/config.py#L12-33). |
| `inform`                    | string or list | Email address(es) to inform about the status of the production. (Default empty) |
| `automatically_configure`\* | boolean        | Deduce common options based on the input data. (Default `false`, see [automatic configuration](#automatic-configuration)) |
| `turbo`\*                   | boolean        | Required to be `true` if using turbo stream data and `automatically_configure` is enabled (Default `false`) |
| `root_in_tes`\*             | string         | Set the value of `RootInTES` for when running over MDST input. Requires `automatically_configure` to be enabled |
| `checks`\*                  | dict           | Additional tasks to perform while testing a job. See [checks](#checks) |

*\* optional keys.*

A job can therefore be created like this:

```yaml
My_job:
  application: DaVinci/v45r4
  wg: WG
  automatically_configure: yes
  turbo: no
  inform:
    - someone@cern.ch
  options:
    - make_ntuple.py
  input:
    bk_query: /some/bookkeeping/path.DST
  output: DVNtuple.root
```

Instead of defining the same values for every job, you can use the special key `defaults`.

```yaml
defaults:
  application: DaVinci/v45r4
  wg: WG
  automatically_configure: yes
  turbo: no
  inform:
    - someone@cern.ch
  options:
    - make_ntuple.py
  output: DVNtuple.root

My_MagUp_job:
  input:
    bk_query: /some/MagUp/bookkeeping/path.DST

My_MagDown_job:
  input:
    bk_query: /some/MagDown/bookkeeping/path.DST
```

You can use the [Jinja](https://jinja.palletsprojects.com/) templating language to add some python functionality, *e.g.* looping over years and polarities.

```yaml
defaults:
  application: DaVinci/v45r4
  wg: WG
  automatically_configure: yes
  turbo: no
  inform:
    - someone@cern.ch
  options:
    - make_ntuple.py
  output: DVNtuple.root

{%- set datasets = [
  (11, 3500, '14', '21r1'),
  (12, 4000, '14', '21'),
  (15, 6500, '15a', '24r2'),
  (16, 6500, '16', '28r2'),
  (17, 6500, '17', '29r2'),
  (18, 6500, '18', '34'),
]%}

{%- for year, energy, reco, strip in datasets %}
  {%- for polarity in ['MagDown', 'MagUp'] %}

My_20{{year}}_{{polarity}}_job:
  input:
    bk_query: /LHCb/Collision{{year}}/Beam{{energy}}GeV-VeloClosed-{{polarity}}/Real Data/Reco{{reco}}/Stripping{{strip}}/90000000/BHADRON.MDST

  {%- endfor %}
{%- endfor %}

```

#### Automatic Configuration

If `automatically_configure` is enabled, the following attributes of the Gaudi application will be deduced:

- `DataType`
- `InputType`
- `Simulation`
- `Lumi` (takes the opposite value than `Simulation`)
- `CondDBtag` and `DDDBtag` (using `LatestGlobalTagByDataType` if running over real data)

Enabling `automatically_configure` also allows the attributes `Turbo` and `RootInTES` to be configured from keys in `info.yaml`.

#### Input

There are three ways to define what input a job should take.
##### bk_query
A bookkeeping query will specify a particular part of the Dirac bookkeeping to take input files from. All LFNs in this location will be used as input.
```python
2011_13164062_MagDown:
  input:
    bk_query: /MC/2011/Beam3500GeV-2011-MagDown-Nu2-Pythia8/Sim09f-ReDecay01/Trig0x40760037/Reco14c/Stripping21r1NoPrescalingFlagged/13164062/ALLSTREAMS.DST
  options:
    - MC_options/MC_13164062_ALLSTREAMS_options.py
```

##### transform_ids
Alternatively one could specify the transformation IDs that correspond to the desired data/MC. It might be that a given `bk_query` would correspond to two different MC samples so specifying the transformation IDs that correspond to just one of the samples will avoid running over undesired MC.
```python
2011_13164062_MagDown:
  input:
    transform_ids:
      - 132268
    filetype: ALLSTREAMS.DST
  options:
    - MC_options/MC_13164062_ALLSTREAMS_options.py
```

##### job_name
It is also possible to set up "job chains", so one job can take its input as the output of another job.
```python
2015_12163001_MagDown_Strip:
  application: DaVinci/v44r10p5
  input:
    bk_query: /MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09e-ReDecay01/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/12163001/ALLSTREAMS.DST
  options:
    - strip_options/strip_ALLSTREAMS_options.py
  output: B02D0KPI.STRIP.DST

2015_12163001_MagDown:
  input:
    job_name: 2015_12163001_MagDown_Strip
    filetype: B02D0KPI.STRIP.DST
  options:
    - MC_options/MC_12163001_B02D0KPI.STRIP_options.py
```

#### Checks

Optionally, some checks can be defined that are automatically run during the test of a job. You can use the special key `checks`, for example:

```yaml
checks:
  histogram_deltaM:
    type: range
    expression: Dstar_M-D0_M
    limits:
      min: 139
      max: 155
    blind_ranges:
      -
        min: 143
        max: 147
  at_least_50_entries:
    type: num_entries
    tree_pattern: TupleDstToD0pi_D0ToKK/DecayTree
    count: 50
```

For each check, you must specify a `type` - the types of checks currently defined are:

- `range` for 1D histograms
- `range_bkg_subtracted` for 1D histograms with background subtraction
- `range_nd` for 2-3D histograms
- `num_entries` to require a minimum number of entries
- `num_entries_per_invpb` to require a minimum number of entries per unit luminosity (for real data only)
- `branches_exist` to require that certain branches exist in the output ntuple

You can also specify a `tree_pattern` for each check. This is a regex that will be compared against all TTrees in the ntuple created by the test, and the check will be run on all matching trees. If not specified, this will take the default value of `r"(.*/DecayTree)|(.*/MCDecayTree)"`, i.e., the check will be run on all TTrees.

Each type of check has its own set of options used to define its behaviour. The full set of options for each check type are:

* `range`: creates a 1D histogram

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `expression`                | string         | The name of a branch (or a combination of branch names) to evaluate to plot on the horizontal histogram axis. |
| `limits`                    | dict           | Takes two keys: `min` and `max`, the values of which must both be floats. These define the min and max values of the horizontal histogram axis. |
| `blind_ranges`\*            | list or dict   | Defines ranges to blind, ie. ranges for which entries will not be included in the histogram. This uses similar `min`/`max` key syntax as the `limits` option uses. You can either provide a single range, or a list of ranges. |
| `exp_mean`\*                | float          | Expected mean value of the 1D distribution. This is an optional parameter. If provided, it is compared with the observed value and their agreement within the provided `mean_tolerance` is checked. |
| `exp_std`\*                 | float          | Expected value for the standard deviation of the 1D distribution. This is an optional parameter. If provided, it is compared with the observed value and their agreement whitin the provided `std_tolerance` is checked. |
| `mean_tolerance`\*          | float          | Maximum shift tolerated between expected and observed mean values. This is an optional parameter and it has to be provided only if also `exp_mean` is provided. |
| `std_tolerance`\*           | float          | Maximum shift tolerated between expected and observed values of standard deviation. This is an optional parameter and it has to be provided only if also `exp_std` is provided. |
| `n_bins`\*                  | int            | Number of bins to use in the histogram. The default is 50. |

* `range_bkg_subtracted`: creates a 1D histogram where the background component has been subtracted. **N.B.** the background is assumed to be linearly distributed on the control variable, and no fit is performed. In particular, signal ([m-s/2., m+s/2.]) and background ([m-b-delta, m-b] U [m+b, m+b+delta]) windows have to be defined on a control variable\*\*. Then, one histogram is created for events falling in the signal region and another histogram is created for events falling in the background region. The subtraction, using the proper scaling factor, is finally performed.

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `expression`                | string         | The name of a branch (or a combination of branch names) to evaluate to plot on the horizontal histogram axis. |
| `limits`                    | dict           | Takes two keys: `min` and `max`, the values of which must both be floats. These define the min and max values of the horizontal histogram axis. |
| `expr_for_subtraction`      | string         | The name of a control variable (branch or a combination of branch names) to be used to perform the background subtraction. |
| `mean_sig`                  | float          | Expected mean value of the `expr_for_subtraction` variable. The signal window will be centered around this value. |
| `background_shift`          | float          | Shift, w.r.t the `mean_sig` value, used to define the two background regions. |
| `background_window`         | float          | Size of the background windows. |
| `signal_window`             | float          | Size of the signal window. |
| `blind_ranges`\*            | list or dict   | Defines ranges to blind, ie. ranges for which entries will not be included in the histogram. This uses similar `min`/`max` key syntax as the `limits` option uses. You can either provide a single range, or a list of ranges. |
| `n_bins`\*                  | int            | Number of bins to use in the histograms. The default is 50. |

*\*\* Here m= `mean_sig`, s=`signal_window`, delta=`background_shift` and b=`background_window`*

* `range_nd`: creates a 2D or 3D histogram. If 3 axes are specified, both the full 3D histogram and the set of all 2D histograms are created.

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `expressions`               | dict           | Keys are the axes of the histograms, (in order) `x`, `y`, and `z`. Values for each are the expression for that axis, using the same syntax as `expression` for `range` checks. The `z` axis is optional. |
| `limits`                    | dict           | Keys are the axes of the histograms, (in order) `x`, `y`, and `z`. Values for each are a dictionary for max/min values for that axis, using the same syntax as `limits` for `range` checks. The `z` axis is optional. |
| `blind_ranges`\*            | list           | A list of dictionaries defining ranges that will be blinded. For each entry, the keys are the axes of the histograms, (in order) `x`, `y`, and `z`, and the values for each are a dictionary for max/min values for that axis, using the same syntax as `limits` for `range` checks. The `z` axis is optional. |
| `n_bins`\*                  | dict           | Keys are the axes of the histograms, (in order) `x`, `y`, and `z`. Values for each are an integer defining the number of bins to use in that axis of the histogram. The default number of bins for each axis is 50. The `z` axis is optional. |

* `num_entries`: requires a minimum number of entries in the test TTree

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `count`                | int           | The minimum number of entries required. |

* `num_entries_per_invpb`: requires a minimum number of entries in the test TTree per pb^-1 of luminosity. **N.B.** this will only work with real data because Monte Carlo has no luminosity information.

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `count_per_invpb`      | float         | The minimum number of entries required per inverse picobarn. |
| `lumi_pattern`\*        | string        | Regex used to identify the object inside the ntuple containing luminosity information. Default value is `(.*/LumiTuple)` |

* `branches_exist`: requires that the output ntuple contains a certain list of branches.

| Key | Type | Meaning |
|:-- | :-- | :-- |
| `branches`      | list          | A list of branch names that must be in the final ntuple. |

*\* optional keys.*

Checks are run when using `lb-ap test` for a job, eg. `lb-ap test MyProduction MyJobName`. Any histograms created by tests can be found in the `checks` folder within the `output` directory for that job.

Checks can also be run on the ntuple created in an earlier test using the `check` command. This saves you from having to re-run DaVinci if you're only changing your checks, and not your options files. This command requires the same two arguments as `lb-ap test`, plus one extra for the path to the ntuple from a previous test. For example: `lb-ap check MyProduction MyJobName local-tests/MyProduction-2021-06-10-18-09-01/output/00012345_00006789_1.D02KK.ROOT`. If you want to save the results of these checks to a different folder, you can provide an optional fourth argument with a directory into which to save these files (if not specified, this will be saved to the same `checks` folder as described above for `lb-ap test`)

### Options files

The options files to be used with your jobs must be placed in the same folder as `info.yaml` or a subdirectory of that folder.

### Python environment

The environment variables set by `AnalysisProductions.xenv` append the top-level directory of this datapackage to `$PYTHONPATH`.
This means that as long as you include `__init__.py` files in your folders, you can `import` them as Python modules without needing to manipulate the environment yourself.

For example, say you have the following directory structure, where `utils.py` contains some classes or functions that you want to use in `make_ntuple.py`:
```
AnalysisProductions/
└── MyAnalysis/
    ├── __init__.py
    ├── info.yaml
    ├── make_ntuple.py
    └── utils.py
```
In `make_ntuple.py` you can add:
```python
from MyAnalysis.utils import MyCoolClass, my_fantastic_function
```

## Testing

### Local testing

Local tests can be performed using the `lb-ap` command in the LHCb environment.
See [`LbAPLocal`](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbaplocal#lbaplocal) for more details.

### Continuous integration

Every commit that is pushed to this repository is tested using GitLab CI.
The CI logs will show a link which can be used for getting detailed information about the status of tests.

## Submitting

Open a merge request to master using the GitLab web interface.

**Note:** If you require a new release of DaVinci add the `DV release` label to your merge request to notify the DaVinci maintainer. They will set a two week deadline for new merge requests to be included in the new DaVinci release in order to collate new features.

### Merge requests

When creating your merge request:

* Add a label for your WG
* Mark as "WIP:" until you're happy with the CI results

**Note:** If the merge request supersedes a previous one, add a comment at the end of the header: `(supersedes !<MR number>)`.
Please state explicitly in the description if the superseded ntuples can be deleted immediately, or after the new merge request is finalised

When ready:

* Remove the "WIP:" from the title:
* Assign the merge request to you working group's DPA/RTA liaison

To find your working group's DPA/RTA liaison, see [this page](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/LHCbWGLiaisons)

(These instructions are also part of the template MR and appear in the header when a new MR is created.)

## Monitoring

The status of a production can be monitored on the [`Analysis Productions` webpage](https://lhcb-analysis-productions.web.cern.ch/productions/).
The previous monitoring on the [LHCbDirac web portal](https://lhcb-portal-dirac.cern.ch/DIRAC/?url_state=1|*LHCbDIRAC.AnalysisProductions.classes.AnalysisProductions:,) is being decommissioned.

## Accessing output

Currently the easiest way to access the output is using XRootD.
The page used for [monitoring](#monitoring) can be used for finding the XRootD URLs.

## Instructions for liaisons

1. Review the pull request checking:
    * Size of the output file doesn't seem excessive
    * The analysis isn't including too many variables in the ntuple
    * Encourage people to share productions between analyses where practical
    * **Note:** There is no need to be too strict, it's more important that productions are submitted promptly.
2. Approve and merge!
   **Note:** This requires membership in the [`lhcb-dpa-emtf-rta-liaisons`](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10387547) egroup. You are responsible for subscribing to it yourself.
3. When the CI pipeline finishes, ensure that a JIRA task was automatically created [here](https://its.cern.ch/jira/projects/WGP/)
    * The title should be "WG - ANALYSIS - AnalysisProductions VERSION"
    * @lhcbdpa should post the link to the JIRA task on the Merge Request.
    * The link to the JIRA task is additionally shown in the logs of the CI pipeline.


## Technical support

Questions cam be asked on the ["DPA WP2 Analysis Productions" mattermost channel](https://mattermost.web.cern.ch/lhcb/channels/dpa-wp2-analysis-productions).
