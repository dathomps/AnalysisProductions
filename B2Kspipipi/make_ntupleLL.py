from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper


theTuple = DecayTreeTuple("B_KspipipiTuple")
theTuple.Decay = "[B+ -> ^pi+ ^pi- ^pi+ ^(KS0 -> ^pi+ ^pi-)]CC"

theTuple.addBranches ({
    "B" : "[B+ -> pi+ pi- pi+ (KS0 -> pi+ pi-)]CC",
    "pi1" : "[B+ -> ^pi+ pi- pi+ (KS0 -> pi+ pi-)]CC",
    "pim" : "[B+ -> pi+ ^pi- pi+ (KS0 -> pi+ pi-)]CC",
    "pi2" : "[B+ -> pi+ pi- ^pi+ (KS0 -> pi+ pi-)]CC",
    "Ks" : "[B+ -> pi+ pi- pi+ ^(KS0 -> pi+ pi-)]CC",
    "pipK" : "[B+ -> pi+ pi- pi+ (KS0 -> ^pi+ pi-)]CC",
    "pimK" : "[B+ -> pi+ pi- pi+ (KS0 -> pi+ ^pi-)]CC"
    })
    
theTuple.addTupleTool('TupleToolTrackInfo')
##theTuple.addTupleTool('TupleToolKinematic')
#theTuple.addTupleTool('TupleToolBremInfo')
theTuple.addTupleTool('TupleToolAngles')

##theTuple.addTupleTool('TupleToolHelicity') #TupleTool Added manually (SK-Lesson)

if DaVinci().Simulation == True:
    theTuple.addTupleTool('TupleToolMCTruth')

TriggerList = [
   #in this case there aren't L0 trigger 'L0ElectronDecision','L0HadronDecision',
    'Hlt1TrackMVADecision',
    'Hlt2Topo2BodyDecision','Hlt2Topo3BodyDecision','Hlt2Topo4BodyDecision'
    ]
theTuple.addTupleTool('TupleToolTISTOS')
theTuple.TupleToolTISTOS.TriggerList = TriggerList
theTuple.TupleToolTISTOS.Verbose = True


LokiVariables = theTuple.addTupleTool('LoKi::Hybrid::TupleTool/LokiVariables')
LokiVariables.Variables = {
  "ETA" : "ETA",
  "M"   : "M" ,
  "PHI" : "PHI",
  #"LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" , #LoKi functor to constraint the J/Psi mass
  #"VCHI2NDOF" : "VFASPF(VCHI2/VDOF)"
    }

# Dealing w microDST (MDST file)
line = 'B2KShhh_PiPiPi_LLLine'
if DaVinci().Simulation == True:
    #This must be changed when the MC request is done
    theTuple.Inputs = ['/Event/B2KShhh.Strip/Phys/'+line+'/Particles']
else:
    DaVinci().RootInTES = "/Event/Bhadron"
    theTuple.Inputs = ['Phys/'+line+'/Particles']

DaVinci().UserAlgorithms += [theTuple]


#Those lines are necessary?
#DaVinci().Lumi = True
#if DaVinci().Simulation == True:
#    DaVinci().Lumi = False
