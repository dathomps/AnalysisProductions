###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test pass-through Sprucing line. Produces spruce_passthrough.dst"""
import os
from pathlib import Path

from Configurables import Moore
from Gaudi.Configuration import appendPostConfigAction
from Moore import options
from Moore.tcks import load_hlt2_configuration
from Moore.lines import PassLine

options.input_raw_format = 0.3
options.input_type = 'RAW'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'

options.output_file = 'spruce_passthrough.dst'
options.output_type = 'ROOT'

load_hlt2_configuration(Path(os.environ["ANALYSIS_PRODUCTIONS_BASE"]) / "FEST" / "tck.json")


def pass_through_line(name="PassThroughLine"):
    """Return a Sprucing line that performs no selection
    """
    return PassLine(name=name)


def make_lines():
    return [pass_through_line()]


@appendPostConfigAction
def dump():
    from Moore.tcks import dump_passthrough_configuration

    dump_passthrough_configuration(None, "passthro_FEST.tck.json")

moore = Moore()

moore.DDDBtag = 'dddb-20210617'
moore.CondDBtag = 'sim-20210617-vc-md100'

moore.spruce = True
moore.from_file = True
options.lines_maker = make_lines
