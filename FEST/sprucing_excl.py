"""Test running 2 different exclusive Sprucing lines on FEST data.

Produces `spruce_test_stream_A_FEST.dst`, `spruce_test_stream_B_FEST.dst`, `spruce_all_lines_FEST.tck.json`
"""
import os
from pathlib import Path

from Configurables import Moore
from Gaudi.Configuration import appendPostConfigAction
from Moore import options
from Moore.tcks import dump_sprucing_configuration, load_hlt2_configuration
from pprint import pprint
##Make dummy SpruceLine
from Moore import streams_spruce
from Moore.config import SpruceLine
from Hlt2Conf.lines.monitoring.v0_monitoring import make_ks0, filter_pions
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.reconstruction_objects import make_pvs_v2 as make_pvs
from Hlt2Conf.standard_particles import make_long_pions


options.input_raw_format = 0.3
options.input_type = 'RAW'

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'

options.output_file = 'spruce_{stream}_FEST.dst'
options.output_type = 'ROOT'

load_hlt2_configuration(Path(os.environ["ANALYSIS_PRODUCTIONS_BASE"]) / "FEST" / "tck.json")


#Make 2 different SpruceLines and make each one its own stream
def make_line1():
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs)
    return SpruceLine(
        name="SpruceTestLine_1",
        algs=upfront_reconstruction() + [make_ks0(pions, pvs, vertex_chi2=8)])


def make_line2():
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs)
    return SpruceLine(
        name="SpruceTestLine_2",
        algs=upfront_reconstruction() + [make_ks0(pions, pvs, max_ipchi2=8)])


def make_streams():
    linedict = dict(test_stream_A=[make_line1()], test_stream_B=[make_line2()])
    pprint(linedict)
    return linedict


#Define streams and the detector RawBanks they persist
streams_spruce.stream_banks = {
    "test_stream_A": ['ODIN', 'Rich'],
    "test_stream_B": ['ODIN']  # Remove RICH raw bank
}
print("Moore.streams_spruce.stream_banks")
pprint(streams_spruce.stream_banks)


@appendPostConfigAction
def dump():
    dump_sprucing_configuration(None, "spruce_all_lines_FEST.tck.json")

moore = Moore()

moore.DDDBtag = 'dddb-20210617'
moore.CondDBtag = 'sim-20210617-vc-md100'

moore.spruce = True
moore.from_file = True
options.lines_maker = make_streams
