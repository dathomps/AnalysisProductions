# WG productions options for Sigma+ -> p mu+ mu-

The options contained in this directory generate ntuples for:

1. Signal channel: Sigma+ -> p mu+ mu-

* `Sigma+ -> p+ mu+ mu- LONG or DOWNSTREAM`
* `Sigma+ -> p+ (KS0 -> mu+ mu-)`
* `Sigma+ -> p~- mu+ mu+K`

2. Control channel: K -> pi pi pi

* `K+ -> pi+ pi+ pi- LONG or DOWNSTREAM`

3. Control channel: Sigma+ -> p+ pi0

* `Sigma+ -> p+ pi0`

## Generating the need information for the `info.yaml` file

The `info.yaml` file can be filled using a script.
Examples of valid arguments:

```bash
./make_info_file.py --help
./make_info_file.py --years 2016 --polarities MagDown --tag-types Sigma2PMuMuData  --data-types RealData
```
Valid years are 2016, 2017 or 2018.
Valid polarities are MagDown or MagUp.
Valid data-types are RealData or MC.
If you specify RealData you can only specify tag-types Sigma2PMuMuData and you will get all of the 3 aforementioned ntuples.
If you instead specify MC you can separately specify as a tag-type: Sigma2PMuMu_MC, K2pipipi_MC or Sigma2PPi0_MC.
