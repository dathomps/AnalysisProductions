# ============================================================
# @author Francesco Dettori
######## options ########
debug = False
data = 0                   # data or MC
    
#datatype = "2012"          # this counts for data
evtot=   -1
FirstEv = 1
if not debug :
    evtot = -1


inclusiveMCTruth = True  #true for inclusive ntupes
isPGun = False         
doStripping   = False  #True if MC are not stripping flagged
veloMuonTracking = False

useTCK = False #if True TCK file has to be updated

myTCK = hex(0x40730035) #not used

onlySigma = useTCK

rightDatabase = True
printTree = False
MCFiltered = False
microDST = False
isMDST = False
doITClusters  = False
doTuples      = True
doMatchedNtuples = True
doMCTruth     = True

if data : #lets be safe
    doMCTruth = False
    doMatchedNtuples = False
    inclusiveMCTruth = False
if not doTuples :
    doMCTruth = False
    inclusiveMCTruth = False
#########################################################################
import GaudiKernel.SystemOfUnits as Units 
from Gaudi.Configuration import *
####################################################################### 
from Configurables import GaudiSequencer, DaVinci,SelDSTWriter, CombineParticles, OfflineVertexFitter 
from PhysSelPython.Wrappers import DataOnDemand,Selection,SelectionSequence,AutomaticData
####################################################################### 
from Configurables import  DecayTreeTuple , TupleToolDecay, TupleToolTrigger, TupleToolGeometry, MCDecayTreeTuple, TupleToolTISTOS, TupleToolNeutrinoReco, TupleToolMuonPid, TupleToolDalitz, TupleToolAngles,  TupleToolVtxDaughters
from Configurables import MCDecayTreeTuple, MCTupleToolKinematic, MCTupleToolHierarchy, LoKi__Hybrid__MCTupleTool, MCTupleToolReconstructed, TupleToolANNPID
from Configurables import TupleToolTrackIsolation 
##################################################################################333#333
from Configurables import LoKi__Hybrid__TupleTool
#from StandardParticles import StdAllNoPIDsPions, StdNoPIDsDownPions, StdAllNoPIDsProtons, StdNoPIDsDownProtons, StdLoosePions, StdLooseProtons

#===============================
# Get various TCKs
#mySeq=GaudiSequencer("MoveTCKtoNewLocation")
if useTCK:
    from Configurables import RawEventJuggler

    RawEventJuggler().Input=3.1 #change to suit
    RawEventJuggler().Output=2.0 #change to suit
    RawEventJuggler().DataOnDemand=True
    RawEventJuggler().TCK=myTCK
#juggler = RawEventJuggler( DataOnDemand=True, Input=3.2, Output=4.1 )
#if getTCK:

#    juggler.TCK=myTCK
#DaVinci().UserAlgorithms  +=[ mySeq ]
#MessageSvc().setVerbose   +={"RawEventJuggler"};

from Configurables import EventCountHisto,  LoKi__Hybrid__EvtTupleTool,  LoKi__Hybrid__TupleTool, LoKi__VoidFilter

onepv = LoKi__VoidFilter('AtLeastOnePV', Code = "CONTAINS('Rec/Vertex/Primary') > 0")

DaVinci().MoniSequence += [EventCountHisto("DaVinciMonitor") ] 
    
    

from Configurables import HltDecReportsDecoder, HltSelReportsDecoder, HltVertexReportsDecoder, L0DUFromRawTool
#DataOnDemandSvc().AlgMap["Hlt/DecReports"] = HltDecReportsDecoder(OutputLevel = WARNING)
#DataOnDemandSvc().AlgMap["Hlt/SelReports"] = HltSelReportsDecoder(OutputLevel = WARNING)
#DataOnDemandSvc().AlgMap["Hlt/VertexReports"] = HltVertexReportsDecoder( OutputLevel = WARNING)
#DataOnDemandSvc().AlgMap["L0/L0DUReport"] = L0DUFromRawTool( OutputLevel = FATAL)
DataOnDemandSvc().OutputLevel = DEBUG


## ----  IT clusters decoding  --- ##
if doITClusters : 
    from Configurables import RawBankToSTClusterAlg
    createITClusters = RawBankToSTClusterAlg("CreateITClusters")
    createITClusters.DetType     = "IT"
    mySequencer = GaudiSequencer('mySequencer')
    mySequencer.Members += [ createITClusters ]
    DaVinci().UserAlgorithms  +=[ mySequencer ]
## ---- Define  Input locations  -----
prefix = "" #RareStrangeSigmaPMuMu.Strip/"
#commonname = "RareStrange"
#commonname = "RareStrangeSigmaPMuMu.Strip"
commonname = "RareStrange"

if data :
  #prefix += "DimuonVELORAW/" ##OK only for DiMuon lines!
  prefix += "Leptonic/" ##OK only for DiMuon lines!
else :
    if MCFiltered :
        #prefix += "MCFilter/"
        #prefix += ""# "Strip/"
        prefix = "RareStrangeSigmaPMuMu.Strip/"
    else : 
        prefix += "AllStreams/" ##one container for all MC!

  
## --- default Stripping location

if doStripping :
    prefix = ""
    commonname ="RareStrange"

mdst_RootInTES = "/Event/Leptonic/"
RelInfoPrefix  = "/Event/"

SigmaPMuMuInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLine/Particles"
SigmaPMuMuDownInput  = prefix+"Phys/"+commonname+"SigmaPMuMuDownLine/Particles"
SigmaPMuMuDetInput  = prefix+"Phys/"+commonname+"SigmaPMuMuDetLine/Particles"
SigmaPMuMuLFVInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLFVLine/Particles"
SigmaPMuMuLFVDownInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLFVDownLine/Particles"

SigmaPEEInput     = prefix+"Phys/"+commonname+"SigmaPEELine/Particles"
SigmaPEEDownInput = prefix+"Phys/"+commonname+"SigmaPEEDownLine/Particles"
SigmaPEEDetInput  = prefix+"Phys/"+commonname+"SigmaPEEDetLine/Particles"

SigmaPEMuInput     = prefix+"Phys/"+commonname+"SigmaPEMuLine/Particles"

SigmaPPi0CalInput = prefix+"Phys/"+commonname+"SigmaPPi0CalLine/Particles"
SigmaPPi0DalInput = prefix+"Phys/"+commonname+"SigmaPPi0Line/Particles"

KPiMuMuInput = prefix+ "Phys/"+commonname+"KPiMuMuLine/Particles"
KPiMuMuDownInput = prefix+ "Phys/"+commonname+"KPiMuMuDownLine/Particles"
KPiMuMuLFVInput = prefix+ "Phys/"+commonname+"KPiMuMuLFVLine/Particles"
KPiMuMuLFVDownInput = prefix+ "Phys/"+commonname+"KPiMuMuLFVDownLine/Particles"

KPiPiPiInput = prefix+ "Phys/"+commonname+"KPiPiPiLine/Particles"
KPiPiPiDownInput = prefix+ "Phys/"+commonname+"KPiPiPiDownLine/Particles"
KPiPiPiMassMeasInput = prefix+ "Phys/"+commonname+"KPiPiPiMassMeasLine/Particles"
KPiPiPiMassMeasDownInput = prefix+ "Phys/"+commonname+"KPiPiPiMassMeasDownLine/Particles"

LambdaPPiInput = prefix+ "Phys/"+commonname+"LambdaPPiLine/Particles"
LambdaPPiEEInput = prefix+ "Phys/"+commonname+"LambdaPPiEELine/Particles"

PhiKMuInput = prefix+ "Phys/"+commonname+"PhiKMuLine/Particles"
    
if doMatchedNtuples:
    _Preambulo = ["from LoKiPhysMC.decorators import *","from LoKiPhysMC.functions import mcMatch"]
    _stdAllNoPIDsMuons   = DataOnDemand(Location = "Phys/StdAllNoPIDsMuons/Particles")
    _stdAllNoPIDsElectrons   = DataOnDemand(Location = "Phys/StdAllNoPIDsElectrons/Particles")
    _stdNoPIDsDownElectrons   = DataOnDemand(Location = "Phys/StdNoPIDsDownElectrons/Particles")
    _stdAllNoPIDsProtons   = DataOnDemand(Location = "Phys/StdAllNoPIDsProtons/Particles")
    _stdNoPIDsDownProtons   = DataOnDemand(Location = "Phys/StdAllNoPIDsDownProtons/Particles")
    _stdAllNoPIDsPions   = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
    _stdLooseResolvedPi0   = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
    _stdLooseMergedPi0   = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")

    MatchedSigmaPMuMuComb =  CombineParticles("MatchedSigmaPMuMuComb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedSigmaPMuMuComb.DecayDescriptor = "[Sigma+ -> p+ mu+ mu-]cc"
    MatchedSigmaPMuMuComb.Preambulo = _Preambulo
    MatchedSigmaPMuMuComb.addTool( OfflineVertexFitter )
    MatchedSigmaPMuMuComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedSigmaPMuMuComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedSigmaPMuMuComb.ReFitPVs = False
    MatchedSigmaPMuMuComb.DaughtersCuts = { "p+" : "mcMatch( '[p+]cc' )",
                                            "mu+" : "mcMatch( '[mu+]cc' )"
                                            }
    MatchedSigmaPMuMuComb.MotherCut     =  "(mcMatch('[Sigma+ => p+ mu+ mu-]CC'))" ##give error "configure me!" if not set

  
    SelMSigmaPMuMu = Selection("MatchSigmaPMuMu", 
                            Algorithm = MatchedSigmaPMuMuComb,
                            RequiredSelections=[       _stdAllNoPIDsMuons, _stdAllNoPIDsProtons   ])
    SeqMyMatchedSigmaPMuMu = SelectionSequence('SeqMyMatchedSigmaPMuMu', TopSelection  =  SelMSigmaPMuMu, EventPreSelector = [onepv])
    SequenceMatchSigmaPMuMu = GaudiSequencer( "SequenceMatchSigmaPMuMu" )
    SequenceMatchSigmaPMuMu.Members += [ SeqMyMatchedSigmaPMuMu.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchSigmaPMuMu ]


    MatchedSigmaPPi0Comb =  CombineParticles("MatchedSigmaPPi0Comb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedSigmaPPi0Comb.DecayDescriptor = "[Sigma+ -> p+ pi0]cc"
    MatchedSigmaPPi0Comb.Preambulo = _Preambulo
    MatchedSigmaPPi0Comb.ReFitPVs = False
    MatchedSigmaPPi0Comb.DaughtersCuts = { "p+" : "mcMatch( '[p+]cc' )",
                                            "pi0" : "mcMatch( 'pi0' )"
                                            }
    MatchedSigmaPPi0Comb.MotherCut     =  "(mcMatch('[Sigma+ => p+ pi0]CC'))" ##give error "configure me!" if not set

  
    SelMSigmaPPi0 = Selection("MatchSigmaPPi0", 
                            Algorithm = MatchedSigmaPPi0Comb,
                            RequiredSelections=[       _stdLooseResolvedPi0, _stdLooseMergedPi0, _stdAllNoPIDsProtons   ])
    SeqMyMatchedSigmaPPi0 = SelectionSequence('SeqMyMatchedSigmaPPi0', TopSelection  =  SelMSigmaPPi0, EventPreSelector = [onepv])
    SequenceMatchSigmaPPi0 = GaudiSequencer( "SequenceMatchSigmaPPi0" )
    SequenceMatchSigmaPPi0.Members += [ SeqMyMatchedSigmaPPi0.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchSigmaPPi0 ]



    MatchedSigmaPMuMuLFVComb =  CombineParticles("MatchedSigmaPMuMuLFVComb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedSigmaPMuMuLFVComb.DecayDescriptor = "[Sigma+ -> p~- mu+ mu+]cc"
    MatchedSigmaPMuMuLFVComb.Preambulo = _Preambulo
    MatchedSigmaPMuMuLFVComb.addTool( OfflineVertexFitter )
    MatchedSigmaPMuMuLFVComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedSigmaPMuMuLFVComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedSigmaPMuMuLFVComb.ReFitPVs = False
    MatchedSigmaPMuMuLFVComb.DaughtersCuts = { "p~-" : "mcMatch( '[p+]cc' )",
                                            "mu+" : "mcMatch( '[mu+]cc' )"
                                            }
    MatchedSigmaPMuMuLFVComb.MotherCut     =  "(mcMatch('[Sigma+ => p~- mu+ mu+]CC'))" ##give error "configure me!" if not set

  
    SelMSigmaPMuMuLFV = Selection("MatchSigmaPMuMuLFV", 
                            Algorithm = MatchedSigmaPMuMuLFVComb,
                            RequiredSelections=[       _stdAllNoPIDsMuons, _stdAllNoPIDsProtons   ])
    SeqMyMatchedSigmaPMuMuLFV = SelectionSequence('SeqMyMatchedSigmaPMuMuLFV', TopSelection  =  SelMSigmaPMuMuLFV, EventPreSelector = [onepv])
    SequenceMatchSigmaPMuMuLFV = GaudiSequencer( "SequenceMatchSigmaPMuMuLFV" )
    SequenceMatchSigmaPMuMuLFV.Members += [ SeqMyMatchedSigmaPMuMuLFV.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchSigmaPMuMuLFV ]


    MatchedSigmaPEEComb =  CombineParticles("MatchedSigmaPEEComb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedSigmaPEEComb.DecayDescriptor = "[Sigma+ -> p+ e+ e-]cc"
    MatchedSigmaPEEComb.Preambulo = _Preambulo
    MatchedSigmaPEEComb.addTool( OfflineVertexFitter )
    MatchedSigmaPEEComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedSigmaPEEComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedSigmaPEEComb.ReFitPVs = False
    MatchedSigmaPEEComb.DaughtersCuts = { "p+" : "mcMatch( '[p+]cc' )",
                                            "e+" : "mcMatch( '[e+]cc' )"
                                            }
    MatchedSigmaPEEComb.MotherCut     =  "(mcMatch('[Sigma+ -> p+ pi0]CC'))" ##give error "configure me!" if not set

  
    SelMSigmaPEE = Selection("MatchSigmaPEE", 
                            Algorithm = MatchedSigmaPEEComb,
                            RequiredSelections=[       _stdAllNoPIDsElectrons, _stdAllNoPIDsProtons   ])
    SeqMyMatchedSigmaPEE = SelectionSequence('SeqMyMatchedSigmaPEE', TopSelection  =  SelMSigmaPEE, EventPreSelector = [onepv])
    SequenceMatchSigmaPEE = GaudiSequencer( "SequenceMatchSigmaPEE" )
    SequenceMatchSigmaPEE.Members += [ SeqMyMatchedSigmaPEE.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchSigmaPEE ]


    MatchedSigmaPEEDownComb =  CombineParticles("MatchedSigmaPEEDownComb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedSigmaPEEDownComb.DecayDescriptor = "[Sigma+ -> p+ e+ e-]cc"
    MatchedSigmaPEEDownComb.Preambulo = _Preambulo
    MatchedSigmaPEEDownComb.addTool( OfflineVertexFitter )
    MatchedSigmaPEEDownComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedSigmaPEEDownComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedSigmaPEEDownComb.ReFitPVs = False
    MatchedSigmaPEEDownComb.DaughtersCuts = { "p+" : "mcMatch( '[p+]cc' )",
                                            "e+" : "mcMatch( '[e+]cc' )"
                                            }
    MatchedSigmaPEEDownComb.MotherCut     =  "(mcMatch('[Sigma+ -> p+ pi0]CC'))" ##give error "configure me!" if not set

  
    SelMSigmaPEEDown = Selection("MatchSigmaPEEDown", 
                            Algorithm = MatchedSigmaPEEDownComb,
                            RequiredSelections=[       _stdNoPIDsDownElectrons, _stdNoPIDsDownProtons   ])
    SeqMyMatchedSigmaPEEDown = SelectionSequence('SeqMyMatchedSigmaPEEDown', TopSelection  =  SelMSigmaPEEDown, EventPreSelector = [onepv])
    SequenceMatchSigmaPEEDown = GaudiSequencer( "SequenceMatchSigmaPEEDown" )
    SequenceMatchSigmaPEEDown.Members += [ SeqMyMatchedSigmaPEEDown.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchSigmaPEEDown ]

    ## MatchedSigmaPGammaComb =  CombineParticles("MatchedSigmaPGammaComb") ## makes a particle out of the two muons
    ## ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## ## otherwise you get double candidates 
    ## MatchedSigmaPGammaComb.DecayDescriptor = "[Sigma+ -> p+ gamma]cc"
    ## MatchedSigmaPGammaComb.Preambulo = _Preambulo
    ## MatchedSigmaPGammaComb.addTool( OfflineVertexFitter )
    ## MatchedSigmaPGammaComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    ## MatchedSigmaPGammaComb.OfflineVertexFitter.useResonanceVertex = False
    ## MatchedSigmaPGammaComb.ReFitPVs = False
    ## MatchedSigmaPGammaComb.DaughtersCuts = { "p+" : "mcMatch( '[p+]cc' )",
    ##                                         "gamma" : "mcMatch( 'gamma' )"
    ##                                         }
    ## MatchedSigmaPGammaComb.MotherCut     =  "(mcMatch('[Sigma+ -> p+ gamma]CC'))" ##give error "configure me!" if not set

  
    ## SelMSigmaPGamma = Selection("MatchSigmaPGamma", 
    ##                         Algorithm = MatchedSigmaPGammaComb,
    ##                         RequiredSelections=[       _stdAllNoPIDsElectrons, _stdAllNoPIDsProtons   ])
    ## SeqMyMatchedSigmaPGamma = SelectionSequence('SeqMyMatchedSigmaPGamma', TopSelection  =  SelMSigmaPGamma)
    ## SequenceMatchSigmaPGamma = GaudiSequencer( "SequenceMatchSigmaPGamma" )
    ## SequenceMatchSigmaPGamma.Members += [ SeqMyMatchedSigmaPGamma.sequence() ]
    ## DaVinci().UserAlgorithms += [ SequenceMatchSigmaPGamma ]



    MatchedKPiMuMuComb =  CombineParticles("MatchedKPiMuMuComb") ## makes a particle out of the two muons
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedKPiMuMuComb.DecayDescriptor = "[K+ -> pi+ mu+ mu-]cc"
    MatchedKPiMuMuComb.Preambulo = _Preambulo
    MatchedKPiMuMuComb.addTool( OfflineVertexFitter )
    MatchedKPiMuMuComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedKPiMuMuComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedKPiMuMuComb.ReFitPVs = False
    MatchedKPiMuMuComb.DaughtersCuts = { "pi+" : "mcMatch( '[pi+]cc' )",
                                            "mu+" : "mcMatch( '[mu+]cc' )"
                                            }
    MatchedKPiMuMuComb.MotherCut     =  "(mcMatch('[K+ => pi+ mu+ mu-]CC'))" ##give error "configure me!" if not set

  
    SelMKPiMuMu = Selection("MatchKPiMuMu", 
                            Algorithm = MatchedKPiMuMuComb,
                            RequiredSelections=[       _stdAllNoPIDsMuons, _stdAllNoPIDsPions   ])
    SeqMyMatchedKPiMuMu = SelectionSequence('SeqMyMatchedKPiMuMu', TopSelection  =  SelMKPiMuMu, EventPreSelector = [onepv])
    SequenceMatchKPiMuMu = GaudiSequencer( "SequenceMatchKPiMuMu" )
    SequenceMatchKPiMuMu.Members += [ SeqMyMatchedKPiMuMu.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchKPiMuMu ]

    MatchedKPiPiPiComb =  CombineParticles("MatchedKPiPiPiComb") ## makes a particle out of the two pions
    ## Note this the DecayDescriptor here is just the name given to the output candidates do NOT put cc!
    ## otherwise you get double candidates 
    MatchedKPiPiPiComb.DecayDescriptor = "[K+ -> pi+ pi+ pi-]cc"
    MatchedKPiPiPiComb.Preambulo = _Preambulo
    MatchedKPiPiPiComb.addTool( OfflineVertexFitter )
    MatchedKPiPiPiComb.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
    MatchedKPiPiPiComb.OfflineVertexFitter.useResonanceVertex = False
    MatchedKPiPiPiComb.ReFitPVs = False
    MatchedKPiPiPiComb.DaughtersCuts = { "pi+" : "mcMatch( '[pi+]cc' )"
                                            }
    MatchedKPiPiPiComb.MotherCut     =  "(mcMatch('[K+ ==> pi+ pi+ pi-]CC'))" ##give error "configure me!" if not set


    SelMKPiPiPi = Selection("MatchKPiPiPi", 
                            Algorithm = MatchedKPiPiPiComb,
                            RequiredSelections=[    _stdAllNoPIDsPions   ])
    SeqMyMatchedKPiPiPi = SelectionSequence('SeqMyMatchedKPiPiPi', TopSelection  =  SelMKPiPiPi, EventPreSelector = [onepv])
    SequenceMatchKPiPiPi = GaudiSequencer( "SequenceMatchKPiPiPi" )
    SequenceMatchKPiPiPi.Members += [ SeqMyMatchedKPiPiPi.sequence() ]
    DaVinci().UserAlgorithms += [ SequenceMatchKPiPiPi ]





########################
#   Prepare VELO stuff #    
########################
from Configurables import  FastVeloTracking, TrackPrepareVelo, DecodeVeloRawBuffer, TrackEventFitter, TrackMasterFitter,StoreExplorerAlg

storeExp = StoreExplorerAlg()
storeExp.Load = 1
storeExp.PrintFreq = 1.0
storeExp.OutputLevel = 3

name=""
preve = TrackPrepareVelo("preve")
preve.inputLocation = "Rec/Track/Velo"
preve.outputLocation = "Rec/Track/UnFittedVelo"
preve.bestLocation = ""

#TODO: apparently FastVelo is now (april 2012) run with fixes in the production which don't neccessarily apply to the stripping...
if veloMuonTracking : 
    alg = GaudiSequencer("VeloMuonTrackingFor"+name,
                         Members = [ DecodeVeloRawBuffer(name+"VeloDecoding",DecodeToVeloLiteClusters=True,DecodeToVeloClusters=True),
                                     FastVeloTracking(name+"FastVelo",OutputTracksName="Rec/Track/Velo"),
                                     preve])
    





############################
### Configure the ntuple ###
############################

myNTUPLE = DecayTreeTuple('myNTUPLE')
#if isMDST:
#    myNTUPLE.RootInTES = mdst_RootInTES

myNTUPLE.OutputLevel = WARNING # silence please
myNTUPLE.IgnoreP2PVFromInputLocations = True # ignore all stored Particle -> PV relations
myNTUPLE.ReFitPVs = False # re-fit the PVs
myNTUPLE.ToolList +=  [  "TupleToolGeometry"
                         , "TupleToolKinematic"
                          , "TupleToolEventInfo"
                          , "TupleToolPid"
                          , "TupleToolPi0Info"
#                          , "TupleToolPropertime"
                          , "TupleToolRecoStats"
                          , "TupleToolTrigger"
                          , "TupleToolTISTOS"
                         , "TupleToolTrackInfo"
                         , "TupleToolDalitz"
                         , "TupleToolAngles"
                         , "TupleToolVtxDaughters"
#                         , "TupleToolTrackIsolation"
#                         ,  "TupleToolMuonPid"
#                         ,"LoKi::Hybrid::EvtTupleTool/LoKiEvent"
#                         ,"LoKi::Hybrid::TupleTool/LoKi_All"

#                         ,"LoKi::Hybrid::TupleTool/Isolations"
#                         ,"TupleToolVeloTrackMatch"
                          ]

if isPGun:
    myNTUPLE.remove("TupleToolTrigger")
    myNTUPLE.remove("TupleToolTISTOS")




## ----------  Store Triggers  ---------##

L0Triggers = ["L0MuonDecision" , "L0DiMuonDecision", "L0HadronDecision","L0HadronNoSPDDecision", "L0ElectronDecision", "L0ElectronHiDecision", "L0PhotonDecision", "L0PhotonHiDecision",
              "L0DiEM,lowMultDecision",  "L0DiHadron,lowMultDecision", "L0DiMuon,lowMultDecision" , "L0DiMuonNoSPDDecision", "L0Electron,lowMultDecision",
              "L0Muon,minbiasDecision", "L0Muon,lowMultDecision", "L0MuonNoSPDDecision"]        


Hlt1Triggers = [  "Hlt1SingleMuonNoIPDecision"  ,"Hlt1SingleMuonHighPTDecision"
                  ,"Hlt1SingleElectronNoIPDecision" ,"Hlt1SingleElectronHighPTDecision"
                   ,'Hlt1TrackAllL0Decision'  ,'Hlt1TrackAllL0TightDecision'   ,'Hlt1TrackMuonDecision'  ,'Hlt1TrackPhotonDecision'
                  ,"Hlt1DiMuonLowMassDecision" ,"Hlt1DiMuonHighMassDecision"
                  , "Hlt1MBNoBiasDecision", "Hlt1NoPVPassThroughDecision", "Hlt1VertexDisplVertexDecision"                  
                  ,'Hlt1MB.*Decision']

Hlt2Triggers = [
    ## muon lines
    "Hlt2SingleMuonDecision", "Hlt2SingleMuonLowPTDecision", "Hlt2SingleMuonHighPTDecision",
    "Hlt2DiMuonDecision",  "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",  "Hlt2DiMuonJPsiHighPTDecision",  "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonDetachedDecision",  "Hlt2DiMuonDetachedJPsiDecision", "Hlt2DiMuonDetachedHeavyDecision", "Hlt2TriMuonTauDecision", 
    ## hadron/Topo lines
    "Hlt2B2HHDecision",
    "Hlt2DiMuonBDecision",  'Hlt2DiMuonZDecision',
    "Hlt2TopoMu2BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",   "Hlt2Topo3BodyBBDTDecision",   "Hlt2Topo4BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision", "Hlt2Topo3BodySimpleDecision", "Hlt2Topo4BodySimpleDecision",
    ##others
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
    "Hlt2IncPhiDecision",
    ## inclusive decisions
    "Hlt2DiMuonDY.*Decision","Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", 'Hlt2DiElectron.*Decision', 'Hlt2.*GammaDecision'
    ]

Hlt1TriggersRunII = [  "Hlt1SingleMuonNoIPDecision"  ,"Hlt1SingleMuonHighPTDecision"
                  ,"Hlt1SingleElectronNoIPDecision" ,"Hlt1SingleElectronHighPTDecision"
                   ,'Hlt1TrackMVADecision'  , 'Hlt1TwoTrackMVADecision'
                   ,'Hlt1TrackMuonDecision'  , "Hlt1DiMuonLowMassDecision" ,"Hlt1DiMuonHighMassDecision", "Hlt1DiMuonNoL0Decision","Hlt1DiMuonNoIPDecision"
                  , "Hlt1MBNoBiasDecision", "Hlt1NoPVPassThroughDecision", "Hlt1VertexDisplVertexDecision" , "Hlt1MultiMuonNoL0Decision","Hlt1MultiMuonDiMuonNoIPDecision"
                  ,'Hlt1LowMultDecision'
                  ,'Hlt1MB.*Decision']


Hlt2TriggersRunII = [
    ## muon lines
    "Hlt2SingleMuonDecision", "Hlt2SingleMuonRareDecision", "Hlt2SingleMuonLowPTDecision", "Hlt2SingleMuonHighPTDecision",
    "Hlt2DiMuonDecision",  "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",  "Hlt2DiMuonJPsiHighPTDecision",  "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonDetachedDecision",  "Hlt2DiMuonDetachedJPsiDecision", "Hlt2DiMuonDetachedHeavyDecision", "Hlt2TriMuonTauDecision",
    "Hlt2DiMuonSoftDecision", "Hlt2LowMultDiMuon",
    ## hadron/Topo lines
    "Hlt2B2HHDecision",
    "Hlt2DiMuonBDecision",  'Hlt2DiMuonZDecision',
    "Hlt2TopoMu2BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",   "Hlt2Topo3BodyBBDTDecision",   "Hlt2Topo4BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision", "Hlt2Topo3BodySimpleDecision", "Hlt2Topo4BodySimpleDecision",
    
    ##others
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
    "Hlt2IncPhiDecision",
    # Exclusive lines,
    "Hlt2RareStrangeSigmaPMuMuDecision", "Hlt2RareStrangeKPiMuMuDecision", "Hlt2RareStrangeKPiMuMuDecision", "Hlt2StrangeKPiPiPiDecision",
    ## inclusive decisions
    "Hlt2DiMuonDY.*Decision","Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", 'Hlt2DiElectron.*Decision', 'Hlt2.*GammaDecision'
    ]



#triggerListF = L0Triggers + Hlt1Triggers + Hlt2Triggers
#if datatype == "2015" :
triggerListF = L0Triggers + Hlt1TriggersRunII + Hlt2TriggersRunII

if useTCK:
    # get your lines from TCK
    from triggerlines import *
    if myTCK in triggerlines.keys() :
        print "xxxxxxx"
        for i in triggerlines[myTCK]['L0'].keys() :
            L0Triggers.append('L0'+i+'Decision')
        print L0Triggers
        L0Triggers = list(set(L0Triggers))
        print L0Triggers
        for i in triggerlines[myTCK]['HLT1'].keys() :
            Hlt1Triggers.append(i+'Decision')
        for i in triggerlines[myTCK]['HLT2'].keys() :
            Hlt2Triggers.append(i+'Decision')

        Hlt1Triggers = list(set(Hlt1Triggers))
        Hlt2Triggers = list(set(Hlt2Triggers))

    else:
        print " ERROR TCK not found "
        

    triggerListF = L0Triggers + Hlt1Triggers + Hlt2Triggers




if not isPGun: 
    myNTUPLE.addTool(TupleToolTISTOS)
    myNTUPLE.TupleToolTISTOS.VerboseL0 = True
    myNTUPLE.TupleToolTISTOS.VerboseHlt1 = True
    myNTUPLE.TupleToolTISTOS.VerboseHlt2 = True
    myNTUPLE.TupleToolTISTOS.OutputLevel = WARNING
    myNTUPLE.TupleToolTISTOS.TriggerList = triggerListF
    #myNTUPLE.TupleToolTISTOS.PIDList = ["211", "13"]


myNTUPLE.addTool(TupleToolGeometry)
myNTUPLE.TupleToolGeometry.Verbose = True
myNTUPLE.addTool(TupleToolANNPID)
myNTUPLE.TupleToolANNPID.ANNPIDTunes= ["MC15TuneDNNV1", "MC15TuneCatBoostV1", "MC15TuneDNNV1", "MC15TuneFLAT4dV1", "MC15TuneV1"]

myNTUPLE.addTool(TupleToolVtxDaughters)
myNTUPLE.TupleToolVtxDaughters.FillSubVtxInfo = True
#myNTUPLE.addTool(TupleToolDalitz)
#myNTUPLE.TupleToolDalitz.OutputLevel = VERBOSE


## ---------- For storing some event variables   ---------##
from DecayTreeTuple.Configuration import *

if not microDST : 
    LoKiEventTuple = LoKi__Hybrid__EvtTupleTool("LoKiEvent")
    LoKiEventTuple.Preambulo = [ 
        "from LoKiTracks.decorators import *",
        "from LoKiCore.functions import *"
        ]
    LoKiEventTuple.VOID_Variables =  {
        "nTTCls"   :  "CONTAINS('Raw/TT/Clusters') " , ## number of Clusters in TT 
        "nVeloTrks"   :   " TrSOURCE('Rec/Track/Best' , TrVELO) >> TrSIZE " , ## number of Velo tracks
        "nLongTrks"   :   " TrSOURCE('Rec/Track/Best', TrLONG) >> TrSIZE " , ## number of Long tracks
        "nDownTrks"   :   " TrSOURCE('Rec/Track/Best', TrDOWNSTREAM) >> TrSIZE " , ## number of Down tracks
        "nTTrks"      :   " TrSOURCE('Rec/Track/Best', TrTTRACK) >> TrSIZE ",  ## number of T tracks
        }

    myNTUPLE.addTool(LoKiEventTuple)


## ---------- Yet some other stuff   ---------##
LoKiTuple = myNTUPLE.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
#LoKiTuple = LoKi__Hybrid__TupleTool("LoKi_All")
LoKiTuple.Variables = {"BPVDIRA" : "BPVDIRA"
                       , "BPVVDCHI2" : "BPVVDCHI2"
                       , "BPVIPCHI2" : "BPVIPCHI2()"
                       , "BPVVDZ" : "BPVVDZ"
                       , "VFASPF" : "VFASPF(VCHI2/VDOF)"
                       , "DOCAMAX" : "DOCAMAX"
                       , "MINIPCHI2": "MIPCHI2DV(PRIMARY)"
                       }
myNTUPLE.addTool(LoKiTuple, name = "LoKi_All")




myNTUPLE.addTool(TupleToolDecay, name="Sigma")
myNTUPLE.addTool(TupleToolDecay, name="proton")
myNTUPLE.addTool(TupleToolDecay, name="muplus")
myNTUPLE.addTool(TupleToolDecay, name="muminus")
myNTUPLE.addTool(TupleToolDecay, name="pizero")
myNTUPLE.addTool(TupleToolDecay, name="eplus")
myNTUPLE.addTool(TupleToolDecay, name="eminus")
myNTUPLE.addTool(TupleToolDecay, name="electron")
myNTUPLE.addTool(TupleToolDecay, name="muon")
myNTUPLE.addTool(TupleToolDecay, name="piminus")
myNTUPLE.addTool(TupleToolDecay, name="piplus")
myNTUPLE.addTool(TupleToolDecay, name="pion")
myNTUPLE.addTool(TupleToolDecay, name="K")
myNTUPLE.addTool(TupleToolDecay, name="Lambda")




def addRelatedInfo(tuple, particle, inputpath, varprefix, varpostfix):
    tuplehead = tuple.Sigma
    if tuple.name().startswith("K") :
        tuplehead = tuple.K
    elif tuple.name().startswith("Lambda") :
        tuplehead = tuple.Lambda
    
    #print "cazzus " ,  "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"10"+varpostfix
    tuplehead.ToolList+=["LoKi::Hybrid::TupleTool/"+particle+"_Iso"]
    LoKi_iso_vars = LoKi__Hybrid__TupleTool(particle+'_Iso')

    LoKi_iso_vars.Variables = {
        particle+"_cangle_1.00" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cangle_0.90" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cangle_1.10" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cpt_1.00" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cpt_0.90" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cpt_1.10" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cmult_1.00" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEMULT', -1.)",
        particle+"_cmult_0.90" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEMULT', -1.)",
        particle+"_cmult_1.10" : "RELINFO('"+RelInfoPrefix+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEMULT', -1.)",

    }
    tuplehead.addTool(LoKi_iso_vars)
    return LoKi_iso_vars


#============================================================
#  MC TRUTH
if doMCTruth : 
    from Configurables import  TupleToolMCTruth, MCTupleToolKinematic
    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList = [
        "MCTupleToolKinematic" ,
        "MCTupleToolHierarchy"
        ]
    MCTruth.addTool( MCTupleToolKinematic())
    MCTruth.MCTupleToolKinematic.StoreStablePropertime = True
    myNTUPLE.addTool(MCTruth)
    
    myNTUPLE.ToolList += [
        "TupleToolMCTruth"
        , "TupleToolMCBackgroundInfo"
#        , "TupleToolGeneration"
        ,"MCTupleToolPrimaries"
        ]


SigmaPMuMuTuple = myNTUPLE.clone("SigmaPMuMuTuple")
SigmaPMuMuTuple.Inputs = [ SigmaPMuMuInput ] 
SigmaPMuMuTuple.Decay = "[Sigma+ -> ^p+ ^mu+ ^mu-]CC"
SigmaPMuMuTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ mu+ mu-]CC",
    "muminus" : "[Sigma+ -> p+ mu+ ^mu-]CC",
    "muplus" : "[Sigma+ -> p+ ^mu+ mu-]CC",
    "proton" : "[Sigma+ -> ^p+ mu+ mu-]CC"
    }
addRelatedInfo(SigmaPMuMuTuple, "Sigma", SigmaPMuMuInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuTuple, "proton", SigmaPMuMuInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuTuple, "muplus", SigmaPMuMuInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuTuple, "muminus", SigmaPMuMuInput, "P2CVMu", "_2")


SigmaPMuMuDownTuple = myNTUPLE.clone("SigmaPMuMuDownTuple")
SigmaPMuMuDownTuple.Inputs = [ SigmaPMuMuDownInput ] 
SigmaPMuMuDownTuple.Decay = "[Sigma+ -> ^p+ ^mu+ ^mu-]CC"
SigmaPMuMuDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ mu+ mu-]CC",
    "muminus" : "[Sigma+ -> p+ mu+ ^mu-]CC",
    "muplus" : "[Sigma+ -> p+ ^mu+ mu-]CC",
    "proton" : "[Sigma+ -> ^p+ mu+ mu-]CC"
    }

addRelatedInfo(SigmaPMuMuDownTuple, "Sigma", SigmaPMuMuDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuDownTuple, "proton", SigmaPMuMuDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuDownTuple, "muplus", SigmaPMuMuDownInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuDownTuple, "muminus", SigmaPMuMuDownInput, "P2CVMu", "_2")


SigmaPMuMuDetTuple = myNTUPLE.clone("SigmaPMuMuDetTuple")
SigmaPMuMuDetTuple.Inputs = [    SigmaPMuMuDetInput ]
SigmaPMuMuDetTuple.Decay = "[Sigma+ -> ^p+ ^(KS0 -> ^mu+ ^mu-)]CC"
SigmaPMuMuDetTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ (KS0 -> mu+ mu-)]CC",
    "muminus" : "[Sigma+ -> p+ (KS0 -> mu+ ^mu-)]CC",
    "muplus" : "[Sigma+ -> p+ (KS0 -> ^mu+ mu-)]CC",
    "proton" : "[Sigma+ -> ^p+ (KS0 -> mu+ mu-)]CC",
    "dimuon" : "[Sigma+ -> p+ ^(KS0 -> mu+ mu-)]CC"    
    }

addRelatedInfo(SigmaPMuMuDetTuple, "Sigma", SigmaPMuMuDetInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuDetTuple, "proton", SigmaPMuMuDetInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuDetTuple, "muplus", SigmaPMuMuDetInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuDetTuple, "muminus", SigmaPMuMuDetInput, "P2CVMu", "_2")


SigmaPMuMuLFVTuple = myNTUPLE.clone("SigmaPMuMuLFVTuple")
SigmaPMuMuLFVTuple.Inputs = [ SigmaPMuMuLFVInput ] 
SigmaPMuMuLFVTuple.Decay = "[Sigma+ -> ^p~- ^mu+ ^mu+]CC"
SigmaPMuMuLFVTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p~- mu+ mu+]CC",
    "muminus" : "[Sigma+ -> p~- ^mu+ mu+]CC",
    "muplus" : "[Sigma+ -> p~- mu+ ^mu+]CC",
    "proton" : "[Sigma+ -> ^p~- mu+ mu+]CC"
    }
addRelatedInfo(SigmaPMuMuLFVTuple, "Sigma", SigmaPMuMuLFVInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuLFVTuple, "proton", SigmaPMuMuLFVInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuLFVTuple, "muplus", SigmaPMuMuLFVInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuLFVTuple, "muminus", SigmaPMuMuLFVInput, "P2CVMu", "_2")


SigmaPMuMuLFVDownTuple = myNTUPLE.clone("SigmaPMuMuLFVDownTuple")
SigmaPMuMuLFVDownTuple.Inputs = [ SigmaPMuMuLFVDownInput ] 
SigmaPMuMuLFVDownTuple.Decay = "[Sigma+ -> ^p~- ^mu+ ^mu+]CC"
SigmaPMuMuLFVDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p~- mu+ mu+]CC",
    "muminus" : "[Sigma+ -> p~- ^mu+ mu+]CC",
    "muplus" : "[Sigma+ -> p~- mu+ ^mu+]CC",
    "proton" : "[Sigma+ -> ^p~- mu+ mu+]CC"
    }
addRelatedInfo(SigmaPMuMuLFVDownTuple, "Sigma", SigmaPMuMuLFVDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "proton", SigmaPMuMuLFVDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "muplus", SigmaPMuMuLFVDownInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "muminus", SigmaPMuMuLFVDownInput, "P2CVMu", "_2")


SigmaPEMuTuple = myNTUPLE.clone("SigmaPEMuTuple")
SigmaPEMuTuple.ToolList.remove("TupleToolDalitz") #bad crash
SigmaPEMuTuple.Inputs = [    SigmaPEMuInput ]
SigmaPEMuTuple.Decay =  "( ([Sigma+ -> ^p+ ^e+ ^mu-]CC) || ([Sigma+ -> ^p+ ^mu+ ^e-]CC) )"
SigmaPEMuTuple.Branches = {
    "Sigma"  : "( (^[Sigma+ -> p+ e+ mu-]CC) || (^[Sigma+ -> p+ mu+ e-]CC) )",
    "electron" :"( ([Sigma+ -> p+ ^e+ mu-]CC) || ([Sigma+ -> p+ mu+ ^e-]CC) )",
    "muon" :  "( ([Sigma+ -> p+ e+ ^mu-]CC) || ([Sigma+ -> p+ ^mu+ e-]CC) )",
    "proton" : "( ([Sigma+ -> ^p+ e+ mu-]CC) || ([Sigma+ -> ^p+ mu+ e-]CC) )"
    }
addRelatedInfo(SigmaPEMuTuple, "Sigma", SigmaPEMuInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEMuTuple, "proton", SigmaPEMuInput, "P2CVProton", "")
addRelatedInfo(SigmaPEMuTuple, "muon", SigmaPEMuInput, "P2CVMu", "")
addRelatedInfo(SigmaPEMuTuple, "electron", SigmaPEMuInput, "P2CVE", "")



SigmaPEETuple = myNTUPLE.clone("SigmaPEETuple")
SigmaPEETuple.Inputs = [    SigmaPEEInput ]
SigmaPEETuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
SigmaPEETuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
    "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
    "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
    "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
    }
addRelatedInfo(SigmaPEETuple, "Sigma", SigmaPEEInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEETuple, "proton", SigmaPEEInput, "P2CVProton", "")
addRelatedInfo(SigmaPEETuple, "eplus", SigmaPEEInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEETuple, "eminus", SigmaPEEInput, "P2CVE", "_2")

SigmaPEEDownTuple = myNTUPLE.clone("SigmaPEEDownTuple")
SigmaPEEDownTuple.Inputs = [    SigmaPEEDownInput ]
SigmaPEEDownTuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
SigmaPEEDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
    "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
    "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
    "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
    }
addRelatedInfo(SigmaPEEDownTuple, "Sigma", SigmaPEEDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEEDownTuple, "proton", SigmaPEEDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPEEDownTuple, "eplus", SigmaPEEDownInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEEDownTuple, "eminus", SigmaPEEDownInput, "P2CVE", "_2")


SigmaPEEDetTuple = myNTUPLE.clone("SigmaPEEDetTuple")
SigmaPEEDetTuple.Inputs = [    SigmaPEEDetInput ]
SigmaPEEDetTuple.Decay = "[Sigma+ -> ^p+ ^(KS0 -> ^e+ ^e-)]CC"
SigmaPEEDetTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ (KS0 -> e+ e-)]CC",
    "eminus" : "[Sigma+ -> p+ (KS0 -> e+ ^e-)]CC",
    "eplus" : "[Sigma+ -> p+ (KS0 -> ^e+ e-)]CC",
    "proton" : "[Sigma+ -> ^p+ (KS0 -> e+ e-)]CC",
    "dielectron" : "[Sigma+ -> p+ ^(KS0 -> e+ e-)]CC"    
    }

addRelatedInfo(SigmaPEEDetTuple, "Sigma", SigmaPEEDetInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEEDetTuple, "proton", SigmaPEEDetInput, "P2CVProton", "")
addRelatedInfo(SigmaPEEDetTuple, "eplus", SigmaPEEDetInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEEDetTuple, "eminus", SigmaPEEDetInput, "P2CVE", "_2")


SigmaPPi0CalTuple = myNTUPLE.clone("SigmaPPi0CalTuple")
SigmaPPi0CalTuple.Inputs = [    SigmaPPi0CalInput ]
SigmaPPi0CalTuple.Decay = "[Sigma+ -> ^p+ ^pi0]CC"
SigmaPPi0CalTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ pi0]CC",
    "proton" : "[Sigma+ -> ^p+ pi0]CC",
    "pizero" : "[Sigma+ -> p+ ^pi0]CC"
    }
addRelatedInfo(SigmaPPi0CalTuple, "Sigma", SigmaPPi0CalInput, "P2CVSigma", "")
addRelatedInfo(SigmaPPi0CalTuple, "proton", SigmaPPi0CalInput, "P2CVProton", "")
addRelatedInfo(SigmaPPi0CalTuple, "pizero", SigmaPPi0CalInput, "P2CVPi0", "")



SigmaPPi0DalTuple = myNTUPLE.clone("SigmaPPi0DalTuple")
SigmaPPi0DalTuple.Inputs = [    SigmaPPi0DalInput ]
SigmaPPi0DalTuple.Decay = "[Sigma+ -> ^p+ ^pi0]CC"
SigmaPPi0DalTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ pi0]CC",
    "proton" : "[Sigma+ -> ^p+ pi0]CC",
    "pizero" : "[Sigma+ -> p+ ^pi0]CC"
    }
addRelatedInfo(SigmaPPi0DalTuple, "Sigma", SigmaPPi0DalInput, "P2CVSigma", "")
addRelatedInfo(SigmaPPi0DalTuple, "proton", SigmaPPi0DalInput, "P2CVProton", "")
addRelatedInfo(SigmaPPi0DalTuple, "pizero", SigmaPPi0DalInput, "P2CVPi0", "")

KPiMuMuTuple = myNTUPLE.clone("KPiMuMuTuple")
KPiMuMuTuple.Inputs = [ KPiMuMuInput ] 
KPiMuMuTuple.Decay = "[K+ -> ^pi+ ^mu+ ^mu-]CC"
KPiMuMuTuple.Branches = {
    "K"  : "^[K+ -> pi+ mu+ mu-]CC",
    "muminus" : "[K+ -> pi+ mu+ ^mu-]CC",
    "muplus" : "[K+ -> pi+ ^mu+ mu-]CC",
    "pion" : "[K+ -> ^pi+ mu+ mu-]CC"
    }
addRelatedInfo(KPiMuMuTuple, "K", KPiMuMuInput, "P2CVK", "")
addRelatedInfo(KPiMuMuTuple, "pion", KPiMuMuInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuTuple, "muplus", KPiMuMuInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuTuple, "muminus", KPiMuMuInput, "P2CVMu", "_2")

KPiMuMuDownTuple = myNTUPLE.clone("KPiMuMuDownTuple")
KPiMuMuDownTuple.Inputs = [ KPiMuMuDownInput ] 
KPiMuMuDownTuple.Decay = "[K+ -> ^pi+ ^mu+ ^mu-]CC"
KPiMuMuDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ mu+ mu-]CC",
    "muminus" : "[K+ -> pi+ mu+ ^mu-]CC",
    "muplus" : "[K+ -> pi+ ^mu+ mu-]CC",
    "pion" : "[K+ -> ^pi+ mu+ mu-]CC"
    }
addRelatedInfo(KPiMuMuDownTuple, "K", KPiMuMuDownInput, "P2CVK", "")
addRelatedInfo(KPiMuMuDownTuple, "pion", KPiMuMuDownInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuDownTuple, "muplus", KPiMuMuDownInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuDownTuple, "muminus", KPiMuMuDownInput, "P2CVMu", "_2")



KPiMuMuLFVTuple = myNTUPLE.clone("KPiMuMuLFVTuple")
KPiMuMuLFVTuple.Inputs = [ KPiMuMuLFVInput ] 
KPiMuMuLFVTuple.Decay = "[K+ -> ^pi- ^mu+ ^mu+]CC"
KPiMuMuLFVTuple.Branches = {
    "K"  : "^[K+ -> pi- mu+ mu+]CC",
    "muminus" : "[K+ -> pi- mu+ ^mu+]CC",
    "muplus" : "[K+ -> pi- ^mu+ mu+]CC",
    "pion" : "[K+ -> ^pi- mu+ mu+]CC"
    }
addRelatedInfo(KPiMuMuLFVTuple, "K", KPiMuMuLFVInput, "P2CVK", "")
addRelatedInfo(KPiMuMuLFVTuple, "pion", KPiMuMuLFVInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuLFVTuple, "muplus", KPiMuMuLFVInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuLFVTuple, "muminus", KPiMuMuLFVInput, "P2CVMu", "_2")


KPiMuMuLFVDownTuple = myNTUPLE.clone("KPiMuMuLFVDownTuple")
KPiMuMuLFVDownTuple.Inputs = [ KPiMuMuLFVDownInput ] 
KPiMuMuLFVDownTuple.Decay = "[K+ -> ^pi- ^mu+ ^mu+]CC"
KPiMuMuLFVDownTuple.Branches = {
    "K"  : "^[K+ -> pi- mu+ mu+]CC",
    "muminus" : "[K+ -> pi- mu+ ^mu+]CC",
    "muplus" : "[K+ -> pi- ^mu+ mu+]CC",
    "pion" : "[K+ -> ^pi- mu+ mu+]CC"
    }
addRelatedInfo(KPiMuMuLFVDownTuple, "K", KPiMuMuLFVDownInput, "P2CVK", "")
addRelatedInfo(KPiMuMuLFVDownTuple, "pion", KPiMuMuLFVDownInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuLFVDownTuple, "muplus", KPiMuMuLFVDownInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuLFVDownTuple, "muminus", KPiMuMuLFVDownInput, "P2CVMu", "_2")


KPiPiPiTuple = myNTUPLE.clone("KPiPiPiTuple")
KPiPiPiTuple.Inputs = [ KPiPiPiInput ] 
KPiPiPiTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiTuple, "K", KPiPiPiInput, "P2CVK", "")
addRelatedInfo(KPiPiPiTuple, "pion", KPiPiPiInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiTuple, "piplus", KPiPiPiInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiTuple, "piminus", KPiPiPiInput, "P2CVPi", "_2")


KPiPiPiDownTuple = myNTUPLE.clone("KPiPiPiDownTuple")
KPiPiPiDownTuple.Inputs = [ KPiPiPiDownInput ] 
KPiPiPiDownTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiDownTuple, "K", KPiPiPiDownInput, "P2CVK", "")
addRelatedInfo(KPiPiPiDownTuple, "pion", KPiPiPiDownInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiDownTuple, "piplus", KPiPiPiDownInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiDownTuple, "piminus", KPiPiPiDownInput, "P2CVPi", "_2")


KPiPiPiMassMeasTuple = myNTUPLE.clone("KPiPiPiMassMeasTuple")
KPiPiPiMassMeasTuple.Inputs = [ KPiPiPiMassMeasInput ] 
KPiPiPiMassMeasTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiMassMeasTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiMassMeasTuple, "K", KPiPiPiMassMeasInput, "P2CVK", "")
addRelatedInfo(KPiPiPiMassMeasTuple, "pion", KPiPiPiMassMeasInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiMassMeasTuple, "piplus", KPiPiPiMassMeasInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiMassMeasTuple, "piminus", KPiPiPiMassMeasInput, "P2CVPi", "_2")


KPiPiPiMassMeasDownTuple = myNTUPLE.clone("KPiPiPiMassMeasDownTuple")
KPiPiPiMassMeasDownTuple.Inputs = [ KPiPiPiMassMeasDownInput ] 
KPiPiPiMassMeasDownTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiMassMeasDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiMassMeasDownTuple, "K", KPiPiPiMassMeasDownInput, "P2CVK", "")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "pion", KPiPiPiMassMeasDownInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "piplus", KPiPiPiMassMeasDownInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "piminus", KPiPiPiMassMeasDownInput, "P2CVPi", "_2")


LambdaPPiTuple = myNTUPLE.clone("LambdaPPiTuple")
LambdaPPiTuple.Inputs = [ LambdaPPiInput ] 
LambdaPPiTuple.Decay = "[Lambda0 -> ^p+ ^pi-]CC" 
LambdaPPiTuple.Branches = {
    "Lambda"  : "^[Lambda0 -> p+ pi-]CC" ,
    "proton" : "[Lambda0 -> ^p+ pi-]CC" ,
    "pion" : "[Lambda0 -> p+ ^pi-]CC" ,
    }
addRelatedInfo(LambdaPPiTuple, "Lambda", LambdaPPiInput, "P2CVSigma", "") # Not a mistake
addRelatedInfo(LambdaPPiTuple, "pion", LambdaPPiInput, "P2CVPi", "")
addRelatedInfo(LambdaPPiTuple, "proton", LambdaPPiInput, "P2CVProton", "")



LambdaPPiEETuple = myNTUPLE.clone("LambdaPPiEETuple")
LambdaPPiEETuple.Inputs = [ LambdaPPiEEInput ] 
LambdaPPiEETuple.Decay = "[Lambda0 -> ^p+ ^pi- ^(KS0 -> ^e+ ^e-)]CC" 
LambdaPPiEETuple.Branches = {
    "Lambda"  : "^[Lambda0 -> p+ pi- (KS0 -> e+ e-)]CC" ,
    "proton" : "[Lambda0 -> ^p+ pi- (KS0 -> e+ e-)]CC" ,
    "pion" : "[Lambda0 -> p+ ^pi-  (KS0 -> e+ e-)]CC" ,
    "dielectron" : "[Lambda0 -> p+ pi-  ^(KS0 -> e+ e-)]CC" ,
    "eplus" : "[Lambda0 -> p+ ^pi-  (KS0 -> ^e+ e-)]CC" ,
    "eminus" : "[Lambda0 -> p+ ^pi-  (KS0 -> e+ ^e-)]CC" ,
    }
addRelatedInfo(LambdaPPiEETuple, "Lambda", LambdaPPiEEInput, "P2CVSigma", "") # Not a mistake
addRelatedInfo(LambdaPPiEETuple, "pion", LambdaPPiEEInput, "P2CVPi", "")
addRelatedInfo(LambdaPPiEETuple, "proton", LambdaPPiEEInput, "P2CVProton", "")
# isolation on the other is not in stripping... forget about it


PhiKMuTuple = myNTUPLE.clone("PhiKMuTuple")
PhiKMuTuple.Inputs = [ PhiKMuInput ] 
PhiKMuTuple.Decay = "[phi(1020) -> ^K+ ^mu-]CC"
PhiKMuTuple.Branches = {
    "phi"  : "[phi(1020) -> K+ mu-]CC",
    "kaon" :  "[phi(1020) -> ^K+ mu-]CC",
    "muon" :  "[phi(1020) -> K+ ^mu-]CC"
    }


if doMatchedNtuples:
    matched_myNTUPLE = myNTUPLE.clone("matched")
#    matched_myNTUPLE.ToolList.remove('TupleToolPropertime')
    
    MSigmaPMuMuTuple = matched_myNTUPLE.clone("MSigmaPMuMuTuple")
    MSigmaPMuMuTuple.Inputs = [ SelMSigmaPMuMu.outputLocation() ] 
    MSigmaPMuMuTuple.Decay =  "[Sigma+ -> ^p+ ^mu+ ^mu-]CC"
    MSigmaPMuMuTuple.Branches = {
        "Sigma"  : "^[Sigma+ -> p+ mu+ mu-]CC",
        "muminus" : "[Sigma+ -> p+ mu+ ^mu-]CC",
        "muplus" : "[Sigma+ -> p+ ^mu+ mu-]CC",
        "proton" : "[Sigma+ -> ^p+ mu+ mu-]CC"
        }

    MSigmaPPi0Tuple = matched_myNTUPLE.clone("MSigmaPPi0Tuple")
    MSigmaPPi0Tuple.Inputs = [ SelMSigmaPPi0.outputLocation() ]
    MSigmaPPi0Tuple.Decay = "[Sigma+ -> ^p+ ^pi0]CC"
    MSigmaPPi0Tuple.Branches = {
        "Sigma"  : "^[Sigma+ -> p+ pi0]CC",
        "proton" : "[Sigma+ -> ^p+ pi0]CC",
        "pizero" : "[Sigma+ -> p+ ^pi0]CC"
        }

    MSigmaPMuMuLFVTuple = matched_myNTUPLE.clone("MSigmaPMuMuLFVTuple")
    MSigmaPMuMuLFVTuple.Inputs = [ SelMSigmaPMuMuLFV.outputLocation() ]
    MSigmaPMuMuLFVTuple.Decay = "[Sigma+ -> ^p~- ^mu+ ^mu+]CC"
    MSigmaPMuMuLFVTuple.Branches = {
        "Sigma"  : "^[Sigma+ -> p~- mu+ mu+]CC",
        "muminus" : "[Sigma+ -> p~- ^mu+ mu+]CC",
        "muplus" : "[Sigma+ -> p~- mu+ ^mu+]CC",
        "proton" : "[Sigma+ -> ^p~- mu+ mu+]CC"
        }
    MSigmaPEETuple = matched_myNTUPLE.clone("MSigmaPEETuple")
    MSigmaPEETuple.Inputs = [ SelMSigmaPEE.outputLocation() ] 
    MSigmaPEETuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
    MSigmaPEETuple.Branches = {
        "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
        "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
        "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
        "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
        }
    MSigmaPEEDownTuple = matched_myNTUPLE.clone("MSigmaPEEDownTuple")
    MSigmaPEEDownTuple.Inputs = [ SelMSigmaPEEDown.outputLocation() ] 
    MSigmaPEEDownTuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
    MSigmaPEEDownTuple.Branches = {
        "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
        "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
        "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
        "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
        }

    MKPiMuMuTuple = matched_myNTUPLE.clone("MKPiMuMuTuple")
    MKPiMuMuTuple.Inputs = [ SelMKPiMuMu.outputLocation() ] 
    MKPiMuMuTuple.Decay =  "[K+ -> ^pi+ ^mu+ ^mu-]CC"
    MKPiMuMuTuple.Branches = {
        "K"  : "^[K+ -> pi+ mu+ mu-]CC",
        "muminus" : "[K+ -> pi+ mu+ ^mu-]CC",
        "muplus" : "[K+ -> pi+ ^mu+ mu-]CC",
        "pion" : "[K+ -> ^pi+ mu+ mu-]CC"
        }

    MKPiPiPiTuple = matched_myNTUPLE.clone("MKPiPiPiTuple")
    MKPiPiPiTuple.Inputs = [ SelMKPiPiPi.outputLocation() ] 
    MKPiPiPiTuple.Decay =  "[K+ -> ^pi+ ^pi+ ^pi-]CC"
    MKPiPiPiTuple.Branches = {
        "K"  : "^[K+ -> pi+ pi+ pi-]CC",
        "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
        "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
        "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
        }


    mymctuple = MCDecayTreeTuple('mymctuple')
    mymctuple.ToolList += [ "MCTupleToolKinematic"
                            ,"MCTupleToolHierarchy"
                            ,"MCTupleToolPID"
#                            , "MCTupleToolV0Ancestors"
                           , "MCTupleToolReconstructed"  ]
    mymctuple.addTool(MCTupleToolKinematic())
    mymctuple.MCTupleToolKinematic.Verbose=True
    mymctuple.MCTupleToolKinematic.StoreStablePropertime = True
    mymctuple.addTool(MCTupleToolReconstructed())
    mymctuple.MCTupleToolReconstructed.Verbose=True

    mctuple = mymctuple.clone( 'MCSigmaPMuMuTuple' )
    mctuple.Decay =    "[Sigma+ ==> ^p+ ^mu+ ^mu-]CC"
    mctuple.Branches = {
        "Sigma"  : "^[Sigma+ ==> p+ mu+ mu- ]CC",
        "muminus" : "[Sigma+ ==> p+ mu+ ^mu-  ]CC",
        "muplus" : "[Sigma+ ==> p+ ^mu+ mu-  ]CC",
        "proton" : "[Sigma+ ==> ^p+ mu+ mu-  ]CC"
        }

    mctuplelfv = mymctuple.clone( 'MCSigmaPMuMuLFVTuple' )
    mctuplelfv.Decay = "[Sigma+ ==> ^p~- ^mu+ ^mu+ ]CC"
    mctuplelfv.Branches = {
        "Sigma"  : "^[Sigma+ ==> p~- mu+ mu+ ]CC",
        "muminus" : "[Sigma+ ==> p~- ^mu+ mu+ ]CC",
        "muplus" : "[Sigma+ ==> p~- mu+ ^mu+ ]CC",
        "proton" : "[Sigma+ ==> ^p~- mu+ mu+ ]CC"
        }


    mctupleppi0 = mymctuple.clone( 'MCSigmaPPi0Tuple' )
    mctupleppi0.Decay = "[Sigma+ ==> ^p+ ^pi0  ]CC"
    mctupleppi0.Branches = {
        "Sigma"  : "^[Sigma+ ==> p+ pi0  ]CC",
        "proton" : "[Sigma+ ==> ^p+ pi0  ]CC",
        "pi0"    : "[Sigma+ ==> p+ ^pi0  ]CC" 
        }

    mctuplepgamma = mymctuple.clone( 'MCSigmaPGammaTuple' )
    mctuplepgamma.Decay = "[Sigma+ ==> ^p+ ^gamma  ]CC"
    mctuplepgamma.Branches = {
        "Sigma"  : "^[Sigma+ ==> p+ gamma  ]CC",
        "proton" : "[Sigma+ ==> ^p+ gamma  ]CC",
        "gamma"    : "[Sigma+ ==> p+ ^gamma  ]CC" 
        }



    mctuplep = mymctuple.clone( 'MCSigmaPTuple' )
    mctuplep.Decay = "[Sigma+ ->  p+ ...]CC"
    # ntuples for cross-section "measurement" 
    mctupleincl = mymctuple.clone( 'MCSigmaInclTuple' )
    mctupleincl.Decay = "[Sigma+]cc"
    mctuplekincl = mymctuple.clone( 'MCKInclTuple' )
    mctuplekincl.Decay = "[K+]cc"
    mctuplelamincl = mymctuple.clone( 'MCLamInclTuple' )
    mctuplelamincl.Decay = "[Lambda0]cc"
    mctuplekshincl = mymctuple.clone( 'MCKS0InclTuple' )
    mctuplekshincl.Decay = "[KS0]cc"

    

    mctuplek = mymctuple.clone( 'MCKPiMuMuTuple' )
    mctuplek.Decay =    "[K+ ==> ^pi+ ^mu+ ^mu-  ]CC"
    mctuplek.Branches = {
        "K"  : "^[K+ ==> pi+ mu+ mu- ]CC",
        "muminus" : "[K+ ==> pi+ mu+ ^mu-  ]CC",
        "muplus" : "[K+ ==> pi+ ^mu+ mu-  ]CC",
        "pion" : "[K+ ==> ^pi+ mu+ mu-  ]CC"
        }

    mctuplekpipipi = mymctuple.clone( 'MCKPiPiPiTuple' )
    mctuplekpipipi.Decay =    "[K+ ==> ^pi+ ^pi+ ^pi-  ]CC"
    mctuplekpipipi.Branches = {
        "K"  : "^[K+ ==> pi+ pi+ pi- ]CC",
        "piminus" : "[K+ ==> pi+ pi+ ^pi-  ]CC",
        "piplus" : "[K+ ==> pi+ ^pi+ pi-  ]CC",
        "pion" : "[K+ ==> ^pi+ pi+ pi-  ]CC"
        }


from Configurables import PrintMCTree
pMC = PrintMCTree()
pMC.ParticleNames = [ "Sigma+" ]
if not data and printTree:
    DaVinci().UserAlgorithms += [pMC]


DaVinci().TupleFile = 'Sigmapmumu.root'

#============================================================
if veloMuonTracking : 
    DaVinci().appendToMainSequence([alg])

if doStripping :
    from StrippingConf.Configuration import StrippingConf
    #use CommonParticlesArchive
    from CommonParticlesArchive import CommonParticlesArchiveConf
    CommonParticlesArchiveConf().redirect("stripping21r0p1")
    
    # Tighten Trk Chi2 to <3
    from CommonParticles.Utils import DefaultTrackingCuts
    DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                    "CloneDistCut" : [5000, 9e+99 ] }
    
    # Specify the name of your configuration
    #confname="DstarPromptWithD02HHMuMuControl" #FOR USERS
    confname = "RareStrange"


    # NOTE: this will work only if you inserted correctly the 
    # default_config dictionary in the code where your LineBuilder 
    # is defined.
    from StrippingSelections import buildersConf
    confs = buildersConf()
    
    from StrippingSelections.Utils import lineBuilder, buildStreamsFromBuilder
    confs[confname]["CONFIG"]["SigmaPPi0CalPrescale"] = 1.

    for i in     confs[confname]["CONFIG"] :
        if "Prescale" in i:
            confs[confname]["CONFIG"][i] = 1.
    
    streams = buildStreamsFromBuilder(confs,confname)
    
    #clone lines for CommonParticles overhead-free timing
    print "Creating line clones for timing"
    for s in streams:
        for l in s.lines:
            if "_TIMING" not in l.name():
                cloned = l.clone(l.name().strip("Stripping")+"_TIMING")
                s.appendLines([cloned])
                
    #define stream names
    leptonicMicroDSTname   = 'Leptonic'
    charmMicroDSTname      = 'Charm'
    pidMicroDSTname        = 'PID'
    bhadronMicroDSTname    = 'Bhadron'
    mdstStreams = [ leptonicMicroDSTname,charmMicroDSTname,pidMicroDSTname,bhadronMicroDSTname ]
    dstStreams  = [ "BhadronCompleteEvent", "CharmCompleteEvent", "CharmToBeSwum", "Dimuon",
                    "EW", "Semileptonic", "Calibration", "MiniBias", "Radiative" ]
    
    stripTESPrefix = 'Strip'
    from Configurables import  ProcStatusCheck
    filterBadEvents =  ProcStatusCheck()

    sc = StrippingConf( Streams = streams,
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    TESPrefix = stripTESPrefix,
                    ActiveMDSTStream = True,
                    Verbose = True,
                    DSTStreams = dstStreams,
                    MicroDSTStreams = mdstStreams )


    DaVinci().appendToMainSequence( [sc.sequence()])


    from Configurables import StrippingReport
    sr = StrippingReport(Selections = sc.selections())
    DaVinci().appendToMainSequence( [sr] )

from Configurables import AuditorSvc, ChronoAuditor                          
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

if (isMDST):

# Kill DAQ, temporary fix
    from Configurables import EventNodeKiller
    eventNodeKiller = EventNodeKiller('DAQkiller')
    eventNodeKiller.Nodes = ['/Event/DAQ','/Event/pRec']



    
#MessageSvc().OutputLevel = DEBUG
MessageSvc().OutputLevel = INFO
if doTuples :
    if onlySigma:
        DaVinci().MoniSequence += [ SigmaPMuMuTuple, SigmaPMuMuDownTuple,# SigmaPMuMuLFVTuple, SigmaPMuMuLFVDownTuple,
                                    SigmaPMuMuDetTuple, SigmaPPi0CalTuple ] 
    else : 
        
        DaVinci().MoniSequence += [ SigmaPMuMuTuple, SigmaPMuMuDownTuple, SigmaPMuMuLFVTuple,
                                    # SigmaPMuMuLFVDownTuple,
                                    SigmaPMuMuDetTuple,
                                    #SigmaPEETuple,
                                    #SigmaPEEDetTuple,
                                    #SigmaPEMuTuple, 
                                    #SigmaPEEDownTuple,  
                                    SigmaPPi0CalTuple, SigmaPPi0DalTuple,
                                    #KPiMuMuTuple,KPiMuMuDownTuple, KPiMuMuLFVTuple,KPiMuMuLFVDownTuple,
                                    KPiPiPiTuple, KPiPiPiDownTuple,
                                    KPiPiPiMassMeasTuple, KPiPiPiMassMeasDownTuple,
                                    LambdaPPiTuple, LambdaPPiEETuple
                                    #PhiKMuTuple
                                    ]
        
    if doMatchedNtuples:
        if onlySigma :
            DaVinci().MoniSequence += [ MSigmaPMuMuTuple]
        else : 
            DaVinci().MoniSequence += [ MSigmaPMuMuTuple , MSigmaPPi0Tuple, MSigmaPMuMuTuple , MKPiMuMuTuple, MKPiPiPiTuple,
                                        MSigmaPMuMuLFVTuple ,
                                        #MSigmaPEETuple ,MSigmaPEEDownTuple ,
                                        mctuple, mctuplelfv, mctuplep,
                                        mctuplek, mctuplekpipipi,mctupleppi0, mctuplepgamma]
            

            if inclusiveMCTruth : 
                DaVinci().MoniSequence += [ mctupleincl,mctuplekincl,
                                            mctuplelamincl,
                                            mctuplekshincl  ]

            


if debug:

    DaVinci().DDDBtag = 'dddb-20150724'
    DaVinci().CondDBtag = 'sim-20161124-2-vc-mu100' #MC
    #DaVinci().CondDBtag = 'cond-20161004' #DATA 
    DaVinci().TupleFile     = "prova.root"
    DaVinci().HistogramFile = 'DVHistos.root'
    DaVinci().InputType     = 'MDST'
    DaVinci().Simulation    = True
    DaVinci().Lumi          = True
    DaVinci().PrintFreq     = 500
    DaVinci().EvtMax        = evtot
    DaVinci().DataType      = '2016'
    DaVinci().SkipEvents    = 0
    
    from GaudiConf import IOExtension
    IOExtension().inputFiles(['root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000004_1.mcfilter.ldst',
                             'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000001_1.mcfilter.ldst',
                             'root://svr018.gla.scotgrid.ac.uk:1094//dpm/gla.scotgrid.ac.uk/home/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000003_1.mcfilter.ldst',
                             'root://gfe02.grid.hep.ph.ic.ac.uk:1094/pnfs/hep.ph.ic.ac.uk/data/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000008_1.mcfilter.ldst',
                             'root://proxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000002_1.mcfilter.ldst',
                             'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000005_1.mcfilter.ldst',
                             'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000007_1.mcfilter.ldst',
                             'root://svr018.gla.scotgrid.ac.uk:1094//dpm/gla.scotgrid.ac.uk/home/lhcb/MC/2016/MCFILTER.LDST/00062279/0000/00062279_00000006_1.mcfilter.ldst',
                             ], clear=True)

    EventSelector().FirstEvent = FirstEv
    if isMDST :
        DaVinci().UserAlgorithms += [eventNodeKiller]  

    
        IODataManager().AgeLimit = 0

