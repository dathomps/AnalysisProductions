#!/usr/bin/env python
import argparse
import json
import yaml
from os.path import dirname, join


all_years = [2015, 2016, 2017, 2018]
all_polarities = ['MagDown', 'MagUp']
davinci_versions = {
    2015: 'v45r6',
    2016: 'v45r6',
    2017: 'v45r6',
    2018: 'v45r6',
}
bk_paths = {
    'Sigma2PMuMuData': {
        2016: '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco16/Stripping28r2/90000000/LEPTONIC.MDST',
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco17/Stripping29r2/90000000/LEPTONIC.MDST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco18/Stripping34/90000000/LEPTONIC.MDST',
    },
    'Sigma2PMuMu_MC': {
        2016: '/MC/2016/Beam6500GeV-2016-{polarity}-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/32113001/ALLSTREAMS.DST',
        2017: '',
        2018: '',
    },
    'K2pipipi_MC': {
        2016: '/MC/2016/Beam6500GeV-2016-{polarity}-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/37103000/ALLSTREAMS.DST',
        2017: '',
        2018: '' ,
    },
    'Sigma2PPi0_MC': {
        2016: '/MC/2016/Beam6500GeV-2016-{polarity}-Nu1.6-25ns-Pythia8/Sim09b/Trig0x6138160F/Reco16/Turbo03/Stripping26NoPrescalingFlagged/32101400/ALLSTREAMS.DST',
        2017: '',
        2018: '',
    }
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
    parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
    parser.add_argument('--tag-types', nargs='+', choices=list(bk_paths.keys()), default=list(bk_paths.keys()))
    parser.add_argument('--data-types', type=str, choices=['RealData', 'MC'], default='RealData')
    args = parser.parse_args()

    results = {}

    if args.data_types == 'RealData':
        ChainOption = [
                        'DataTypes/real_data.py',
                        ]
    elif args.data_types == 'MC':
        ChainOption = [
                        'DataTypes/mc.py',
                        ]
    else:
        raise NotImplementedError(args.data_types)

    for tag_type in args.tag_types:
        for year in args.years:
            if args.data_types == 'RealData' and tag_type != 'Sigma2PMuMuData':
                    continue
            if args.data_types == 'MC' and tag_type == 'Sigma2PMuMuData':
                    continue
            for polarity in args.polarities:
                    key = '_'.join([str(year), polarity, tag_type])
                    assert key not in results, 'Duplicate keys are not possible'
                    results[key] = {
                        'options': ChainOption + ['DataTypes/'+str(year)+'.py']
                                               + (['DataTypes/{}.py'.format(polarity)] if args.data_types == 'MC' else [])
                                               + [ 'ntuple_maker.py'],
                        'input': [],
                        'bk_query': bk_paths[tag_type][year].format(polarity=polarity),
#                        'dq_flag': 'OK',
                        'application': 'DaVinci/'+davinci_versions[year],
                        'output': '_'.join(['RD', tag_type,str(year)]).upper()+'.ROOT'
                    }


    with open(join(dirname(__file__), 'info_test.yaml'), 'wt') as fp:
        yaml.dump(results, fp)


if __name__ == '__main__':
    parse_args()
