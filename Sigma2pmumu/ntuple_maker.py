# ============================================================
# @author Francesco Dettori
######## options ########
from Configurables import GaudiSequencer, DaVinci

debug = False
IsMC = DaVinci().Simulation
data = not IsMC     # data or MC

#datatype = "2012"          # this counts for data
evtot=   -1
FirstEv = 1

inclusiveMCTruth = False  #true for inclusive ntupes
isPGun = False
doStripping   = False  #True if MC are not stripping flagged
veloMuonTracking = True
onlySigma = False
doCombine = False
writeDST =      False
rightDatabase = True
printTree = False
MCFiltered = False
microDST = True
isMDST = False
doITClusters  = False
doTuples      = True
doMatchedNtuples = False
doMCTruth     = False
#addModifiedTools = True

selSameSign = False
if data : #lets be safe
    doMCTruth = False
    doMatchedNtuples = False
    inclusiveMCTruth = False
if not doTuples :
    doMCTruth = False
    selSameSign = False
    #    addModifiedTools = False
    inclusiveMCTruth = False
#########################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
#######################################################################
from Configurables import GaudiSequencer, SelDSTWriter, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import DataOnDemand,Selection,SelectionSequence,AutomaticData
#######################################################################
from Configurables import  DecayTreeTuple , TupleToolDecay, TupleToolTrigger, TupleToolGeometry, MCDecayTreeTuple, TupleToolTISTOS, TupleToolNeutrinoReco, TupleToolMuonPid, TupleToolDalitz, TupleToolAngles, TupleToolVtxDaughters
from Configurables import MCDecayTreeTuple, MCTupleToolKinematic, MCTupleToolHierarchy, LoKi__Hybrid__MCTupleTool, MCTupleToolReconstructed
from Configurables import TupleToolTrackIsolation , TupleToolTrackInfo
##################################################################################333#333
from Configurables import LoKi__Hybrid__TupleTool
#from StandardParticles import StdAllNoPIDsPions, StdNoPIDsDownPions, StdAllNoPIDsProtons, StdNoPIDsDownProtons, StdLoosePions, StdLooseProtons

from Configurables import EventCountHisto,  LoKi__Hybrid__EvtTupleTool,  LoKi__Hybrid__TupleTool, LoKi__VoidFilter

onepv = LoKi__VoidFilter('AtLeastOnePV', Code = "CONTAINS('Rec/Vertex/Primary') > 0")

DaVinci().MoniSequence += [EventCountHisto("DaVinciMonitor") ]



from Configurables import HltDecReportsDecoder, HltSelReportsDecoder, HltVertexReportsDecoder, L0DUFromRawTool
DataOnDemandSvc().OutputLevel = DEBUG


## ----  IT clusters decoding  --- ##
if doITClusters :
    from Configurables import RawBankToSTClusterAlg
    createITClusters = RawBankToSTClusterAlg("CreateITClusters")
    createITClusters.DetType     = "IT"
    mySequencer = GaudiSequencer('mySequencer')
    mySequencer.Members += [ createITClusters ]
    DaVinci().UserAlgorithms  +=[ mySequencer ]
## ---- Define  Input locations  -----
prefix = ""
commonname = "RareStrange"

mdst_RootInTES = "/Event/Leptonic/"
RelInfoPrefix  = "/Event/"

SigmaPMuMuInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLine/Particles"
SigmaPMuMuDownInput  = prefix+"Phys/"+commonname+"SigmaPMuMuDownLine/Particles"
SigmaPMuMuDetInput  = prefix+"Phys/"+commonname+"SigmaPMuMuDetLine/Particles"
SigmaPMuMuLFVInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLFVLine/Particles"
SigmaPMuMuLFVDownInput  = prefix+"Phys/"+commonname+"SigmaPMuMuLFVDownLine/Particles"

SigmaPEEInput     = prefix+"Phys/"+commonname+"SigmaPEELine/Particles"
SigmaPEEDownInput = prefix+"Phys/"+commonname+"SigmaPEEDownLine/Particles"
SigmaPEEDetInput  = prefix+"Phys/"+commonname+"SigmaPEEDetLine/Particles"

SigmaPEMuInput     = prefix+"Phys/"+commonname+"SigmaPEMuLine/Particles"

SigmaPPi0CalInput = prefix+"Phys/"+commonname+"SigmaPPi0CalLine/Particles"
SigmaPPi0DalInput = prefix+"Phys/"+commonname+"SigmaPPi0Line/Particles"

KPiMuMuInput = prefix+ "Phys/"+commonname+"KPiMuMuLine/Particles"
KPiMuMuDownInput = prefix+ "Phys/"+commonname+"KPiMuMuDownLine/Particles"
KPiMuMuLFVInput = prefix+ "Phys/"+commonname+"KPiMuMuLFVLine/Particles"
KPiMuMuLFVDownInput = prefix+ "Phys/"+commonname+"KPiMuMuLFVDownLine/Particles"

KPiPiPiInput = prefix+ "Phys/"+commonname+"KPiPiPiLine/Particles"
KPiPiPiDownInput = prefix+ "Phys/"+commonname+"KPiPiPiDownLine/Particles"
KPiPiPiMassMeasInput = prefix+ "Phys/"+commonname+"KPiPiPiMassMeasLine/Particles"
KPiPiPiMassMeasDownInput = prefix+ "Phys/"+commonname+"KPiPiPiMassMeasDownLine/Particles"

LambdaPPiInput = prefix+ "Phys/"+commonname+"LambdaPPiLine/Particles"
LambdaPPiEEInput = prefix+ "Phys/"+commonname+"LambdaPPiEELine/Particles"

PhiKMuInput = prefix+ "Phys/"+commonname+"PhiKMuLine/Particles"


########################
#   Prepare VELO stuff #
########################
from Configurables import  FastVeloTracking, TrackPrepareVelo, DecodeVeloRawBuffer, TrackEventFitter, TrackMasterFitter,StoreExplorerAlg

storeExp = StoreExplorerAlg()
storeExp.Load = 1
storeExp.PrintFreq = 1.0
storeExp.OutputLevel = 3

name=""
preve = TrackPrepareVelo("preve")
preve.inputLocation = "Rec/Track/Velo"
preve.outputLocation = "Rec/Track/UnFittedVelo"
preve.bestLocation = ""

#TODO: apparently FastVelo is now (april 2012) run with fixes in the production which don't neccessarily apply to the stripping...
if veloMuonTracking :
    alg = GaudiSequencer("VeloMuonTrackingFor"+name,
                         Members = [ DecodeVeloRawBuffer(name+"VeloDecoding",DecodeToVeloLiteClusters=True,DecodeToVeloClusters=True),
                                     FastVeloTracking(name+"FastVelo",OutputTracksName="Rec/Track/Velo"),
                                     preve])


############################
### Configure the ntuple ###
############################

myNTUPLE = DecayTreeTuple('myNTUPLE')
#if isMDST:
myNTUPLE.RootInTES = mdst_RootInTES

myNTUPLE.OutputLevel = WARNING # silence please
myNTUPLE.IgnoreP2PVFromInputLocations = True # ignore all stored Particle -> PV relations
myNTUPLE.ReFitPVs = False # re-fit the PVs
myNTUPLE.ToolList +=  [  "TupleToolGeometry"
                         , "TupleToolKinematic"
                          , "TupleToolEventInfo"
                          , "TupleToolPid"
                          , "TupleToolPi0Info"
#                          , "TupleToolPropertime"
                          , "TupleToolRecoStats"
                          , "TupleToolTrigger"
                          , "TupleToolTISTOS"
                         , "TupleToolTrackInfo"
                         , "TupleToolDalitz"
                         , "TupleToolAngles"
                         , "TupleToolVtxDaughters"
#                         , "TupleToolTrackIsolation"
#                         ,  "TupleToolMuonPid"
#                         ,"LoKi::Hybrid::EvtTupleTool/LoKiEvent"
#                         ,"LoKi::Hybrid::TupleTool/LoKi_All"

#                         ,"LoKi::Hybrid::TupleTool/Isolations"
#                         ,"TupleToolVeloTrackMatch"
                          ]

if isPGun:
    myNTUPLE.remove("TupleToolTrigger")
    myNTUPLE.remove("TupleToolTISTOS")




## ----------  Store Triggers  ---------##

L0Triggers = ["L0MuonDecision" , "L0DiMuonDecision", "L0HadronDecision","L0HadronNoSPDDecision", "L0ElectronDecision", "L0ElectronHiDecision", "L0PhotonDecision", "L0PhotonHiDecision",
              "L0DiEM,lowMultDecision",  "L0DiHadron,lowMultDecision", "L0DiMuon,lowMultDecision" , "L0DiMuonNoSPDDecision", "L0Electron,lowMultDecision",
              "L0Muon,minbiasDecision", "L0Muon,lowMultDecision", "L0MuonNoSPDDecision"]


Hlt1Triggers = [  "Hlt1SingleMuonNoIPDecision"  ,"Hlt1SingleMuonHighPTDecision"
                  ,"Hlt1SingleElectronNoIPDecision" ,"Hlt1SingleElectronHighPTDecision"
                   ,'Hlt1TrackAllL0Decision'  ,'Hlt1TrackAllL0TightDecision'   ,'Hlt1TrackMuonDecision'  ,'Hlt1TrackPhotonDecision'
                  ,"Hlt1DiMuonLowMassDecision" ,"Hlt1DiMuonHighMassDecision"
                  , "Hlt1MBNoBiasDecision", "Hlt1NoPVPassThroughDecision", "Hlt1VertexDisplVertexDecision"
                  ,'Hlt1MB.*Decision']

Hlt2Triggers = [
    ## muon lines
    "Hlt2SingleMuonDecision", "Hlt2SingleMuonLowPTDecision", "Hlt2SingleMuonHighPTDecision",
    "Hlt2DiMuonDecision",  "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",  "Hlt2DiMuonJPsiHighPTDecision",  "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonDetachedDecision",  "Hlt2DiMuonDetachedJPsiDecision", "Hlt2DiMuonDetachedHeavyDecision", "Hlt2TriMuonTauDecision",
    ## hadron/Topo lines
    "Hlt2B2HHDecision",
    "Hlt2DiMuonBDecision",  'Hlt2DiMuonZDecision',
    "Hlt2TopoMu2BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",   "Hlt2Topo3BodyBBDTDecision",   "Hlt2Topo4BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision", "Hlt2Topo3BodySimpleDecision", "Hlt2Topo4BodySimpleDecision",
    ##others
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
    "Hlt2IncPhiDecision",
    ## inclusive decisions
    "Hlt2DiMuonDY.*Decision","Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", 'Hlt2DiElectron.*Decision', 'Hlt2.*GammaDecision'
    ]

Hlt1TriggersRunII = [  "Hlt1SingleMuonNoIPDecision"  ,"Hlt1SingleMuonHighPTDecision"
                  ,"Hlt1SingleElectronNoIPDecision" ,"Hlt1SingleElectronHighPTDecision"
                   ,'Hlt1TrackMVADecision'  , 'Hlt1TwoTrackMVADecision'
                   ,'Hlt1TrackMuonDecision'  , "Hlt1DiMuonLowMassDecision" ,"Hlt1DiMuonHighMassDecision", "Hlt1DiMuonNoL0Decision","Hlt1DiMuonNoIPDecision"
                  , "Hlt1MBNoBiasDecision", "Hlt1NoPVPassThroughDecision", "Hlt1VertexDisplVertexDecision" , "Hlt1MultiMuonNoL0Decision","Hlt1MultiMuonDiMuonNoIPDecision"
                  ,'Hlt1LowMultDecision'
                  ,'Hlt1MB.*Decision']


Hlt2TriggersRunII = [
    ## muon lines
    "Hlt2SingleMuonDecision", "Hlt2SingleMuonRareDecision", "Hlt2SingleMuonLowPTDecision", "Hlt2SingleMuonHighPTDecision",
    "Hlt2DiMuonDecision",  "Hlt2DiMuonLowMassDecision",
    "Hlt2DiMuonJPsiDecision",  "Hlt2DiMuonJPsiHighPTDecision",  "Hlt2DiMuonPsi2SDecision",
    "Hlt2DiMuonDetachedDecision",  "Hlt2DiMuonDetachedJPsiDecision", "Hlt2DiMuonDetachedHeavyDecision", "Hlt2TriMuonTauDecision",
    "Hlt2DiMuonSoftDecision", "Hlt2LowMultDiMuonDecision",
    ## hadron/Topo lines
    "Hlt2B2HHDecision",
    "Hlt2DiMuonBDecision",  'Hlt2DiMuonZDecision',
    "Hlt2TopoMu2BodyBBDTDecision", "Hlt2TopoMu3BodyBBDTDecision", "Hlt2TopoMu4BodyBBDTDecision",
    "Hlt2Topo2BodyBBDTDecision",   "Hlt2Topo3BodyBBDTDecision",   "Hlt2Topo4BodyBBDTDecision",
    "Hlt2Topo2BodySimpleDecision", "Hlt2Topo3BodySimpleDecision", "Hlt2Topo4BodySimpleDecision",

    ##others
    "Hlt2PassThroughDecision",
    "Hlt2TransparentDecision",
    "Hlt2IncPhiDecision",
    # Exclusive lines,
    "Hlt2RareStrangeSigmaPMuMuDecision", "Hlt2RareStrangeKPiMuMuDecision", "Hlt2RareStrangeKPiMuMuDecision", "Hlt2StrangeKPiPiPiDecision",
    ## inclusive decisions
    "Hlt2DiMuonDY.*Decision","Hlt2TopoE.*Decision", "Hlt2Topo.*Decision",  "Hlt2Charm.*Decision", 'Hlt2DiElectron.*Decision', 'Hlt2.*GammaDecision'
    ]



#triggerListF = L0Triggers + Hlt1Triggers + Hlt2Triggers
#if datatype == "2015" :
triggerListF = L0Triggers + Hlt1TriggersRunII + Hlt2TriggersRunII



if not isPGun:
    myNTUPLE.addTool(TupleToolTISTOS)
    myNTUPLE.TupleToolTISTOS.VerboseL0 = True
    myNTUPLE.TupleToolTISTOS.VerboseHlt1 = True
    myNTUPLE.TupleToolTISTOS.VerboseHlt2 = True
    myNTUPLE.TupleToolTISTOS.OutputLevel = WARNING
    myNTUPLE.TupleToolTISTOS.TriggerList = triggerListF
    #myNTUPLE.TupleToolTISTOS.PIDList = ["211", "13"]


myNTUPLE.addTool(TupleToolGeometry)
myNTUPLE.TupleToolGeometry.Verbose = True

myNTUPLE.addTool(TupleToolTrackInfo)
myNTUPLE.TupleToolTrackInfo.Verbose = True

myNTUPLE.addTool(TupleToolVtxDaughters)
myNTUPLE.TupleToolVtxDaughters.FillSubVtxInfo = True
##myNTUPLE.addTool(TupleToolDalitz)
##myNTUPLE.TupleToolDalitz.OutputLevel = VERBOSE


## ---------- For storing some event variables   ---------##
from DecayTreeTuple.Configuration import *

if not microDST :
    LoKiEventTuple = LoKi__Hybrid__EvtTupleTool("LoKiEvent")
    LoKiEventTuple.Preambulo = [
        "from LoKiTracks.decorators import *",
        "from LoKiCore.functions import *"
        ]
    LoKiEventTuple.VOID_Variables =  {
        "nTTCls"   :  "CONTAINS('Raw/TT/Clusters') " , ## number of Clusters in TT
        "nVeloTrks"   :   " TrSOURCE('Rec/Track/Best' , TrVELO) >> TrSIZE " , ## number of Velo tracks
        "nLongTrks"   :   " TrSOURCE('Rec/Track/Best', TrLONG) >> TrSIZE " , ## number of Long tracks
        "nDownTrks"   :   " TrSOURCE('Rec/Track/Best', TrDOWNSTREAM) >> TrSIZE " , ## number of Down tracks
        "nTTrks"      :   " TrSOURCE('Rec/Track/Best', TrTTRACK) >> TrSIZE ",  ## number of T tracks
        }

    myNTUPLE.addTool(LoKiEventTuple)


## ---------- Yet some other stuff   ---------##
LoKiTuple = myNTUPLE.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_All")
#LoKiTuple = LoKi__Hybrid__TupleTool("LoKi_All")
LoKiTuple.Variables = {"BPVDIRA" : "BPVDIRA"
                       , "BPVVDCHI2" : "BPVVDCHI2"
                       , "BPVIPCHI2" : "BPVIPCHI2()"
                       , "BPVVDZ" : "BPVVDZ"
                       , "VFASPF" : "VFASPF(VCHI2/VDOF)"
                       , "DOCAMAX" : "DOCAMAX"
                       , "MINIPCHI2": "MIPCHI2DV(PRIMARY)"
                       }
myNTUPLE.addTool(LoKiTuple, name = "LoKi_All")

LoKi_Sigma = LoKi__Hybrid__TupleTool('LoKi_Sigma')
LoKi_Sigma.Variables = {
    'ETA'                  : 'ETA'
    ,'DOCA'                 : 'DOCA(1,2)'
    ,'Y'                    : 'Y'
    ,'LOKI_FDS'             : 'BPVDLS'
    ,'LOKI_DTF_CHI2NDOF'    : 'DTF_CHI2NDOF(False)'
    ,'LOKI_DTF_VCHI2NDOF'   : 'DTF_FUN(VFASPF(VCHI2/VDOF), True)'
    ,"LOKI_DTF_M_PVconstr" : "DTF_FUN ( M , True)"  #PV constraint
    ,"LOKI_DTF_M_noconstr"  : "DTF_FUN ( M , False)"  #no constraints
    ,"LOKI_DTF_PE"      : "DTF_FUN(E,  False)"  #no constraints
    ,"LOKI_DTF_PX"      : "DTF_FUN(PX,  False)"  #no constraints
    ,"LOKI_DTF_PY"      : "DTF_FUN(PY,  False)"  #no constraints
    ,"LOKI_DTF_PZ"      : "DTF_FUN(PZ,  False)"  #no constraints
    ,"LOKI_DTF_P"   : "DTF_FUN(P, False)"  #no constraints
    ,"LOKI_DTF_PT"   : "DTF_FUN(PT, False)"  #no constraints
}



myNTUPLE.addTool(TupleToolDecay, name="Sigma")
myNTUPLE.addTool(TupleToolDecay, name="proton")
myNTUPLE.addTool(TupleToolDecay, name="muplus")
myNTUPLE.addTool(TupleToolDecay, name="muminus")
myNTUPLE.addTool(TupleToolDecay, name="pizero")
myNTUPLE.addTool(TupleToolDecay, name="eplus")
myNTUPLE.addTool(TupleToolDecay, name="eminus")
myNTUPLE.addTool(TupleToolDecay, name="electron")
myNTUPLE.addTool(TupleToolDecay, name="muon")
myNTUPLE.addTool(TupleToolDecay, name="piminus")
myNTUPLE.addTool(TupleToolDecay, name="piplus")
myNTUPLE.addTool(TupleToolDecay, name="pion")
myNTUPLE.addTool(TupleToolDecay, name="K")
myNTUPLE.addTool(TupleToolDecay, name="Lambda")




def addRelatedInfo(tuple, particle, inputpath, varprefix, varpostfix):
    tuplehead = tuple.Sigma
    if tuple.name().startswith("K") :
        tuplehead = tuple.K
    elif tuple.name().startswith("Lambda") :
        tuplehead = tuple.Lambda

    tuplehead.ToolList+=["LoKi::Hybrid::TupleTool/"+particle+"_Iso"]
    LoKi_iso_vars = LoKi__Hybrid__TupleTool(particle+'_Iso')

    LoKi_iso_vars.Variables = {
        particle+"_cangle_1.00" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cangle_0.90" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cangle_1.10" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEANGLE', -1.)",
        particle+"_cpt_1.00" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cpt_0.90" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cpt_1.10" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEPTASYM', -1.)",
        particle+"_cmult_1.00" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"10"+varpostfix+"', 'CONEMULT', -1.)",
        particle+"_cmult_0.90" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"09"+varpostfix+"', 'CONEMULT', -1.)",
        particle+"_cmult_1.10" : "RELINFO('"+RelInfoPrefix+"Leptonic/"+inputpath.replace("Particles","")+varprefix+"11"+varpostfix+"', 'CONEMULT', -1.)",

    }
    tuplehead.addTool(LoKi_iso_vars)
    return LoKi_iso_vars


#============================================================

SigmaPMuMuTuple = myNTUPLE.clone("SigmaPMuMuTuple")
SigmaPMuMuTuple.Inputs = [ SigmaPMuMuInput ]
SigmaPMuMuTuple.Decay = "[Sigma+ -> ^p+ ^mu+ ^mu-]CC"
SigmaPMuMuTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ mu+ mu-]CC",
    "muminus" : "[Sigma+ -> p+ mu+ ^mu-]CC",
    "muplus" : "[Sigma+ -> p+ ^mu+ mu-]CC",
    "proton" : "[Sigma+ -> ^p+ mu+ mu-]CC"
    }
SigmaPMuMuTuple.addTool(TupleToolVtxDaughters)
SigmaPMuMuTuple.Sigma.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_Sigma']
SigmaPMuMuTuple.Sigma.addTool(LoKi_Sigma)
addRelatedInfo(SigmaPMuMuTuple, "Sigma", SigmaPMuMuInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuTuple, "proton", SigmaPMuMuInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuTuple, "muplus", SigmaPMuMuInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuTuple, "muminus", SigmaPMuMuInput, "P2CVMu", "_2")

# DTF refit
from Configurables import TupleToolDecayTreeFitter

SigmaPMuMuTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
SigmaPMuMuTuple.Sigma.addTool(TupleToolDecayTreeFitter('NoConstr'))
SigmaPMuMuTuple.Sigma.NoConstr.Verbose = True
SigmaPMuMuTuple.Sigma.NoConstr.UpdateDaughters = False
SigmaPMuMuTuple.Sigma.NoConstr.constrainToOriginVertex = False

SigmaPMuMuTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
SigmaPMuMuTuple.Sigma.addTool(TupleToolDecayTreeFitter('PVConstr'))
SigmaPMuMuTuple.Sigma.PVConstr.Verbose = True
SigmaPMuMuTuple.Sigma.PVConstr.UpdateDaughters = False
SigmaPMuMuTuple.Sigma.PVConstr.constrainToOriginVertex = True

SigmaPMuMuDownTuple = myNTUPLE.clone("SigmaPMuMuDownTuple")
SigmaPMuMuDownTuple.Inputs = [ SigmaPMuMuDownInput ]
SigmaPMuMuDownTuple.Decay = "[Sigma+ -> ^p+ ^mu+ ^mu-]CC"
SigmaPMuMuDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ mu+ mu-]CC",
    "muminus" : "[Sigma+ -> p+ mu+ ^mu-]CC",
    "muplus" : "[Sigma+ -> p+ ^mu+ mu-]CC",
    "proton" : "[Sigma+ -> ^p+ mu+ mu-]CC"
    }

addRelatedInfo(SigmaPMuMuDownTuple, "Sigma", SigmaPMuMuDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuDownTuple, "proton", SigmaPMuMuDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuDownTuple, "muplus", SigmaPMuMuDownInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuDownTuple, "muminus", SigmaPMuMuDownInput, "P2CVMu", "_2")


SigmaPMuMuDetTuple = myNTUPLE.clone("SigmaPMuMuDetTuple")
SigmaPMuMuDetTuple.Inputs = [    SigmaPMuMuDetInput ]
SigmaPMuMuDetTuple.Decay = "[Sigma+ -> ^p+ ^(KS0 -> ^mu+ ^mu-)]CC"
SigmaPMuMuDetTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ (KS0 -> mu+ mu-)]CC",
    "muminus" : "[Sigma+ -> p+ (KS0 -> mu+ ^mu-)]CC",
    "muplus" : "[Sigma+ -> p+ (KS0 -> ^mu+ mu-)]CC",
    "proton" : "[Sigma+ -> ^p+ (KS0 -> mu+ mu-)]CC",
    "dimuon" : "[Sigma+ -> p+ ^(KS0 -> mu+ mu-)]CC"
    }

addRelatedInfo(SigmaPMuMuDetTuple, "Sigma", SigmaPMuMuDetInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuDetTuple, "proton", SigmaPMuMuDetInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuDetTuple, "muplus", SigmaPMuMuDetInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuDetTuple, "muminus", SigmaPMuMuDetInput, "P2CVMu", "_2")

SigmaPMuMuDetTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
SigmaPMuMuDetTuple.Sigma.addTool(TupleToolDecayTreeFitter('NoConstr'))
SigmaPMuMuDetTuple.Sigma.NoConstr.Verbose = True
SigmaPMuMuDetTuple.Sigma.NoConstr.UpdateDaughters = False
SigmaPMuMuDetTuple.Sigma.NoConstr.constrainToOriginVertex = False

SigmaPMuMuDetTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
SigmaPMuMuDetTuple.Sigma.addTool(TupleToolDecayTreeFitter('PVConstr'))
SigmaPMuMuDetTuple.Sigma.PVConstr.Verbose = True
SigmaPMuMuDetTuple.Sigma.PVConstr.UpdateDaughters = False


SigmaPMuMuLFVTuple = myNTUPLE.clone("SigmaPMuMuLFVTuple")
SigmaPMuMuLFVTuple.Inputs = [ SigmaPMuMuLFVInput ]
SigmaPMuMuLFVTuple.Decay = "[Sigma+ -> ^p~- ^mu+ ^mu+]CC"
SigmaPMuMuLFVTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p~- mu+ mu+]CC",
    "muminus" : "[Sigma+ -> p~- ^mu+ mu+]CC",
    "muplus" : "[Sigma+ -> p~- mu+ ^mu+]CC",
    "proton" : "[Sigma+ -> ^p~- mu+ mu+]CC"
    }
addRelatedInfo(SigmaPMuMuLFVTuple, "Sigma", SigmaPMuMuLFVInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuLFVTuple, "proton", SigmaPMuMuLFVInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuLFVTuple, "muplus", SigmaPMuMuLFVInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuLFVTuple, "muminus", SigmaPMuMuLFVInput, "P2CVMu", "_2")

SigmaPMuMuLFVTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
SigmaPMuMuLFVTuple.Sigma.addTool(TupleToolDecayTreeFitter('NoConstr'))
SigmaPMuMuLFVTuple.Sigma.NoConstr.Verbose = True
SigmaPMuMuLFVTuple.Sigma.NoConstr.UpdateDaughters = False
SigmaPMuMuLFVTuple.Sigma.NoConstr.constrainToOriginVertex = False

SigmaPMuMuLFVTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
SigmaPMuMuLFVTuple.Sigma.addTool(TupleToolDecayTreeFitter('PVConstr'))
SigmaPMuMuLFVTuple.Sigma.PVConstr.Verbose = True
SigmaPMuMuLFVTuple.Sigma.PVConstr.UpdateDaughters = False
SigmaPMuMuLFVTuple.Sigma.PVConstr.constrainToOriginVertex = True


SigmaPMuMuLFVDownTuple = myNTUPLE.clone("SigmaPMuMuLFVDownTuple")
SigmaPMuMuLFVDownTuple.Inputs = [ SigmaPMuMuLFVDownInput ]
SigmaPMuMuLFVDownTuple.Decay = "[Sigma+ -> ^p~- ^mu+ ^mu+]CC"
SigmaPMuMuLFVDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p~- mu+ mu+]CC",
    "muminus" : "[Sigma+ -> p~- ^mu+ mu+]CC",
    "muplus" : "[Sigma+ -> p~- mu+ ^mu+]CC",
    "proton" : "[Sigma+ -> ^p~- mu+ mu+]CC"
    }
addRelatedInfo(SigmaPMuMuLFVDownTuple, "Sigma", SigmaPMuMuLFVDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "proton", SigmaPMuMuLFVDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "muplus", SigmaPMuMuLFVDownInput, "P2CVMu", "_1")
addRelatedInfo(SigmaPMuMuLFVDownTuple, "muminus", SigmaPMuMuLFVDownInput, "P2CVMu", "_2")


SigmaPEMuTuple = myNTUPLE.clone("SigmaPEMuTuple")
#SigmaPEMuTuple.ToolList.remove("TupleToolDalitz") #bad crash
SigmaPEMuTuple.Inputs = [    SigmaPEMuInput ]
SigmaPEMuTuple.Decay =  "( ([Sigma+ -> ^p+ ^e+ ^mu-]CC) || ([Sigma+ -> ^p+ ^mu+ ^e-]CC) )"
SigmaPEMuTuple.Branches = {
    "Sigma"  : "( (^[Sigma+ -> p+ e+ mu-]CC) || (^[Sigma+ -> p+ mu+ e-]CC) )",
    "electron" :"( ([Sigma+ -> p+ ^e+ mu-]CC) || ([Sigma+ -> p+ mu+ ^e-]CC) )",
    "muon" :  "( ([Sigma+ -> p+ e+ ^mu-]CC) || ([Sigma+ -> p+ ^mu+ e-]CC) )",
    "proton" : "( ([Sigma+ -> ^p+ e+ mu-]CC) || ([Sigma+ -> ^p+ mu+ e-]CC) )"
    }
addRelatedInfo(SigmaPEMuTuple, "Sigma", SigmaPEMuInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEMuTuple, "proton", SigmaPEMuInput, "P2CVProton", "")
addRelatedInfo(SigmaPEMuTuple, "muon", SigmaPEMuInput, "P2CVMu", "")
addRelatedInfo(SigmaPEMuTuple, "electron", SigmaPEMuInput, "P2CVE", "")



SigmaPEETuple = myNTUPLE.clone("SigmaPEETuple")
SigmaPEETuple.Inputs = [    SigmaPEEInput ]
SigmaPEETuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
SigmaPEETuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
    "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
    "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
    "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
    }
addRelatedInfo(SigmaPEETuple, "Sigma", SigmaPEEInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEETuple, "proton", SigmaPEEInput, "P2CVProton", "")
addRelatedInfo(SigmaPEETuple, "eplus", SigmaPEEInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEETuple, "eminus", SigmaPEEInput, "P2CVE", "_2")

SigmaPEEDownTuple = myNTUPLE.clone("SigmaPEEDownTuple")
SigmaPEEDownTuple.Inputs = [    SigmaPEEDownInput ]
SigmaPEEDownTuple.Decay = "[Sigma+ -> ^p+ ^e+ ^e-]CC"
SigmaPEEDownTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ e+ e-]CC",
    "eminus" : "[Sigma+ -> p+ e+ ^e-]CC",
    "eplus" : "[Sigma+ -> p+ ^e+ e-]CC",
    "proton" : "[Sigma+ -> ^p+ e+ e-]CC"
    }
addRelatedInfo(SigmaPEEDownTuple, "Sigma", SigmaPEEDownInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEEDownTuple, "proton", SigmaPEEDownInput, "P2CVProton", "")
addRelatedInfo(SigmaPEEDownTuple, "eplus", SigmaPEEDownInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEEDownTuple, "eminus", SigmaPEEDownInput, "P2CVE", "_2")


SigmaPEEDetTuple = myNTUPLE.clone("SigmaPEEDetTuple")
SigmaPEEDetTuple.Inputs = [    SigmaPEEDetInput ]
SigmaPEEDetTuple.Decay = "[Sigma+ -> ^p+ ^(KS0 -> ^e+ ^e-)]CC"
SigmaPEEDetTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ (KS0 -> e+ e-)]CC",
    "eminus" : "[Sigma+ -> p+ (KS0 -> e+ ^e-)]CC",
    "eplus" : "[Sigma+ -> p+ (KS0 -> ^e+ e-)]CC",
    "proton" : "[Sigma+ -> ^p+ (KS0 -> e+ e-)]CC",
    "dielectron" : "[Sigma+ -> p+ ^(KS0 -> e+ e-)]CC"
    }

addRelatedInfo(SigmaPEEDetTuple, "Sigma", SigmaPEEDetInput, "P2CVSigma", "")
addRelatedInfo(SigmaPEEDetTuple, "proton", SigmaPEEDetInput, "P2CVProton", "")
addRelatedInfo(SigmaPEEDetTuple, "eplus", SigmaPEEDetInput, "P2CVE", "_1")
addRelatedInfo(SigmaPEEDetTuple, "eminus", SigmaPEEDetInput, "P2CVE", "_2")


SigmaPPi0CalTuple = myNTUPLE.clone("SigmaPPi0CalTuple")
SigmaPPi0CalTuple.Inputs = [    SigmaPPi0CalInput ]
#SigmaPPi0CalTuple.ToolList.remove("TupleToolVtxDaughters") #Cannot use on two body
SigmaPPi0CalTuple.Decay = "[Sigma+ -> ^p+ ^pi0]CC"
SigmaPPi0CalTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ pi0]CC",
    "proton" : "[Sigma+ -> ^p+ pi0]CC",
    "pizero" : "[Sigma+ -> p+ ^pi0]CC"
    }
addRelatedInfo(SigmaPPi0CalTuple, "Sigma", SigmaPPi0CalInput, "P2CVSigma", "")
addRelatedInfo(SigmaPPi0CalTuple, "proton", SigmaPPi0CalInput, "P2CVProton", "")
addRelatedInfo(SigmaPPi0CalTuple, "pizero", SigmaPPi0CalInput, "P2CVPi0", "")

#SigmaPPi0CalTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
#SigmaPPi0CalTuple.Sigma.addTool(TupleToolDecayTreeFitter('NoConstr'))
#SigmaPPi0CalTuple.Sigma.NoConstr.Verbose = True
#SigmaPPi0CalTuple.Sigma.NoConstr.UpdateDaughters = False
#SigmaPPi0CalTuple.Sigma.NoConstr.constrainToOriginVertex = False

#SigmaPPi0CalTuple.Sigma.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
#SigmaPPi0CalTuple.Sigma.addTool(TupleToolDecayTreeFitter('PVConstr'))
#SigmaPPi0CalTuple.Sigma.PVConstr.Verbose = True
#SigmaPPi0CalTuple.Sigma.PVConstr.UpdateDaughters = False
#SigmaPPi0CalTuple.Sigma.PVConstr.constrainToOriginVertex = True



SigmaPPi0DalTuple = myNTUPLE.clone("SigmaPPi0DalTuple")
SigmaPPi0DalTuple.Inputs = [    SigmaPPi0DalInput ]
#SigmaPPi0DalTuple.ToolList.remove("TupleToolVtxDaughters") #Cannot use on two body
SigmaPPi0DalTuple.Decay = "[Sigma+ -> ^p+ ^pi0]CC"
SigmaPPi0DalTuple.Branches = {
    "Sigma"  : "^[Sigma+ -> p+ pi0]CC",
    "proton" : "[Sigma+ -> ^p+ pi0]CC",
    "pizero" : "[Sigma+ -> p+ ^pi0]CC"
    }
addRelatedInfo(SigmaPPi0DalTuple, "Sigma", SigmaPPi0DalInput, "P2CVSigma", "")
addRelatedInfo(SigmaPPi0DalTuple, "proton", SigmaPPi0DalInput, "P2CVProton", "")
addRelatedInfo(SigmaPPi0DalTuple, "pizero", SigmaPPi0DalInput, "P2CVPi0", "")

KPiMuMuTuple = myNTUPLE.clone("KPiMuMuTuple")
KPiMuMuTuple.Inputs = [ KPiMuMuInput ]
KPiMuMuTuple.Decay = "[K+ -> ^pi+ ^mu+ ^mu-]CC"
KPiMuMuTuple.Branches = {
    "K"  : "^[K+ -> pi+ mu+ mu-]CC",
    "muminus" : "[K+ -> pi+ mu+ ^mu-]CC",
    "muplus" : "[K+ -> pi+ ^mu+ mu-]CC",
    "pion" : "[K+ -> ^pi+ mu+ mu-]CC"
    }
addRelatedInfo(KPiMuMuTuple, "K", KPiMuMuInput, "P2CVK", "")
addRelatedInfo(KPiMuMuTuple, "pion", KPiMuMuInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuTuple, "muplus", KPiMuMuInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuTuple, "muminus", KPiMuMuInput, "P2CVMu", "_2")

KPiMuMuDownTuple = myNTUPLE.clone("KPiMuMuDownTuple")
KPiMuMuDownTuple.Inputs = [ KPiMuMuDownInput ]
KPiMuMuDownTuple.Decay = "[K+ -> ^pi+ ^mu+ ^mu-]CC"
KPiMuMuDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ mu+ mu-]CC",
    "muminus" : "[K+ -> pi+ mu+ ^mu-]CC",
    "muplus" : "[K+ -> pi+ ^mu+ mu-]CC",
    "pion" : "[K+ -> ^pi+ mu+ mu-]CC"
    }
addRelatedInfo(KPiMuMuDownTuple, "K", KPiMuMuDownInput, "P2CVK", "")
addRelatedInfo(KPiMuMuDownTuple, "pion", KPiMuMuDownInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuDownTuple, "muplus", KPiMuMuDownInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuDownTuple, "muminus", KPiMuMuDownInput, "P2CVMu", "_2")



KPiMuMuLFVTuple = myNTUPLE.clone("KPiMuMuLFVTuple")
KPiMuMuLFVTuple.Inputs = [ KPiMuMuLFVInput ]
KPiMuMuLFVTuple.Decay = "[K+ -> ^pi- ^mu+ ^mu+]CC"
KPiMuMuLFVTuple.Branches = {
    "K"  : "^[K+ -> pi- mu+ mu+]CC",
    "muminus" : "[K+ -> pi- mu+ ^mu+]CC",
    "muplus" : "[K+ -> pi- ^mu+ mu+]CC",
    "pion" : "[K+ -> ^pi- mu+ mu+]CC"
    }
addRelatedInfo(KPiMuMuLFVTuple, "K", KPiMuMuLFVInput, "P2CVK", "")
addRelatedInfo(KPiMuMuLFVTuple, "pion", KPiMuMuLFVInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuLFVTuple, "muplus", KPiMuMuLFVInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuLFVTuple, "muminus", KPiMuMuLFVInput, "P2CVMu", "_2")


KPiMuMuLFVDownTuple = myNTUPLE.clone("KPiMuMuLFVDownTuple")
KPiMuMuLFVDownTuple.Inputs = [ KPiMuMuLFVDownInput ]
KPiMuMuLFVDownTuple.Decay = "[K+ -> ^pi- ^mu+ ^mu+]CC"
KPiMuMuLFVDownTuple.Branches = {
    "K"  : "^[K+ -> pi- mu+ mu+]CC",
    "muminus" : "[K+ -> pi- mu+ ^mu+]CC",
    "muplus" : "[K+ -> pi- ^mu+ mu+]CC",
    "pion" : "[K+ -> ^pi- mu+ mu+]CC"
    }
addRelatedInfo(KPiMuMuLFVDownTuple, "K", KPiMuMuLFVDownInput, "P2CVK", "")
addRelatedInfo(KPiMuMuLFVDownTuple, "pion", KPiMuMuLFVDownInput, "P2CVPi", "")
addRelatedInfo(KPiMuMuLFVDownTuple, "muplus", KPiMuMuLFVDownInput, "P2CVMu", "_1")
addRelatedInfo(KPiMuMuLFVDownTuple, "muminus", KPiMuMuLFVDownInput, "P2CVMu", "_2")


KPiPiPiTuple = myNTUPLE.clone("KPiPiPiTuple")
KPiPiPiTuple.Inputs = [ KPiPiPiInput ]
KPiPiPiTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiTuple, "K", KPiPiPiInput, "P2CVK", "")
addRelatedInfo(KPiPiPiTuple, "pion", KPiPiPiInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiTuple, "piplus", KPiPiPiInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiTuple, "piminus", KPiPiPiInput, "P2CVPi", "_2")


KPiPiPiDownTuple = myNTUPLE.clone("KPiPiPiDownTuple")
KPiPiPiDownTuple.Inputs = [ KPiPiPiDownInput ]
KPiPiPiDownTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiDownTuple, "K", KPiPiPiDownInput, "P2CVK", "")
addRelatedInfo(KPiPiPiDownTuple, "pion", KPiPiPiDownInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiDownTuple, "piplus", KPiPiPiDownInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiDownTuple, "piminus", KPiPiPiDownInput, "P2CVPi", "_2")


KPiPiPiMassMeasTuple = myNTUPLE.clone("KPiPiPiMassMeasTuple")
KPiPiPiMassMeasTuple.Inputs = [ KPiPiPiMassMeasInput ]
KPiPiPiMassMeasTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiMassMeasTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiMassMeasTuple, "K", KPiPiPiMassMeasInput, "P2CVK", "")
addRelatedInfo(KPiPiPiMassMeasTuple, "pion", KPiPiPiMassMeasInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiMassMeasTuple, "piplus", KPiPiPiMassMeasInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiMassMeasTuple, "piminus", KPiPiPiMassMeasInput, "P2CVPi", "_2")

KPiPiPiMassMeasTuple.K.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
KPiPiPiMassMeasTuple.K.addTool(TupleToolDecayTreeFitter('NoConstr'))
KPiPiPiMassMeasTuple.K.NoConstr.Verbose = True
KPiPiPiMassMeasTuple.K.NoConstr.UpdateDaughters = False
KPiPiPiMassMeasTuple.K.NoConstr.constrainToOriginVertex = False

KPiPiPiMassMeasTuple.K.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
KPiPiPiMassMeasTuple.K.addTool(TupleToolDecayTreeFitter('PVConstr'))
KPiPiPiMassMeasTuple.K.PVConstr.Verbose = True
KPiPiPiMassMeasTuple.K.PVConstr.UpdateDaughters = False
KPiPiPiMassMeasTuple.K.PVConstr.constrainToOriginVertex = True


KPiPiPiMassMeasDownTuple = myNTUPLE.clone("KPiPiPiMassMeasDownTuple")
KPiPiPiMassMeasDownTuple.Inputs = [ KPiPiPiMassMeasDownInput ]
KPiPiPiMassMeasDownTuple.Decay = "[K+ -> ^pi+ ^pi+ ^pi-]CC"
KPiPiPiMassMeasDownTuple.Branches = {
    "K"  : "^[K+ -> pi+ pi+ pi-]CC",
    "piminus" : "[K+ -> pi+ pi+ ^pi-]CC",
    "piplus" : "[K+ -> pi+ ^pi+ pi-]CC",
    "pion" : "[K+ -> ^pi+ pi+ pi-]CC"
    }
addRelatedInfo(KPiPiPiMassMeasDownTuple, "K", KPiPiPiMassMeasDownInput, "P2CVK", "")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "pion", KPiPiPiMassMeasDownInput, "P2CVPi", "")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "piplus", KPiPiPiMassMeasDownInput, "P2CVPi", "_1")
addRelatedInfo(KPiPiPiMassMeasDownTuple, "piminus", KPiPiPiMassMeasDownInput, "P2CVPi", "_2")


LambdaPPiTuple = myNTUPLE.clone("LambdaPPiTuple")
LambdaPPiTuple.Inputs = [ LambdaPPiInput ]
LambdaPPiTuple.Decay = "[Lambda0 -> ^p+ ^pi-]CC"
#LambdaPPiTuple.ToolList.remove("TupleToolVtxDaughters") #Cannot use on two body
LambdaPPiTuple.Branches = {
    "Lambda"  : "^[Lambda0 -> p+ pi-]CC" ,
    "proton" : "[Lambda0 -> ^p+ pi-]CC" ,
    "pion" : "[Lambda0 -> p+ ^pi-]CC" ,
    }
addRelatedInfo(LambdaPPiTuple, "Lambda", LambdaPPiInput, "P2CVSigma", "") # Not a mistake
addRelatedInfo(LambdaPPiTuple, "pion", LambdaPPiInput, "P2CVPi", "")
addRelatedInfo(LambdaPPiTuple, "proton", LambdaPPiInput, "P2CVProton", "")

LambdaPPiTuple.Lambda.ToolList += ['TupleToolDecayTreeFitter/NoConstr']
LambdaPPiTuple.Lambda.addTool(TupleToolDecayTreeFitter('NoConstr'))
LambdaPPiTuple.Lambda.NoConstr.Verbose = True
LambdaPPiTuple.Lambda.NoConstr.UpdateDaughters = False
LambdaPPiTuple.Lambda.NoConstr.constrainToOriginVertex = False

LambdaPPiTuple.Lambda.ToolList += ['TupleToolDecayTreeFitter/PVConstr']
LambdaPPiTuple.Lambda.addTool(TupleToolDecayTreeFitter('PVConstr'))
LambdaPPiTuple.Lambda.PVConstr.Verbose = True
LambdaPPiTuple.Lambda.PVConstr.UpdateDaughters = False
LambdaPPiTuple.Lambda.PVConstr.constrainToOriginVertex = True



LambdaPPiEETuple = myNTUPLE.clone("LambdaPPiEETuple")
LambdaPPiEETuple.Inputs = [ LambdaPPiEEInput ]
LambdaPPiEETuple.Decay = "[Lambda0 -> ^p+ ^pi- ^(KS0 -> ^e+ ^e-)]CC"
LambdaPPiEETuple.Branches = {
    "Lambda"  : "^[Lambda0 -> p+ pi- (KS0 -> e+ e-)]CC" ,
    "proton" : "[Lambda0 -> ^p+ pi- (KS0 -> e+ e-)]CC" ,
    "pion" : "[Lambda0 -> p+ ^pi-  (KS0 -> e+ e-)]CC" ,
    "dielectron" : "[Lambda0 -> p+ pi-  ^(KS0 -> e+ e-)]CC" ,
    "eplus" : "[Lambda0 -> p+ ^pi-  (KS0 -> ^e+ e-)]CC" ,
    "eminus" : "[Lambda0 -> p+ ^pi-  (KS0 -> e+ ^e-)]CC" ,
    }
addRelatedInfo(LambdaPPiEETuple, "Lambda", LambdaPPiEEInput, "P2CVSigma", "") # Not a mistake
addRelatedInfo(LambdaPPiEETuple, "pion", LambdaPPiEEInput, "P2CVPi", "")
addRelatedInfo(LambdaPPiEETuple, "proton", LambdaPPiEEInput, "P2CVProton", "")
# isolation on the other is not in stripping... forget about it


PhiKMuTuple = myNTUPLE.clone("PhiKMuTuple")
PhiKMuTuple.Inputs = [ PhiKMuInput ]
PhiKMuTuple.Decay = "[phi(1020) -> ^K+ ^mu-]CC"
#PhiKMuTuple.ToolList.remove("TupleToolVtxDaughters") #Cannot use on two body
PhiKMuTuple.Branches = {
    "phi"  : "[phi(1020) -> K+ mu-]CC",
    "kaon" :  "[phi(1020) -> ^K+ mu-]CC",
    "muon" :  "[phi(1020) -> K+ ^mu-]CC"
    }



from Configurables import PrintMCTree
pMC = PrintMCTree()
pMC.ParticleNames = [ "Sigma+" ]
if not data and printTree:
    DaVinci().UserAlgorithms += [pMC]


DaVinci().TupleFile = 'Sigmapmumu.root'

#============================================================
if veloMuonTracking :
    DaVinci().appendToMainSequence([alg])


from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

if (isMDST):

# Kill DAQ, temporary fix
    from Configurables import EventNodeKiller
    eventNodeKiller = EventNodeKiller('DAQkiller')
    eventNodeKiller.Nodes = ['/Event/DAQ','/Event/pRec']




#MessageSvc().OutputLevel = DEBUG
MessageSvc().OutputLevel = INFO
if doTuples :
    if onlySigma:
        DaVinci().MoniSequence += [ SigmaPMuMuTuple, SigmaPMuMuDownTuple, SigmaPMuMuLFVTuple, #SigmaPMuMuLFVDownTuple,
                                    SigmaPMuMuDetTuple, SigmaPPi0CalTuple ]
    else :

        DaVinci().MoniSequence += [ SigmaPMuMuTuple,
                                    #SigmaPMuMuDownTuple,
                                    SigmaPMuMuLFVTuple,# SigmaPMuMuLFVDownTuple,
                                    SigmaPMuMuDetTuple,
                                    SigmaPPi0CalTuple, #SigmaPPi0DalTuple,
                                    #KPiPiPiTuple, KPiPiPiDownTuple,
                                    KPiPiPiMassMeasTuple,
                                    #KPiPiPiMassMeasDownTuple,
                                    LambdaPPiTuple,
                                    #LambdaPPiEETuple
                                    #PhiKMuTuple
                                    ]

if debug:
    DaVinci().DDDBtag = 'dddb-20150724'
    DaVinci().CondDBtag = 'cond-20161004' #DATA
    DaVinci().TupleFile     = "prova.root"
    DaVinci().HistogramFile = 'DVHistos.root'
    DaVinci().InputType     = 'MDST'
    DaVinci().Simulation    = False
    DaVinci().Lumi          = True
    DaVinci().PrintFreq     = 500
    DaVinci().EvtMax        = evtot
    DaVinci().DataType      = '2016'
    DaVinci().SkipEvents    = 0

    from GaudiConf import IOExtension
    IOExtension().inputFiles(['/eos/lhcb/user/j/jpriscia/Sigma_Analysis/DATA/00060479_00005826_1.leptonic.mdst'], clear=True)

    EventSelector().FirstEvent = FirstEv
    if isMDST :
        DaVinci().UserAlgorithms += [eventNodeKiller]


        IODataManager().AgeLimit = 0
