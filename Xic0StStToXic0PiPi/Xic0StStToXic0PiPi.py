# coding=utf-8

# Ryunosuke O'Neil
# r.oneil@cern.ch

# DaVinci version: DaVinci/v45r6
# NOTICE: when using this options file, please make note of the below:

# - These should be set in another options file BEFORE this one:
#    DaVinci().Simulation=RUNNING_SIMULATION
#    DaVinci().DataType=DATA_YEAR
# - If Simulation == True, please don't forget also to set the
#   right DDDB and CondDB tags.


from Configurables import CheckPV
from PhysConf.Selections import MomentumScaling
from StandardParticles import StdAllNoPIDsPions

from Configurables import DaVinci
from Configurables import DstConf, TurboConf

from Configurables import (
    TupleToolRecoStats,
    TupleToolTISTOS,
    TupleToolPropertime,
    SubstitutePID,
    CondDB,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    LoKi__Hybrid__DTFDict as DTFDict,
    MCDecayTreeTuple,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
)
from TeslaTools import TeslaTruthUtils

from PhysConf.Selections import (
    FilterSelection,
    # MergedSelection,
    SelectionSequence,
    CombineSelection,
    TupleSelection,
    RebuildSelection,
    AutomaticData,
    Selection,
)

from PhysConf.Filters import LoKi_Filters

from DecayTreeTuple.Configuration import *  # noqa: F401, F403

HLT2_xicp = "Hlt2CharmHadXicpToPpKmPipTurbo"
HLT2_xic0 = "Hlt2CharmHadXic0ToPpKmKmPipTurbo"

DEBUGGING = False
N_EVENTS_MAX = -1

triggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0PhysDecision",
    "L0GlobalDecision",
    # Hlt1 track
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackPhotonDecision",
    "Hlt1GlobalDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1TrackMVALooseDecision",
    "Hlt1TwoTrackMVALooseDecision",
    "Hlt1L0AnyDecision",
]

toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]

mcToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]

Xic0Tree_LoKiVariables = {
    "BPVVDCHI2": "BPVVDCHI2",
    # IPChi2 on the related PV.
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    # Rapidity and pseudorapidity on the Xic+.
    "Y": "Y",
    "ETA": "ETA",
}


BachelorPionFilterCut = "(PT>200*MeV) & (TRGHOSTPROB<0.4) & (PROBNNpi>0.1)"

BachelorPionCut = "(BPVIPCHI2() < 9)"

Xic0StSt_CombineSelectionCuts = dict(
    CombinationCut="(AM < 3350*MeV)",
    # Avoid problems with missing PV and proper time fit
    MotherCut="""(M < 3300*MeV) & BPVVALID() & (BPVIPCHI2() < 25)""",
    CheckOverlapTool="LoKi::CheckOverlap",
    ReFitPVs=True,
    DaughtersCuts={
        # cut on pion pT > 200 offline
        "pi+": BachelorPionCut,
        "pi-": BachelorPionCut,
    },
)

XicpSt_CombineSelectionCuts = dict(
    CombinationCut="(AM < 3350*MeV) & ((AM - AM1 - AM2) < 200)",
    MotherCut=(
        "(M < 3300*MeV) & ((M - CHILD(M, 1) - CHILD(M, 2)) < 150) & "
        "((M - CHILD(M, 1) - CHILD(M, 2)) > 0) & BPVVALID() & (CHI2VXNDF < 15)"
        "& (BPVIPCHI2() > 0)"
    ),
    CheckOverlapTool="LoKi::CheckOverlap",
    ReFitPVs=True,
    DaughtersCuts={
        # cut on pion pT > 200 offline
        "pi+": BachelorPionCut,
        "pi-": BachelorPionCut,
    },
)

# No Momentum Scaling for 2018
transientEventStore_root = {
    "2015": "/Event/Turbo",
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmmultibody/Turbo",
    "2018": "/Event/Charmmultibody/Turbo",
}


def make_decay_str_and_branches(decay):
    from string import Formatter

    branches = [fn for _, fn, _, _ in Formatter().parse(decay) if fn is not None]
    branch_dict = {}
    for branch in branches:
        branch_dict[branch] = decay.format(
            **dict((b, "^" if b == branch else "") for b in branches)
        )
    decay_allcaret = decay.format(**dict((b, "^") for b in branches))
    # no caret at the beginning (for the decay key)
    return {"Decay": decay_allcaret[1:], "Branches": branch_dict}


def ConfigureDTT(dtt, TISTOS_Branches=[]):
    dtt.ReFitPVs = True
    dtt.ToolList = toolList[:]

    tt_proptime = dtt.addTupleTool(TupleToolPropertime)
    tt_proptime.Verbose = True

    tt_recostats = dtt.addTupleTool(TupleToolRecoStats)
    tt_recostats.Verbose = True

    for br in TISTOS_Branches:
        tt_tistos = br.addTupleTool(TupleToolTISTOS)
        tt_tistos.Verbose = True
        tt_tistos.VerboseHlt1 = True
        tt_tistos.VerboseL0 = True
        tt_tistos.FillHlt2 = False
        tt_tistos.TriggerList = triggerList

    if DaVinci().Simulation:
        relations = TeslaTruthUtils.getRelLocs() + [
            TeslaTruthUtils.getRelLoc(""),
            # Location of the truth tables for PersistReco objects
            "Relations/Hlt2/Protos/Charged",
        ]
        mc_tools = mcToolList[:]
        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)

        tt_mctruth = dtt.addTupleTool(TupleToolMCTruth)
        tt_mctruth.ToolList = mcToolList[:]
        dtt.addTupleTool(TupleToolMCBackgroundInfo)

    return dtt


def ConfigureDTF(dtt, branch_name, DTFDictVariables=None, constrainParticle="Xi_c0"):
    MakeDTFD(
        dtt,
        branch_name,
        DTFDictVariables,
        tool_name="DTF_PV_XicPDG",
        constrainPV=True,
        constrainDaughters=[constrainParticle],
    )

    MakeDTFD(
        dtt,
        branch_name,
        DTFDictVariables,
        tool_name="DTF_PV",
        constrainPV=True,
    )

    MakeDTFD(
        dtt,
        branch_name,
        DTFDictVariables,
        tool_name="DTF_XicPDG",
        constrainPV=False,
        constrainDaughters=[constrainParticle],
    )


def MakeDTFD(
    dtt,
    branch_name,
    variables,
    tool_name="DTFTuple",
    constrainPV=True,
    constrainDaughters=None,
):
    branch = getattr(dtt, branch_name)

    DictTuple = branch.addTupleTool(LoKi__Hybrid__Dict2Tuple, tool_name)
    DictTuple.addTool(DTFDict, tool_name)
    DictTuple.Source = "LoKi::Hybrid::DTFDict/" + tool_name
    DictTuple.NumVar = len(
        variables.items()
    )  # reserve a suitable size for the dictionary

    DTF = getattr(DictTuple, tool_name)

    # configure the DecayTreeFitter in the usual way
    DTF.constrainToOriginVertex = constrainPV
    if constrainDaughters is not None:
        DTF.daughtersToConstrain = constrainDaughters

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict_" + tool_name)
    DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict_" + tool_name

    DTF_dict = getattr(DTF, "dict_" + tool_name)
    DTF_dict.Variables = dict(**variables)


def ConfigureXic0(dtt, branch_name):
    # LoKi variables on the Xic+ in the Xic**+ tree.
    branch = getattr(dtt, branch_name)
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = dict(**Xic0Tree_LoKiVariables)


combiners = {}


def GetCombineSelection(combiner_name, input_particles, DecayDescriptor, **kwargs):
    if combiner_name in combiners:
        combiner = combiners[combiner_name]
        assert combiner["descriptor"] == DecayDescriptor
        print("NOTE: we are RE-USING a CombineSelection object.")
        print("Descriptor: " + combiner["descriptor"])
        return combiner["object"]

    new_combiner = CombineSelection(
        combiner_name + "Combiner",
        input_particles,
        DecayDescriptor=DecayDescriptor,
        **kwargs
    )

    combiners[combiner_name] = {"descriptor": DecayDescriptor, "object": new_combiner}

    return new_combiner


def make_tree(
    tree_name,
    Xic0,
    pions,
    decay_str_with_branches,
    # If this is None, we only reconstruct the Xicp and ignore the pions.
    XicStSt_branch=None,
    # Xicp is always reconstructed.
    Xic_branch="Xic0",
    # If True, we attempt to include the intermediate Xic(2645)+ state
    include_intermediate_decay=False,
    # The bachelor pions
    Xic_particle="Xi_c0",
    bachelor_pion_1="pi+",
    bachelor_pion_2="pi-",
    **combine_selection_kwargs
):
    decay_dict = make_decay_str_and_branches(decay_str_with_branches)

    input_particles = [Xic0] if XicStSt_branch is None else [Xic0, pions]
    name = None

    if XicStSt_branch is not None:
        if XicStSt_branch == "Xic0StSt":
            if include_intermediate_decay:
                input_particles = [
                    GetCombineSelection(
                        "XicpSt_To_" + Xic_particle + "_" + bachelor_pion_1 + "_cc",
                        input_particles,
                        "[Xi_c*+ -> " + Xic_particle + " " + bachelor_pion_1 + "]cc",
                        **XicpSt_CombineSelectionCuts
                    ),
                    pions,
                ]
                decay_descriptor = "[Omega_c*0 -> Xi_c*+ " + bachelor_pion_2 + "]cc"
                name = "Xic0StSt_To_XicpSt_" + bachelor_pion_2 + "_cc"
            else:
                # Reconstruct Xic** directly.
                decay_descriptor = (
                    "[Omega_c*0 -> "
                    + Xic_particle
                    + " "
                    + bachelor_pion_2
                    + " "
                    + bachelor_pion_2
                    + "]cc"
                )
                name = (
                    "Xic0StSt_To_"
                    + Xic_particle
                    + "_"
                    + bachelor_pion_2
                    + "_"
                    + bachelor_pion_2
                    + "_cc"
                )

        elif XicStSt_branch == "XicpSt":
            decay_descriptor = (
                "[Xi_c*+ -> " + Xic_particle + " " + bachelor_pion_1 + "]cc"
            )
            name = "XicpSt_To_" + Xic_particle + "_" + bachelor_pion_1 + "_cc"
        else:
            raise RuntimeError("Unrecognised Xic0StSt_branch " + XicStSt_branch)

        assert name is not None
        combineSel = [
            GetCombineSelection(
                name,  # the name
                input_particles,  # inputs to combine
                DecayDescriptor=decay_descriptor,
                **combine_selection_kwargs
            )
        ]
    else:
        if include_intermediate_decay:
            raise RuntimeError(
                "Can't set include_intermediate_decay True if XicStSt_branch is None"
            )
        combineSel = input_particles

    tupleSel = TupleSelection(tree_name, combineSel, **decay_dict)

    dtt = tupleSel.algorithm()

    dtf_dict = {"M12": "M12", "M": "M", "PT": "PT"}

    if XicStSt_branch == "Xic0StSt" and include_intermediate_decay:
        Xic0St_dtf_dict = dict(**dtf_dict)
        Xic0St_dtf_dict[Xic_branch + "_M"] = "CHILD(M, 1)"
        ConfigureDTF(
            dtt,
            "XicpSt",
            DTFDictVariables=Xic0St_dtf_dict,
            constrainParticle=Xic_particle,
        )

    tt_br = [getattr(dtt, b) for b in [Xic_branch, XicStSt_branch] if b is not None]
    ConfigureDTT(dtt, TISTOS_Branches=tt_br)

    if XicStSt_branch is not None:
        Xic0StSt_dtf_dict = dict(**dtf_dict)
        if include_intermediate_decay:
            Xic0StSt_dtf_dict[Xic_branch + "_M"] = "CHILD(M, 1, 1)"
            Xic0StSt_dtf_dict[Xic_branch + "_PT"] = "CHILD(PT, 1, 1)"
        else:
            Xic0StSt_dtf_dict[Xic_branch + "_M"] = "CHILD(M, 1)"
            Xic0StSt_dtf_dict[Xic_branch + "_PT"] = "CHILD(PT, 1)"

        ConfigureDTF(
            dtt,
            XicStSt_branch,
            DTFDictVariables=Xic0StSt_dtf_dict,
            constrainParticle=Xic_particle,
        )

    ConfigureXic0(dtt, Xic_branch)

    return [
        SelectionSequence(
            tree_name + "_SelectionSequence",
            tupleSel,
            EventPreSelector=[CheckPV("TwoPV", MinPVs=1)],
        ).sequence()
    ]


DATA_YEAR = DaVinci().DataType
rootInTES = transientEventStore_root[DATA_YEAR]
xic0_line = "{}/Particles".format(HLT2_xic0)
xicp_line = "{}/Particles".format(HLT2_xicp)

all_sequences = []

# CondDB tag corresp. to the year (if data, and not MC)
if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)
# Otherwise, DDDB and CondDB tags set in another options file!

mcTuple_seq = None
if DaVinci().Simulation:
    mcTuple = MCDecayTreeTuple("MCTuple")

    dk_branches = make_decay_str_and_branches(
        "{Xic0StSt}([Omega_c*0 -> "
        "{Xic0}(Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+) "
        "{Pi1}pi- {Pi2}pi+]CC)"
    )
    # "[Omega_c*0 -> ^(Xi_c0 -> ^p+ ^K- K- ^pi+) ^pi+ ^pi-]CC"
    mcTuple.Decay = dk_branches["Decay"]
    mcTuple.ToolList = mcToolList[:]
    mcTuple.Branches = dk_branches["Branches"]

    mcTuple_sel = Selection("MCTree", Algorithm=mcTuple, RequiredSelections=[])
    all_sequences += [SelectionSequence("MCTREE_SEQ", mcTuple_sel).sequence()]

trigger_filter = LoKi_Filters(
    # Adjust this regular expression to match whatever set of lines you're
    # interested in studying
    HLT2_Code="HLT_PASS_RE('.*Hlt2CharmHadXic(p|0)ToPp(Km|KmKm)PipTurbo.*')"
)
# Read data from the Turbo trigger line
Xic0 = AutomaticData(xic0_line)
Xicp = AutomaticData(xicp_line)
# (2) get pions from PersistReco
pions = RebuildSelection(StdAllNoPIDsPions)

pions = FilterSelection("PionFilter", [pions], Code=BachelorPionFilterCut)

if DEBUGGING:
    from PhysConf.Selections import PrintSelection

    pions = PrintSelection(pions)


# Xic0 = FilterSelection(
#     'Xic0Filter',
#     [Xic0],
#     # Code="(CHI2VXNDF < 16) & " + \
#     #      "(INTREE((ABSID=='p+') &" + \
#     #      "(TRGHOSTPROB<0.5) &" + \
#     #      "(PROBNNp>0.2) &" + \
#     #      "(P>10000))) &" + \
#     #      "(MINTREE('K+'==ABSID, PROBNNk ) > 0.1) &" + \
#     #      "(INTREE((ABSID=='pi+') &" + \
#     #      "(TRGHOSTPROB<0.5) &" + \
#     #      "(PROBNNpi>0.1))) &" + \
#     #      "(ADMASS('Xi_c0') < 70)" # check me!
# )

subPID = SubstitutePID(
    "MakeXicpToPpKmPip",
    Code="DECTREE('[Lambda_c+ -> K- p+ pi+]CC')",
    MaxChi2PerDoF=-1,  # necessary to correctly fill loki doca variables
    Substitutions={
        "Lambda_c+  -> K- p+  pi+": "Xi_c+",
        "Lambda_c~- -> K+ p~- pi-": "Xi_c~-",
    },
)

Xicp = Selection("Xicp", Algorithm=subPID, RequiredSelections=[Xicp])

# Xic0 = Xic0Turbo

if not DaVinci().Simulation:
    # insert momentum scaling
    Xic0 = MomentumScaling(Xic0, Turbo="PERSISTRECO", Year=DATA_YEAR)
    Xicp = MomentumScaling(Xicp, Turbo="PERSISTRECO", Year=DATA_YEAR)


all_sequences += make_tree(
    tree_name="Xic0StSt_ToXic0PipPim",
    Xic0=Xic0,
    pions=pions,
    decay_str_with_branches=(
        "{Xic0StSt}[Omega_c*0 -> {XicpSt}(Xi_c*+ -> "
        "{Xic0}(Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+) "
        "{Pi1}pi+) {Pi2}pi-]CC"
    ),
    XicStSt_branch="Xic0StSt",
    Xic_branch="Xic0",
    include_intermediate_decay=True,
    bachelor_pion_1="pi+",
    bachelor_pion_2="pi-",
    **Xic0StSt_CombineSelectionCuts
)

############################################################################
# Wrong Sign Xic0 Pip Pip

all_sequences += make_tree(
    tree_name="Xic0StStWS_ToXic0PimPim",
    Xic0=Xic0,
    pions=pions,
    decay_str_with_branches=(
        "{Xic0StSt}[Omega_c*0 -> {XicpSt}(Xi_c*+ -> "
        "{Xic0}(Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+) {Pi1}pi-) "
        "{Pi2}pi-]CC"
    ),
    XicStSt_branch="Xic0StSt",
    Xic_branch="Xic0",
    include_intermediate_decay=True,
    bachelor_pion_1="pi-",
    bachelor_pion_2="pi-",
    **Xic0StSt_CombineSelectionCuts
)


#
# Tuple containing Xi_c0 info
#

all_sequences += make_tree(
    tree_name="Xic0_ToPpKmKmPip",
    Xic0=Xic0,
    pions=None,
    decay_str_with_branches=(
        "{Xic0}[Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+]CC"
    ),
    XicStSt_branch=None,
    Xic_branch="Xic0",
    Xic_particle="Xi_c0",
)

#####
# Xic*+ -> Xic0 pi+

all_sequences += make_tree(
    tree_name="XicpSt_ToXic0Pip",
    Xic0=Xic0,
    pions=pions,
    decay_str_with_branches=(
        "{XicpSt}[Xi_c*+ -> "
        "{Xic0}(Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+) "
        "{Pi1}pi+]CC"
    ),
    XicStSt_branch="XicpSt",
    Xic_branch="Xic0",
    Xic_particle="Xi_c0",
    bachelor_pion_1="pi+",
    bachelor_pion_2=None,
    **XicpSt_CombineSelectionCuts
)


#####
# Xic*+ -> Xic0 pi- WS

all_sequences += make_tree(
    tree_name="XicpStWS_ToXic0Pim",
    Xic0=Xic0,
    pions=pions,
    decay_str_with_branches=(
        "{XicpSt}[Xi_c*+ -> "
        "{Xic0}(Xi_c0 -> {Xic0_Pp}p+ {Xic0_K1m}K- {Xic0_K2m}K- {Xic0_Pip}pi+) "
        "{Pi1}pi-]CC"
    ),
    XicStSt_branch="XicpSt",
    Xic_branch="Xic0",
    Xic_particle="Xi_c0",
    bachelor_pion_1="pi-",
    bachelor_pion_2=None,
    **XicpSt_CombineSelectionCuts
)

#####
# Xic*+ -> Xic+ pi+ WS

all_sequences += make_tree(
    tree_name="XicpStWS_ToXicpPip",
    Xic0=Xicp,
    pions=pions,
    decay_str_with_branches=(
        "{XicpSt}[Xi_c*+ -> "
        "{Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) "
        "{Pi1}pi+]CC"
    ),
    XicStSt_branch="XicpSt",
    Xic_branch="Xicp",
    Xic_particle="Xi_c+",
    bachelor_pion_1="pi+",
    bachelor_pion_2=None,
    **XicpSt_CombineSelectionCuts
)


#####
# Xic*+ -> Xic+ pi- WS

all_sequences += make_tree(
    tree_name="XicpStWS_ToXicpPim",
    Xic0=Xicp,
    pions=pions,
    decay_str_with_branches=(
        "{XicpSt}[Xi_c*+ -> "
        "{Xicp}(Xi_c+ -> {Xicp_Pp}p+ {Xicp_Km}K- {Xicp_Pip}pi+) "
        "{Pi1}pi-]CC"
    ),
    XicStSt_branch="XicpSt",
    Xic_branch="Xicp",
    Xic_particle="Xi_c+",
    bachelor_pion_1="pi-",
    bachelor_pion_2=None,
    **XicpSt_CombineSelectionCuts
)

# (7) configure DaVinci
DaVinci().PrintFreq = 10000
DaVinci().RootInTES = rootInTES
DaVinci().InputType = "MDST"
DaVinci().Turbo = True

# Ignore events that don't pass the HLT2 trigger.
DaVinci().EventPreFilters = trigger_filter.filters("TriggerFilter")

# (8) insert our sequence into DaVinci
DaVinci().UserAlgorithms = all_sequences

DaVinci().Lumi = not DaVinci().Simulation

# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
    from Configurables import DataOnDemandSvc
    from Configurables import Gaudi__DataLink as Link

    dod = DataOnDemandSvc()
    for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
        dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")
