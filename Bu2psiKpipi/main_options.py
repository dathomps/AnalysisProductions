from Gaudi.Configuration import *
MessageSvc().Format = "% F%80W%S%7W%R%T %0W%M"

from Configurables import DaVinci
from Configurables import FilterDesktop
from Configurables import CombineParticles
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['DAQ','pRec']

############# Global settings
year = DaVinci().DataType
data = True
if(DaVinci().Simulation == True): data = False

stream = "AllStreams"
if(data): stream = "Leptonic"
line = 'B2XMuMu_Line'

##### Restrip MC
if(DaVinci().Simulation == True and (DaVinci().DDDBtag == 'dddb-20130929-1' or DaVinci().DDDBtag == 'dddb-20130929' )):
#if(DaVinci().Simulation == True):
    from StrippingConf.Configuration import StrippingConf, StrippingStream
    from StrippingSettings.Utils import strippingConfiguration
    from StrippingArchive.Utils import buildStreams
    from StrippingArchive import strippingArchive
    from Configurables import ( EventNodeKiller, ProcStatusCheck,DaVinci, DecayTreeTuple)
    from DecayTreeTuple.Configuration import *
    # Node killer: remove the previous Stripping
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']                             
    # Build a new stream called 'CustomStream' that only
    # contains the desired line
    strip = 'stripping21'
    if(year=='2011'): strip = 'Stripping21r1p2'
    if(year=='2012'): strip = 'Stripping21r0p2'
    if(year=='2015'): strip = 'Stripping24r2'
    if(year=='2016'): strip = 'Stripping28r2'
    if(year=='2017'): strip = 'Stripping29r2p1'
    if(year=='2018'): strip = 'Stripping34r0p1'
    streams = buildStreams(stripping=strippingConfiguration(strip), archive=strippingArchive(strip))
    custom_stream = StrippingStream('CustomStream')
    custom_line = 'Stripping'+line
    for stream in streams:
       for sline in stream.lines:
           if sline.name() == custom_line:
               custom_stream.appendLines([sline])
                                                           
    ## Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()
    sc = StrippingConf(Streams=[custom_stream],MaxCandidates=2000,AcceptBadEvents=False, BadEventSelection=filterBadEvents)


triggerlines = [ "L0MuonDecision", \
                "L0DiMuonDecision", \
                "L0HadronDecision", \
                #hlt1
                "Hlt1TrackMuonDecision", \
                "Hlt1TrackAllL0Decision", \
                "Hlt1DiMuonLowMassDecision", \
                "Hlt1DiMuonHighMassDecision", \
                "Hlt1SingleMuonHighPTDecision", \
                "Hlt1TrackMVADecision",
                "Hlt1TwoTrackMVADecision",
                'Hlt1TrackMVALooseDecision', 'Hlt1TwoTrackMVALooseDecision',
                'Hlt1TrackMuonDecision', 'Hlt1TrackMuonMVADecision',
                #hlt2
                'Hlt2SingleMuonDecision', \
                'Hlt2SingleMuonHighPTDecision', \
                'Hlt2SingleMuonVHighPTDecision', \
                'Hlt2SingleMuonLowPTDecision', \
                'Hlt2DiMuonDecision', \
                'Hlt2DiMuonLowMassDecision', \
                'Hlt2DiMuonJPsiDecision', \
                'Hlt2DiMuonJPsiHighPTDecision', \
                'Hlt2DiMuonPsi2SDecision', \
                'Hlt2DiMuonPsi2SHighPTDecision', \
                'Hlt2DiMuonDetachedDecision', \
                'Hlt2DiMuonDetachedHeavyDecision', \
                'Hlt2DiMuonDetachedJPsiDecision', 
                'Hlt2DiMuonDetachedPsi2SDecision',
                #topo
                'Hlt2Topo2BodySimpleDecision', \
                'Hlt2Topo3BodySimpleDecision', \
                'Hlt2Topo4BodySimpleDecision', \
                'Hlt2Topo2BodyBBDTDecision', \
                'Hlt2Topo3BodyBBDTDecision', \
                'Hlt2Topo4BodyBBDTDecision', \
                'Hlt2TopoMu2BodyBBDTDecision', \
                'Hlt2TopoMu3BodyBBDTDecision', \
                'Hlt2TopoMu4BodyBBDTDecision', 
                'Hlt2Topo2BodyDecision',
                'Hlt2Topo3BodyDecision',
                'Hlt2Topo4BodyDecision',
                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",
                 "Hlt2TopoMuMu2BodyDecision",
                 "Hlt2TopoMuMu3BodyDecision",
                 "Hlt2TopoMuMu4BodyDecision",
                ] 


###############   Pre Filter, does not really do much except choose only candidates passing the Stripping line, maybe beneficial to performance
from Configurables import LoKi__HDRFilter as StripFilter
stripFilter = StripFilter( 'stripPassFilter', Code = "HLT_PASS('StrippingB2XMuMu_LineDecision')", Location= "/Event/Strip/Phys/DecReports")

############# DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolTISTOS
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import FilterDesktop
from Configurables import PrintDecayTree, PrintDecayTreeTool
from Configurables import SubstitutePID
from Configurables import TupleToolDecayTreeFitter, TupleToolTrackIsolation, TupleToolRecoStats, TupleToolKinematic, TupleToolGeometry, TupleToolVtxIsoln
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import FilterDesktop

from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import FilterDesktop

inputs = 'Phys/{0}/Particles'.format(line)
if(DaVinci().Simulation == True and (DaVinci().DDDBtag != 'dddb-20130929-1' and DaVinci().DDDBtag != 'dddb-20130929' ) ): inputs = '/Event/AllStreams/Phys/{0}/Particles'.format(line)

reqsel = AutomaticData(Location = inputs)
#MySel = FilterDesktop("MySel", Code = "(INTREE((ABSID=='B+')&(M>5000))) & (INTREE((ABSID=='B+')&(M<6000))) & (INTREE( (ID=='mu+') & (ISMUON) & (PIDmu-PIDpi>0) ) ) & (INTREE((ABSID=='J/psi(1S)')&(M>3000))) & (INTREE((ABSID=='B+')&(PT>2000))) & (INTREE((ABSID=='B+')&(BPVLTIME()>0.0003))) & (INTREE( (ID=='mu-') & (ISMUON) & (PIDmu-PIDpi>0) ) ) & (INTREE( (ABSID=='K+') & (PIDK-PIDpi>0) ) ) & (INTREE( (ID=='pi+') & (PIDK-PIDpi<20) ) ) & (INTREE( (ID=='pi-') & (PIDK-PIDpi<20) ) )")
MySel = FilterDesktop("MySel", Code = "(INTREE((ABSID=='B+') & (M>5000) & (M<6000) & (BPVDIRA > 0.9999) & (BPVIPCHI2() < 20) & (BPVVDCHI2 > 100) & (BPVLTIME()>0.0002) & (PT>1500) )) & (INTREE( (ID=='mu+') & (ISMUON) ) ) & (INTREE((ABSID=='J/psi(1S)')&(M>3000))) & (INTREE( (ID=='mu-') & (ISMUON) ) )")
MyFilterSel = Selection("MyFilterSel", Algorithm = MySel, RequiredSelections = [reqsel])

#B+ -> (K_1(1270)+ -> K+ pi+ pi-) (J/psi(1S) => ^mu+ ^mu-)
bu2kpipimumutuple = DecayTreeTuple("Bu2kpipimumuTuple")
bu2kpipimumutuple.Decay = "[B+ -> ^(K_1(1270)+ -> ^K+ ^pi+ ^pi-) ^(J/psi(1S) -> ^mu+ ^mu-)]CC"
bu2kpipimumutuple.ReFitPVs = True

bu2kpipimumutuple.ToolList +=  ["TupleToolGeometry", "TupleToolKinematic", "TupleToolRecoStats", "TupleToolMuonPid", "TupleToolTrackIsolation"]

if (data==False):
    bu2kpipimumutuple.ToolList +=  ["TupleToolMCTruth", "TupleToolMCBackgroundInfo","TupleToolPhotonInfo"]
    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  [ "MCTupleToolHierarchy" , "MCTupleToolKinematic", "MCTupleToolReconstructed" ]
    bu2kpipimumutuple.addTool(MCTruth) 


bu2kpipimumutuple.addTool(TupleToolTrackIsolation, name="TupleToolTrackIsolation")
bu2kpipimumutuple.TupleToolTrackIsolation.FillAsymmetry = True
bu2kpipimumutuple.TupleToolTrackIsolation.FillDeltaAngles = False
bu2kpipimumutuple.TupleToolTrackIsolation.MinConeAngle = 1.0

bu2kpipimumutuple.addTool( TupleToolKinematic,name = "TupleToolKinematic" )
bu2kpipimumutuple.TupleToolKinematic.Verbose = False
    
bu2kpipimumutuple.addTool( TupleToolGeometry, name = "TupleToolGeometry" )
bu2kpipimumutuple.TupleToolGeometry.Verbose = True
bu2kpipimumutuple.TupleToolGeometry.RefitPVs = True

bu2kpipimumutuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
bu2kpipimumutuple.TupleToolRecoStats.Verbose = True
bu2kpipimumutuple.UseLabXSyntax = True                          
bu2kpipimumutuple.RevertToPositiveID = False

bu2kpipimumutuple.Branches= {
"Bplus" : "^([B+ -> (K_1(1270)+ -> K+ pi+ pi-) (J/psi(1S) -> mu+ mu-)]CC)" ,
"K_1_1270_plus" : "[B+ -> ^(K_1(1270)+ -> K+ pi+ pi-) (J/psi(1S) -> mu+ mu-)]CC",
"Kplus" : "[B+ -> (K_1(1270)+ -> ^K+ pi+ pi-) (J/psi(1S) -> mu+ mu-)]CC",
"piplus" : "[B+ -> (K_1(1270)+ -> K+ ^pi+ pi-) (J/psi(1S) -> mu+ mu-)]CC",
"piminus" : "[B+ -> (K_1(1270)+ -> K+ pi+ ^pi-) (J/psi(1S) -> mu+ mu-)]CC",
"J_psi_1S" : "[B+ -> (K_1(1270)+ -> K+ pi+ pi-) ^(J/psi(1S) -> mu+ mu-)]CC",
"muplus" : "[B+ -> (K_1(1270)+ -> K+ pi+ pi-) (J/psi(1S) -> ^mu+ mu-)]CC",
"muminus" : "[B+ -> (K_1(1270)+ -> K+ pi+ pi-) (J/psi(1S) -> mu+ ^mu-)]CC"
}

bu2kpipimumutuple.addTool(TupleToolDecay, name="Bplus")
bu2kpipimumutuple.addTool(TupleToolDecay, name="J_psi_1S")

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("DTF"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/DTF" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.DTF.constrainToOriginVertex = True
bu2kpipimumutuple.Bplus.DTF.daughtersToConstrain = ["B+" , "J/psi(1S)" ]  
bu2kpipimumutuple.Bplus.DTF.UpdateDaughters = True
bu2kpipimumutuple.Bplus.DTF.Verbose = True
#bu2kpipimumutuple.B.DTF.OutputLevel = 1

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("PV"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/PV" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.PV.constrainToOriginVertex = True
#bu2kpipimumutuple.Bplus.PV.UpdateDaughters = True
bu2kpipimumutuple.Bplus.PV.Verbose = True
#bu2kpipimumutuple.B.DTF.OutputLevel = 1

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("JpsiFit"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/JpsiFit" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.JpsiFit.constrainToOriginVertex = True
bu2kpipimumutuple.Bplus.JpsiFit.daughtersToConstrain = ["J/psi(1S)" ]  
#bu2kpipimumutuple.Bplus.JpsiFit.UpdateDaughters = True
bu2kpipimumutuple.Bplus.JpsiFit.Verbose = True

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("BFit"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/BFit" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.BFit.constrainToOriginVertex = True
bu2kpipimumutuple.Bplus.BFit.daughtersToConstrain = ["B+"]  
#bu2kpipimumutuple.Bplus.BFit.UpdateDaughters = True
bu2kpipimumutuple.Bplus.BFit.Verbose = True

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("psiFit"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/psiFit" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.psiFit.constrainToOriginVertex = True
bu2kpipimumutuple.Bplus.psiFit.Substitutions = { 'Beauty -> Hadron ^Charm' : 'psi(2S)' }
bu2kpipimumutuple.Bplus.psiFit.daughtersToConstrain = ["psi(2S)" ]  
#bu2kpipimumutuple.Bplus.psiFit.UpdateDaughters = True
bu2kpipimumutuple.Bplus.psiFit.Verbose = True

bu2kpipimumutuple.Bplus.addTool(TupleToolDecayTreeFitter("psiDTF"))
bu2kpipimumutuple.Bplus.ToolList +=  ["TupleToolDecayTreeFitter/psiDTF" ]         # fit with all constraints I can think of
bu2kpipimumutuple.Bplus.psiDTF.constrainToOriginVertex = True
bu2kpipimumutuple.Bplus.psiDTF.Substitutions = { 'Beauty -> Hadron ^Charm' : 'psi(2S)' }
bu2kpipimumutuple.Bplus.psiDTF.daughtersToConstrain = ["B+", "psi(2S)" ]  
bu2kpipimumutuple.Bplus.psiDTF.UpdateDaughters = True
bu2kpipimumutuple.Bplus.psiDTF.Verbose = True


LoKiToolB = bu2kpipimumutuple.Bplus.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolB")
LoKiToolB.Variables = {"DOCA" : "DOCA(1,2)", "TAU" : "BPVLTIME()", "TAUERR" : "BPVLTERR()" };

##Trigger
#tttt = TupleToolTISTOS()
#bu2kpipimumutuple.addTool(tttt)

tos=TupleToolTISTOS("tos")
bu2kpipimumutuple.Bplus.ToolList += ["TupleToolTISTOS/tos"]
bu2kpipimumutuple.Bplus.addTool(tos)
bu2kpipimumutuple.Bplus.tos.VerboseL0=True
bu2kpipimumutuple.Bplus.tos.VerboseHlt1=True
bu2kpipimumutuple.Bplus.tos.VerboseHlt2=True
bu2kpipimumutuple.Bplus.tos.TriggerList = triggerlines

tosPsi=TupleToolTISTOS("tosPsi")
bu2kpipimumutuple.J_psi_1S.ToolList += ["TupleToolTISTOS/tosPsi"]
bu2kpipimumutuple.J_psi_1S.addTool(tosPsi)
bu2kpipimumutuple.J_psi_1S.tosPsi.VerboseL0=True
bu2kpipimumutuple.J_psi_1S.tosPsi.VerboseHlt1=True
bu2kpipimumutuple.J_psi_1S.tosPsi.VerboseHlt2=True
bu2kpipimumutuple.J_psi_1S.tosPsi.TriggerList = triggerlines


makeb2dkpipiseq = SelectionSequence("makeb2dkpipiseq", TopSelection = MyFilterSel)
bu2kpipimumutuple.Inputs = [makeb2dkpipiseq.outputLocation()]
bu2kpipimumuseq = GaudiSequencer("Bu2kpipimumuSeq")
bu2kpipimumuseq.Members += [makeb2dkpipiseq.sequence(), bu2kpipimumutuple]


from Configurables import CheckPV
checkPVs = CheckPV("checkPVs")
checkPVs.MinPVs = 1
checkPVs.MaxPVs = -1

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks"""
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

if(data):
    DaVinci().InputType = 'MDST'
    DaVinci().RootInTES = '/Event/{0}'.format(stream) #to be used for micro-DSTs
else:
    DaVinci().InputType = 'DST'

if(DaVinci().DDDBtag == 'dddb-20130929-1' or DaVinci().DDDBtag == 'dddb-20130929' ):   DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
else: DaVinci().EventPreFilters = [stripFilter]

DaVinci().UserAlgorithms += [MomentumCorrection(DaVinci().Simulation)]
if(data): DaVinci().UserAlgorithms += [eventNodeKiller]
DaVinci().UserAlgorithms += [bu2kpipimumuseq ]

DaVinci().EvtMax = -1 #10000 #100000 #100000
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 100000
DaVinci().TupleFile = "b2psiKpipi.root"
DaVinci().Lumi = not DaVinci().Simulation

## Use the local input data
#from GaudiConf import IOHelper
#IOHelper().inputFiles([
#                       '/afs/cern.ch/work/p/phdargen/makeTuple/00071501_00000030_1.leptonic.mdst'
#                       ], clear=True)
