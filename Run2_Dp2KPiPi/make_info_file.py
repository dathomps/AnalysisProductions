#!/usr/bin/env python
import argparse
from ruamel.yaml import YAML
from os.path import dirname, join

# Settings
all_years = [2016, 2017, 2018]
all_polarities = ['MagDown', 'MagUp']
channels = ['DpToKPiPi']
davinci_versions = {
    2016: 'v44r7',
    2017: 'v44r7',
    2018: 'v44r7',
}
bk_paths = {
    'Semileptonic': {
        2016: '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco16/Stripping28r2/90000000/SEMILEPTONIC.DST',
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco17/Stripping29r2/90000000/SEMILEPTONIC.DST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco18/Stripping34r0p1/90000000/SEMILEPTONIC.DST' ,
    },
}

# YAML
yaml = YAML()
yaml.indent(sequence = 4, offset=4)


# Parse
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
    parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
    parser.add_argument('--tag-types', nargs='+', choices=list(bk_paths.keys()), default=list(bk_paths.keys()))
    parser.add_argument('--channels', nargs='+', choices=channels, default=channels)
    parser.add_argument('--data-types', type=str, choices=['RealData', 'MC'], default='RealData')
    args = parser.parse_args()

    # Naming
    results = {
            "defaults": { 
                          "wg": "Charm",
                          "inform": ["surapat.ek-in@cern.ch"],
                          "automatically_configure": "yes",
                          "turbo" : "no",  
                         }
            }

    if args.data_types == 'RealData':
        ChainOption = [     
                        'DataTypes/real_data.py',
                        ]
    elif args.data_types == 'MC':  
        ChainOption = [     
                        'DataTypes/mc.py',
                        ]
    else:
        raise NotImplementedError(args.data_types)

    for tag_type in args.tag_types:
        for channel in args.channels:
            for year in args.years:
                for polarity in args.polarities:
                    key = '_'.join([str(year), polarity, tag_type, channel])
                    assert key not in results, 'Duplicate keys are not possible'

                    # Format  
                    results[key] = {
                        'application': 'DaVinci/{}'.format(davinci_versions[year]),
                        'options': ChainOption + ['DataTypes/'+str(year)+'.py',
                                                 'TupleFiles/'+tag_type+'_'+channel+'.py']
                                               + (['DataTypes/{}.py'.format(polarity)] if args.data_types == 'MC' else []) 
                                               + [ 'main_options.py'],
                        'input': {'bk_query': "{}".format(bk_paths[tag_type][year].format(polarity=polarity))},
                        'output': '_'.join(['CHARM', tag_type, channel]).upper()+'.ROOT', 
                    }

    with open(join(dirname(__file__), 'info.yaml'), 'wt') as fp:
        yaml.dump(results, fp)


if __name__ == '__main__':
    parse_args()
