"""
    This follows what describes on 
        http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping28r2/semileptonic/strippingb2dmunux_dp.html
        decay = [ '[B0 -> D- mu+]cc' , '[B0 -> D- mu-]cc' ]
        decay = [ '[D+ -> K- pi+ pi+]cc' ]

"""
from PhysConf.Selections import (
    AutomaticData,
    CombineSelection,
    MomentumScaling,
    RebuildSelection,
    SelectionSequence,
    TupleSelection,
)
from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci, TupleToolTrigger
from GaudiKernel.SystemOfUnits import MeV
from StandardParticles import StdAllLooseMuons
from Configurables import CondDB
from Configurables import DstConf, TurboConf  # necessary for DaVinci v40r1 onwards
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import MessageSvc

from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, MCTupleToolHierarchy 

triggerList = [
    'L0DiMuonDecision',
    'L0ElectronDecision',
    'L0HadronDecision',
    'L0MuonDecision',
    'L0PhotonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMVATightDecision',
    'Hlt1TwoTrackMVATightDecision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackMuonMVADecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2CharmHadInclDst2PiD02HHXBDTDecision'
]

# Prefix "Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_",
triggerHlt2Head = [
    "_KS0LLTurboDecision",
    "_KS0DDTurboDecision",
    "_KS0LL_LTUNBTurboDecision",
    "_KS0DD_LTUNBTurboDecision",
]

default_variables = {
    'ID':     'ID',
    'P':      'P',
    'PX':     'PX',
    'PY':     'PY',
    'PZ':     'PZ',
    'E':      'E',
    'PT':     'PT',
    'ETA':    'ETA',
    'PHI':    'PHI',
    'IPCHI2': 'BPVIPCHI2()',
    'IP':     'BPVIP()',
}

track_variables = {
    'TRCHI2DOF':   'TRCHI2DOF',
    'GHOSTPROB':   'TRGHOSTPROB',
    'CLONEDIST':   'CLONEDIST',
    'ProbNNmu':    'PROBNNmu',
    'ProbNNk':     'PROBNNk',
    'ProbNNpi':    'PROBNNpi',
    'ProbNNghost': 'PROBNNghost',
    'ProbNNp':     'PROBNNp',
    'PIDpi':       'PIDpi',
    'PIDe':        'PIDe',
    'PIDp':        'PIDp',
    'PIDK':        'PIDK',
    'PIDmu':       'PIDmu',
    'IsMuon':      'switch(ISMUON,1,0)',
}

composite_variables = {
    'M':         'M',
    'VX':        'VFASPF(VX)',
    'VY':        'VFASPF(VY)',
    'VZ':        'VFASPF(VZ)',
    'BPVDIRA':   'BPVDIRA',
    'BPVVDCHI2': 'BPVVDCHI2',
    'BPVX':      'BPV(VX)',
    'BPVY':      'BPV(VY)',
    'BPVZ':      'BPV(VZ)',
    'VCHI2NDOF': 'CHI2VXNDOF',
}

# Only SingleTag 
DTF_variables_B = {
    # B
    "DTF_M":          "M", 
    "DTF_BPVCORRM":   "BPVCORRM", 
    "DTF_VCHI2NDOF":  "CHI2VXNDOF",
    "DTF_ID":         "ID",
    "DTF_PX":         "PX",
    "DTF_PY":         "PY",
    "DTF_PZ":         "PZ",
    "DTF_PT":         "PT",
    "DTF_E":          "E",
    "DTF_ETA":        "ETA",
    "DTF_PHI":        "PHI",

    # Dp
    "DTF_Dp_M"          : "CHILD(M,   1)",
    "DTF_Dp_PX"         : "CHILD(PX,  1)",
    "DTF_Dp_PY"         : "CHILD(PY,  1)",
    "DTF_Dp_PZ"         : "CHILD(PZ,  1)",
    "DTF_Dp_PT"         : "CHILD(PT,  1)",
    "DTF_Dp_E"          : "CHILD(E,   1)",
    "DTF_Dp_ETA"        : "CHILD(ETA, 1)",
    "DTF_Dp_PHI"        : "CHILD(PHI, 1)",

    # Mu
    "DTF_Mu_ID"         : "CHILD(ID , 2)",
    "DTF_Mu_PX"         : "CHILD(PX , 2)",
    "DTF_Mu_PY"         : "CHILD(PY , 2)",
    "DTF_Mu_PZ"         : "CHILD(PZ , 2)",
    "DTF_Mu_PT"         : "CHILD(PT , 2)",
    "DTF_Mu_E"          : "CHILD(E  , 2)",
    "DTF_Mu_ETA"        : "CHILD(ETA, 2)",
    "DTF_Mu_PHI"        : "CHILD(PHI, 2)",

    # K
    "DTF_Dp_K_M"       : "CHILD(CHILD(M,    1), 1)",
    "DTF_Dp_K_ID"      : "CHILD(CHILD(ID,   1), 1)",
    "DTF_Dp_K_PX"      : "CHILD(CHILD(PX,   1), 1)",
    "DTF_Dp_K_PY"      : "CHILD(CHILD(PY,   1), 1)",
    "DTF_Dp_K_PZ"      : "CHILD(CHILD(PZ,   1), 1)",
    "DTF_Dp_K_PT"      : "CHILD(CHILD(PT,   1), 1)",
    "DTF_Dp_K_E"       : "CHILD(CHILD(E ,   1), 1)",
    "DTF_Dp_K_ETA"     : "CHILD(CHILD(ETA , 1), 1)",
    "DTF_Dp_K_PHI"     : "CHILD(CHILD(PHI , 1), 1)",

    # H1
    "DTF_Dp_h1_ID"      : "CHILD(CHILD(ID,  2), 1)",
    "DTF_Dp_h1_PX"      : "CHILD(CHILD(PX,  2), 1)",
    "DTF_Dp_h1_PY"      : "CHILD(CHILD(PY,  2), 1)",
    "DTF_Dp_h1_PZ"      : "CHILD(CHILD(PZ,  2), 1)",
    "DTF_Dp_h1_PT"      : "CHILD(CHILD(PT,  2), 1)",
    "DTF_Dp_h1_E"       : "CHILD(CHILD(E ,  2), 1)",
    "DTF_Dp_h1_ETA"     : "CHILD(CHILD(ETA ,2), 1)",
    "DTF_Dp_h1_PHI"     : "CHILD(CHILD(PHI ,2), 1)",

    # H2
    "DTF_Dp_h2_ID"      : "CHILD(CHILD(ID,  3), 1)",
    "DTF_Dp_h2_PX"      : "CHILD(CHILD(PX,  3), 1)",
    "DTF_Dp_h2_PY"      : "CHILD(CHILD(PY,  3), 1)",
    "DTF_Dp_h2_PZ"      : "CHILD(CHILD(PZ,  3), 1)",
    "DTF_Dp_h2_PT"      : "CHILD(CHILD(PT,  3), 1)",
    "DTF_Dp_h2_E"       : "CHILD(CHILD(E ,  3), 1)",
    "DTF_Dp_h2_ETA"     : "CHILD(CHILD(ETA ,3), 1)",
    "DTF_Dp_h2_PHI"     : "CHILD(CHILD(PHI ,3), 1)",

    # Others
    "DTF_Dp_K_H1_M"    : "CHILD(MASS(1,2), 1)",
    "DTF_Dp_K_H2_M"    : "CHILD(MASS(1,3), 1)",
    "DTF_Dp_H1_H2_M"   : "CHILD(MASS(2,3), 1)",
}

decay_descriptors = {
    # Semileptonic Dp OS tagged
    'Semileptonic_DpToKPiPi':   '${B}[Beauty -> ${Dp}(D+ -> ${Dp_K}(K-) ${h1}pi+ ${h2}pi+ ) ${mu}mu-]CC',

    # Semileptonic Dp SS tagged
    'SemileptonicWS_DpToKPiPi': '${B}[Beauty -> ${Dp}(D+ -> ${Dp_K}(K-) ${h1}pi+ ${h2}pi+ ) ${mu}mu+]CC',
}

input_containers = {
    # Secondary tagged
    'Semileptonic_DpToKPiPi': '/Event/Semileptonic/Phys/B2DMuNuX_Dp/Particles',
}


def parseDescriptorTemplate(template):
    from string import Template
    # The argument 'template' is a Python string template
    # e.g. "${D}[D0 -> ${kaon}K- ${pion}pi+]CC"
    # Here ["D", "kaon", "pion"] are the branch names you want
    dd = Template(template)

    # This parses the temlate to get the list of branch names,
    # i.e. ["D", "kaon", "pion"]
    particles = [y[1] if len(y[1]) else y[2] for y in dd.pattern.findall(dd.template) if len(y[1]) or len(y[2])]

    # To form the decay descriptor, we need to mark all the particles
    # except for the top-level particle, which is included by default
    mapping = {p: '^' for p in particles}
    mapping[particles[0]] = ''

    # Make the descriptor
    # "[D0 -> ^K- ^pi+]CC"
    decay = dd.substitute(mapping)

    # Now make the branches
    branches = {}
    for p in particles:
        # Need a version of the descriptor where particle 'p' is marked but nothing else is.
        mapping = {q: '^' if p == q else '' for q in particles}
        branches[p] = dd.substitute(mapping)

    # Finally, add the branches to the DecayTreeTuple
    return decay, branches

def make_tuple(name, input_data, line = 'Turbo'):
    # Set up the momentum scaling
    if not DaVinci().Simulation:
        input_data = MomentumScaling(input_data, Turbo=DaVinci().Turbo, Year=DaVinci().DataType)

    # Create the DecayTreeTuple
    decay, branches = parseDescriptorTemplate(decay_descriptors[name])
    print("checking {} {}".format(decay, branches))
    dtt = TupleSelection(name, [input_data], Decay=decay, Branches=branches, ToolList=[
        'TupleToolEventInfo',
        'TupleToolRecoStats',
        'TupleToolL0Data',
        'TupleToolGeometry',
        'TupleToolPrimaries',
    ])
    constrain_vertex = not hasattr(dtt, 'B')
        
    # Add trigger information
    # Add Hlt2 Charm lines only to head on all sample
    trigTool = dtt.addTupleTool("TupleToolTrigger", name="TupleToolTrigger")
    Hlt2_prefix = "Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_" 
    trigTool.VerboseHlt2 = True
    trigTool.TriggerList = [Hlt2_prefix + line_suf for line_suf in triggerHlt2Head] 

    # TISTOS
    tistosTool = dtt.addTupleTool("TupleToolTISTOS", name="TupleToolTISTOS")
    tistosTool.VerboseL0 = True
    tistosTool.VerboseHlt1 = True
    tistosTool.VerboseHlt2 = True
    tistosTool.TriggerList = triggerList
    tistosTool.TUS = True
    tistosTool.TPS = True

    # Add LoKi variables
    dtt.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Variables').Variables = default_variables

    # Add DTF corrected mass for secondary sample  
    # DTF below does not have these varibles - consider adding separately 
    # Add ReFit variables
    refit = dtt.B.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    refit.Verbose= True
    refit.constrainToOriginVertex = False
    refit.UpdateDaughters = True

    # Add vars because different cuts applied on Turbo prompt - see at the beginning of this file 
    dtt.Dp.addTupleTool('LoKi::Hybrid::TupleTool/Dp').Variables = {
        'BPVLTIME':      'BPVLTIME()',
    }

    dtt.Dp.addTupleTool('TupleToolPropertime') # Add Dp decay time.

    # Variable for composite track
    for particle in ['B', 'Dp']:
        getattr(dtt, particle).addTupleTool('LoKi::Hybrid::TupleTool/CompositeParticle').Variables = composite_variables

    # Variable for long-lived track 
    trackinfo = []
    for particle in ['Dp_K', 'h1', 'h2', 'mu']:
        getattr(dtt, particle).addTupleTool('LoKi::Hybrid::TupleTool/ChargedParticle').Variables = track_variables
        trackinfo.append(getattr(dtt, particle).addTupleTool('TupleToolTrackInfo', name = 'TupleToolTrackInfo_{}'.format(particle)))
        trackinfo[-1].Verbose = True

    # Add decay tree fitter variables if we have a D*+ -> D0 pi decay
    if line == 'Stripping':
        dtf_particle = dtt.B
        dtf_var = DTF_variables_B
    else:
        print "TESTTTT "+line
        print dtf_particle

    # Add D0 CTAU manually - cannot add CTAU to Loki::Hybrid
    dtf_particle.addTupleTool('LoKi::Hybrid::TupleTool/LoKiDTF').Variables = {
        'DTF_Dp_CTAU'    : "DTF_CTAU('D+'==ABSID, "+str(constrain_vertex)+")",
        'DTFDp_Dp_CTAU'  : "DTF_CTAU('D+'==ABSID, "+str(constrain_vertex)+", strings(['D+']))",
    }

    add_dtf(dtf_particle, '', constrain_vertex, [], dtf_var)
    add_dtf(dtf_particle, 'Dp', constrain_vertex, ['D+'], dtf_var)


    # Add MC TupleTool 
    if DaVinci().Simulation:
        # Add Bkg info
        dtt.addTool(TupleToolMCBackgroundInfo, name="TupleToolMCBackgroundInfo")
        dtt.TupleToolMCBackgroundInfo.Verbose=True

        # Add Truth matching
        dtt.addTool(TupleToolMCTruth, name="truth")
        dtt.truth.ToolList+=["MCTupleToolHierarchy"]
        dtt.ToolList+=["TupleToolMCTruth/truth"]

    print(dtt.name())
    DaVinci().UserAlgorithms.append(
        SelectionSequence("Seq"+dtt.name(), dtt).sequence()
    )

    return dtt


def add_dtf(particle, suffix, constrain_vertex, constrain_masses, DTF_variables):
    dtf = particle.addTupleTool(LoKi__Hybrid__Dict2Tuple, 'DTFTuple'+suffix)
    dtf.addTool(DTFDict, 'DTF')
    dtf.Source = 'LoKi::Hybrid::DTFDict/DTF'
    dtf.NumVar = len(DTF_variables) 
    dtf.DTF.constrainToOriginVertex = constrain_vertex
    dtf.DTF.daughtersToConstrain = constrain_masses
    dtf.DTF.addTool(LoKi__Hybrid__DictOfFunctors, 'dict')
    dtf.DTF.Source = 'LoKi::Hybrid::DictOfFunctors/dict'
    dtf.DTF.dict.Variables = {k.replace('DTF', 'DTF'+suffix): v for k, v in DTF_variables.items()}


# Use the TupleFile to figure out which kind of data we are processing
assert DaVinci().TupleFile.lower().endswith('.root'), 'TupleFile must end with .root'
name = DaVinci().TupleFile[:-len('.root')]

# Configure generic DaVinci options
DaVinci().InputType = 'DST'
DaVinci().Lumi = not DaVinci().Simulation
MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'
if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)

# Configure channel specific DaVinci options
DaVinci().Turbo = False
DaVinci().EventPreFilters = [LoKi_Filters(
    STRIP_Code="HLT_PASS_RE('Stripping"+input_containers[name].split('/')[4]+"Decision')"
).sequencer('PreFilter')]

# Add the DecayTreeTuples
tuple_name = name

# Make tuple depending on name given in the info
# Ntuple should contain (3 = Main + DoubleTag + DoubleWS) * (2 KsType)
if name.startswith('Semileptonic'):

    # RS Semileptonic sample
    input_data_SL_DpRS = AutomaticData(Location = input_containers[name])
    make_tuple('Semileptonic'+tuple_name[len('Semileptonic'):], input_data_SL_DpRS, "Stripping")

    # DoubleTagWS - use the same input_data_WS as DoubleTag
    input_data_SL_DpWS = AutomaticData(Location = input_containers[name])
    input_data_SL_DpWS._name = input_data_SL_DpWS._name+"WS"
    make_tuple('SemileptonicWS'+tuple_name[len('Semileptonic'):], input_data_SL_DpWS, "Stripping")

else:
    raise NameError("The name given here is invalid: name: {}".format(name))
