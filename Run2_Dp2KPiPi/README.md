# WG productions options for D0 → Ks H H

The options contained in this directory generate ntuples for:

1. Semileptonic Dp

* `B0 -> (D+ -> (K- H1+ H2+)) mu-`
* `B0 -> (D+ -> (K- H1+ H2+)) mu+`

Where `H1` and `H2` are  pions.

## Generating the `info.yaml` file

As there are a lot of sub-productions in this WG production, the `info.yaml` file is generated using a script.
Examples of valid arguments:

```bash
./make_info_file.py --help
./make_info_file.py
./make_info_file.py --years 2016 2017
./make_info_file.py --years 2016 2017 --channels DpToKPiPi 
./make_info_file.py --years 2016 2017 --polarities MagDown --tag-types Semileptonic 
./make_info_file.py --tag-types Semileptonic --data-types RealData
./make_info_file.py --tag-types Semileptonic --data-types RealData --channels DpToKPiPi 
```
