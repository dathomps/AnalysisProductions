from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements, stripMicroDSTStreamConf, stripMicroDSTElements

def write_dst(selection_sequences, enable_packing = True, selective_raw_event = False, microDST = False, is_mc = None, extra_items = None):
    # Configuration of SelDSTWriter
    if microDST:
        elements = {"default": stripMicroDSTElements(pack = enable_packing, isMC = is_mc)}
        config = {"default": stripMicroDSTStreamConf(pack = enable_packing, isMC = is_mc)}
    else:
        elements = {"default": stripDSTElements(pack = enable_packing)}
        config = {"default": stripDSTStreamConf(pack = enable_packing, selectiveRawEvent = selective_raw_event)}
    if extra_items:
        config["default"].extraItems += extra_items
    dst_writer = SelDSTWriter("MyDSTWriter", StreamConf = config, MicroDSTElements = elements, OutputFileSuffix = "0"*6,  SelectionSequences = selection_sequences)
    return dst_writer

