from Configurables import MCDecayTreeTuple
from DecayTreeTuple.Configuration import *
import re

def make_mcdtt(name, decay):
	mctuple = MCDecayTreeTuple(name+"MCTuple", Decay = re.sub(r"\${.*?}","",decay))
	mctuple.setDescriptorTemplate(decay)
	mctuple.ToolList = [
		"MCTupleToolHierarchy",
		"MCTupleToolKinematic",
		"TupleToolEventInfo",
		"MCTupleToolRedecay",
	]
	return [mctuple]
