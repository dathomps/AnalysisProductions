from Configurables import DaVinci, DecayTreeTuple
from PhysSelPython.Wrappers import AutomaticData, FilterSelection, SelectionSequence, TupleSelection
from DecayTreeTuple.Configuration import *
from copy import copy
import re

MC = DaVinci().Simulation
year = int(DaVinci().DataType)

stables = ["pion", "kaon", "proton", "muplus", "muminus", "gamma"]

Tools = [
	# Particle-level info
	"TupleToolKinematic",
	#"TupleToolGeometry", # Added later to all particles except photon
	"TupleToolANNPID",
	"TupleToolPid", # Redundant? A lot of unnecessary Used/Thres branches
	#"TupleToolPropertime", # Added later to head
	"TupleToolTrackInfo",
	# Event-level info
	"TupleToolEventInfo",
	"TupleToolRecoStats",
	#"TupleToolPrimaries", # Adds a bunch of useless array branches
]
if MC:
	Tools += [
		"TupleToolMCBackgroundInfo",
		"MCTupleToolRedecay",
	]

L0Lines = [
	"L0PhysicsDecision",
	"L0MuonDecision",
	"L0DiMuonDecision",
	"L0MuonHighDecision",
	"L0HadronDecision",
	"L0PhotonDecision",
]
Hlt1Lines = [
	"Hlt1DiMuonHighMassDecision",
	"Hlt1DiMuonLowMassDecision",
	"Hlt1SingleMuonNoIPDecision",
	"Hlt1SingleMuonHighPTDecision",
	"Hlt1TrackAllL0Decision",
	"Hlt1TrackMuonDecision",
	"Hlt1TrackPhotonDecision",
	"Hlt1L0AnyDecision",
]
Hlt2Lines = [
	"Hlt2DiMuonDetachedDecision",
	"Hlt2DiMuonDetachedJPsiDecision",
	"Hlt2DiMuonDetachedHeavyDecision",
	"Hlt2DiMuonJPsiDecision",
	"Hlt2DiMuonJPsiHighPTDecision",
	"Hlt2DiMuonPsi2SDecision",
	"Hlt2DiMuonPsi2SHighPTDecision",
	"Hlt2SingleMuonHighPTDecision",
]

if year < 2013:
	Hlt2Lines += [
		"Hlt2Topo2BodyBBDTDecision",
		"Hlt2Topo3BodyBBDTDecision",
		"Hlt2Topo4BodyBBDTDecision",
		"Hlt2TopoMu2BodyBBDTDecision",
		"Hlt2TopoMu3BodyBBDTDecision",
		"Hlt2TopoMu4BodyBBDTDecision",
	]
elif year < 2019:
	L0Lines += [
		"L0MuonNoSPDDecision",
		"L0MuonEWDecision",
	]
	Hlt1Lines += [
		"Hlt1TrackMVADecision",
		"Hlt1TwoTrackMVADecision",
		"Hlt1TrackMVATightDecision",
		"Hlt1TwoTrackMVATightDecision",
		"Hlt1TrackMuonMVADecision",
		"Hlt1DiMuonNoL0Decision",
		"Hlt1DiMuonNoIPDecision",
		"Hlt1SingleMuonHighPTNoMUIDDecision",
	]
	Hlt2Lines += [
		"Hlt2DiMuonDetachedPsi2SDecision",
		"Hlt2Topo2BodyDecision",
		"Hlt2Topo3BodyDecision",
		"Hlt2Topo4BodyDecision",
		"Hlt2TopoMu2BodyDecision",
		"Hlt2TopoMu3BodyDecision",
		"Hlt2TopoMu4BodyDecision",
		"Hlt2TopoMuMu2BodyDecision",
		"Hlt2TopoMuMu3BodyDecision",
		"Hlt2TopoMuMu4BodyDecision",
	]

def add_dtf(dtt, name = "DTF", head = "Lambda_b0", toConstrain = ["J/psi(1S)"], PV = False, tracks = False):
	"""
	Using TupleToolDecayTreeFitter
	pros: not a lot of code in the options file
	cons: horrible verbose branch names
	"""
	dtf = getattr(dtt, head).addTupleTool("TupleToolDecayTreeFitter/"+name)
	dtf.UpdateDaughters = tracks
	dtf.constrainToOriginVertex = PV
	dtf.daughtersToConstrain = toConstrain
	for charmonium in ["chi_c0(1P)", "chi_c2(1P)"]:
		if charmonium in toConstrain:
			dtf.Substitutions = {"Beauty -> ^chi_c1(1P) {Baryon} Meson {Meson}": charmonium}

def strip_branch_names(decay):
	return re.sub(r"\${.*?}", "", decay)

def get_head(decay):
	return re.match(r"\${(.*?)}", decay).group(1)

def make_selectors(decay, include_head = False):
	selectors = {}
	for i,match in enumerate(re.finditer(r"\${(.*?)}", decay)):
		particle = match.group(1)
		if i == 0:
			if include_head:
				selectors[particle] = strip_branch_names(decay)
			else:
				continue
		else:
			selectors[particle] = strip_branch_names(decay.replace(match.group(0), "^"))
	return selectors

def make_dtf_vars(selectors):
	loki_vars = {
		"M": "M",
		"PE": "E", # the one reason this is a dict not a list :(
		"PX": "PX",
		"PY": "PY",
		"PZ": "PZ",
	}
	dtf_vars = {
		branch: loki_vars[branch]
		for branch in loki_vars
	}
	for particle in selectors:
		dtf_vars.update({
			"_".join([particle, branch]): "CHILD({}, '{}')".format(loki_vars[branch], selectors[particle])
			for branch in loki_vars
		})
	print(dtf_vars)
	return dtf_vars

def add_dtf_loki(dtt, decay, name = "DTF", toConstrain = ["J/psi(1S)"], PV = False, tracks = False):
	"""
	Using Dict2Tuple
	pros: more control over branches
	cons: lots more code in the options file
	"""
	from Configurables import LoKi__Hybrid__DTFDict, LoKi__Hybrid__DictOfFunctors
	head = get_head(decay)
	dict_to_tuple = getattr(dtt, head).addTupleTool("LoKi::Hybrid::Dict2Tuple/{}Dict2Tuple".format(name))
	dtf = dict_to_tuple.addTool(LoKi__Hybrid__DTFDict, name) # This name is prepended to the branch names
	dtf.constrainToOriginVertex = PV
	dtf.daughtersToConstrain = toConstrain
	selectors = make_selectors(decay, False) if tracks else {}
	for charmonium in ["chi_c0(1P)", "chi_c2(1P)"]:
		if charmonium in toConstrain:
			dtf.Substitutions = {"Beauty -> ^chi_c1(1P) {Baryon} Meson {Meson}": charmonium}
			for particle in selectors:
				selectors[particle] = selectors[particle].replace("chi_c1(1P)", charmonium)
	functor_dict = dtf.addTool(LoKi__Hybrid__DictOfFunctors, "{}Functors".format(name))
	functor_dict.Variables = make_dtf_vars(selectors)
	# Link everything together
	dtf.Source = functor_dict.getFullName()
	dict_to_tuple.Source = dtf.getFullName()
	dict_to_tuple.NumVar = len(functor_dict.Variables)

def make_dtt(name, location, decay, cuts = None):
	head = get_head(decay)
	# Assume the head's branch name is the DaVinci name where + and - are replaced
	head_full = head.replace("plus", "+").replace("minus", "-")
	algorithms = [] # The list that will be returned
	# Add any extra cuts to a FilterSelection
	if cuts is not None:
		preselection = FilterSelection(name+"Preselection", [AutomaticData(location)], Code=cuts)
	else:
		preselection = AutomaticData(location)
	dtt = TupleSelection(name+"Tuple", [preselection], Decay = strip_branch_names(decay), ToolList = copy(Tools))
	dtt.algorithm().setDescriptorTemplate(decay)
	# put the sequence in the algorithms
	algorithms += [SelectionSequence(name+"Sequence", dtt).sequence()]
	# TISTOS tool
	tistostool = getattr(dtt, head).addTupleTool("TupleToolTISTOS/TupleToolTISTOSParent")
	tistostool.TriggerList = L0Lines + Hlt1Lines + Hlt2Lines
	tistostool.Verbose = True
	# MC truth
	if MC:
			MCTruth = dtt.addTupleTool("TupleToolMCTruth/MyMCTruthTool")
			MCTruth.ToolList += [
				"MCTupleToolKinematic",
				"MCTupleToolHierarchy",
			]
	# Configure DecayTreeFitter
	if "chi_c" in decay:
		combinations = {
			"DTFB1": [ head_full, "chi_c1(1P)", "J/psi(1S)" ], # For amplitude analysis
			"DTFB2": [ head_full, "chi_c2(1P)", "J/psi(1S)" ], # For amplitude analysis
			"DTF1": [ "chi_c1(1P)", "J/psi(1S)" ], # For branching fractions and selection
			"DTF2": [ "chi_c2(1P)", "J/psi(1S)" ], # For branching fractions and selection
		}
	else:
		combinations = {
			"DTFB": [ head_full, "J/psi(1S)" ], # For amplitude analysis
			"DTF": [ "J/psi(1S)" ], # For branching fractions and selection
		}
	for name in combinations:
		comb = combinations[name]
		#add_dtf(dtt, name, head, comb, PV = True, tracks = True)
		add_dtf_loki(dtt, decay, name, comb, PV = True, tracks = True)
	# Extra LoKi variables for all particles
	dtt.addTupleTool("LoKi::Hybrid::TupleTool/AllExtraVars").Variables = {
		"ETA": "ETA", # pseudorapidity
		"PHI": "PHI", # azimuthal angle around beam axis
	}
	# Lifetime fit just for head:
	getattr(dtt, head).addTupleTool("TupleToolPropertime")
	# LoKi variables just for muons
	for muon in ["muplus", "muminus"]:
		getattr(dtt, muon).addTupleTool("LoKi::Hybrid::TupleTool/NShared"+muon).Variables = {
			"NSHAREDMU": "NSHAREDMU",
		}
	for particle in dtt.Branches:
		# Photon information
		if particle == "gamma":
			dtt.gamma.ToolList += ["TupleToolPhotonInfo/PhotonInfo", "TupleToolPi0Info/Pi0Info"]
			dtt.gamma.addTupleTool("LoKi::Hybrid::TupleTool/pi0veto").Variables = {
				"pi0veto": "PINFO(25030, -1)", # magic number defined in PsiX0Conf.chi_c from StrippingBandQ/StrippingPsiX0.py
			}
		else:
			geom_dict = {
				"IPCHI2_OWNPV" : "BPVIPCHI2()",
			}
			if particle not in stables:
				geom_dict.update({
					"ENDVERTEX_CHI2": "VFASPF(VCHI2)",
					"ENDVERTEX_NDOF": "VFASPF(VDOF)",
				})
			if particle == head:
				geom_dict.update({
					"DIRA_OWNPV" : "BPVDIRA",
					"FDCHI2_OWNPV": "BPVVDCHI2",
					"DLS_OWNPV": "BPVDLS", # strongly correlated with sqrt(FDCHI2_OWNPV)
				})
			getattr(dtt, particle).addTupleTool("LoKi::Hybrid::TupleTool/"+particle+"Geometry").Variables = geom_dict
	return algorithms
