#!/usr/bin/env python
# For WGSelection code see:
# https://gitlab.cern.ch/lhcb/Stripping/-/blob/run2-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingBandQ/StrippingPsiXForBandQ.py
# https://gitlab.cern.ch/lhcb/Stripping/-/blob/run2-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingBandQ/StrippingPsiX0.py

"""
################################################################################
'MuonCut':
ISMUON &
( PT            >  550 * MeV ) &
( PIDmu - PIDpi >    0       ) &
( CLONEDIST     > 5000       )
################################################################################
'PionCut'   :
( PT          > 200 * MeV ) &
( CLONEDIST   > 5000      ) &
( TRGHOSTPROB < 0.5       ) &
( TRCHI2DOF   < 4         ) &
in_range ( 2          , ETA , 5         ) &
in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
HASRICH                     &
( MIPCHI2DV()  > 4        )
################################################################################
'KaonCut':
( PT          > 200 * MeV ) &
( CLONEDIST   > 5000      ) &
( TRGHOSTPROB < 0.5       ) &
( TRCHI2DOF   < 4         ) &
in_range ( 2          , ETA , 5         ) &
in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
HASRICH                     &
( MIPCHI2DV()  > 4        )
################################################################################
'ProtonCut':
( PT           > 200 * MeV ) &
( CLONEDIST    > 5000      ) &
( TRCHI2DOF    < 4         ) &
( TRGHOSTPROB  < 0.5       ) &
in_range ( 2         , ETA , 5         ) &
in_range ( 10 * GeV  , P   , 150 * GeV ) &
HASRICH                     &
( MIPCHI2DV()  > 4        )
################################################################################
## PID-cuts for hadrons
'PionPIDCut':
" PROBNNpi > 0.1 ",
'KaonPIDCut':
" PROBNNk  > 0.1 ",
'ProtonPIDCut':
" PROBNNp  > 0.1 ",
################################################################################
# 'JpsiCut':
comb:
( ADAMASS  ( 'J/psi(1S)' ) < 120 * MeV ) |
( ADAMASS  (   'psi(2S)' ) < 120 * MeV )
mother:
VFASPF(VCHI2PDOF) < 20
################################################################################
# 'chicCut'
DaughtersCuts={
'J/psi(1S)': " M  < ( 3.220 ) * GeV ",
'gamma': " PT > 400 * MeV "
},
CombinationCut= ( AM - AM1 ) < 650 * MeV
################################################################################
# Lb -> psi(') pK
Combination12Cut= ( AM < 5.850 * GeV )  &
( ACHI2DOCA(1,2) <  20 )
#
CombinationCut=        in_range ( 5.350 * GeV , AM , 5.850 * GeV   )     &
( ACHI2DOCA(1,3) <  20 ) &
( ACHI2DOCA(2,3) <  20 )
##
from GaudiKernel.PhysicalConstants import c_light
MotherCut=        in_range ( 5.390 * GeV ,  M , 5.810 * GeV   )            &
( VFASPF(VCHI2PDOF) < 10 ) &
( BPVLTIME ( 25 ) * c_light      > 75 * micrometer )
################################################################################
Lb -> chic pK
Combination12Cut ( AM < 6 * GeV  ) &
( ACHI2DOCA(1,2) < 20 )
##
CombinationCut= in_range ( 5.00 * GeV , AM , 6.50 * GeV ) &
( ACHI2DOCA(1,3) < 20 ) &
( ACHI2DOCA(2,3) < 20 )
##
MotherCut=
in_range ( 5.05 * GeV ,  M , 6.45 * GeV )                 &
( VFASPF(VCHI2PDOF) < 10      ) &
( BPVLTIME ( 25 ) * c_light      > 100 * micrometer      )
################################################################################
B -> psi(') K
CombinationCut=" in_range ( 5.100 * GeV , AM , 5.550 * GeV   ) | in_range ( 6.050 * GeV , AM , 6.555 * GeV   ) ",
MotherCut=        ( VFASPF(VCHI2PDOF) < 12  ) &
( ( in_range ( 5.140 * GeV ,  M , 5.510 * GeV    ) & ( BPVLTIME ( 25 ) * c_light > 75 * micrometer ) ) | ( in_range ( 6.090 * GeV ,  M , 6.510 * GeV ) & ( BPVLTIME ( 25 ) * c_light > 50 * micrometer ) ) )
################################################################################
B -> chic K
CombinationCut= in_range ( 4.50 * GeV , AM , 6.00 * GeV ) & ( ACHI2DOCA(1,2) < 20 )
MotherCut     =  in_range ( 4.55 * GeV ,  M , 5.95 * GeV ) &
( VFASPF(VCHI2PDOF) < 10 ) &
( BPVLTIME ( 25 ) * c_light > 100 * micrometer )
################################################################################
B -> psi(') K+ pi-
Combination12Cut = ( AM < 5.55 * GeV )  & ( ACHI2DOCA(1,2) <  20 )
CombinationCut   =  in_range ( 5.100 * GeV , AM , 5.550 * GeV   ) & ( ACHI2DOCA(1,3) <  20 ) & ( ACHI2DOCA(2,3) <  20 )
MotherCut        =  in_range ( 5.140 * GeV ,  M , 5.510 * GeV    )            &
        ( chi2vxndf < 10 ) &
        ( ctau      > 75 * micrometer )
################################################################################
B -> chic K- pi+
Combination12Cut = ( AM < 6 * GeV  ) &
( ACHI2DOCA(1,2) < 20 )
CombinationCut   = in_range ( 4.50 * GeV , AM , 6.00 * GeV ) &
( ACHI2DOCA(1,3) < 20 ) &
( ACHI2DOCA(2,3) < 20 )
MotherCut        =
in_range ( 4.55 * GeV ,  M , 5.95 * GeV )                 &
( chi2vxNDF < 10      ) &
( ctau      > 100  * micrometer      )

################################################################################
"""

chi_c1_M = 3510.67

cuts = {
	"pion"  : [
		"PROBNNpi > 0.1",
		"TRGHOSTPROB < 0.4",
		"TRCHI2DOF < 4",
		"PT > 250 * MeV",
	],
	"kaon"  : [
		"PROBNNk > 0.1",
		"TRGHOSTPROB < 0.4",
		"TRCHI2DOF < 4",
		"PT > 250 * MeV",
	],
	"proton": [
		"PROBNNk < 0.8",
		"PROBNNp > 0.1",
		"TRGHOSTPROB < 0.4",
		"TRCHI2DOF < 4",
		"PT > 500 * MeV",
	],
	"muon"  : [
		"PROBNNmu > 0.1",
		"TRGHOSTPROB < 0.4",
		"TRCHI2DOF < 4",
	],
	"gamma" : [
		"CL > 0.03",
	],
	"jpsi" : [
		"ADMASS('J/psi(1S)') < 120 * MeV", # Get rid of psi(2S) region
		"PT > 1 * GeV",
	],
	"chi_c" : [
		"in_range(200 * MeV, M - MAXTREE('J/psi(1S)' == ABSID, M), 580 * MeV)", # capture the following: chic0(3415), chic1(3511), chic2(3556)
	],
	"beauty": [
		"PT > 3 * GeV",
		"BPVDLS > 10",
		"BPVDIRA > 0.9999",
		"MIPCHI2DV(PRIMARY) < 25", # already present
		"VFASPF(VCHI2PDOF) < 10", # mostly already there, except B -> psi(') K
	],
	"Lb_chicpK": [ # Cuts specific to reconstruction as Lb -> chi_c1 p K
		"M - MAXTREE('chi_c1(1P)' == ABSID, M) + {} * MeV < 6000 * MeV".format(chi_c1_M),
		"in_range(0, DTF_CHI2NDOF(True, strings(['chi_c1(1P)', 'J/psi(1S)'])), 4)",
	],
	"Lb_JpsipK": [ # Cuts specific to reconstruction as Lb -> J/psi p K
		"in_range(5390 * MeV, M, 5810 * MeV)", # already present
		"in_range(0, DTF_CHI2NDOF(True, 'J/psi(1S)'), 4)",
	],
	"Bu_chicK": [ # Cuts specific to reconstruction as B+ -> chi_c1 K
		"in_range(5140 * MeV, M - MAXTREE('chi_c1(1P)' == ABSID, M) + {}, 5510 * MeV)".format(chi_c1_M),
		"in_range(0, DTF_CHI2NDOF(True, strings(['chi_c1(1P)', 'J/psi(1S)'])), 4)",
	],
	"Bu_JpsiK": [ # Cuts specific to reconstruction as B+ -> J/psi K
		"in_range(5140 * MeV, M, 5510 * MeV)", # Get rid of B_c mass window
		"in_range(0, DTF_CHI2NDOF(True, 'J/psi(1S)'), 4)",
	],
	"Bd_chicKpi": [ # Cuts specific to reconstruction as B0 -> chi_c1 K pi
		"in_range(5140 * MeV, M - MAXTREE('chi_c1(1P)' == ABSID, M) + {}, 5510 * MeV)".format(chi_c1_M),
		"in_range(0, DTF_CHI2NDOF(True, strings(['chi_c1(1P)', 'J/psi(1S)'])), 4)",
	],
	"Bd_JpsiKpi": [ # Cuts specific to reconstruction as B0 -> J/psi K pi
		"in_range(5140 * MeV, M, 5510 * MeV)",
		"in_range(0, DTF_CHI2NDOF(True, 'J/psi(1S)'), 4)",
	],
}

def build_child_cuts(childcut_selections):
	return ["CHILDCUT({}, '{}')".format(" & ".join(["({})".format(cut) for cut in childcut_selections[selector]]), selector) for selector in childcut_selections]

