# Ntuple production for $`\Lambda_b \to (c\overline{c}) p K^-`$, $`B^+ \to (c\overline{c}) K^+`$, $`B^0 \to (c\overline{c}) \pi^- K^+`$ with $`(c\overline{c}) \in [J/\psi \to \mu^+\mu^-, \chi_c \to J/\psi \gamma]`$

## Dataset

The `info.yaml` file is generated from the `davinci/` folder in [the analysis repository](https://gitlab.cern.ch/admorris/lb2chicpk) (likely to move: check the B&Q GitLab namespace).
The mapping between sample nickname, event type and set of DecayTreeTuples is determined in `dataset.json`.
The data bookkeeping paths are chosen manually (see `BK_locations_90000000.txt`.
For each MC event type: all bookkeeping locations not matching a line in `bk_ignore` are found automatically using the [`anubis`](https://gitlab.cern.ch/admorris/anubis) bookkeeping utils.

## Restripping

A restripping production is created for MC samples with:

- different stripping version to data
- full DST output

The DaVinci version is chosen according to what is used in MC production (not necessarily the same as the stripping campaigns).

## WG selection

A slimmed-down version of the B&Q 17r1 WG selection is found in `options/RunSelection.py`.
We do not directly use the output of the WG selection production because of wanting to use the latest stripping versions.
Regardless, it is necessary to run this on MC ourselves, so data may as well get the same treatment.

The file `options/PreFilter.py` includes the same `EventPreFilters` found in the WG selection.

## Ntupling

A `DecayTreeTuple` or `MCDecayTreeTuple` algorithm is added to a production with modular options files `options/<decay>.py` and `options/<decay>_MC_Truth.py`.
Inside these, selection cuts are built by defining LoKi particle selectors and cuts, which are then reduced to a single string passed to a `FilterSelection` algorithm.
Standard selection cuts for specific particles and decay candidates are maintained in `helpers/Preselection.py`.
`DecayTreeTuple` algorithms are built using the `make_dtt` function defined in `helpers/DecayTreeTupleMaker.py`. Similarly, for `MCDecayTreeTuple`s in `helpers/MCDecayTreeTupleMaker.py`.

When a sample's nickname agrees with the tuple name, it is deemed to be "signal" and the corresponding `MCDecayTreeTuple` is added, to allow efficiencies to be calculated.
Additionally, event pre-filtering is removed.

### DecayTreeFitter

Generally there are two variations of DTF added to the ntuples:

- with charmonium mass(es) constrained (for selection and mass fits)
- with charmonium mass(es) and b-hadron mass constrained (for amplitude fits)

For modes containing $`\chi_{cJ}`$ both $`\chi_{c1}`$ and $`\chi_{c2}`$ hypotheses are included (for a total of 4 DTFs).

In all DTFs, the PV constraint is applied and the masses and four-momenta (`M`, `PX`, `PY`, `PZ`, `PE`) of all particles in the decay tree are written, as well as the $`\chi^2`$ and ndof of the fit.
Due to using `DTFDict`, only the results of the fit using the best PV are included (i.e. no array branches).

### TupleTools

The tools `TupleToolKinematic`, `TupleToolANNPID`, `TupleToolPid`, `TupleToolTrackInfo`, `TupleToolEventInfo` and `TupleToolRecoStats` are added with default configuration to the top level.

`TupleToolGeometry` is not used, and instead replaced with a sparser set of LoKi functors.
All tracks and candidates built from tracks have $`\chi^2_\text{IP}`$ relative to the PV.
All candidates built from tracks have $`\chi^2_\text{Vtx}`$ and ndof.
The b-hadron in addition has DIRA, DLS and $`\chi^2_\text{FD}`$, all relative to PV

`TupleToolProperTime` is added to the b-hadron.

The LoKi functor `NSHAREDMU` is written for muons.

The photon from $`\chi_c \to J/\psi \gamma`$ has `TupleToolPhotonInfo` and `TupleToolPi0Info` plus a boolean $`\pi^0`$ veto variable defined in the WG selection.

For MC we have `TupleToolMCBackgroundInfo`,	`MCTupleToolRedecay`, `MCTupleToolKinematic` and `MCTupleToolHierarchy`.

A large list of useful and potentially-useful triggers is added to the b-hadron with `TupleToolTISTOS`.
