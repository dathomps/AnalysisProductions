from Configurables import DaVinci
from LbchicpK.helpers.MCDecayTreeTupleMaker import make_mcdtt

DaVinci().UserAlgorithms += make_mcdtt(
	name="LbJpsipK",
	decay="${Lambda_b0}[Lambda_b0 ==> ${Jpsi}(J/psi(1S) => ${muplus}mu+ ${muminus}mu-) ${proton}p+ ${kaon}K-]CC",
)

