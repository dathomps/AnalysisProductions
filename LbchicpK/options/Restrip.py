from Configurables import DaVinci
from LbchicpK.helpers.Stripping import event_node_killer, restripping, line_name_matches
from LbchicpK.helpers.DSTWriter import write_dst

if DaVinci().InputType == "MDST":
	DaVinci().RootInTES = "/Event/AllStreams"

stripping_version = {
    "2011": "21",
    "2012": "21r1",
    "2015": "24r2",
    "2016": "28r2",
    "2017": "29r2",
    "2018": "34",
}[DaVinci().DataType]

restrip_conf = restripping(stripping_version, line_name_matches(r".*DiMuon.*Psi2MuMu.*"), ignore_filter_passed = True)
dst_writer = write_dst(restrip_conf.activeStreams(), extra_items = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"])

DaVinci().ProductionType = "Stripping"
DaVinci().appendToMainSequence([event_node_killer, restrip_conf.sequence(), dst_writer.sequence()])
