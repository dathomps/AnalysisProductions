# Slimmed-down version of WGProduction script
# https://gitlab.cern.ch/lhcb-datapkg/WG/BandQConfig/-/blob/v17r1/scripts/BandQ.py

from Configurables import DaVinci
from PhysSelPython.Wrappers import MultiSelectionSequence, SelectionSequence
from StrippingSelections.StrippingBandQ.StrippingPsiXForBandQ import PsiX_BQ_Conf as PsiX
from StrippingSelections.StrippingBandQ.StrippingPsiX0        import PsiX0Conf    as PsiX0

if DaVinci().Simulation:
	stream = "AllStreams"
else:
	stream = "Dimuon"

if DaVinci().InputType == "MDST":
	DaVinci().RootInTES = "/Event/{}".format(stream)
	tes_prefix = ""
else:
	tes_prefix = "/Event/{}/".format(stream)

jpsi_name = "FullDSTDiMuonJpsi2MuMuDetachedLine"
psi2_name = "FullDSTDiMuonPsi2MuMuDetachedLine"

jpsi_line  = tes_prefix+"Phys/{}/Particles".format(jpsi_name)
psi2s_line = tes_prefix+"Phys/{}/Particles".format(psi2_name)

config = {"DIMUONLINES": [jpsi_line, psi2s_line]}

psix  = PsiX ("PsiX" , config)
psix0 = PsiX0("PsiX0", config)

psi_x = MultiSelectionSequence(
	"PSIX",
	Sequences = [
		SelectionSequence("Lb2PsiPK" , psix .psi_pK()),    # [Lambda_b0 -> J/psi(1S) p+ K-]cc
		SelectionSequence("Lb2ChicPK", psix0.lb2chicpK()), # [Lambda_b0 -> chi_c1(1P) p+ K-]cc
		SelectionSequence("Bu2PsiK"  , psix .psi_K()),     # [B+ -> J/psi(1S) K+ ]cc
		SelectionSequence("Bu2ChicK" , psix0.b2chicK()),   # [B+ -> chi_c1(1P) K+ ]cc
		SelectionSequence("B2PsiKPi" , psix.psi_2Kpi()),   # [B0 -> J/psi(1S) K+ pi-]cc
		SelectionSequence("B2ChicKPi", psix0.b2chicKpi()), # [B0 -> chi_c1(1P) K- pi+]cc
	]
)

DaVinci().UserAlgorithms += [psi_x.sequence()]
