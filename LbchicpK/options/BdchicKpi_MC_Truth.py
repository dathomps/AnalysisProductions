from Configurables import DaVinci
from LbchicpK.helpers.MCDecayTreeTupleMaker import make_mcdtt

DaVinci().UserAlgorithms += make_mcdtt(
	name="BdchicKpi",
	decay="${B0}[[B0]cc ==> ${Jpsi}(J/psi(1S) => ${muplus}mu+ ${muminus}mu-) ${gamma}gamma ${kaon}K+ ${pion}pi-]CC"
)

