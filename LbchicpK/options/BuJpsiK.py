from Configurables import DaVinci
from LbchicpK.helpers.DecayTreeTupleMaker import make_dtt
from LbchicpK.helpers.Preselection import *

childcut_selections = {
	"Beauty -> J/psi(1S) ^(K+|K-)"              : cuts["kaon"],
	"Beauty -> (J/psi(1S) -> ^mu+ mu-) (K+|K-)" : cuts["muon"],
	"Beauty -> (J/psi(1S) -> mu+ ^mu-) (K+|K-)" : cuts["muon"],
	"Beauty -> ^J/psi(1S) (K+|K-)"              : cuts["jpsi"],
}










all_cuts = [" & ".join(["({})".format(cut) for cut in cuts["Bu_JpsiK"] + cuts["beauty"]])] + build_child_cuts(childcut_selections)

DaVinci().UserAlgorithms += make_dtt(
	name="BuJpsiK",
	location="Phys/SelPsiKForPsiX/Particles",
	decay="${Bplus}[B+ -> ${Jpsi}(J/psi(1S) -> ${muplus}mu+ ${muminus}mu-) ${kaon}K+]CC",
	cuts=" & ".join(all_cuts)
)

