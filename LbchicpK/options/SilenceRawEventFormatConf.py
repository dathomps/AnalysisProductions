"""
Override the __str__ method of RawEventFormatConf to prevent it from printing the usual extremely long line in the log.
This saves about 34 kB of space per subjob.
"""
from Configurables import RawEventFormatConf

RawEventFormatConf.__str__ = lambda self: ""

