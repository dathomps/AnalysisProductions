from Configurables import DaVinci
from LbchicpK.helpers.DecayTreeTupleMaker import make_dtt
from LbchicpK.helpers.Preselection import *

childcut_selections = {
	"Beauty -> chi_c1(1P) ^(K+|K-) (pi+|pi-)"                                   : cuts["kaon"],
	"Beauty -> chi_c1(1P) (K+|K-) ^(pi+|pi-)"                                   : cuts["pion"],
#	"Beauty -> (chi_c1(1P) -> (J/psi(1S) -> ^mu+ mu-) gamma) (K+|K-) (pi+|pi-)" : cuts["muon"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> (J/psi(1S) -> mu+ ^mu-) gamma) (K+|K-) (pi+|pi-)" : cuts["muon"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> J/psi(1S) ^gamma) (K+|K-) (pi+|pi-)"              : cuts["gamma"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> ^J/psi(1S) gamma) (K+|K-) (pi+|pi-)"              : cuts["jpsi"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
	"Beauty -> ^chi_c1(1P) (K+|K-) (pi+|pi-)"                                   : cuts["chi_c"],
}


intree_selections = [ # Cope with ChildPredicate errors
	"INTREE((ID == 'gamma') & {})".format(" & ".join(["({})".format(cut) for cut in cuts["gamma"]])),
	"NINTREE((ABSID == 'mu+') & {}) == 2".format(" & ".join(["({})".format(cut) for cut in cuts["muon"]])),
	"INTREE((ID == 'J/psi(1S)') & {})".format(" & ".join(["({})".format(cut) for cut in cuts["jpsi"]])),
]

all_cuts = [" & ".join(["({})".format(cut) for cut in cuts["Bd_chicKpi"] + cuts["beauty"] + intree_selections])] + build_child_cuts(childcut_selections)

DaVinci().UserAlgorithms += make_dtt(
	name="BdchicKpi",
	location="Phys/SelB2ChicKPiForPsiX0/Particles",
	decay="${B0}[B0 -> ${chi_c}(chi_c1(1P) -> ${Jpsi}(J/psi(1S) -> ${muplus}mu+ ${muminus}mu-) ${gamma}gamma) ${kaon}K- ${pion}pi+]CC",
	cuts=" & ".join(all_cuts)
)

