# Taken from WGProduction script
# https://gitlab.cern.ch/lhcb-datapkg/WG/BandQConfig/-/blob/v17r1/scripts/BandQ.py

from Configurables import DaVinci
from PhysConf.Filters import LoKi_Filters

fltrs = LoKi_Filters(
	STRIP_Code = """
		HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuTOS.*'      ) |
		HLT_PASS_RE ( 'Stripping.*DiMuonPsi2MuMuTOS.*'       ) |
		HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuDetached.*' ) |
		HLT_PASS_RE ( 'Stripping.*DiMuonPsi2MuMuDetached.*'  ) |
		HLT_PASS_RE ( 'Stripping.*DiMuonDiMuonHighMass.*'    )
	"""
)

DaVinci().EventPreFilters = fltrs.filters("Filters")
