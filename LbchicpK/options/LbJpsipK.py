from Configurables import DaVinci
from LbchicpK.helpers.DecayTreeTupleMaker import make_dtt
from LbchicpK.helpers.Preselection import *

childcut_selections = {
	"Beauty -> J/psi(1S) (p+|p~-) ^(K-|K+)"              : cuts["kaon"],
	"Beauty -> J/psi(1S) ^(p+|p~-) (K-|K+)"              : cuts["proton"],
	"Beauty -> (J/psi(1S) -> ^mu+ mu-) (p+|p~-) (K-|K+)" : cuts["muon"],
	"Beauty -> (J/psi(1S) -> mu+ ^mu-) (p+|p~-) (K-|K+)" : cuts["muon"],
	"Beauty -> ^J/psi(1S) (p+|p~-) (K-|K+)"              : cuts["jpsi"],
}









all_cuts = [" & ".join(["({})".format(cut) for cut in cuts["Lb_JpsipK"] + cuts["beauty"]])] + build_child_cuts(childcut_selections)

DaVinci().UserAlgorithms += make_dtt(
	name="LbJpsipK",
	location="Phys/SelPsiPKForPsiX/Particles",
	decay="${Lambda_b0}[Lambda_b0 -> ${Jpsi}(J/psi(1S) -> ${muplus}mu+ ${muminus}mu-) ${proton}p+ ${kaon}K-]CC",
	cuts=" & ".join(all_cuts)
)

