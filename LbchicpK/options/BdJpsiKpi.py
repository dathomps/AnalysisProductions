from Configurables import DaVinci
from LbchicpK.helpers.DecayTreeTupleMaker import make_dtt
from LbchicpK.helpers.Preselection import *

childcut_selections = {
	"Beauty -> J/psi(1S) ^(K+|K-) (pi+|pi-)"              : cuts["kaon"],
	"Beauty -> J/psi(1S) (K+|K-) ^(pi+|pi-)"              : cuts["pion"],
	"Beauty -> (J/psi(1S) -> ^mu+ mu-) (K+|K-) (pi+|pi-)" : cuts["muon"],
	"Beauty -> (J/psi(1S) -> mu+ ^mu-) (K+|K-) (pi+|pi-)" : cuts["muon"],
	"Beauty -> ^J/psi(1S) (K+|K-) (pi+|pi-)"              : cuts["jpsi"],
}










all_cuts = [" & ".join(["({})".format(cut) for cut in cuts["Bd_JpsiKpi"] + cuts["beauty"]])] + build_child_cuts(childcut_selections)

DaVinci().UserAlgorithms += make_dtt(
	name="BdJpsiKpi",
	location="Phys/SelPsi2KPiForPsiX/Particles",
	decay="${B0}[B0 -> ${Jpsi}(J/psi(1S) -> ${muplus}mu+ ${muminus}mu-) ${kaon}K+ ${pion}pi-]CC",
	cuts=" & ".join(all_cuts)
)

