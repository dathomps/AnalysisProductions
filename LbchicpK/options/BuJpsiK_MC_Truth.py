from Configurables import DaVinci
from LbchicpK.helpers.MCDecayTreeTupleMaker import make_mcdtt

DaVinci().UserAlgorithms += make_mcdtt(
	name="BuJpsiK",
	decay="${Bplus}[B+ => ${Jpsi}(J/psi(1S) => ${muplus}mu+ ${muminus}mu-) ${kaon}K+]CC"
)

