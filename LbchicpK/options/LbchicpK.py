from Configurables import DaVinci
from LbchicpK.helpers.DecayTreeTupleMaker import make_dtt
from LbchicpK.helpers.Preselection import *

childcut_selections = {
	"Beauty -> chi_c1(1P) (p+|p~-) ^(K-|K+)"                                   : cuts["kaon"],
	"Beauty -> chi_c1(1P) ^(p+|p~-) (K-|K+)"                                   : cuts["proton"],
#	"Beauty -> (chi_c1(1P) -> (J/psi(1S) -> ^mu+ mu-) gamma) (p+|p~-) (K-|K+)" : cuts["muon"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> (J/psi(1S) -> mu+ ^mu-) gamma) (p+|p~-) (K-|K+)" : cuts["muon"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> J/psi(1S) ^gamma) (p+|p~-) (K-|K+)"              : cuts["gamma"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
#	"Beauty -> (chi_c1(1P) -> ^J/psi(1S) gamma) (p+|p~-) (K-|K+)"              : cuts["jpsi"], # LoKiSvc.REPORT      ERROR LoKi::Particles::ChildPredicate: 	Child LHCb::Particle* points to NULL, return 'false'
	"Beauty -> ^chi_c1(1P) (p+|p~-) (K-|K+)"                                   : cuts["chi_c"],
}

intree_selections = [ # Cope with ChildPredicate errors
	"INTREE((ID == 'gamma') & {})".format(" & ".join(["({})".format(cut) for cut in cuts["gamma"]])),
	"NINTREE((ABSID == 'mu+') & {}) == 2".format(" & ".join(["({})".format(cut) for cut in cuts["muon"]])),
	"INTREE((ID == 'J/psi(1S)') & {})".format(" & ".join(["({})".format(cut) for cut in cuts["jpsi"]])),
]

all_cuts = [" & ".join(["({})".format(cut) for cut in cuts["Lb_chicpK"] + cuts["beauty"] + intree_selections])] + build_child_cuts(childcut_selections)

DaVinci().UserAlgorithms += make_dtt(
	name="LbchicpK",
	location="Phys/SelLb2ChicPKForPsiX0/Particles",
	decay="${Lambda_b0}[Lambda_b0 -> ${chi_c}(chi_c1(1P) -> ${Jpsi}(J/psi(1S) -> ${muplus}mu+ ${muminus}mu-) ${gamma}gamma) ${proton}p+ ${kaon}K-]CC",
	cuts=" & ".join(all_cuts)
)

