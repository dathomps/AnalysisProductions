""" make tuples for LcD0K from B2OC stripping lines
    @author Marian Stahl marian.stahl@cern.ch
"""
from PhysSelPython.Wrappers import AutomaticData, FilterSelection, SelectionSequence, TupleSelection
from PhysConf.Filters import LoKi_Filters
from DecayTreeTuple.Configuration import *
from Configurables import (RawEventFormatConf, DaVinci, LoKi__Hybrid__DictOfFunctors, LoKi__Hybrid__DTFDict, LoKi__Hybrid__Dict2Tuple, TupleToolTrigger, TupleToolTISTOS)
from collections import OrderedDict
from MVADictHelpers import *
from LcD0K.standard_functors import *
from LcD0K.dfrombtools import getMVAVars

# Override the __str__ method of RawEventFormatConf to prevent it from printing the usual extremely long line in the log. This saves about 34 kB of space per subjob.
RawEventFormatConf.__str__ = lambda self: ""

try:
    isMC = DaVinci().Simulation
except AttributeError:
    isMC = False

if DaVinci().InputType == "MDST":
    DaVinci().RootInTES = '/Event/AllStreams' if isMC else '/Event/Bhadron'
    tes_prefix = ""
else:
    tes_prefix = "/Event/AllStreams/"

LcD0KTES = "X2LcD0KD02KPiBeauty2CharmLine"
# filter by Stripping lines
if not isMC: # We want to keep all MC events to use for efficiency
    DaVinci().EventPreFilters = LoKi_Filters(STRIP_Code="HLT_PASS_RE('Stripping"+LcD0KTES+"Decision')").filters('PreFilter')

# for DfromBBDTs
DfBKids    = { "D0" : OrderedDict({'K':1, 'pi':2}), "Lc" : OrderedDict({'p':1, 'K':2, 'pi':3}) } # daughter-names are the ones in the xml file
DfBWeightsPath = "$TMVAWEIGHTSROOT/data/DfromB/"
add_bdt_val = lambda s, p : addTMVAclassifierValue(s.algorithm(), DfBWeightsPath+p+"Pi_"+DaVinci().DataType+"_GBDT.weights.xml", getMVAVars(p, DfBKids[p]), p+"_BDT")
add_bdt_tpl = lambda b, p : addTMVAclassifierTuple(b, DfBWeightsPath+p+"Pi_"+DaVinci().DataType+"_GBDT.weights.xml", getMVAVars(p, DfBKids[p]))

LcD0K = FilterSelection("LcD0KSelection", [AutomaticData(tes_prefix+'Phys/'+LcD0KTES+'/Particles')],
                        Code="""(CHILDCUT(VALUE('LoKi::Hybrid::DictValue/Lc_BDT')>-0.5,1)) & (CHILDCUT(VALUE('LoKi::Hybrid::DictValue/D0_BDT')>-0.5,2)) &
                                (DOCACHI2(1,2)<16) & (DOCA(1,2)<0.2*mm) & (DOCACHI2(1,3)<20) & (DOCACHI2(2,3)<25) & (DOCA(1,3)<0.4*mm) & (DOCA(2,3)<0.5*mm) &
                                (in_range(5.1*GeV,M,6.2*GeV)) & (CHI2VXNDF<32) & (BPVVDZ>0.2*mm) & (PT>4*GeV) & (P>32*GeV) &
                                (CHILDIP(1)<0.2*mm) & (CHILDIPCHI2(1)<16) & (CHILDIP(2)<0.3*mm) & (CHILDIPCHI2(2)<16) & (CHILDIP(3)<0.5*mm) & (CHILDIPCHI2(3)<16)""")
add_bdt_val(LcD0K,"Lc")
add_bdt_val(LcD0K,"D0")

triggers_list = ["L0HadronDecision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision",
                 "Hlt2TopoE2BodyDecision",
                 "Hlt2TopoE3BodyDecision",
                 "Hlt2TopoE4BodyDecision",
                 "Hlt2TopoEE2BodyDecision",
                 "Hlt2TopoEE3BodyDecision",
                 "Hlt2TopoEE4BodyDecision",
                 "Hlt2TopoMu2BodyDecision",
                 "Hlt2TopoMu3BodyDecision",
                 "Hlt2TopoMu4BodyDecision",
                 "Hlt2TopoMuE2BodyDecision",
                 "Hlt2TopoMuE3BodyDecision",
                 "Hlt2TopoMuE4BodyDecision",
                 "Hlt2TopoMuMu2BodyDecision",
                 "Hlt2TopoMuMu3BodyDecision",
                 "Hlt2TopoMuMu4BodyDecision",
                 "Hlt2TopoMuMuDDDecision",
                 "Hlt2PhiIncPhiDecision" ]
if DaVinci().DataType == '2017' or DaVinci().DataType == '2018':
  triggers_list.append("Hlt2CharmHadInclLcpToKmPpPipBDTTurboDecision")

# Make tuples
LcD0KTpl = TupleSelection("LcD0K", [LcD0K], Decay = "((B0 -> ^(Lambda_c+ -> ^p+ ^K- ^pi+) ^(D0 -> ^K+ ^pi-) ^K-) || (B0 -> ^(Lambda_c~- -> ^p~- ^K+ ^pi-) ^(D0 -> ^K- ^pi+) ^K+))")
LcD0KSeq = SelectionSequence("LcD0KSeq",TopSelection=LcD0KTpl)

LcD0KTpl.TupleName = "t"
LcD0KTpl.RevertToPositiveID = False
LcD0KTpl.ErrorMax = 100
branchdict = {
  "Lc"    : "((B0 -> ^(Lambda_c+ -> p+ K- pi+) (D0 -> K+ pi-) K-) || (B0 -> ^(Lambda_c~- -> p~- K+ pi-) (D0 -> K- pi+) K+))",
  "D0"    : "((B0 -> (Lambda_c+ -> p+ K- pi+) ^(D0 -> K+ pi-) K-) || (B0 -> (Lambda_c~- -> p~- K+ pi-) ^(D0 -> K- pi+) K+))",
  "K"     : "((B0 -> (Lambda_c+ -> p+ K- pi+) (D0 -> K+ pi-) ^K-) || (B0 -> (Lambda_c~- -> p~- K+ pi-) (D0 -> K- pi+) ^K+))",
  "p"     : "((B0 -> (Lambda_c+ -> ^p+ K- pi+) (D0 -> K+ pi-) K-) || (B0 -> (Lambda_c~- -> ^p~- K+ pi-) (D0 -> K- pi+) K+))",
  "Lc_K"  : "((B0 -> (Lambda_c+ -> p+ ^K- pi+) (D0 -> K+ pi-) K-) || (B0 -> (Lambda_c~- -> p~- ^K+ pi-) (D0 -> K- pi+) K+))",
  "Lc_pi" : "((B0 -> (Lambda_c+ -> p+ K- ^pi+) (D0 -> K+ pi-) K-) || (B0 -> (Lambda_c~- -> p~- K+ ^pi-) (D0 -> K- pi+) K+))",
  "D0_K"  : "((B0 -> (Lambda_c+ -> p+ K- pi+) (D0 -> ^K+ pi-) K-) || (B0 -> (Lambda_c~- -> p~- K+ pi-) (D0 -> ^K- pi+) K+))",
  "D0_pi" : "((B0 -> (Lambda_c+ -> p+ K- pi+) (D0 -> K+ ^pi-) K-) || (B0 -> (Lambda_c~- -> p~- K+ pi-) (D0 -> K- ^pi+) K+))"
}
LcD0KTpl.addBranches(dict({"Lb" : "^(B0 -> (Lambda_c+ -> p+ K- pi+) (D0 -> K+ pi-) K-) || ^(B0 -> (Lambda_c~- -> p~- K+ pi-) (D0 -> K- pi+) K+)",},**branchdict))
LcD0KTpl.ToolList = ['TupleToolKinematic', 'TupleToolPid', 'TupleToolEventInfo']
if isMC:
    LcD0KTpl.ToolList += ["TupleToolMCBackgroundInfo"]
    # MC-specific stuff
    mctools = ["MCTupleToolKinematic", "MCTupleToolHierarchy", "MCTupleToolPID"]
    MCTruth = LcD0KTpl.addTupleTool("TupleToolMCTruth")
    MCTruth.ToolList = mctools
# event specific tool
LoKiEventTuple = LcD0KTpl.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKiEvent")
LoKiEventTuple.VOID_Variables = std_event_vars()
# Add branches and branch-specific tools
LbBranch = getattr(LcD0KTpl, "Lb")
LcBranch = getattr(LcD0KTpl, "Lc")
D0Branch = getattr(LcD0KTpl, "D0")
# trigger
TTT = TupleToolTrigger("TTT", TriggerList=triggers_list, Verbose=True, VerboseL0=True, VerboseHlt1=True, VerboseHlt2=False, FillHlt2=False)
TTTT = TupleToolTISTOS("TTTT", TriggerList=triggers_list, Verbose=True, VerboseHlt1=True, VerboseHlt2=True)
LcD0KTpl.addTupleTool(TTT)
LbBranch.addTupleTool(TTTT)
if DaVinci().DataType == '2016' or DaVinci().DataType == '2015': # This line does not exist in S29r2 or S34
  TTS = LcD0KTpl.addTupleTool("TupleToolStripping/TTS")
  TTS.StrippingList = ["StrippingInclusiveCharmBaryons_LcLineDecision"]

# DTF
DTF_Vars = {
    "PX"     : "PX",
    "PY"     : "PY",
    "PZ"     : "PZ",
    "PE"     : "E",
    "M"      : "M",
    "ETA"    : "ETA",
#    "VX"     : "VFASPF(VX)",
#    "VY"     : "VFASPF(VY)",
#    "VZ"     : "VFASPF(VZ)",


    "Lc_PE"  : "CHILD(E,1)",
    "Lc_PX"  : "CHILD(PX,1)",
    "Lc_PY"  : "CHILD(PY,1)",
    "Lc_PZ"  : "CHILD(PZ,1)",
    "Lc_M"   : "CHILD(M,1)",
    "Lc_ETA" : "CHILD(ETA,1)",
#    "Lc_VX"  : "CHILD(VFASPF(VX),1)",
#    "Lc_VY"  : "CHILD(VFASPF(VY),1)",
#    "Lc_VZ"  : "CHILD(VFASPF(VZ),1)",

    "D0_PE"  : "CHILD(E,2)",
    "D0_PX"  : "CHILD(PX,2)",
    "D0_PY"  : "CHILD(PY,2)",
    "D0_PZ"  : "CHILD(PZ,2)",
    "D0_M"   : "CHILD(M,2)",
    "D0_ETA" : "CHILD(ETA,2)",
#    "D0_VX"  : "CHILD(VFASPF(VX),2)",
#    "D0_VY"  : "CHILD(VFASPF(VY),2)",
#    "D0_VZ"  : "CHILD(VFASPF(VZ),2)",

    "K_PE"   : "CHILD(E,3)",
    "K_PX"   : "CHILD(PX,3)",
    "K_PY"   : "CHILD(PY,3)",
    "K_PZ"   : "CHILD(PZ,3)",
    "K_M"    : "CHILD(M,3)",
    "K_ETA"  : "CHILD(ETA,3)",
#    "K_VX"   : "CHILD(VFASPF(VX),3)",
#    "K_VY"   : "CHILD(VFASPF(VY),3)",
#    "K_VZ"   : "CHILD(VFASPF(VZ),3)",
}
# DTF with Lc and D0 mass constraint 
cdtf_doft = LoKi__Hybrid__DictOfFunctors("cdtf_functors", Variables=DTF_Vars)
cdtf_dt = LoKi__Hybrid__DTFDict("lcdDTF", constrainToOriginVertex=False, daughtersToConstrain=[
                                "Lambda_c+", "Lambda_c~-", "D0"], Source=cdtf_doft.getFullName())
cdtf_dt.addTool(cdtf_doft)
cdtf_tuple_tool = LoKi__Hybrid__Dict2Tuple(
    "cdtf_tuple", Source=cdtf_dt.getFullName(), NumVar=len(DTF_Vars))
cdtf_tuple_tool.addTool(cdtf_dt)
LbBranch.addTupleTool(cdtf_tuple_tool)

# DTF with D0 mass constraint 
d0dtf_doft = LoKi__Hybrid__DictOfFunctors(
    "d0dtf_functors", Variables=DTF_Vars)
d0dtf_dt = LoKi__Hybrid__DTFDict("dDTF", constrainToOriginVertex=False, daughtersToConstrain=[
                                 "D0"], Source=d0dtf_doft.getFullName())
d0dtf_dt.addTool(d0dtf_doft)
d0dtf_tuple_tool = LoKi__Hybrid__Dict2Tuple(
    "d0dtf_tuple", Source=d0dtf_dt.getFullName(), NumVar=len(DTF_Vars))
d0dtf_tuple_tool.addTool(d0dtf_dt)
LbBranch.addTupleTool(d0dtf_tuple_tool)
# some variables for detached decays
LbBranch.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolLb").Variables = dict(std_detached_vars(["Lc","D0","K"]),**std_bpv_vars())
LcBranch.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolLc").Variables = std_detached_vars(["p","Lc_K","Lc_pi"])
D0Branch.addTupleTool("LoKi::Hybrid::TupleTool/LoKiToolD0").Variables   = std_detached_vars(["D0_K","D0_pi"])
add_bdt_tpl(LcBranch, "Lc")
add_bdt_tpl(D0Branch, "D0")
# add stable_vars dict to all final state particles. we also want to know if we have downstream or long tracks
getattr(LcD0KTpl,"K").addTupleTool("LoKi::Hybrid::TupleTool/KLoKiTool").Variables          = std_stable_vars()
getattr(LcD0KTpl,"p").addTupleTool("LoKi::Hybrid::TupleTool/pLoKiTool").Variables          = std_stable_vars()
getattr(LcD0KTpl,"Lc_K").addTupleTool("LoKi::Hybrid::TupleTool/Lc_KLoKiTool").Variables    = std_stable_vars()
getattr(LcD0KTpl,"Lc_pi").addTupleTool("LoKi::Hybrid::TupleTool/Lc_piLoKiTool").Variables  = std_stable_vars()
getattr(LcD0KTpl,"D0_K").addTupleTool("LoKi::Hybrid::TupleTool/D0_KLoKiTool").Variables    = std_stable_vars()
getattr(LcD0KTpl,"D0_pi").addTupleTool("LoKi::Hybrid::TupleTool/D0_piLoKiTool").Variables  = std_stable_vars()

# add sequences
DaVinci().UserAlgorithms += [LcD0KSeq.sequence()]

if isMC:
    # MC truth tuple
    mc_tpl = MCDecayTreeTuple("Truth", TupleName="t", RevertToPositiveID=False, ToolList=mctools)
    mc_tpl.Decay = "[Lambda_b0 ==> ^(Lambda_c+ ==> ^p+ ^K- ^pi+) ^(D~0 ==> ^K+ ^pi-) ^K-]CC"
    mc_tpl.setDescriptorTemplate("[${Lb}(Lambda_b0 ==> ${Lc}(Lambda_c+ ==> ${p}p+ ${Lc_K}K- ${Lc_pi}pi+) ${D0}(D~0 ==> ${D0_K}K+ ${D0_pi}pi-) ${K}K-)]CC")
    DaVinci().UserAlgorithms += [mc_tpl]
