# $`\Lambda_b \to \Lambda_c D^0 K`$ ntuple production

## Creating/updating `info.yaml`

In order to add or remove samples for future productions, modify the `samples` dict in `generate_info_yml`.

`info.yaml` is generated from the script `generate_info_yml.py`. There is a `Makefile` to simplify this, so all you should need to do is call `make`. If using a really old version of Python 3 (e.g. the system-installed verion on lxplus) then you may have to run e.g.
```
lb-run default make
```

