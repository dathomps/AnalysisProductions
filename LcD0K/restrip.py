from Configurables import EventNodeKiller, ProcStatusCheck, StrippingTCK, DaVinci
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

# User configurables
stripping_version = {
    "2015": "24r2",
    "2016": "28r2",
    "2017": "29r2",
    "2018": "34",
}[DaVinci().DataType]

stripping_tck = {
    "2015": 0x44105242, # Use DaVinci v44r10p5
    "2016": 0x44105282, # Use DaVinci v44r10p5
    "2017": 0x42732920, # Use DaVinci v42r7p3
    "2018": 0x44703400, # Use DaVinci v44r7
}[DaVinci().DataType]

output_tes = "AllStreams" # Your candidates will be found in "/Event/{output_tes}/Phys/{stripping_line}". The output filename will be "{output_tes}.DST"
stripping_line_list = ["X2LcD0KD02KPiBeauty2CharmLine", "InclusiveCharmBaryons_LcLine"]

# Event node killer
event_node_killer = EventNodeKiller("StripKiller", Nodes=["/Event/AllStreams", "/Event/Strip"])

# Build streams
stripping_name = "stripping{}".format(stripping_version)
streams = buildStreams(stripping = strippingConfiguration(stripping_name), archive = strippingArchive(stripping_name))
custom_stream = StrippingStream(output_tes)
custom_lines = ["Stripping" + l for l in stripping_line_list]
for stream in streams:
    for sline in stream.lines:
        if sline.name() in custom_lines:
            custom_stream.appendLines([sline])
sc = StrippingConf(Streams=[custom_stream], MaxCandidates = 2000, AcceptBadEvents = False, BadEventSelection = ProcStatusCheck())
custom_stream.sequence().IgnoreFilterPassed = True # We want to keep all MC events to use for efficiency

# Configuration of SelDSTWriter
enablePacking = True

SelDSTWriterElements = {"default": stripDSTElements(pack = enablePacking)}
SelDSTWriterConf = {"default": stripDSTStreamConf(pack = enablePacking, selectiveRawEvent=False)}

# Make sure items that might get lost when running the CALO+PROTO ReProcessing in DV are present on full DST streams
SelDSTWriterConf["default"].extraItems += [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

dstWriter = SelDSTWriter("MyDSTWriter", StreamConf = SelDSTWriterConf, MicroDSTElements = SelDSTWriterElements, OutputFileSuffix = "000000",  SelectionSequences = sc.activeStreams())

# Add stripping TCK
stck = StrippingTCK(HDRLocation = "/Event/Strip/Phys/DecReports", TCK=stripping_tck)

# Configure DaVinci
DaVinci().ProductionType = "Stripping"
DaVinci().appendToMainSequence([event_node_killer, sc.sequence(), stck, dstWriter.sequence()])

