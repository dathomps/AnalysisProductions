# Imports for LoKi functors
from LoKiPhys.decorators import *
from LoKiArrayFunctors.decorators import *
from LoKiProtoParticles.decorators import *

def getMVAVars(parentname, daugthers):
  """
  Return all variables required for the BDT
  Variable names MUST correspond exactly to what is needed by classifier (xml)
  """

  bdt_vars = {}
  # Variables for D and daughters;  prefixes added later
  vars_parent = {
    'log_P'              : 'log10(P)',
    'log_PT'             : 'log10(PT)',
    'log_ENDVERTEX_CHI2' : 'log10(VFASPF(VCHI2))',
    'log_IPCHI2_OWNPV'   : 'log10(MIPCHI2DV(PRIMARY))',
    'log_FDCHI2_OWNPV'   : 'log10(BPVVDCHI2)',
     # we need a hack here, since decay descriptors don't always have the leading particle at the first position
     # the hack is that you order the daughters dict such, that the leading particle is at first position even though it might be 2nd or 3rd child in the decay descriptor
    'beta'               : '(SUMTREE(P,ISBASIC,0.)-(2.*CHILD(P,{0})))/SUMTREE(P,ISBASIC,0.)'.format(daugthers.items()[0][1]),
  }
  vars_daughters = {
    'log_PT'                 : 'log10(CHILD(PT,{0}))',
    'log_IPCHI2_OWNPV'       : 'log10(CHILD(MIPCHI2DV(PRIMARY),{0}))',
    'log_TRACK_VeloCHI2NDOF' : 'log10(switch(CHILD(TINFO(LHCb.Track.FitVeloNDoF,-1),{0})>0,CHILD(TINFO(LHCb.Track.FitVeloChi2,-1),{0})/CHILD(TINFO(LHCb.Track.FitVeloNDoF,-1),{0}),-1))',
    'log_TRACK_TCHI2NDOF'    : 'log10(switch(CHILD(TINFO(LHCb.Track.FitTNDoF,-1),{0})>0,CHILD(TINFO(LHCb.Track.FitTChi2,-1),{0})/CHILD(TINFO(LHCb.Track.FitTNDoF,-1),{0}),-1))',
    'log_TRACK_MatchCHI2'    : 'log10(CHILD(TINFO(LHCb.Track.FitMatchChi2,-1.),{0}))',
    'log_TRACK_GhostProb'    : 'log10(CHILD(TRGHOSTPROB,{0}))',
    'UsedRichAerogel'        : 'switch(CHILDCUT(PPCUT(PP_USEDAEROGEL),{0}),1,0)',
    'UsedRich1Gas'           : 'switch(CHILDCUT(PPCUT(PP_USEDRICH1GAS),{0}),1,0)',
    'UsedRich2Gas'           : 'switch(CHILDCUT(PPCUT(PP_USEDRICH2GAS),{0}),1,0)',
    'RichAbovePiThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_PI),{0}),1,0)',
    'RichAboveKaThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_K),{0}),1,0)',
    'RichAbovePrThres'       : 'switch(CHILDCUT(PPCUT(PP_RICHTHRES_P),{0}),1,0)',
    'atan_RichDLLe'          : 'atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLe,-1000),{0}))',
    'atan_RichDLLmu'         : 'atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000),{0}))',
    'atan_RichDLLk'          : 'atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLk,-1000),{0}))',
    'atan_RichDLLp'          : 'atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLp,-1000),{0}))',
    'atan_RichDLLbt'         : 'atan(CHILD(PPINFO(LHCb.ProtoParticle.RichDLLbt,-1000),{0}))',
    'atan_MuonLLbg'          : 'atan(switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonBkgLL,-10000.),{0}),-1000.))',
    'atan_MuonLLmu'          : 'atan(switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonMuLL,-10000.),{0}),-1000.))',
    'isMuon'                 : 'switch(CHILDCUT(ISMUON,{0}),1,0)',
    'MuonNShared'            : 'switch(CHILD(PPINFO(LHCb.ProtoParticle.InAccMuon,0),{0})==1,CHILD(PPINFO(LHCb.ProtoParticle.MuonNShared,0),{0}),-1)',
    'VeloCharge'             : 'CHILD(PPINFO(LHCb.ProtoParticle.VeloCharge,-1000.),{0})',
  }
  # Add all parent variables to output
  for var, loki in vars_parent.iteritems():
    bdt_vars.update({'{}_{}'.format(parentname, var) : loki})
  # Add all daughter variables to output
  for daugthername, lab in daugthers.iteritems():
    for var, loki in vars_daughters.iteritems():
      bdt_vars.update({'{}_{}_{}'.format(parentname,daugthername, var) : loki.format(lab)})
  # Print out variables for sanity
  #for key in sorted(bdt_vars):
  #    print '{:<25} : {}'.format(key, bdt_vars[key].replace(',', ', '))
  #print 80 * '-'
  return bdt_vars
