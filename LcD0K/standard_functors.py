"""
  Collection of functor dicts to import to your DV script
  @author Marian Stahl marian.stahl@cern.ch
"""
from string import Template
import re

def parse_descriptor_template(template):
  """
    Turns descriptor templates into something we can stick into a functor. Taken from https://gitlab.cern.ch/lhcb/Analysis/-/blob/67907622a1b151cc23bb22b17b368e44405be954/Phys/DecayTreeTuple/python/DecayTreeTuple/Configuration.py
    Returns:
      dict of branchname and decay descriptor
  """
  dd = Template(template)
  particles = [y[1] if len(y[1]) else y[2] for y in dd.pattern.findall(dd.template) if len(y[1]) or len(y[2])]
  # the next line is a bit different from the original code, since we don't want to add a name and branch for the head explicitly and leave it away in the first place
  mapping = {p: '^' for p in particles}
  clean = dd.template.replace(' ', '')
  for i, o in enumerate(re.findall("(\[\$|\$)", clean)):
    if o == '[$': mapping[particles[i]] = ''
  branches = {}
  for p in particles:
    branches[p] = dd.substitute({q: mapping[p] if p == q else '' for q in particles})
  return branches

def std_event_vars():
  """
    To be used with LoKi__Hybrid__EvtTupleTool
    Returns:
      dict of functors including number of PVs, tracks (different types of) and hits in various subdetector systems
  """
  return {"nPV"     : "RECSUMMARY(LHCb.RecSummary.nPVs,-9999)",
          "nLong"   : "RECSUMMARY(LHCb.RecSummary.nLongTracks,-9999)",
          "nDown"   : "RECSUMMARY(LHCb.RecSummary.nDownstreamTracks,-9999)",
          "nUp"     : "RECSUMMARY(LHCb.RecSummary.nUpstreamTracks,-9999)",
          "nVelo"   : "RECSUMMARY(LHCb.RecSummary.nVeloTracks,-9999)",
          "nTT"     : "RECSUMMARY(LHCb.RecSummary.nTTracks,-9999)",
          "nBack"   : "RECSUMMARY(LHCb.RecSummary.nBackTracks,-9999)",
          "nMuon"   : "RECSUMMARY(LHCb.RecSummary.nMuonTracks,-9999)",
          "nTracks" : "RECSUMMARY(LHCb.RecSummary.nTracks,-9999)",
          "hRich1"  : "RECSUMMARY(LHCb.RecSummary.nRich1Hits,-9999)",
          "hRich2"  : "RECSUMMARY(LHCb.RecSummary.nRich2Hits,-9999)",
          "hVelo"   : "RECSUMMARY(LHCb.RecSummary.nVeloClusters,-9999)",
          "hIT"     : "RECSUMMARY(LHCb.RecSummary.nITClusters,-9999)",
          "hTT"     : "RECSUMMARY(LHCb.RecSummary.nTTClusters,-9999)",
          "hOT"     : "RECSUMMARY(LHCb.RecSummary.nOTClusters,-9999)",
          "hSPD"    : "RECSUMMARY(LHCb.RecSummary.nSPDhits,-9999)"}

def std_detached_vars(final_state_particles):
  """
    Functors for (end-)vertex position, chi2/ndof and covariances.
    Functors for IP(CHI2) and FD(CHI2) w.r.t. the best PV.
    Functors for DOCAs and impact parameters of all the children's track trajectories w.r.t. the decay vertex of the parent.
    Note: with that info, DIRAs and their uncertainties can be calculated offline
    Args:
      final_state_particles (list): names of (direct) children as given in the DecayTreeTuple. The order matters and should be as in the decay descriptor!
    Returns:
      dict of functors
  """
  vars = {"DOCACHI2MAX"  : "DOCACHI2MAX",
          "BPVIP"        : "BPVIP()",
          "BPVIPCHI2"    : "BPVIPCHI2()",
          "BPVVD"        : "BPVVD",
          "BPVVDCHI2"    : "BPVVDCHI2",
          "VX"           : "VFASPF(VX)",
          "VY"           : "VFASPF(VY)",
          "VZ"           : "VFASPF(VZ)",
          "VCOV_XX"      : "VFASPF(VCOV2(0,0))",
          "VCOV_XY"      : "VFASPF(VCOV2(0,1))",
          "VCOV_XZ"      : "VFASPF(VCOV2(0,2))",
          "VCOV_YY"      : "VFASPF(VCOV2(1,1))",
          "VCOV_YZ"      : "VFASPF(VCOV2(1,2))",
          "VCOV_ZZ"      : "VFASPF(VCOV2(2,2))",
          "CHI2VXNDF"    : "CHI2VXNDF"}
  for i in range(1,len(final_state_particles)+1):
    vars[final_state_particles[i-1]+"_IP"]     = "CHILDIP({})".format(i)
    vars[final_state_particles[i-1]+"_IPCHI2"] = "CHILDIPCHI2({})".format(i)
    for j in range(i+1,len(final_state_particles)+1):
      vars[final_state_particles[i-1]+"_"+final_state_particles[j-1]+"_DOCA"] = "DOCA({0},{1})".format(i,j)
      vars[final_state_particles[i-1]+"_"+final_state_particles[j-1]+"_DOCACHI2"] = "DOCACHI2({0},{1})".format(i,j)
  return vars

def std_lorentz_vars(descriptor_template):
  """
    Functors for four vectors (E,PT,ETA,PHI) of all particles in the descriptor template.
    Args:
      particles (list): names of (direct) children as given in the DecayTreeTuple. The order matters and should be as in the decay descriptor!
    Returns:
      dict of functors
  """
  vars = {"E":"E", "PT":"PT", "ETA":"ETA", "PHI":"PHI"}
  def add_variables(branches):
    for k, v in branches.items() :
      vars[k+"_E"]   = "CHILD(E,'{}')".format(str(v))
      vars[k+"_PT"]  = "CHILD(PT,'{}')".format(str(v))
      vars[k+"_ETA"] = "CHILD(ETA,'{}')".format(str(v))
      vars[k+"_PHI"] = "CHILD(PHI,'{}')".format(str(v))
    return
  if(isinstance(descriptor_template, str)):
    add_variables(parse_descriptor_template(descriptor_template))
  elif(isinstance(descriptor_template, dict)):
    add_variables(descriptor_template)
  else : print("ERROR in std_lorentz_vars: that shouldn't happen")
  return vars

def std_bpv_vars():
  """
    Functors for BPV position and covariances.
    Returns:
      dict of functors
  """
  return {"BPVX"      : "BPV(VX)",
          "BPVY"      : "BPV(VY)",
          "BPVZ"      : "BPV(VZ)",
          "BPVCOV_XX" : "BPV(VCOV2(0,0))",
          "BPVCOV_XY" : "BPV(VCOV2(0,1))",
          "BPVCOV_XZ" : "BPV(VCOV2(0,2))",
          "BPVCOV_YY" : "BPV(VCOV2(1,1))",
          "BPVCOV_YZ" : "BPV(VCOV2(1,2))",
          "BPVCOV_ZZ" : "BPV(VCOV2(2,2))"}

def std_stable_vars():
  """
    Functors for stable particles in the decay chain. Useful for training MVAs
    Returns:
      dict of functors
  """
  return {"MIPCHI2"                : "MIPCHI2DV(PRIMARY)",
          "MIP"                    : "MIPDV(PRIMARY)",
          "KEY"                    : "KEY",
          "log_TRACK_VeloCHI2NDOF" : "log10(switch(TINFO(LHCb.Track.FitVeloNDoF,-1)>0,TINFO(LHCb.Track.FitVeloChi2,-1)/TINFO(LHCb.Track.FitVeloNDoF,-1),-1))",
          "log_TRACK_TCHI2NDOF"    : "log10(switch(TINFO(LHCb.Track.FitTNDoF,-1)>0,TINFO(LHCb.Track.FitTChi2,-1)/TINFO(LHCb.Track.FitTNDoF,-1),-1))",
          "log_TRACK_MatchCHI2"    : "log10(TINFO(LHCb.Track.FitMatchChi2,-1.))",
          "log_TRACK_GhostProb"    : "log10(TRGHOSTPROB)",
          "atan_RichDLLe"          : "atan(PPINFO(LHCb.ProtoParticle.RichDLLe,-1000))",
          "atan_RichDLLmu"         : "atan(PPINFO(LHCb.ProtoParticle.RichDLLmu,-1000))",
          "atan_RichDLLk"          : "atan(PPINFO(LHCb.ProtoParticle.RichDLLk,-1000))",
          "atan_RichDLLp"          : "atan(PPINFO(LHCb.ProtoParticle.RichDLLp,-1000))",
          "atan_RichDLLbt"         : "atan(PPINFO(LHCb.ProtoParticle.RichDLLbt,-1000))",
          "atan_MuonLLbg"          : "atan(switch(PPINFO(LHCb.ProtoParticle.InAccMuon,0)==1,PPINFO(LHCb.ProtoParticle.MuonBkgLL,-10000),-1000))",
          "atan_MuonLLmu"          : "atan(switch(PPINFO(LHCb.ProtoParticle.InAccMuon,0)==1,PPINFO(LHCb.ProtoParticle.MuonMuLL,-10000),-1000))",
          "MuonNShared"            : "switch(PPINFO(LHCb.ProtoParticle.InAccMuon,0)==1,PPINFO(LHCb.ProtoParticle.MuonNShared,0),-1)",
          "VeloCharge"             : "PPINFO(LHCb.ProtoParticle.VeloCharge,-1000)"}
