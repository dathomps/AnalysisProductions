import re

def get_year(path):
    find = get_match(r"/(Collision|20)(\d\d)/", 2)
    match = find(path)
    if match is None:
        raise RuntimeError(f"Year not found in path {path}")
    return match

def get_stripping_version(path):
    find = get_match(r"Stripping([\drp]+)")
    match = find(path)
    if match is None:
        raise RuntimeError(f"Stripping version not found in path {path}")
    return match

def get_polarity(path):
    find = get_match(r"Mag([DownUp]+)")
    match = find(path)
    if match is None:
        raise RuntimeError(f"Polarity not found in path {path}")
    return match

def get_match(expr, groupno = 1):
    return lambda path: re.search(expr, path).group(groupno)

stripping_dv_versions = {
    "21r1"  : "v36r1p5",
    "21r1p1": "v39r1p1",
    "21r1p2": "v39r1p6",
    "21"    : "v36r1p5",
    "21r0p1": "v39r1p1",
    "21r0p2": "v39r1p6",
    "24r2"  : "v44r10p5",
    "28r2"  : "v44r10p5",
    "29r2"  : "v42r7p3",
    "29r2p1": "v42r9p2",
    "34"    : "v44r7",
    "34r0p1": "v44r10p2",
}

samples = {
    "90000000" : {
        "name" : "data",
        "paths" : [
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/BHADRON.MDST",
            "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/BHADRON.MDST",
            "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/BHADRON.MDST",
            "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/BHADRON.MDST",
            "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST",
            "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST",
            "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST",
            "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST",
        ],
    },
    "15396200" : {
        "name" : "LcDst2D0gK",
        "paths" : [
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396200/ALLSTREAMS.DST",
        ],
    },
    "15396400" : {
        "name" : "LcDst2D0piK",
        "paths" : [
            "/MC/2015/Beam6500GeV-2015-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2016/Beam6500GeV-2016-MagUp-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/Reco16/Turbo03/Stripping28r1NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2017/Beam6500GeV-2017-MagUp-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2017/Beam6500GeV-2017-MagDown-Nu1.6-25ns-Pythia8/Sim09e/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2018/Beam6500GeV-2018-MagDown-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
            "/MC/2018/Beam6500GeV-2018-MagUp-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/15396400/ALLSTREAMS.DST",
        ],
    },
}

data_stripping = {}
for path in samples["90000000"]["paths"][::2]:
    data_stripping[get_year(path)] = get_stripping_version(path)

from collections import OrderedDict

job_dict = {
    "defaults": {
        "application": "DaVinci/v45r7",
        "wg": "BandQ",
        "automatically_configure": True,
        "inform": [
            "mindaugas.sarpis@cern.ch",
        ],
    }
}

def restripping_job(job_input, stripping):
    dv_ver = stripping_dv_versions[stripping]
    return {"input": job_input, "output": "AllStreams.DST", "options": ["restrip.py"], "application": f"DaVinci/{dv_ver}"}

def ntupling_job(job_input, is_mc):
    return {"input": job_input, "output": "DVNtuple.root", "options": ["ntuple.py"]}

for evttype in samples:
    is_mc = evttype != "90000000"
    name = samples[evttype]["name"]
    for path in samples[evttype]["paths"]:
        year = get_year(path)
        polarity = get_polarity(path)
        stripping = get_stripping_version(path)
        if is_mc and stripping != data_stripping[year] and "Filtered" not in path:
            job_name = "_".join([name, "20"+year, "Mag"+polarity, "S"+stripping, "to", data_stripping[year], "restripping"])
            job_dict[job_name] = restripping_job({"bk_query": path}, data_stripping[year])
            job_input = {"job_name": job_name}
        else:
            job_input = {"bk_query": path}
        job_name = "_".join([name, "20"+year, "Mag"+polarity, "ntupling"])
        job_dict[job_name] = ntupling_job(job_input, is_mc)

import yaml
with open("info.yaml", "w") as f:
    f.write(f"# This file is automatically generated. Changes made here will be over-written. Please edit {__file__} (see README.md).\n")
    yaml.dump(job_dict, f, default_flow_style=False, sort_keys=False)

