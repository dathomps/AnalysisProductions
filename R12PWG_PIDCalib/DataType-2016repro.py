from Configurables import DaVinci
import os

DaVinci().DataType = '2016'

from Configurables import SWeightsTableFiles
#SWeightsTableFiles(sTableMagUpFile  = 'sTables/sPlotTables-2016MagUpRepro.root',
#                   sTableMagDownFile= 'sTables/sPlotTables-2016MagDownRepro.root')
os.environ["PIDCALIBROOT"] = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/WG/PIDCalib/v9r5/"
SWeightsTableFiles(sTableMagUpFile  = os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2016MagUpRepro.root',
                   sTableMagDownFile= os.environ['PIDCALIBROOT'] + '/sTables/sPlotTables-2016MagDownRepro.root')
