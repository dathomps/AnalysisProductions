# Options for reprocessing of the 2016 PIDCalib tuples (2021 campaign) with the fix for the failing isolation tool.

We run Castelao over the updated options to reprocess the PIDCalib samples for the 2016 dataset.
The options are copied from here: https://gitlab.cern.ch/lhcb-datapkg/WG/PIDCalib/-/tree/v9r5/scriptsR2

We split the production for each magnet polarity in three productions:
- the production for Jpsi_nopt muon sample // makeTuples_pp_2016_reprocessing2021_Jpsinopt.py
- the production for all proton samples // makeTuples_pp_2016_reprocessing2021_protons.py
- the production for all the rest (kaons, pions, remaining muons). // makeTuples_pp_2016_reprocessing2021_therest.py 
The names of the option files are self-explanatory.
