from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci

DaVinci().TupleFile       = "PSITOJPSIPIPI.ROOT"
DaVinci().Simulation      = True

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from PsiToJpsiPiPi.XPsi import configure_selection
DaVinci().appendToMainSequence( configure_selection( part="X_1(3872)" ) )