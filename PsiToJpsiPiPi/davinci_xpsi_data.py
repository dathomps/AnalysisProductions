from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import DaVinci, GaudiSequencer

from PhysConf.Filters import LoKi_Filters

from Configurables import CondDB

fltrs = LoKi_Filters( 
    HLT2_Code = " HLT_PASS_RE ('Hlt2diPhotonDiMuonDecision') | HLT_PASS_RE ('Hlt2LowMultMuonDecision') | HLT_PASS_RE ('Hlt2LowMultDiMuonDecision') "
)
#DaVinci().EventPreFilters = [fltrs.sequence("Filters")]
DaVinci().Lumi            = True
DaVinci().TupleFile       = "PSITOJPSIPIPI.ROOT"

from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
from PsiToJpsiPiPi.XPsi import configure_selection

myFilteredSequence = GaudiSequencer("DansFilteredDVsequence") # GaudiSequencer's are "AND" by default (DaVinci main sequence is non-standard)
myFilteredSequence.Members += [fltrs.sequence("Filters")]
myFilteredSequence.Members += configure_selection()
DaVinci().UserAlgorithms = [myFilteredSequence]

db = CondDB  ( LatestGlobalTagByDataType = DaVinci().DataType )
db.Tags["DQFLAGS"] = "herschel"
