def configure_selection( part = "X_1(3872)", finalState = "DiMuonDiPion" ) :

    from Configurables import DaVinci

    from Configurables import TrackSmearState
    from Configurables import TrackScaleState

    from Configurables import HCRawBankDecoder
    from Configurables import HCDigitCorrector

    # Do the selection
    from StandardParticles import StdAllNoPIDsPions, StdAllNoPIDsMuons, StdAllLooseMuons
    _stdNoPIDsMuons = StdAllNoPIDsMuons
    _stdLooseMuons = StdAllLooseMuons
    _stdNoPIDsPions = StdAllNoPIDsPions


    l0List = [ 'Muon,lowMult','DiMuon,lowMult'  ]
    hlt1List = [ 'NoPVPassThrough' ]
    hlt2List = [ 'LowMultMuon','diPhotonDiMuon','LowMultDiMuon' ]

    triggerList = []
    for trigger in l0List:
        triggerList.append( 'L0' + trigger + 'Decision')
    for trigger in hlt1List:
        triggerList.append( 'Hlt1' + trigger + 'Decision')
    for trigger in hlt2List:
        triggerList.append( 'Hlt2' + trigger + 'Decision')

    from Configurables import EventTuple
    from Configurables import MCDecayTreeTuple,CombineParticles,DecayTreeTuple,FitDecayTrees,TupleToolRecoStats,TupleToolTrigger,TupleToolRecoStats,TupleToolProtoPData
    from Configurables import TupleToolStripping,TupleToolTISTOS,TupleToolMCTruth,MCTupleToolKinematic,MCTupleToolHierarchy,TupleToolMCTruth,TupleToolDecay
    from Configurables import TupleToolAllVeloTracks
    MCTuple = MCDecayTreeTuple("MCTuple")

    if part == 'X_1(3872)':
        MCTuple.Decay =part + ' -->  ^(  J/psi(1S) ==>  ^mu+ ^mu- ) ^( rho(770)0 ==> ^pi+ ^pi- )'
        MCTuple.Branches= {
            'Jpsi' : part + ' -->  ^(  J/psi(1S) ==>  mu+ mu- ) ( rho(770)0 ==> pi+ pi- )',
            'rho0': part + ' -->  (  J/psi(1S) ==>  mu+ mu- ) ^( rho(770)0 ==> pi+ pi- )',
            'piplus' : part + ' -->  (  J/psi(1S) ==>  mu+ mu- ) ( rho(770)0 ==> ^pi+ pi- )',
            'piminus': part + ' -->  (  J/psi(1S) ==>  mu+ mu- ) ( rho(770)0 ==> pi+ ^pi- )',
            'muplus' : part + ' -->  (  J/psi(1S) ==>  ^mu+ mu- ) ( rho(770)0 ==> pi+ pi- )',
            'muminus': part + ' -->  (  J/psi(1S) ==>  mu+ ^mu- ) ( rho(770)0 ==> pi+ pi- )'}
    else: # This includes the psi(2S) which goes not to J/psi rho but J/psi pi pi
        if finalState == 'DiMuon':
            MCTuple.Decay =part + ' ==> ^mu+ ^mu-'
            MCTuple.Branches= {
                'muplus' : part + ' ==>  ^mu+ mu- ',
                'muminus': part + ' ==>  mu+ ^mu- '}
        elif finalState == 'DiMuonDiPion':
            MCTuple.Decay =part + ' ==x>  ^(  J/psi(1S) ==>  ^mu+ ^mu- ) ^pi+ ^pi-'
            MCTuple.Branches= {
                'Jpsi' : part + ' ==x>  ^(  J/psi(1S) ==>  mu+ mu- ) pi+ pi- ',
                'piplus' : part + ' ==x>  (  J/psi(1S) ==>  mu+ mu- ) ^pi+ pi- ',
                'piminus': part + ' ==x>  (  J/psi(1S) ==>  mu+ mu- ) pi+ ^pi- ',
                'muplus' : part + ' ==x>  (  J/psi(1S) ==>  ^mu+ mu- ) pi+ pi- ',
                'muminus': part + ' ==x>  (  J/psi(1S) ==>  mu+ ^mu- ) pi+ pi- '}
            
    MCTuple.ToolList = [ "TupleToolEventInfo", "MCTupleToolReconstructed"  , "MCTupleToolHierarchy" , "MCTupleToolKinematic" ]

    from PhysSelPython.Wrappers import Selection,SelectionSequence
    from Configurables import LoKi__Hybrid__TupleTool
    LoKiTool = LoKi__Hybrid__TupleTool('LoKiTool')

    myJpsi = "Jpsimumu"
    _JpsimumuDauCuts = { "mu-": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)", "mu+": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)" }
    _JpsimumuCombCut = "AM>0"
    _Jpsimumu = CombineParticles( myJpsi ,  DecayDescriptor = "J/psi(1S) -> mu- mu+",    DaughtersCuts = _JpsimumuDauCuts, CombinationCut = _JpsimumuCombCut , MotherCut = 'P>0.'   )
    if DaVinci().Simulation :
        Jpsimumu = Selection ( "Sel"+myJpsi,  Algorithm = _Jpsimumu,  RequiredSelections = [_stdNoPIDsMuons]    )
    else:
        Jpsimumu = Selection ( "Sel"+myJpsi,  Algorithm = _Jpsimumu,  RequiredSelections = [_stdLooseMuons]    )

    myRho = "rhopipi"
    _RhopipiDauCuts = { "pi-": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)", "pi+": "(PT>0.*MeV) & (TRCHI2DOF<5) & (ISLONG)" }
    _RhopipiCombCut = "AM>0."
    _Rhopipi = CombineParticles( myRho ,  DecayDescriptor = "rho(770)0 -> pi- pi+",    DaughtersCuts = _RhopipiDauCuts, CombinationCut = _RhopipiCombCut , MotherCut = 'P>0.'   )
    Rhopipi = Selection ( "Sel"+myRho,  Algorithm = _Rhopipi,  RequiredSelections = [_stdNoPIDsPions]    )

    myX = 'X2Jpsipipi_Unfitted'
    _XJpsipipiDauCuts = { "J/psi(1S)": "(PT>0.*MeV) & (ADMASS('J/psi(1S)') < 2000*MeV )",   "rho(770)0": "(PT>0.*MeV) "}
    _XJpsipipi = CombineParticles( myX,   DecayDescriptor = "X_1(3872) -> J/psi(1S) rho(770)0",  DaughtersCuts = _XJpsipipiDauCuts  , CombinationCut = 'AP>0.' , MotherCut = 'P>0.'  )   
    XJpsipipi = Selection("Sel"+myX, Algorithm = _XJpsipipi, RequiredSelections=[Jpsimumu,Rhopipi]  )

    myAlg = FitDecayTrees ( 'XFitter', Code = "DECTREE('X_1(3872) -> J/psi(1S) rho(770)0')" ,  MaxChi2PerDoF = 10, MassConstraints = [ 'J/psi(1S)' ] )
    myXFitted =  Selection('XFitted',Algorithm=myAlg, RequiredSelections=[XJpsipipi])


    XJpsipipiOfflineTree = DecayTreeTuple('TupleXJpsipipi')
    XJpsipipiOfflineTree.Decay = "(X_1(3872)  -> ^( J/psi(1S) -> ^mu+ ^mu-)  ^( rho(770)0 -> ^pi+ ^pi- ))"
    XJpsipipiOfflineTree.Branches = {
        "Jpsi"    :    "X_1(3872) -> ^(  J/psi(1S) ->  mu+  mu- ) ( rho(770)0 -> pi+ pi- )",
        "rho0"    :    "X_1(3872) ->  (  J/psi(1S) ->  mu+  mu- ) ^( rho(770)0 -> pi+ pi- )",
        "piplus"  :    "X_1(3872) ->  (  J/psi(1S) ->  mu+  mu- ) ( rho(770)0 -> ^pi+ pi- )",
        "piminus" :    "X_1(3872) ->  (  J/psi(1S) ->  mu+  mu- ) ( rho(770)0 -> pi+ ^pi- )",
        "muplus"  :    "X_1(3872) ->  (  J/psi(1S) -> ^mu+  mu- ) ( rho(770)0 -> pi+ pi- )",
        "muminus" :    "X_1(3872) ->  (  J/psi(1S) ->  mu+ ^mu- ) ( rho(770)0 -> pi+ pi- )",
        "X"       :    " ^(X_1(3872) ->  (  J/psi(1S) ->  mu+ mu- ) ( rho(770)0 -> pi+ pi- ))"
        }

    XJpsipipiOfflineTree.ToolList = [ "TupleToolKinematic",
                                        "TupleToolGeometry",
                                        "TupleToolPid",
                                        "TupleToolEventInfo", 
                                        "TupleToolTrackInfo",
                                        "TupleToolParticleStats",
                                        "TupleToolProtoPData",
                                        "TupleToolRecoStats",
                                        "TupleToolTrigger",
                                        "TupleToolVeloTrackClusterInfo",
                                        # "TupleToolVELOClusters",
                                        "TupleToolAllVeloTracks",
                                        "TupleToolAllPhotons",
                                        "TupleToolTISTOS"
                                        ]
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        XJpsipipiOfflineTree.ToolList += ["TupleToolStripping"]
    if not DaVinci().Simulation and DaVinci().DataType in ['2015','2016','2017','2018'] :
        XJpsipipiOfflineTree.ToolList += ["TupleToolHerschel"]
    if not DaVinci().Simulation and DaVinci().DataType in ['2015','2016','2017','2018'] :
        from Configurables import TupleToolHerschel
        XJpsipipiOfflineTree.addTool(TupleToolHerschel, name='tth')
        if DaVinci().DataType == '2015' :
            XJpsipipiOfflineTree.tth.DigitsLocation="Raw/HC/CorrectedDigits"
        else :
            XJpsipipiOfflineTree.tth.DigitsLocation="Raw/HC/Digits"
    if DaVinci().Simulation :
        XJpsipipiOfflineTree.ToolList += ["TupleToolMCTruth"]
    XJpsipipiOfflineTree.Inputs = [XJpsipipi.outputLocation()]
    XJpsipipiOfflineTree.addTool(LoKiTool, name="LoKiTool")
    XJpsipipiOfflineTree.LoKiTool.Variables={
        "cosTheta1" : "LV01",
        "cosTheta2" : "LV02",
        "chi2pdof"  : "VFASPF(VCHI2PDOF)"
        }
    XJpsipipiOfflineTree.addTool(TupleToolProtoPData)
    XJpsipipiOfflineTree.TupleToolProtoPData.DataList = ['InAccMuon']
    XJpsipipiOfflineTree.addTool(TupleToolRecoStats)
    XJpsipipiOfflineTree.TupleToolRecoStats.Verbose = True
    XJpsipipiOfflineTree.addTool(TupleToolTrigger)
    XJpsipipiOfflineTree.TupleToolTrigger.Verbose=True
    XJpsipipiOfflineTree.TupleToolTrigger.TriggerList= triggerList
    XJpsipipiOfflineTree.addTool(TupleToolTISTOS)
    XJpsipipiOfflineTree.TupleToolTISTOS.Verbose=True
    XJpsipipiOfflineTree.TupleToolTISTOS.TriggerList= triggerList
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        XJpsipipiOfflineTree.addTool(TupleToolStripping)
        XJpsipipiOfflineTree.TupleToolStripping.Verbose=True
        XJpsipipiOfflineTree.TupleToolStripping.StrippingList= ["StrippingLowMultPP2PPMuMuLineDecision","StrippingLowMultMuonLineDecision"]
    XJpsipipiOfflineTree.addTool(TupleToolAllVeloTracks)
    XJpsipipiOfflineTree.TupleToolAllVeloTracks.Location = 'Rec/Track/Best'

    XJpsipipiOfflineTree.addTool (TupleToolDecay, name = "X")
    XJpsipipiOfflineTree.X.InheritTools = True
    XJpsipipiOfflineTree.X.ToolList+=["LoKi::Hybrid::TupleTool/DTF_tool"]
    XJpsipipiOfflineTree.X.addTool(LoKi__Hybrid__TupleTool, name="DTF_tool")
    XJpsipipiOfflineTree.X.DTF_tool.Variables={
        "Mass_JpsiConstr" : "DTF_FUN(M, False, 'J/psi(1S)')", 
        "PT_JpsiConstr" : "DTF_FUN(PT, False, 'J/psi(1S)')", 
        "Chi2_JpsiConstr" : "DTF_CHI2NDOF(False, 'J/psi(1S)')"
        } 

    if DaVinci().Simulation :
        XJpsipipiOfflineTree.addTool(TupleToolMCTruth)
        XJpsipipiOfflineTree.TupleToolMCTruth.ToolList = [ "MCTupleToolReconstructed"  , "MCTupleToolKinematic" ]
        XJpsipipiOfflineTree.TupleToolMCTruth.addTool(MCTupleToolKinematic)
        XJpsipipiOfflineTree.TupleToolMCTruth.addTool(MCTupleToolHierarchy)

    seqX = SelectionSequence("Seq"+'XFitted', TopSelection = myXFitted , PostSelectionAlgs = [XJpsipipiOfflineTree]  )

    MuMuOfflineTree = DecayTreeTuple('TupleMuMu')
    MuMuOfflineTree.Decay = "( J/psi(1S) -> ^mu+ ^mu-)"
    MuMuOfflineTree.Branches = {
        "muplus"  :    "J/psi(1S) -> ^mu+  mu-",
        "muminus" :    "J/psi(1S) ->  mu+ ^mu-",
        "DiMu"    :    " ^(J/psi(1S) -> mu+ mu-)"
        }

    MuMuOfflineTree.ToolList = [ "TupleToolKinematic",
                                        "TupleToolGeometry",
                                        "TupleToolPid",
                                        "TupleToolEventInfo", 
                                        "TupleToolTrackInfo",
                                        "TupleToolParticleStats",
                                        "TupleToolProtoPData",
                                        "TupleToolRecoStats",
                                        "TupleToolTrigger",
                                        "TupleToolVeloTrackClusterInfo",
                                        # "TupleToolVELOClusters",
                                        "TupleToolAllVeloTracks",
                                        "TupleToolAllPhotons",
                                        "TupleToolTISTOS"
                                        ]
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        MuMuOfflineTree.ToolList += ["TupleToolStripping"]
    if DaVinci().Simulation :
        MuMuOfflineTree.ToolList += ["TupleToolMCTruth"]
    if not DaVinci().Simulation and DaVinci().DataType in ['2015','2016','2017','2018'] :
        MuMuOfflineTree.ToolList += ["TupleToolHerschel"]
    MuMuOfflineTree.Inputs = [Jpsimumu.outputLocation()]
    MuMuOfflineTree.addTool(LoKiTool, name="LoKiTool")
    MuMuOfflineTree.LoKiTool.Variables={
        "cosTheta1" : "LV01",
        "cosTheta2" : "LV02",
        "chi2pdof"  : "VFASPF(VCHI2PDOF)"
        }
    MuMuOfflineTree.addTool(TupleToolProtoPData)
    MuMuOfflineTree.TupleToolProtoPData.DataList = ['InAccMuon']
    MuMuOfflineTree.addTool(TupleToolRecoStats)
    MuMuOfflineTree.TupleToolRecoStats.Verbose = True
    MuMuOfflineTree.addTool(TupleToolTrigger)
    MuMuOfflineTree.TupleToolTrigger.Verbose=True
    MuMuOfflineTree.TupleToolTrigger.TriggerList= triggerList
    MuMuOfflineTree.addTool(TupleToolTISTOS)
    MuMuOfflineTree.TupleToolTISTOS.Verbose=True
    MuMuOfflineTree.TupleToolTISTOS.TriggerList= triggerList
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        MuMuOfflineTree.addTool(TupleToolStripping)
        MuMuOfflineTree.TupleToolStripping.Verbose=True
        MuMuOfflineTree.TupleToolStripping.StrippingList= ["StrippingLowMultPP2PPMuMuLineDecision","StrippingLowMultMuonLineDecision"]
    MuMuOfflineTree.addTool(TupleToolAllVeloTracks)
    MuMuOfflineTree.TupleToolAllVeloTracks.Location = 'Rec/Track/Best'
    if DaVinci().Simulation :
        MuMuOfflineTree.addTool(TupleToolMCTruth)


    MuMuOfflineTree.addTool (TupleToolDecay, name = "DiMu")
    MuMuOfflineTree.DiMu.InheritTools = True
    MuMuOfflineTree.DiMu.ToolList+=["LoKi::Hybrid::TupleTool/DTF_tool"]
    MuMuOfflineTree.DiMu.addTool(LoKi__Hybrid__TupleTool, name="DTF_tool")
    MuMuOfflineTree.DiMu.DTF_tool.Variables={
        "Mass_DTF" : "DTF_FUN(M,False)", 
        "PT_DTF" : "DTF_FUN(PT,False)", 
        "Chi2_DTF" : "DTF_CHI2NDOF(False)"
        } 

    if DaVinci().Simulation :
        MuMuOfflineTree.addTool(TupleToolMCTruth)
        MuMuOfflineTree.TupleToolMCTruth.ToolList = [ "MCTupleToolReconstructed"  , "MCTupleToolKinematic" ]
        MuMuOfflineTree.TupleToolMCTruth.addTool(MCTupleToolKinematic)
        MuMuOfflineTree.TupleToolMCTruth.addTool(MCTupleToolHierarchy)


    MuMuOfflineTree.addTool (TupleToolDecay, name = "muplus")
    MuMuOfflineTree.muplus.InheritTools = True
    MuMuOfflineTree.muplus.addTool( LoKi__Hybrid__TupleTool,name="LoKiHybridMUP")
    MuMuOfflineTree.muplus.LoKiHybridMUP.Variables={
        "perr2" : "PERR2"
        } 
    MuMuOfflineTree.addTool (TupleToolDecay, name = "muminus")
    MuMuOfflineTree.muplus.InheritTools = True
    MuMuOfflineTree.muplus.addTool( LoKi__Hybrid__TupleTool,name="LoKiHybridMUM")
    MuMuOfflineTree.muplus.LoKiHybridMUM.Variables={
        "perr2" : "PERR2"
        } 

    if not DaVinci().Simulation and DaVinci().DataType in ['2015','2016','2017','2018'] :
        from Configurables import TupleToolHerschel
        MuMuOfflineTree.addTool(TupleToolHerschel, name='tthMuMu')
        if DaVinci().DataType == '2015' :
            MuMuOfflineTree.tthMuMu.DigitsLocation="Raw/HC/CorrectedDigits"
        else :
            MuMuOfflineTree.tthMuMu.DigitsLocation="Raw/HC/Digits"

    seqmumu = SelectionSequence("Seq"+'MuMu', TopSelection = Jpsimumu , PostSelectionAlgs = [MuMuOfflineTree]  )

    etuple = EventTuple("etuple")
    etuple.ToolList += [ "TupleToolPrimaries", "TupleToolEventInfo","TupleToolTrigger"]
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        etuple.ToolList += ["TupleToolStripping"]
    etuple.addTool(TupleToolRecoStats)
    etuple.TupleToolRecoStats.Verbose = True
    etuple.addTool(TupleToolTrigger)
    etuple.TupleToolTrigger.Verbose=True
    etuple.TupleToolTrigger.TriggerList= triggerList
    if DaVinci().DataType in ['2011','2012','2015','2016'] :
        etuple.addTool(TupleToolStripping)
        etuple.TupleToolStripping.Verbose=True
        etuple.TupleToolStripping.StrippingList= ["StrippingLowMultPP2PPMuMuLineDecision","StrippingLowMultMuonLineDecision"]


    seqToReturn = None

    if DaVinci().Simulation :
        smear  = TrackSmearState('Smear')
        seqToReturn = [smear,etuple,MCTuple,seqX.sequence(),seqmumu.sequence()]
    else :
        scaler = TrackScaleState('Scaler')
        seqToReturn = [scaler]

        if DaVinci().DataType in ['2015','2016','2017','2018'] :
            hcrawbankdecoder = HCRawBankDecoder("HCRawBankDecoder")
            seqToReturn += [hcrawbankdecoder]
        if DaVinci().DataType == '2015' :
            corr = HCDigitCorrector()
            seqToReturn += [ corr ]            
        
        seqToReturn += [etuple,seqX.sequence(),seqmumu.sequence()]

    return seqToReturn