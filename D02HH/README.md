# AnalysisProductions options for $` D^{0}\rightarrow h^+ h^- `$

The options contained in this directory generate the NTuples for the simulation
of prompt $` D^{\ast+} \rightarrow (D^{0}\rightarrow h^+ h^-) \pi^{+}_{tag} `$
decays (for both standard and LTUNB triggers),
with $` h^+ h^- = K^- \pi^+,\; K^+ K^-,\; \pi^+ \pi^-,\; K^+ \pi^- `$.

General features:
  * `DecayTreeFitter` algorithm applied to the $` D^{\ast+} `$ decay chain
     w/ the PV constraint, w/ and w/o the $` D^{0} `$ mass constraint;
  * combination of the RS prompt sample with a `PersistReco` muon to be used
    as proxy for secondary decays;
  * momentum scaling of all particles, except the persisted muons of the prompt
    sample.
