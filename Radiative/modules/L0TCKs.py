"""
TCK list
"""

import sys

def getList( tag='all') :
    tcks={}
    tcks['2011'] = [ "0x0032", "0x0033", "0x0034", "0x0035", "0x0036", "0x0037", "0x0038"]
    tcks['2012'] = [ "0x0039", "0x0040", "0x0041", "0x003A", "0x003B", "0x003C", "0x003D",
                     "0x003E", "0x003F", "0x0042", "0x0043", "0x0044", "0x0045",
                     "0x0046", "0x0047", "0x0048", "0x0049", "0x004A"]
    tcks['2015'] = [ "0x014C","0x141","0x014D","0x014E","0x050","0x051","0x00A1","0x00A2",
                     "0x00A3","0x00A4","0x00A5","0x00A6","0x00A7","0x00A8","0x014F",
                     "0x024F","0x024E","0x024B","0x024C","0x024D"]
    tcks['2016'] = [ "0x1600","0x1601","0x1602","0x1603","0x1604","0x1605","0x1606",
                     "0x1607","0x1608","0x1609","0x160A","0x160B","0x160C","0x160D","0x160E","0x160F"]

    all=[]
    for key in tcks :
        for tck in tcks[key] :
            all.append(tck)
    if tag == 'all' :
        return all

    try:
        tcks[tag]
    except KeyError :
        print("WARNING : empty L0TCK list for tag='"+tag+"'")
        return []

    return tcks[tag]
    
