from Configurables import DecayTreeTuple
from Configurables import TupleToolTISTOS
from Configurables import TupleToolDecay
from Configurables import LoKi__Hybrid__TupleTool
from LHCbKernel.Configuration import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, MCDecayTreeTuple
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData
from Configurables import FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand,MergedSelection

import os


# Functions to add cone-stripping-isolation related variables to the tuple
# The branch of the head is always named B in this productions 

def addConeVars(tuple, branch = 'B', conevars_dir = ""):
	
	LoKi_conevars = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_conevars")
	
	daughters = ['B','Res', 'Gamma']
	angles = [1.0, 1.35, 1.7]
	cone_vars_list = ['CONEANGLE', 'CONEMULT', 'CONEP',
	                  'CONEPASYM', 'CONEPT', 'CONEPTASYM']
	paths_conevar = dict(( (dau, ang),
                      os.path.join(conevars_dir,
	                                   "ConeVarsInfo/{}/{}".format(dau, ang)) )
	                     for dau in daughters for ang in angles)
	cone_vars = dict(("ConeVars_{}_{}_{}".format(dau, ang, var),
        	          "RELINFO('{}', '{}', -100)".format(full_path, var))
        	         for (dau, ang), full_path in list(paths_conevar.items())
        	         for var in cone_vars_list)
	LoKi_conevars.Variables = cone_vars
	

def addConeIsoVars(tuple, branch = 'B', conevars_dir = ""):

	LoKi_coneiso = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_coneiso")

	daughters = ['B','Res', 'Gamma']
	isoangles = [0.4, 1.0, 1.35, 1.7]
	# Res can be charged
	cone_iso_list_charged = ['CC_ANGLE','CC_MULT','CC_SPT','CC_VPT','CC_PX','CC_PY',
                 'CC_PZ','CC_PASYM','CC_PTASYM','CC_PXASYM','CC_PYASYM',
                 'CC_PZASYM','CC_DELTAETA','CC_DELTAPHI','CC_IT',
                 'CC_MAXPT_PT','CC_MAXPT_PX', 'CC_MAXPT_PY', 'CC_MAXPT_PZ']

	cone_iso_list_neutral = ['NC_ANGLE','NC_MULT','NC_SPT','NC_VPT','NC_PX','NC_PY',
                 'NC_PZ','NC_PASYM','NC_PTASYM','NC_PXASYM','NC_PYASYM',
                 'NC_PZASYM','NC_DELTAETA','NC_DELTAPHI','NC_IT',
                 'NC_MAXPT_PT','NC_MAXPT_PX', 'NC_MAXPT_PY', 'NC_MAXPT_PZ']
	cone_iso_list = cone_iso_list_charged+cone_iso_list_neutral
	paths_iso = dict(( (dau, ang),
        	           os.path.join(conevars_dir,
        	                        "NeutralConeVarsInfo/{}/{}".format(dau, ang)) )
        		         for dau in daughters for ang in isoangles)
	cone_iso = dict(("ConeIso_{}_{}_{}".format(dau, ang, var),
        	         "RELINFO('{}','{}', -100)".format(full_path, var))
        	        for (dau, ang), full_path in list(paths_iso.items())
        	        for var in cone_iso_list)
	LoKi_coneiso.Variables = cone_iso


def addVtxRadInfo(tuple, branch = 'B', conevars_dir = ""):
	LoKi_VtxRadInfo = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_VtxRadInfo")
	LoKi_VtxRadInfo.Variables = {
	"NEWVTXISONUMVTX": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISONUMVTX', -100)".format(conevars_dir),
    	"NEWVTXISOTRKRELD0": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKRELD0', -100)".format(conevars_dir),
    	"NEWVTXISOTRKDCHI2": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKDCHI2', -100)".format(conevars_dir),
    	"NEWVTXISODCHI2MASS": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISODCHI2MASS', -100)".format(conevars_dir),
}




def addVtxIsoInfo(input, line, name, typ, conevars_dir=""):

	
	# Fix for vertex and conevariables because the information is lost after doing the SubstitutePID
	# VERY VERY BAD, only available way around the PIDSubstitute
	preamble_cone =[]
	daughters = ['B', 'Res', 'Gamma']
	angles = [0.4, 1.0, 1.35, 1.7]
	letters = ['a', "b", "c", "d"]
	cone_vars_list = ['CONEANGLE', 'CONEMULT', 'CONEP',
			  'CONEPASYM', 'CONEPT', 'CONEPTASYM']
	#cone_vars_list = ['CONEANGLE', 'CONEMULT', 'CONEP']
		
	cone_iso_list_charged = ['CC_ANGLE','CC_MULT','CC_SPT','CC_VPT','CC_PX','CC_PY',
				 'CC_PZ','CC_PASYM','CC_PTASYM','CC_PXASYM','CC_PYASYM',
				 'CC_PZASYM','CC_DELTAETA','CC_DELTAPHI','CC_IT',
				 'CC_MAXPT_PT','CC_MAXPT_PX', 'CC_MAXPT_PY', 'CC_MAXPT_PZ']
	
	cone_iso_list_neutral = ['NC_ANGLE','NC_MULT','NC_SPT','NC_VPT','NC_PX','NC_PY',
				 'NC_PZ','NC_PASYM','NC_PTASYM','NC_PXASYM','NC_PYASYM',
				 'NC_PZASYM','NC_DELTAETA','NC_DELTAPHI','NC_IT',
				 'NC_MAXPT_PT','NC_MAXPT_PX', 'NC_MAXPT_PY', 'NC_MAXPT_PZ']

	cone_iso_list_charged = []
	
	cone_iso_list = cone_iso_list_charged+cone_iso_list_neutral
				
	#Initializes dictionnary
	dict_cone_keys = {}
	key = 9023
	variables = []
	
	# loop for cone variables
	for daughter in daughters:
		for angle, letter in zip(angles, letters):
			path_to_cone_info = os.path.join(conevars_dir,'ConeVarsInfo/{}/{}'.format(daughter, angle))
			for var in cone_vars_list:
				preamble_cone.append("{}_{}_{} = SINFO( {}, RELINFO('{}', '{}', -1000000.),True)".format(daughter,var,letter,key,path_to_cone_info,var))
				dict_cone_keys['{}_{}_{}'.format(daughter,var,angle)]="INFO( {}, -1000000. )".format(key)
				variables.append('&({}_{}_{}>-1000000.)'.format(daughter,var,letter))
				key +=1
				
	# Loop for cone iso variables
	for daughter in daughters:
		for angle, letter in zip(angles, letters):
			path_to_cone_info = os.path.join(conevars_dir,'NeutralConeVarsInfo/{}/{}'.format(daughter, angle))
			for var in cone_iso_list:
				preamble_cone.append("{}_{}_{} = SINFO( {}, RELINFO('{}', '{}', -1000000.),True)".format(daughter,var,letter,key,path_to_cone_info,var))
				dict_cone_keys['{}_{}_{}'.format(daughter,var,angle)]="INFO( {}, -1000000. )".format(key)
				variables.append('&({}_{}_{}>-1000000.)'.format(daughter,var,letter))
				key +=1
				
																								


	filter = FilterDesktop( '{0}_cv_naive_filter_{1}'.format(name, typ) )

	#Vertex variables
	preamble = ["a = SINFO( 9014, RELINFO('{}VertexIsoInfo', 'VTXISONUMVTX', -1.),True)".format(conevars_dir),
		    "b = SINFO( 9015, RELINFO('{}VertexIsoInfo', 'VTXISODCHI2ONETRACK', -1.),True)".format(conevars_dir),
		    "c = SINFO( 9016, RELINFO('{}VertexIsoInfo', 'VTXISODCHI2MASSONETRACK', -1.),True)".format(conevars_dir),
		    "d = SINFO( 9017, RELINFO('{}VertexIsoInfo', 'VTXISODCHI2TWOTRACK', -1.),True)".format(conevars_dir),
		    "e = SINFO( 9018, RELINFO('{}VertexIsoInfo', 'VTXISODCHI2MASSTWOTRACK', -1.),True)".format(conevars_dir),
		    "f = SINFO( 9019, RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISONUMVTX', -1.),True)".format(conevars_dir),
		    "g = SINFO( 9020, RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKRELD0', -1.),True)".format(conevars_dir),
		    "h = SINFO( 9021, RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKDCHI2', -1.),True)".format(conevars_dir),
		    "i = SINFO( 9022, RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISODCHI2MASS', -1.),True)".format(conevars_dir),
		    ]

	preamble += preamble_cone
	filter_cuts = ''.join(variables)
	filter.Preambulo = preamble
	filter.Code = '(a>-3000)&(a>-3000)&(b>-3000)&(c>-3000)&(d>-3000)&(e>-3000)&(f>-3000)&(g>-3000)&(h>-3000)&(i>-3000)'+filter_cuts

	#print filter_cuts
	filterSel = Selection( '{0}_cv_naive_sel_{1}'.format(name, typ), Algorithm = filter, RequiredSelections = input)
	return filterSel


def copyVtxIsoInfo():
	
	# Fix for vertex and conevariables because the information is lost after doing the SubstitutePID
	# VERY VERY BAD, only available way around the PIDSubstitute
	
	daughters = ['B', 'Res', 'Gamma']
	angles = [0.4, 1.0, 1.35, 1.7]
	letters = ['a','b','c','d']
	cone_vars_list = ['CONEANGLE', 'CONEMULT', 'CONEP',
			  'CONEPASYM', 'CONEPT', 'CONEPTASYM']
	cone_iso_list_charged = ['CC_ANGLE','CC_MULT','CC_SPT','CC_VPT','CC_PX','CC_PY',
				 'CC_PZ','CC_PASYM','CC_PTASYM','CC_PXASYM','CC_PYASYM',
				 'CC_PZASYM','CC_DELTAETA','CC_DELTAPHI','CC_IT',
				 'CC_MAXPT_PT','CC_MAXPT_PX', 'CC_MAXPT_PY', 'CC_MAXPT_PZ']
	
	cone_iso_list_neutral = ['NC_ANGLE','NC_MULT','NC_SPT','NC_VPT','NC_PX','NC_PY',
				 'NC_PZ','NC_PASYM','NC_PTASYM','NC_PXASYM','NC_PYASYM',
				 'NC_PZASYM','NC_DELTAETA','NC_DELTAPHI','NC_IT',
				 'NC_MAXPT_PT','NC_MAXPT_PX', 'NC_MAXPT_PY', 'NC_MAXPT_PZ']

	#list of charged cone isolation not found, breaks cone isolation variables
	cone_iso_list_charged = []
	cone_iso_list = cone_iso_list_charged+cone_iso_list_neutral
	
	#Initializes dictionnary
	dict_cone_keys = {}
	key = 9023
	
	
	# loop for cone variables
	for daughter in daughters:
		for angle, letter in zip(angles, letters):
			for var in cone_vars_list:
				dict_cone_keys['{}_{}_{}'.format(daughter,var,angle)]="INFO( {}, -10000. )".format(key)
				key +=1
				
	# Loop for cone iso variables
	for daughter in daughters:
		for angle, letter in zip(angles, letters):
			for var in cone_iso_list:
				dict_cone_keys['{}_{}_{}'.format(daughter,var,angle)]="INFO( {}, -10000. )".format(key)
				key +=1
	
	return dict_cone_keys
