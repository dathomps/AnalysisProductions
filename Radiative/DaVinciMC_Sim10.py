"""
Standard MC DaVinci construction for Radiative Ntuples for sim10

SIM tags have to be checked
Careful with tags please.

"""
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import CondDB

# Setup environment options
os.environ['DATATYPE'] = 'MC'

# Import environment variables
year = 		os.getenv("YEAR")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP")

DaVinci().Simulation   = True
DaVinci().Lumi         = False
DaVinci().DataType = year  

print('------------------------------')
magnet = 'md'  # set magdown as default
polarity=os.getenv('POLARITY','unset')
if polarity == 'up' :
    magnet='mu'
elif polarity == 'unset' :
    print('Magnet polarity should be set for MC - assume MagDown')

# ==== dictionary for latest db ==== #
sim = {}

# -- sim10 to be filled  [Update on 15/09/2021]
sim['2010,reco14++']=''
sim['2011,reco14++']=''
sim['2012,reco14++']='' 
sim['2013,reco14++']=''
sim['2015,reco14++']=''   
sim['2016,reco14++']='sim-20201113-6' 
sim['2017,reco14++']='sim-20201113-7'   
sim['2018,reco14++']='sim-20201113-8'   
    
dddb={} #[update on 15/09/2021] 
dddb['2010']=''
dddb['2011']=''
dddb['2012']=''
dddb['2013']=''
dddb['2015']=''
dddb['2016']='dddb-20210528-6'
dddb['2017']='dddb-20210528-7'
dddb['2018']='dddb-20210528-8'  

# -------- apply DB settings
calo='reco14++'

#===    
DaVinci().DDDBtag = dddb[year]
#Notice sim10 at the end of the conddb tag.
DaVinci().CondDBtag = sim[year+','+calo]+'-vc-'+magnet+'100-Sim10'

print(' - DB setting for '+year+' MC / stripping='+stripping+' / calo='+calo+' / polarity = '+polarity)
print('   - SimCond  : '+DaVinci().CondDBtag)
print('   - DDDB     : '+DaVinci().DDDBtag)


# -- L0Monitoring on MC

from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import L0DUReportMonitor
l0moni = L0DUReportMonitor("L0DUMoni")
from Configurables import GaudiSequencer
DaVinci().UserAlgorithms  += [l0moni]


# == get stripping version

