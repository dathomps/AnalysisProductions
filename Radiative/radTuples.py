"""
Adapted for RadTuple production
01/2019

Constructor of all category decayTrees, basic TupleTools+TupleToolTrigger

"""


from  Gaudi.Configuration import *
from  Configurables import DaVinci, DecayTreeTuple
import os
import sys

# CharmWG PATH SHOULD BE HERE
#sys.path.append("/afs/cern.ch/work/p/pgironel/private/WG_prod_radv2/WGProd/modules")

from Radiative.modules import decayTree, tupTool

#------------------------------------#
#----- Prepare generic tupling ------# 
#------------------------------------#

# Import environment options
category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP")
issim = DaVinci().getProp("Simulation")

# DST/mDST options
dst=True
if DaVinci().InputType=="MDST" : dst = False

# ProtoP data to be tupled :
# Prob* and Comb are in TupleToolANNID
dataList="'InAcc*', 'IsPhoton','*IsNot*','*PhotonID*','*CaloCluster*','*CaloTr*'"

# Charged ProtoP data
if "2hksG_ExclTDCPV" not in category:
	dataList=dataList+",'VeloCharge'"

dataList=dataList+",'*Neutral*','*Shape*','*PrsM*','*CaloDeposit*','*ShowerShape*','*ClusterMass*'"  # full neutral info !


# Create default predefined tuple :
radTuple=decayTree.defTuple('radTuple', branch='B', Simulation  = DaVinci().getProp('Simulation'))
# add some specific tupleTools :
print(" == Additional specific tools == ")
tupTool.add(radTuple,"TupleToolL0Data"           )
tupTool.add(radTuple,"TupleToolDalitz"           )

if "2hksG_ExclTDCPV" in category:
   tupTool.add(radTuple,"TupleToolL0Calo/L0Ecal"    , Code="WhichCalo='ECAL'", branch='gamma')
   tupTool.add(radTuple,"TupleToolProtoPData"       , Code="DataList = ["+dataList+"]", branch='gamma', verbose = True)
else:
   tupTool.add(radTuple,"TupleToolL0Calo/L0Ecal"    , Code="WhichCalo='ECAL'")	
   tupTool.add(radTuple,"TupleToolProtoPData"       , Code="DataList = ["+dataList+"]" )

tupTool.add(radTuple,"TupleToolL0Calo/L0Hcal"    , Code="WhichCalo='HCAL'")
tupTool.add(radTuple,"TupleToolPhotonInfo"       )
tupTool.add(radTuple,"TupleToolBremInfo"         ).fullDST=dst
tupTool.add(radTuple,"TupleToolVeto/PhotonVeto", Code =["Particle='gamma'",
                                                        "Veto['Pi02gg']=['/Event/Phys/StdLoosePi02gg/Particles']",
                                                        "Veto['Pi0R']=['/Event/Phys/StdLooseResolvedPi0/Particles']",
                                                        "Veto['Eta2gg']=['/Event/Phys/StdLooseEta2gg/Particles']",
                                                        "Veto['Eta']=['/Event/Phys/StdLooseResolvedEta/Particles']",
                                                        "Veto['Pi0M']=['/Event/Phys/StdLooseMergedPi0/Particles']"
                                                        ])
# fullDST info :
if dst :
    tupTool.add(radTuple,"TupleToolParticleStats")
#    tupTool.add(radTuple,"TupleToolCaloHypo"     )      

# B-branch data
if "2hksG_ExclTDCPV" not in category:
    tupTool.add(radTuple,"TupleToolRadUpDownAsym", branch='B') # ex-private tool (new DaVinci versions should have it)

#  Trigger & Stripping data :
from Radiative.modules import tupTisTos
tupTisTos.addTriggerList     (radTuple, tupTisTos.radTriggerList(stripping,True), branch='B' ) # TupleToolTrigger on the floor -> 'B' branch

#  tupTool.add(radTuple,"TupleToolVtxIsoln"-  # setup in tupSequencer 


if DaVinci().getProp('Simulation') :
    #-------- TUPLETOOLL0DATA : monitor 2011/2012/2015/2016 TCKs
    from Radiative.modules import L0TCKs
    tupTool.add(radTuple, 'TupleToolL0Data'           ).TCKList = L0TCKs.getList()

import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()
