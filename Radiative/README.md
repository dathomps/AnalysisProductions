# Radiative Ntuple Scripts for WGProductions

Radiative Sub-Working Group Option files for creating ntuples via WGProductions, adapted from old ganga radiative centralized procedure, moved from CharmWGProd to AnalysisProductions.

## General Information

Most files names are self explanatory, relevant:
 * `caloOptions.py`: Specific options for the Calorimeter used in Radiative, namely PostCalibration and ReCalibration
 * `DaVinciMC_Sim09*.py`: Check for simdb and condb tags.
 * `tupSequencer.py`: Core of the tupling files.
 * `modules`: Folder containing functions used throughout the main files.

The ntuple scripts only work with Castelao >= v3r1 due to usage of [restoreRelations](https://gitlab.cern.ch/lhcb/Castelao/tree/master/WGProductions/WGProdRadiative) Algorithm. This unables to make tuples using DaVinci versions older than v45r2.

Ntuple options are divided by radiative categories, that can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCb/RaDecayTupleStat). Some of these categories are only available for Run2.

MC requests for Radiative can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/RadiativeDecaysMC).

# Productions

For information about productions already done or ongoing check [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/RadiativeWGProductions)
