##################################################
# Script to produce Bc-> Bs mu nu and Bc-> Bs pi
# with Bs -> Ds pi
# Using stripping from Bc->BsPi discovery 
# and some similar cuts
# Authors: Adam Davis, Tamaki Holly McGrath
##################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import FilterInTrees
from PhysSelPython.Wrappers import Selection, SelectionSequence, AutomaticData
import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
from PhysConf.Selections import SimpleSelection, FilterSelection
from StandardParticles import StdLooseKaons, StdLooseMuons, StdLoosePions, StdAllNoPIDsMuons, StdAllNoPIDsPions
#stripping conf
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
#tuple tools
from Configurables import DecayTreeTuple, MCDecayTreeTuple, PrintDecayTreeTool
from DecayTreeTuple.Configuration import *
from Configurables import (TupleToolDecay, TupleToolTISTOS,  TupleToolNeutrinoReco,
                           TupleToolDecayTreeFitter, TupleToolTrigger, MCTupleToolKinematic,
                           TriggerTisTos, TupleToolDecayTreeFitter, TupleToolConeIsolation,
                           LoKi__Hybrid__TupleTool)

from GaudiKernel.SystemOfUnits import MeV

##############################################################################
#make a dictionary of cuts so we don't have to go searching
##############################################################################

selection_dictionary = {
    'K'   : {'mom_T': '100*MeV',
             'mom': '1000*MeV',
             'trackchi2': '4'},
    'pi'  : {'mom_T': '100*MeV',
             'mom': '1000*MeV',
             'trackchi2': '4'},
    'D_s-': {'mass_window': '200*MeV',
             'vchi2ndf_cut': '10',
             'mom_T': '1500*MeV',
             },
    'pi_from_B_s0' : {'mom_T' : '1700*MeV',
                      'mom' : '10000*MeV',
                      'trackchi2': '2.5',
                      'min_ipchi2': '1',
                     },
    'B_s0': {'vchi2ndf_cut': '10',
             'm_low': '5000*MeV',
             'm_high': '6000*MeV',
             'ltime': '0.000',
            },
    'mu+' : {'mom_T': '350*MeV',
             'trackchi2': '3',
             'min_ipchi2': '1',
            },
    'B_c+': {'vchi2ndf_cut': '100'}
   }
selection_sequences = []
if DaVinci().Simulation == False:
    (selection_dictionary['K'])['pidk'] = '-10'
    (selection_dictionary['pi'])['pidk'] = '20'
    (selection_dictionary['pi_from_B_s0'])['pidk'] = '10'

##############################################################################
# Selection sequences
##############################################################################

line_1 = "B02DPiNoIPD2HHHCFPIDBeauty2CharmLine"
line_2 = "BetaSBs2DsPiDetachedLine"

if DaVinci().Simulation == True:

    # Remove prevous stripping
    from Configurables import EventNodeKiller, ProcStatusCheck
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']
    # Build a stream that contains the desired line
    if DaVinci().DataType == '2015':
        strip = 'stripping24r2'
    if DaVinci().DataType == '2016':
        strip = 'stripping28r2'
    if DaVinci().DataType == '2017':
        strip = 'stripping28r2'
    if DaVinci().DataType == '2018':
        strip = 'stripping34'
    streams = buildStreams(stripping=strippingConfiguration(strip),
                           archive=strippingArchive(strip))
    my_lines = ["Stripping{0}".format(line_1), "Stripping{0}".format(line_2)]
    custom_stream = StrippingStream('CustomStream')
    for stream in streams:
        for sline in stream.lines:
            if sline.name() in my_lines:
                custom_stream.appendLines([sline])

    # Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[custom_stream],
                       MaxCandidates=2000,
                       AcceptBadEvents=False,
                       BadEventSelection=filterBadEvents)
    selection_sequences.append(sc)
    
    # Configure
    DaVinci().UserAlgorithms+=[event_node_killer]

# Ds
# Select Ds first, select kaons and pions from there, then recombine to Ds
if DaVinci().Simulation == False: # if data
    bs_event1 = "/Event/BhadronCompleteEvent/Phys/{0}/Particles".format(line_1)
    bs_event2 = "/Event/BhadronCompleteEvent/Phys/{0}/Particles".format(line_2)
else:
    bs_event1 = "/Event/Phys/{0}/Particles".format(line_1)
    bs_event2 = "/Event/Phys/{0}/Particles".format(line_2)

bs_strip_1 = AutomaticData(bs_event1)
bs_strip_2 = AutomaticData(bs_event2)

# "B02DPiNoIPD2HHHCFPIDBeauty2CharmLine" 
ds_presel = SimpleSelection("ds_presel",
                            ConfigurableGenerators.FilterInTrees,
                            [bs_strip_1],
                            Code = "('D+'==ABSID)"
                           )

kaon_code = "('K+'==ABSID) & (PT > %(mom_T)s) & (P > %(mom)s) & (TRCHI2DOF < %(trackchi2)s)"%selection_dictionary['K']
pion_code = "('pi+'==ABSID) & (PT > %(mom_T)s) & (P > %(mom)s) & (TRCHI2DOF < %(trackchi2)s)"%selection_dictionary['pi']
if DaVinci().Simulation == False:
    kaon_code = kaon_code + " & (PIDK > %(pidk)s)"%selection_dictionary['K']
    pion_code = pion_code + " & (PIDK < %(pidk)s)"%selection_dictionary['pi']
   
kaon_sel = SimpleSelection("kaon_sel",
                            ConfigurableGenerators.FilterInTrees,
                            [ds_presel],
                            Code = kaon_code
)
pion_sel = SimpleSelection("pion_sel",
                         ConfigurableGenerators.FilterInTrees,
                         [ds_presel],
                         Code = pion_code
)
ds_sel_1 = SimpleSelection("ds_sel_1",
                         ConfigurableGenerators.CombineParticles,
                         [kaon_sel, pion_sel],
                         DecayDescriptor = '[D_s- -> K+ K- pi-]cc',
                         CombinationCut = '(AM < 2100) & (AM > 1750 )',
                         MotherCut = "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) & (PT > %(mom_T)s)"%selection_dictionary['D_s-']
)
ds_sel_1_seq = SelectionSequence("ds_sel_1_seq",TopSelection = ds_sel_1)
selection_sequences.append(ds_sel_1_seq)


pi_from_B_s0_code = "('pi+'==ABSID) & (PT > %(mom_T)s) & (P > %(mom)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s)"%selection_dictionary['pi_from_B_s0']
if DaVinci().Simulation == False:
    pi_from_B_s0_code = pi_from_B_s0_code + " & (PIDK < %(pidk)s)"%selection_dictionary['pi_from_B_s0']
 
# Make Bs candidate
bs2dspi_sel_1 = SimpleSelection('bs2dspi_sel_1',
                              ConfigurableGenerators.CombineParticles,
                              [ds_sel_1, StdAllNoPIDsPions],
                              DecayDescriptors = ['[B_s0 -> D_s- pi+]cc', '[B_s~0 -> D_s- pi+]cc'],
                              DaughtersCuts = {"pi+": pi_from_B_s0_code},
                              CombinationCut = "(AM > %(m_low)s) & (%(m_high)s > AM)"%selection_dictionary['B_s0'],
                              MotherCut = "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) & (BPVLTIME() > %(ltime)s)"%selection_dictionary['B_s0']
)
bs2dspi_sel_1_seq = SelectionSequence('bs2dspi_sel_1_seq',TopSelection = bs2dspi_sel_1)
selection_sequences.append(bs2dspi_sel_1_seq)

# Make Bc->Bsmunu          
bc2bsmunu_dspi_sel_1 = SimpleSelection('bc2bsmunu_dspi_sel_1',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2dspi_sel_1, StdAllNoPIDsMuons],
                                     DecayDescriptor = '[B_c+ -> B_s0 mu+]cc',
                                     DaughtersCuts = {"mu+": "('mu+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s) & ISMUON"%selection_dictionary['mu+']},
                                     MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) '%selection_dictionary['B_c+']
)
bc2bsmunu_dspi_sel_1_seq = SelectionSequence("bc2bsmunu_dspi_sel_1_seq",TopSelection = bc2bsmunu_dspi_sel_1)
selection_sequences.append(bc2bsmunu_dspi_sel_1_seq)

# Make Bc->Bspi
bc2bspi_dspi_sel_1 = SimpleSelection('bc2bspi_dspi_sel_1',
                                   ConfigurableGenerators.CombineParticles,
                                   [bs2dspi_sel_1, StdAllNoPIDsPions],
                                   DecayDescriptor = '[B_c+ -> B_s0 pi+]cc',
                                   DaughtersCuts = {"pi+": "('pi+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s)"%selection_dictionary['mu+']},
                                   MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bspi_dspi_sel_1_seq = SelectionSequence("bc2bspi_dspi_sel_1_seq",TopSelection = bc2bspi_dspi_sel_1)
selection_sequences.append(bc2bspi_dspi_sel_1_seq)

######################################################################################################################################

# "BetaSBs2DsPiDetachedLine"
ds_sel_2 = SimpleSelection("ds_sel_2",
                            ConfigurableGenerators.FilterInTrees,
                            [bs_strip_2],
                            Code = "('D_s+'==ABSID) & (PT > %(mom_T)s)"%selection_dictionary['D_s-']
                           )
ds_sel_2_seq = SelectionSequence("ds_sel_2_seq",TopSelection = ds_sel_2)
selection_sequences.append(ds_sel_2_seq)

# Make Bs candidate
bs2dspi_sel_2 = SimpleSelection('bs2dspi_sel_2',
                              ConfigurableGenerators.CombineParticles,
                              [ds_sel_2, StdAllNoPIDsPions],
                              DecayDescriptors = ['[B_s0 -> D_s- pi+]cc', '[B_s~0 -> D_s- pi+]cc'],
                              DaughtersCuts = {"pi+": pi_from_B_s0_code},
                              CombinationCut = "(AM > %(m_low)s) & (%(m_high)s > AM)"%selection_dictionary['B_s0'],
                              MotherCut = "(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) & (BPVLTIME() > %(ltime)s)"%selection_dictionary['B_s0']
)
bs2dspi_sel_2_seq = SelectionSequence('bs2dspi_sel_2_seq',TopSelection = bs2dspi_sel_2)
selection_sequences.append(bs2dspi_sel_2_seq)

# Make Bc->Bsmunu          
bc2bsmunu_dspi_sel_2 = SimpleSelection('bc2bsmunu_dspi_sel_2',
                                     ConfigurableGenerators.CombineParticles,
                                     [bs2dspi_sel_2, StdAllNoPIDsMuons],
                                     DecayDescriptor = '[B_c+ -> B_s0 mu+]cc',
                                     DaughtersCuts = {"mu+": "('mu+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s) & ISMUON"%selection_dictionary['mu+']},
                                     MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s) '%selection_dictionary['B_c+']
)
bc2bsmunu_dspi_sel_2_seq = SelectionSequence("bc2bsmunu_dspi_sel_2_seq",TopSelection = bc2bsmunu_dspi_sel_2)
selection_sequences.append(bc2bsmunu_dspi_sel_2_seq)

# Make Bc->Bspi
bc2bspi_dspi_sel_2 = SimpleSelection('bc2bspi_dspi_sel_2',
                                   ConfigurableGenerators.CombineParticles,
                                   [bs2dspi_sel_2, StdAllNoPIDsPions],
                                   DecayDescriptor = '[B_c+ -> B_s0 pi+]cc',
                                   DaughtersCuts = {"pi+": "('pi+'==ABSID) & (PT > %(mom_T)s) & (TRCHI2DOF < %(trackchi2)s) & (TRGHP<0.4) & (MIPCHI2DV(PRIMARY) > %(min_ipchi2)s)"%selection_dictionary['mu+']},
                                   MotherCut = '(VFASPF(VCHI2PDOF) < %(vchi2ndf_cut)s)'%selection_dictionary['B_c+']
)
bc2bspi_dspi_sel_2_seq = SelectionSequence("bc2bspi_dspi_sel_2_seq",TopSelection = bc2bspi_dspi_sel_2)
selection_sequences.append(bc2bspi_dspi_sel_2_seq)


# All sequences appended

##############################################################################
# Make the decay tree tuples
##############################################################################

# Signal
Bc2BsMu_Bs2DsPi = DecayTreeTuple("Bc2BsMu_Bs2DsPi",
                                 Inputs = [bc2bsmunu_dspi_sel_1_seq.outputLocation(), bc2bsmunu_dspi_sel_2_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^(B_s0 -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^mu+]CC')
Bc2BsMu_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC)',
    'Bs':'[B_c+ -> ^(B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Ds':'[B_c+ -> (B_s0 -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'K1':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'K2':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Pi':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) mu+]CC',
    'Pi_from_Bs':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) mu+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^mu+]CC'
    })

# Norm
Bc2BsPi_Bs2DsPi = DecayTreeTuple("Bc2BsPi_Bs2DsPi",
                                 Inputs = [bc2bspi_dspi_sel_1_seq.outputLocation(), bc2bspi_dspi_sel_2_seq.outputLocation()],
                                 Decay = '[B_c+ -> ^(B_s0 -> ^((D_s- | D_s+) -> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^pi+]CC')
Bc2BsPi_Bs2DsPi.addBranches({
    'Bc':'^([B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC)',
    'Bs':'[B_c+ -> ^(B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Ds':'[B_c+ -> (B_s0 -> ^((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'K1':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'K2':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Pi':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
    'Pi_from_Bs':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) pi+]CC',
    'Mu_bach':'[B_c+ -> (B_s0 -> ((D_s- | D_s+) -> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^pi+]CC'
    })

# MC decay tree tuple - added neutrino
if DaVinci().Simulation == True:
    # Signal
    MC_Bc2BsMu_Bs2DsPi = MCDecayTreeTuple("MC_Bc2BsMu_Bs2DsPi",
                                     Inputs = [bc2bsmunu_dspi_sel_1_seq.outputLocation()],
                                     Decay = '[B_c+ => ^(B_s0 => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^mu+ ^nu_mu]CC')
    MC_Bc2BsMu_Bs2DsPi.addBranches({
        'Bc':'^([B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC)',
        'Bs':'[B_c+ => ^(B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Ds':'[B_c+ => (B_s0 => ^((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'K1':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'K2':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Pi':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC',
        'Pi_from_Bs':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) mu+ nu_mu]CC',
        'Mu_bach':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^mu+ nu_mu]CC',
        'Nu_mu':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ ^nu_mu]CC'
        })


    # Norm
    MC_Bc2BsPi_Bs2DsPi = MCDecayTreeTuple("MC_Bc2BsPi_Bs2DsPi",
                                     Inputs = [bc2bspi_dspi_sel_1_seq.outputLocation()],
                                     Decay = '[B_c+ => ^(B_s0 => ^((D_s- | D_s+) ==> ^K- ^K+ ^(pi- | pi+ ) ) ^(pi+ | pi-) ) ^pi+]CC')
    MC_Bc2BsPi_Bs2DsPi.addBranches({
        'Bc':'^([B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC)',
        'Bs':'[B_c+ => ^(B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Ds':'[B_c+ => (B_s0 => ^((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'K1':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> ^K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'K2':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- ^K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Pi':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ ^(pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC',
        'Pi_from_Bs':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) ^(pi+ | pi-) ) pi+]CC',
        'Mu_bach':'[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) ^pi+]CC'
        })

##############################################################################
# Configure the tools for the DTTs
##############################################################################

tupletoollist = ["TupleToolKinematic",
                 "TupleToolGeometry",
                 "TupleToolPrimaries",
                 "TupleToolRecoStats",
                 "TupleToolEventInfo",
                 "TupleToolTrackInfo",
                 "TupleToolPropertime",
                 "TupleToolANNPID",
                 "TupleToolTrigger"
                 ]

mctupletoollist = ["MCTupleToolHierarchy",
                   "MCTupleToolReconstructed",
                   #"MCTupleToolKinematic",
                   #"TupleToolMCTruth",
                   #"TupleToolMCBackgroundInfo",
                   #"MCTupleToolAngles",
		   #"LoKi::Hybrid::MCTupleTool/LoKi_Photons",
                  ]

triglist = ['L0MuonDecision',
            'L0HadronDecision',
            'Hlt1SingleMuonNoIPDecision',
            'Hlt1TrackMVADecision',
            'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMVADecision',
            'Hlt1TrackMuonDecision',
            'Hlt1TrackAllL0Decision',
            'Hlt2SingleMuonDecision',
            'Hlt2TopoMu2BodyDecision',
            'Hlt2TopoMu3BodyDecision',
            'Hlt2TopoMu4BodyDecision',
            'Hlt2Topo2BodyDecision',
            'Hlt2Topo3BodyDecision',
            'Hlt2Topo4BodyDecision'
        ]

# cone isolation tuple tool configuration
from Configurables import TupleToolConeIsolation
neutral = TupleToolConeIsolation()
neutral.FillIsolation = True
neutral.FillComponents = True
neutral.FillAsymmetry = True
neutral.FillDeltas = True
neutral.FillCharged = False
neutral.FillNeutral = True
neutral.MaxPtParticlesLocation = "Phys/StdAllNoPIDsMuons/Particles"

from Configurables import MCMatchObjP2MCRelator
# Just for MC
if DaVinci().Simulation == True:
    for mctuple in [MC_Bc2BsMu_Bs2DsPi, MC_Bc2BsPi_Bs2DsPi]:
        mctuple.ToolList+=mctupletoollist
        mctuple.addTool(MCTupleToolKinematic())
        mctuple.MCTupleToolKinematic.Verbose = True
        nPhotons = mctuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_Photons")
        nPhotons.Preambulo+=["from LoKiPhysMC.decorators import *" ,"from LoKiPhysMC.functions import *"]
        nPhotons.Variables = {"nPhotons": "MCNINTREE(('gamma' == MCABSID)) "}
        default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
        rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]


for dttuple in [Bc2BsMu_Bs2DsPi, Bc2BsPi_Bs2DsPi]:

    dttuple.ToolList+=tupletoollist
    LoKi_Bc = dttuple.Bc.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bc")
    LoKi_Bc.Preambulo += ["from LoKiPhysMC.decorators import *",
                          "from LoKiPhysMC.functions import mcMatch"]
    LoKi_Bc.Variables = {"PV_ETA" : "BPVETA",
                         "PHI" : "PHI",
                         "KEY": "KEY",
                         "CORR_M" : "BPVCORRM",
                        }

    # fill cone isolation variables
    dttuple.Bc.ToolList += ["TupleToolConeIsolation"] 
    dttuple.Bc.addTool(neutral)

    if DaVinci().Simulation == True:
        MCTruth = dttuple.addTupleTool('TupleToolMCTruth')
        MCTruth.ToolList += ['MCTupleToolKinematic']
        MCTruth.ToolList += ['MCTupleToolHierarchy']
        MCTruth.addTool(MCMatchObjP2MCRelator)
        MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
        dttuple.addTupleTool('TupleToolMCBackgroundInfo')

        # MC Match variable
        LoKi_Bc.Variables["is_from_Bc_BsMuNu"] = "switch( mcMatch(  '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) mu+ nu_mu]CC'  , 1  ) , 1 , 0 )"
        LoKi_Bc.Variables["is_from_Bc_BsPi"] = "switch( mcMatch( '[B_c+ => (B_s0 => ((D_s- | D_s+) ==> K- K+ (pi- | pi+ ) ) (pi+ | pi-) ) pi+]CC'  , 1  ) , 1 , 0 )"
        
        LoKi_Bs = dttuple.Bs.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bs")
        LoKi_Bs.Preambulo+=["from LoKiPhysMC.decorators import *",
                            "from LoKiPhysMC.functions import mcMatch"]
        LoKi_Bs.Variables = {'is_from_Bs_DsPi' : "switch(  mcMatch (' [(B_s0|B_s~0) => (D_s- ==> K- K+ pi-) pi+]CC ', 1), 1, 0)"}

    if Bc2BsMu_Bs2DsPi==dttuple:
        LoKi_MuBach = dttuple.Mu_bach.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_MuBach")
        LoKi_MuBach.Variables = {"NSHAREDMU":"NSHAREDMU"}
        ttnr = dttuple.Bc.addTupleTool(TupleToolNeutrinoReco,name='ttnr')
        ttnr.Verbose=True
        ttnr.MotherMass = 6274.47*MeV#LHCb-PAPER-2020-003
        #http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/LHCb-PAPER-2020-003.html

    ttdtf=dttuple.Bc.addTupleTool(TupleToolDecayTreeFitter("TTDTF"))
    ttdtf.Verbose = True
    ttdtf.constrainToOriginVertex =False
    ttdtf.daughtersToConstrain = ["D_s-",'B_s0']

    ttttBc = dttuple.Bc.addTupleTool(TupleToolTISTOS,name="ttttBc")
    ttttBs = dttuple.Bs.addTupleTool(TupleToolTISTOS,name="ttttBs") 
    ttttMu = dttuple.Mu_bach.addTupleTool(TupleToolTISTOS,name="ttttMu")
    for T in [ttttBc,ttttBs, ttttMu]:
        T.VerboseL0   = True
        T.VerboseHlt1 = True
        T.VerboseHlt2 = True
        T.TriggerList = triglist[:]

##############################################################################
# Configure DaVinci
##############################################################################

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
from Configurables import AuditorSvc, ChronoAuditor
AuditorSvc().Auditors.append( ChronoAuditor("Chrono") )

DaVinci().UserAlgorithms+=[s.sequence() for s in selection_sequences]
DaVinci().UserAlgorithms+=[Bc2BsMu_Bs2DsPi]
DaVinci().UserAlgorithms+=[Bc2BsPi_Bs2DsPi]
if DaVinci().Simulation == True:
    DaVinci().UserAlgorithms+=[MC_Bc2BsMu_Bs2DsPi]
    DaVinci().UserAlgorithms+=[MC_Bc2BsPi_Bs2DsPi]


