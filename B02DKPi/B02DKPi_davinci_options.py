from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import EventTuple
from Configurables import DecayTreeTuple
from Configurables import MCDecayTreeTuple
from Configurables import L0TriggerTisTos, TriggerTisTos
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolGeneration, TupleToolPropertime, TupleToolTISTOS, TupleToolStripping
from Configurables import MCTupleToolAngles, MCTupleToolDecayType, MCTupleToolHierarchy, MCTupleToolInteractions, MCTupleToolKinematic, MCTupleToolPrimaries
from Configurables import MCTupleToolEventType, MCTupleToolPID, MCTupleToolReconstructed
from Configurables import LoKi__Hybrid__TupleTool, TupleToolDecay, TupleToolTrackIsolation
from Configurables import LoKi__Hybrid__EvtTupleTool

from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
import sys
from PhysConf.Filters import LoKi_Filters


from B02DKPi import mc_decay_descriptors
# For any questions about this AP please contact Aidan Wiederhold (aidan.richard.wiederhold@cern.ch), I would like to thank Hannah Pullen for providing the options file that this was based on (but any bugs are of course my own fault!).

# Handy function to return the key associated with an element of a dictionary, assuming there is only one key for each unique element so be careful!
def GetDictKey(dictionary, element):
    for key, value in dictionary.items():
        if element in value:
            return key

def configure_tuple(requiredDecays, stream, nEvents=-1, event_type=""):

    DaVinci().EvtMax = nEvents
    year = DaVinci().DataType[-2:] # Just use the final two digits of the year
    simulation = DaVinci().Simulation
    inputType = DaVinci().InputType

    if simulation: assert(event_type!="")

    #=====================================#
    #=== Stripping candidate locations ===#
    #=====================================#

    # A useful dictionary to keep track of the different decays that may be used
    decayCategories = {}
    decayCategories["HH"] = ["KK", "KPi", "PiK", "PiPi"]
    decayCategories["HHHH"] = ["KPiPiPi", "PiKPiPi","PiPiPiPi"]
    decayCategories["KSHH"] = ["KsKK", "KsPiPi"]
    decayCategories["Pi0HH"] = ["Pi0KK", "Pi0KPi", "Pi0PiK", "Pi0PiPi"]

    tracks = ['LL', 'DD'] # This is only used for stripping lines with KSHH in their name i.e. decays involving a Ks0 since LL/DD refer to where the Ks0 decays in the detector
    signs = ["","_WS"] # Only used for decays for which a Wrong Sign stripping line is available
    bDecs = ['D0KPi']
    dDecs = []
    if "HH" in requiredDecays: dDecs.extend(decayCategories["HH"])
    if "HHHH" in requiredDecays: dDecs.extend(decayCategories["HHHH"])
    if "KSHH" in requiredDecays: dDecs.extend(decayCategories["KSHH"])
    if "Pi0HH" in requiredDecays: dDecs.extend(decayCategories["Pi0HH"])

    Pi0HHLine = "Pi0HHResolved"
    if year in ["11","12"]: Pi0HHLine = "Pi0KPiResolved"

    # set input TES and location predix based in inputs
    if inputType=="MDST":
        DaVinci().RootInTES = '/Event/%s' % stream
        locationPrefix = ""
    else:
        locationPrefix = "/Event/{}".format(stream)
    locations = {}
    for b in bDecs:
        for d in dDecs:
            if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                locations["{}_{}".format(b,d)] = ["{}/Phys/B02{}D2{}Beauty2CharmLine/Particles".format(locationPrefix, b, GetDictKey(decayCategories, d))]
                if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                    locations["{}_{}_WS".format(b,d)] = ["{}/Phys/B02{}D2{}WSBeauty2CharmLine/Particles".format(locationPrefix, b, GetDictKey(decayCategories, d))]
            if d in decayCategories["Pi0HH"]:
                locations["{}_{}".format(b,d)] = ["{}/Phys/B02{}D2{}Beauty2CharmLine/Particles".format(locationPrefix, b, Pi0HHLine)]
            if d in decayCategories["KSHH"]:
                for t in tracks:
                    locations["{}_{}_{}".format(b,d,t)] = ["{}/Phys/B02{}D2KSHH{}Beauty2CharmLine/Particles".format(locationPrefix, b, t)]
                    locations["{}_{}_{}_WS".format(b,d,t)] = ["{}/Phys/B02{}D2KSHH{}WSBeauty2CharmLine/Particles".format(locationPrefix, b, t)]

    #=========================#
    #=== Decay descriptors ===#
    #=========================#

    decayDesc = {}

    # KS0 Decays
    if "KSHH" in requiredDecays:
        decayDesc['D0KPi_KsPiPi'] = ("[(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KsPiPi_WS'] = ("[(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                         "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                         "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                         "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^pi- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KsKK'] = ("[(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K+ ^K-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                    "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K+ ^K-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KsKK_WS'] = ("[(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K+ ^K+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K+ ^K+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                       "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K- ^K-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^(KS0 --> ^pi+ ^pi-) ^K- ^K-) ^(K*(892)~0 -> ^K- ^pi+))]")

    #2-body decays
    if "HH" in requiredDecays:
        decayDesc['D0KPi_PiPi'] = ("[(B0 --> ^(D0 --> ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                    "(B0 --> ^(D0 --> ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_PiPi_WS'] = ("[(B0 --> ^(D0 --> ^pi+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^pi+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                       "(B0 --> ^(D0 --> ^pi- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^pi- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KK'] = ("[(B0 --> ^(D0 --> ^K+ ^K-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                  "(B0 --> ^(D0 --> ^K+ ^K-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KK_WS'] = ("[(B0 --> ^(D0 --> ^K+ ^K+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                     "(B0 --> ^(D0 --> ^K+ ^K+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                     "(B0 --> ^(D0 --> ^K- ^K-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                     "(B0 --> ^(D0 --> ^K- ^K-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KPi'] = ("[(B0 --> ^(D0 --> ^K+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                   "(B0 --> ^(D0 --> ^K- ^pi+) ^(K*(892)~0 -> ^K- ^pi+))]")
        decayDesc['D0KPi_PiK'] = ("[(B0 --> ^(D0 --> ^K- ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                   "(B0 --> ^(D0 --> ^K+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KPi_WS'] = ("[(B0 --> ^(D0 --> ^K+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^K+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                      "(B0 --> ^(D0 --> ^K- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^K- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        #This will just be identical to the KPi_WS tuple. Maybe we can skip this one and just remember that the KPi_WS is for both K+Pi- and K-Pi+? Or we should merge those two together?
        # I will keep it here commented out for posterity but will omit future similar repetitions.
        """
        decayDesc['D0KPi_PiK_WS'] = ("[(B0 --> ^(D0 --> ^K+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^K+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                      "(B0 --> ^(D0 --> ^K- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^K- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")
        """

    # 4-body decays
    if "HHHH" in requiredDecays:
        decayDesc['D0KPi_KPiPiPi'] = ("[(B0 --> ^(D0 --> ^K+ ^pi- ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^K- ^pi+ ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")
        decayDesc['D0KPi_PiKPiPi'] = ("[(B0 --> ^(D0 --> ^K- ^pi+ ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^K+ ^pi- ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_KPiPiPi_WS'] = ("[(B0 --> ^(D0 --> ^K+ ^pi+ ^pi+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                          "(B0 --> ^(D0 --> ^K+ ^pi+ ^pi+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                          "(B0 --> ^(D0 --> ^K- ^pi- ^pi- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                          "(B0 --> ^(D0 --> ^K- ^pi- ^pi- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_PiPiPiPi'] = ("[(B0 --> ^(D0 --> ^pi+ ^pi- ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                        "(B0 --> ^(D0 --> ^pi+ ^pi- ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_PiPiPiPi_WS'] = ("[(B0 --> ^(D0 --> ^pi+ ^pi+ ^pi+ ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                           "(B0 --> ^(D0 --> ^pi+ ^pi+ ^pi+ ^pi+) ^(K*(892)~0 -> ^K- ^pi+)),"
                                           "(B0 --> ^(D0 --> ^pi- ^pi- ^pi- ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                           "(B0 --> ^(D0 --> ^pi- ^pi- ^pi- ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

    # Pi0 Decays
    if "Pi0HH" in requiredDecays:
        decayDesc['D0KPi_Pi0PiPi'] = ("[(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^pi+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                       "(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^pi+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_Pi0KK'] = ("[(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K+ ^K-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                     "(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K+ ^K-) ^(K*(892)~0 -> ^K- ^pi+))]")

        decayDesc['D0KPi_Pi0KPi'] = ("[(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K+ ^pi-) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K- ^pi+) ^(K*(892)~0 -> ^K- ^pi+))]")
        decayDesc['D0KPi_Pi0PiK'] = ("[(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K- ^pi+) ^(K*(892)0  -> ^K+ ^pi-)),"
                                      "(B0 --> ^(D0 --> ^(pi0 -> ^gamma ^gamma) ^K+ ^pi-) ^(K*(892)~0 -> ^K- ^pi+))]")

    #=============================#
    #=== List of trigger lines ===#
    #=============================#

    triggerListL0 = [ "L0HadronDecision" ]

    if year not in ['11', '12']:
        # run 2 triggers
        triggerListHlt1 = [ "Hlt1TrackMVADecision",
                            "Hlt1TwoTrackMVADecision",
                            "Hlt1TrackMuonDecision",
                            "Hlt1TrackPhotonDecision",
                            "Hlt1TrackForwardPassThroughDecision",
                            "Hlt1TrackForwardPassThroughLooseDecision" ]

        triggerListHlt2 = [ "Hlt2Topo2BodyDecision",
                            "Hlt2Topo3BodyDecision",
                            "Hlt2Topo4BodyDecision" ]
    else:
        # run 1 triggers
        triggerListHlt1 = [ "Hlt1TrackAllL0Decision",
                            "Hlt1Trackturbo: noMuonDecision",
                            "Hlt1TrackPhotonDecision",
                            "Hlt1TrackForwardPassThroughDecision",
                            "Hlt1TrackForwardPassThroughLooseDecision"]

        triggerListHlt2 = [ "Hlt2Topo2BodyBBDTDecision",
                            "Hlt2Topo3BodyBBDTDecision",
                            "Hlt2Topo4BodyBBDTDecision" ]

    triggerListAll = triggerListL0 + triggerListHlt1 + triggerListHlt2

    #===============================#
    #=== List of stripping lines ===#
    #===============================#

    strippingList = []
    stripCode = ""

    for b in bDecs:
        for d in dDecs:
            if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                stripping = "StrippingB02{}D2{}Beauty2CharmLineDecision".format(b, GetDictKey(decayCategories, d))
                strippingList.append(stripping)
                if stripCode: stripCode += " | "
                stripCode += "HLT_PASS_RE('{}')".format(stripping)
                if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                    strippingWS = "StrippingB02{}D2{}WSBeauty2CharmLineDecision".format(b, GetDictKey(decayCategories, d))
                    strippingList.append(strippingWS)
                    if stripCode: stripCode += " | "
                    stripCode += "HLT_PASS_RE('{}')".format(strippingWS)
            if d in decayCategories["Pi0HH"]:
                stripping = "StrippingB02{}D2{}Beauty2CharmLineDecision".format(b,Pi0HHLine)
                strippingList.append(stripping)
                if stripCode: stripCode += " | "
                stripCode += "HLT_PASS_RE('{}')".format(stripping)
            if d in decayCategories["KSHH"]:
                for t in tracks:
                    stripping = "StrippingB02{}D2KSHH{}Beauty2CharmLineDecision".format(b, t)
                    strippingWS = "StrippingB02{}D2KSHH{}WSBeauty2CharmLineDecision".format(b, t)
                    strippingList.append(stripping)
                    strippingList.append(strippingWS)
                    if stripCode: stripCode += " | "
                    stripCode += "HLT_PASS_RE('{}') | HLT_PASS_RE('{}')".format(stripping, strippingWS)

    # Make sure DaVinci filters on stripping and HLT2 as first thing (saves time)
    filters = LoKi_Filters(
        HLT2_Code = " HLT_PASS_RE('Hlt2Topo.*Decision') ",
        STRIP_Code = stripCode
        )
    DaVinci.EventPreFilters = filters.filters("Filters")

    #=========================#
    #=== Set up EventTuple ===#
    #=========================#

    eventTuple = EventTuple("EventTuple")
    eventTuple.ToolList = [ "TupleToolEventInfo" ]

    #==============================#
    #=== Set up DecayTreeTuples ===#
    #==============================#

    tuples = {}
    from collections import defaultdict
    tupleLists = defaultdict(list)

    for b in bDecs:
        for d in dDecs:
            if d in decayCategories["Pi0HH"]:
                name = "B02{}_D2{}".format(b, d) # replace 0 to have consistent naming with 15+16 B2DK GGSZ analysis ## Good to remember but I'm not sure if we're that bothered about this?
                tuples[name] = DecayTreeTuple(name)
                tuples[name].Inputs = locations['{}_{}'.format(b,d)]
                tuples[name].Decay = decayDesc["{}_{}".format(b, d)]
                tupleLists["{}_{}".format(b, d)].append(tuples[name]) #Decay specific key
                tupleLists["Pi0HH"].append(tuples[name]) #Decay category key
                tupleLists["all"].append(tuples[name]) # All decays key
            if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                    for w in signs:
                        name = "B02{}_D2{}{}".format(b, d, w)
                        tuples[name] = DecayTreeTuple(name)
                        tuples[name].Inputs = locations['{}_{}{}'.format(b, d, w)]
                        tuples[name].Decay = decayDesc["{}_{}{}".format(b, d, w)]
                        tupleLists["{}_{}{}".format(b, d, w)].append(tuples[name]) #Decay specific key
                        tupleLists[GetDictKey(decayCategories, d)].append(tuples[name]) #Decay category key
                        tupleLists["all"].append(tuples[name]) # All decays key
                else:
                    name = "B02{}_D2{}".format(b, d)
                    tuples[name] = DecayTreeTuple(name)
                    tuples[name].Inputs = locations['{}_{}'.format(b, d)]
                    tuples[name].Decay = decayDesc["{}_{}".format(b, d)]
                    tupleLists["{}_{}".format(b, d)].append(tuples[name]) #Decay specific key
                    tupleLists[GetDictKey(decayCategories, d)].append(tuples[name]) #Decay category key
                    tupleLists["all"].append(tuples[name]) # All decays key
            if d in decayCategories["KSHH"]:
                for t in tracks:
                    for w in signs:
                        name = "B02{}_D2{}_{}{}".format(b, d, t, w)
                        tuples[name] = DecayTreeTuple(name)
                        tuples[name].Inputs = locations['{}_{}_{}{}'.format(b, d, t, w)]
                        tuples[name].Decay = decayDesc["{}_{}{}".format(b, d, w)]
                        tupleLists["{}_{}{}".format(b, d, w)].append(tuples[name]) #Decay specific key
                        tupleLists["KSHH"].append(tuples[name]) #Decay category key
                        tupleLists["all"].append(tuples[name]) # All decays key

    #=================================#
    #=== Set up MC DecayTreeTuple ===#
    #=================================#
    Dstar_D0X = ["D0pi0","D0g"]
    if simulation:
        mc_tuples = []
        if "decay" in mc_decay_descriptors.mc_decay_descriptors[event_type]:
            mc_tuple = MCDecayTreeTuple("TruthTuple")
            mc_tuple.setDescriptorTemplate(mc_decay_descriptors.mc_decay_descriptors[event_type]["decay"])

            mc_tuple.ToolList = []
            mc_tuple.ToolList = [
                                "TupleToolEventInfo",
                                "MCTupleToolDecayType",
                                "MCTupleToolHierarchy",
                                "MCTupleToolKinematic",
                                "MCTupleToolPID"
                                ]
            mc_tuples.append(mc_tuple)
        else:
            for i in [1,2]:
                mc_tuple=MCDecayTreeTuple("TruthTuple_{}".format(Dstar_D0X[i-1]))
                mc_tuple.setDescriptorTemplate(mc_decay_descriptors.mc_decay_descriptors[event_type]["decay{}".format(i)])
                mc_tuple.ToolList = []
                mc_tuple.ToolList = [
                                     "TupleToolEventInfo",
                                     "MCTupleToolDecayType",
                                     "MCTupleToolHierarchy",
                                     "MCTupleToolKinematic",
                                     "MCTupleToolPID"
                                     ]
                mc_tuples.append(mc_tuple)
    #======================#
    #=== LoKi variables ===#
    #======================#

    #=== LoKi variables for BachK, h1, h2, Ksh1, Ksh2 ===#

    LoKih = {
        "Q"           : "Q"
        ,"MIP_PV"      : "MIPDV(PRIMARY)"
        ,"MIPCHI2_PV"  : "MIPCHI2DV(PRIMARY)"
        ,"TRCHI2DOF"   : "TRCHI2DOF"
        ,"TRGHOSTPROB" : "TRGHP"
        ,"TRTYPE"      : "TRTYPE"
        ,"ETA"         : "ETA"
        ,"Y"           : "Y"
        }

    #=== Common LoKi variables for BachK, h1, h2, Ksh1, Ksh2 ===#
    LoKiBDK = {
        "Q"              : "Q"
        ,"DIRA_BPV"      : "BPVDIRA"
        ,"MAXDOCA"       : "DOCAMAX"
        ,"AMAXDOCA"      : "PFUNA(AMAXDOCA(''))"
        ,"AMINDOCA"      : "PFUNA(AMINDOCA(''))"
        ,"MIP_PV"        : "MIPDV(PRIMARY)"
        ,"MIPCHI2_PV"    : "MIPCHI2DV(PRIMARY)"
        ,"IP_BPV"        : "BPVIP()"
        ,"IPCHI2_BPV"    : "BPVIPCHI2()"
        ,"FD_BPV"        : "BPVVD"
        ,"FD_BPV_SIGNED" : "BPVVDSIGN"
        ,"FDCHI2_BPV"    : "BPVVDCHI2"
        ,"RHO_BPV"       : "BPVVDRHO"
        ,"Z_BPV"         : "BPVVDZ"
        ,"VTXCHI2DOF"    : "VFASPF(VCHI2/VDOF)"
        ,"ENDVTX_X"      : "VFASPF(VX)"
        ,"ENDVTX_Y"      : "VFASPF(VY)"
        ,"ENDVTX_Z"      : "VFASPF(VZ)"
        ,"LT_BPV"        : "BPVLTIME('PropertimeFitter/ProperTime    ::PUBLIC')"
        ,"LTCHI2_BPV"    : "BPVLTCHI2('PropertimeFitter/ProperTime   ::PUBLIC')"
        ,"LTFITCHI2_BPV" : "BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC')"
        ,"ETA"           : "ETA"
        ,"Y"             : "Y"
        }

    #=== LoKi variables for Ks, Pi0 ===#
    LoKiKs = {"DM": "DMASS('KS0')"}
    LoKiPi0 = {"DM": "DMASS('pi0')"}

    #=== LoKi variables for Bd ===#
    LoKiBd = {
        "DM" : "DMASS('B0')"
        ,"LV01" : "LV01"
        ,"LV02" : "LV02"
        }

    #=== LoKi variables for D0 ===#
    LoKiD0 = {
        "DM" : "DMASS('D0')"
        }

    #=== LoKi variables for the event ===#
    LoKiEvt = {
        #track information
        "LoKi_nTracks" : "RECSUMMARY(LHCb.RecSummary.nTracks     , -1)"
        , "LoKi_nLong" : "RECSUMMARY(LHCb.RecSummary.nLongTracks , -1)"
        #RICH multiplities
        , "LoKi_nRICH1Hits" : "switch(HASRECSUMMARY(20),RECSUMMARY(20, -1),-1)"
        , "LoKi_nRICH2Hits" : "switch(HASRECSUMMARY(21),RECSUMMARY(21, -1),-1)"
        }

    #=== Add branches ===#
    if "KSHH" in requiredDecays:
        for ntp in tupleLists["D0KPi_KsPiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Ks" : ("[(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Ksh1" : ("[(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Ksh2" : ("[(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_KsPiPi_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "Ks" : ("[(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ ^pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Ksh1" : ("[(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "Ksh2" : ("[(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi+ pi+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) pi- pi-) (K*(892)~0 -> K- ^pi+))]")
                })


        for ntp in tupleLists["D0KPi_KsKK"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Ks" : ("[(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ ^K-) (K*(892)~0 -> K- pi+))]"),
                "Ksh1" : ("[(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Ksh2" : ("[(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_KsKK_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)~0 -> K- pi+))]"),
                "Ks" : ("[(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^(KS0 --> pi+ pi-) K- K-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) ^K- K-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ ^K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ ^K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- ^K-) (K*(892)~0 -> K- pi+))]"),
                "Ksh1" : ("[(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> ^pi+ pi-) K- K-) (K*(892)~0 -> K- pi+))]"),
                "Ksh2" : ("[(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ ^pi-) K- K-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K+ K+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> (KS0 --> pi+ pi-) K- K-) (K*(892)~0 -> K- ^pi+))]")
                })

    if "HH" in requiredDecays:
        for ntp in tupleLists["D0KPi_PiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_PiPi_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> pi+ ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ ^pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> pi+ pi+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- pi-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> pi+ pi+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> pi- pi-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> pi+ pi+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> pi+ pi+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> pi- pi-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> pi- pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_KK"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ K-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^K-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ K-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> K+ K-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ K-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> K+ K-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  K+ K-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  K+ K-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_KK_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ K+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ K+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> K- K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K- K-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K- K-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^K- K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K- K-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^K+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^K+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- ^K-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ K+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ K+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- K-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- K-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ K+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K+ K+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> K- K-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K- K-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> K+ K+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K+ K+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> K- K-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K- K-) (K*(892)~0 -> K- ^pi+))]"),
                })

        for ntp in tupleLists["D0KPi_KPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K- pi+) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K- pi+) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K- pi+) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- ^pi+) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> K- pi+) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> K- pi+) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  K+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  K- pi+) (K*(892)~0 -> K- ^pi+))]")
                })
        for ntp in tupleLists["D0KPi_PiK"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K- pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K- pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K- pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K- ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K- pi+) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> K+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K- pi+) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> K+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  K- pi+) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  K+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_KPi_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ pi+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> K- pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K- pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> K- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K- pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^K- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K- pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ pi+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- pi-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ pi+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> K- pi-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K- pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> K+ pi+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K+ pi+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> K- pi-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K- pi-) (K*(892)~0 -> K- ^pi+))]")
                })

    if "HHHH" in requiredDecays:
        for ntp in tupleLists["D0KPi_KPiPiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K- pi+ pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K- pi+ pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K- pi+ pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- ^pi+ pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h3" : ("[(B0 --> (D0 --> K+ pi- ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi+ ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h4" : ("[(B0 --> (D0 --> K+ pi- pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi+ pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ pi- pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> K- pi+ pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ pi- pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> K- pi+ pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  K+ pi- pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  K- pi+ pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })
        for ntp in tupleLists["D0KPi_PiKPiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K- pi+ pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K- pi+ pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K- pi+ pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K- ^pi+ pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h3" : ("[(B0 --> (D0 --> K- pi+ ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi- ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h4" : ("[(B0 --> (D0 --> K- pi+ pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi- pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K- pi+ pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> K+ pi- pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K- pi+ pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> K+ pi- pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  K- pi+ pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  K+ pi- pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })


        for ntp in tupleLists["D0KPi_KPiPiPi_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> K+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> K- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> K- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^K+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^K- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^K- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> K+ ^pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ ^pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- ^pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- ^pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h3" : ("[(B0 --> (D0 --> K+ pi+ ^pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+ ^pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- pi- ^pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi- ^pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h4" : ("[(B0 --> (D0 --> K+ pi+ pi+ ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+ pi+ ^pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- pi- pi- ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi- pi- ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> K+ pi+ pi+ pi+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+ pi+ pi+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K+ pi+ pi+ pi+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> K- pi- pi- pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_PiPiPiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> pi+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> pi+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^pi+ pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi+ pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> pi+ ^pi- pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ ^pi- pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h3" : ("[(B0 --> (D0 --> pi+ pi- ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi- ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h4" : ("[(B0 --> (D0 --> pi+ pi- pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi- pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> pi+ pi- pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> pi+ pi- pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> pi+ pi- pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> pi+ pi- pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  pi+ pi- pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  pi+ pi- pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_PiPiPiPi_WS"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "^(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> pi+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> ^(D0 --> pi- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> pi- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> ^pi+ pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi+ pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> ^pi- pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> ^pi- pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> pi+ ^pi+ pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ ^pi+ pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- ^pi- pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- ^pi- pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h3" : ("[(B0 --> (D0 --> pi+ pi+ ^pi+ pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+ ^pi+ pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- pi- ^pi- pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi- ^pi- pi-) (K*(892)~0 -> K- pi+))]"),
                "h4" : ("[(B0 --> (D0 --> pi+ pi+ pi+ ^pi+) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+ pi+ ^pi+) (K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- pi- pi- ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi- pi- ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> pi+ pi+ pi+ pi+) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+ pi+ pi+) ^(K*(892)~0 -> K- pi+)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) ^(K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)~0 -> ^K- pi+)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)0  -> ^K+ pi-)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> pi+ pi+ pi+ pi+) (K*(892)~0 -> K- ^pi+)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)0  -> K+ ^pi-)),"
                         "(B0 --> (D0 --> pi- pi- pi- pi-) (K*(892)~0 -> K- ^pi+))]")
                })

    if "Pi0HH" in requiredDecays:
        for ntp in tupleLists["D0KPi_Pi0PiPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Pi0" : ("[(B0 --> (D0 --> ^(pi0 -> gamma gamma) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                          "(B0 --> (D0 --> ^(pi0 -> gamma gamma) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Pi0g1" : ("[(B0 --> (D0 --> (pi0 -> ^gamma gamma) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> ^gamma gamma) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "Pi0g2" : ("[(B0 --> (D0 --> (pi0 -> gamma ^gamma) pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma ^gamma) pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) ^pi+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) ^pi+ pi-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ ^pi-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ pi-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  (pi0 -> gamma gamma) pi+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  (pi0 -> gamma gamma) pi+ pi-) (K*(892)~0 -> K- ^pi+))]")
                })


        for ntp in tupleLists["D0KPi_Pi0KK"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Pi0" : ("[(B0 --> (D0 --> ^(pi0 -> gamma gamma) K+ K-) (K*(892)0  -> K+ pi-)),"
                          "(B0 --> (D0 --> ^(pi0 -> gamma gamma) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Pi0g1" : ("[(B0 --> (D0 --> (pi0 -> ^gamma gamma) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> ^gamma gamma) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "Pi0g2" : ("[(B0 --> (D0 --> (pi0 -> gamma ^gamma) K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma ^gamma) K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) ^K+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) ^K+ K-) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) K+ ^K-) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (pi0 -> gamma gamma) K+ K-) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  (pi0 -> gamma gamma) K+ K-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  (pi0 -> gamma gamma) K+ K-) (K*(892)~0 -> K- ^pi+))]")
                })

        for ntp in tupleLists["D0KPi_Pi0KPi"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (pi0 -> gamma gamma) K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (pi0 -> gamma gamma) K- pi+) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (pi0 -> gamma gamma) K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (pi0 -> gamma gamma) K- pi+) (K*(892)~0 -> K- pi+))]"),
                "Pi0" : ("[(B0 --> (D0 --> ^(pi0 -> gamma gamma) K+ pi-) (K*(892)0  -> K+ pi-)),"
                          "(B0 --> (D0 --> ^(pi0 -> gamma gamma) K- pi+) (K*(892)~0 -> K- pi+))]"),
                "Pi0g1" : ("[(B0 --> (D0 --> (pi0 -> ^gamma gamma) K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> ^gamma gamma) K- pi+) (K*(892)~0 -> K- pi+))]"),
                "Pi0g2" : ("[(B0 --> (D0 --> (pi0 -> gamma ^gamma) K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma ^gamma) K- pi+) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) ^K+ pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) ^K- pi+) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ ^pi-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) K- ^pi+) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ pi-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (pi0 -> gamma gamma) K- pi+) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) K+ pi-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (pi0 -> gamma gamma) K- pi+) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  (pi0 -> gamma gamma) K+ pi-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  (pi0 -> gamma gamma) K- pi+) (K*(892)~0 -> K- ^pi+))]")
                })
        for ntp in tupleLists["D0KPi_Pi0PiK"]:
            ntp.addBranches({
                "Bd" : ("[^(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ K-) (K*(892)0  -> K+ pi-)),"
                         "^(B0 --> (D0 --> (pi0 -> gamma gamma) pi- K+) (K*(892)~0 -> K- pi+))]"),
                "D0" : ("[(B0 --> ^(D0 --> (pi0 -> gamma gamma) pi+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> ^(D0 --> (pi0 -> gamma gamma) pi- K+) (K*(892)~0 -> K- pi+))]"),
                "Pi0" : ("[(B0 --> (D0 --> ^(pi0 -> gamma gamma) pi+ K-) (K*(892)0  -> K+ pi-)),"
                          "(B0 --> (D0 --> ^(pi0 -> gamma gamma) pi- K+) (K*(892)~0 -> K- pi+))]"),
                "Pi0g1" : ("[(B0 --> (D0 --> (pi0 -> ^gamma gamma) pi+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> ^gamma gamma) pi- K+) (K*(892)~0 -> K- pi+))]"),
                "Pi0g2" : ("[(B0 --> (D0 --> (pi0 -> gamma ^gamma) pi+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma ^gamma) pi- K+) (K*(892)~0 -> K- pi+))]"),
                "h1" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ ^K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) pi- ^K+) (K*(892)~0 -> K- pi+))]"),
                "h2" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) ^pi+ K-) (K*(892)0  -> K+ pi-)),"
                         "(B0 --> (D0 --> (pi0 -> gamma gamma) ^pi- K+) (K*(892)~0 -> K- pi+))]"),
                "Kstar" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ K-) ^(K*(892)0  -> K+ pi-)),"
                            "(B0 --> (D0 --> (pi0 -> gamma gamma) pi- K+) ^(K*(892)~0 -> K- pi+))]"),
                "BachK" : ("[(B0 --> (D0 --> (pi0 -> gamma gamma) pi+ K-) (K*(892)0  -> ^K+ pi-)),"
                           "(B0 --> (D0 --> (pi0 -> gamma gamma) pi- K+) (K*(892)~0 -> ^K- pi+))]"),
                "BachPi" : ("[(B0 --> (D0 -->  (pi0 -> gamma gamma) pi+ K-) (K*(892)0  -> K+ ^pi-)),"
                              "(B0 --> (D0 -->  (pi0 -> gamma gamma) pi- K+) (K*(892)~0 -> K- ^pi+))]")
                })

    for ntp in tupleLists["all"]:

        ntp.ToolList = []
        ntp.ToolList += [ "TupleToolAngles",
                        "TupleToolEventInfo",
                        #"TupleToolGeometry",
                        "TupleToolKinematic",
                        "TupleToolPrimaries",
                        "TupleToolPropertime",
                        "TupleToolRecoStats",
                        #"TupleToolRICHPid",
                        "TupleToolTrackInfo",
                        "TupleToolEventInfo",
                        "TupleToolANNPID"
                        ]

        if simulation:
            ntp.ToolList += ["TupleToolMCBackgroundInfo"]
            ttMCt = ntp.addTupleTool("TupleToolMCTruth/ttMCt")
            ttMCt.ToolList = [
                    "MCTupleToolDecayType",
                    "MCTupleToolHierarchy",
                    "MCTupleToolKinematic",
                    #"MCTupleToolReconstructed"
                    ]

        if ntp not in tupleLists["Pi0HH"]: ntp.ToolList.append("TupleToolGeometry")
        else:
            ntp.Bd.addTupleTool("TupleToolGeometry")
            ntp.D0.addTupleTool("TupleToolGeometry")
            ntp.Pi0.addTupleTool("TupleToolGeometry")
            ntp.h1.addTupleTool("TupleToolGeometry")
            ntp.h2.addTupleTool("TupleToolGeometry")
            ntp.Kstar.addTupleTool("TupleToolGeometry")
            ntp.BachK.addTupleTool("TupleToolGeometry")
            ntp.BachPi.addTupleTool("TupleToolGeometry")

        ttpid = ntp.addTupleTool("TupleToolPid/ttpid")
        ttpid.Verbose = True

        tttt = ntp.addTupleTool("TupleToolTISTOS/tttt")
        tttt.VerboseL0 = True
        tttt.VerboseHlt1 = True
        tttt.VerboseHlt2 = True
        tttt.TriggerList = triggerListAll
        if True:
            tttt.addTool(TriggerTisTos())
            tttt.TriggerTisTos.TOSFracEcal = 0.
            tttt.TriggerTisTos.TOSFracHcal = 0.
        if True:
            tttt.addTool(TriggerTisTos())
            tttt.TriggerTisTos.TOSFracMuon = 0.

        #TupleToolTrigger
        ttt = ntp.addTupleTool("TupleToolTrigger/ttt")
        ttt.Verbose = True
        ttt.TriggerList = triggerListAll

        #TupleToolStripping
        tts = ntp.addTupleTool("TupleToolStripping/tts")
        tts.StrippingList = strippingList

        # DecayTreeFitter refits
        if ntp in tupleLists["KSHH"]:
            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0D0KSPV")
            ntp.Bd.constB0D0KSPV.constrainToOriginVertex = True
            ntp.Bd.constB0D0KSPV.Verbose = True
            ntp.Bd.constB0D0KSPV.daughtersToConstrain = ['B0', 'D0', 'KS0']
            ntp.Bd.constB0D0KSPV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0KSPV")
            ntp.Bd.constB0KSPV.constrainToOriginVertex = True
            ntp.Bd.constB0KSPV.Verbose = True
            ntp.Bd.constB0KSPV.daughtersToConstrain = ['B0', 'KS0']
            ntp.Bd.constB0KSPV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0D0KSPV")
            ntp.Bd.constBs0D0KSPV.constrainToOriginVertex = True
            ntp.Bd.constBs0D0KSPV.Verbose = True
            ntp.Bd.constBs0D0KSPV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
            ntp.Bd.constBs0D0KSPV.daughtersToConstrain = ['B_s0', 'D0', 'KS0']
            ntp.Bd.constBs0D0KSPV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0KSPV")
            ntp.Bd.constBs0KSPV.constrainToOriginVertex = True
            ntp.Bd.constBs0KSPV.Verbose = True
            ntp.Bd.constBs0KSPV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
            ntp.Bd.constBs0KSPV.daughtersToConstrain = ['B_s0', 'KS0']
            ntp.Bd.constBs0KSPV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constD0KSPV")
            ntp.Bd.constD0KSPV.constrainToOriginVertex = True
            ntp.Bd.constD0KSPV.Verbose = True
            ntp.Bd.constD0KSPV.daughtersToConstrain = ['D0', 'KS0']
            ntp.Bd.constD0KSPV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constKSPV")
            ntp.Bd.constKSPV.constrainToOriginVertex = True
            ntp.Bd.constKSPV.Verbose = True
            ntp.Bd.constKSPV.daughtersToConstrain = ['KS0']
            ntp.Bd.constKSPV.UpdateDaughters = True

        if ntp in tupleLists["Pi0HH"]:
            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0D0Pi0PV")
            ntp.Bd.constB0D0Pi0PV.constrainToOriginVertex = True
            ntp.Bd.constB0D0Pi0PV.Verbose = True
            ntp.Bd.constB0D0Pi0PV.daughtersToConstrain = ['B0', 'D0', 'pi0']
            ntp.Bd.constB0D0Pi0PV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0Pi0PV")
            ntp.Bd.constB0Pi0PV.constrainToOriginVertex = True
            ntp.Bd.constB0Pi0PV.Verbose = True
            ntp.Bd.constB0Pi0PV.daughtersToConstrain = ['B0', 'pi0']
            ntp.Bd.constB0Pi0PV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0D0Pi0PV")
            ntp.Bd.constBs0D0Pi0PV.constrainToOriginVertex = True
            ntp.Bd.constBs0D0Pi0PV.Verbose = True
            ntp.Bd.constBs0D0Pi0PV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
            ntp.Bd.constBs0D0Pi0PV.daughtersToConstrain = ['B_s0', 'D0', 'pi0']
            ntp.Bd.constBs0D0Pi0PV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0Pi0PV")
            ntp.Bd.constBs0Pi0PV.constrainToOriginVertex = True
            ntp.Bd.constBs0Pi0PV.Verbose = True
            ntp.Bd.constBs0Pi0PV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
            ntp.Bd.constBs0Pi0PV.daughtersToConstrain = ['B_s0', 'pi0']
            ntp.Bd.constBs0Pi0PV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constD0Pi0PV")
            ntp.Bd.constD0Pi0PV.constrainToOriginVertex = True
            ntp.Bd.constD0Pi0PV.Verbose = True
            ntp.Bd.constD0Pi0PV.daughtersToConstrain = ['D0', 'pi0']
            ntp.Bd.constD0Pi0PV.UpdateDaughters = True

            ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constPi0PV")
            ntp.Bd.constPi0PV.constrainToOriginVertex = True
            ntp.Bd.constPi0PV.Verbose = True
            ntp.Bd.constPi0PV.daughtersToConstrain = ['pi0']
            ntp.Bd.constPi0PV.UpdateDaughters = True
        
        ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0PV")
        ntp.Bd.constB0PV.constrainToOriginVertex = True
        ntp.Bd.constB0PV.Verbose = True
        ntp.Bd.constB0PV.daughtersToConstrain = ['B0']
        ntp.Bd.constB0PV.UpdateDaughters = True

        ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constB0D0PV")
        ntp.Bd.constB0D0PV.constrainToOriginVertex = True
        ntp.Bd.constB0D0PV.Verbose = True
        ntp.Bd.constB0D0PV.daughtersToConstrain = ['B0', 'D0']
        ntp.Bd.constB0D0PV.UpdateDaughters = True

        ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constD0PV")
        ntp.Bd.constD0PV.constrainToOriginVertex = True
        ntp.Bd.constD0PV.Verbose = True
        ntp.Bd.constD0PV.daughtersToConstrain = ['D0']
        ntp.Bd.constD0PV.UpdateDaughters = True

        ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0PV")
        ntp.Bd.constBs0PV.constrainToOriginVertex = True
        ntp.Bd.constBs0PV.Verbose = True
        ntp.Bd.constBs0PV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
        ntp.Bd.constBs0PV.daughtersToConstrain = ['B_s0']
        ntp.Bd.constBs0PV.UpdateDaughters = True

        ntp.Bd.addTupleTool("TupleToolDecayTreeFitter/constBs0D0PV")
        ntp.Bd.constBs0D0PV.constrainToOriginVertex = True
        ntp.Bd.constBs0D0PV.Verbose = True
        ntp.Bd.constBs0D0PV.Substitutions = {ntp.Branches["Bd"]: "B_s0"}
        ntp.Bd.constBs0D0PV.daughtersToConstrain = ['B_s0', 'D0']
        ntp.Bd.constBs0D0PV.UpdateDaughters = True

        LoKi_Bd = ntp.Bd.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bd") ### Do we need stuff for Bs0?
        LoKi_Bd.Variables = dict(LoKiBd.items() + LoKiBDK.items())

        LoKi_D0 = ntp.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
        LoKi_D0.Variables =  dict(LoKiD0.items() + LoKiBDK.items())

        if ntp in tupleLists["KSHH"]:
            LoKi_Ks = ntp.Ks.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Ks")
            LoKi_Ks.Variables =  dict(LoKiKs.items() + LoKiBDK.items())

        if ntp in tupleLists["Pi0HH"]:
            LoKi_Pi0 = ntp.Pi0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Pi0")
            LoKi_Pi0.Variables =  dict(LoKiPi0.items() + LoKiBDK.items())

        LoKi_BachK = ntp.BachK.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_BachK")
        LoKi_BachK.Preambulo = ["from LoKiTracks.decorators import *"]
        LoKi_BachK.Variables = LoKih

        LoKi_BachPi = ntp.BachPi.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_BachPi")
        LoKi_BachPi.Preambulo = ["from LoKiTracks.decorators import *"]
        LoKi_BachPi.Variables = LoKih

        LoKi_h1 = ntp.h1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_h1")
        LoKi_h1.Preambulo = ["from LoKiTracks.decorators import *"]
        LoKi_h1.Variables = LoKih

        LoKi_h2 = ntp.h2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_h2")
        LoKi_h2.Preambulo = ["from LoKiTracks.decorators import *"]
        LoKi_h2.Variables = LoKih

        if ntp in tupleLists["HHHH"]:
            LoKi_h3 = ntp.h3.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_h3")
            LoKi_h3.Preambulo = ["from LoKiTracks.decorators import *"]
            LoKi_h3.Variables = LoKih

            LoKi_h4 = ntp.h4.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_h4")
            LoKi_h4.Preambulo = ["from LoKiTracks.decorators import *"]
            LoKi_h4.Variables = LoKih

        if ntp in tupleLists["KSHH"]:
            LoKi_Ksh1 = ntp.Ksh1.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Ksh1") ### Do we need something similar for Pi0g?
            LoKi_Ksh1.Preambulo = ["from LoKiTracks.decorators import *"]
            LoKi_Ksh1.Variables = LoKih

            LoKi_Ksh2 = ntp.Ksh2.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Ksh2")
            LoKi_Ksh2.Preambulo = ["from LoKiTracks.decorators import *"]
            LoKi_Ksh2.Variables = LoKih

        LoKi_evt = ntp.addTupleTool("LoKi::Hybrid::EvtTupleTool/LoKi_evt")
        LoKi_evt.Preambulo = ['from LoKiTracks.decorators import *',
                              'from LoKiNumbers.decorators import *',
                              'from LoKiCore.functions import *']
        LoKi_evt.VOID_Variables = LoKiEvt

    # add event and mc tuple
    tupleLists["all"].append(eventTuple)
    for ntp in mc_tuples: tupleLists["all"].append(ntp)

    # === Cone variables ===#
    # RELINFO('/Event/Bhadron/Phys/<MyLine>/P2ConeVar1' ...
    if inputType == "MDST":
        coneVars = {}
        for b in bDecs:
            for d in dDecs:
                if d in decayCategories["Pi0HH"]:
                    coneVars["{}_{}".format(b, d)] = {
                        # Cone variables
                          "PTASY_1.5"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)".format(b,Pi0HHLine)
                        , "PTASY_1.7"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar2','CONEPTASYM',-1000.)".format(b,Pi0HHLine)
                        , "PTASY_1.0"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar3','CONEPTASYM',-1000.)".format(b,Pi0HHLine)
                    }
                    name = "B02{}_D2{}".format(b, d)
                    LoKi_Bd = tuples[name].Bd.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bd_CONE")
                    LoKi_Bd.Variables = coneVars["{}_{}".format(b, d)]
                if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                    coneVars["{}_{}".format(b, d)] = {
                        # Cone variables
                          "PTASY_1.5"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                        , "PTASY_1.7"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar2','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                        , "PTASY_1.0"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}Beauty2CharmLine/P2ConeVar3','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                    }
                    if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                        coneVars["{}_{}_WS".format(b, d)] = {
                            # Cone variables
                              "PTASY_1.5"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}WSBeauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                            , "PTASY_1.7"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}WSBeauty2CharmLine/P2ConeVar2','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                            , "PTASY_1.0"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2{}WSBeauty2CharmLine/P2ConeVar3','CONEPTASYM',-1000.)".format(b, GetDictKey(decayCategories, d))
                        }
                        for w in signs:
                            name = "B02{}_D2{}{}".format(b, d, w)
                            LoKi_Bd = tuples[name].Bd.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bd_CONE")
                            LoKi_Bd.Variables = coneVars["{}_{}{}".format(b, d, w)]
                    else:
                        name = "B02{}_D2{}".format(b, d)
                        LoKi_Bd = tuples[name].Bd.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bd_CONE")
                        LoKi_Bd.Variables = coneVars["{}_{}".format(b, d)]
                if d in decayCategories["KSHH"]:
                    for t in tracks:
                        coneVars["{}_{}_{}".format(b, d, t)] = {
                            # Cone variables
                              "PTASY_1.5"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}Beauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)".format(b, t)
                            , "PTASY_1.7"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}Beauty2CharmLine/P2ConeVar2','CONEPTASYM',-1000.)".format(b, t)
                            , "PTASY_1.0"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}Beauty2CharmLine/P2ConeVar3','CONEPTASYM',-1000.)".format(b, t)
                        }
                        coneVars["{}_{}_{}_WS".format(b, d, t)] = {
                            # Cone variables
                              "PTASY_1.5"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}WSBeauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)".format(b, t)
                            , "PTASY_1.7"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}WSBeauty2CharmLine/P2ConeVar2','CONEPTASYM',-1000.)".format(b, t)
                            , "PTASY_1.0"       : "RELINFO('/Event/"+stream+"/Phys/B02{}D2KSHH{}WSBeauty2CharmLine/P2ConeVar3','CONEPTASYM',-1000.)".format(b, t)
                        }
                        for w in signs:
                            name = "B02{}_D2{}_{}{}".format(b, d, t, w)
                            LoKi_Bd = tuples[name].Bd.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Bd_CONE")
                            LoKi_Bd.Variables = coneVars["{}_{}_{}{}".format(b, d, t, w)]

    else:
        for b in bDecs:
            for d in dDecs:
                if d in decayCategories["KSHH"]:
                    for t in tracks:
                        for w in ["", "_WS"]:
                            name = "B02{}_D2{}_{}{}".format(b, d, t, w)
                            Cone = tuples[name].Bd.addTupleTool("TupleToolTrackIsolation/Cone")
                            Cone.FillAsymmetry = True
                            Cone.MinConeAngle = 1.0
                            Cone.MaxConeAngle = 1.7
                            Cone.StepSize = 0.1
                            Cone.Verbose = True
                else:
                    if (d not in ["PiK"]) and (d not in ["PiKPiPi"]) and (d not in decayCategories["Pi0HH"]):
                        for w in ["", "_WS"]:
                            name = "B02{}_D2{}{}".format(b, d, w)
                            Cone = tuples[name].Bd.addTupleTool("TupleToolTrackIsolation/Cone")
                            Cone.FillAsymmetry = True
                            Cone.MinConeAngle = 1.0
                            Cone.MaxConeAngle = 1.7
                            Cone.StepSize = 0.1
                            Cone.Verbose = True
                    else:
                        name = "B02{}_D2{}".format(b, d)
                        Cone = tuples[name].Bd.addTupleTool("TupleToolTrackIsolation/Cone")
                        Cone.FillAsymmetry = True
                        Cone.MinConeAngle = 1.0
                        Cone.MaxConeAngle = 1.7
                        Cone.StepSize = 0.1
                        Cone.Verbose = True


    # ==========================
    # Momentum Scaler
    # ==========================
    if not simulation:
        from Configurables import TrackScaleState
        scaler = TrackScaleState('StateScale')
        if inputType == "MDST":
            scaler.RootInTES = '/Event/{0}/'.format(stream)
        DaVinci().appendToMainSequence([scaler])

    #========================================#
    #=== Add tuples to list of algorithms ===#
    #========================================#
    DaVinci().appendToMainSequence(tupleLists["all"])
    #mc_tuples.append(eventTuple)
    #DaVinci().appendToMainSequence(mc_tuples)