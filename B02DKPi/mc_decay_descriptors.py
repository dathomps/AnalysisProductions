                        # HH
                        # DKPi
mc_decay_descriptors = {"11164057": {"decay":  "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}pi+ ${h2}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${D0}(D0  => ${h1}pi+ ${h2}pi-) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        "11164047": {"decay":  "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K+  ${h2}K-)  ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K+  ${h2}K-)  ${BachK}K- ${BachPi}pi+]CC]"},  # SIGNAL
                        "11164072": {"decay":  "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        # Bs2DKPi
                        "13164062": {"decay":  "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${BachK}K+ ${BachPi}pi-]CC]"},
                        "11164073": {"decay":  "[${Bd}[[B0]nos    => ${D0}(D0 => ${h1}K-  ${h2}pi+) ${BachK}K+ ${BachPi}pi-]CC,"
					                            "${Bd}[[B0]os      => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachK}K- ${BachPi}pi+]CC]"},
                        "12265002": {"decay":  "${Bu}[[B+]nos    ==> ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12163001": {"decay":  "${Bu}[[B+]nos    => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachPi}pi+]CC"},
                        "12163011": {"decay":  "${Bu}[[B+]nos    => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachK}K+]CC"},
                        
                        
                        #BdDstKPi
                        "11364424": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK}K- ${BachPi}pi+]CC]"}, # do we need pi0 => gamma gamma?
                        "11364425": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK}K- ${BachPi}pi+]CC]"}, 
                        "11164611": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${g}gamma)                            ${BachK}K+ ${BachPi}pi-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachK}K- ${BachPi}pi+]CC]"}, 
                        
                        "13364402": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"}, 
                        "13364403": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"}, 
                        "13164601": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 =>  ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  =>  ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"}, 

                        "11164056": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-)                                      ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}pi+ ${h2}pi-)                                      ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11164055": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}K+  ${h2}K-)                                       ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}K+  ${h2}K-)                                       ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11164063": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}K+  ${h2}pi-)                                      ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}K-  ${h2}pi+)                                      ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11364420": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC]"}, 
                        "11364421": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC]"}, 
                        "11164601": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachPi1}pi- ${BachPi2}pi+]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachPi1}pi- ${BachPi2}pi+]CC]"}, 
                        "11164088": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-)                                      ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}pi+ ${h2}pi-)                                      ${BachK1}K+   ${BachK2}K-]CC]"},
                        "11164089": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}K+  ${h2}K-)                                       ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}K+  ${h2}K-)                                       ${BachK1}K+   ${BachK2}K-]CC]"},
                        "11164086": {"decay":  "[${Bd}[[B0]nos =>                         ${D0}(D~0 =>  ${h1}K+  ${h2}pi-)                                      ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  =>                         ${D0}(D0  =>  ${h1}K-  ${h2}pi+)                                      ${BachK1}K+   ${BachK2}K-]CC]"},
                        "11364422": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC]"}, 
                        "11364423": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC]"}, 
                        "11164621": {"decay1": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+   ${BachK2}K-]CC]",
                                     "decay2": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 =>  ${h1}K+  ${h2}pi-) ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC,"
                                                "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  =>  ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachK1}K+   ${BachK2}K-]CC]"}, 

                        "13164078": {"decay":  "[${Bs0}[[B_s0]nos =>                        ${D0}(D~0 => ${h1}pi+ ${h2}pi-)                                       ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  =>                        ${D0}(D0  => ${h1}pi+ ${h2}pi-)                                       ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13164079": {"decay":  "[${Bs0}[[B_s0]nos =>                        ${D0}(D~0 => ${h1}K+  ${h2}K-)                                        ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  =>                        ${D0}(D0  => ${h1}K+  ${h2}K-)                                        ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13164076": {"decay":  "[${Bs0}[[B_s0]nos =>                        ${D0}(D~0 => ${h1}K+  ${h2}pi-)                                       ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  =>                        ${D0}(D0  => ${h1}K-  ${h2}pi+)                                       ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13364400": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi-) ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC]"}, 
                        "13364401": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}K-)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}K-)  ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC]"}, 
                        "13164611": {"decay1": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]",
                                     "decay2": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC,"
                                                "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachK1}K+ ${BachK2}K-]CC]"}, 
                        "15164051": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}pi- ${h2}pi+)                                       ${BachP}p+ ${BachPi}pi-]CC"},
                        "15164061": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}K-  ${h2}K+)                                        ${BachP}p+ ${BachPi}pi-]CC"},
                        "15164012": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}K-  ${h2}pi+)                                       ${BachP}p+ ${BachPi}pi-]CC"},
                        "15364400": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}pi- ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}pi- ${h2}pi+) ${g}gamma)                            ${BachP}p+ ${BachPi}pi-]CC"}, 
                        "15364401": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}K+)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}K+)  ${g}gamma)                            ${BachP}p+ ${BachPi}pi-]CC"}, 
                        "15164601": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachP}p+ ${BachPi}pi-]CC"}, 
                        "15164070": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}pi- ${h2}pi+)                                       ${BachP}p+ ${BachK}K-]CC"},
                        "15164080": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}K-  ${h2}K+)                                        ${BachP}p+ ${BachK}K-]CC"},
                        "15164022": {"decay":   "${Lb}[Lambda_b0 =>                         ${D0}(D0  => ${h1}K-  ${h2}pi+)                                       ${BachP}p+ ${BachK}K-]CC"},
                        "15364410": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}pi- ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}pi- ${h2}pi+) ${g}gamma)                            ${BachP}p+ ${BachK}K-]CC"}, 
                        "15364420": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}K+)  ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}K+)  ${g}gamma)                            ${BachP}p+ ${BachK}K-]CC"}, 
                        "15164611": {"decay1":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}pi+) ${Pi0}(pi0 => ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC",
                                     "decay2":  "${Lb}[Lambda_b0 =>  ${Dstar}(D*(2007)0 =>  ${D0}(D0  => ${h1}K-  ${h2}pi+) ${g}gamma)                            ${BachP}p+ ${BachK}K-]CC"}, 


                        # HHHH
                        # BdDKPi
                        "11166071": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        "11166072": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        "11166073": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        # BdDstKPi
                        "11166271": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166272": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166273": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166471": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166472": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166473": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]"},
                        # Bs0DstKpi
                        "13166271": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166272": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166273": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166471": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166472": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166473": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]"},
                        # BdDPiPi
                        "11166074": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166075": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166076": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        # BdDstPiPi
                        "11166274": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166275": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166276": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166474": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166475": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166476": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        # BdDKK
                        "11166077": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166078": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166079": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # BdDstKK
                        "11166277": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+ ${h2}pi-  ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC," 
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K- ${h2}pi+  ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166278": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K- ${h2}pi+  ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+ ${h2}pi-  ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166279": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166477": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+ ${h2}pi-  ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K- ${h2}pi+  ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166478": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K- ${h2}pi+  ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+ ${h2}pi-  ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166479": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # Bs0DKK
                        "13166077": {"decay": "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166078": {"decay": "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166079": {"decay": "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # Bs0DstKK
                        "13166277": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166278": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166279": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166477": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166478": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166479": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # LbDpPi
                        "15166074": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166075": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166076": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachPi}pi-]CC"},
                        # LbDstpPi
                        "15166274": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166275": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166276": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166474": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166475": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166476": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC"},
                        # LbDKp
                        "15166077": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachK}K-]CC"},
                        "15166078": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachK}K-]CC"},
                        "15166079": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${BachP}p+ ${BachK}K-]CC"},
                        # LbDstKp
                        "15166277": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachK}K-]CC"},
                        "15166278": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachK}K-]CC"},
                        "15166279": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${g}gamma)                             ${BachP}p+ ${BachK}K-]CC"},
                        "15166477": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K-  ${h2}pi+ ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC"},
                        "15166478": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}K+  ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC"},
                        "15166479": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${h1}pi+ ${h2}pi- ${h3}pi+ ${h4}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC"},

                        # KSHH
                        "11166111": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        "11166112": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK}K- ${BachPi}pi+]CC]"}, # SIGNAL
                        # B0DstKPi
                        "11166330": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166331": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166530": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]"},
                        "11166531": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC]"},
                        # Bs0DstKPi
                        "13166330": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166331": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166530": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]"},
                        "13166531": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K- ${BachPi}pi+]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK}K+ ${BachPi}pi-]CC]"},
                        # B0DPiPi
                        "11166117": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166118": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        ## All KSHH modes above are important backgrounds for our double Dalitz toy studies.
                        # B0DstPiPi
                        "11166332": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166333": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166532": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        "11166533": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi-]CC]"},
                        # B0DKK
                        "11166119": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166120": {"decay": "[${Bd}[[B0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK1}K+ ${BachK2}K-]CC]"},
                        # B0DstKK
                        "11166334": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166335": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166534": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "11166535": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # Bs0DKK
                        "13166119": {"decay": "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166120": {"decay": "[${Bs0}[[B_s0]nos => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachK1}K+ ${BachK2}K-]CC]"},
                        # Bs0DstKK
                        "13166334": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166335": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166534": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        "13166535": {"decay": "[${Bs0}[[B_s0]nos => ${Dstar}(D*(2007)~0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC,"
                                               "${Bs0}[[B_s0]os  => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachK1}K+ ${BachK2}K-]CC]"},
                        # LbDpPi
                        "15166117": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166118": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachP}p+ ${BachPi}pi-]CC"},
                        # LbDstpPi
                        "15166317": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166318": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166517": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC"},
                        "15166518": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachPi}pi-]CC"},
                        # LbDKp
                        "15166119": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${BachP}p+ ${BachK}K-]CC"},
                        "15166120": {"decay": "${Lb}[Lambda_b0 => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${BachP}p+ ${BachK}K-]CC"},
                        # LbDstKp
                        "15166319": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${g}gamma)                             ${BachP}p+ ${BachK}K-]CC"},
                        "15166320": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${g}gamma)                             ${BachP}p+ ${BachK}K-]CC"},
                        "15166519": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+ ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC"},
                        "15166520": {"decay": "${Lb}[Lambda_b0 => ${Dstar}(D*(2007)0  => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}K+  ${h2}K-)  ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachP}p+ ${BachK}K-]CC"},
                        
                        
                        # add_bkg
                        "12265021": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12265027": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}K+  ${h2}K-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12265028": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}pi+  ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12267100": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12265042": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${BachK}K+ ${BachPi1}pi- ${BachPi2}pi+]CC"},
                        "12265043": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}K+  ${h2}K-) ${BachK}K+ ${BachPi1}pi- ${BachPi2}pi+]CC"},
                        "12265044": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${h1}pi+  ${h2}pi-) ${BachK}K+ ${BachPi1}pi- ${BachPi2}pi+]CC"},
                        "12267150": {"decay": "${Bu}[[B+]nos ==> ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${BachK}K+ ${BachPi1}pi- ${BachPi2}pi+]CC"},
                        "12265203": {"decay": "${Bu}[[B+]nos ==> ${Dstar}(D*(2007)0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${g}gamma) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12265403": {"decay": "${Bu}[[B+]nos ==> ${Dstar}(D*(2007)0 => ${D0}(D~0 => ${h1}K+  ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12267310": {"decay": "${Bu}[[B+]nos ==> ${Dstar}(D*(2007)0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${g}gamma) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "12267510": {"decay": "${Bu}[[B+]nos ==> ${Dstar}(D*(2007)0 => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${Pi0}(pi0 =>  ${g1}gamma ${g2}gamma)) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC"},
                        "11266013": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2010)- => ${D0}(D~0 => ${h1}K+ ${h2}pi-) ${DstarPi}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2010)+ => ${D0}(D0  => ${h1}K- ${h2}pi+) ${DstarPi}pi+) ${BachPi1}pi- ${BachPi2}pi+ ${BachPi3}pi-]CC]"},
                        "11268110": {"decay": "[${Bd}[[B0]nos => ${Dstar}(D*(2010)- => ${D0}(D~0 => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${DstarPi}pi-) ${BachPi1}pi+ ${BachPi2}pi- ${BachPi3}pi+]CC,"
                                               "${Bd}[[B0]os  => ${Dstar}(D*(2010)+ => ${D0}(D0  => ${Ks}(KS0 => ${Ksh1}pi+ ${Ksh2}pi-) ${h1}pi+  ${h2}pi-) ${DstarPi}pi+) ${BachPi1}pi- ${BachPi2}pi+ ${BachPi3}pi-]CC]"},
                        }
