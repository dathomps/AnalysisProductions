from B02DKPi import B02DKPi_davinci_options
from Configurables import DaVinci

# Data/MC specific variables
year = "11"
requiredDecays = ["KSHH", "HHHH"]
simulation = False
inputType="MDST"
stream = "Bhadron"
nEvents = -1

# Set up the tuples
tuple_lists = B02DKPi_davinci_options.configure_tuple(year, requiredDecays, stream, simulation, inputType, nEvents)
