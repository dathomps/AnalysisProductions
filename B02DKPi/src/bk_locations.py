import subprocess
import argparse

parser = argparse.ArgumentParser(description="Writes a file of the bookkeeping locations of each MC sample, ordered per year per sample.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--test", default=False, action="store_const", const=True, help="Test the find_locations mode by only running on one MC sample.")
args = parser.parse_args()

modes = {}

## HH Modes
modes["B2DKPi_D2PiPi"]=11164057
modes["B2DKPi_D2KK"]=11164047
modes["B2D~KPi_D2KPi"]=11164072

modes["B2DstKPi_D2PiPi"]=11364424
modes["B2DstKPi_D2KK"]=11364425
modes["B2DstKPi_D2KPi"]=11164611

modes["Bs2DstKPi_D2PiPi"]=13364402
modes["Bs2DstKPi_D2KK"]=13364403
modes["Bs2DstKPi_D2KPi"]=13164601

modes["B2DPiPi_D2PiPi"]=11164056
modes["B2DPiPi_D2KK"]=11164055
modes["B2DPiPi_D2KPi"]=11164063

modes["B2DstPiPi_D2PiPi"]=11364420
modes["B2DstPiPi_D2KK"]=11364421
modes["B2DstPiPi_D2KPi"]=11164601

modes["B2DKK_D2PiPi"]=11164088
modes["B2DKK_D2KK"]=11164089
modes["B2DKK_D2KPi"]=11164086

modes["B2DstKK_D2PiPi"]=11364422
modes["B2DstKK_D2KK"]=11364423
modes["B2DstKK_D2KPi"]=11164621

modes["Bs2DKK_D2PiPi"]=13164078
modes["Bs2DKK_D2KK"]=13164079
modes["Bs2DKK_D2KPi"]=13164076

modes["Bs2DstKK_D2PiPi"]=13364400
modes["Bs2DstKK_D2KK"]=13364401
modes["Bs2DstKK_D2KPi"]=13164611

modes["Lb2DPPi_D2PiPi"]=15164051
modes["Lb2DPPi_D2KK"]=15164061
modes["Lb2DPPi_D2KPi"]=15164012

modes["Lb2DstPPi_D2PiPi"]=15364400
modes["Lb2DstPPi_D2KK"]=15364401
modes["Lb2DstPPi_D2KPi"]=15164601

modes["Lb~2DKP~_D2PiPi"]=15164070
modes["Lb~2DKP~_D2KK"]=15164080
modes["Lb~2DKP~_D2KPi"]=15164022

modes["Lb~2DstKP~_D2PiPi"]=15364410
modes["Lb~2DstKP~_D2KK"]=15364420
modes["Lb~2DstKP~_D2KPi"]=15164611

## HHHH Modes
modes["B2DKPi_D2KPiPiPi"]=11166071
modes["B2DKPi_D2PiKPiPi"]=11166072
modes["B2DKPi_D2PiPiPiPi"]=11166073

modes["B2DstKPi_Dst2Dg_D2KPiPiPi"]=11166271
modes["B2DstKPi_Dst2Dg_D2PiKPiPi"]=11166272
modes["B2DstKPi_Dst2Dg_D2PiPiPiPi"]=11166273
modes["B2DstKPi_Dst2Dpi0_D2KPiPiPi"]=11166471
modes["B2DstKPi_Dst2Dpi0_D2PiKPiPi"]=11166472
modes["B2DstKPi_Dst2Dpi0_D2PiPiPiPi"]=11166473

modes["Bs2DstKPi_Dst2Dg_D2KPiPiPi"]=13166271
modes["Bs2DstKPi_Dst2Dg_D2PiKPiPi"]=13166272
modes["Bs2DstKPi_Dst2Dg_D2PiPiPiPi"]=13166273
modes["Bs2DstKPi_Dst2Dpi0_D2KPiPiPi"]=13166471
modes["Bs2DstKPi_Dst2Dpi0_D2PiKPiPi"]=13166472
modes["Bs2DstKPi_Dst2Dpi0_D2PiPiPiPi"]=13166473

modes["B2DPiPi_D2KPiPiPi"]=11166074
modes["B2DPiPi_D2PiKPiPi"]=11166075
modes["B2DPiPi_D2PiPiPiPi"]=11166076

modes["B2DstPiPi_Dst2Dg_D2KPiPiPi"]=11166274
modes["B2DstPiPi_Dst2Dg_D2PiKPiPi"]=11166275
modes["B2DstPiPi_Dst2Dg_D2PiPiPiPi"]=11166276
modes["B2DstPiPi_Dst2Dpi0_D2KPiPiPi"]=11166474
modes["B2DstPiPi_Dst2Dpi0_D2PiKPiPi"]=11166475
modes["B2DstPiPi_Dst2Dpi0_D2PiPiPiPi"]=11166476

modes["B2DKK_D2KPiPiPi"]=11166077
modes["B2DKK_D2PiKPiPi"]=11166078
modes["B2DKK_D2PiPiPiPi"]=11166079

modes["B2DstKK_Dst2Dg_D2KPiPiPi"]=11166277
modes["B2DstKK_Dst2Dg_D2PiKPiPi"]=11166278
modes["B2DstKK_Dst2Dg_D2PiPiPiPi"]=11166279
modes["B2DstKK_Dst2Dpi0_D2KPiPiPi"]=11166477
modes["B2DstKK_Dst2Dpi0_D2PiKPiPi"]=11166478
modes["B2DstKK_Dst2Dpi0_D2PiPiPiPi"]=11166479

modes["Bs2DKK_D2KPiPiPi"]=13166077
modes["Bs2DKK_D2PiKPiPi"]=13166078
modes["Bs2DKK_D2PiPiPiPi"]=13166079

modes["Bs2DstKK_Dst2Dg_D2KPiPiPi"]=13166277
modes["Bs2DstKK_Dst2Dg_D2PiKPiPi"]=13166278
modes["Bs2DstKK_Dst2Dg_D2PiPiPiPi"]=13166279
modes["Bs2DstKK_Dst2Dpi0_D2KPiPiPi"]=13166477
modes["Bs2DstKK_Dst2Dpi0_D2PiKPiPi"]=13166478
modes["Bs2DstKK_Dst2Dpi0_D2PiPiPiPi"]=13166479

modes["Lb2DPPi_D2KPiPiPi"]=15166074
modes["Lb2DPPi_D2PiKPiPi"]=15166075
modes["Lb2DPPi_D2PiPiPiPi"]=15166076

modes["Lb2DstPPi_Dst2Dg_D2KPiPiPi"]=15166274
modes["Lb2DstPPi_Dst2Dg_D2PiKPiPi"]=15166275
modes["Lb2DstPPi_Dst2Dg_D2PiPiPiPi"]=15166276
modes["Lb2DstPPi_Dst2Dpi0_D2KPiPiPi"]=15166474
modes["Lb2DstPPi_Dst2Dpi0_D2PiKPiPi"]=15166475
modes["Lb2DstPPi_Dst2Dpi0_D2PiPiPiPi"]=15166476

modes["Lb~2DKP~_D2KPiPiPi"]=15166077
modes["Lb~2DKP~_D2PiKPiPi"]=15166078
modes["Lb~2DKP~_D2PiPiPiPi"]=15166079

modes["Lb~2DstKP~_Dst2Dg_D2KPiPiPi"]=15166277
modes["Lb~2DstKP~_Dst2Dg_D2PiKPiPi"]=15166278
modes["Lb~2DstKP~_Dst2Dg_D2PiPiPiPi"]=15166279
modes["Lb~2DstKP~_Dst2Dpi0_D2KPiPiPi"]=15166477
modes["Lb~2DstKP~_Dst2Dpi0_D2PiKPiPi"]=15166478
modes["Lb~2DstKP~_Dst2Dpi0_D2PiPiPiPi"]=15166479

## KSHH Modes
modes["B2DKPi_D2KSPiPi"]=11166111
modes["B2DKPi_D2KSKK"]=11166112

modes["B2DstKPi_Dst2Dg_D2KS0PiPi"]=11166330
modes["B2DstKPi_Dst2Dg_D2KS0KK"]=11166331
modes["B2DstKPi_Dst2Dpi0_D2KS0PiPi"]=11166530
modes["B2DstKPi_Dst2Dpi0_D2KS0KK"]=11166531


modes["Bs2DstKPi_Dst2Dg_D2KS0PiPi"]=13166330
modes["Bs2DstKPi_Dst2Dg_D2KS0KK"]=13166331
modes["Bs2DstKPi_Dst2Dpi0_D2KS0PiPi"]=13166530
modes["Bs2DstKPi_Dst2Dpi0_D2KS0KK"]=13166531

modes["B2DPiPi_D2KS0PiPi"]=11166117
modes["B2DPiPi_D2KS0KK"]=11166118

modes["B2DstPiPi_Dst2Dg_D2KS0PiPi"]=11166332
modes["B2DstPiPi_Dst2Dg_D2KS0KK"]=11166333
modes["B2DstPiPi_Dst2Dpi0_D2KS0PiPi"]=11166532
modes["B2DstPiPi_Dst2Dpi0_D2KS0KK"]=11166533

modes["B2DKK_D2KS0PiPi"]=11166119
modes["B2DKK_D2KS0KK"]=11166120

modes["B2DstKK_Dst2Dg_D2KS0PiPi"]=11166334
modes["B2DstKK_Dst2Dg_D2KS0KK"]=11166335
modes["B2DstKK_Dst2Dpi0_D2KS0PiPi"]=11166534
modes["B2DstKK_Dst2Dpi0_D2KS0KK"]=11166535

modes["Bs2DKK_D2KS0PiPi"]=13166119
modes["Bs2DKK_D2KS0KK"]=13166120

modes["Bs2DstKK_Dst2Dg_D2KS0PiPi"]=13166334
modes["Bs2DstKK_Dst2Dg_D2KS0KK"]=13166335
modes["Bs2DstKK_Dst2Dpi0_D2KS0PiPi"]=13166534
modes["Bs2DstKK_Dst2Dpi0_D2KS0KK"]=13166535

modes["Lb2DPPi_D2KS0PiPi"]=15166117
modes["Lb2DPPi_D2KS0KK"]=15166118

modes["Lb2DstPPi_Dst2Dg_D2KS0PiPi"]=15166317
modes["Lb2DstPPi_Dst2Dg_D2KS0KK"]=15166318
modes["Lb2DstPPi_Dst2Dpi0_D2KS0PiPi"]=15166517
modes["Lb2DstPPi_Dst2Dpi0_D2KS0KK"]=15166518

modes["Lb~2DKP~_D2KS0PiPi"]=15166119
modes["Lb~2DKP~_D2KS0KK"]=15166120

modes["Lb~2DstKP~_Dst2Dg_D2KS0PiPi"]=15166319
modes["Lb~2DstKP~_Dst2Dg_D2KS0KK"]=15166320
modes["Lb~2DstKP~_Dst2Dpi0_D2KS0PiPi"]=15166519
modes["Lb~2DstKP~_Dst2Dpi0_D2KS0KK"]=15166520

modes["Bs2DKPi_D2KPi"] = 13164062
modes["B2DKPi_D2KPi"] = 11164073
modes["Bu2DPiPiPi_D2KPi"] = 12265002
modes["Bu2DPi_D2KPi"] = 12163001
modes["Bu2DK_D2KPi"] = 12163011


# additional bkgs
modes["Bu2DPiPiPi_D2KPi"] = 12265021
modes["Bu2DPiPiPi_D2KK"] = 12265027
modes["Bu2DPiPiPi_D2PiPi"] = 12265028
modes["Bu2DPiPiPi_D2KS0PiPi"] = 12267100
modes["Bu2DKPiPi_D2KPi"] = 12265042
modes["Bu2DKPiPi_D2KK"] = 12265043
modes["Bu2DKPiPi_D2PiPi"] = 12265044
modes["Bu2DKPiPi_D2KS0PiPi"] = 12267150
modes["Bu2Dst0PiPiPi_Dst02Dg_D2KPi"] = 12265203
modes["Bu2Dst0PiPiPi_Dst02Dpi0_D2KPi"] = 12265403
modes["Bu2Dst0PiPiPi_Dst02Dg_D2KS0PiPi"] = 12267310
modes["Bu2Dst0PiPiPi_Dst02Dpi0_D2KS0PiPi"] = 12267510
modes["Bd2Dst-PiPiPi_Dst-2Dpi-_D2KPi"] = 11266013
modes["Bd2Dst-PiPiPi_Dst-2Dpi-_D2KS0PiPi"] = 11268110


def bk_locations():
    counter=0
    previous_year = 11
    outf_name = "B02DKPi/bk_locations.txt"
    outf = open(outf_name, "w")

    for decay in modes:
        if (not args.test) or counter==0:
            bk_locations = []
            output = subprocess.check_output(["lb-dirac", "dirac-bookkeeping-decays-path", str(modes[decay])]).decode()
            output=output.split("\n")
            outf.write("Decay: "+decay+" "+"Event Type: "+str(modes[decay])+"\n")
            for line in output:
                if line!="":
                    bk_location = line.split(" ")[0]
                    year = bk_location.split("/")[2]
                    bk_locations.append([year, bk_location])
            bk_locations = sorted(bk_locations, key=lambda x: x[0])
            for locs in bk_locations:
                if locs[0]!=previous_year:
                    outf.write("\n")
                    previous_year=locs[0]
                outf.write(locs[1])
                #print(locs[1])
                outf.write("\n")
            outf.write("\n\n")
            counter+=1

    outf.close()
    print(f"bk_locations() was successful, output can be found in {outf_name}.txt")

bk_locations()
