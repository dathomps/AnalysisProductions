import strictyaml
import os
import math

def check_end(l):
    if l[-1] == " ":
        checked_line = check_end(l[:-1])
        return checked_line
    elif l[-1] == "\n":
        checked_line = check_end(l[:-1])
        return checked_line
    else:
        return l

def check_archived_mc():


    n_archive_files = 14
    # split the master list into smaller files to allow upload to GitLab
    # this list can not be uploaded to GitLab due to its size but can be constructed from https://its.cern.ch/jira/browse/LBOPG-131
    src = "./B02DKPi/src/"
    if os.path.isfile(f"{src}archived_MC.txt"):
        with open(f"{src}/archived_MC.txt", "r") as master_list:
            counter = 0
            line_list = master_list.readlines()
            for i in range(n_archive_files):
                with open(f"{src}archived_MC_{i}.txt", "w") as new_file:
                    for l in line_list[counter:counter+1000]:
                        new_file.write(l)
                        counter+=1


    archived_MC = [f"{src}archived_MC_{i}.txt" for i in range(n_archive_files)]
    
    yaml_file = "./B02DKPi/info.yaml"

    bookkeeping_to_job = {}

    with open(yaml_file, "r") as yaml:
        data = yaml.read()
        jobs = strictyaml.load(data, strictyaml.Any())
        for job in jobs:
            if job.data!="defaults":
                bookkeeping_to_job[jobs[job].data["input"]["bk_query"]] = job.data

    counter=0
    with open("./B02DKPi/required_MC.txt", "w") as outf:
        for inf in archived_MC:
            with open(inf, "r") as archive_list:
                for l in archive_list:
                    checked_line = check_end(l)
                    if checked_line in bookkeeping_to_job:
                        outf.write(f"{checked_line}\n")
    
if __name__ == "__main__":
    check_archived_mc()