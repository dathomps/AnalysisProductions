import argparse

parser = argparse.ArgumentParser(description="Writes out the info.yaml for the AP.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--check_locations", default=False, action="store_const", const=True, help="Check that the bk locations used in info.yaml exist.")
parser.add_argument("--write_info_yaml", default=False, action="store_const", const=True, help="Writes the info.yaml.")
parser.add_argument("--write_all_jobs", default=False, action="store_const", const=True, help="Writes out all jobs even if the bookkeeping location does not exist.")
parser.add_argument("--write_options", default=False, action="store_const", const=True, help="Writes out options files for each job if a suitable one does not exist")
parser.add_argument("--recreate_options", default=False, action="store_const", const=True, help="Forces it to recreate any options files that already exist.")

parser.add_argument("--data", default=False, action="store_const", const=True, help="Includes data jobs")
parser.add_argument("--mc", default=False, action="store_const", const=True, help="Includes ALL MC jobs")
parser.add_argument("--mc_signal", default=False, action="store_const", const=True, help="Includes ALL signal MC jobs")
parser.add_argument("--mc_HH", default=False, action="store_const", const=True, help="Includes bkg D2HH MC jobs")
parser.add_argument("--mc_HHHH", default=False, action="store_const", const=True, help="Includes bkg D2HHHH MC jobs")
parser.add_argument("--mc_KSHH", default=False, action="store_const", const=True, help="Includes bkg D2KSHH MC jobs")
parser.add_argument("--add_bkg", default=False, action="store_const", const=True, help="Only run on additional bkgs")

parser.add_argument('--years', nargs="+", default = ["11", "12", "15", "16", "17", "18"], help='Select which years from 11,12,15,16,17,18 to run on.')
args = parser.parse_args()

# The following block can be removed when the archived MC is available.
archived_MC = []
if "12" in args.years:
    with open("./B02DKPi/required_MC.txt") as archive_list:
        for l in archive_list:
            archived_MC.append(l[:-1])
print(f"{archived_MC = }")

if (not args.check_locations) and (not args.write_info_yaml):
    print("No programme mode chosen, defaulting to checking locations.")
    print("Use `-h` to learn what modes are available")
    args.check_locations = True

if (not args.write_info_yaml) and args.write_all_jobs: args.write_all_jobs = False

if args.mc:
    args.mc_signal = True
    args.mc_HH = True
    args.mc_HHHH = True
    args.mc_KSHH = True

# In event type tuples the first entry is the decay type,
# the second entry is the list of years for which a MC sample is available,
# the third entry is a dictionary containing any changes that need to be made to
# the settings for any years a sample is available for
# Comments next to each Decay ID in the D2HH event types list the years that we have requested,
# we don't need this for any of the other modes since we requested samples for each year for each of these modes.


# The comments here show the settings for samples that exist but for which we have requested a new sample such that we have the desired number of events.
# (11164057, [12], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
# (11164047, [12], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
# (11164072, [12,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},

D2HH_signal = [(11164057, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18 SIGNAL
               (11164047, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18 SIGNAL
               (11164072, [11, 12, 17,18], {"17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}, # 11, 12 SIGNAL
                                             "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}})]
                                            #{"17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # 11, 12 SIGNAL
                                            #"18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}})]

D2HH_bkg = [(11364424, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (11364425, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (11164611, [11, 12], {}), # TODO RESTRIP , 16], {"16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}), # 11, 12

            (13364402, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (13364403, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (13164601, [12], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
                                 # TODO add 16 to year list and RESTRIP "16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
                                 #"16": {"sim": "09c", "turbo": "/Turbo03", "strip": "28r1Filtered", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # Is this fine or should we use the ALLSTREAMS version since it has more recent stripping? I think this one since it has the B02D0PIPI stream so I think was used in the same analysis as most of these preexisting samples, also I think there are actually some more years already existing?

            (11164056, [12,15,16,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "15": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "16": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "17": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "18": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
            (11164055, [12,15,16,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "15": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "16": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "17": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "18": {"sim": "09i-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
            (11164063, [12,15,16,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "15": {"sim": "09j-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "16": {"sim": "09j-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "17": {"sim": "09j-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                          "18": {"sim": "09j-ReDecay01", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),

            (11364420, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (11364421, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (11164601, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18

            (11164088, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (11164089, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (11164086, [12,15,17,18], {"12": {"sim": "09d", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                       "15": {"sim": "09i-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "MDST"},
                                       "17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"},
                                       "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),

            (11364422, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (11364423, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (11164621, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18

            (13164078, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (13164079, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (13164076, [11, 12, 17,18], {"17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}, # 11, 12
                                 "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),

            (13364400, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (13364401, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (13164611, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18

            (15164051, [12,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                    "17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"},
                                    "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),
            (15164061, [12,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                    "17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"},
                                    "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),
            (15164012, [12,17,18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                    "17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"},
                                    "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),

            (15364400, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (15364401, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18
            (15164601, [12, 15, 16, 17, 18], {"12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # 15, 16, 17, 18

            (15164070, [11, 12, 17,18], {"17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}, # 11, 12
                                 "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),
            (15164080, [11, 12, 17,18], {"17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}, # 11, 12
                                 "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),
            (15164022, [11, 12, 17,18], {"17": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}, # 11, 12
                                 "18": {"sim": "09f-ReDecay01", "stream": "B2DHH.STRIP", "file_type": "DST"}}),

            (15364410, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (15364420, [11,12,15,16,17,18], {}), # 11, 12, 15, 16, 17, 18
            (15164611, [11,12,15,16,17,18], {})  # 11, 12, 15, 16, 17, 18
            ]

D2HH_event_types = []
if args.mc_signal: D2HH_event_types.extend(D2HH_signal)
if args.mc_HH:  D2HH_event_types.extend(D2HH_bkg)

D2HH_year_settings = [(11, 3500, "Nu2", "09j-ReDecay01", "0x40760037", "14c", "", "21r1Filtered", "B02D0KPI.STRIP", "MDST"),
                      (12, 4000, "Nu2.5", "09j-ReDecay01", "0x409f0045", "14c", "", "21Filtered", "B02D0KPI.STRIP", "MDST"),
                      (15, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (16, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (17, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (18, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34Filtered", "B02D0KPI.STRIP", "MDST"),
                      ]

D2HHHH_signal = [(11166071, [11,12,15,16,17,18], {}), # SIGNAL
                 (11166072, [11,12,15,16,17,18], {}), # SIGNAL
                 (11166073, [11,12,15,16,17,18], {})] # SIGNAL

D2HHHH_bkg = [(11166271, [11,12,15,16,17,18], {}),
              (11166272, [11,12,15,16,17,18], {}),
              (11166273, [11,12,15,16,17,18], {}),
              (11166471, [11,12,15,16,17,18], {}),
              (11166472, [11,12,15,16,17,18], {}),
              (11166473, [11,12,15,16,17,18], {}),

              (13166271, [11,12,15,16,17,18], {}),
              (13166272, [11,12,15,16,17,18], {}),
              (13166273, [11,12,15,16,17,18], {}),
              (13166471, [11,12,15,16,17,18], {}),
              (13166472, [11,12,15,16,17,18], {}),
              (13166473, [11,12,15,16,17,18], {}),

              (11166074, [11,12,15,16,17,18], {}),
              (11166075, [11,12,15,16,17,18], {}),
              (11166076, [11,12,15,16,17,18], {}),

              (11166274, [11,12,15,16,17,18], {}),
              (11166275, [11,12,15,16,17,18], {}),
              (11166276, [11,12,15,16,17,18], {}),
              (11166474, [11,12,15,16,17,18], {}),
              (11166475, [11,12,15,16,17,18], {}),
              (11166476, [11,12,15,16,17,18], {}),

              (11166077, [11,12,15,16,17,18], {}),
              (11166078, [11,12,15,16,17,18], {}),
              (11166079, [11,12,15,16,17,18], {}),

              (11166277, [11,12,15,16,17,18], {}),
              (11166278, [11,12,15,16,17,18], {}),
              (11166279, [11,12,15,16,17,18], {}),
              (11166477, [11,12,15,16,17,18], {}),
              (11166478, [11,12,15,16,17,18], {}),
              (11166479, [11,12,15,16,17,18], {}),

              (13166077, [11,12,15,16,17,18], {}),
              (13166078, [11,12,15,16,17,18], {}),
              (13166079, [11,12,15,16,17,18], {}),

              (13166277, [11,12,15,16,17,18], {}),
              (13166278, [11,12,15,16,17,18], {}),
              (13166279, [11,12,15,16,17,18], {}),
              (13166477, [11,12,15,16,17,18], {}),
              (13166478, [11,12,15,16,17,18], {}),
              (13166479, [11,12,15,16,17,18], {}),

              (15166074, [11,12,15,16,17,18], {}),
              (15166075, [11,12,15,16,17,18], {}),
              (15166076, [11,12,15,16,17,18], {}),

              (15166274, [11,12,15,16,17,18], {}),
              (15166275, [11,12,15,16,17,18], {}),
              (15166276, [11,12,15,16,17,18], {}),
              (15166474, [11,12,15,16,17,18], {}),
              (15166475, [11,12,15,16,17,18], {}),
              (15166476, [11,12,15,16,17,18], {}),

              (15166077, [11,12,15,16,17,18], {}),
              (15166078, [11,12,15,16,17,18], {}),
              (15166079, [11,12,15,16,17,18], {}),

              (15166277, [11,12,15,16,17,18], {}),
              (15166278, [11,12,15,16,17,18], {}),
              (15166279, [11,12,15,16,17,18], {}),
              (15166477, [11,12,15,16,17,18], {}),
              (15166478, [11,12,15,16,17,18], {}),
              (15166479, [11,12,15,16,17,18], {})
              ]

D2HHHH_event_types = []
if args.mc_signal: D2HHHH_event_types.extend(D2HHHH_signal)
if args.mc_HHHH:  D2HHHH_event_types.extend(D2HHHH_bkg)

D2HHHH_year_settings = [(11, 3500, "Nu2", "09j-ReDecay01", "0x40760037", "14c", "", "21r1p2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (12, 4000, "Nu2.5", "09j-ReDecay01", "0x409f0045", "14c", "", "21r0p2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (15, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (16, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (17, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (18, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34Filtered", "B02D0KPI.STRIP", "MDST"),
                        ]

D2KSHH_signal = [(11166111, [11,12,15,16,17,18], {}), # SIGNAL
                 (11166112, [11,12,15,16,17,18], {})] # SIGNAL

D2KSHH_bkg = [(11166330, [11,12,15,16,17,18], {}),
              (11166331, [11,12,15,16,17,18], {}),
              (11166530, [11,12,15,16,17,18], {}),
              (11166531, [11,12,15,16,17,18], {}),

              (13166330, [11,12,15,16,17,18], {}),
              (13166331, [11,12,15,16,17,18], {}),
              (13166530, [11,12,15,16,17,18], {}),
              (13166531, [11,12,15,16,17,18], {}),

              (11166117, [11,12,15,16,17,18], {}),
              (11166118, [11,12,15,16,17,18], {}),

              (11166332, [11,12,15,16,17,18], {}),
              (11166333, [11,12,15,16,17,18], {}),
              (11166532, [11,12,15,16,17,18], {}),
              (11166533, [11,12,15,16,17,18], {}),

              (11166119, [11,12,15,16,17,18], {}),
              (11166120, [11,12,15,16,17,18], {}),

              (11166334, [11,12,15,16,17,18], {}),
              (11166335, [11,12,15,16,17,18], {}),
              (11166534, [11,12,15,16,17,18], {}),
              (11166535, [11,12,15,16,17,18], {}),

              (13166119, [11,12,15,16,17,18], {}),
              (13166120, [11,12,15,16,17,18], {}),

              (13166334, [11,12,15,16,17,18], {}),
              (13166335, [11,12,15,16,17,18], {}),
              (13166534, [11,12,15,16,17,18], {}),
              (13166535, [11,12,15,16,17,18], {}),

              (15166117, [11,12,15,16,17,18], {}),
              (15166118, [11,12,15,16,17,18], {}),

              (15166317, [11,12,15,16,17,18], {}),
              (15166318, [11,12,15,16,17,18], {}),
              (15166517, [11,12,15,16,17,18], {}),
              (15166518, [11,12,15,16,17,18], {}),

              (15166119, [11,12,15,16,17,18], {}),
              (15166120, [11,12,15,16,17,18], {}),

              (15166319, [11,12,15,16,17,18], {}),
              (15166320, [11,12,15,16,17,18], {}),
              (15166519, [11,12,15,16,17,18], {}),
              (15166520, [11,12,15,16,17,18], {})
              ]

D2KSHH_event_types = []
if args.mc_signal: D2KSHH_event_types.extend(D2KSHH_signal)
if args.mc_KSHH:  D2KSHH_event_types.extend(D2KSHH_bkg)

D2KSHH_year_settings = [(11, 3500, "Nu2", "09j-ReDecay01", "0x40760037", "14c", "", "21r1p2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (12, 4000, "Nu2.5", "09j-ReDecay01", "0x409f0045", "14c", "", "21r0p2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (15, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (16, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                        (17, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2p1Filtered", "B02D0KPI.STRIP", "MDST"),
                        (18, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34r0p1Filtered", "B02D0KPI.STRIP", "MDST"),
                        ]

# additional bkgs added for version 10
add_bkg_signal = [] # this should obviously be empty

add_bkg_bkg = [(12265021, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265027, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265028, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267100, [11,12,15,16,17,18], {"11": {"sim": "09h",},
                                               "12": {"sim": "09h",},
                                               "15": {"sim": "09h",},
                                               "16": {"sim": "09h", "trig": "0x6139160F"},
                                               "17": {"sim": "09h",},
                                               "18": {"sim": "09h",}}),
              (12265042, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265043, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265044, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267150, [11,12,15,16,17,18], {"11": {"sim": "09h",},
                                               "12": {"sim": "09h",},
                                               "15": {"sim": "09h",},
                                               "16": {"sim": "09h", "trig": "0x6139160F"},
                                               "17": {"sim": "09h",},
                                               "18": {"sim": "09g",}}),
              (12265203, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         }),
              (12265403, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},}),
              (12267310, [12,18], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # maybe needs to be restripped?
                                   "18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267510, [12,18], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # maybe needs to be restripped?
                                   "18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (11266013, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},}),
              (11268110, [12,18], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # maybe needs to be restripped?
                                   "18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),              
              ]

add_bkg_event_types = []
if args.add_bkg: add_bkg_event_types.extend(add_bkg_signal)
if args.add_bkg:  add_bkg_event_types.extend(add_bkg_bkg)

add_bkg_year_settings = [(11, 3500, "Nu2", "09k-ReDecay01", "0x40760037", "14c", "", "21r1p2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (12, 4000, "Nu2.5", "09k-ReDecay01", "0x409f0045", "14c", "", "21r0p2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (15, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (16, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (17, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2p1Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (18, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34r0p1Filtered", "B2DKPIPI.STRIP", "MDST"),
                        ]

# set up the functionality to write options files for each mode
import os
os.system('mkdir -p B02DKPi/MC_options')

streams = {"B02D0PIPI.STRIP": "B02D0pipi.Strip",
           "B2DHH.STRIP": "b2dhh.Strip",
           "ALLSTREAMS": "AllStreams",
           "B02D0KPI.STRIP": "B02D0KPi.Strip",
           "B2DKPIPI.STRIP": "£b2dkpipi.Strip"}

def options_maker(required_decays, event_type, stream, recreate, fname):
    file_stream=streams[stream]
    if (not os.path.isfile(fname)) or recreate:
        outf = open("B02DKPi/{}".format(fname), "w")
        lines = [
                "from B02DKPi import B02DKPi_davinci_options",
                "from Configurables import DaVinci",
                "",
                "# Data/MC specific variables",
                "requiredDecays = [\"{}\"]".format(required_decays),
                "stream=\"{}\"".format(file_stream),
                "nEvents = -1",
                #"nEvents = 500", # comment this out and uncomment above before submitting
                "event_type = \"{}\"".format(event_type),
                "",
                "# Pass all this to the main options",
                "tuple_lists = B02DKPi_davinci_options.configure_tuple(requiredDecays, stream, nEvents, event_type)"
                ]
        for l in lines:
            outf.write(l+"\n")
        outf.close()


# See [this page](https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses#2012_data)
# to determine which DaVinci versions correspond to which stripping. Could be useful to
# write a script that just holds this info for the future.
strip_davinci = {"test": "test",
                 "21r1Filtered": "v36r1p1",
                 "21Filtered": "v36r1p1",
                 "21r1Filtered": "v36r1p1",
                 "21r1p2Filtered": "v39r1p6",
                 "24r2Filtered": "v44r10p5",
                 "28r1Filtered": "v41r4p4",
                 "28r1p1NoPrescalingFlagged": "v41r4p5",
                 "28r2Filtered": "v44r10p5",
                 "29r2Filtered": "v42r7p2",
                 "34Filtered": "v44r4",
                 "21r1p2Filtered": "v39r1p6",
                 "21r0p2Filtered": "v39r1p6",
                 "29r2p1Filtered": "v42r9p2",
                 "34r0p1Filtered": "v44r10p2"
                 }

def temp_settings(event_dic, year, settings_vars, settings_keys):
    for setting in event_dic[str(year)]: # Replace any settings that need to be
        settings_vars[settings_keys.index(setting)] = event_dic[str(year)][setting]
    return settings_vars

def check_info_locations(info_yaml=None):
    all_exist = True
    loc_file = open("B02DKPi/bk_locations.txt", "r")
    loc_list = loc_file.read().split("\n")
    decay_category_dic = {
                          "HH": {"event_types": D2HH_event_types, "settings": D2HH_year_settings},
                          "HHHH": {"event_types": D2HHHH_event_types, "settings": D2HHHH_year_settings},
                          "KSHH": {"event_types": D2KSHH_event_types, "settings": D2KSHH_year_settings},
                          "add_bkg": {"event_types": add_bkg_event_types, "settings": add_bkg_year_settings},
                          }
    if args.add_bkg:
        decay_category_dic = {"add_bkg": decay_category_dic["add_bkg"]}
    for decay_category in decay_category_dic:
        if decay_category == "KSHH": priority = "1a"
        else: priority = "1b"
        if args.write_info_yaml: info_yaml.write("# {} Modes \n\n".format(decay_category))
        for year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type in decay_category_dic[decay_category]["settings"]:
            settings_keys = ["year", "energy", "nu", "sim", "trig", "reco", "turbo", "strip", "stream", "file_type"]
            settings_vars = [year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type]

            for event_type, years, event_dic in decay_category_dic[decay_category]["event_types"]:
                if (year in years) and (str(year) in args.years):
                    # If the year has a dictionary then it has some settings that need replacing
                    if str(year) in event_dic:
                        year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type = temp_settings(event_dic, year, settings_vars, settings_keys)

                    # Set which DaVinci version we need
                    #version = strip_davinci[strip]

                    # Check that the bookkeeping location exists and add it to the info.yaml if it does.
                    for polarity in ['MagDown', 'MagUp']:
                        bk_location = "/MC/20{}/Beam{}GeV-20{}-{}-{}-Pythia8/Sim{}/Trig{}/Reco{}{}/Stripping{}/{}/{}.{}".format(year,energy,year,polarity,nu,sim,trig,reco,turbo,strip,event_type,stream,file_type)
                        if (bk_location not in loc_list) and not args.write_all_jobs:
                            all_exist=False
                            if args.write_info_yaml:
                                info_yaml.write(f"#{bk_location} is not an existing bookkeeping location.\n")
                            else:
                                print(bk_location+" is not an existing bookkeeping location.")
                        elif bk_location in archived_MC: # remove when archived MC is available again
                            print(f"{bk_location} is archived!")
                        elif (bk_location in loc_list) or args.write_all_jobs:
                            # prepare an options file for the mode
                            options_name = "MC_options/MC_{}_{}_options.py".format(event_type,stream)
                            if args.write_options: options_maker(decay_category, event_type, stream, args.recreate_options, options_name)
                            if args.write_info_yaml:
                                info_yaml.write("20{}_{}_{}:\n".format(year, event_type, polarity))
                                info_yaml.write(f"  priority: {priority}\n")
                                info_yaml.write("  input:\n")
                                info_yaml.write("    bk_query: {}\n".format(bk_location))
                                info_yaml.write("  options:\n")
                                info_yaml.write("    - {}\n".format(options_name))
                                info_yaml.write("    - B02DKPi_davinci_options.py\n\n")
                            else: print(bk_location+" exists.")

                        else: # This should never occur
                            if args.write_info_yaml: info_yaml.write("#[ERROR]: it is not known if "+bk_location+" exists or not, this should never occur.\n")
                            else: print("[ERROR]: it is not known if "+bk_location+" exists or not, this should never occur.")

                    # Revert the changed settings
                    if str(year) in event_dic:
                        for l in decay_category_dic[decay_category]["settings"]:
                            if year in l:
                                year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type = decay_category_dic[decay_category]["settings"][decay_category_dic[decay_category]["settings"].index(l)]
                                settings_vars = [year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type]

    if all_exist: print("All bookkeeping locations exist and if requested have had their corresponding jobs added to the info.yaml")
    else: print("Not all bookkeeping locations exist but if requested those that do have had their corresponding jobs added to the info.yaml and non-existing ones have been noted as a comment in the info.yaml.")
    loc_file.close()

def add_to_info_yaml(yaml_piece, info_yaml):
    yaml_piece_info = yaml_piece.read().split("\n")
    for l in yaml_piece_info: info_yaml.write(l+"\n")

if args.check_locations or args.write_info_yaml:
    if args.write_info_yaml:
        info_yaml = open("B02DKPi/info.yaml", "w")
        default_yaml = open("B02DKPi/src/defaults.yaml", "r")
        add_to_info_yaml(default_yaml, info_yaml)
        if args.data:
            data_yaml = open("B02DKPi/src/data.yaml", "r")
            add_to_info_yaml(data_yaml, info_yaml)
        check_info_locations(info_yaml)
    else: check_info_locations()
