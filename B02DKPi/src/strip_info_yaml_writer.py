import argparse

parser = argparse.ArgumentParser(description="Writes out the info.yaml for the AP.", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--check_locations", default=False, action="store_const", const=True, help="Check that the bk locations used in info.yaml exist.")
parser.add_argument("--write_info_yaml", default=False, action="store_const", const=True, help="Writes the info.yaml.")
parser.add_argument("--write_all_jobs", default=False, action="store_const", const=True, help="Writes out all jobs even if the bookkeeping location does not exist.")
parser.add_argument("--write_options", default=False, action="store_const", const=True, help="Writes out options files for each job if a suitable one does not exist")
parser.add_argument("--recreate_options", default=False, action="store_const", const=True, help="Forces it to recreate any options files that already exist.")

parser.add_argument('--years', nargs="+", default = ["11", "12", "15", "16", "17", "18"], help='Select which years from 11,12,15,16,17,18 to run on.')
parser.add_argument("--Bs2DKPi", default=False, action="store_const", const=True, help="Include the samples for the Bs2DKPi analysis.")
parser.add_argument("--add_bkg", default=False, action="store_const", const=True, help="Only run on additional bkgs")

args = parser.parse_args()

if args.Bs2DKPi and args.add_bkg:
    raise RuntimeError("Bs2DKPi and add_bkg productions are not simultaneous versions!")

if (not args.check_locations) and (not args.write_info_yaml):
    print("No programme mode chosen, defaulting to checking locations.")
    print("Use `-h` to learn what modes are available")
    args.check_locations = True

if (not args.write_info_yaml) and args.write_all_jobs: args.write_all_jobs = False

# In event type tuples the first entry is the decay type,
# the second entry is the list of years for which a MC sample is available,
# the third entry is a dictionary containing any changes that need to be made to
# the settings for any years a sample is available for
# Comments next to each Decay ID in the D2HH event types list the years that we have requested,
# we don't need this for any of the other modes since we requested samples for each year for each of these modes.

# TODO errors to fix
## relations table 'Relations/Rec/Calo/Clusters' is empty. This most likely can be ignored.

strip_signal = []

strip_bkg = [(13164601, [15,16], {"15": {"sim": "09f", "strip": "24r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                  "16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}), # TODO check with Yuya about this one DONE
           (11164055, [11], {"11": {"sim": "09h-ReDecay01", "strip": "21r1p2NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}), # there is a sim09k but that is filtered not flagged DONE
           (11164056, [11], {"11": {"sim": "09h-ReDecay01", "strip": "21r1p2NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
           (11164063, [11], {"11": {"sim": "09h-ReDecay01", "strip": "21r1p2NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
           (11164611, [16], {"16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}})]


tuple_signal = [(11164072, [15,16,17,18], {"15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                           "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                           "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                           "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}})]

tuple_bkg = [(11164086, [11], {"11": {"sim": "09i-ReDecay01", "strip": "21r1Filtered","stream": "B2DHH.STRIP"}}),
             (13364401, [17], {}), # previous problem job that needs to be resubmitted
              (13164601, [11], {"11": {"sim": "09f", "strip": "21r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}), # TODO check with Yuya about this one, 11 uses Sim09f DONE
              (15164012, [11,15,16,17,18], {"11": {"sim": "09k-ReDecay01", "stream": "LB2D0PH.STRIP"}, # LB has about twice as many events as B02D0KPi
                                            "15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # B02D0PIPI has more events than LB
                                            "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # this is the only sample with correct stripping TODO has a different trigger setting? DINE this is just a typo in Dirac
                                            "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # stripped and good sim version
                                            "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # stripped and good sim version # TODO does this not also need 2012 it has a sim09k, 21filtered? DONE (we already have a 2012 sample tupled)
              (15164051, [11,15,16,17,18], {"11": {"sim": "09k-ReDecay01", "stream": "LB2D0PH.STRIP"}, # only correct stripping
                                            "15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # B02D0PIPI has more events
                                            "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # only one with correct stripping
                                            "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # B2DHH has Sim09f
                                            "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # B2DHH has Sim09f, do we not want 2012? DONE no
              (15164061, [11,15,16,17,18], {"11": {"sim": "09k-ReDecay01", "stream": "LB2D0PH.STRIP"}, # only correct stripping
                                            "15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # B02D0PIPI has more events
                                            "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # only one with correct stripping
                                            "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # B2DHH has Sim09f
                                            "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # B2DHH has Sim09f, do we not want 2012? DONE no
              (13164076, [15,16,17,18], {"15":{"sim":"09k-ReDecay01"},
                                         "16":{"sim":"09k-ReDecay01"},
                                         "17":{"sim":"09k-ReDecay01"},
                                         "18":{"sim":"09k-ReDecay01"}}),
              (15164070, [15,16,17,18], {"15": {"sim":"09k-ReDecay01", "stream":"LB2D0PH.STRIP"},
                                         "16": {"sim":"09k-ReDecay01"},
                                         "17": {"sim":"09k-ReDecay01"},
                                         "18": {"sim":"09k-ReDecay01"}}), # TODO want to update 2011, 2012 to sim09k? DONE no
              (15164080, [15,16,17,18], {"15": {"sim":"09k-ReDecay01", "stream":"LB2D0PH.STRIP"},
                                         "16": {"sim":"09k-ReDecay01"},
                                         "17": {"sim":"09k-ReDecay01"},
                                         "18": {"sim":"09k-ReDecay01"}}), # TODO want to update 2011, 2012 to sim09k? DONE no
              (15164022, [15,16,17,18], {"15": {"sim":"09k-ReDecay01", "stream":"LB2D0PH.STRIP"},
                                         "16": {"sim":"09k-ReDecay01"},
                                         "17": {"sim":"09k-ReDecay01"},
                                         "18": {"sim":"09k-ReDecay01"}})] # TODO want to update 2011, 2012 to sim09k? DONE no

Bs2DKPi_strip = [(12163001, [15,16], {"15": {"sim": "09e-ReDecay01", "strip": "24r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                      "16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
                 # not in table but could be handy Bu2DK
                 (12163011, [15,16], {"15": {"sim": "09e-ReDecay01", "strip": "24r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                      "16": {"sim": "09e-ReDecay01", "turbo": "/Turbo03", "strip": "28r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}})
                                      ]

"""Luismi had
(13164062, [11,12,15,16,17,18], {"11": {"sim": "09f-ReDecay01", "strip": "21r1NoPrescalingFlagged", "stream": "ALLSTREAMDS", "file_type": "DST"},
                                            "12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                            "15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                            "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                            "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"},
                                            "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}),
"""

Bs2DKPi_tuple = [(13164062, [11,12,15,16,17,18], {"11": {"sim": "09f-ReDecay01", "strip": "21r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # same as Luismi, but uses Sim09f # all reco and truth tuples, haven't checked what % of truth
                                            "12": {"sim": "09b", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # same as Luismi
                                            "15": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # same as Luismi
                                            "16": {"sim": "09k", "trig": "0x6139160F", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # same as Luismi
                                            "17": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}, # same as Luismi
                                            "18": {"sim": "09k", "stream": "B02D0PIPI.STRIP", "file_type": "DST"}}), # same as Luismi
              (11164073, [11,12,15,16,17,18], {}), # all reco, no truth, haven't checked what % of truth
              # 11164063 12 15 16 17 18
              # 11164086 12 15 17 18
              # 15164012 12 17 18
              # 11164072 11 12 17 18
              # 15164022 11 12 17 18
              # 13164601 12 16
              # 11164601 12 15 16 17 18
              # 11164621 11 12 15 16 17 18
              # 13164611 11 12 15 16 17 18
              # 15164601 12 15 16 17 18

              (12265002, [11,12,15,16,17,18], {}), # no truth tuples for some reason, haven't checked what % of truth
              (12163001, [11,12,17,18], {"11": {"sim": "09b", "strip": "21r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # , haven't checked what % of truth
                                               "12": {"sim": "09b", "strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                               "17": {"sim": "09h", "strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                               "18": {"sim": "09h-ReDecay01", "strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
              # not in table but could be handy Bu2DK
              (12163011, [11,12,17,18], {"11": {"sim": "09b", "strip": "21r1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}, # all reco, all truth, haven't checked what % of truth
                                               "12": {"sim": "09b", "strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                               "17": {"sim": "09h", "strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"},
                                               "18": {"sim": "09h-ReDecay01", "strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}})
                                               ]

# additional bkgs added for version 10
add_bkg_tuple = [(12265021, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265027, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265028, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                          "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267100, [11,12,15,16,17,18], {"11": {"sim": "09h",},
                                               "12": {"sim": "09h",},
                                               "15": {"sim": "09h",},
                                               "16": {"sim": "09h", "trig": "0x6139160F"},
                                               "17": {"sim": "09h",},
                                               "18": {"sim": "09h",}}),
              (12265042, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265043, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12265044, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267150, [11,12,15,16,17,18], {"11": {"sim": "09h",},
                                               "12": {"sim": "09h",},
                                               "15": {"sim": "09h",},
                                               "16": {"sim": "09h", "trig": "0x6139160F"},
                                               "17": {"sim": "09h",},
                                               "18": {"sim": "09g",}}),
              (12265203, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         }),
              (12265403, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},}),
              (12267310, [18], {"18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (12267510, [18], {"18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),
              (11266013, [12,16,17,18], {"12": {"strip": "21NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "16": {"strip": "28r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "17": {"strip": "29r2NoPrescalingFlagged", "stream": "ALLSTREAMS"},
                                         "18": {"strip": "34NoPrescalingFlagged", "stream": "ALLSTREAMS"},}),
              (11268110, [18], {"18": {"sim": "09h", "strip": "34r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS"}}),              
              ]

add_bkg_strip = [(12267310, [12], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
                 (12267510, [12], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}),
                 (11268110, [12], {"12": {"sim": "09i-ReDecay01", "strip": "21r0p1NoPrescalingFlagged", "stream": "ALLSTREAMS", "file_type": "DST"}}), 
                 ]


strip_event_types = []
tuple_event_types = []

if not args.Bs2DKPi and not args.add_bkg:
    default_settings = [(11, 3500, "Nu2", "09k-ReDecay01", "0x40760037", "14c", "", "21r1Filtered", "B02D0KPI.STRIP", "MDST"),
                      (12, 4000, "Nu2.5", "09k-ReDecay01", "0x409f0045", "14c", "", "21Filtered", "B02D0KPI.STRIP", "MDST"),
                      (15, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (16, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (17, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (18, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34Filtered", "B02D0KPI.STRIP", "MDST"),
                      ]
    strip_event_types.extend(Bs2DKPi_strip)
    tuple_event_types.extend(Bs2DKPi_tuple)

    strip_event_types.extend(strip_signal)
    strip_event_types.extend(strip_bkg)

    tuple_event_types.extend(tuple_signal)
    tuple_event_types.extend(tuple_bkg)

    default_settings = [(11, 3500, "Nu2", "09j-ReDecay01", "0x40760037", "14c", "", "21r1Filtered", "B02D0KPI.STRIP", "MDST"),
                      (12, 4000, "Nu2.5", "09j-ReDecay01", "0x409f0045", "14c", "", "21Filtered", "B02D0KPI.STRIP", "MDST"),
                      (15, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (16, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (17, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (18, 6500, "Nu1.6-25ns", "09j-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34Filtered", "B02D0KPI.STRIP", "MDST"),
                      ]
if args.Bs2DKPi:
    default_settings = [(11, 3500, "Nu2", "09k-ReDecay01", "0x40760037", "14c", "", "21r1Filtered", "B02D0KPI.STRIP", "MDST"),
                      (12, 4000, "Nu2.5", "09k-ReDecay01", "0x409f0045", "14c", "", "21Filtered", "B02D0KPI.STRIP", "MDST"),
                      (15, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (16, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (17, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2Filtered", "B02D0KPI.STRIP", "MDST"),
                      (18, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34Filtered", "B02D0KPI.STRIP", "MDST"),
                      ]
    strip_event_types.extend(Bs2DKPi_strip)
    tuple_event_types.extend(Bs2DKPi_tuple)

if args.add_bkg:
    default_settings = [(11, 3500, "Nu2", "09k-ReDecay01", "0x40760037", "14c", "", "21r1p2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (12, 4000, "Nu2.5", "09k-ReDecay01", "0x409f0045", "14c", "", "21r0p2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (15, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x411400a2", "15a", "/Turbo02", "24r2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (16, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x6138160F", "16", "/Turbo03a", "28r2Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (17, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x62661709", "17", "/Turbo04a-WithTurcal", "29r2p1Filtered", "B2DKPIPI.STRIP", "MDST"),
                        (18, 6500, "Nu1.6-25ns", "09k-ReDecay01", "0x617d18a4", "18", "/Turbo05-WithTurcal", "34r0p1Filtered", "B2DKPIPI.STRIP", "MDST"),
                        ]
    strip_event_types.extend(add_bkg_strip)
    tuple_event_types.extend(add_bkg_tuple)

# set up the functionality to write options files for each mode
import os
os.system('mkdir -p B02DKPi/strip_options')
os.system('mkdir -p B02DKPi/MC_options')

streams = {"B02D0PIPI.STRIP": "B02D0pipi.Strip",
           "B2DHH.STRIP": "b2dhh.Strip",
           "ALLSTREAMS": "AllStreams",
           "B02D0KPI.STRIP": "B02D0KPi.Strip",
           "LB2D0PH.STRIP": "Lb2D0ph.Strip",
           "BU2DPI.STRIP": "bu2dpi.Strip",
           "B2DKPIPI.STRIP": "b2dkpipi.Strip"}

KsHH_event_types = [12267100, 12267150, 12267310, 12267510, 11268110]
HHHH_event_types = []

def strip_options_maker(event_type, stream, recreate, fname):
    file_stream=streams[stream]
    if event_type in KsHH_event_types:
        required_decays = "KSHH"
    elif event_type in HHHH_event_types:
        required_decays = "HHHH"
    else:
        required_decays = "HH"
    if (not os.path.isfile(fname)) or recreate:
        outf = open("B02DKPi/{}".format(fname), "w")
        lines = [
                "from B02DKPi import stripper",
                "",
                "# Data/MC specific variables",
                "requiredDecays = [\"{}\"]".format(required_decays),
                f"stream=\"{stream}\"",
                "nEvents = -1",
                #"nEvents = 3000", # comment this out and uncomment above before submitting
                "",
                "# Pass all this to the main options",
                "stripper.configure_stripping(requiredDecays,stream, nEvents)"
                ]
        for l in lines:
            outf.write(l+"\n")
        outf.close()

def tuple_options_maker(event_type, stream, recreate, fname):
    file_stream=streams[stream]
    if event_type in KsHH_event_types:
        required_decays = "KSHH"
    elif event_type in HHHH_event_types:
        required_decays = "HHHH"
    else:
        required_decays = "HH"
    if (not os.path.isfile(fname)) or recreate:
        outf = open("B02DKPi/{}".format(fname), "w")
        lines = [
                "from B02DKPi import B02DKPi_davinci_options",
                "",
                "# Data/MC specific variables",
                "requiredDecays = [\"{}\"]".format(required_decays),
                "stream=\"{}\"".format(file_stream),
                "nEvents = -1",
                #"nEvents = 500", # comment this out and uncomment above before submitting
                "event_type = \"{}\"".format(event_type),
                "",
                "# Pass all this to the main options",
                "B02DKPi_davinci_options.configure_tuple(requiredDecays, stream, nEvents, event_type)"
                ]
        for l in lines:
            outf.write(l+"\n")
        outf.close()


# See [this page](https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses#2012_data)
# to determine which DaVinci versions correspond to which stripping. Could be useful to
# write a script that just holds this info for the future.
strip_davinci = {"test": "test",
                 "21r1Filtered": "v36r1p1",
                 "21Filtered": "v36r1p1",
                 "21r1p2Filtered": "v39r1p6",
                 "24r2Filtered": "v44r10p5",
                 "28r1Filtered": "v41r4p4",
                 "28r1p1NoPrescalingFlagged": "v41r4p5",
                 "28r2Filtered": "v44r10p5",
                 "29r2Filtered": "v42r7p2",
                 "34Filtered": "v44r4",
                 "21r0p2Filtered": "v39r1p6",
                 "29r2p1Filtered": "v42r9p2",
                 "34r0p1Filtered": "v44r10p2"
                 }

year_to_strip = {11: "21r1Filtered",
                 12: "21Filtered",
                 15: "24r2Filtered",
                 16: "28r2Filtered",
                 17: "29r2Filtered",
                 18: "34Filtered"}

def temp_settings(event_dic, year, settings_vars, settings_keys):
    for setting in event_dic[str(year)]: # Replace any settings that need to be
        settings_vars[settings_keys.index(setting)] = event_dic[str(year)][setting]
    return settings_vars

def check_info_locations(info_yaml=None):
    all_exist = True
    loc_file = open("B02DKPi/bk_locations.txt", "r")
    loc_list = loc_file.read().split("\n")
    settings_dict = {"strip": {"event_types": strip_event_types},
                     "tuple": {"event_types": tuple_event_types}
    }

    for year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type in default_settings:
        settings_keys = ["year", "energy", "nu", "sim", "trig", "reco", "turbo", "strip", "stream", "file_type"]
        settings_vars = [year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type]

        for operation in settings_dict:
            for event_type, years, event_dic in settings_dict[operation]["event_types"]:
                if (year in years) and (str(year) in args.years):
                        # If the year has a dictionary then it has some settings that need replacing
                        if str(year) in event_dic:
                            year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type = temp_settings(event_dic, year, settings_vars, settings_keys)

                        # Set which DaVinci version we need
                        version = strip_davinci[year_to_strip[year]]

                        # Check that the bookkeeping location exists and add it to the info.yaml if it does.
                        polarities = ['MagDown', 'MagUp']
                        if event_type=="13364401": polarities = ["MagDown"] # to resubmit a previously broken job
                        for polarity in polarities:
                            bk_location = "/MC/20{}/Beam{}GeV-20{}-{}-{}-Pythia8/Sim{}/Trig{}/Reco{}{}/Stripping{}/{}/{}.{}".format(year,energy,year,polarity,nu,sim,trig,reco,turbo,strip,event_type,stream,file_type)
                            if (bk_location not in loc_list) and not args.write_all_jobs:
                                all_exist=False
                                if args.write_info_yaml:
                                    info_yaml.write(f"#{bk_location} is not an existing bookkeeping location.\n")
                                else:
                                    print(bk_location+" is not an existing bookkeeping location.")
                            elif (bk_location in loc_list) or args.write_all_jobs:
                                if operation == "strip":
                                    # prepare an options file for the mode
                                    strip_options_name = "strip_options/strip_{}_{}_options.py".format(event_type, stream)
                                    tuple_options_name = "MC_options/MC_{}_B02D0KPI.STRIP_options.py".format(event_type)
                                    if args.write_options: 
                                        strip_options_maker(event_type, stream, args.recreate_options, strip_options_name)
                                        tuple_options_maker(event_type, "B02D0KPI.STRIP", args.recreate_options, tuple_options_name)
                                    if args.write_info_yaml:
                                        info_yaml.write("20{}_{}_{}_Strip:\n".format(year, event_type, polarity))
                                        info_yaml.write("  application: DaVinci/{}\n".format(version))
                                        info_yaml.write("  input:\n")
                                        info_yaml.write("    bk_query: {}\n".format(bk_location))
                                        info_yaml.write("  options:\n")
                                        info_yaml.write("    - {}\n".format(strip_options_name))
                                        info_yaml.write("  output: B02D0KPI.STRIP.{}\n\n".format(file_type))
                                        info_yaml.write("20{}_{}_{}:\n".format(year, event_type, polarity))
                                        info_yaml.write("  input:\n")
                                        info_yaml.write("    job_name: 20{}_{}_{}_Strip\n".format(year, event_type, polarity))
                                        info_yaml.write("  options:\n")
                                        info_yaml.write("    - {}\n\n".format(tuple_options_name))
                                    else: print(bk_location+" exists.")
                                elif operation == "tuple":
                                    # prepare an options file for the mode
                                    tuple_options_name = "MC_options/MC_{}_{}_options.py".format(event_type, stream)
                                    if args.write_options:
                                        tuple_options_maker(event_type, stream, args.recreate_options, tuple_options_name)
                                    if args.write_info_yaml:
                                        info_yaml.write("20{}_{}_{}:\n".format(year, event_type, polarity))
                                        info_yaml.write("  input:\n")
                                        info_yaml.write("    bk_query: {}\n".format(bk_location))
                                        info_yaml.write("  options:\n")
                                        info_yaml.write("    - {}\n".format(tuple_options_name))
                                        info_yaml.write("  extra_checks:\n")
                                        info_yaml.write("    - Bd_M_range\n")
                                        info_yaml.write("    - D0_M_range\n\n")
                                    else: print(bk_location+" exists.")
                                else:
                                    raise ValueError("Unexpected data transformation operation!")

                            else: # This should never occur
                                if args.write_info_yaml: info_yaml.write("#[ERROR]: it is not known if "+bk_location+" exists or not, this should never occur.\n")
                                else: print("[ERROR]: it is not known if "+bk_location+" exists or not, this should never occur.")

                        # Revert the changed settings
                        if str(year) in event_dic:
                            for l in default_settings:
                                if year in l:
                                    year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type = default_settings[default_settings.index(l)]
                                    settings_vars = [year, energy, nu, sim, trig, reco, turbo, strip, stream, file_type]

    if all_exist: print("All bookkeeping locations exist and if requested have had their corresponding jobs added to the info.yaml")
    else: print("Not all bookkeeping locations exist but if requested those that do have had their corresponding jobs added to the info.yaml and non-existing ones have been noted as a comment in the info.yaml.")
    loc_file.close()

def add_to_info_yaml(yaml_piece, info_yaml):
    yaml_piece_info = yaml_piece.read().split("\n")
    for l in yaml_piece_info: info_yaml.write(l+"\n")

if args.check_locations or args.write_info_yaml:
    if args.write_info_yaml:
        info_yaml = open("B02DKPi/info.yaml", "w")
        default_yaml = open("B02DKPi/src/defaults.yaml", "r")
        add_to_info_yaml(default_yaml, info_yaml)
        check_info_locations(info_yaml)
    else:
        check_info_locations()
