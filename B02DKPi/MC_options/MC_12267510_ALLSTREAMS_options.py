from B02DKPi import B02DKPi_davinci_options

# Data/MC specific variables
requiredDecays = ["KSHH"]
stream="AllStreams"
nEvents = -1
event_type = "12267510"

# Pass all this to the main options
B02DKPi_davinci_options.configure_tuple(requiredDecays, stream, nEvents, event_type)
