from B02DKPi import stripper

# Data/MC specific variables
requiredDecays = ["KSHH"]
stream="ALLSTREAMS"
nEvents = -1

# Pass all this to the main options
stripper.configure_stripping(requiredDecays,stream, nEvents)
