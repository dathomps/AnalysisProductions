from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (
    SelDSTWriter,
    stripDSTStreamConf,
    stripDSTElements
)

STRIPPING_VERSIONS = {
    '2011': 'Stripping21r1',
    '2012': 'Stripping21',
    '2015': 'Stripping24r2',
    '2016': 'Stripping28r2',
    '2017': 'Stripping29r2',
    '2018': 'Stripping34'
}

# Handy function to return the key associated with an element of a dictionary, assuming there is only one key for each unique element so be careful!
def GetDictKey(dictionary, element):
    for key, value in dictionary.items():
        if element in value:
            return key

def configure_stripping(requiredDecays, stream, nEvents=-1):

    DaVinci().EvtMax = nEvents

    # some initial bookkeeping
    # A useful dictionary to keep track of the different decays that may be used
    decayCategories = {}
    decayCategories["HH"] = ["KK", "KPi", "PiK", "PiPi"]
    decayCategories["HHHH"] = ["KPiPiPi", "PiKPiPi","PiPiPiPi"]
    decayCategories["KSHH"] = ["KsKK", "KsPiPi"]
    decayCategories["Pi0HH"] = ["Pi0KK", "Pi0KPi", "Pi0PiK", "Pi0PiPi"]

    tracks = ['LL', 'DD'] # This is only used for stripping lines with KSHH in their name i.e. decays involving a Ks0 since LL/DD refer to where the Ks0 decays in the detector
    signs = ["","_WS"] # Only used for decays for which a Wrong Sign stripping line is available
    bDecs = ['D0KPi']
    dDecs = []
    if "HH" in requiredDecays: dDecs.extend(decayCategories["HH"])
    if "HHHH" in requiredDecays: dDecs.extend(decayCategories["HHHH"])
    if "KSHH" in requiredDecays: dDecs.extend(decayCategories["KSHH"])
    if "Pi0HH" in requiredDecays: dDecs.extend(decayCategories["Pi0HH"])

    lines = []
    for b in bDecs:
        for d in dDecs:
            if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                lines.append("StrippingB02{}D2{}Beauty2CharmLine".format(b, GetDictKey(decayCategories, d)))
                if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                    lines.append("StrippingB02{}D2{}WSBeauty2CharmLine".format(b, GetDictKey(decayCategories, d)))
            if d in decayCategories["Pi0HH"]:
                lines.append("StrippingB02{}D2{}Beauty2CharmLine".format(b, Pi0HHLine))
            if d in decayCategories["KSHH"]:
                for t in tracks:
                    lines.append("StrippingB02{}D2KSHH{}Beauty2CharmLine".format(b, t))
                    lines.append("StrippingB02{}D2KSHH{}WSBeauty2CharmLine".format(b, t))

    from Configurables import EventNodeKiller
    event_node_killer = EventNodeKiller('StripKiller')
    event_node_killer.Nodes = ['/Event/{}'.format(stream), '/Event/Strip']

    stripping_version = STRIPPING_VERSIONS[DaVinci().DataType]

    streams = buildStreams(stripping=strippingConfiguration(stripping_version), archive=strippingArchive(stripping_version))

    custom_stream = StrippingStream('B02D0KPi.Strip')

    for stream in streams:
        for sline in stream.lines:
            if sline.name() in lines:
                custom_stream.appendLines([sline])

    # Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents)

    enablePacking = False

    SelDSTWriterElements = {'default': stripDSTElements(pack=enablePacking)}

    SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking,
                                                      selectiveRawEvent=True,
                                                      fileExtension='.{}'.format(DaVinci().InputType))}

    dstWriter = SelDSTWriter('MyDSTWriter',
                             StreamConf=SelDSTWriterConf,
                             MicroDSTElements=SelDSTWriterElements,
                             OutputFileSuffix='',
                             SelectionSequences=sc.activeStreams()
                             )

    DaVinci().UserAlgorithms =[event_node_killer, sc.sequence(), dstWriter.sequence()]
