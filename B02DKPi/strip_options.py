from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive
from Configurables import (
    EventNodeKiller,
    ProcStatusCheck,
    DaVinci,
    DecayTreeTuple
)
from GaudiConf import IOHelper
from DecayTreeTuple.Configuration import *
from B02DKPi import stripping

# Handy function to return the key associated with an element of a dictionary, assuming there is only one key for each unique element so be careful!
def GetDictKey(dictionary, element):
    for key, value in dictionary.items():
        if element in value:
            return key

def configure_stripping(requiredDecays, stream, nEvents=-1):

    DaVinci().EvtMax = nEvents

    # some initial bookkeeping
    # A useful dictionary to keep track of the different decays that may be used
    decayCategories = {}
    decayCategories["HH"] = ["KK", "KPi", "PiK", "PiPi"]
    decayCategories["HHHH"] = ["KPiPiPi", "PiKPiPi","PiPiPiPi"]
    decayCategories["KSHH"] = ["KsKK", "KsPiPi"]
    decayCategories["Pi0HH"] = ["Pi0KK", "Pi0KPi", "Pi0PiK", "Pi0PiPi"]

    tracks = ['LL', 'DD'] # This is only used for stripping lines with KSHH in their name i.e. decays involving a Ks0 since LL/DD refer to where the Ks0 decays in the detector
    signs = ["","_WS"] # Only used for decays for which a Wrong Sign stripping line is available
    bDecs = ['D0KPi']
    dDecs = []
    if "HH" in requiredDecays: dDecs.extend(decayCategories["HH"])
    if "HHHH" in requiredDecays: dDecs.extend(decayCategories["HHHH"])
    if "KSHH" in requiredDecays: dDecs.extend(decayCategories["KSHH"])
    if "Pi0HH" in requiredDecays: dDecs.extend(decayCategories["Pi0HH"])


    #event_node_killer = EventNodeKiller('StripKiller')
    #event_node_killer.Nodes = ['/Event/{}'.format(stream), '/Event/Strip']

    #strip = 'stripping21r1'
    #streams = buildStreams(stripping=strippingConfiguration(strip),
    #                    archive=strippingArchive(strip))

    lines = []
    for b in bDecs:
        for d in dDecs:
            if (d in decayCategories["HH"]) or (d in decayCategories["HHHH"]):
                lines.append("StrippingB02{}D2{}Beauty2CharmLine".format(b, GetDictKey(decayCategories, d)))
                if (d not in ["PiK"]) and (d not in ["PiKPiPi"]):
                    lines.append("StrippingB02{}D2{}WSBeauty2CharmLine".format(b, GetDictKey(decayCategories, d)))
            if d in decayCategories["Pi0HH"]:
                lines.append("StrippingB02{}D2{}Beauty2CharmLine".format(b, Pi0HHLine))
            if d in decayCategories["KSHH"]:
                for t in tracks:
                    lines.append("StrippingB02{}D2KSHH{}Beauty2CharmLine".format(b, t))
                    lines.append("StrippingB02{}D2KSHH{}WSBeauty2CharmLine".format(b, t))
    """
    custom_stream = StrippingStream('B02D0KPi.Strip')

    for stream in streams:
        for sline in stream.lines:
            if sline.name() in lines:
                custom_stream.appendLines([sline])

    # Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()

    sc = StrippingConf(Streams=[custom_stream],
                    MaxCandidates=2000,
                    AcceptBadEvents=False,
                    BadEventSelection=filterBadEvents)

    DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
    """
    data_type = DaVinci().DataType

    seq = stripping.stripping(data_type, lines, stream)
    DaVinci().UserAlgorithms = [seq]