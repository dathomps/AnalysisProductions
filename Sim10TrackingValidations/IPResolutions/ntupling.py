'''Functions to configure IP ntupling.'''

from Configurables import GaudiSequencer, PVOfflineTool, VertexCompare, SplitTracks, PatPV3D, LSAdaptPV3DFitter, \
    TrackSelector, TrackContainerCopy, PrimaryVertexChecker, PVResolution
from math import pi

def pv_tracks(DataType, InputTracks = ''):
    '''Get the default input tracks location for PV fitting.'''
    if InputTracks:
        return InputTracks
    if int(DataType) < 2015:
        return 'Rec/Track/Best'
    return 'Rec/Track/FittedHLT1VeloTracks'

def configure_pv_tool(ipMoniAlgo, DataType, InputTracks = '') :
    '''Configure the PV fitter for Run2, which is non-default. This is copied from RecSysConf. 
    Ideally, they'd have put the PV fitter config into a function there, but this works anyway.'''

    InputTracks = pv_tracks(DataType, InputTracks)

    ipMoniAlgo.addTool(PVOfflineTool, 'PVOfflineTool')
    pvTool = ipMoniAlgo.PVOfflineTool
    pvTool.InputTracks = [ InputTracks ]

    if int(DataType) < 2015:
        return

    # Run2 Config copied from RecSys.
    pvTool.addTool(LSAdaptPV3DFitter, "LSAdaptPV3DFitter")
    pvTool.PVFitterName = "LSAdaptPV3DFitter"
    pvTool.LSAdaptPV3DFitter.UseFittedTracks = True
    pvTool.LSAdaptPV3DFitter.AddMultipleScattering = False
    pvTool.LSAdaptPV3DFitter.TrackErrorScaleFactor = 1.0
    pvTool.LSAdaptPV3DFitter.MinTracks = 4
    pvTool.LSAdaptPV3DFitter.trackMaxChi2 = 12.0
    pvTool.UseBeamSpotRCut = True
    pvTool.BeamSpotRCut = 0.2

def vertex_compare_seq(name, tracks1, tracks2, DataType):
    '''Make the sequence for VertexCompare with the given split track locations.'''
    
    pvAlg1 = PatPV3D("PatPV3D_" + name + "1")
    pvAlg2 = PatPV3D("PatPV3D_" + name + "2")
    configure_pv_tool(pvAlg1, DataType, tracks1)
    configure_pv_tool(pvAlg2, DataType, tracks2)

    pvAlg1.OutputVerticesName = "Rec/Vertex/Primary" + name + "1"
    pvAlg2.OutputVerticesName = "Rec/Vertex/Primary" + name + "2"

    vertexCompare = VertexCompare("VertexCompare" + name)
    vertexCompare.inputVerticesName1 = pvAlg1.OutputVerticesName
    vertexCompare.inputVerticesName2 = pvAlg2.OutputVerticesName
    vertexCompare.produceNtuple = True
    vertexCompare.produceHistogram = True

    seq = GaudiSequencer('VertexCompare' + name + 'Seq', IgnoreFilterPassed = True,
                         ModeOR = True, ShortCircuit = False,
                         Members = [pvAlg1, pvAlg2, vertexCompare])
    return seq
    

def vertex_compare_random_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with random track splitting.'''
    
    split  = SplitTracks("SplitRandom")
    split.InputTracks = InputTracks
    split.OutputTracks1 = "Rec/Track/FittedSampleA"
    split.OutputTracks2 = "Rec/Track/FittedSampleB"
    # Force the split tracks containers to be the same size.
    split.EqualizeSplitContainerSizes = True

    seq = vertex_compare_seq('', split.OutputTracks1, split.OutputTracks2, DataType)
    seq.Members.insert(0, split)

    return seq

def track_selector_phi_range(name, InputTracks, phimin, phimax):
    '''Make a TrackContainerCopy instance selecting tracks in the given phi range.'''
    
    copy = TrackContainerCopy(name, inputLocations = [InputTracks], outputLocation = InputTracks + name)
    selector = TrackSelector(name + 'Selector', MinPhiCut = phimin, MaxPhiCut = phimax)
    copy.Selector = selector
    return copy
    

def vertex_compare_LR_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with left-right track splitting.'''
    
    copyR = track_selector_phi_range('CopyR', InputTracks, -pi/2, pi/2)
    copyL1 = track_selector_phi_range('CopyL1', InputTracks, -pi, -pi/2)
    copyL2 = track_selector_phi_range('CopyL2', InputTracks, pi/2, pi)
    copyL = TrackContainerCopy('CopyL', inputLocations = [copyL1.outputLocation, copyL2.outputLocation],
                               outputLocation = InputTracks + 'CopyL')
    seq = vertex_compare_seq('LR', copyR.outputLocation, copyL.outputLocation, DataType)
    seq.Members = [copyR, copyL1, copyL2, copyL] + seq.Members
    return seq

def vertex_compare_UD_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with up-down track splitting.'''
    
    copyU = track_selector_phi_range('CopyU', InputTracks, 0., pi)
    copyD = track_selector_phi_range('CopyD', InputTracks, -pi, 0.)
    seq = vertex_compare_seq('UD', copyU.outputLocation, copyD.outputLocation, DataType)
    seq.Members = [copyU, copyD] + seq.Members
    return seq

def vertex_compare_FB_seq(DataType, InputTracks):
    '''Make the sequence for VertexCompare with forward-backward track splitting.'''
    
    copyF = TrackContainerCopy('CopyF', inputLocations = ['InputTracks'], outputLocation = InputTracks + 'CopyF',
                               Selector = TrackSelector('CopyFSelector', TrackTypes = ['Long', 'Velo', 'Upstream']))
    copyB = TrackContainerCopy('CopyB', inputLocations = ['InputTracks'], outputLocation = InputTracks + 'CopyB',
                               Selector = TrackSelector('CopyBSelector', TrackTypes = ['Backward']))
    seq = vertex_compare_seq('FB', copyF.outputLocation, copyB.outputLocation, DataType)
    seq.Members = [copyF, copyB] + seq.Members
    return seq

def make_confs(ipres = True, pvres = True, pvtracks = ''):
    '''Make the configurables for IP and PV resolution'''
    confs = []
    # PV res goes first as IP res might filter Rec/Track/Best (only relevant for Run 1).
    if pvres:
        from Sim10TrackingValidations.IPResolutions.Configuration import PVResolutionsConf
        pvconf = PVResolutionsConf('PVResolutionConf')
        pvconf.InputTracks = pvtracks
        confs.append(pvconf)
    if ipres:
        from Sim10TrackingValidations.IPResolutions.Configuration import IPResolutionsConf
        ipconf = IPResolutionsConf('VeloIPResolutionMonitorConf')
        ipconf.PVTracksLocation = pvtracks
        confs.append(ipconf)
    return confs

def configure_dv(ipres = True, pvres = True, pvtracks = ''):
    '''Configure DaVinci for ntupling.'''
    from Configurables import DaVinci
    from Gaudi.Configuration import appendPostConfigAction
    
    dv = DaVinci()
    dv.TupleFile = 'IPTuple.root'

    confs = make_confs(ipres, pvres, pvtracks)
    for conf in confs:
        dv.UserAlgorithms.append(conf.sequence())
    if pvres:
        appendPostConfigAction(lambda : dv_post_config(confs[0]))
    return confs

def dv_post_config(pvconf):
    '''Post config actions for DaVinci.'''
    from Configurables import DaVinci, EventNodeKiller, TrackAssociator
    from Gaudi.Configuration import allConfigurables
    
    if int(DaVinci().DataType) < 2015:
        return
    pvtracks = pv_tracks(DaVinci().DataType, pvconf.getProp('InputTracks'))
    # Need to re-reconstruct all VELO tracks in Run 2, since only tracks previously used in PVs
    # are kept
    seq = GaudiSequencer('RerunVELOSeq', IgnoreFilterPassed = True)
    reco = GaudiSequencer('RecoVELOSeq')
    init = allConfigurables['VeloOnlyInitAlg']
    init.TrackLocation = 'Rec/Track/Velo'
    reco.Members.append(init)
    seq.Members += [reco]
    if DaVinci().getProp('Simulation'):
        assoc = TrackAssociator('AssocVELO', TracksInContainer = 'Rec/Track/Velo')
        seq.Members.append(assoc)
    # Redirect PV resolutions algo to the rebuilt tracks.
    pvconf.InputTracks = 'Rec/Track/Velo'
    pvconf.__apply_configuration__()
    GaudiSequencer('DaVinciUserSequence').Members.insert(0, seq)

def kill_sequence(seqName) :
    '''Remove all members from a sequence.'''
    if isinstance(seqName, list) :
        for name in seqName :
            kill_sequence(name)
    else :
        GaudiSequencer(seqName).Members = []

def configure_brunel(ipres = True, pvres = True, pvtracks = '', veloonly = False):
    '''Configure Brunel for ntupling.'''
    from Configurables import Brunel, RecSysConf, RecMoniConf, TrackSys
    from Gaudi.Configuration import HistogramPersistencySvc, NTupleSvc, appendPostConfigAction

    # Instantiate the application.
    app = Brunel()
    # Don't save a DST
    app.OutputType = 'NONE'

    # Save a histo and ntuple file.
    HistogramPersistencySvc().OutputFile = "BrunelHisto.root"
    NTupleSvc().Output=["FILE1 DATAFILE='IPTuple.root' TYP='ROOT' OPT='NEW'"]

    RecMoniConf().Detectors = []
    # Set these to run only the VELO tracking (for Run 2 PV resolution)
    if veloonly:
        RecSysConf().Detectors = ['Velo', 'Tr']
        TrackSys().TrackPatRecAlgorithms = ["Velo","FastVelo"]

    confs = make_confs(ipres, pvres, pvtracks)
    appendPostConfigAction(lambda : brunel_post_config(confs))
    return confs

def brunel_post_config(confs):
    '''Post config actions for Brunel: kill unneeded algos and add the IP/PV res. sequences.'''

    from Configurables import Brunel
    mc = Brunel().getProp('Simulation')
    datatype = Brunel().DataType

    if int(datatype) >= 2015:
        # Don't clean tracks not included in PVs.
        pvseq = GaudiSequencer('RecoVertexSeq')
        if pvseq.Members[-1].name() == 'PVVeloTracksCleaner':
            pvseq.Members.pop()

    # Kill everything in Brunel that we don't need.
    kill_sequence(["MoniVELOSeq",
                   "MoniCALOSeq",
                   "RecoRICHSeq",
                   "RecoRICHFUTURESeq",
                   "MoniRICHSeq",
                   "MoniRICHFUTURESeq",
                   "DecodeTriggerSeq",
                   "RecoPROTOSeq",
                   "MoniPROTOSeq",
                   "LumiSeq",
                   "MoniHltSeq",
                   "HltfilterSeq",
                   "CaloBanksHandler",
                   "MoniMUONSeq",
                   "RecoCALOSeq",
                   "CaloPIDsSeq",
                   "RecoMUONSeq",
                   "MoniGENERALSeq",
                   "MoniTrSeq",
                   "MoniOTSeq",
                   "MoniSTSeq"])
    if mc:
        kill_sequence(["CheckRICHSeq",
                       "CheckRICHFUTURESeq",
                       "CheckMUONSeq",
                       "MCLinksCaloSeq"])
        # If we're running on simulated data add the IP monitoring algorthim
        # later in the sequence, after the MC links have been made.
        seq = GaudiSequencer("CheckPatSeq")
    else:
        seq = GaudiSequencer("MoniVELOSeq")

    seq.IgnoreFilterPassed = True
    for conf in confs:
        seq.Members.append(conf.sequence())
