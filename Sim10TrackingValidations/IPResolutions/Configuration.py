'''Configurable for running the IP and PV resolution monitoring.'''

from Configurables import LHCbConfigurableUser, GaudiSequencer, Velo__VeloIPResolutionMonitorNT, TrackSelector, \
    TrackContainerCopy, Gaudi__DataLink, Gaudi__DataCopy, TrackContainerCleaner
from Sim10TrackingValidations.IPResolutions.ntupling import *
# Tried just using LHCbApp and adding the Confs to LHCbApp.__used_configurables__, but it didn't work.
# This is less clean, but works at least.
try:
    from Configurables import Brunel as App
except ImportError:
    from Configurables import DaVinci as App

class IPResolutionsConf(LHCbConfigurableUser):
    '''Configurable for IP resolutions ntuples.'''

    __slotsanddoc = {'PVTracksLocation' : ('', 'Use a non-default location of tracks that were used to make PVs.'),
                     'TrackSelectorKwargs' : ({}, 'Keyword-args to be given to a TrackSelector instance to pre-filter tracks.'),
                     'LoKiFiltersKwargs' : ({}, 'Keyword-args for LoKi_Filters, to pre-filter on trigger/stripping decisions'),
                     'TracksLocation' : ('Rec/Track/Best', 'Location of tracks for IP resolutions')}

    __slots__ = {k : v[0] for k, v in __slotsanddoc.items()}
    _propertyDocDict = {k : v[1] for k, v in __slotsanddoc.items()}

    def sequence(self):
        '''Get the sequence.'''
        return GaudiSequencer(self.name().replace('Conf', '') + 'Seq')

    def __apply_configuration__(self):
        '''Do the configuration.'''
        
        mc = App().getProp('Simulation')
        datatype = App().getProp('DataType')
        pvtracks = pv_tracks(datatype, self.getProp('PVTracksLocation'))
        trackslocation = self.getProp('TracksLocation')
        name = self.name().replace('Conf', '')

        self.sequence().Members = []
        # Stripping/HLT filters.
        if self.getProp('LoKiFiltersKwargs'):
            # This only works in DaVinci as LoKi_Filters lives in Phys.
            try:
                from PhysConf.Filters import LoKi_Filters
            except ImportError:
                raise Exception('IPResolutionsConf.LoKiFiltersKwargs only works in DaVinci!')
            filters = LoKi_Filters(**self.LoKiFiltersKwargs)
            self.sequence().Members.append(filters.sequence(name + ':IPMoniFilters'))

        if self.getProp('TrackSelectorKwargs'):
            # Can't currently work out how to copy tracks and retain MCLinks, so just filter
            # them in place just now.
            # copytracks = Gaudi__DataCopy(name + ':CopyTracks', What = trackslocation,
            #                              Target = 'Rec/Track/' + name + 'FilteredTracks')
            selector = TrackSelector(name + ':TrackSelector', **self.TrackSelectorKwargs)
            cleantracks = TrackContainerCleaner(name + ':TrackCleaner', 
                                                #inputLocation = copytracks.Target,
                                                inputLocation = trackslocation,
                                                selectorName = 'TrackSelector/' + name + ':TrackSelector')
            cleantracks.addTool(selector)
            #self.sequence().Members.append(copytracks)
            self.sequence().Members.append(cleantracks)
            # if mc:
            #     # Link the MCLinks of the tracks to the new location.
            #     link = Gaudi__DataLink(name + ':LinkMCLinks', What = 'Link/' + trackslocation, 
            #                            Target = 'Link/' + copytracks.Target)
            #     self.sequence().Members.append(link)
            # trackslocation = copytracks.Target

        ipmoni = Velo__VeloIPResolutionMonitorNT(name + 'NT')
        configure_pv_tool(ipmoni, datatype, pvtracks)
        ipmoni.CheckIDs = True
        ipmoni.WithMC = mc
        ipmoni.TrackLocation = trackslocation
        self.sequence().Members.append(ipmoni)

class PVResolutionsConf(LHCbConfigurableUser):
    
    __slotsanddoc = {'InputTracks' : ('', 'Use a non-default location for the tracks used in PVs'),
                     'FillRandom' : (True, 'Whether to fill random splits'),
                     'FillLR' : (True, 'Whether to fill left-right splits'),
                     'FillUD' : (True, 'Whether to fill up-down splits'),
                     'FillFB' : (True, 'Whether to fill forward-backward splits'),
                     'UsePrimaryVertexCheckerMCAlg' : (False, 'Whether to use PrimaryVertexChecker algo on MC (requires xdigi or xdst input)'),
                     'UsePVResolutionMCAlg' : (True, 'Whether to use PVResolution algo on MC')}

    __slots__ = {k : v[0] for k, v in __slotsanddoc.items()}
    _propertyDocDict = {k : v[1] for k, v in __slotsanddoc.items()}

    def sequence(self):
        '''Get the sequence.'''
        return GaudiSequencer(self.name().replace('Conf', '') + 'Seq', IgnoreFilterPassed = True)

    def __apply_configuration__(self):
        '''Apply the configuration.'''

        DataType = App().getProp('DataType')
        mc = App().getProp('Simulation')
        InputTracks = pv_tracks(DataType, self.getProp('InputTracks'))
        name = self.name().replace('Conf', '')

        self.sequence().Members = []
        if self.getProp('FillRandom'):
            self.sequence().Members.append(vertex_compare_random_seq(DataType, InputTracks))
        if self.getProp('FillLR'):
            self.sequence().Members.append(vertex_compare_LR_seq(DataType, InputTracks))
        if self.getProp('FillUD'):
            self.sequence().Members.append(vertex_compare_UD_seq(DataType, InputTracks))
        if self.getProp('FillFB'):
            self.sequence().Members.append(vertex_compare_FB_seq(DataType, InputTracks))
        if mc:
            if self.getProp('UsePrimaryVertexCheckerMCAlg'):
                checker = PrimaryVertexChecker('PVCheckerMC')
                checker.produceNtuple  = True
                checker.produceHistogram = True
                checker.inputVerticesName = "Rec/Vertex/Primary"
                checker.matchByTracks = False
                checker.nTracksToBeRecble = 4
                checker.inputTracksName = InputTracks
                self.sequence().Members.append(checker)
            if self.getProp('UsePVResolutionMCAlg'):
                checker = PVResolution('PVResolutionMC')
                checker.WithMC = True
                checker.TrackLocation = InputTracks
                checker.FillLR = False
                checker.FillRand = False
                configure_pv_tool(checker, DataType)
                self.sequence().Members.append(checker)
