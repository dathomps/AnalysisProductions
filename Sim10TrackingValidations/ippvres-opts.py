from Sim10TrackingValidations.IPResolutions.ntupling import configure_dv

pvconf, ipconf = configure_dv()

pvconf.FillUD = False
pvconf.FillLR = False
pvconf.FillFB = False

ipconf.TrackSelectorKwargs = dict(TrackTypes = ["Long"], MinPtCut = 300)
