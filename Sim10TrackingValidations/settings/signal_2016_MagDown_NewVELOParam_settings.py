'''
Production Info: 
    Configuration Name: MC
    Configuration Version: Dev
    Event type: 12143001
-----------------------
 StepName: Sim10Dev07 - 2016 - MD - Nu1.6 (Lumi 4 at 25ns) - 25ns spillover - Pythia8 
    StepId             : 142721
    ApplicationName    : Gauss
    ApplicationVersion : v54r4
    OptionFiles        : $APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py;$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py;$APPCONFIGOPTS/Gauss/DataType-2016.py;$APPCONFIGOPTS/Gauss/RICHRandomHits.py;$DECFILESROOT/options/@{eventType}.py;$LBPYTHIA8ROOT/options/Pythia8.py;$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmOpt2.py
    DDB                : dddb-20200424-3
    CONDDB             : sim-20200507-6-vc-md100-Sim10
    ExtraPackages      : AppConfig.v3r400;Gen/DecFiles.v31r4
    Visible            : Y
-----------------------
Number of Steps   1791
Total number of files: 3682
         GAUSSHIST:100
         SIM:1791
         LOG:1791
Number of events 0
Path:  /MC/Dev/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim10Dev07

'''

from Configurables import DaVinci
DaVinci().InputType = 'LDST'
DaVinci().DataType = '2016'
DaVinci().Simulation = True
DaVinci().CondDBtag = 'sim-20200507-6-vc-md100-Sim10'
DaVinci().DDDBtag = 'dddb-20200424-3'
