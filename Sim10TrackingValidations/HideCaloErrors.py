'''
Hide errors from CaloReadoutTool
ERROR putStatusOnTES(): RawBankReadoutStatus not created for this event, not putting it on TES
which spam stdout
'''

from Gaudi.Configuration import allConfigurables, FATAL, appendPostConfigAction

def hide():
    tools = ('PrsFromRaw.PrsFromRawTool', 'SpdFromRaw.SpdFromRawTool', 
             'EcalZSup.EcalZSupTool', 'HcalZSup.HcalZSupTool')
    for tool in tools:
        allConfigurables[tool].OutputLevel = FATAL

appendPostConfigAction(hide)
