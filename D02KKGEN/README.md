# AnalysisProductions options for $` D^{0}\rightarrow K^+ K^- `$ generator level

The options contained in this directory generate the NTuples for the simulation
of $` D^{\ast+} \rightarrow (D^{0}\rightarrow K^+ K^-) \pi^{+}_{tag} `$
decays (either prompt or secondaries) at generator level. It is not a Full Simulation, as it is devoid of the detection response.

General features:
  * `MCTupleToolPrompt` and `MCTupleToolHierarchy` tools employed in order to distinguish prompt decays from secondaries and to check the decay trees;
  * `MCTupleToolPrimaries` returning the true PVs in order to conduct studies on secondaries, where `Dst_TRUEORIGINVERTEX` significantly differs from `TRUE_PV`.

Warnings:
  * as the detector is not simulated, no smearing effect caused by resolution/reconstruction is here present;
  * as already stated, this simulation contains secondary decays.
