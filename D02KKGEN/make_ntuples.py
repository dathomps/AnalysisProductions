from Configurables import (
    DaVinci,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
decay: Decay you want to inspect
decay_heads: Particles you'd like to see the decay tree of
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = "${Dst}[D*(2010)+ => ${D0}([D0]cc => ${P1}K+  ${P2}K- ) ${sPi}pi+]CC"
decay_heads = ["D*(2010)+", "D*(2010)-"]
year = 2018

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("KK_MCTuple")
mctuple.TupleName = "KK"
mctuple.setDescriptorTemplate(decay)
mctuple.ToolList = []

mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
mctuple.addTupleTool("MCTupleToolPID")
mctuple.addTupleTool("MCTupleToolPrimaries")
mctuple.addTupleTool("MCTupleToolPrompt")
mctuple.addTupleTool("MCTupleToolHierarchy")


# Configure DaVinci
DaVinci().TupleFile = "CHARM_GEN_D02HH_DVNTUPLE.ROOT"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
DaVinci().CondDBtag = 'sim-20190430-vc-mu100'
DaVinci().DDDBtag = 'dddb-20170721-3'
DaVinci().UserAlgorithms = [mctuple]


from Gaudi.Configuration import appendPostConfigAction
def doIt():
    """
    specific post-config action for (x)GEN-files 
    """
    extension = "rgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy 
    algs = deepcopy ( dod.AlgMap ) 
    bad  = set() 
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )
        
    for b in bad :
        del algs[b]
            
    dod.AlgMap = algs
    
    from Configurables import EventClockSvc, CondDB 
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )
    
appendPostConfigAction( doIt )
