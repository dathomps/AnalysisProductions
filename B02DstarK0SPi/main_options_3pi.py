#Remember to set here DaVinci().Simulation option

#options for B0->D*3Pi, D*->D0Pi
from Gaudi.Configuration import *
from GaudiConf import IOHelper

from Configurables import DaVinci, DecayTreeTuple, CombineParticles, FilterDesktop, TupleToolVeto, TupleToolDalitz, TupleToolPhotonInfo, LoKi__Hybrid__TupleTool, TupleToolDecayTreeFitter
from Configurables import TupleToolTrigger, TupleToolTISTOS
from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolEventInfo
from Configurables import MCDecayTreeTuple, MCTupleToolDecayType, MCTupleToolHierarchy, MCTupleToolKinematic, MCTupleToolEventType, MCTupleToolPrimaries, MCTupleToolInteractions, MCTupleToolPID, MCTupleToolAngles, MCTupleToolReconstructed

from DecayTreeTuple.Configuration import *

#set DaVinci.Simulation() option
DaVinci().Simulation=True

# Stream and stripping line
if DaVinci().Simulation: stream = 'B02DstK0sPi.strip'
else: stream = 'Bhadron'
line_names = ['B02DstPiPiPiDstar2D0PiPIDBeauty2CharmLine', 'B02DstPiPiPiWSDstar2D0PiPIDBeauty2CharmLine']
input_particles = 'Phys/{0}/Particles'
if DaVinci().Simulation: input_particles='/Event/B02DstK0sPi.Strip/'+input_particles, to be restored
#if DaVinci().Simulation: input_particles='/Event/B02DstK0sPi.Strip/MC/Particles', tested 22.11.21

######################### TRIGGER LINES ####################

commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
commonTriggerList += ['Hlt1'+x+'Decision' for x in ['TrackMVA','TwoTrackMVA']]

additionalD0TriggerList = ['Hlt2'+x+'Decision' for x in ['Hlt2CharmHadD02KmPipTurbo']];
additionalDstTriggerList = ['Hlt2'+x+'Decision' for x in ['Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo', 'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo']];
additionalBTriggerList = ['Hlt2'+x+'Decision' for x in ['Topo2Body', 'Topo3Body', 'Topo4Body']];

TriggerLists = {'lab0'     : commonTriggerList+additionalBTriggerList,
                'lab1'     : commonTriggerList+additionalDstTriggerList,
                'lab2'     : commonTriggerList+additionalD0TriggerList,
                'lab3'     : commonTriggerList,
                'lab4'     : commonTriggerList,
                'lab5'     : commonTriggerList,
#                'lab6'     : commonTriggerList,
                'lab7'     : commonTriggerList,
                'lab8'     : commonTriggerList,
                'lab9'     : commonTriggerList
                }


######################### NO SELECTIONS #########################

############################ nTUPLEs ###########################

#Set tools to store MC truth
def SetupMCTools(dtt):
    dtt.ToolList += ["TupleToolMCTruth","TupleToolMCBackgroundInfo"]
    mc_tools = ['MCTupleToolPrompt','MCTupleToolKinematic']
    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList = mc_tools
    dtt.addTool(MCTruth)
    return

# Create ntuples to store B0 decays and MCtruth
ntuple_names = [ 'TupleB02Dstar3Pi','TupleB02Dstar3PiWS' ]
mcntuple_names = [ 'MCTupleB02Dstar3Pi','MCTupleB02Dstar3PiWS' ]

def CreateMCTree(intuple,idecay):

    ntuple = mcntuple_names[intuple]
   
    B0MCTree = MCDecayTreeTuple(ntuple)

    if idecay == 0: 
        B0MCTree.setDescriptorTemplate('${B0}[B0 => ${Dstar}(D*(2010)+ => ${D0}(D0 => ${D0_Kminus}K- ${D0_piplus}pi+) ${Dstar_piplus}pi+) ${a1_piplus}pi+ ${a1_piminus1}pi- ${a1_piminus2}pi-]CC')
    if idecay == 1: 
        B0MCTree.setDescriptorTemplate('${B0}[B0 => ${Dstar}(D*(2010)+ => ${D0}(D0 => ${D0_Kminus}K- ${D0_piplus}pi+) ${Dstar_piplus}pi+) ${a1_piplus1}pi+ ${a1_piminus}pi- ${a1_piplus2}pi+]CC')

    B0MCTree.ToolList = []
    B0MCTree.ToolList = [
        "TupleToolEventInfo",
        "MCTupleToolDecayType",
        "MCTupleToolHierarchy",
        "MCTupleToolKinematic",
        "MCTupleToolPID"
    ]
    return B0MCTree
 #end function CreateMCTree


def CreateTree(intuple,idecay,isMC=False):

    ntuple = ntuple_names[intuple]
    line = line_names[intuple]

    B0Tree = DecayTreeTuple(ntuple)

    B0Tree.Inputs = [input_particles.format(line)]
    if idecay == 0:
        B0Tree.Decay = '[B0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+) ^pi+ ^pi- ^pi-]CC'
    if idecay == 1:
        B0Tree.Decay = '[B0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+) ^pi+ ^pi- ^pi+]CC'

    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
    B0Tree.ToolList = ['TupleTool'+tool for tool in GeneralTools]

    if(isMC): SetupMCTools(B0Tree)

    if idecay == 0:
        B0Tree.addBranches({'lab0': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ pi- pi-]CC',
                            'lab1': '[B0 -> ^(D*(2010)+ ->  (D0 -> K- pi+) pi+) pi+ pi- pi-]CC', #D*
                            'lab2': '[B0 -> (D*(2010)+ ->  ^(D0 -> K- pi+) pi+) pi+ pi- pi-]CC', #D0 (D*)
                            'lab3': '[B0 -> (D*(2010)+ ->  (D0 -> ^K- pi+) pi+) pi+ pi- pi-]CC', #K- (D0)
                            'lab4': '[B0 -> (D*(2010)+ ->  (D0 -> K- ^pi+) pi+) pi+ pi- pi-]CC', #pi+ (D0)
                            'lab5': '[B0 -> (D*(2010)+ ->  (D0 -> K- pi+) ^pi+) pi+ pi- pi-]CC', #pi+ (D*)
                            'lab7': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) ^pi+ pi- pi-]CC', #pi+ (a1)
                            'lab8': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ ^pi- pi-]CC', #pi- (a1)
                            'lab9': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ pi- ^pi-]CC'  #pi- (a1)
                            })

    if idecay == 1:
        B0Tree.addBranches({'lab0': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ pi- pi+]CC',
                            'lab1': '[B0 -> ^(D*(2010)+ ->  (D0 -> K- pi+) pi+) pi+ pi- pi+]CC', #D*
                            'lab2': '[B0 -> (D*(2010)+ ->  ^(D0 -> K- pi+) pi+) pi+ pi- pi+]CC', #D0 (D*)
                            'lab3': '[B0 -> (D*(2010)+ ->  (D0 -> ^K- pi+) pi+) pi+ pi- pi+]CC', #K- (D0)
                            'lab4': '[B0 -> (D*(2010)+ ->  (D0 -> K- ^pi+) pi+) pi+ pi- pi+]CC', #pi+ (D0)
                            'lab5': '[B0 -> (D*(2010)+ ->  (D0 -> K- pi+) ^pi+) pi+ pi- pi+]CC', #pi+ (D*)
                            'lab7': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) ^pi+ pi- pi+]CC', #pi+ (a1)
                            'lab8': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ ^pi- pi+]CC', #pi- (a1)
                            'lab9': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) pi+ pi- ^pi+]CC'  #pi- (a1)
                            })

    B0Tree.UseLabXSyntax = True
    B0Tree.RevertToPositiveID = False

#DFT no mass constraint
    B0_Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), 'lab0') ]
    fit = B0_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose = True
    fit.constrainToOriginVertex = True
    fit.UpdateDaughters = True

#DTF with mass constraints
    B0_Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), 'lab0') ]
    fit2 = B0_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit2')
    fit2.Verbose = True
    fit2.constrainToOriginVertex = True
    fit2.daughtersToConstrain += ['D0', 'D*(2010)+']
    fit2.UpdateDaughters = True

    for nodeName, tList in TriggerLists.iteritems():
        Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList

    return B0Tree
 #end function CreateTree

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
    Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

Tree = [CreateTree(0,0,DaVinci().Simulation)]
MCTree = [CreateMCTree(0,0)]
#Tree2 = [CreateTree(1,1,DaVinci().Simulation)]
#MCTree2 = [CreateMCTree(1,1)]

#algos are executed ACCORDING TO the sequence specified below, sequence of operation has to be kept
DaVinci().UserAlgorithms += [MomentumCorrection(DaVinci().Simulation)]

DaVinci().InputType = 'MDST'
if DaVinci().Simulation: DaVinci().RootInTES = '/Event/B02DstK0sPi.Strip'# 8.09.20
else: DaVinci().RootInTES = '/Event/{0}'.format(stream)

DaVinci().UserAlgorithms += Tree+MCTree#+Tree2+MCTree2

DaVinci().Lumi = True
DaVinci().TupleFile = 'DVntuple.root'
