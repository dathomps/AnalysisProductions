### B0(s)-> D*(2010)K0sPi analysis

# Analysis Production Version: 2
This is the 2nd time this AP has been run. The version 0 of the code was imported from CharmWGProd tool. There, we reconstructed B0(s)->DstarK0sPi in Run I and Run II data. In AP, the analysis version 1 reconstructed MC data samples that were specifically required for this study. This version 2 produces MCtruth tuples for signal.

## Analysis Summary
This analysis will study B0(s)->Dstar(2010)K0SPi. For details, please look at https://indico.cern.ch/event/1073788/contributions/4549516/attachments/2324088/3958204/palano_dstark0sh_oct21.pdf

## Options Files
The main options files are 'main_options.py' and 'main_options_bs0.py'. In this AP version they are devoted to the production of MCtruth tuples for B0 and B0s decays, respectively.

## Data (RunI, Run II)
RunI and RunII data were processed by means of the CharmWGProd analysis tool.

## MC samples (MagUp and MagDown)

MC data samples are related to the following event type:
11166171 (B0->Dstar(2010)K0sPi, signal)
11166002 (B0->Dstar(2010)PiPiPi, background)
13166151 (B0s->Dstar(2010)K0sPi, signal)

using the following Trig/Reco/Strippings:
 
2012 - Trig0x409f0045/Reco14c/Stripping21Filtered
2015 - Trig0x411400a2/Reco15a/Turbo02/Stripping24r2Filtered
2016 - Trig0x6139160F/Reco16/Turbo03a/Stripping28r2Filtered
2017 - Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered
2018 - Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34Filtered
