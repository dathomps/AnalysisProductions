# Option files to reconstruct $`D^{0}\rightarrow K^0_S \pi^+ \pi^- \pi^0`$ decays

Can be based on `Charm` or `Bhadron` streams (the latter only from S28r2p2, S29r2p2, S34r0p2)
Possible options are:
  * doubly tagged with $`B \to D^{\ast-} \mu^\pm`$ (`Charm` or `Bhadron`);
  * doubly tagged with $`B \to D^{\ast-} \pi^+`$ (`Bhadron`);
  * singly tagged with $`B \to \bar{D}^{0} \mu^\pm`$ (`Charm`);
  * prompt $`D^{\ast+}`$ (`Charm`).

Current production is for:
  * simulation from 2016 (Stripping28r2p1);
  * both CHARM and BHADRON streams.
