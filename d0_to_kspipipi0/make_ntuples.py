from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

from copy import copy

from Configurables import (
        DaVinci,
        GaudiSequencer,
        ChargedProtoANNPIDConf,
        LoKi__Hybrid__Dict2Tuple,
        LoKi__Hybrid__DictOfFunctors)
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

from PhysConf.Selections import (AutomaticData, MomentumScaling,
                                 RebuildSelection, SelectionSequence,
                                 CombineSelection)
from PhysConf.Filters import LoKi_Filters
from StandardParticles import StdAllLooseMuons
from TeslaTools import TeslaTruthUtils


if not DaVinci().Simulation:
    charm_stream = (DaVinci().RootInTES == '/Event/Charm')
else:
    charm_stream = (DaVinci().TupleFile == 'CHARM')

prompt = False
sl = False
sl_dt = True
had_dt = False if DaVinci().Simulation else (not charm_stream)

# check that configuration is consistent
if charm_stream:
    assert not had_dt
else:
    assert (not prompt) and (not sl)

# set stripping lines for each category
ks_types = ['LL', 'DD']
pi0_types = ['Merged', 'Resolved']
lines = {}
for ks in ks_types:
    for pi0 in pi0_types:
        if sl:
            lines['SL_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = ['b2D0MuXKsPiPiPi0{pi0}{ks}CharmFromBSemiLine'.format(pi0=pi0, ks=ks)]
        if sl_dt:
            lines['SL_DT_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = [
                    'b2DstarMuXKsPiPiPi0{pi0}{ks}CharmFromBSemiLine'.format(pi0=pi0, ks=ks)] if charm_stream else [
                    'B02DstarMuNu{kind}Dst2D0Pi_D2KSPi0HH{ks}{pi0}Beauty2CharmLine'.format(kind=kind, pi0=pi0, ks=ks) for kind in ['', 'WS']]
        if had_dt:
            lines['BHAD_DT_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = [
                    'B02DstarPi{kind}Dst2D0Pi_D2KSPi0HH{ks}{pi0}Beauty2CharmLine'.format(kind=kind, ks=ks, pi0=pi0) for kind in ['', 'WS']]
        if prompt:
            lines['Prompt_D02KSPipPimPi0_{pi0tag}{ks}'.format(pi0tag='' if pi0=='Merged' else 'Resolved_', ks=ks)] = [
                    'DstarD2KSHHPi0DstarD2PiPiPi0KS{ks}Line'.format(ks=ks)]

# set decay descriptors
decay_descriptor_sl     = '${B}[Xb ->                     ${D0}([D0]cc -> ${KS}(KS0 -> ${KS_Pip}pi+ ${KS_Pim}pi-) ${Pip}pi+ ${Pim}pi- ${Piz}pi0) ${Mu}mu- ]CC'
decay_descriptor_sl_dt  = '${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}([D0]cc -> ${KS}(KS0 -> ${KS_Pip}pi+ ${KS_Pim}pi-) ${Pip}pi+ ${Pim}pi- ${Piz}pi0) ${sPi}pi+)   ${Mu}[mu-]cc]CC'
decay_descriptor_had_dt = '${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}([D0]cc -> ${KS}(KS0 -> ${KS_Pip}pi+ ${KS_Pim}pi-) ${Pip}pi+ ${Pim}pi- ${Piz}pi0) ${sPi}pi+) ${B_Pi}[pi-]cc]CC'
decay_descriptor_prompt = '           ${Dst}[D*(2010)+ -> ${D0}([D0]cc -> ${KS}(KS0 -> ${KS_Pip}pi+ ${KS_Pim}pi-) ${Pip}pi+ ${Pim}pi- ${Piz}pi0) ${sPi}pi+]CC'
decay_descriptor_mc_sl_dt = '${B}[Xb --> ${Dst}(D*(2010)+ => ${D0}([D0]cc ==> ${KS}(KS0 => ${KS_Pip}pi+ ${KS_Pim}pi-) ${Pip}pi+ ${Pim}pi- ${Piz}pi0) ${sPi}pi+) ${Mu}mu- ${Nu}nu_mu~]CC'  # selects only semimuonic decays with a single neutrino and without D** resonances

decay_descriptors = {}
decay_descriptors_mc = {}
for ks in ks_types:
    for pi0 in pi0_types:
        decay_descriptors['SL_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = decay_descriptor_sl
        decay_descriptors['SL_DT_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = decay_descriptor_sl_dt
        decay_descriptors['BHAD_DT_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = decay_descriptor_had_dt
        decay_descriptors['Prompt_D02KSPipPimPi0_{pi0_tag}{ks}'.format(pi0_tag='' if pi0=='Merged' else 'Resolved_', ks=ks)] = decay_descriptor_prompt
        decay_descriptors_mc['SL_DT_D02KSPipPimPi0_{pi0}_{ks}'.format(pi0=pi0, ks=ks)] = decay_descriptor_mc_sl_dt

for k, v in decay_descriptors.items():
    if 'Resolved' in k:
        decay_descriptors[k] = v.replace('pi0', '(pi0 -> ${gamma1}gamma ${gamma2}gamma)')
for k, v in decay_descriptors_mc.items():
    if 'Resolved' in k:
        v = v.replace('pi0', '(pi0 => ${gamma1}gamma ${gamma2}gamma)')


def AddDTF(dtt, kind, constrain_pv=False, constrain_dst_m=False,
        constrain_d0_m=False, constrain_ks_m=True, constrain_pi0_m=True,
        resolved=False):

    assert kind in ['SL', 'SL_DT', 'BHAD_DT', 'Prompt']

    # DecayTreeFitter configuration
    # https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinciTutorial9b
    name = 'DTF{0}{1}{2}{3}{4}'.format(
            '_PV' if constrain_pv else '',
            '_DstM' if constrain_dst_m else '',
            '_D0M' if constrain_d0_m else '',
            '_KSM' if constrain_ks_m else '',
            '_Pi0M' if constrain_pi0_m else '')

    if kind == 'Prompt':
        head_particle = getattr(dtt, 'Dst')
        d0_parent = getattr(dtt, 'Dst')
    else:
        head_particle = getattr(dtt, 'B')
        if kind == 'SL':
            d0_parent = getattr(dtt, 'B')
        else:
            d0_parent = getattr(dtt, 'Dst')
    DictTuple = head_particle.addTupleTool(LoKi__Hybrid__Dict2Tuple,
                                           '{}_DictTuple'.format(name))
    DictTuple.addTool(DTFDict, name)
    DictTuple.Source = 'LoKi::Hybrid::DTFDict/{}'.format(name)
    num_var = 28
    if resolved:
        num_var += 7  # for photons
    if kind in ['SL_DT', 'BHAD_DT']:
        num_var += 4  # for Dst_M and sPi momentum
    DictTuple.NumVar = num_var
    dtf = getattr(DictTuple, name)
    dtf.constrainToOriginVertex = constrain_pv
    daughtersToConstrain = []
    if constrain_dst_m:
        daughtersToConstrain.extend(['D*(2010)+', 'D*(2010)-'])
    if constrain_d0_m:
        daughtersToConstrain.extend(['D0', 'D~0'])
    if constrain_ks_m:
        daughtersToConstrain.extend(['KS0'])
    if constrain_pi0_m:
        daughtersToConstrain.extend(['pi0'])
    dtf.daughtersToConstrain = copy(daughtersToConstrain)
    dtf.addTool(LoKi__Hybrid__DictOfFunctors, 'dict')
    dtf.Source = 'LoKi::Hybrid::DictOfFunctors/dict'
    # order should reflect stripping
    #     D* -> D0 pi+ for charm stream
    #     D* -> pi+ D0 for bhadron stream
    d0_index = 1 if charm_stream else 2
    sPi_index = 2 if charm_stream else 1
    ks_index = 1 if charm_stream else 3
    pip_index = 2 if charm_stream else 1
    pim_index = 3 if charm_stream else 2
    d0_string = 'CHILD(1, {})' if kind in ['SL', 'Prompt'] else 'CHILD(1, CHILD({}, {{}}))'.format(d0_index)
    dtf.dict.Variables = {
            '{}_M'.format('Dst' if kind=='Prompt' else 'B'): 'M',
            'D0_M': d0_string.format('M'),
            'D0_PX': d0_string.format('PX'),
            'D0_PY': d0_string.format('PY'),
            'D0_PZ': d0_string.format('PZ'),
            'KS_PX': d0_string.format('CHILD({}, PX)'.format(ks_index)),
            'KS_PY': d0_string.format('CHILD({}, PY)'.format(ks_index)),
            'KS_PZ': d0_string.format('CHILD({}, PZ)'.format(ks_index)),
            'KS_Pip_PX': d0_string.format('CHILD({}, CHILD(1, PX))'.format(ks_index)),
            'KS_Pip_PY': d0_string.format('CHILD({}, CHILD(1, PY))'.format(ks_index)),
            'KS_Pip_PZ': d0_string.format('CHILD({}, CHILD(1, PZ))'.format(ks_index)),
            'KS_Pim_PX': d0_string.format('CHILD({}, CHILD(2, PX))'.format(ks_index)),
            'KS_Pim_PY': d0_string.format('CHILD({}, CHILD(2, PY))'.format(ks_index)),
            'KS_Pim_PZ': d0_string.format('CHILD({}, CHILD(2, PZ))'.format(ks_index)),
            'Pip_PX': d0_string.format('CHILD({}, PX)'.format(pip_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Pip_PY': d0_string.format('CHILD({}, PY)'.format(pip_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Pip_PZ': d0_string.format('CHILD({}, PZ)'.format(pip_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Pim_PX': d0_string.format('CHILD({}, PX)'.format(pim_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Pim_PY': d0_string.format('CHILD({}, PY)'.format(pim_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Pim_PZ': d0_string.format('CHILD({}, PZ)'.format(pim_index)),  # Pip and Pim will be flipped for Bhadron stream
            'Piz_PX': d0_string.format('CHILD(4, PX)'),
            'Piz_PY': d0_string.format('CHILD(4, PY)'),
            'Piz_PZ': d0_string.format('CHILD(4, PZ)'),
            '{}_PX'.format('sPi' if kind=='Prompt' else 'B_Pi' if kind=='BHAD_DT' else 'Mu'): 'CHILD(2, PX)',
            '{}_PY'.format('sPi' if kind=='Prompt' else 'B_Pi' if kind=='BHAD_DT' else 'Mu'): 'CHILD(2, PY)',
            '{}_PZ'.format('sPi' if kind=='Prompt' else 'B_Pi' if kind=='BHAD_DT' else 'Mu'): 'CHILD(2, PZ)'}
    if kind in ['SL_DT', 'BHAD_DT']:
        dtf.dict.Variables.update({
                'Dst_M': 'CHILD(1, M)',
                'sPi_PX': 'CHILD(1, CHILD({}, PX))'.format(sPi_index),
                'sPi_PY': 'CHILD(1, CHILD({}, PY))'.format(sPi_index),
                'sPi_PZ': 'CHILD(1, CHILD({}, PZ))'.format(sPi_index)})
    if resolved:
        dtf.dict.Variables.update({
                'Piz_M': d0_string.format('CHILD(4, M)'),
                'gamma1_PX': d0_string.format('CHILD(4, CHILD(1, PX))'),
                'gamma1_PY': d0_string.format('CHILD(4, CHILD(1, PY))'),
                'gamma1_PZ': d0_string.format('CHILD(4, CHILD(1, PZ))'),
                'gamma2_PX': d0_string.format('CHILD(4, CHILD(2, PX))'),
                'gamma2_PY': d0_string.format('CHILD(4, CHILD(2, PY))'),
                'gamma2_PZ': d0_string.format('CHILD(4, CHILD(2, PZ))')})

    if constrain_dst_m or constrain_d0_m or constrain_ks_m or constrain_pi0_m:
        string_for_constraint = ', strings([{}])'.format(', '.join(
                '"{}"'.format(daugh) for daugh in daughtersToConstrain))
    else:
        string_for_constraint = ''

    # the DTF fit is run once for each of the following variable
    Loki_DTF = head_particle.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_{0}'.format(
            name))
    Loki_DTF.Variables = {
            '{0}_D0_CTAU'.format(name): 'DTF_CTAU("D0" == ABSID, {}{})'.format(
                    True if constrain_pv else False, string_for_constraint),
            '{0}_D0_CTAUERR'.format(name): 'DTF_CTAUERR("D0" == ABSID, {}{})'.format(
                    True if constrain_pv else False, string_for_constraint)}
    return


# https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/*/src/*
tuple_tools = [
        'TupleToolEventInfo',   # DecayTreeTupleTrigger
        'TupleToolBeamSpot',    # DecayTreeTuple
        'TupleToolPid',         # DecayTreeTuple
        'TupleToolRecoStats']   # DecayTreeTupleReco

l0_triggers = [
        'L0ElectronDecision',
        'L0HadronDecision',
        'L0PhotonDecision']
l0_muon_triggers = [
        'L0MuonDecision',
        'L0DiMuonDecision']

hlt1_triggers = [
        'Hlt1TrackMVADecision',
        'Hlt1TwoTrackMVADecision']
hlt1_muon_triggers = [
        'Hlt1TrackMuonDecision',
        'Hlt1TrackMuonMVADecision']

hlt2_triggers = [
        'Hlt2InclHc2HHXDecision',
        'Hlt2InclHcst2PiHc2HHXDecision',
        'Hlt2InclDst2PiD02HHXDecision',
        'Hlt2Topo2BodyDecision',
        'Hlt2Topo3BodyDecision',
        'Hlt2Topo4BodyDecision']
hlt2_muon_triggers = [
        'Hlt2SingleMuonDecision',
        'Hlt2TopoMu2BodyDecision',
        'Hlt2TopoMu3BodyDecision',
        'Hlt2TopoMu4BodyDecision']

loki_vars_dst = {
        'DOCACHI2': 'DOCACHI2(1,2)',
        'DOCA': 'DOCA(1,2)'}

loki_vars_d0 = {
        'DOCACHI2_PipPim': 'DOCACHI2(2,3)',
        'DOCA_PipPim': 'DOCA(2,3)',
        'DOCACHI2_KSPip': 'DOCACHI2(1,2)',
        'DOCA_KSPip': 'DOCA(1,2)',
        'DOCACHI2_KSPim': 'DOCACHI2(1,3)',
        'DOCA_KSPim': 'DOCA(1,3)',
        'BPVLTIME': 'BPVLTIME()'}

loki_vars_B = {
        'BPVMCORR' : 'BPVCORRM',
        'DOCA'     : 'DOCA(1,2)'}


def SetupMCTools(dtt):
    dtt.ToolList += ['TupleToolMCTruth', 'TupleToolMCBackgroundInfo', 'MCTupleToolRedecay']
    mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic', 'MCTupleToolHierarchy']
    mc_truth = TupleToolMCTruth()
    mc_truth.ToolList = mc_tools
    dtt.addTool(mc_truth)
    return


def MakeTuple(key):
    """ Returns a DecayTreeTuple algorithm configured to be run on real data"""

    resolved = True if 'Resolved' in key else False

    if 'Prompt' in key:
        kind = 'Prompt'
    elif 'BHAD_DT' in key:
        kind = 'BHAD_DT'
    elif 'SL_DT' in key:
        kind = 'SL_DT'
    else:
        kind = 'SL'

    dtt = DecayTreeTuple("{0}_Tuple".format(key))
    dtt.TupleName = key
    dtt.setDescriptorTemplate(decay_descriptors[key])
    dtt.Inputs = ['Phys/{}/Particles'.format(line) for line in lines[key]]

    dtt.ToolList = copy(tuple_tools)
    dtt.Piz.addTupleTool("TupleToolPi0Info")
    if resolved:
        for gamma in [dtt.gamma1, dtt.gamma2]:
            gamma.addTupleTool('TupleToolPhotonInfo')
            if not DaVinci().Simulation:  # TODO
                gamma.addTupleTool('TupleToolCaloHypo')
            mytool = gamma.addTupleTool('TupleToolProtoPData')
            mytool.DataList = ['IsNotE', 'isNotH']

    # path of header files starts with https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/*/src
    dtt.addTupleTool("TupleToolTrackInfo").Verbose = True               # DecayTreeTupleReco
    dtt.addTupleTool("TupleToolGeometry").Verbose = False               # DecayTreeTuple
    dtt.addTupleTool("TupleToolKinematic").Verbose = True               # DecayTreeTuple
    dtt.addTupleTool("TupleToolANNPID").ANNPIDTunes = ["MC15TuneV1"]    # DecayTreeTupleANNPID
    primTool = dtt.addTupleTool("TupleToolPrimaries")                   # DecayTreeTuple
    primTool.Verbose = True

    # trigger global info for the event - DecayTreeTupleTrigger/src/TupleToolTrigger.h
    global_trigger = dtt.addTupleTool("TupleToolTrigger")
    global_trigger.VerboseL0 = True
    global_trigger.TriggerList = copy(l0_triggers) + copy(l0_muon_triggers)

    # TISTOS - DecayTreeTupleTrigger/src/TupleToolTISTOS.h
    tis_tos_tools = [
            dtt.D0.addTupleTool("TupleToolTISTOS"),
            dtt.KS.addTupleTool("TupleToolTISTOS"),
            dtt.Pip.addTupleTool("TupleToolTISTOS"),
            dtt.Pim.addTupleTool("TupleToolTISTOS"),
            dtt.Piz.addTupleTool("TupleToolTISTOS")]
    for tis_tos_tool in tis_tos_tools:
        tis_tos_tool.VerboseL0 = True
        tis_tos_tool.VerboseHlt1 = True
        tis_tos_tool.TriggerList = l0_triggers + hlt1_triggers

    if kind == 'Prompt':
        hlt2_tool = dtt.Dst.addTupleTool('TupleToolTISTOS')
    else:
        hlt2_tool = dtt.B.addTupleTool('TupleToolTISTOS')
    hlt2_tool.VerboseHlt2 = True
    hlt2_tool.FillHlt2 = True
    hlt2_tool.TriggerList = copy(hlt2_triggers)

    if kind in ['SL', 'SL_DT']:
        tis_tos_muon = dtt.Mu.addTupleTool('TupleToolTISTOS')
        tis_tos_muon.VerboseL0 = True
        tis_tos_muon.VerboseHlt1 = True
        tis_tos_muon.TriggerList = l0_muon_triggers + hlt1_muon_triggers + hlt1_triggers

        hlt2_tool.TriggerList += copy(hlt2_muon_triggers)

        LoKi_B = dtt.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B")
        LoKi_B.Variables = loki_vars_B


    LoKi_D0 = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
    LoKi_D0.Variables = loki_vars_d0

    AddDTF(dtt, kind, constrain_pv=(kind in ['Prompt', 'BHAD_DT']), constrain_dst_m=False, constrain_d0_m=False, constrain_ks_m=True, constrain_pi0_m=True, resolved=resolved)
    AddDTF(dtt, kind, constrain_pv=(kind in ['Prompt', 'BHAD_DT']), constrain_dst_m=False, constrain_d0_m=True, constrain_ks_m=True, constrain_pi0_m=True, resolved=resolved)
    if kind == 'BHAD_DT':
        AddDTF(dtt, kind, constrain_pv=(kind in ['Prompt', 'BHAD_DT']), constrain_dst_m=True, constrain_d0_m=True, constrain_ks_m=True, constrain_pi0_m=True, resolved=resolved)

    if DaVinci().Simulation:
        SetupMCTools(dtt)

    return dtt


def MakeTupleMC(key):
    """ Returns a MCDecayTreeTuple algorithm configured to be run on simulated
        data
    """

    dtt = MCDecayTreeTuple('{0}_MCTuple'.format(key))
    dtt.TupleName = key
    dtt.setDescriptorTemplate(decay_descriptors_mc[key])
    dtt.ToolList = [
            "MCTupleToolKinematic",
            "TupleToolEventInfo"]
    return dtt


tuples = [MakeTuple(key) for key in lines.keys()]
if DaVinci().Simulation:
    tuples += [MakeTupleMC(key) for key in lines.keys()]


def MomentumCorrection(is_mc=False):
    """
    Returns the momentum scale correction algorithm for data tracks or the
    momentum smearing algorithm for MC tracks
    """
    if is_mc:
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    else:
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    return


allLines = []
for linesList in lines.values():
    allLines.extend(linesList)

hlt1_filter = ' | '.join('HLT_PASS_RE("{}")'.format(line) for line in hlt1_triggers + hlt1_muon_triggers)
hlt2_filter = ' | '.join('HLT_PASS_RE("{}")'.format(line) for line in hlt2_triggers + hlt2_muon_triggers)
stripping_filter = ' | '.join(["HLT_PASS_RE('Stripping{}Decision')".format(line) for line in allLines])

strip_trig_filter = LoKi_Filters(
        HLT1_Code=hlt1_filter,
        HLT2_Code=hlt2_filter,
        STRIP_Code=stripping_filter)

DaVinci().EventPreFilters = strip_trig_filter.filters('StrippingTriggerFilter')
DaVinci().UserAlgorithms += [MomentumCorrection(DaVinci().Simulation)]
DaVinci().UserAlgorithms += tuples
