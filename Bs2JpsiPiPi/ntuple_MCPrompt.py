from Gaudi.Configuration import *

from Configurables import  DecayTreeTuple
line =  'B2JpsiHHBs2Jpsif0PrescaledLine'

from Configurables import DaVinci
#DaVinci().Lumi = False
#DaVinci().InputType = 'DST'
#DaVinci().EvtMax = -1
#DaVinci().Simulation = True
#DaVinci().DataType = "2016"
#DaVinci().PrintFreq	  = 10000
#DaVinci().DDDBtag         =  'dddb-20150724' #''
#DaVinci().CondDBtag       = 'sim-20161124-2-vc-md100' #''

tuple = DecayTreeTuple( 'Bs2Jpsif0Tree' )
tuple.Inputs = [ '/Event/AllStreams/Phys/{0}/Particles'.format(line) ]
tuple.TupleName = "mytree"
tuple.Decay = 'B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(f_0(980) -> ^pi+ ^pi-)'
tuple.Branches = {
    "Bs"		:  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ pi-)",
    "R"		:  "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(f_0(980) -> pi+ pi-)",
    "H1"		:  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> ^pi+ pi-)",
    "H2"		:  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ ^pi-)",
    "J_psi_1S"	:  "B_s0 -> ^(J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ pi-)"
   }

#particles
from Configurables import TupleToolDecay
names = ['Bs','R','H1','H2','J_psi_1S']
for part in names:
    tuple.addTool(TupleToolDecay, name = part)

from Configurables import TupleToolAngles, TupleToolEventInfo, TupleToolKinematic, TupleToolPid, TupleToolPrimaries, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTagging, TupleToolPropertime, TupleToolGeometry, TupleToolMCTruth, TupleToolMCBackgroundInfo
tuple.ToolList = [ "TupleToolAngles"     ,
                   "TupleToolEventInfo"  ,
			 "TupleToolKinematic"  ,
			 "TupleToolPid"        ,
			 "TupleToolPrimaries"  ,
			 "TupleToolTrackInfo"  ,
			 "TupleToolRecoStats"  ,
			 "TupleToolGeometry"   ,
#			 "TupleToolTagging"    ,
			 "TupleToolMCTruth"    ,
			 "TupleToolMCBackgroundInfo" ]

#Geo
TupleToolGeometry            = TupleToolGeometry('TupleToolGeometry')
TupleToolGeometry.RefitPVs   = True
TupleToolGeometry.Verbose    = True
tuple.addTool(TupleToolGeometry)

#Refit
tuple.ReFitPVs = True

#Trigger
from Configurables import TupleToolTISTOS
triglist = [ "L0MuonDecision"                 ,
             "L0DiMuonDecision"               ,
		 "Hlt1DiMuonHighMassDecision"     ,
		 "Hlt1TrackMuonDecision"          ,
		 "Hlt1TrackMVADecision"           ,
		 "Hlt2DiMuonDetachedDecision"     ,
		 "Hlt2DiMuonDetachedJPsiDecision" ,
		 "Hlt2DiMuonJPsiDecision"         ]

tistos = TupleToolTISTOS('tistos')
tistos.VerboseL0   = True
tistos.VerboseHlt1 = True
tistos.VerboseHlt2 = True
tistos.TriggerList = triglist[:]
tuple.Bs.addTool(tistos)
tuple.Bs.ToolList+=[ "TupleToolTISTOS/tistos" ]
tuple.J_psi_1S.addTool(tistos)
tuple.J_psi_1S.ToolList+=[ "TupleToolTISTOS/tistos" ]

#Change  TOSFracMu for Jpsi 
from Configurables import TriggerTisTos
tuple.J_psi_1S.tistos.addTool(TriggerTisTos())
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracMuon = 0.
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracEcal = 0.
tuple.J_psi_1S.tistos.TriggerTisTos.TOSFracHcal = 0.

#Loki
from Configurables import LoKi__Hybrid__TupleTool
LoKi_B = LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Variables =  { "LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )"                ,
			    "LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )"               ,
			    "LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )"             ,
			    "LOKI_DTF_CTAUs"       : "DTF_CTAUSIGNIFICANCE( 0, True )"    ,
			    "LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
			    "ETA"                  : "ETA"                                ,
			    "Y"                    : "Y"                                  }
tuple.Bs.addTool(LoKi_B)
tuple.Bs.ToolList+=[ "LoKi::Hybrid::TupleTool/LoKi_B" ]

#DTF
from Configurables import TupleToolDecayTreeFitter
PVFit = TupleToolDecayTreeFitter("PVFit")
PVFit.Verbose = True
PVFit.UpdateDaughters = True
PVFit.constrainToOriginVertex = True

PVFitJpsi = TupleToolDecayTreeFitter("PVFitJpsi")
PVFitJpsi.Verbose = True
PVFitJpsi.UpdateDaughters = True
PVFitJpsi.constrainToOriginVertex = True
PVFitJpsi.daughtersToConstrain = [ "J/psi(1S)" ]

PVFitBs = TupleToolDecayTreeFitter("PVFitBs")
PVFitBs.Verbose = True
PVFitBs.UpdateDaughters = True
PVFitBs.constrainToOriginVertex = True
PVFitBs.daughtersToConstrain = [ "J/psi(1S)", "B_s0" ]

tuple.Bs.addTool(PVFit)
tuple.Bs.ToolList+=[ "TupleToolDecayTreeFitter/PVFit" ]
tuple.Bs.addTool(PVFitJpsi)
tuple.Bs.ToolList+=[ "TupleToolDecayTreeFitter/PVFitJpsi" ]
tuple.Bs.addTool(PVFitBs)
tuple.Bs.ToolList+=[ "TupleToolDecayTreeFitter/PVFitBs" ]

#MCTruth
MCTruth = TupleToolMCTruth()
MCTruth.ToolList = [ "MCTupleToolKinematic" ,
                     "MCTupleToolHierarchy" ,
		     "MCTupleToolPID"]
tuple.addTool(MCTruth)


#check pv
from Configurables import CheckPV
checkpv = CheckPV()

DaVinci().UserAlgorithms = [checkpv,tuple]






