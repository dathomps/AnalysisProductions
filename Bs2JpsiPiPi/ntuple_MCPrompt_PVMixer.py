from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolGeometry
from Configurables import TupleToolTrigger, TupleToolTISTOS
from Configurables import TupleToolDecay, TupleToolRecoStats
from Configurables import (LoKi__Hybrid__TupleTool, TupleToolTrackPosition,
                           TupleToolTrackInfo)
from Configurables import TupleToolTagging, TupleToolP2VV
from Configurables import PVMixer
from GaudiConfUtils.ConfigurableGenerators import DecayTreeTuple as TUPLE
from PhysSelPython.Wrappers import SimpleSelection
from Configurables import TriggerTisTos
from Configurables import CondDB
from PhysSelPython.Wrappers import AutomaticData  # , MomentumScaling
from Configurables import TupleToolDecayTreeFitter
from Configurables import TriggerTisTos
from Configurables import LoKi__Hybrid__EvtTupleTool
from GaudiConf import IOHelper

from PhysSelPython.Wrappers import (
    FilterSelection,
    SelectionSequence,
    TupleSelection
)

from Gaudi.Configuration import *


from Configurables import (
    CheckPV,
    DaVinci,
    GaudiSequencer,
    TrackScaleState,
    TrackSmearState
)

def tuple_sequence():
    """Return the sequencer used for running tuple algorithms.

    The sequencer is configured such that all members are run, as their filter
    flags are ignored.
    """
    seq = GaudiSequencer('TupleSeq')
    seq.IgnoreFilterPassed = True

    return seq



name = 'Bs2JpsiPiPi_Prompt_MC_PVMixer'
line = 'B2JpsiHHBs2Jpsif0PrescaledLine'
#DaVinci().Simulation = True
#DaVinci().InputType = 'DST'
#DaVinci().EvtMax = -1                       # Number of events
#DaVinci().DataType = "2016"
#DaVinci().PrintFreq	  = 10000
#DaVinci().DDDBtag         =  'dddb-20150724' #''
#DaVinci().CondDBtag       = 'sim-20161124-2-vc-md100' #''
#DaVinci().Lumi = False

mixer = PVMixer("PVMixer")
mixer.PVOutputLocation = 'Rec/Vertex/Mixed'
mixer.WaitEvents = 2


tes_root = '/Event/AllStreams'
location = 'Phys/{}/Particles'.format(line)
location = os.path.join(tes_root, location)
tuple_input = AutomaticData(location)

decay='B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(f_0(980) -> ^pi+ ^pi-)'
branches={
    "Bs":  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ pi-)",
    "R":  "B_s0 -> (J/psi(1S) -> mu+ mu-) ^(f_0(980) -> pi+ pi-)",
    "H1":  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> ^pi+ pi-)",
    "H2":  "B_s0 -> (J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ ^pi-)",
    "J_psi_1S":  "B_s0 -> ^(J/psi(1S) -> mu+ mu-) (f_0(980) -> pi+ pi-)"
}



tuple_input = FilterSelection(
    'CheckRefittedPVs_{}'.format(name),
    [tuple_input],
    Code='BPVVALID()',
    ReFitPVs=True
)

tools = [
    'TupleToolEventInfo',
    'TupleToolKinematic',
    'TupleToolPid',
    'TupleToolPrimaries',
    'TupleToolTISTOS',
    "TupleToolAngles",
    "TupleToolRecoStats",
    "TupleToolTrackInfo",
    "TupleToolMCBackgroundInfo"
]

L0_LINES = [
    'L0DiMuonDecision',
    'L0MuonDecision'
]
HLT1_LINES = [
    'Hlt1DiMuonHighMassDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TrackMuonDecision'
]
HLT2_LINES = [
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonJPsiDecision'
]
TRIGGER_LINES = L0_LINES + HLT1_LINES + HLT2_LINES

tuple_selection = TupleSelection(
    '{}_Tuple'.format(name),
    [tuple_input],
    decay,
    ToolList=tools,
    ReFitPVs= True
)
dtt = tuple_selection.algorithm()
dtt.addBranches(branches)


geom = dtt.addTupleTool('TupleToolGeometry')
geom.RefitPVs = True
geom.Verbose = True


trig_tool = dtt.addTupleTool('TupleToolTrigger')
trig_tool.Verbose = True
trig_tool.TriggerList = TRIGGER_LINES
trig_tool.OutputLevel = 6

b_tistos = dtt.Bs.addTupleTool('TupleToolTISTOS')
b_tistos.Verbose = True
b_tistos.VerboseL0   = True
b_tistos.VerboseHlt1 = True
b_tistos.VerboseHlt2 = True
b_tistos.TriggerList = TRIGGER_LINES

jpsi_tistos = dtt.J_psi_1S.addTupleTool('TupleToolTISTOS')
jpsi_tistos.Verbose = True
jpsi_tistos.TriggerList = TRIGGER_LINES


#from Configurables import LoKi__Hybrid__TupleTool
LoKi_B = dtt.Bs.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_B')
LoKi_B.Variables = {
    "LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )"                ,
    "LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )"               ,
    "LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )"             ,
    "LOKI_DTF_CTAUs"       : "DTF_CTAUSIGNIFICANCE( 0, True )"    ,
    "LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
    "ETA"                  : "ETA"                                ,
    "Y"                    : "Y"                                  
}

dtt.InputPrimaryVertices = mixer.PVOutputLocation
dtt.UseP2PVRelations = True
dtt.IgnoreP2PVFromInputLocations = True
# Use PV mixing refitter
dtt.PVReFitters = {"": "MixingPVReFitter:PUBLIC"}

fake_fun = ('BPV(LoKi.Vertices.Info({}, -99., -100.))'.format(mixer.getProp('ExtraInfoKey')))
LoKi_B.Variables['FakeVertex'] = fake_fun



# refit with constraints
fitter_params = [
    ('PVFit', True, []),
    ('PVFitJpsi', True, ['J/psi(1S)']),
    ('PVFitBs', True, ['B_s0', 'J/psi(1S)'])
]

for fitter_name, constrain_to_pv, daughter_constraints in fitter_params:
    fitter = dtt.Bs.addTupleTool('TupleToolDecayTreeFitter/{}'.format(fitter_name))
    fitter.Verbose = True
    fitter.UpdateDaughters = True
    fitter.constrainToOriginVertex = constrain_to_pv
    fitter.daughtersToConstrain = daughter_constraints

tuple_seq = SelectionSequence('{}_SelSeq'.format(name), tuple_selection)

mctruth = dtt.addTupleTool('TupleToolMCTruth')
mctruth.ToolList = [
    'MCTupleToolKinematic',
    'MCTupleToolHierarchy',
    'MCTupleToolPID'
]


tuple_seq=tuple_seq.sequence()
dtt = tuple_seq.Members[-1]
seq = tuple_sequence()
seq.Members += [mixer, tuple_seq] 


main_seq = GaudiSequencer('MainSeq')
main_seq.Members = [
    CheckPV(),
    seq
]

DaVinci().UserAlgorithms = [main_seq]

