from StandardParticles import StdAllNoPIDsPions as pions
from StandardParticles import StdTightANNKaons as kaons

from Configurables import (
    DaVinci,
    DstConf,
    TurboConf,
    TupleToolRecoStats,
    CondDB,
    TupleToolMCTruth,
    TupleToolMCBackgroundInfo,
    TupleToolDecay,
    LoKi__Hybrid__DictOfFunctors,
    LoKi__Hybrid__Dict2Tuple,
    LoKi__Hybrid__DTFDict as DTFDict,
)
from PhysConf.Selections import (
    FilterSelection,
    MomentumScaling,
    MomentumSmear,
    TupleSelection,
    CombineSelection,
    Combine3BodySelection,
    SelectionSequence,
    RebuildSelection,
    AutomaticData,
)
from PhysConf.Filters import LoKi_Filters
from TeslaTools import TeslaTruthUtils
from DecayTreeTuple.Configuration import *  # noqa: F403, F401

# can be taken out for AProds
# DaVinci().DataType = "2016"
# DaVinci().Simulation = False
# DaVinci().Turbo = True
#


all_sequences = []

HLT2_LINE = "Hlt2CharmHadLcpToPpKmPipTurbo"

toolList = [
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolANNPID",
    "TupleToolGeometry",
    "TupleToolPrimaries",
    "TupleToolTrackInfo",
    "TupleToolEventInfo",
]

mcToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]

Xic_decay_products = {
    "K-": "(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNk>0.1) & (PT>200)",
    "pi+": "(BPVIPCHI2() < 9) & (TRGHOSTPROB<0.3) & (PROBNNpi>0.1) & (PT>200)",
}

LcKpi_combi_cuts = dict(
    CombinationCut="(AM - AM1 < 1800) & (APT2 > 180*MeV) & (APT3 > 180*MeV)",
    # Very loose cut on the Lc Pi mass difference around the expected two resonances (as seen in simulation)
    Combination12Cut="(AM>0) & (APT2 > 180*MeV) & ((AM-AM1) < 400)",
    DaughtersCuts=Xic_decay_products,
    MotherCut=(
        "BPVVALID() & (M - M1 < 1600) & ((M12-M1) < 375) & "
        "(VFASPF(VCHI2PDOF) < 20) & "
        "(CHILD(PROBNNk, 3) > 0.1) & (CHILD(PT,3) > 200*MeV) & (CHILD(TRGHOSTPROB, 3) < 0.3) & (CHILD(BPVIPCHI2(), 3) < 9) & "  # K
        "(CHILD(PROBNNpi, 2) > 0.1) & (CHILD(PT,2) > 200*MeV) & (CHILD(TRGHOSTPROB, 2) < 0.3) & (CHILD(BPVIPCHI2(), 2) < 9)"  # Pi
    ),
    ReFitPVs=True,
    CheckOverlapTool="LoKi::CheckOverlap",
)

Xic_LoKiVariables = {
    "BPVVDCHI2": "BPVVDCHI2",
    "BPVIPCHI2": "BPVIPCHI2()",
    # DOCA information
    "DOCAMAX": "DOCAMAX",
    "DOCAMIN": "LoKi.Particles.PFunA(AMINDOCA('LoKi::TrgDistanceCalculator'))",
    "DOCACHI2MAX": "DOCACHI2MAX",
    "DOCA12": "DOCA(1,2)",
    "DOCA13": "DOCA(1,3)",
    "DOCA23": "DOCA(2,3)",
    "Y": "Y",
    "ETA": "ETA",
    "SUM_PT": "(CHILD(PT, 1) + CHILD(PT, 2) + CHILD(PT, 3))",
    "M12": "M12",
    "M23": "M23",
    "M13": "M13",
}

if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)


# Get the Lc, kaon, pion (persistreco) candidates
Lcp = AutomaticData("%s/Particles" % HLT2_LINE)
my_kaons = RebuildSelection(kaons)
my_pions = RebuildSelection(pions)

if DaVinci().Simulation:
    Lcp = MomentumSmear(Lcp)
else:
    Lcp = MomentumScaling(Lcp, Turbo="PERSISTRECO", Year=DaVinci().DataType)

# Lc cuts
Lcp = FilterSelection("Lc2pKpi_filter", [Lcp], Code="(BPVIPCHI2() < 9)")


def MakeDTFD(
    dtt,
    branch_name,
    variables,
    tool_name="DTFTuple",
    constrainPV=True,
    constrainDaughters=None,
):
    branch = getattr(dtt, branch_name)

    DictTuple = branch.addTupleTool(LoKi__Hybrid__Dict2Tuple, tool_name)
    DictTuple.addTool(DTFDict, tool_name)
    DictTuple.Source = "LoKi::Hybrid::DTFDict/" + tool_name
    DictTuple.NumVar = len(
        variables.items()
    )  # reserve a suitable size for the dictionary

    DTF = getattr(DictTuple, tool_name)

    # configure the DecayTreeFitter in the usual way
    DTF.constrainToOriginVertex = constrainPV
    if constrainDaughters is not None:
        DTF.daughtersToConstrain = constrainDaughters

    # Add LoKiFunctors to the tool chain, just as we did to the Hybrid::TupleTool above
    # these functors will be applied to the refitted(!) decay tree
    # they act as a source to the DTFDict
    DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict_" + tool_name)
    DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict_" + tool_name

    DTF_dict = getattr(DTF, "dict_" + tool_name)
    DTF_dict.Variables = dict(**variables)


def XicaddLoki(dtt, branch_name):
    branch = getattr(dtt, branch_name)
    branch.addTupleTool(
        "LoKi::Hybrid::TupleTool/LoKi/%s" % branch_name
    ).Variables = dict(**Xic_LoKiVariables)


def make_tree(
    tree_name,
    input_particles,
    descriptor,
    decay,
    branches,
    combi_cuts,
    dtfd_branch_name=None,
    DTFDictVariables=None,
):  # Lc, K, pi

    combinationSel = Combine3BodySelection(
        tree_name + "_combination",
        input_particles,
        DecayDescriptor=descriptor,
        **combi_cuts
    )

    tupSel = TupleSelection(tree_name, combinationSel, Decay=decay, Branches=branches)

    dtt = tupSel.algorithm()

    dtt.ToolList = toolList[:]

    dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    dtt.TupleToolRecoStats.Verbose = True
    dtt.addTool(TupleToolDecay)

    if dtfd_branch_name:
        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_PV_LcPDG",
            constrainPV=True,
            constrainDaughters=["Lambda_c+"],
        )

        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_PV",
            constrainPV=True,
        )

        MakeDTFD(
            dtt,
            dtfd_branch_name,
            DTFDictVariables,
            tool_name="DTF_LcPDG",
            constrainPV=False,
            constrainDaughters=["Lambda_c+"],
        )

    XicaddLoki(dtt, "Xicst")
    XicaddLoki(dtt, "Lc")

    if DaVinci().Simulation:
        dtt.ToolList += ["TupleToolMCBackgroundInfo", "TupleToolMCTruth"]
        relations = TeslaTruthUtils.getRelLocs() + [
            TeslaTruthUtils.getRelLoc(""),
            # Location of the truth tables for PersistReco objects
            "Relations/Hlt2/Protos/Charged",
        ]
        TeslaTruthUtils.makeTruth(dtt, relations, mcToolList)

    return [SelectionSequence(tree_name + "_seq", tupSel).sequence()]


## Lambda_c tree

tupSel = TupleSelection(
    "Lc2PKPi",
    [Lcp],
    Decay="[Lambda_c+ -> ^p+  ^K- ^pi+]CC",
    Branches={
        "Lc": "^[Lambda_c+ -> p+  K- pi+]CC",
        "Lc_p": "[Lambda_c+ -> ^p+  K- pi+]CC",
        "Lc_Km": "[Lambda_c+ -> p+  ^K- pi+]CC",
        "Lc_pip": "[Lambda_c+ -> p+  K- ^pi+]CC",
    },
)

Lc_dtt = tupSel.algorithm()
Lc_dtt.ToolList = toolList[:]
Lc_dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
Lc_dtt.TupleToolRecoStats.Verbose = True
Lc_dtt.addTool(TupleToolDecay)
XicaddLoki(Lc_dtt, "Lc")

all_sequences += [SelectionSequence("Lc2PKPi_seq", tupSel).sequence()]


# # right sign tree
all_sequences += make_tree(
    "XicStSt_LcPiK_RS",
    [Lcp, my_pions, my_kaons],
    "[Sigma_c*+ ->  Lambda_c+ pi+ K-]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^pi+ ^K-]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi+ K-]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) pi+ K-]CC",
        "Lc_p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) pi+ K-]CC",
        "Lc_Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) pi+ K-]CC",
        "Lc_pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) pi+ K-]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi+ ^K-]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^pi+ K-]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)


all_sequences += make_tree(
    "XicStSt_LcPiK_WS1",
    [Lcp, my_pions, my_kaons],
    "[Sigma_c*+ ->  Lambda_c+ pi- K+]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^pi- ^K+]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi- K+]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) pi- K+]CC",
        "Lc_p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) pi- K+]CC",
        "Lc_Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) pi- K+]CC",
        "Lc_pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) pi- K+]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi- ^K+]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^pi- K+]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)


all_sequences += make_tree(
    # LcKpi_WS2 = make_tree(
    "XicStSt_LcPiK_WS2",
    [Lcp, my_pions, my_kaons],
    "[Sigma_c*+ ->  Lambda_c+ pi+ K+]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^pi+ ^K+]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi+ K+]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) pi+ K+]CC",
        "Lc_p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) pi+ K+]CC",
        "Lc_Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) pi+ K+]CC",
        "Lc_pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) pi+ K+]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi+ ^K+]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^pi+ K+]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)


all_sequences += make_tree(
    # LcKpi_WS3 = make_tree(
    "XicStSt_LcPiK_WS3",
    [Lcp, my_pions, my_kaons],
    "[Sigma_c*+ ->  Lambda_c+ pi- K-]cc",
    "[Sigma_c*+ ->  ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^pi- ^K-]CC",
    {
        "Xicst": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi- K-]CC",
        "Lc": "[Sigma_c*+ -> ^(Lambda_c+ -> p+  K- pi+) pi- K-]CC",
        "Lc_p": "[Sigma_c*+ -> (Lambda_c+ -> ^p+  K- pi+) pi- K-]CC",
        "Lc_Km": "[Sigma_c*+ -> (Lambda_c+ -> p+  ^K- pi+) pi- K-]CC",
        "Lc_pip": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- ^pi+) pi- K-]CC",
        "Kbach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) pi- ^K-]CC",
        "pibach": "[Sigma_c*+ -> (Lambda_c+ -> p+  K- pi+) ^pi- K-]CC",
    },
    LcKpi_combi_cuts,
    dtfd_branch_name="Xicst",
    DTFDictVariables={
        "M12": "M12",
        "M": "M",
        "PT": "PT",
        "Lc_M": "CHILD(M, 1)",
        "Lc_PT": "CHILD(PT, 1)",
    },
)

## MC-only RS tree
if DaVinci().Simulation:
    LcPi_MCRS_combi = CombineSelection(
        "LcPi_MCRS_combi",
        [Lcp, my_pions],
        DecayDescriptor="[Sigma_c*++ -> Lambda_c+ pi+]cc",
        CombinationCut="in_range(50, AM-AM1, 400)",
        DaughtersCuts=Xic_decay_products,
        MotherCut=("BPVVALID() & in_range(75, M-M1, 375)"),
        ReFitPVs=True,
        CheckOverlapTool="LoKi::CheckOverlap",
    )
    LcPiK_MCRS_combi = CombineSelection(
        "LcPiK_MCRS_combi",
        [LcPi_MCRS_combi, my_kaons],
        DecayDescriptor="[Sigma_c*+ -> Sigma_c*++ K-]cc",
        CombinationCut="(APT2 > 0*MeV)",
        DaughtersCuts=Xic_decay_products,
        MotherCut=(
            "BPVVALID() & (M - CHILD(M, 1, 1) < 1600) & (VFASPF(VCHI2PDOF) < 20)"
        ),
        ReFitPVs=True,
        CheckOverlapTool="LoKi::CheckOverlap",
    )

    Xicst_LcPiK_MCRS_tsel = TupleSelection(
        "Xicst_LcPiK_MCRS",
        [LcPiK_MCRS_combi],
        Decay="[Sigma_c*+ -> ^(Sigma_c*++ -> ^(Lambda_c+ -> ^p+  ^K- ^pi+) ^pi+) ^K-]CC",
        Branches={
            "Xicst": "^[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> p+  K- pi+) pi+) K-]CC",
            "Sigmacpp": "[Sigma_c*+ -> ^(Sigma_c*++ -> (Lambda_c+ -> p+  K- pi+) pi+) K-]CC",
            "Lc": "[Sigma_c*+ -> (Sigma_c*++ -> ^(Lambda_c+ -> p+  K- pi+) pi+) K-]CC",
            "Lc_p": "[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> ^p+  K- pi+) pi+) K-]CC",
            "Lc_Km": "[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> p+  ^K- pi+) pi+) K-]CC",
            "Lc_pip": "[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> p+  K- ^pi+) pi+) K-]CC",
            "Kbach": "[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> p+  K- pi+) pi+) ^K-]CC",
            "pibach": "[Sigma_c*+ -> (Sigma_c*++ -> (Lambda_c+ -> p+  K- pi+) ^pi+) K-]CC",
        },
    )

    Xicst_LcPiK_MCRS_dtt = Xicst_LcPiK_MCRS_tsel.algorithm()
    Xicst_LcPiK_MCRS_dtt.ToolList = toolList[:]
    Xicst_LcPiK_MCRS_dtt.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    Xicst_LcPiK_MCRS_dtt.TupleToolRecoStats.Verbose = True
    Xicst_LcPiK_MCRS_dtt.addTool(TupleToolDecay)

    XicaddLoki(Xicst_LcPiK_MCRS_dtt, "Xicst")
    XicaddLoki(Xicst_LcPiK_MCRS_dtt, "Lc")

    relations = TeslaTruthUtils.getRelLocs() + [
        TeslaTruthUtils.getRelLoc(""),
        # Location of the truth tables for PersistReco objects
        "Relations/Hlt2/Protos/Charged",
    ]
    mc_tools = mcToolList[:]

    tt_mctruth = Xicst_LcPiK_MCRS_dtt.addTupleTool(TupleToolMCTruth)
    tt_mctruth.ToolList = mcToolList[:]

    Xicst_LcPiK_MCRS_dtt.addTupleTool(TupleToolMCBackgroundInfo)
    TeslaTruthUtils.makeTruth(Xicst_LcPiK_MCRS_dtt, relations, mc_tools)

    all_sequences += [
        SelectionSequence("Xicst_LcPiK_MCRS_seq", Xicst_LcPiK_MCRS_tsel).sequence()
    ]


transientEventStore_root = {
    "2016": "/Event/Turbo",
    "2017": "/Event/Charmspec/Turbo",
    "2018": "/Event/Charmspec/Turbo",
}

DaVinci().PrintFreq = 10000
DaVinci().RootInTES = transientEventStore_root[DaVinci().DataType]
DaVinci().InputType = "MDST"

# Ignore events that don't pass the HLT2 trigger.
trigger_filter = LoKi_Filters(
    # Adjust this regular expression to match whatever set of lines you're
    # interested in studying
    HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(HLT2_LINE)
)
DaVinci().EventPreFilters = trigger_filter.filters("TriggerFilter")

# insert our sequence into DaVinci
DaVinci().UserAlgorithms = all_sequences

DaVinci().Lumi = not DaVinci().Simulation


# =============================================================================
# Stuff specific to Persist Reco, not needed for "plain" Turbo
# =============================================================================
# Lines below prevents the error "WARNING Inconsistent setting of
# PersistReco for TurboConf"

DstConf().Turbo = True
TurboConf().PersistReco = True
TurboConf().DataType = DaVinci().DataType

# (9) specific for persist reco (2016)
if TurboConf().DataType == "2016":
    from Configurables import DataOnDemandSvc
    from Configurables import Gaudi__DataLink as Link

    dod = DataOnDemandSvc()
    for name, target, what in [
        (
            "LinkHlt2Tracks",
            "/Event/Turbo/Hlt2/TrackFitted/Long",
            "/Event/Hlt2/TrackFitted/Long",
        )
    ]:
        dod.AlgMap[target] = Link(name, Target=target, What=what, RootInTES="")
