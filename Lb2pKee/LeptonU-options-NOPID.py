# author : Y.Amhis && V.Lisovskyi
# description : Produce MC without PID cuts in the stripping

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, SubstitutePID
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

from Configurables import GaudiSequencer, DataOnDemandSvc,CondDB
from Configurables import DstConf, CaloDstUnPackConf, LoKi__Hybrid__TupleTool
from Configurables import EventNodeKiller, ProcStatusCheck
from Configurables import StrippingReport, TimingAuditor, SequencerTimerTool

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

##from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
##from GaudiConf import IOHelper

# Here, we add all the variables appropriately to the ntuples
def addNtupleToDaVinci(name, isSim, isDST, restrip=0, filtered=False):
  
  theTuple = DecayTreeTuple(name)
  if name == "tuple_eeLine2":
    line = "Bu2LLK_eeLine2"
    lineQ = "'Bu2LLK_eeLine2'"
    theTuple.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC"
    theInput = "Phys/" + line + "/Particles"
    theTuple.addBranches ({
      "Lb":" [Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "L1":" [Lambda_b0->  (J/psi(1S) -> ^e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "L2":" [Lambda_b0->  (J/psi(1S) -> e+ ^e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "Jpsi":" [Lambda_b0->  ^(J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "Proton":" [Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> ^p+  K-) ]CC",
      "Kaon": "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  ^K-) ]CC",
      "Lambdastar": "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ^( Lambda(1520)0 -> p+  K-)]CC"
    })
  elif name == "tuple_mmLine":
    theTuple.Decay = "[Lambda_b0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(Lambda(1520)0 -> ^p+ ^K-)]CC"
    line = "Bu2LLK_mmLine"
    lineQ = "'Bu2LLK_mmLine'"
    theInput = "Phys/" + line + "/Particles"
    theTuple.addBranches ({
      "Lb":" [Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "L1":" [Lambda_b0->  (J/psi(1S) -> ^mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "L2":" [Lambda_b0->  (J/psi(1S) -> mu+ ^mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "Jpsi":" [Lambda_b0->  ^(J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
      "Proton":" [Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> ^p+  K-) ]CC",
      "Kaon": "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  ^K-) ]CC",
      "Lambdastar": "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ^( Lambda(1520)0 -> p+  K-)]CC"
    })
  else:
    print("Unknown tuple name :", name)
    return

  # The restripping
  if (isSim and restrip):
    if Year == "2012" : 
      strip = "stripping21r0p1"
    elif Year == "2011" : 
      strip = "stripping21r1p1"
    elif Year == "2016" :
      #strip = "stripping28r2"
      strip = "stripping28r1p1"
    elif Year == "2017" :
      strip = "stripping29r2p1"
    elif Year == "2018" :
      strip = "stripping34r0p1"
    else:
      raise("ERROR: year not configured for restripping")
    # The event node killer should be run before  
    event_node_killer = EventNodeKiller("StripKiller")
    event_node_killer.Nodes = ["/Event/AllStreams", "/Event/Strip"]
    
    DaVinci().appendToMainSequence( [ event_node_killer ] )
    streams = buildStreams(stripping=strippingConfiguration(strip), archive=strippingArchive(strip))

    #custom_stream = StrippingStream("CustomStream"+line)
    custom_stream = StrippingStream("CustomStream")
    custom_line = "Stripping" + line
    print (line)
    print (custom_line)
    for stream in streams:
        for line2 in stream.lines:
            if line2.name() == custom_line:
                custom_stream.appendLines([line2])
                print ("line2 name during strip lines loop:", line2.name())
    # Create the actual Stripping configurable
    filterBadEvents = ProcStatusCheck()
    sc = StrippingConf(Streams=[custom_stream], MaxCandidates=2000, AcceptBadEvents=False, BadEventSelection=filterBadEvents)
    # The output is placed directly into Phys, so we only need to
    print ("line2 name after creating strip config:", line2.name())
    # for some very weird reason this is not the same anymore!!
    # so we don't use this
    #tesLoc = "/Event/Phys/{0}/Particles".format(line2.name())
    #print "now we print the bloody tes location"
    #print tesLoc
    # get the selection(s) created by the stripping
    #strippingSels = [DataOnDemand(Location=tesLoc)]
    sr = StrippingReport(Selections = sc.selections())
    DaVinci().appendToMainSequence( [ sc.sequence() ] )

  if isSim:
    if restrip:
      theTuple.Inputs = ["/Event/Phys/{}/Particles".format(line)]
    elif filtered:
      stream = "Bu2KLL_NoPID.Strip"
      #line_noPID = line.replace("Bu2LLK", "Bu2LLKNoPID")
      line_noPID=line
      theTuple.Inputs = ['/Event/{}/Phys/{}/Particles'.format(stream,line_noPID)]
    else: #flagged MC
      theTuple.Inputs = ['/Event/AllStreams/Phys/{}/Particles'.format(line)]
  else:
    DaVinci().RootInTES = "/Event/Leptonic/"
    theTuple.Inputs = [ theInput ]

  TriggerListL0 = [
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0DiMuonDecision"
  ]
  if Year == "2011" or Year == "2012":
    TriggerListHlt =[   
      "Hlt1TrackAllL0Decision",
      "Hlt1DiMuonLowMassDecision",
      "Hlt1DiMuonHighMassDecision",
      "Hlt1MuTrackDecision",
      "Hlt1TrackMuonDecision",
      "Hlt2Topo2BodyBBDTDecision",
      "Hlt2Topo3BodyBBDTDecision",
      "Hlt2Topo4BodyBBDTDecision",
      "Hlt2TopoE2BodyBBDTDecision",
      "Hlt2TopoE3BodyBBDTDecision",
      "Hlt2TopoE4BodyBBDTDecision",
      "Hlt2TopoMu2BodyBBDTDecision",
      "Hlt2TopoMu3BodyBBDTDecision",
      "Hlt2TopoMu4BodyBBDTDecision",
      "Hlt2RadiativeTopoTrackTOSDecision",
      "Hlt2RadiativeTopoPhotonL0Decision",
      "Hlt2SingleMuonDecision",
      "Hlt2DiMuonDetachedDecision"
    ]
  elif Year in ["2015", "2016", "2017", "2018"]:
    TriggerListHlt = ['Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1TrackMuonMVADecision',  'Hlt1TrackMuonDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1DiMuonHighMassDecision', 'Hlt2Topo2BodyDecision','Hlt2Topo3BodyDecision','Hlt2Topo4BodyDecision','Hlt2TopoE2BodyDecision','Hlt2TopoE3BodyDecision','Hlt2TopoE4BodyDecision','Hlt2TopoMu2BodyDecision','Hlt2TopoMu3BodyDecision','Hlt2TopoMu4BodyDecision','Hlt2TopoEE2BodyDecision','Hlt2TopoEE3BodyDecision','Hlt2TopoEE4BodyDecision','Hlt2TopoMuMu2BodyDecision','Hlt2TopoMuMu3BodyDecision','Hlt2TopoMuMu4BodyDecision','Hlt2DiMuonDetachedDecision','Hlt2SingleMuonDecision']
#    TriggerListHlt = ['Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision', 'Hlt1TrackMuonMVADecision',  'Hlt1TrackMuonDecision', 'Hlt1SingleMuonHighPTDecision', 'Hlt1DiMuonLowMassDecision', 'Hlt1DiMuonHighMassDecision', 'Hlt2Topo2BodyBBDTDecision','Hlt2Topo3BodyBBDTDecision','Hlt2Topo4BodyBBDTDecision','Hlt2TopoE2BodyBBDTDecision','Hlt2TopoE3BodyBBDTDecision','Hlt2TopoE4BodyBBDTDecision','Hlt2TopoMu2BodyBBDTDecision','Hlt2TopoMu3BodyBBDTDecision','Hlt2TopoMu4BodyBBDTDecision','Hlt2TopoEE2BodyBBDTDecision','Hlt2TopoEE3BodyBBDTDecision','Hlt2TopoEE4BodyBBDTDecision','Hlt2TopoMuMu2BodyBBDTDecision','Hlt2TopoMuMu3BodyBBDTDecision','Hlt2TopoMuMu4BodyBBDTDecision','Hlt2DiMuonDetachedDecision','Hlt2SingleMuonDecision']

  
## We are redefining the tupletool list from scratch : the default tupletool list does not load some of important geometry variables.

  theTuple.ToolList = []

  LoKiVariables = LoKi__Hybrid__TupleTool("LoKiVariables")
  LoKiVariables.Preambulo += ['from LoKiTracks.decorators import *']
  LoKiVariables.Preambulo += ['masses = LoKi.AuxDTFBase.MASSES()',
                              'masses ["J/psi(1S)"] = 3686.097'  ]
  LoKiVariables.Variables = {
      #"LOKI_ENERGY" : "E",
      "LOKI_ETA"    : "ETA",
      "LOKI_PHI"    : "PHI",
      "inMuon"      : "switch(INMUON, 1, 0)",
      "ETA" : "ETA",
      "LOKI_DTF_CTAU"        : "DTF_CTAU( 0, True )",
      "LOKI_DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
      "LOKI_DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
      "LOKI_DTF_CTAUERR"     : "DTF_CTAUERR( 0, True )",
      "LOKI_MASS_JpsiConstr" : "DTF_FUN ( M , True , 'J/psi(1S)' )" , 
      "LOKI_MASS_psi2SConstr": "DTF_FUN ( M , True , 'J/psi(1S)', masses )" ,
      "LOKI_MASS_LbConstr"   : "DTF_FUN ( M , True , 'Lambda_b0' )", 
      "LOKI_DTF_MASS" : "DTF_FUN ( M , True )" ,
      "LOKI_DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
      #some more variables just to study
      ## 'PVX' : "BPV(VX)",
      ## 'PVY' : "BPV(VY)",
      ## 'PVZ' : "BPV(VZ)",
      ## 'VX' : "VFASPF(VX)",
      ## 'VY' : "VFASPF(VY)",
      ## 'VZ' : "VFASPF(VZ)",
      ## 'X_travelled' : "VFASPF(VX)-BPV(VX)",
      ## 'Y_travelled' : "VFASPF(VY)-BPV(VY)",
      ## 'Z_travelled' : "VFASPF(VZ)-BPV(VZ)",
      ## 'P_Parallel' : "BPVDIRA*P",
      ## 'P_Perp' : "sin(acos(BPVDIRA))*P",
     # 'Corrected_Mass' : "BPVCORRM",
  }
  theTuple.addTool(LoKiVariables , name = "LoKiVariables" )
  theTuple.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiVariables"]

  #if isSim:
#  if isDST == "DST":# for the DST MC, filling isolation variables is easy
#    theTuple.addTupleTool("TupleToolConeIsolation")
#    theTuple.TupleToolConeIsolation.MinConeSize = 0.5
#    theTuple.TupleToolConeIsolation.MaxConeSize = 0.5
#    #theTuple.TupleToolConeIsolation.FillAsymmetry = True
#    #theTuple.TupleToolConeIsolation.FillDeltas = True
#    #theTuple.TupleToolConeIsolation.FillComponents = False
#    theTuple.addTupleTool("TupleToolTrackIsolation")
#    theTuple.TupleToolTrackIsolation.MaxConeAngle = 0.5
#    #theTuple.TupleToolTrackIsolation.FillAsymmetry = True
#    #theTuple.TupleToolTrackIsolation.FillDeltaAngles = True
#    theTuple.addTupleTool("TupleToolVtxIsoln")    
   

  #if isSim==False: 
  #if isDST == "MDST": ##isolation variables from RelInfo of microDST (data)
  if True: #should work both in data and MC! Too lazy to unindent all the block 
    if isSim:
      if restrip:
        path_pref = '/Event/Phys/'
      else: 
        path_pref = '/Event/AllStreams/Phys/'
    else:
      path_pref = '/Event/Leptonic/Phys/'

    LoKi_Cone =  LoKi__Hybrid__TupleTool("LoKi_Cone")
    LoKi_Cone.Variables = {}
    
    # track iso for children
    track_iso_vars = {
      'cpt': 'CONEPT',
      'cp' : 'CONEP',
      'cmult': 'CONEMULT',
      'ptasy': 'CONEPTASYM', 
      'pasy' : 'CONEPASYM',
      'deltaEta': 'CONEDELTAETA',
      'deltaPhi': 'CONEDELTAPHI'}
    child_list  = ['L1', 'L2', 'H1', 'H2', 'LL', 'HH']
    child_track_vars = dict( ("{}_{}_0.50".format(ch, var_ntp), 
                              "RELINFO('{}'".format(path_pref) + lineQ +"'/TrackIsoInfo{}', '{}', -999.)".format(ch, var_loki) )
                             for ch in child_list
                             for var_ntp, var_loki in track_iso_vars.items()
                             )
    LoKi_Cone.Variables.update( child_track_vars )
    
    # track iso for parent
    parent_radii = ["10", "15", "20"]
    parent_track_vars = dict( ("Lb_{}_{}".format(r, var_ntp),
                               "RELINFO('{}'".format(path_pref) + lineQ +"'/TrackIsoInfo{}', '{}', -999.)".format(r, var_loki))
                              for r in parent_radii
                              for var_ntp, var_loki in track_iso_vars.items()
                              )
    LoKi_Cone.Variables.update( parent_track_vars )    

    # track iso BDT for children
    track_iso_bdt_names = ["TRKISOBDTFIRSTVALUE", "TRKISOBDTSECONDVALUE", "TRKISOBDTTHIRDVALUE"]
    track_iso_bdt_vars = dict( ("TrIsoBDT_{}_{}".format(ch, var_name),
                                "RELINFO('{}'".format(path_pref) + lineQ +"'/TrackIsoBDTInfo{}', '{}', -999.)".format(ch, var_name))
                               for ch in child_list
                               for var_name in track_iso_bdt_names
                               )
    LoKi_Cone.Variables.update(track_iso_bdt_vars)

    # cone iso for children
    cone_iso_vars = {
      'cc_mult': 'CC_MULT',
      'cc_sPT' : 'CC_SPT',
      'cc_VPT' : 'CC_VPT',
      'cc_asy_P': 'CC_PASYM', 
      'cc_asy_PT': 'CC_PTASYM',
      'cc_deltaEta': 'CC_DELTAETA',
      'cc_deltaPhi': 'CC_DELTAPHI',
      'cc_IT': 'CC_IT',
      'cc_maxPt_Q': 'CC_MAXPT_Q',
      'cc_maxPt_PT': 'CC_MAXPT_PT',
      'cc_maxPt_PE': 'CC_MAXPT_PE',
      'cc_maxPt_PZ': 'CC_MAXPT_PZ',
      }
    neutral_cone_iso_vars = dict( (var_ntp.replace("cc", "nc"),
                                   var_loki.replace("CC", "NC")) 
                                   for var_ntp, var_loki in cone_iso_vars.items()
                                  if var_loki not in ['CC_MAXPT_Q', 'CC_MAXPT_PE']
                                  )
    cone_iso_vars.update( neutral_cone_iso_vars )
    child_cone_vars = dict( ("{}_0.50_{}".format(ch, var_ntp), 
                              "RELINFO('{}'".format(path_pref) + lineQ +"'/ConeIsoInfo{}', '{}', -999.)".format(ch, var_loki) )
                             for ch in child_list
                             for var_ntp, var_loki in cone_iso_vars.items()
                             )
    LoKi_Cone.Variables.update( child_cone_vars )    

    # cone iso for parent
    parent_cone_vars  = dict( ("Lb_{}_{}".format(r, var_ntp),
                               "RELINFO('{}'".format(path_pref) + lineQ +"'/ConeIsoInfo{}', '{}', -999.)".format(r, var_loki))
                              for r in parent_radii
                              for var_ntp, var_loki in cone_iso_vars.items()
                              )
    LoKi_Cone.Variables.update( parent_cone_vars )

    # vertex iso info
    vtxiso_vars = {
      "NumVtxWithinChi2WindowOneTrack": 'VTXISONUMVTX',
      "SmallestDeltaChi2OneTrack"     : 'VTXISODCHI2ONETRACK',
      "SmallestDeltaChi2MassOneTrack" : 'VTXISODCHI2MASSONETRACK',
      "SmallestDeltaChi2TwoTracks"    : 'VTXISODCHI2TWOTRACK',
      "SmallestDeltaChi2MassTwoTracks": 'VTXISODCHI2MASSTWOTRACK',
      }
    vtxiso_vars_dict = dict( (var_ntp, "RELINFO('{}'".format(path_pref) + lineQ +"'/VertexIsoInfo', '{}', -999.)".format(var_loki))
                             for var_ntp, var_loki in vtxiso_vars.items() )
    LoKi_Cone.Variables.update(vtxiso_vars_dict)

    # vtx iso BDT
    vtx_iso_bdt_names = ["VTXISOBDTHARDFIRSTVALUE", "VTXISOBDTHARDSECONDVALUE", "VTXISOBDTHARDTHIRDVALUE"]
    vtx_iso_bdt_vars = dict( ("VtxIsoBDT_{}".format(var_name),
                              "RELINFO('{}'".format(path_pref) + lineQ +"'/VertexIsoBDTInfo', '{}', -999.)".format(var_name))
                               for var_name in vtx_iso_bdt_names
                               )
    LoKi_Cone.Variables.update(vtx_iso_bdt_vars)

    theTuple.Lb.addTool(LoKi_Cone , name = "LoKi_Cone" )
    theTuple.Lb.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKi_Cone"]

##### mass hypothesis substitutions, to study misID backgrounds #####
  theTuple.Lb.addTupleTool( 'TupleToolSubMass' )
  theTuple.Lb.ToolList += [ "TupleToolSubMass" ]
  
## common for mm and ee modes:  
  theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => pi+"]+["K+ => p+"]
  theTuple.Lb.TupleToolSubMass.Substitution += ["p+ => K+"]+["p+ => pi+"]

  theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K-/p+ => p+/K-"]+["K-/p+ => pi+/K-"]+["K-/p+ => p+/pi-"]+["K-/p+ => pi+/pi-"]+["K-/p+ => mu+/mu-"]+["K-/p+ => e+/e-"]

##involving specific leptons:
  #electrons:
  if name == "tuple_eeLine2":
    theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => e+"]+["p+ => e+"]
    theTuple.Lb.TupleToolSubMass.Substitution += ["e+ => K+"]+["e+ => pi+"]+["e+ => p+"]+["e+ => mu+"]
   
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["e+/e- => pi+/pi-"]+["e+/e- => K+/K-"]+["e+/e- => K+/pi-"]+["e+/e- => pi+/K-"]+["e+/e- => mu+/mu-"]
    
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/e+ => e+/K+"]+["K+/e+ => pi+/pi+"]+["K+/e+ => p+/p+"]
  
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/e+ => e+/p+"]+["p+/e+ => pi+/pi+"]+["p+/e+ => K+/K+"]+["p+/e+ => K+/pi+"]
  
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/e- => e+/K-"]+["K+/e- => pi+/pi-"]+["K+/e- => p+/p~-"]+["K+/e- => pi+/K-"]+["K+/e- => pi+/p~-"]+["K+/e- => p+/pi-"]+["K+/e- => p+/K-"]+["K+/e- => mu+/mu-"]
    
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/e- => e+/p~-"]+["p+/e- => K+/K-"]+["p+/e- => pi+/pi-"]+["p+/e- => pi+/p~-"]+["p+/e- => K+/pi-"]+["p+/e- => K+/p~-"]+["p+/e- => pi+/K-"]+["p+/e- => mu+/mu-"]


  #muons
  if name == "tuple_mmLine" :  
    theTuple.Lb.TupleToolSubMass.Substitution += ["K+ => mu+"]+["p+ => mu+"]
    theTuple.Lb.TupleToolSubMass.Substitution += ["mu+ => K+"]+["mu+ => pi+"]+["mu+ => p+"]+["mu+ => e+"]

    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["mu+/mu- => pi+/pi-"]+["mu+/mu- => K+/K-"]+["mu+/mu- => K+/pi-"]+["mu+/mu- => pi+/K-"]+["mu+/mu- => e+/e-"]
    
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/mu+ => mu+/K+"]+["K+/mu+ => pi+/pi+"]+["K+/mu+ => p+/p+"]
  
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/mu+ => mu+/p+"]+["p+/mu+ => pi+/pi+"]+["p+/mu+ => K+/K+"]+["p+/mu+ => K+/pi+"]
 
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["K+/mu- => mu+/K-"]+["K+/mu- => pi+/pi-"]+["K+/mu- => p+/p~-"]+["K+/mu- => pi+/K-"]+["K+/mu- => pi+/p~-"]+["K+/mu- => p+/pi-"]+["K+/mu- => p+/K-"]+["K+/mu- => e+/e-"] 
    
    theTuple.Lb.TupleToolSubMass.DoubleSubstitution += ["p+/mu- => mu+/p~-"]+["p+/mu- => pi+/pi-"]+["p+/mu- => pi+/p~-"]+["p+/mu- => K+/pi-"]+["p+/mu- => K+/p~-"]+["p+/mu- => pi+/K-"]+["p+/mu- => e+/e-"]

#other useful variables
  theTuple.addTupleTool('TupleToolTrackInfo')#, name = "TupleToolTrackInfo")
  theTuple.TupleToolTrackInfo.Verbose=True

  theTuple.addTupleTool('TupleToolEventInfo')#, name = "TupleToolTrackInfo")
  theTuple.TupleToolEventInfo.Verbose=True

  theTuple.addTupleTool('TupleToolPropertime')#, name = "TupleToolTrackInfo")
  theTuple.TupleToolPropertime.Verbose=True   

  ##theTuple.addTupleTool('TupleToolRICHPid')#, name="TupleToolRICHPid")
  ##theTuple.TupleToolRICHPid.Verbose=True
  ### probably we do not need it for the moment

  theTuple.addTupleTool('TupleToolRecoStats')#, name="TupleToolRecoStats")
  theTuple.TupleToolRecoStats.Verbose=True

  theTuple.addTupleTool('TupleToolGeometry')#, name="TupleToolGeometry")
  theTuple.TupleToolGeometry.Verbose=True

  theTuple.addTupleTool('TupleToolKinematic')#, name="TupleToolKinematic")
  theTuple.TupleToolKinematic.Verbose=True

  theTuple.addTupleTool('TupleToolPid')#, name="TupleToolPid")
  theTuple.TupleToolPid.Verbose=True

  theTuple.addTupleTool('TupleToolBremInfo')#, name="TupleToolPid")
  theTuple.TupleToolBremInfo.Verbose=True

  theTuple.addTupleTool('TupleToolPhotonInfo')#, name="TupleToolPid")
  theTuple.TupleToolPhotonInfo.Verbose=True

  theTuple.addTupleTool('TupleToolTrigger')#, name="TupleToolPid")
  theTuple.TupleToolTrigger.Verbose=True

  theTuple.addTupleTool('TupleToolAngles')#, name="TupleToolPid")
  theTuple.TupleToolAngles.Verbose=True

  theTuple.addTupleTool('TupleToolPrimaries')#, name="TupleToolPid")
  theTuple.TupleToolPrimaries.Verbose=True

  LbDocas = theTuple.Lb.addTupleTool('TupleToolDOCA')
  theTuple.Lb.TupleToolDOCA.Verbose=True

  LbDocas.Name = ["Jpsi", "Lambdastar"]
  
  if name=="tuple_eeLine2":
    LbDocas.Location1 = ["[Lambda_b0->  (J/psi(1S) -> ^e+ e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
                         "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> ^p+  K-) ]CC"]
    LbDocas.Location2 = ["[Lambda_b0->  (J/psi(1S) -> e+ ^e-)  ( Lambda(1520)0 -> p+  K-) ]CC",
                         "[Lambda_b0->  (J/psi(1S) -> e+ e-)  ( Lambda(1520)0 -> p+  ^K-) ]CC"]

  if name=="tuple_mmLine":
    LbDocas.Location1 = ["[Lambda_b0->  (J/psi(1S) -> ^mu+ mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
                         "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> ^p+  K-) ]CC"]
    LbDocas.Location2 = ["[Lambda_b0->  (J/psi(1S) -> mu+ ^mu-)  ( Lambda(1520)0 -> p+  K-) ]CC",
                         "[Lambda_b0->  (J/psi(1S) -> mu+ mu-)  ( Lambda(1520)0 -> p+  ^K-) ]CC"]

  print(len(LbDocas.Name))
  print(len(LbDocas.Location1))

  #theTuple.addTupleTool('TupleToolL0Data')
  theTuple.ToolList += ["TupleToolL0Data"]  
  #theTuple.addTupleTool('TupleToolAllTracks')#, name="TupleToolPid")
  #theTuple.TupleToolAllTracks.Verbose=True  ##not works for mDST

 # theTuple.addTupleTool('TupleToolTrackPosition')
 # theTuple.TupleToolTrackPosition.Z=7500.


  #needs a self-build DaVinci version
#  theTuple.Lb.addTupleTool('TupleToolP2VV') 
#  theTuple.Lb.TupleToolP2VV.Verbose=True 
#  theTuple.Lb.TupleToolP2VV.Calculator = 'Lb2LstarMuMuAngleCalculator'
## test mode! self-written calculator

  # HOP: only when using the proper DV version
  if line == "Bu2LLK_eeLine2": #only for electrons
    theTuple.addTupleTool('TupleToolHOP')
    theTuple.TupleToolHOP.Verbose=True

    LoKiCaloVars = LoKi__Hybrid__TupleTool("LoKiCaloVars")
    LoKiCaloVars.Variables = {
        "TRACK_PX" : "TRFUN(TrPX)",
        "TRACK_PY" : "TRFUN(TrPY)",
        "TRACK_PZ" : "TRFUN(TrPZ)",
        "TrP"      : "TRFUN(TrP)",
        "TrQ"      : "TRFUN(TrQ)",

        "LoKi_InAccBrem" : "PPINFO( LHCb.ProtoParticle.InAccBrem, -1 )",

        "CaloTrMatch"       : "PPINFO( LHCb.ProtoParticle.CaloTrMatch,       -999 )",
        "CaloElectronMatch" : "PPINFO( LHCb.ProtoParticle.CaloElectronMatch, -999 )",
        "CaloBremMatch"     : "PPINFO( LHCb.ProtoParticle.CaloBremMatch,     -999 )"
        }

    theTuple.L1.addTool(LoKiCaloVars , name="LoKiCaloVars" )
    theTuple.L1.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiCaloVars"]
    theTuple.L2.addTool(LoKiCaloVars , name="LoKiCaloVars" )
    theTuple.L2.ToolList   += [ "LoKi::Hybrid::TupleTool/LoKiCaloVars"]

  theTuple.addTupleTool('TupleToolTISTOS')#, name = "TupleToolTISTOS")
  theTuple.TupleToolTISTOS.TriggerList = TriggerListL0
  theTuple.TupleToolTISTOS.TriggerList += TriggerListHlt

  theTuple.TupleToolTISTOS.Verbose = True 
  theTuple.TupleToolTISTOS.VerboseL0 = True
  theTuple.TupleToolTISTOS.VerboseHlt1 = True
  theTuple.TupleToolTISTOS.VerboseHlt2 = True

  theTuple.addTupleTool('TupleToolANNPID')#, name = "TupleToolANNPID")
  #theTuple.TupleToolANNPID.ANNPIDTunes = ["MC12TuneV2", "MC12TuneV3", "MC12TuneV4"] ## if we want to overwrite the ANNPID in order to select only MC12 but not MC15 variables.


  theTuple.L1.addTupleTool( 'TupleToolL0Calo', name = "L1L0ECalo" )
  theTuple.L1.ToolList += [ "TupleToolL0Calo/L1L0ECalo" ]
  theTuple.L1.L1L0ECalo.WhichCalo = "ECAL"
  theTuple.L2.addTupleTool( 'TupleToolL0Calo', name = "L2L0ECalo" )
  theTuple.L2.ToolList += [ "TupleToolL0Calo/L2L0ECalo" ]
  theTuple.L2.L2L0ECalo.WhichCalo = "ECAL"

  theTuple.L1.addTupleTool( 'TupleToolL0Calo', name = "L1L0HCalo" )
  theTuple.L1.ToolList += [ "TupleToolL0Calo/L1L0HCalo" ]
  theTuple.L1.L1L0HCalo.WhichCalo = "HCAL"
  theTuple.L2.addTupleTool( 'TupleToolL0Calo', name = "L2L0HCalo" )
  theTuple.L2.ToolList += [ "TupleToolL0Calo/L2L0HCalo" ]
  theTuple.L2.L2L0HCalo.WhichCalo = "HCAL"

  theTuple.Kaon.addTupleTool( 'TupleToolL0Calo', name = "KaonL0Calo" )
  theTuple.Kaon.ToolList += [ "TupleToolL0Calo/KaonL0Calo" ]
  theTuple.Kaon.KaonL0Calo.WhichCalo = "HCAL"
  theTuple.Proton.addTupleTool( 'TupleToolL0Calo', name = "ProtonL0Calo" )
  theTuple.Proton.ToolList += [ "TupleToolL0Calo/ProtonL0Calo" ]
  theTuple.Proton.ProtonL0Calo.WhichCalo = "HCAL"


##DecayTreeFitter
  theTuple.Lb.addTupleTool( 'TupleToolDecayTreeFitter', name = "DTF" )
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_PV",
                                            constrainToOriginVertex = True ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_JPsi",
                                           constrainToOriginVertex = False,
                                            daughtersToConstrain = [ "J/psi(1S)" ] ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF.clone( "DTF_Psi",
                                            constrainToOriginVertex = False,
                                            Substitutions = { "[ [Lambda_b0]cc -> ^J/psi(1S) Lambda(1520)0 ]CC" : "psi(2S)" },
                                            daughtersToConstrain = [ "psi(2S)" ] ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_Psi" ]

  # DTF with substitutions
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubPToK",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) ( Lambda(1520)0 -> ^p+ K- )" : "K+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- K+)" : "K-"
                                                                         } ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubPToK" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubPToPi",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ K-)" : "pi+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- K+)" : "pi-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubPToPi" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKToPi",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> p+ ^K-)" : "pi-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> p~- ^K+)" : "pi+"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKToPi" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKToP",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> p+ ^K-)" : "pi-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> p~- ^K+)" : "pi+"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKToP" ]

  # double misID
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToPK",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "p~-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "p+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ Hadron)" : "K+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- Hadron)" : "K-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToPK" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToPiK",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "pi-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "pi+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ Hadron)" : "K+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- Hadron)" : "K-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToPiK" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToPPi",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "p~-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "p+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ Hadron)" : "pi+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- Hadron)" : "pi-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToPPi" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToPiPi",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "pi-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "pi+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ Hadron)" : "pi+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- Hadron)" : "pi-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToPiPi" ]
  # use X here instead of Hadron since particle becomes lepton after first substitution
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToMuMu",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "mu-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "mu+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ X-)" : "mu+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- X+)" : "mu-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToMuMu" ]
  theTuple.Lb.addTupleTool( theTuple.Lb.DTF_JPsi.clone( "DTF_JPsi_SubKPToEE",
                                                         Substitutions={ "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> Hadron ^K-)" : "e-",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> Hadron ^K+)" : "e+",
                                                                         "Lambda_b0 -> J/psi(1S) (Lambda(1520)0 -> ^p+ X-)" : "e+",
                                                                         "Lambda_b~0 -> J/psi(1S) (Lambda(1520)~0 -> ^p~- X+)" : "e-"} ) )
  theTuple.Lb.ToolList += [ "TupleToolDecayTreeFitter/DTF_JPsi_SubKPToEE" ]  
  
##_TRUE* variables for MC
  if isSim:
    theTuple.addTupleTool('TupleToolMCTruth')
    theTuple.addTupleTool('TupleToolMCBackgroundInfo')
    theTuple.TupleToolMCTruth.ToolList = ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
    theTuple.ToolList += [ "TupleToolMCTruth" ]
    theTuple.ToolList += [ "TupleToolMCBackgroundInfo" ]

  # create tuple seq and add to DaVinci
  tuple_seq = GaudiSequencer("TupleSeq") # 1st time it's created, 2nd use same obj
  tuple_seq.IgnoreFilterPassed = True # don't filter
  tuple_seq.Members.append(theTuple)
  if tuple_seq not in DaVinci().UserAlgorithms: # add only once
    DaVinci().UserAlgorithms += [tuple_seq]

    

# Return the database information based on inputs
def getDBInfo(sim, magnet, lyr):
  if sim:
    if lyr == "2012":
      TagDDDB = "dddb-20150928"
      TagCondDB = "sim-20160321"#-2-vc-md100
      #TagDDDB   = "dddb-20150522"# New version for calo reconstruction
      #TagCondDB = "sim-20150522"    
    if lyr == "2011":
      #TagDDDB   += "-1"
      TagDDDB = "dddb-20160318-1"
      TagCondDB = "sim-20160614"
      TagCondDB += "-1"
      TagCondDB += "-vc"
    if lyr == "2012":
      #TagDDDB   += "-2"
      TagCondDB += "-2"
      TagCondDB += "-vc"

    if lyr == "2015":
      TagDDDB   = "dddb-20150724"
      TagCondDB = "sim-20161124-vc"
    if lyr == "2016":
      TagDDDB   = "dddb-20170721-3"#"dddb-20150724"
      TagCondDB = "sim-20170721-2-vc"#"sim-20161124-2-vc"
    if lyr == "2017":
      TagDDDB   = "dddb-20170721-3" #Lb2L1520mm MC 
      TagCondDB = "sim-20190430-1-vc" #Lb2L1520mm MC 
    if lyr == "2018":
      TagDDDB   = "dddb-20170721-3"
      #TagCondDB = "sim-20190128-vc"
      TagCondDB = "sim-20190430-vc" #Lb2pK(Jpsi)ll MC (2020 prod)
      
    if magnet == "MD":
      TagCondDB += "-md100"
    else:
      TagCondDB += "-mu100"
  else: #for data - default tags are the best
    if lyr == "2011": 
      #TagDDDB  = "dddb-20150928"
      TagDDDB  = ""#"dddb-20150522-1"
      #TagCondDB= "cond-20150409-1"
      TagCondDB= ""#"cond-20150409"
    elif lyr == "2012" : 
      TagDDDB  = ""#"dddb-20150928"
      TagCondDB= ""#"cond-20150409-1"
    elif lyr == "2015" :
      TagDDDB  = ""#"dddb-20150724"
      #TagCondDB= "cond-20150828"
      TagCondDB= ""#"cond-20151016"
    elif lyr == "2016" : 
      TagDDDB  = ""#"dddb-20150724"
      TagCondDB= ""#"cond-20160517"
      #TagCondDB= "cond-20160717"
    elif lyr == "2017" : 
      TagDDDB  = ""
      TagCondDB= ""
    elif lyr == "2018" : 
      TagDDDB  = ""
      TagCondDB= ""

  return TagDDDB, TagCondDB

##############################################################
#        You should make changes in the lines below          #   
##############################################################

# 1) SELECT THE YEAR #########################################
#Year = "2011"
#Year = "2012"
#Year = "2015"
Year = "2016"
#Year = "2017"
#Year = "2018"

# 2) SELECT MAGNET POLARITY ###################################
MagnetPolarity= "MU"
#MagnetPolarity= "MD"

# 3) SELECT DATA OR MC ########################################
#DaVinci().Simulation = True
DaVinci().Simulation = False

if DaVinci().Simulation: #if MC
  
  # 3a) IF YOUR MC IS MDST BUT NOT DST PLEASE CHANGE THIS ####################
  DaVinci().InputType = "DST"
  #DaVinci().InputType = "MDST" #==> please select this line if running on MC which is MDST
  #calo rerunning for Run1 MC
#  if Year == "2011" or Year == "2012":
#     importOptions("$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")
  DaVinci().Lumi = False
else: #if data
  DaVinci().InputType = "MDST"
  DaVinci().Lumi = True

#gets DB tags as described above
tDDDB, tConDB = getDBInfo(DaVinci().Simulation, MagnetPolarity, Year)
DaVinci().DDDBtag   = tDDDB
DaVinci().CondDBtag = tConDB

#DaVinci().DDDBtag   = 'dddb-20150928'
#DaVinci().CondDBtag = 'sim-20160321-2-vc-md100'

# 4) PICK HERE IF YOU RUN ELECTRONS OR MUONS ################################
#addNtupleToDaVinci("tuple_eeLine2", DaVinci().Simulation, DaVinci().InputType, filtered = True)
addNtupleToDaVinci("tuple_mmLine", DaVinci().Simulation, DaVinci().InputType)
#addNtupleToDaVinci("tuple_eeLine2", DaVinci().Simulation, DaVinci().InputType, restrip=True)

DaVinci().DataType = Year

DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().TupleFile = "LeptonU.root"
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
#MessageSvc().OutputLevel = VERBOSE #to debug


#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068070/0000/00068070_00000007_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input=["/eos/lhcb/grid/prod/lhcb/MC/2016/ALLSTREAMS.DST/00075762/0000/00075762_00000020_1.AllStreams.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068043/0000/00068043_00000023_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068070/0000/00068070_00000001_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/ALLSTREAMS.DST/00056299/0000/00056299_00000005_3.AllStreams.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod/lhcb/validation/Collision16/LEPTONIC.MDST/00068826/0000/00068826_00000107_1.leptonic.mdst"]
#/eos/lhcb/grid/prod//lhcb/validation/Collision16/LEPTONIC.MDST/00068824/0000/00068824_00000148_1.leptonic.mdst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod/lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068049/0000/00068049_00000020_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068067/0000/00068067_00000001_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input = ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2012/ALLSTREAMS.DST/00051197/0000/00051197_00000071_2.AllStreams.dst"]     
#DaVinci().Input = [
#"/eos/lhcb/grid/prod/lhcb/MC/2012/LEPTONIC.STRIP.DST/00061364/0000/00061364_00000010_1.leptonic.strip.dst"
#  "/eos/lhcb/grid/prod/lhcb/MC/2012/LEPTONIC.STRIP.DST/00061352/0000/00061352_00000003_1.leptonic.strip.dst"
#                   ]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/LHCb/Collision16/LEPTONIC.MDST/00059905/0000/00059905_00000581_1.leptonic.mdst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2011/ALLSTREAMS.DST/00066846/0000/00066846_00000001_5.AllStreams.dst"]
#DaVinci().Input = ["/eos/lhcb/grid/prod//lhcb/MC/2016/BU2KLL_NOPID.STRIP.DST/00068070/0000/00068070_00000008_1.bu2kll_nopid.strip.dst"]
#DaVinci().Input = ["root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/2018/ALLSTREAMS.DST/00087084/0000/00087084_00000002_7.AllStreams.dst"] # Lb2pKJpsi(mm) MC 2018 MagUp
#DaVinci().Input = ["root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/2018/ALLSTREAMS.DST/00086927/0000/00086927_00000001_7.AllStreams.dst"] # Lb2pKJpsi(ee) MC 2018 MagDown
#DaVinci().Input = ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/LEPTONIC.MDST/00092412/0000/00092412_00000216_1.leptonic.mdst"] # Leptonic 2018 data
