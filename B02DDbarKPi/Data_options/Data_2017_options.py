from B02DDbarKPi import selectB2DDh

the_rootintes = ""
DDDB = "dddb-20170721-3"
CONDDB = "cond-20170724"

from Configurables import DaVinci
from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters

# Pre-filter to improve performance
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02HHD02HHBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02K3PiD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02HHD02K3PiBeauty2CharmLineDecision')"""
    )

dv = DaVinci(  
               DDDBtag         = DDDB                      ,
               CondDBtag       = CONDDB                    ,
               EventPreFilters = fltrs.filters ('Filters') ,
               EvtMax          =   -1                      ,
             )

dv.RootInTES = "/Event/Bhadron/"
the_year = dv.DataType
simulation = dv.Simulation
inputType = dv.InputType

################## Shouldn't need to edit below this ######################
from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
dv.appendToMainSequence(selectB2DDh.configure_b2oc_aman_ddhDalitz_selection( locationRoot = the_rootintes, dataYear = the_year, isSimulation = simulation ) )


