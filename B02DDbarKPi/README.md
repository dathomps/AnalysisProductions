# Amplitude Analysis

## Analysis Summary

This is an amplitude analysis of the $`B^0 \rightarrow D^0 \bar{D^0} K^+ \pi^-`$ decay, with either $`(D^0 \rightarrow K^- \pi^+) (\bar{D^0} \rightarrow K^+ \pi^-)`$, $`(D^0 \rightarrow K^- \pi^+ \pi^+ \pi^-) (\bar{D^0} \rightarrow K^+ \pi^-)`$ or $`(D^0 \rightarrow K^- \pi^+) (\bar{D^0} \rightarrow K^+ \pi^- \pi^- \pi^+)`$. The analysis follows from the [branching fraction measurement of this decay](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/B2DDKpi), and adds 2017 and 2018 data as well as $`D \rightarrow K 3\pi`$ sub-decays.

## Analysis Production

### Options files

There is a general purpose options file `selectB2DDh.py` that is used for all jobs, and is called from options files in the `Data_options` and `MC_options` folders. Magnet polarities are separated by bookkeeping location. $`D \rightarrow K\pi`$ modes use the *StrippingB02D0D0KstD02HHD02HHBeauty2CharmLine* stripping line, whereas $`D \rightarrow K3\pi`$ modes use the *B02D0D0KstD02HHD02K3PiBeauty2CharmLine* stripping line.

### Data

Data is taken from the BHadron stream. 

#### 2018

Stripping34 and DaVinci/v44r4 are used for 2018 data.

#### 2017

Stripping29r2 and DaVinci/v42r7p2 are used for 2017 data.

#### 2016

Stripping28r2 and DaVinci/v44r10p5 are used for 2016 data.

#### 2012

Stripping21 and DaVinci/v44r5 are used for 2012 data.

#### 2011

Stripping21r1 and DaVinci/v44r5 are used for 2011 data.

### MC

The $`D \rightarrow K3\pi`$ MC samples for this production are of event types **11198099** (Full signal PHSP), **11198012** ($`D^*DK`$ mode - control mode in previous analysis) and **12197026** ($`B^+ \rightarrow DDK^+`$ mode - particularly dangerous source of background).

$`D \rightarrow K\pi`$ MC samples are of event types **11196099** (Full signal) and **11196011** (control).

Stripping lines and DaVinci versions are consistent across data/MC. ReDecay versions of the KPi signal mode are included for normalisation.

Note: for KPi MC the 2012 options file can be used for 2011 data

