from B02DDbarKPi import selectB2DDh

the_rootintes = "/Event/b2ddkst.Strip/"
K3Pi = False
DDDB = "dddb-20170721-3"
CONDDB = "sim-20180411-vc-mu100"

from Configurables import DaVinci
from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS('StrippingB02D0DKBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0DKD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02HHD02HHBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02K3PiD02K3PiBeauty2CharmLineDecision') | \
    HLT_PASS('StrippingB02D0D0KstD02HHD02K3PiBeauty2CharmLineDecision')"""
    )

dv = DaVinci ( EventPreFilters = fltrs.filters ('Filters') ,
               DDDBtag         =        DDDB               ,
               CondDBtag       =        CONDDB             ,
               EvtMax          =        -1
             )

the_year = dv.DataType
simulation = dv.Simulation
inputType = dv.InputType

################## Shouldn't need to edit below this ######################
from Configurables import DecayTreeTuple             
from DecayTreeTuple.Configuration import *
dv.appendToMainSequence(selectB2DDh.configure_b2oc_aman_ddhDalitz_selection( locationRoot = the_rootintes, dataYear = the_year, isSimulation = simulation, isK3Pi = K3Pi ) )


